﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace Sufindo.Report.ListItemByRak
{
    public partial class ListItemByRakFormCR : Form
    {
        Connection conn;
        public ListItemByRakFormCR()
        {
            InitializeComponent();
        }

        public ListItemByRakFormCR(String ITEMCODE, String RAK, Int64 MAXQTY, Int64 MINQTY)
        {
            InitializeComponent();

            ListItemByRakDS listitembyrakds = new ListItemByRakDS();
            DataSet ds = listitembyrakds;

            conn = new Connection();

            List<SqlParameter> sqlParam = new List<SqlParameter>();
            if (ITEMCODE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@ITEMCODE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@ITEMCODE", ITEMCODE));
            }

            if (RAK.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@RAK", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@RAK", RAK));
            }

            sqlParam.Add(new SqlParameter("@MAXQTY", DBNull.Value));
            sqlParam.Add(new SqlParameter("@MINQTY", DBNull.Value));

            conn.fillDataSetQuery(ref ds, "SELECT_DATA_STOCKTAG", sqlParam, "ListItemByRakDataTable");

            ListItemByRakCR listitembyrackCR = new ListItemByRakCR();

            listitembyrackCR.SetDataSource(ds);

            crystalReportViewer.ReportSource = listitembyrackCR;
            crystalReportViewer.Refresh();
        }
    }
}
