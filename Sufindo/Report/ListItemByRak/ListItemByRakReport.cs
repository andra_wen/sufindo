﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Sufindo.Report.ListItemByRak
{
    public partial class ListItemByRakReport : Form
    {
        private delegate void fillAutoCompleteTextBox();

        private Itemdatagridview itemdatagridview;
        private Rackdatagridview rackdatagridview;
        private Connection connection;

        private MenuStrip menustripAction;
        private List<String> PartNo;
        private List<String> rackID;

        public ListItemByRakReport()
        {
            InitializeComponent();
            if (connection == null) {
                connection = new Connection();
            }
        }

        public ListItemByRakReport(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            this.menustripAction = menustripAction;
            if (connection == null)
            {
                connection = new Connection();
            }
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }
        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e){

        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e) {
            if (rackID == null)
            {
                rackID = new List<string>();
            }
            else
            {
                rackID.Clear();
            }
            SqlDataReader sqldataReader = connection.sqlDataReaders(String.Format("SELECT RACKID FROM M_RACK"));

            while (sqldataReader.Read())
            {
                rackID.Add(sqldataReader.GetString(0));
            }

            if (PartNo == null)
            {
                PartNo = new List<string>();
            }
            else
            {
                PartNo.Clear();
            }
            String query = String.Format("SELECT ITEMID FROM M_ITEM");
            sqldataReader = connection.sqlDataReaders(query);

            while (sqldataReader.Read())
            {
                PartNo.Add(sqldataReader.GetString(0));
            }

            if (IsHandleCreated) BeginInvoke(new fillAutoCompleteTextBox(this.fillAutoComplete));
        }

        private void fillAutoComplete()
        {
            RackIDtextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            RackIDtextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            RackIDtextBox.AutoCompleteCustomSource.AddRange(rackID.ToArray());
            RackIDtextBox.Text = "";

            PartNotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            PartNotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            PartNotextBox.AutoCompleteCustomSource.Clear();
            PartNotextBox.AutoCompleteCustomSource.AddRange(PartNo.ToArray());
        }

        public void print() {
            String rackid = "";
            String partno = "";

            if (!RackIDtextBox.Text.Equals("")) {
                rackid = RackIDtextBox.Text;
                partno = "";
            }

            if (!PartNotextBox.Text.Equals("")) {
                partno = PartNotextBox.Text;
                rackid = "";
            }
            ListItemByRakFormCR listitembyrackformcr = new ListItemByRakFormCR(partno, rackid, 0, 0);
            listitembyrackformcr.ShowDialog();
        }

        private void searchPartNobutton_Click(object sender, EventArgs e)
        {
            if (itemdatagridview == null)
            {
                itemdatagridview = new Itemdatagridview("MASTERITEM");
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
            else if (itemdatagridview.IsDisposed)
            {
                itemdatagridview = new Itemdatagridview("MASTERITEM");
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
        }

        private void MasterItemPassingData(DataTable sender) {
            PartNotextBox.Text = sender.Rows[0]["ITEMID"].ToString();
        }

        private void MasterRackPassingData(DataTable sender) {
            RackIDtextBox.Text = sender.Rows[0]["RACKID"].ToString();
        }

        private void searchRackIDbutton_Click(object sender, EventArgs e)
        {
            if (rackdatagridview == null)
            {
                rackdatagridview = new Rackdatagridview("MASTERRACK");
                rackdatagridview.masterRackPassingData = new Rackdatagridview.MasterRackPassingData(MasterRackPassingData);
                rackdatagridview.ShowDialog();
            }
            else if (itemdatagridview.IsDisposed)
            {
                rackdatagridview = new Rackdatagridview("MASTERRACK");
                rackdatagridview.masterRackPassingData = new Rackdatagridview.MasterRackPassingData(MasterRackPassingData);
                rackdatagridview.ShowDialog();
            }
        }
        private void InventoryAuditReport_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            Main.ACTIVEFORM = this;
            reloadAllData();
        }

        private void PartNotextBox_Leave(object sender, EventArgs e)
        {
            Boolean isExistingPartNo = false;
            foreach (String item in PartNo)
            {
                if (item.ToUpper().Equals(PartNotextBox.Text.ToUpper()))
                {
                    isExistingPartNo = true;
                    PartNotextBox.Text = PartNotextBox.Text.ToUpper();
                    break;
                }
            }
            if (!isExistingPartNo)
            {
                PartNotextBox.Text = "";
            }
        }

        private void PartNotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in PartNo)
                {
                    if (item.ToUpper().Equals(PartNotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        PartNotextBox.Text = PartNotextBox.Text.ToUpper();
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    PartNotextBox.Text = "";
                }
            }
        }

        private void RackIDtextBox_Leave(object sender, EventArgs e)
        {
            Boolean isExistingPartNo = false;
            foreach (String item in rackID)
            {
                if (item.ToUpper().Equals(RackIDtextBox.Text.ToUpper()))
                {
                    isExistingPartNo = true;
                    RackIDtextBox.Text = RackIDtextBox.Text.ToUpper();
                    break;
                }
            }
            if (!isExistingPartNo)
            {
                RackIDtextBox.Text = "";
            }
        }

        private void RackIDtextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in rackID)
                {
                    if (item.ToUpper().Equals(RackIDtextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        RackIDtextBox.Text = RackIDtextBox.Text.ToUpper();
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    RackIDtextBox.Text = "";
                }
            }
        }

        private void InventoryAuditReport_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }

        private void textBoxMinQTY_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = Utilities.onlyNumberTextBox(sender, e);
        }

        private void textBoxMaxQTY_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = Utilities.onlyNumberTextBox(sender, e);
        }
    }
}
