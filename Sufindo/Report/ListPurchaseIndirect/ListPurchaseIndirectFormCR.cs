﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace Sufindo.Report.ListPurchaseIndirect
{
    public partial class ListPurchaseIndirectFormCR : Form
    {
        Connection conn;
        public ListPurchaseIndirectFormCR()
        {
            InitializeComponent();
        }

        public ListPurchaseIndirectFormCR(String FROMDATE, String TODATE, String PURCHASEID, Boolean groupby)
        {
            InitializeComponent();

            ListPurchaseIndirectDS listpurchaseindirectds = new ListPurchaseIndirectDS();
            DataSet ds = listpurchaseindirectds;

            conn = new Connection();

            List<SqlParameter> sqlParam = new List<SqlParameter>();
            if (FROMDATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", FROMDATE));
            }

            if (TODATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@TODATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@TODATE", TODATE));
            }

            if (PURCHASEID.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@PURCHASEID", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@PURCHASEID", PURCHASEID));
            }

            sqlParam.Add(new SqlParameter("@GROUPBY", groupby ? 1 : 0));

            conn.fillDataSetQuery(ref ds, "SELECT_DATA_PURCHASEINDIRECT", sqlParam, "ListPurchaseIndirectDataTable");

            if (groupby)
            {
                ListPurchaseIndirectPerSupplierCR listpurchaseindirectsupplier = new ListPurchaseIndirectPerSupplierCR();
                FROMDATE = FROMDATE.Equals("") ? "" : DateTime.Parse(FROMDATE).ToString("dd/MM/yyyy");
                TODATE = TODATE.Equals("") ? "" : DateTime.Parse(TODATE).ToString("dd/MM/yyyy");
                Object parameter = (FROMDATE.Equals("") || TODATE.Equals("")) ? "-" : String.Format("{0} - {1}", FROMDATE, TODATE);

                listpurchaseindirectsupplier.SetDataSource(ds);
                listpurchaseindirectsupplier.SetParameterValue(0, parameter);

                crystalReportViewer.ReportSource = listpurchaseindirectsupplier;
            }
            else
            {
                ListPurchaseIndirectCR listpurchaseindirectcr = new ListPurchaseIndirectCR();
                FROMDATE = FROMDATE.Equals("") ? "" : DateTime.Parse(FROMDATE).ToString("dd/MM/yyyy");
                TODATE = TODATE.Equals("") ? "" : DateTime.Parse(TODATE).ToString("dd/MM/yyyy");
                Object parameter = (FROMDATE.Equals("") || TODATE.Equals("")) ? "-" : String.Format("{0} - {1}", FROMDATE, TODATE);

                listpurchaseindirectcr.SetDataSource(ds);
                listpurchaseindirectcr.SetParameterValue(0, parameter);

                crystalReportViewer.ReportSource = listpurchaseindirectcr;
            }
            crystalReportViewer.Refresh();
        }
    }
}
