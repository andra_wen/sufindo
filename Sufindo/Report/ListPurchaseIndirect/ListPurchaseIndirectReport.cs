﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace Sufindo.Report.ListPurchaseIndirect
{
    public partial class ListPurchaseIndirectReport : Form
    {
        private delegate void fillAutoCompleteTextBox();

        private Purchasedatagridview purchasedatagridview;
        private Connection connection;

        private MenuStrip menustripAction;
        private List<String> receiptID;

        public ListPurchaseIndirectReport()
        {
            InitializeComponent();
            if (connection == null) {
                connection = new Connection();
            }
        }

        public ListPurchaseIndirectReport(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            this.menustripAction = menustripAction;
            if (connection == null)
            {
                connection = new Connection();
            }
        }

        private void checkBoxDate_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePickerEnd.Enabled = checkBoxDate.Checked;
            dateTimePickerStart.Enabled = checkBoxDate.Checked;
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }
        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e){

        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e) {
            if (receiptID == null)
            {
                receiptID = new List<string>();
            }
            else
            {
                receiptID.Clear();
            }
            SqlDataReader sqldataReader = connection.sqlDataReaders(String.Format("SELECT PURCHASEID FROM H_PURCHASE A JOIN H_PURCHASE_ORDER B ON A.PURCHASEORDERID = B.PURCHASEORDERID WHERE A.STATUS = 'Actived'"));
            
            while (sqldataReader.Read())
            {
                receiptID.Add(sqldataReader.GetString(0));
            }

            if (IsHandleCreated) BeginInvoke(new fillAutoCompleteTextBox(this.fillAutoComplete));
        }

        private void fillAutoComplete()
        {
            PurchaseIDtextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            PurchaseIDtextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            PurchaseIDtextBox.AutoCompleteCustomSource.AddRange(receiptID.ToArray());
        }

        public void print() {
            String fromDate = "";
            String toDate = "";

            if (checkBoxDate.Checked) {
                fromDate = dateTimePickerStart.Value.ToString("yyyy-MM-dd");
                toDate = dateTimePickerEnd.Value.ToString("yyyy-MM-dd");
            }
            ListPurchaseIndirectFormCR listpurchaseformcr = new ListPurchaseIndirectFormCR(fromDate, toDate, PurchaseIDtextBox.Text, checkBoxGroupBySupplier.Checked);
            listpurchaseformcr.ShowDialog();
        }

        private void labelFromDate_Click(object sender, EventArgs e)
        {
            checkBoxDate.Checked = !checkBoxDate.Checked;
        }
        private void searchInvoiceIDbutton_Click(object sender, EventArgs e)
        {
            if (purchasedatagridview == null)
            {
                purchasedatagridview = new Purchasedatagridview("ListPurchaseIndirectReport");
                purchasedatagridview.purchasePassingData = new Purchasedatagridview.PurchasePassingData(PurchasePassingData);
                //purchasedatagridview.MdiParent = this.MdiParent;
                purchasedatagridview.ShowDialog();
            }
            else if (purchasedatagridview.IsDisposed)
            {
                purchasedatagridview = new Purchasedatagridview("ListPurchaseIndirectReport");
                purchasedatagridview.purchasePassingData = new Purchasedatagridview.PurchasePassingData(PurchasePassingData);
                //purchasedatagridview.MdiParent = this.MdiParent;
                purchasedatagridview.ShowDialog();
            }
        }

        private void PurchasePassingData(DataTable sender)
        {
            PurchaseIDtextBox.Text = sender.Rows[0]["PURCHASEID"].ToString();
        }

        private void ListInvoiceReport_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }

        private void ListInvoiceReport_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            Main.ACTIVEFORM = this;
            reloadAllData();
        }

        private void InvoiceIDtextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in receiptID)
                {
                    if (item.ToUpper().Equals(PurchaseIDtextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        PurchaseIDtextBox.Text = PurchaseIDtextBox.Text.ToUpper();
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    PurchaseIDtextBox.Text = "";
                }
            }
        }

        private void InvoiceIDtextBox_Leave(object sender, EventArgs e)
        {
            Boolean isExistingPartNo = false;
            foreach (String item in receiptID)
            {
                if (item.ToUpper().Equals(PurchaseIDtextBox.Text.ToUpper()))
                {
                    isExistingPartNo = true;
                    PurchaseIDtextBox.Text = PurchaseIDtextBox.Text.ToUpper();
                    break;
                }
            }
            if (!isExistingPartNo)
            {
                PurchaseIDtextBox.Text = "";
            }
        }
    }
}
