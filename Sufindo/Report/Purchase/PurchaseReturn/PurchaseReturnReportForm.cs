﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using System.Data.SqlClient;
using Sufindo.Report.Purchase.PurchaseReturn;

namespace Sufindo
{
    public partial class PurchaseReturnReportForm : Form
    {
        Connection conn;
        String PURCHASERETURNID;

        public PurchaseReturnReportForm()
        {
            InitializeComponent();
        }

        public PurchaseReturnReportForm(String PURCHASERETURNID,Boolean update)
        {
            InitializeComponent();
            this.PURCHASERETURNID = PURCHASERETURNID;
            if (update)
            {
                foreach (ToolStrip ts in crystalReportViewer.Controls.OfType<ToolStrip>())
                {
                    foreach (ToolStripButton tsb in ts.Items.OfType<ToolStripButton>())
                    {
                        if (tsb.ToolTipText.ToLower().Contains("print"))
                        {
                            tsb.Click += new EventHandler(printButton_Click);
                        }
                    }
                }
            }
            PurchaseReturnDS purchaseReturnDS = new PurchaseReturnDS();
            DataSet ds = purchaseReturnDS;

            conn = new Connection();

            List<SqlParameter> sqlParam = new List<SqlParameter>();

            sqlParam.Add(new SqlParameter("@PURCHASERETURNID", this.PURCHASERETURNID));

            conn.fillDataSetQuery(ref ds, "SELECT_DATA_PURCHASE_RETURN", sqlParam, "PurchaseReturnDataTable");

            //SalesReturnCR salesreturncr = new SalesReturnCR();
            PurchaseReturnCR purchasereturncr = new PurchaseReturnCR();
            purchasereturncr.SetDataSource(ds);

            crystalReportViewer.ReportSource = purchasereturncr;
            crystalReportViewer.Refresh();
        }

        void printButton_Click(object sender, EventArgs e)
        {
            List<SqlParameter> sqlParam = new List<SqlParameter>();

            sqlParam = new List<SqlParameter>();
            sqlParam.Add(new SqlParameter("@PURCHASERETURNID", PURCHASERETURNID));
            sqlParam.Add(new SqlParameter("@STATUS", Status.CONFIRM));
            sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));
            if (conn.callProcedure("UPDATE_STATUS_H_PURCHASE_RETURN", sqlParam))
            {
                MessageBox.Show("Printed");
            }
        }
    }
}
