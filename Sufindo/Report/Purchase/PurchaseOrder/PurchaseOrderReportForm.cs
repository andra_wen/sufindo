﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using Sufindo.Report.Purchase.PurchaseOrder;


namespace Sufindo
{
    public partial class PurchaseOrderReportForm : Form
    {
        Connection conn;
        String PURCHASEORDERID;
        public PurchaseOrderReportForm()
        {
            InitializeComponent();
        }

        public PurchaseOrderReportForm(String PURCHASEORDERID, Boolean isalias)
        {
            InitializeComponent();
            conn = new Connection();

            this.PURCHASEORDERID = PURCHASEORDERID;

            foreach (ToolStrip ts in crystalReportViewer.Controls.OfType<ToolStrip>())
            {
                foreach (ToolStripButton tsb in ts.Items.OfType<ToolStripButton>())
                {
                    if (tsb.ToolTipText.ToLower().Contains("print"))
                    {
                        tsb.Click += new EventHandler(printButton_Click);
                    }
                }
            }

            PurchaseOrderDS purchaseorderds = new PurchaseOrderDS();
            DataSet ds = purchaseorderds;

            List<SqlParameter> sqlParam = new List<SqlParameter>();

            sqlParam.Add(new SqlParameter("@PURCHASEORDERID", PURCHASEORDERID));

            conn.fillDataSetQuery(ref ds, "SELECT_DATA_PURCHASE_ORDER", sqlParam, "PurchaseOrderDataTable");

            if (isalias)
            {
                PurchaseOrderWithAliasCR purhcaseorderaliascr = new PurchaseOrderWithAliasCR();
                purhcaseorderaliascr.SetDataSource(ds);
                crystalReportViewer.ReportSource = purhcaseorderaliascr;
            }
            else
            {
                PurchaseOrderCR purchaseordercr = new PurchaseOrderCR();
                purchaseordercr.SetDataSource(ds);
                crystalReportViewer.ReportSource = purchaseordercr;
            }

            crystalReportViewer.Refresh();
        }

        void printButton_Click(object sender, EventArgs e)
        {
            
        }
    }
}
