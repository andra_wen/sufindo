﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Sufindo.Report.InventoryAudit
{
    public partial class InventoryAuditReport : Form
    {
        private delegate void fillAutoCompleteTextBox();

        private Itemdatagridview itemdatagridview;
        private Supplierdatagridview supplierdatagridview;
        private Connection connection;

        private MenuStrip menustripAction;
        private List<String> PartNo;
        private List<String> supplierID;

        public InventoryAuditReport()
        {
            InitializeComponent();
            if (connection == null) {
                connection = new Connection();
            }
        }

        public InventoryAuditReport(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            this.menustripAction = menustripAction;
            if (connection == null)
            {
                connection = new Connection();
            }
        }

        private void checkBoxDate_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePickerEnd.Enabled = checkBoxDate.Checked;
            dateTimePickerStart.Enabled = checkBoxDate.Checked;
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }
        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e){

        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e) {
            if (supplierID == null)
            {
                supplierID = new List<string>();
            }
            else
            {
                supplierID.Clear();
            }
            SqlDataReader sqldataReader = connection.sqlDataReaders(String.Format("SELECT SUPPLIERID FROM M_SUPPLIER WHERE [STATUS] = '{0}'", Status.ACTIVE));

            while (sqldataReader.Read())
            {
                supplierID.Add(sqldataReader.GetString(0));
            }

            if (PartNo == null)
            {
                PartNo = new List<string>();
            }
            else
            {
                PartNo.Clear();
            }
            String query = String.Format("SELECT ITEMID FROM M_ITEM");
            sqldataReader = connection.sqlDataReaders(query);

            while (sqldataReader.Read())
            {
                PartNo.Add(sqldataReader.GetString(0));
            }

            if (IsHandleCreated) BeginInvoke(new fillAutoCompleteTextBox(this.fillAutoComplete));
        }

        private void fillAutoComplete()
        {
            SupplierIDtextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            SupplierIDtextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            SupplierIDtextBox.AutoCompleteCustomSource.AddRange(supplierID.ToArray());
            SupplierIDtextBox.Text = "";

            PartNotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            PartNotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            PartNotextBox.AutoCompleteCustomSource.Clear();
            PartNotextBox.AutoCompleteCustomSource.AddRange(PartNo.ToArray());
        }

        public void print() {
            String fromDate = "";
            String toDate = "";
            String supplierid = "";
            String partno = "";

            if (checkBoxDate.Checked) {
                fromDate = dateTimePickerStart.Value.ToString("yyyy-MM-dd");
                toDate = dateTimePickerEnd.Value.ToString("yyyy-MM-dd");
            }

            if (!SupplierIDtextBox.Text.Equals("")) {
                supplierid = SupplierIDtextBox.Text;
                partno = "";
            }

            if (!PartNotextBox.Text.Equals("")) {
                partno = PartNotextBox.Text;
                supplierid = "";
            }

            InventoryAuditFormCR inventoryauditformcr = new InventoryAuditFormCR(checkBoxPersuplier.Checked, fromDate, toDate, partno, supplierid);
            inventoryauditformcr.ShowDialog();
        }

        private void searchPartNobutton_Click(object sender, EventArgs e)
        {
            if (itemdatagridview == null)
            {
                itemdatagridview = new Itemdatagridview("MASTERITEM");
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
            else if (itemdatagridview.IsDisposed)
            {
                itemdatagridview = new Itemdatagridview("MASTERITEM");
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
        }

        private void MasterItemPassingData(DataTable sender) {
            PartNotextBox.Text = sender.Rows[0]["ITEMID"].ToString();
        }

        private void searchSupplierIDbutton_Click(object sender, EventArgs e)
        {
            if (supplierdatagridview == null)
            {
                supplierdatagridview = new Supplierdatagridview("MasterSupplier");
                supplierdatagridview.masterItemPassingData = new Supplierdatagridview.MasterItemPassingData(MasterSupplierPassingData);
                //supplierdatagridview.MdiParent = this.MdiParent;
                supplierdatagridview.ShowDialog();
            }
            else if (supplierdatagridview.IsDisposed)
            {
                supplierdatagridview = new Supplierdatagridview("MasterSupplier");
                supplierdatagridview.masterItemPassingData = new Supplierdatagridview.MasterItemPassingData(MasterSupplierPassingData);
                //supplierdatagridview.MdiParent = this.MdiParent;
                supplierdatagridview.ShowDialog();
            }
        }

        private void MasterSupplierPassingData(DataTable sender) {
            SupplierIDtextBox.Text = sender.Rows[0]["SUPPLIERID"].ToString();
        }

        private void labelFromDate_Click(object sender, EventArgs e)
        {
            checkBoxDate.Checked = !checkBoxDate.Checked;
        }

        private void InventoryAuditReport_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            Main.ACTIVEFORM = this;
            reloadAllData();
        }

        private void PartNotextBox_Leave(object sender, EventArgs e)
        {
            Boolean isExistingPartNo = false;
            foreach (String item in PartNo)
            {
                if (item.ToUpper().Equals(PartNotextBox.Text.ToUpper()))
                {
                    isExistingPartNo = true;
                    PartNotextBox.Text = PartNotextBox.Text.ToUpper();
                    break;
                }
            }
            if (!isExistingPartNo)
            {
                PartNotextBox.Text = "";
            }
        }

        private void PartNotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in PartNo)
                {
                    if (item.ToUpper().Equals(PartNotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        PartNotextBox.Text = PartNotextBox.Text.ToUpper();
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    PartNotextBox.Text = "";
                }
            }
        }

        private void SupplierIDtextBox_Leave(object sender, EventArgs e)
        {
            Boolean isExistingPartNo = false;
            foreach (String item in supplierID)
            {
                if (item.ToUpper().Equals(SupplierIDtextBox.Text.ToUpper()))
                {
                    isExistingPartNo = true;
                    SupplierIDtextBox.Text = SupplierIDtextBox.Text.ToUpper();
                    break;
                }
            }
            if (!isExistingPartNo)
            {
                SupplierIDtextBox.Text = "";
            }
        }

        private void SupplierIDtextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in supplierID)
                {
                    if (item.ToUpper().Equals(SupplierIDtextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        SupplierIDtextBox.Text = SupplierIDtextBox.Text.ToUpper();
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    SupplierIDtextBox.Text = "";
                }
            }
        }

        private void InventoryAuditReport_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }
    }
}
