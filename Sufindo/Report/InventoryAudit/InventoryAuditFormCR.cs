﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace Sufindo.Report.InventoryAudit
{
    public partial class InventoryAuditFormCR : Form
    {
        Connection conn;
        public InventoryAuditFormCR()
        {
            InitializeComponent();
        }

        public InventoryAuditFormCR(Boolean Persupplier, String FROMDATE, String TODATE, String ITEMCODE, String SUPPLIERID)
        {
            InitializeComponent();
            conn = new Connection();
            List<SqlParameter> sqlParam = new List<SqlParameter>();
            if (FROMDATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", FROMDATE));
            }

            if (TODATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@TODATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@TODATE", TODATE));
            }

            if (ITEMCODE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@ITEMCODE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@ITEMCODE", ITEMCODE));
            }

            if (SUPPLIERID.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@SUPPLIERID", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@SUPPLIERID", SUPPLIERID));
            }

            if (Persupplier)
            {
                InventoryAuditDSSupplier orderpickingds = new InventoryAuditDSSupplier();
                DataSet ds = orderpickingds;

                conn.fillDataSetQuery(ref ds, "SELECT_DATA_STOCKCARD_PERSUPPLIER", sqlParam, "InventoryAuditPerSupplierDataTable");

                InventoryAuditCRSupplier inventoryCR = new InventoryAuditCRSupplier();
                FROMDATE = FROMDATE.Equals("") ? "" : DateTime.Parse(FROMDATE).ToString("dd/MM/yyyy");
                TODATE = TODATE.Equals("") ? "" : DateTime.Parse(TODATE).ToString("dd/MM/yyyy");
                Object parameter = (FROMDATE.Equals("") || TODATE.Equals("")) ? "-" : String.Format("{0}-{1}", FROMDATE, TODATE);

                inventoryCR.SetDataSource(ds);
                inventoryCR.SetParameterValue(0, parameter);

                crystalReportViewer.ReportSource = inventoryCR;
                crystalReportViewer.Refresh();
            }
            else
            {
                InventoryAuditDS orderpickingds = new InventoryAuditDS();
                DataSet ds = orderpickingds;

                conn.fillDataSetQuery(ref ds, "SELECT_DATA_STOCKCARD", sqlParam, "InventoryAuditDataTable");

                InventoryAuditCR inventoryCR = new InventoryAuditCR();
                FROMDATE = FROMDATE.Equals("") ? "" : DateTime.Parse(FROMDATE).ToString("dd/MM/yyyy");
                TODATE = TODATE.Equals("") ? "" : DateTime.Parse(TODATE).ToString("dd/MM/yyyy");
                Object parameter = (FROMDATE.Equals("") || TODATE.Equals("")) ? "-" : String.Format("{0}-{1}", FROMDATE, TODATE);

                inventoryCR.SetDataSource(ds);
                inventoryCR.SetParameterValue(0, parameter);

                crystalReportViewer.ReportSource = inventoryCR;
                crystalReportViewer.Refresh();
            }
        }

        public InventoryAuditFormCR(String FROMDATE, String TODATE, String ITEMCODE, String SUPPLIERID)
        {
            InitializeComponent();

            InventoryAuditDSSupplier orderpickingds = new InventoryAuditDSSupplier();
            DataSet ds = orderpickingds;

            conn = new Connection();

            List<SqlParameter> sqlParam = new List<SqlParameter>();
            if (FROMDATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", DBNull.Value));
            }
            else {
                sqlParam.Add(new SqlParameter("@STARTDATE", FROMDATE));
            }

            if (TODATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@TODATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@TODATE", TODATE));
            }

            if (ITEMCODE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@ITEMCODE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@ITEMCODE", ITEMCODE));
            }

            if (SUPPLIERID.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@SUPPLIERID", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@SUPPLIERID", SUPPLIERID));
            }

            conn.fillDataSetQuery(ref ds, "SELECT_DATA_STOCKCARD", sqlParam, "InventoryAuditDataTable");

            InventoryAuditCR inventoryCR = new InventoryAuditCR();
            FROMDATE = FROMDATE.Equals("") ? "" : DateTime.Parse(FROMDATE).ToString("dd/MM/yyyy");
            TODATE = TODATE.Equals("") ? "" : DateTime.Parse(TODATE).ToString("dd/MM/yyyy");
            Object parameter = (FROMDATE.Equals("") || TODATE.Equals("")) ? "-" : String.Format("{0}-{1}", FROMDATE, TODATE);

            inventoryCR.SetDataSource(ds);
            inventoryCR.SetParameterValue(0, parameter);

            crystalReportViewer.ReportSource = inventoryCR;
            crystalReportViewer.Refresh();
        }
    }
}
