﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace Sufindo.Report.ListPurchaseReturn
{
    public partial class ListPurchaseReturnFormCR : Form
    {
        Connection conn;
        public ListPurchaseReturnFormCR()
        {
            InitializeComponent();
        }

        public ListPurchaseReturnFormCR(String FROMDATE, String TODATE, String PURCHASERETURNID, String ITEMID)
        {
            InitializeComponent();

            ListPurchaseReturnDS listpurchasereturnds = new ListPurchaseReturnDS();
            DataSet ds = listpurchasereturnds;

            conn = new Connection();

            List<SqlParameter> sqlParam = new List<SqlParameter>();
            if (FROMDATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", FROMDATE));
            }

            if (TODATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@TODATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@TODATE", TODATE));
            }

            if (PURCHASERETURNID.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@PURCHASERETURNID", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@PURCHASERETURNID", PURCHASERETURNID));
            }

            if (ITEMID.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@ITEMQTYID", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@ITEMQTYID", ITEMID));
            }

            conn.fillDataSetQuery(ref ds, "SELECT_DATA_PURCHASERETURN", sqlParam, "ListPurchaseReturnDataTable");


            ListPurchaseReturnCR listpurchasereturncr = new ListPurchaseReturnCR();
            FROMDATE = FROMDATE.Equals("") ? "" : DateTime.Parse(FROMDATE).ToString("dd/MM/yyyy");
            TODATE = TODATE.Equals("") ? "" : DateTime.Parse(TODATE).ToString("dd/MM/yyyy");
            Object parameter = (FROMDATE.Equals("") || TODATE.Equals("")) ? "-" : String.Format("{0} - {1}", FROMDATE, TODATE);

            listpurchasereturncr.SetDataSource(ds);
            listpurchasereturncr.SetParameterValue(0, parameter);

            crystalReportViewer.ReportSource = listpurchasereturncr;
            crystalReportViewer.Refresh();
        }
    }
}
