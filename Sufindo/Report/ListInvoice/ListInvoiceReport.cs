﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Sufindo.Report.ListInvoice
{
    public partial class ListInvoiceReport : Form
    {
        private delegate void fillAutoCompleteTextBox();

        private Invoicedatagridview invoicedatagridview;
        private Connection connection;

        private MenuStrip menustripAction;
        private List<String> invoiceID;

        public ListInvoiceReport()
        {
            InitializeComponent();
            if (connection == null) {
                connection = new Connection();
            }
        }

        public ListInvoiceReport(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            this.menustripAction = menustripAction;
            if (connection == null)
            {
                connection = new Connection();
            }
        }

        private void checkBoxDate_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePickerEnd.Enabled = checkBoxDate.Checked;
            dateTimePickerStart.Enabled = checkBoxDate.Checked;
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }
        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e){

        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e) {
            if (invoiceID == null)
            {
                invoiceID = new List<string>();
            }
            else
            {
                invoiceID.Clear();
            }
            SqlDataReader sqldataReader = connection.sqlDataReaders(String.Format("SELECT INVOICEID FROM H_INVOICE"));

            while (sqldataReader.Read())
            {
                invoiceID.Add(sqldataReader.GetString(0));
            }

            if (IsHandleCreated) BeginInvoke(new fillAutoCompleteTextBox(this.fillAutoComplete));
        }

        private void fillAutoComplete()
        {
            InvoiceIDtextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            InvoiceIDtextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            InvoiceIDtextBox.AutoCompleteCustomSource.AddRange(invoiceID.ToArray());
        }

        public void print() {
            String fromDate = "";
            String toDate = "";

            if (checkBoxDate.Checked) {
                fromDate = dateTimePickerStart.Value.ToString("yyyy-MM-dd");
                toDate = dateTimePickerEnd.Value.ToString("yyyy-MM-dd");
            }

            ListInvoiceFormCR listinvoiceformcr = new ListInvoiceFormCR(fromDate, toDate, InvoiceIDtextBox.Text);
            listinvoiceformcr.ShowDialog();
        }

        private void labelFromDate_Click(object sender, EventArgs e)
        {
            checkBoxDate.Checked = !checkBoxDate.Checked;
        }
        private void searchInvoiceIDbutton_Click(object sender, EventArgs e)
        {
            if (invoicedatagridview == null)
            {
                invoicedatagridview = new Invoicedatagridview("ListInvoiceReport");
                invoicedatagridview.invoicePassingData = new Invoicedatagridview.InvoicePassingData(InvoicePassingData);
                //invoicedatagridview.MdiParent = this.MdiParent;
                invoicedatagridview.ShowDialog();
            }
            else if (invoicedatagridview.IsDisposed)
            {
                invoicedatagridview = new Invoicedatagridview("ListInvoiceReport");
                invoicedatagridview.invoicePassingData = new Invoicedatagridview.InvoicePassingData(InvoicePassingData);
                //invoicedatagridview.MdiParent = this.MdiParent;
                invoicedatagridview.ShowDialog();
            }
        }

        private void InvoicePassingData(DataTable sender)
        {
            InvoiceIDtextBox.Text = sender.Rows[0]["INVOICEID"].ToString();
        }

        private void ListInvoiceReport_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0){
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }

        private void ListInvoiceReport_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            Main.ACTIVEFORM = this;
            reloadAllData();
        }

        private void InvoiceIDtextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in invoiceID)
                {
                    if (item.ToUpper().Equals(InvoiceIDtextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        InvoiceIDtextBox.Text = InvoiceIDtextBox.Text.ToUpper();
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    InvoiceIDtextBox.Text = "";
                }
            }
        }

        private void InvoiceIDtextBox_Leave(object sender, EventArgs e)
        {
            Boolean isExistingPartNo = false;
            foreach (String item in invoiceID)
            {
                if (item.ToUpper().Equals(InvoiceIDtextBox.Text.ToUpper()))
                {
                    isExistingPartNo = true;
                    InvoiceIDtextBox.Text = InvoiceIDtextBox.Text.ToUpper();
                    break;
                }
            }
            if (!isExistingPartNo)
            {
                InvoiceIDtextBox.Text = "";
            }
        }
    }
}
