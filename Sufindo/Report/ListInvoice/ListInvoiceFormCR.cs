﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace Sufindo.Report.ListInvoice
{
    public partial class ListInvoiceFormCR : Form
    {
        Connection conn;
        public ListInvoiceFormCR()
        {
            InitializeComponent();
        }

        public ListInvoiceFormCR(String FROMDATE, String TODATE, String INVOICEID)
        {
            InitializeComponent();

            ListInvoiceDS listinvoiceds = new ListInvoiceDS();
            DataSet ds = listinvoiceds;

            conn = new Connection();

            List<SqlParameter> sqlParam = new List<SqlParameter>();
            if (FROMDATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", DBNull.Value));
            }
            else {
                sqlParam.Add(new SqlParameter("@STARTDATE", FROMDATE));
            }

            if (TODATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@TODATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@TODATE", TODATE));
            }

            if (INVOICEID.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@INVOICEID", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@INVOICEID", INVOICEID));
            }

            conn.fillDataSetQuery(ref ds, "SELECT_DATA_INVOICE", sqlParam, "ListInvoiceDataTable");

            ListInvoiceCR listinvoiceCR = new ListInvoiceCR();
            FROMDATE = FROMDATE.Equals("") ? "" : DateTime.Parse(FROMDATE).ToString("dd/MM/yyyy");
            TODATE = TODATE.Equals("") ? "" : DateTime.Parse(TODATE).ToString("dd/MM/yyyy");
            Object parameter = (FROMDATE.Equals("") || TODATE.Equals("")) ? "-" : String.Format("{0}-{1}", FROMDATE, TODATE);

            listinvoiceCR.SetDataSource(ds);
            listinvoiceCR.SetParameterValue(0, parameter);

            crystalReportViewer.ReportSource = listinvoiceCR;
            crystalReportViewer.Refresh();
        }
    }
}
