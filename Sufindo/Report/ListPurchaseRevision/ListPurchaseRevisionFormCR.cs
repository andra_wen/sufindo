﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace Sufindo.Report.ListPurchaseRevision
{
    public partial class ListPurchaseRevisionFormCR : Form
    {
        Connection conn;
        public ListPurchaseRevisionFormCR()
        {
            InitializeComponent();
        }

        public ListPurchaseRevisionFormCR(String FROMDATE, String TODATE, String PURCHASERETURNID, String ITEMID)
        {
            InitializeComponent();

            ListPurchaseRevisionDS listpurchaserevisionds = new ListPurchaseRevisionDS();
//            ListPurchaseRevisionDS listpurchasereturnds = new ListPurchaseRevisionDS();
            DataSet ds = listpurchaserevisionds;

            conn = new Connection();

            List<SqlParameter> sqlParam = new List<SqlParameter>();
            if (FROMDATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", FROMDATE));
            }

            if (TODATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@TODATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@TODATE", TODATE));
            }

            if (PURCHASERETURNID.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@REVISIONID", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@REVISIONID", PURCHASERETURNID));
            }

            if (ITEMID.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@ITEMQTYID", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@ITEMQTYID", ITEMID));
            }

            conn.fillDataSetQuery(ref ds, "SELECT_DATA_RECEIPTREVISION", sqlParam, "ListReceiptRevisionDataTable");


            ListPurchaseRevisionCR listpurchasereturncr = new ListPurchaseRevisionCR();
            FROMDATE = FROMDATE.Equals("") ? "" : DateTime.Parse(FROMDATE).ToString("dd/MM/yyyy");
            TODATE = TODATE.Equals("") ? "" : DateTime.Parse(TODATE).ToString("dd/MM/yyyy");
            Object parameter = (FROMDATE.Equals("") || TODATE.Equals("")) ? "-" : String.Format("{0} - {1}", FROMDATE, TODATE);

            listpurchasereturncr.SetDataSource(ds);
            listpurchasereturncr.SetParameterValue(0, parameter);

            crystalReportViewer.ReportSource = listpurchasereturncr;
            crystalReportViewer.Refresh();
        }
    }
}
