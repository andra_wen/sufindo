﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Sufindo.Report.ListPurchaseRevision
{
    public partial class ListPurchaseRevisionReport : Form
    {
        private delegate void fillAutoCompleteTextBox();

        private ReceiptRevisiondatagridview receiptrevisiondatagridview;
        private Itemdatagridview itemdatagridview;
        private Connection connection;

        private MenuStrip menustripAction;
        private List<String> purchasereturnid;
        private List<String> itemno;

        public ListPurchaseRevisionReport()
        {
            InitializeComponent();
            if (connection == null) {
                connection = new Connection();
            }
        }

        public ListPurchaseRevisionReport(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            this.menustripAction = menustripAction;
            if (connection == null)
            {
                connection = new Connection();
            }
        }

        private void checkBoxDate_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePickerEnd.Enabled = checkBoxDate.Checked;
            dateTimePickerStart.Enabled = checkBoxDate.Checked;
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }
        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e){

        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e) {
            if (purchasereturnid == null)
            {
                purchasereturnid = new List<string>();
            }
            else
            {
                purchasereturnid.Clear();
            }
            SqlDataReader sqldataReader = connection.sqlDataReaders(String.Format("SELECT REVISIONID FROM H_REVISI_PURCHASE"));

            while (sqldataReader.Read())
            {
                purchasereturnid.Add(sqldataReader.GetString(0));
            }

            if (itemno == null)
            {
                itemno = new List<string>();
            }
            else
            {
                itemno.Clear();
            }
            sqldataReader = connection.sqlDataReaders(String.Format("SELECT * FROM M_ITEM"));

            while (sqldataReader.Read())
            {
                itemno.Add(sqldataReader.GetString(0));
            }

            if (IsHandleCreated) BeginInvoke(new fillAutoCompleteTextBox(this.fillAutoComplete));
        }

        private void fillAutoComplete()
        {
            ReceiptRevisionIDtextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            ReceiptRevisionIDtextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            ReceiptRevisionIDtextBox.AutoCompleteCustomSource.AddRange(purchasereturnid.ToArray());

            PartNotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            PartNotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            PartNotextBox.AutoCompleteCustomSource.AddRange(itemno.ToArray());
        }

        public void print() {
            String fromDate = "";
            String toDate = "";

            if (checkBoxDate.Checked) {
                fromDate = dateTimePickerStart.Value.ToString("yyyy-MM-dd");
                toDate = dateTimePickerEnd.Value.ToString("yyyy-MM-dd");
            }

            ListPurchaseRevisionFormCR listpurchaserevisionformcr = new ListPurchaseRevisionFormCR(fromDate, toDate, ReceiptRevisionIDtextBox.Text, PartNotextBox.Text);
            listpurchaserevisionformcr.ShowDialog();
        }

        private void labelFromDate_Click(object sender, EventArgs e)
        {
            checkBoxDate.Checked = !checkBoxDate.Checked;
        }
        private void searchReceiptRevisionIDbutton_Click(object sender, EventArgs e)
        {
            if (receiptrevisiondatagridview == null)
            {
                receiptrevisiondatagridview = new ReceiptRevisiondatagridview("ListPurchaseRevisionReport");
                receiptrevisiondatagridview.receiverevisionPassingData = new ReceiptRevisiondatagridview.ReceiveRevisionPassingData(ReceiveRevisionPassingData);
                //returnpurchasedatagridview.MdiParent = this.MdiParent;
                receiptrevisiondatagridview.ShowDialog();
            }
            else if (receiptrevisiondatagridview.IsDisposed)
            {
                receiptrevisiondatagridview = new ReceiptRevisiondatagridview("ListPurchaseRevisionReport");
                receiptrevisiondatagridview.receiverevisionPassingData = new ReceiptRevisiondatagridview.ReceiveRevisionPassingData(ReceiveRevisionPassingData);
                //receiptrevisiondatagridview.returnpurchasePassingData = new ReturnPurchasedatagridview.ReturnPurchasePassingData(ReturnPurchasePassingData);
                //returnpurchasedatagridview.MdiParent = this.MdiParent;
                receiptrevisiondatagridview.ShowDialog();
            }
        }

        private void ReceiveRevisionPassingData(DataTable sender)
        {
            ReceiptRevisionIDtextBox.Text = sender.Rows[0]["REVISIONID"].ToString();
        }

        private void ListInvoiceReport_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }

        private void ListInvoiceReport_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            Main.ACTIVEFORM = this;
            reloadAllData();
        }

        private void RevisionReceiptIDtextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in purchasereturnid)
                {
                    if (item.ToUpper().Equals(ReceiptRevisionIDtextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        ReceiptRevisionIDtextBox.Text = ReceiptRevisionIDtextBox.Text.ToUpper();
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    ReceiptRevisionIDtextBox.Text = "";
                }
            }
        }

        private void ReceiptRevisionIDtextBox_Leave(object sender, EventArgs e)
        {
            Boolean isExistingPartNo = false;
            foreach (String item in purchasereturnid)
            {
                if (item.ToUpper().Equals(ReceiptRevisionIDtextBox.Text.ToUpper()))
                {
                    isExistingPartNo = true;
                    ReceiptRevisionIDtextBox.Text = ReceiptRevisionIDtextBox.Text.ToUpper();
                    break;
                }
            }
            if (!isExistingPartNo)
            {
                ReceiptRevisionIDtextBox.Text = "";
            }
        }

        private void searchITEMIDbutton_Click(object sender, EventArgs e)
        {
            if (itemdatagridview == null)
            {
                itemdatagridview = new Itemdatagridview("ListReceiptRevisionReport");
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                //returnpurchasedatagridview.MdiParent = this.MdiParent;
                itemdatagridview.ShowDialog();
            }
            else if (itemdatagridview.IsDisposed)
            {
                itemdatagridview = new Itemdatagridview("ListReceiptRevisionReport");
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                //returnpurchasedatagridview.MdiParent = this.MdiParent;
                itemdatagridview.ShowDialog();
            }
        }

        private void MasterItemPassingData(DataTable sender)
        {
            PartNotextBox.Text = sender.Rows[0]["ITEMID"].ToString();
        }
    }
}
