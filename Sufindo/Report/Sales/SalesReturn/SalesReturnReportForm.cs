﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using System.Data.SqlClient;
using Sufindo.Report.Sales.SalesReturn;

namespace Sufindo
{
    public partial class SalesReturnReportForm : Form
    {
        Connection conn;
        String SALESRETURNID;
        Boolean alias;
        public SalesReturnReportForm()
        {
            InitializeComponent();
        }

        public SalesReturnReportForm(String SALESRETURNID, Boolean alias)
        {
            InitializeComponent();
            this.alias = alias;
            this.SALESRETURNID = SALESRETURNID;
            foreach (ToolStrip ts in crystalReportViewer.Controls.OfType<ToolStrip>())
            {
                foreach (ToolStripButton tsb in ts.Items.OfType<ToolStripButton>())
                {
                    if (tsb.ToolTipText.ToLower().Contains("print"))
                    {
                        tsb.Click += new EventHandler(printButton_Click);
                    }
                }
            }
            SalesReturnDS salesReturnDS = new SalesReturnDS();
            DataSet ds = salesReturnDS;

            conn = new Connection();

            List<SqlParameter> sqlParam = new List<SqlParameter>();

            sqlParam.Add(new SqlParameter("@SALESRETURNID", SALESRETURNID));

            conn.fillDataSetQuery(ref ds, "SELECT_DATA_SALES_RETURN", sqlParam, "SalesReturnDataTable");

            if (alias)
            {
                SalesReturnwithAliasCR salesreturnwithaliascr = new SalesReturnwithAliasCR();

                String query = String.Format("SELECT A.TOTAL, B.CURRENCYID FROM h_sales_return A JOIN h_invoice B " +
                                             "ON A.INVOICEID = B.INVOICEID WHERE SALESRETURNID = '{0}'", SALESRETURNID);
                DataTable dt = conn.openDataTableQuery(query);

                Decimal total = Decimal.Parse(dt.Rows[0]["TOTAL"].ToString());
                String curr = dt.Rows[0]["CURRENCYID"].ToString();
                Object terbilang = Utilities.ConvertMoneyToWords(total, curr);
                //invoicecr.SetDataSource(ds);
                
                salesreturnwithaliascr.SetDataSource(ds);
                salesreturnwithaliascr.SetParameterValue(0, terbilang);

                ReportDocument reportDoc = (ReportDocument)salesreturnwithaliascr;
                float width = 229;
                float height = 162;
                System.Drawing.Printing.PrintDocument pd = new System.Drawing.Printing.PrintDocument();
                string defaultPrinter = pd.PrinterSettings.PrinterName;
                CustomPrintForm.CustomPrintForm.AddCustomPaperSize(defaultPrinter, "Envelope C5 229 x 162 mm", width, height);

                foreach (System.Drawing.Printing.PaperSize paperSize in pd.PrinterSettings.PaperSizes)
                {
                    if (paperSize.PaperName == "Envelope C5 229 x 162 mm")
                    {
                        try
                        {
                            reportDoc.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)paperSize.RawKind;
                        }
                        catch (Exception Ex)
                        {
                            MessageBox.Show("Exception: " + Ex);
                            return;
                        }
                    }
                }

                crystalReportViewer.ReportSource = salesreturnwithaliascr;
            }
            else
            {
                SalesReturnCR salesreturncr = new SalesReturnCR();

                String query = String.Format("SELECT A.TOTAL, B.CURRENCYID FROM h_sales_return A JOIN h_invoice B " +
                                             "ON A.INVOICEID = B.INVOICEID WHERE SALESRETURNID = '{0}'", SALESRETURNID);
                DataTable dt = conn.openDataTableQuery(query);

                Decimal total = Decimal.Parse(dt.Rows[0]["TOTAL"].ToString());
                String curr = dt.Rows[0]["CURRENCYID"].ToString();
                Object terbilang = Utilities.ConvertMoneyToWords(total, curr);

                salesreturncr.SetDataSource(ds);
                salesreturncr.SetParameterValue(0, terbilang);

                ReportDocument reportDoc = (ReportDocument)salesreturncr;
                float width = 229;
                float height = 162;
                System.Drawing.Printing.PrintDocument pd = new System.Drawing.Printing.PrintDocument();
                string defaultPrinter = pd.PrinterSettings.PrinterName;
                CustomPrintForm.CustomPrintForm.AddCustomPaperSize(defaultPrinter, "Envelope C5 229 x 162 mm", width, height);

                foreach (System.Drawing.Printing.PaperSize paperSize in pd.PrinterSettings.PaperSizes)
                {
                    if (paperSize.PaperName == "Envelope C5 229 x 162 mm")
                    {
                        try
                        {
                            reportDoc.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)paperSize.RawKind;
                        }
                        catch (Exception Ex)
                        {
                            MessageBox.Show("Exception: " + Ex);
                            return;
                        }
                    }
                }

                crystalReportViewer.ReportSource = salesreturncr;
            }

            crystalReportViewer.Refresh();
        }

        void printButton_Click(object sender, EventArgs e)
        {
            //List<SqlParameter> sqlParam = new List<SqlParameter>();

            //sqlParam.Add(new SqlParameter("@ORDERPICKINGID", this.SALESRETURNID));
            //DataTable dt = conn.callProcedureDatatable("SELECT_DATA_H_ORDER_PICKING", sqlParam);

            //if (dt.Rows.Count > 0) {
            //    sqlParam = new List<SqlParameter>();
            //    sqlParam.Add(new SqlParameter("@SALESORDERID", dt.Rows[0]["SALESORDERID"].ToString()));
            //    sqlParam.Add(new SqlParameter("@STATUS", Status.RELEASE));

            //    if (conn.callProcedure("UPDATE_STATUS_H_SALES_ORDER", sqlParam)) {

            //        sqlParam = new List<SqlParameter>();
            //        sqlParam.Add(new SqlParameter("@ORDERPICKINGID", this.SALESRETURNID));
            //        sqlParam.Add(new SqlParameter("@STATUS", Status.OPEN));
            //        if (conn.callProcedure("UPDATE_STATUS_H_ORDER_PICKING", sqlParam)) {
            //            sqlParam = new List<SqlParameter>();
            //            sqlParam.Add(new SqlParameter("@ORDERPICKINGID", this.SALESRETURNID));
            //            if (conn.callProcedure("INSERT_LOG_TRANSACTION_ORDER_PICKING", sqlParam)) {
            //                MessageBox.Show("SUCCESS INSERT");
            //            }
            //        }
            //    }
            //}
        }
    }
}
