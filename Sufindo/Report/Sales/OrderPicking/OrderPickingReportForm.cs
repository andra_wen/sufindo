﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using System.Data.SqlClient;
using Sufindo.Report.Sales.OrderPicking;

namespace Sufindo
{
    public partial class OrderPickingReportForm : Form
    {
        public delegate void ClearOrderPickingData();

        public ClearOrderPickingData clearOrderPickingData;

        Connection conn;
        String ORDERPICKINGID;
        String CALLED;
        Boolean update;
        public OrderPickingReportForm()
        {
            InitializeComponent();
        }

        public OrderPickingReportForm(String CALLED, String ORDERPICKINGID, Boolean update)
        {
            InitializeComponent();
            this.ORDERPICKINGID = ORDERPICKINGID;
            this.CALLED = CALLED;
            this.update = update;
            foreach (ToolStrip ts in crystalReportViewer.Controls.OfType<ToolStrip>())
            {
                foreach (ToolStripButton tsb in ts.Items.OfType<ToolStripButton>())
                {
                    if (tsb.ToolTipText.ToLower().Contains("print"))
                    {
                        if (update)
                        {
                            tsb.Click += new EventHandler(printButton_Click);
                        }
                    }
                }
            }

            OrderPickingDS orderpickingds = new OrderPickingDS();
            DataSet ds = orderpickingds;

            conn = new Connection();

            List<SqlParameter> sqlParam = new List<SqlParameter>();

            sqlParam.Add(new SqlParameter("@ORDERPICKINGID", ORDERPICKINGID));

            conn.fillDataSetQuery(ref ds, "SELECT_DATA_H_ORDER_PICKING", sqlParam, "OrderPickingDataTable");

            OrderPickingCR orderpickingcr = new OrderPickingCR();
            orderpickingcr.SetDataSource(ds);

            ReportDocument reportDoc = (ReportDocument)orderpickingcr;

            float width = 229;
            float height = 162;
            System.Drawing.Printing.PrintDocument pd = new System.Drawing.Printing.PrintDocument();
            string defaultPrinter = pd.PrinterSettings.PrinterName;
            CustomPrintForm.CustomPrintForm.AddCustomPaperSize(defaultPrinter, "Envelope C5 229 x 162 mm", width, height);

            foreach (System.Drawing.Printing.PaperSize paperSize in pd.PrinterSettings.PaperSizes)
            {
                if (paperSize.PaperName == "Envelope C5 229 x 162 mm")
                {
                    try
                    {
                        reportDoc.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)paperSize.RawKind;
                    }
                    catch (Exception Ex)
                    {
                        MessageBox.Show("Exception: " + Ex);
                        return;
                    }
                }
            }
            crystalReportViewer.ReportSource = reportDoc;
            crystalReportViewer.Show();
        }

        public OrderPickingReportForm(String ORDERPICKINGID, Boolean update)
        {
            InitializeComponent();
            this.ORDERPICKINGID = ORDERPICKINGID;
            this.CALLED = "";
            this.update = update;
            foreach (ToolStrip ts in crystalReportViewer.Controls.OfType<ToolStrip>())
            {
                foreach (ToolStripButton tsb in ts.Items.OfType<ToolStripButton>())
                {
                    if (tsb.ToolTipText.ToLower().Contains("print"))
                    {
                        if (update)
                        {
                            tsb.Click += new EventHandler(printButton_Click);
                        }
                    }
                }
            }

            OrderPickingDS orderpickingds = new OrderPickingDS();
            DataSet ds = orderpickingds;

            conn = new Connection();

            List<SqlParameter> sqlParam = new List<SqlParameter>();

            sqlParam.Add(new SqlParameter("@ORDERPICKINGID", ORDERPICKINGID));

            conn.fillDataSetQuery(ref ds, "SELECT_DATA_H_ORDER_PICKING", sqlParam, "OrderPickingDataTable");

            OrderPickingCR orderpickingcr = new OrderPickingCR();
            orderpickingcr.SetDataSource(ds);

            ReportDocument reportDoc = (ReportDocument)orderpickingcr;

            float width = 229;
            float height = 162;
            System.Drawing.Printing.PrintDocument pd = new System.Drawing.Printing.PrintDocument();
            string defaultPrinter = pd.PrinterSettings.PrinterName;
            CustomPrintForm.CustomPrintForm.AddCustomPaperSize(defaultPrinter, "Envelope C5 229 x 162 mm", width, height);

            foreach (System.Drawing.Printing.PaperSize paperSize in pd.PrinterSettings.PaperSizes)
            {
                if (paperSize.PaperName == "Envelope C5 229 x 162 mm")
                {
                    try
                    {
                        reportDoc.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)paperSize.RawKind;
                    }
                    catch (Exception Ex)
                    {
                        MessageBox.Show("Exception: " + Ex);
                        return;
                    }
                }
            }
            crystalReportViewer.ReportSource = reportDoc;
            crystalReportViewer.Show();

            //this.CALLED = "";
            //this.ORDERPICKINGID = ORDERPICKINGID;
            //this.update = update;
            //foreach (ToolStrip ts in crystalReportViewer.Controls.OfType<ToolStrip>())
            //{
            //    foreach (ToolStripButton tsb in ts.Items.OfType<ToolStripButton>())
            //    {
            //        if (tsb.ToolTipText.ToLower().Contains("print"))
            //        {
            //            if (update)
            //            {
            //                tsb.Click += new EventHandler(printButton_Click);
            //            }
            //        }
            //    }
            //}

            //OrderPickingDS orderpickingds = new OrderPickingDS();
            //DataSet ds = orderpickingds;

            //conn = new Connection();

            //List<SqlParameter> sqlParam = new List<SqlParameter>();

            //sqlParam.Add(new SqlParameter("@ORDERPICKINGID", ORDERPICKINGID));

            //conn.fillDataSetQuery(ref ds, "SELECT_DATA_H_ORDER_PICKING", sqlParam, "OrderPickingDataTable");

            //OrderPickingCR orderpickingcr = new OrderPickingCR();

            //orderpickingcr.SetDataSource(ds);

            //crystalReportViewer.ReportSource = orderpickingcr;
            //crystalReportViewer.Refresh();            
        }

        void printButton_Click(object sender, EventArgs e)
        {
            if (this.CALLED.Equals("") && this.update)
            {
                this.update = false;
                List<SqlParameter> sqlParam = new List<SqlParameter>();
                crystalReportViewer.ShowPrintButton = false;
                sqlParam.Add(new SqlParameter("@ORDERPICKINGID", this.ORDERPICKINGID));
                DataTable dt = conn.callProcedureDatatable("SELECT_DATA_H_ORDER_PICKING", sqlParam);

                if (dt.Rows.Count > 0)
                {
                    //...
                    sqlParam = new List<SqlParameter>();
                    sqlParam.Add(new SqlParameter("@SALESORDERID", dt.Rows[0]["SALESORDERID"].ToString()));
                    sqlParam.Add(new SqlParameter("@STATUS", Status.RELEASE));

                    if (conn.callProcedure("UPDATE_STATUS_H_SALES_ORDER", sqlParam))
                    {
                        sqlParam = new List<SqlParameter>();
                        sqlParam.Add(new SqlParameter("@ORDERPICKINGID", this.ORDERPICKINGID));
                        sqlParam.Add(new SqlParameter("@STATUS", Status.OPEN));
                        if (conn.callProcedure("UPDATE_STATUS_H_ORDER_PICKING", sqlParam))
                        {
                            sqlParam = new List<SqlParameter>();
                            sqlParam.Add(new SqlParameter("@ORDERPICKINGID", this.ORDERPICKINGID));
                            if (conn.callProcedure("INSERT_LOG_TRANSACTION_ORDER_PICKING", sqlParam))
                            {
                                MessageBox.Show("Order Picking printed, stock terpotong, Jika print gagal, print dari Report");
                            }
                        }
                    }
                }
            }
        }

        private void OrderPickingReportForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (clearOrderPickingData != null)
            {
                clearOrderPickingData();
            }
        }
    }
}
