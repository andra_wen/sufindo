﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using Sufindo.Report.Sales.DeliveryOrder;


namespace Sufindo
{
    public partial class DeliveryOrderReportForm : Form
    {
        public delegate void ClearSalesOrderData();

        public ClearSalesOrderData clearSalesOrderData;

        Connection conn;
        String DELIVERYORDERID;
        public DeliveryOrderReportForm()
        {
            InitializeComponent();
        }

        public DeliveryOrderReportForm(String DELIVERYORDERID)
        {
            InitializeComponent();
            conn = new Connection();

            this.DELIVERYORDERID = DELIVERYORDERID;

            foreach (ToolStrip ts in crystalReportViewer.Controls.OfType<ToolStrip>())
            {
                foreach (ToolStripButton tsb in ts.Items.OfType<ToolStripButton>())
                {
                    if (tsb.ToolTipText.ToLower().Contains("print"))
                    {
                        tsb.Click += new EventHandler(printButton_Click);
                    }
                }
            }

            DeliveryOrderDS deliveryorderds = new DeliveryOrderDS();
            DataSet ds = deliveryorderds;
           
            List<SqlParameter> sqlParam = new List<SqlParameter>();

            sqlParam.Add(new SqlParameter("@DELIVERYORDERID", DELIVERYORDERID));

            conn.fillDataSetQuery(ref ds, "SELECT_DATA_DELIVERY_ORDER", sqlParam, "DeliveryOrderDataTable");

            DeliveryOrderCR deliveryordercr = new DeliveryOrderCR();
            deliveryordercr.SetDataSource(ds);

            ReportDocument reportDoc = (ReportDocument)deliveryordercr;

            float width = 229;
            float height = 162;
            System.Drawing.Printing.PrintDocument pd = new System.Drawing.Printing.PrintDocument();
            string defaultPrinter = pd.PrinterSettings.PrinterName;
            CustomPrintForm.CustomPrintForm.AddCustomPaperSize(defaultPrinter, "Envelope C5 229 x 162 mm", width, height);

            foreach (System.Drawing.Printing.PaperSize paperSize in pd.PrinterSettings.PaperSizes)
            {
                if (paperSize.PaperName == "Envelope C5 229 x 162 mm")
                {
                    try
                    {
                        reportDoc.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)paperSize.RawKind;
                    }
                    catch (Exception Ex)
                    {
                        MessageBox.Show("Exception: " + Ex);
                        return;
                    }
                }
            }
            crystalReportViewer.ReportSource = reportDoc;
            crystalReportViewer.Show();
        }

        void printButton_Click(object sender, EventArgs e)
        {
            
        }

        private void InvoiceReportForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (clearSalesOrderData != null)
            {
                clearSalesOrderData();
            }
        }
    }
}
