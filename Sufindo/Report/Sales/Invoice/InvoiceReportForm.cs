﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using Sufindo.Report.Sales.Invoice;


namespace Sufindo
{
    public partial class InvoiceReportForm : Form
    {
        public delegate void ClearSalesOrderData();

        public ClearSalesOrderData clearSalesOrderData;

        Connection conn;
        String INVOICEID;
        public InvoiceReportForm()
        {
            InitializeComponent();
        }

        public InvoiceReportForm(String INVOICEID)
        {
            InitializeComponent();
            conn = new Connection();

            this.INVOICEID = INVOICEID;

            String query = String.Format("SELECT TOTAL, CURRENCYID FROM H_INVOICE " +
                                         "WHERE INVOICEID = '{0}'", this.INVOICEID);
            DataTable dt = conn.openDataTableQuery(query);

            foreach (ToolStrip ts in crystalReportViewer.Controls.OfType<ToolStrip>())
            {
                foreach (ToolStripButton tsb in ts.Items.OfType<ToolStripButton>())
                {
                    if (tsb.ToolTipText.ToLower().Contains("print"))
                    {
                        tsb.Click += new EventHandler(printButton_Click);
                    }
                }
            }

            InvoiceDS invoiceds = new InvoiceDS();
            DataSet ds = invoiceds;
           
            List<SqlParameter> sqlParam = new List<SqlParameter>();

            sqlParam.Add(new SqlParameter("@INVOICEID", INVOICEID));

            conn.fillDataSetQuery(ref ds, "SELECT_DATA_H_INVOICE", sqlParam, "InvoiceDataTable");
            
            InvoiceCR invoicecr = new InvoiceCR();

            Decimal total = Decimal.Parse(dt.Rows[0]["TOTAL"].ToString());
            String curr = dt.Rows[0]["CURRENCYID"].ToString();
            Object terbilang = Utilities.ConvertMoneyToWords(total, curr);

            invoicecr.SetDataSource(ds);
            invoicecr.SetParameterValue(0, terbilang);

            ReportDocument reportDoc = (ReportDocument)invoicecr;

            float width = 229;
            float height = 162;
            System.Drawing.Printing.PrintDocument pd = new System.Drawing.Printing.PrintDocument();
            string defaultPrinter = pd.PrinterSettings.PrinterName;
            CustomPrintForm.CustomPrintForm.AddCustomPaperSize(defaultPrinter, "Envelope C5 229 x 162 mm", width, height);

            foreach (System.Drawing.Printing.PaperSize paperSize in pd.PrinterSettings.PaperSizes)
            {
                if (paperSize.PaperName == "Envelope C5 229 x 162 mm")
                {
                    try
                    {
                        reportDoc.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)paperSize.RawKind;
                    }
                    catch (Exception Ex)
                    {
                        MessageBox.Show("Exception: " + Ex);
                        return;
                    }
                }
            }
            crystalReportViewer.ReportSource = reportDoc;
            crystalReportViewer.Show();

        }

        void printButton_Click(object sender, EventArgs e)
        {
            
        }

        private void InvoiceReportForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (clearSalesOrderData != null)
            {
                clearSalesOrderData();
            }
        }

        private void InvoiceReportForm_Load(object sender, EventArgs e)
        {
            this.crystalReportViewer.Refresh();
        }
    }
}
