﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using System.Data.SqlClient;
using Sufindo.Report.Sales.Quotation;

namespace Sufindo
{
    public partial class QuotationReportForm : Form
    {
        Connection conn;
        String QUOTATIONID;
        public QuotationReportForm()
        {
            InitializeComponent();
        }

        public QuotationReportForm(String QUOTATIONID)
        {
            InitializeComponent();
            
            this.QUOTATIONID = QUOTATIONID;
            foreach (ToolStrip ts in crystalReportViewer.Controls.OfType<ToolStrip>())
            {
                foreach (ToolStripButton tsb in ts.Items.OfType<ToolStripButton>())
                {
                    if (tsb.ToolTipText.ToLower().Contains("print"))
                    {
                        tsb.Click += new EventHandler(printButton_Click);
                    }
                }
            }

            QuotationDS quotationds = new QuotationDS();
            DataSet ds = quotationds;

            conn = new Connection();

            List<SqlParameter> sqlParam = new List<SqlParameter>();

            sqlParam.Add(new SqlParameter("@QUOTATIONID", QUOTATIONID));

            conn.fillDataSetQuery(ref ds, "SELECT_DATA_H_QUOTATION", sqlParam, "QuotationDataTable");

            QuotationCR quotationcr = new QuotationCR();

            quotationcr.SetDataSource(ds);

            crystalReportViewer.ReportSource = quotationcr;
            crystalReportViewer.Refresh();
        }

        void printButton_Click(object sender, EventArgs e)
        {
            //List<SqlParameter> sqlParam = new List<SqlParameter>();

            //sqlParam.Add(new SqlParameter("@QUOTATIONID", this.QUOTATIONID));
            //DataTable dt = conn.callProcedureDatatable("SELECT_DATA_H_ORDER_PICKING", sqlParam);

            //if (dt.Rows.Count > 0) {
            //    sqlParam = new List<SqlParameter>();
            //    sqlParam.Add(new SqlParameter("@SALESORDERID", dt.Rows[0]["SALESORDERID"].ToString()));
            //    sqlParam.Add(new SqlParameter("@STATUS", Status.RELEASE));

            //    if (conn.callProcedure("UPDATE_STATUS_H_SALES_ORDER", sqlParam)) {

            //        sqlParam = new List<SqlParameter>();
            //        sqlParam.Add(new SqlParameter("@QUOTATIONID", this.QUOTATIONID));
            //        sqlParam.Add(new SqlParameter("@STATUS", Status.OPEN));
            //        if (conn.callProcedure("UPDATE_STATUS_H_ORDER_PICKING", sqlParam)) {
            //            sqlParam = new List<SqlParameter>();
            //            sqlParam.Add(new SqlParameter("@QUOTATIONID", this.QUOTATIONID));
            //            if (conn.callProcedure("INSERT_LOG_TRANSACTION_ORDER_PICKING", sqlParam)) {
            //                MessageBox.Show("SUCCESS INSERT");
            //            }
            //        }
            //    }
            //}
        }
    }
}
