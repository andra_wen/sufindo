﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace Sufindo.Report.ListInvoiceTotal
{
    public partial class ListInvoiceTotalFormCR : Form
    {
        Connection conn;
        public ListInvoiceTotalFormCR()
        {
            InitializeComponent();
        }

        public ListInvoiceTotalFormCR(String FROMDATE, String TODATE)
        {
            InitializeComponent();

            ListInvoiceTotalDS listinvoicetotalds = new ListInvoiceTotalDS();
            DataSet ds = listinvoicetotalds;

            conn = new Connection();

            List<SqlParameter> sqlParam = new List<SqlParameter>();
            if (FROMDATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", FROMDATE));
            }

            if (TODATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@TODATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@TODATE", TODATE));
            }

            conn.fillDataSetQuery(ref ds, "SELECT_DATA_INVOICE", sqlParam, "ListInvoiceTotalDataTable");

            ListInvoiceTotalCR listinvoicetotalCR = new ListInvoiceTotalCR();
            FROMDATE = FROMDATE.Equals("") ? "" : DateTime.Parse(FROMDATE).ToString("dd/MM/yyyy");
            TODATE = TODATE.Equals("") ? "" : DateTime.Parse(TODATE).ToString("dd/MM/yyyy");
            Object parameter = (FROMDATE.Equals("") || TODATE.Equals("")) ? "-" : String.Format("{0}-{1}", FROMDATE, TODATE);

            listinvoicetotalCR.SetDataSource(ds);
            listinvoicetotalCR.SetParameterValue(0, parameter);

            crystalReportViewer.ReportSource = listinvoicetotalCR;
            crystalReportViewer.Refresh();
        }
    }
}
