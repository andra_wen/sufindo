﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace Sufindo.Report.OrderPickingWithInvoice
{
    public partial class OrderPickingWithInvoiceFormCR : Form
    {
        Connection conn;
        public OrderPickingWithInvoiceFormCR()
        {
            InitializeComponent();
        }

        public OrderPickingWithInvoiceFormCR(String FROMDATE, String TODATE, String INVOICEID)
        {
            InitializeComponent();

            OrderPickingWithInvoiceDS orderpickingwithinvoiceds = new OrderPickingWithInvoiceDS();
            DataSet ds = orderpickingwithinvoiceds;

            conn = new Connection();

            List<SqlParameter> sqlParam = new List<SqlParameter>();
            if (FROMDATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", DBNull.Value));
            }
            else {
                sqlParam.Add(new SqlParameter("@STARTDATE", FROMDATE));
            }

            if (TODATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@TODATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@TODATE", TODATE));
            }

            //if (ORDERPICKINGID.Equals(""))
            //{
            //    sqlParam.Add(new SqlParameter("@ORDERPICKINGID", DBNull.Value));
            //}
            //else
            //{
            //    sqlParam.Add(new SqlParameter("@SPOILID", ORDERPICKINGID));
            //}

            if (INVOICEID.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@INVOICEID", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@INVOICEID", INVOICEID));
            }

            conn.fillDataSetQuery(ref ds, "SELECT_DATA_ORDERPICKINGINVOICE", sqlParam, "OrderPickingWithInvoiceDataTable");

            OrderPickingWithInvoiceCR orderpickingwithinvoicecr = new OrderPickingWithInvoiceCR();
            FROMDATE = FROMDATE.Equals("") ? "" : DateTime.Parse(FROMDATE).ToString("dd/MM/yyyy");
            TODATE = TODATE.Equals("") ? "" : DateTime.Parse(TODATE).ToString("dd/MM/yyyy");
            Object parameter = (FROMDATE.Equals("") || TODATE.Equals("")) ? "-" : String.Format("{0}-{1}", FROMDATE, TODATE);

            orderpickingwithinvoicecr.SetDataSource(ds);
            orderpickingwithinvoicecr.SetParameterValue(0, parameter);

            crystalReportViewer.ReportSource = orderpickingwithinvoicecr;
            crystalReportViewer.Refresh();
        }
    }
}
