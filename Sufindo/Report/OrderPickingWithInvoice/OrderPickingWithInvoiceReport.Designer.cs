﻿namespace Sufindo.Report.OrderPickingWithInvoice
{
    partial class OrderPickingWithInvoiceReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.InvoiceNotextBox = new System.Windows.Forms.TextBox();
            this.searchInvoiceNobutton = new System.Windows.Forms.Button();
            this.checkBoxDate = new System.Windows.Forms.CheckBox();
            this.labelFromDate = new System.Windows.Forms.Label();
            this.dateTimePickerStart = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.dateTimePickerEnd = new System.Windows.Forms.DateTimePicker();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.InvoiceNotextBox, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.searchInvoiceNobutton, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxDate, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelFromDate, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.dateTimePickerStart, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.dateTimePickerEnd, 5, 1);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(1, 1);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(450, 66);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label1, 2);
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 20);
            this.label1.TabIndex = 8;
            this.label1.Text = "Invoice No";
            // 
            // InvoiceNotextBox
            // 
            this.InvoiceNotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel1.SetColumnSpan(this.InvoiceNotextBox, 3);
            this.InvoiceNotextBox.Location = new System.Drawing.Point(115, 3);
            this.InvoiceNotextBox.Name = "InvoiceNotextBox";
            this.InvoiceNotextBox.Size = new System.Drawing.Size(183, 26);
            this.InvoiceNotextBox.TabIndex = 18;
            this.InvoiceNotextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.InvoiceNotextBox_KeyDown);
            this.InvoiceNotextBox.Leave += new System.EventHandler(this.InvoiceNotextBox_Leave);
            // 
            // searchInvoiceNobutton
            // 
            this.searchInvoiceNobutton.BackColor = System.Drawing.Color.Transparent;
            this.searchInvoiceNobutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchInvoiceNobutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchInvoiceNobutton.FlatAppearance.BorderSize = 0;
            this.searchInvoiceNobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchInvoiceNobutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchInvoiceNobutton.Location = new System.Drawing.Point(309, 1);
            this.searchInvoiceNobutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.searchInvoiceNobutton.Name = "searchInvoiceNobutton";
            this.searchInvoiceNobutton.Size = new System.Drawing.Size(24, 24);
            this.searchInvoiceNobutton.TabIndex = 20;
            this.searchInvoiceNobutton.UseVisualStyleBackColor = false;
            this.searchInvoiceNobutton.Click += new System.EventHandler(this.searchInvoiceNobutton_Click);
            // 
            // checkBoxDate
            // 
            this.checkBoxDate.AutoSize = true;
            this.checkBoxDate.Location = new System.Drawing.Point(3, 42);
            this.checkBoxDate.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.checkBoxDate.Name = "checkBoxDate";
            this.checkBoxDate.Size = new System.Drawing.Size(15, 14);
            this.checkBoxDate.TabIndex = 12;
            this.checkBoxDate.UseVisualStyleBackColor = true;
            this.checkBoxDate.CheckedChanged += new System.EventHandler(this.checkBoxDate_CheckedChanged);
            // 
            // labelFromDate
            // 
            this.labelFromDate.AutoSize = true;
            this.labelFromDate.Location = new System.Drawing.Point(24, 38);
            this.labelFromDate.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.labelFromDate.Name = "labelFromDate";
            this.labelFromDate.Size = new System.Drawing.Size(85, 20);
            this.labelFromDate.TabIndex = 13;
            this.labelFromDate.Text = "From Date";
            this.labelFromDate.Click += new System.EventHandler(this.labelFromDate_Click);
            // 
            // dateTimePickerStart
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.dateTimePickerStart, 2);
            this.dateTimePickerStart.CustomFormat = "dd/MM/yyyy";
            this.dateTimePickerStart.Enabled = false;
            this.dateTimePickerStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerStart.Location = new System.Drawing.Point(115, 35);
            this.dateTimePickerStart.Name = "dateTimePickerStart";
            this.dateTimePickerStart.Size = new System.Drawing.Size(116, 26);
            this.dateTimePickerStart.TabIndex = 15;
            this.dateTimePickerStart.ValueChanged += new System.EventHandler(this.dateTimePickerStart_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(237, 38);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 20);
            this.label4.TabIndex = 14;
            this.label4.Text = "To Date";
            // 
            // dateTimePickerEnd
            // 
            this.dateTimePickerEnd.CustomFormat = "dd/MM/yyyy";
            this.dateTimePickerEnd.Enabled = false;
            this.dateTimePickerEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerEnd.Location = new System.Drawing.Point(309, 35);
            this.dateTimePickerEnd.Name = "dateTimePickerEnd";
            this.dateTimePickerEnd.Size = new System.Drawing.Size(108, 26);
            this.dateTimePickerEnd.TabIndex = 16;
            // 
            // OrderPickingWithInvoiceReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 68);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OrderPickingWithInvoiceReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Order Picking With Invoice Report";
            this.Activated += new System.EventHandler(this.OrderPickingReport_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OrderPickingReport_FormClosed);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBoxDate;
        private System.Windows.Forms.Label labelFromDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dateTimePickerStart;
        private System.Windows.Forms.DateTimePicker dateTimePickerEnd;
        private System.Windows.Forms.TextBox InvoiceNotextBox;
        private System.Windows.Forms.Button searchInvoiceNobutton;
    }
}