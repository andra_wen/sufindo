﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Sufindo.Report.OrderPickingWithInvoice
{
    public partial class OrderPickingWithInvoiceReport : Form
    {
        private delegate void fillAutoCompleteTextBox();
        private Itemdatagridview itemdatagridview;
        private OrderPickingdatagridview orderpickingdatagridview;
        private Invoicedatagridview invoicedatagridview;
        private Connection connection;

        private MenuStrip menustripAction;
        private List<String> OrderPickingNo;
        private List<String> InvoiceNo;

        public OrderPickingWithInvoiceReport()
        {
            InitializeComponent();
        }

        public OrderPickingWithInvoiceReport(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            this.menustripAction = menustripAction;
            if (connection == null)
            {
                connection = new Connection();
            }
            invoicedatagridview = null;
            orderpickingdatagridview = null;
            itemdatagridview = null;
        }

        private void dateTimePickerStart_ValueChanged(object sender, EventArgs e)
        {

        }
        private void labelFromDate_Click(object sender, EventArgs e)
        {
            checkBoxDate.Checked = !checkBoxDate.Checked;
        }

        private void checkBoxDate_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePickerEnd.Enabled = checkBoxDate.Checked;
            dateTimePickerStart.Enabled = checkBoxDate.Checked;
        }

        private void searchInvoiceNobutton_Click(object sender, EventArgs e)
        {
            if (invoicedatagridview == null)
            {
                invoicedatagridview = new Invoicedatagridview("OrderPickingReport");
                invoicedatagridview.invoicePassingData = new Invoicedatagridview.InvoicePassingData(InvoicePassingData);
                invoicedatagridview.ShowDialog();
            }
            else if (invoicedatagridview.IsDisposed)
            {
                invoicedatagridview = new Invoicedatagridview("OrderPickingReport");
                invoicedatagridview.invoicePassingData = new Invoicedatagridview.InvoicePassingData(InvoicePassingData);
                invoicedatagridview.ShowDialog();
            }
        }
        private void InvoicePassingData(DataTable sender)
        {
            InvoiceNotextBox.Text = sender.Rows[0]["INVOICEID"].ToString();
        }
        private void OrderPickingReport_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }
        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (InvoiceNo == null)
            {
                InvoiceNo = new List<string>();
            }
            else
            {
                InvoiceNo.Clear();
            }
            SqlDataReader sqldataReader = connection.sqlDataReaders(String.Format("SELECT INVOICEID FROM H_INVOICE"));

            while (sqldataReader.Read())
            {
                InvoiceNo.Add(sqldataReader.GetString(0));
            }

            if (IsHandleCreated) BeginInvoke(new fillAutoCompleteTextBox(this.fillAutoComplete));
        }

        private void fillAutoComplete()
        {
            InvoiceNotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            InvoiceNotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            InvoiceNotextBox.AutoCompleteCustomSource.Clear();
            InvoiceNotextBox.AutoCompleteCustomSource.AddRange(InvoiceNo.ToArray());
            InvoiceNotextBox.Text = "";
        }

        public void print()
        {
            String fromDate = "";
            String toDate = "";

            if (checkBoxDate.Checked)
            {
                fromDate = dateTimePickerStart.Value.ToString("yyyy-MM-dd");
                toDate = dateTimePickerEnd.Value.ToString("yyyy-MM-dd");
            }
            OrderPickingWithInvoiceFormCR orderpickingwithinvoiceformcr = new OrderPickingWithInvoiceFormCR(fromDate, toDate, InvoiceNotextBox.Text);
            orderpickingwithinvoiceformcr.ShowDialog();
        }

        private void OrderPickingReport_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            Main.ACTIVEFORM = this;
            reloadAllData();
        }

        private void InvoiceNotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in InvoiceNo)
                {
                    if (item.ToUpper().Equals(InvoiceNotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        InvoiceNotextBox.Text = InvoiceNotextBox.Text.ToUpper();
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    InvoiceNotextBox.Text = "";
                }
            }
        }

        private void InvoiceNotextBox_Leave(object sender, EventArgs e)
        {
            Boolean isExistingPartNo = false;
            foreach (String item in InvoiceNo)
            {
                if (item.ToUpper().Equals(InvoiceNotextBox.Text.ToUpper()))
                {
                    isExistingPartNo = true;
                    InvoiceNotextBox.Text = InvoiceNotextBox.Text.ToUpper();
                    break;
                }
            }
            if (!isExistingPartNo)
            {
                InvoiceNotextBox.Text = "";
            }
        }

        
    }
}
