﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace Sufindo.Report.SalesReturn
{
    public partial class SalesReturnFormCR : Form
    {
        Connection conn;
        public SalesReturnFormCR()
        {
            InitializeComponent();
        }

        public SalesReturnFormCR(String FROMDATE, String TODATE, String SALESRETURNID, String CUSTOMERID, Boolean groupby)
        {
            InitializeComponent();

            SalesReturnReportDS salesreturnds = new SalesReturnReportDS();
            DataSet ds = salesreturnds;

            conn = new Connection();

            List<SqlParameter> sqlParam = new List<SqlParameter>();
            if (FROMDATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", FROMDATE));
            }

            if (TODATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@TODATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@TODATE", TODATE));
            }

            if (SALESRETURNID.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@SALESRETURNID", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@SALESRETURNID", SALESRETURNID));
            }

            if (CUSTOMERID.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@CUSTOMER", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@CUSTOMER", CUSTOMERID));
            }

            if (groupby)
            {
                conn.fillDataSetQuery(ref ds, "SELECT_DATA_SALESRETURN", sqlParam, "SalesReturnDataTable");
                SalesReturnPerTransactionCR salesreturnreportpertransactioncr = new SalesReturnPerTransactionCR();
                FROMDATE = FROMDATE.Equals("") ? "" : DateTime.Parse(FROMDATE).ToString("dd/MM/yyyy");
                TODATE = TODATE.Equals("") ? "" : DateTime.Parse(TODATE).ToString("dd/MM/yyyy");
                Object parameter = (FROMDATE.Equals("") || TODATE.Equals("")) ? "-" : String.Format("{0}-{1}", FROMDATE, TODATE);

                salesreturnreportpertransactioncr.SetDataSource(ds);
                salesreturnreportpertransactioncr.SetParameterValue(0, parameter);

                crystalReportViewer.ReportSource = salesreturnreportpertransactioncr;
            }
            else {
                conn.fillDataSetQuery(ref ds, "SELECT_DATA_SALESRETURNTOTAL", sqlParam, "SalesReturnTotalDataTable");
                SalesReturnReportCR salesreturnreportcr = new SalesReturnReportCR();
                FROMDATE = FROMDATE.Equals("") ? "" : DateTime.Parse(FROMDATE).ToString("dd/MM/yyyy");
                TODATE = TODATE.Equals("") ? "" : DateTime.Parse(TODATE).ToString("dd/MM/yyyy");
                Object parameter = (FROMDATE.Equals("") || TODATE.Equals("")) ? "-" : String.Format("{0}-{1}", FROMDATE, TODATE);

                salesreturnreportcr.SetDataSource(ds);
                salesreturnreportcr.SetParameterValue(0, parameter);

                crystalReportViewer.ReportSource = salesreturnreportcr;
            }
            crystalReportViewer.Refresh();
        }
    }
}
