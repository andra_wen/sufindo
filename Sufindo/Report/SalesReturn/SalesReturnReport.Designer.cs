﻿namespace Sufindo.Report.SalesReturn
{
    partial class SalesReturnReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.searchCustomerIDbutton = new System.Windows.Forms.Button();
            this.CustomerIDtextBox = new System.Windows.Forms.TextBox();
            this.searchInvoiceIDbutton = new System.Windows.Forms.Button();
            this.checkBoxDate = new System.Windows.Forms.CheckBox();
            this.labelFromDate = new System.Windows.Forms.Label();
            this.dateTimePickerStart = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePickerEnd = new System.Windows.Forms.DateTimePicker();
            this.SalesReturnIDtextBox = new System.Windows.Forms.TextBox();
            this.checkBoxGroupByTransaction = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.searchCustomerIDbutton, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.CustomerIDtextBox, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.searchInvoiceIDbutton, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxDate, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelFromDate, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.dateTimePickerStart, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.dateTimePickerEnd, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.SalesReturnIDtextBox, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxGroupByTransaction, 0, 3);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 1);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(453, 139);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // searchCustomerIDbutton
            // 
            this.searchCustomerIDbutton.BackColor = System.Drawing.Color.Transparent;
            this.searchCustomerIDbutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchCustomerIDbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchCustomerIDbutton.FlatAppearance.BorderSize = 0;
            this.searchCustomerIDbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchCustomerIDbutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchCustomerIDbutton.Location = new System.Drawing.Point(310, 33);
            this.searchCustomerIDbutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.searchCustomerIDbutton.Name = "searchCustomerIDbutton";
            this.searchCustomerIDbutton.Size = new System.Drawing.Size(24, 24);
            this.searchCustomerIDbutton.TabIndex = 20;
            this.searchCustomerIDbutton.UseVisualStyleBackColor = false;
            this.searchCustomerIDbutton.Click += new System.EventHandler(this.searchCustomerIDbutton_Click);
            // 
            // CustomerIDtextBox
            // 
            this.CustomerIDtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel1.SetColumnSpan(this.CustomerIDtextBox, 2);
            this.CustomerIDtextBox.Location = new System.Drawing.Point(135, 35);
            this.CustomerIDtextBox.Name = "CustomerIDtextBox";
            this.CustomerIDtextBox.Size = new System.Drawing.Size(169, 26);
            this.CustomerIDtextBox.TabIndex = 19;
            // 
            // searchInvoiceIDbutton
            // 
            this.searchInvoiceIDbutton.BackColor = System.Drawing.Color.Transparent;
            this.searchInvoiceIDbutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchInvoiceIDbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchInvoiceIDbutton.FlatAppearance.BorderSize = 0;
            this.searchInvoiceIDbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchInvoiceIDbutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchInvoiceIDbutton.Location = new System.Drawing.Point(310, 1);
            this.searchInvoiceIDbutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.searchInvoiceIDbutton.Name = "searchInvoiceIDbutton";
            this.searchInvoiceIDbutton.Size = new System.Drawing.Size(24, 24);
            this.searchInvoiceIDbutton.TabIndex = 17;
            this.searchInvoiceIDbutton.UseVisualStyleBackColor = false;
            this.searchInvoiceIDbutton.Click += new System.EventHandler(this.searchInvoiceIDbutton_Click);
            // 
            // checkBoxDate
            // 
            this.checkBoxDate.AutoSize = true;
            this.checkBoxDate.Location = new System.Drawing.Point(3, 74);
            this.checkBoxDate.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.checkBoxDate.Name = "checkBoxDate";
            this.checkBoxDate.Size = new System.Drawing.Size(15, 14);
            this.checkBoxDate.TabIndex = 11;
            this.checkBoxDate.UseVisualStyleBackColor = true;
            this.checkBoxDate.CheckedChanged += new System.EventHandler(this.checkBoxDate_CheckedChanged);
            // 
            // labelFromDate
            // 
            this.labelFromDate.AutoSize = true;
            this.labelFromDate.Location = new System.Drawing.Point(24, 70);
            this.labelFromDate.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.labelFromDate.Name = "labelFromDate";
            this.labelFromDate.Size = new System.Drawing.Size(85, 20);
            this.labelFromDate.TabIndex = 8;
            this.labelFromDate.Text = "From Date";
            this.labelFromDate.Click += new System.EventHandler(this.labelFromDate_Click);
            // 
            // dateTimePickerStart
            // 
            this.dateTimePickerStart.CustomFormat = "dd/MM/yyyy";
            this.dateTimePickerStart.Enabled = false;
            this.dateTimePickerStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerStart.Location = new System.Drawing.Point(135, 67);
            this.dateTimePickerStart.Name = "dateTimePickerStart";
            this.dateTimePickerStart.Size = new System.Drawing.Size(116, 26);
            this.dateTimePickerStart.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label4, 2);
            this.label4.Location = new System.Drawing.Point(257, 70);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 20);
            this.label4.TabIndex = 10;
            this.label4.Text = "To Date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label2, 2);
            this.label2.Location = new System.Drawing.Point(3, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 20);
            this.label2.TabIndex = 18;
            this.label2.Text = "Customer ID";
            // 
            // dateTimePickerEnd
            // 
            this.dateTimePickerEnd.CustomFormat = "dd/MM/yyyy";
            this.dateTimePickerEnd.Enabled = false;
            this.dateTimePickerEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerEnd.Location = new System.Drawing.Point(340, 67);
            this.dateTimePickerEnd.Name = "dateTimePickerEnd";
            this.dateTimePickerEnd.Size = new System.Drawing.Size(108, 26);
            this.dateTimePickerEnd.TabIndex = 4;
            // 
            // SalesReturnIDtextBox
            // 
            this.SalesReturnIDtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel1.SetColumnSpan(this.SalesReturnIDtextBox, 2);
            this.SalesReturnIDtextBox.Location = new System.Drawing.Point(135, 3);
            this.SalesReturnIDtextBox.Name = "SalesReturnIDtextBox";
            this.SalesReturnIDtextBox.Size = new System.Drawing.Size(169, 26);
            this.SalesReturnIDtextBox.TabIndex = 2;
            this.SalesReturnIDtextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.InvoiceIDtextBox_KeyDown);
            this.SalesReturnIDtextBox.Leave += new System.EventHandler(this.InvoiceIDtextBox_Leave);
            // 
            // checkBoxGroupByTransaction
            // 
            this.checkBoxGroupByTransaction.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.checkBoxGroupByTransaction, 3);
            this.checkBoxGroupByTransaction.Location = new System.Drawing.Point(3, 99);
            this.checkBoxGroupByTransaction.Name = "checkBoxGroupByTransaction";
            this.checkBoxGroupByTransaction.Size = new System.Drawing.Size(182, 24);
            this.checkBoxGroupByTransaction.TabIndex = 21;
            this.checkBoxGroupByTransaction.Text = "Group By Transaction";
            this.checkBoxGroupByTransaction.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label1, 2);
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "Sales Return No";
            // 
            // SalesReturnReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 140);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SalesReturnReport";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "List Sales Return Report";
            this.Activated += new System.EventHandler(this.ListInvoiceReport_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ListInvoiceReport_FormClosed);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label labelFromDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerStart;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dateTimePickerEnd;
        private System.Windows.Forms.TextBox SalesReturnIDtextBox;
        private System.Windows.Forms.CheckBox checkBoxDate;
        private System.Windows.Forms.Button searchInvoiceIDbutton;
        private System.Windows.Forms.Button searchCustomerIDbutton;
        private System.Windows.Forms.TextBox CustomerIDtextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBoxGroupByTransaction;
        private System.Windows.Forms.Label label1;
    }
}