﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Sufindo.Report.ListAdjustment
{
    public partial class ListAdjustmentReport : Form
    {
        private delegate void fillAutoCompleteTextBox();

        private Itemdatagridview itemdatagridview;
        private Supplierdatagridview supplierdatagridview;
        private AdjustSpoildatagridview adjustmenDatagridView;
        private Connection connection;

        private MenuStrip menustripAction;
        private List<String> PartNo;
        private List<String> adjustmentID;

        public ListAdjustmentReport()
        {
            InitializeComponent();
            if (connection == null) {
                connection = new Connection();
            }
        }

        public ListAdjustmentReport(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            this.menustripAction = menustripAction;
            if (connection == null)
            {
                connection = new Connection();
            }
        }

        private void checkBoxDate_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePickerEnd.Enabled = checkBoxDate.Checked;
            dateTimePickerStart.Enabled = checkBoxDate.Checked;
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }
        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e){

        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e) {
            if (adjustmentID == null)
            {
                adjustmentID = new List<string>();
            }
            else
            {
                adjustmentID.Clear();
            }
            SqlDataReader sqldataReader = connection.sqlDataReaders(String.Format("SELECT ADJUSTMENTID FROM H_ADJUSTMENT_ITEM WHERE [STATUS] = '{0}'", Status.CONFIRM));

            while (sqldataReader.Read())
            {
                adjustmentID.Add(sqldataReader.GetString(0));
            }

            if (PartNo == null)
            {
                PartNo = new List<string>();
            }
            else
            {
                PartNo.Clear();
            }
            String query = String.Format("SELECT ITEMID FROM M_ITEM");
            sqldataReader = connection.sqlDataReaders(query);

            while (sqldataReader.Read())
            {
                PartNo.Add(sqldataReader.GetString(0));
            }

            if (IsHandleCreated) BeginInvoke(new fillAutoCompleteTextBox(this.fillAutoComplete));
        }

        private void fillAutoComplete()
        {
            AdjustmentIDtextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            AdjustmentIDtextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            AdjustmentIDtextBox.AutoCompleteCustomSource.AddRange(adjustmentID.ToArray());
            AdjustmentIDtextBox.Text = "";

            PartNotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            PartNotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            PartNotextBox.AutoCompleteCustomSource.Clear();
            PartNotextBox.AutoCompleteCustomSource.AddRange(PartNo.ToArray());
        }

        public void print() {
            String fromDate = "";
            String toDate = "";

            if (checkBoxDate.Checked) {
                fromDate = dateTimePickerStart.Value.ToString("yyyy-MM-dd");
                toDate = dateTimePickerEnd.Value.ToString("yyyy-MM-dd");
            }
            ListAdjustmentFormCR listadjustmentformCR = new ListAdjustmentFormCR(fromDate, toDate, AdjustmentIDtextBox.Text, PartNotextBox.Text);
            listadjustmentformCR.ShowDialog();
        }

        private void searchPartNobutton_Click(object sender, EventArgs e)
        {
            if (itemdatagridview == null)
            {
                itemdatagridview = new Itemdatagridview("MASTERITEM");
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
            else if (itemdatagridview.IsDisposed)
            {
                itemdatagridview = new Itemdatagridview("MASTERITEM");
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
        }

        private void MasterItemPassingData(DataTable sender) {
            PartNotextBox.Text = sender.Rows[0]["ITEMID"].ToString();
        }

        private void searchAdjustmentIDbutton_Click(object sender, EventArgs e)
        {

            if (adjustmenDatagridView == null)
            {
                adjustmenDatagridView = new AdjustSpoildatagridview("Adjustment");
                adjustmenDatagridView.Text = "Adjustment";
                adjustmenDatagridView.adjustmentSpoilPassingData = new AdjustSpoildatagridview.AdjustmentSpoilPassingData(AdjustSpoilPassingData);
                adjustmenDatagridView.ShowDialog();
            }
            else if (adjustmenDatagridView.IsDisposed)
            {
                adjustmenDatagridView = new AdjustSpoildatagridview("Adjustment");
                adjustmenDatagridView.Text = "Adjustment";
                adjustmenDatagridView.adjustmentSpoilPassingData = new AdjustSpoildatagridview.AdjustmentSpoilPassingData(AdjustSpoilPassingData);
                adjustmenDatagridView.ShowDialog();
            }
        }
        private void AdjustSpoilPassingData(DataTable sender)
        {
            if (sender.Rows.Count == 1)
            {
                AdjustmentIDtextBox.Text = sender.Rows[0]["ADJUSTMENTID"].ToString();
            }
        }

        private void labelFromDate_Click(object sender, EventArgs e)
        {
            checkBoxDate.Checked = !checkBoxDate.Checked;
        }

        private void ListAdjustmentReport_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            Main.ACTIVEFORM = this;
            reloadAllData();
        }

        private void PartNotextBox_Leave(object sender, EventArgs e)
        {
            Boolean isExistingPartNo = false;
            foreach (String item in PartNo)
            {
                if (item.ToUpper().Equals(PartNotextBox.Text.ToUpper()))
                {
                    isExistingPartNo = true;
                    PartNotextBox.Text = PartNotextBox.Text.ToUpper();
                    break;
                }
            }
            if (!isExistingPartNo)
            {
                PartNotextBox.Text = "";
            }
        }

        private void PartNotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in PartNo)
                {
                    if (item.ToUpper().Equals(PartNotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        PartNotextBox.Text = PartNotextBox.Text.ToUpper();
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    PartNotextBox.Text = "";
                }
            }
        }

        private void SupplierIDtextBox_Leave(object sender, EventArgs e)
        {
            Boolean isExistingPartNo = false;
            foreach (String item in adjustmentID)
            {
                if (item.ToUpper().Equals(AdjustmentIDtextBox.Text.ToUpper()))
                {
                    isExistingPartNo = true;
                    AdjustmentIDtextBox.Text = AdjustmentIDtextBox.Text.ToUpper();
                    break;
                }
            }
            if (!isExistingPartNo)
            {
                AdjustmentIDtextBox.Text = "";
            }
        }

        private void SupplierIDtextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in adjustmentID)
                {
                    if (item.ToUpper().Equals(AdjustmentIDtextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        AdjustmentIDtextBox.Text = AdjustmentIDtextBox.Text.ToUpper();
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    AdjustmentIDtextBox.Text = "";
                }
            }
        }

        private void ListAdjustmentReport_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }
    }
}
