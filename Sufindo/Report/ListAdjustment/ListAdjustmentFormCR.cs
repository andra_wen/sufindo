﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace Sufindo.Report.ListAdjustment
{
    public partial class ListAdjustmentFormCR : Form
    {
        Connection conn;
        public ListAdjustmentFormCR()
        {
            InitializeComponent();
        }

        public ListAdjustmentFormCR(String FROMDATE, String TODATE, String ADJUSTMENTID, String ITEMID)
        {
            InitializeComponent();

            ListAdjustmentDS listadjustmentds = new ListAdjustmentDS();
            DataSet ds = listadjustmentds;

            conn = new Connection();

            List<SqlParameter> sqlParam = new List<SqlParameter>();
            Console.WriteLine(FROMDATE);
            Console.WriteLine(TODATE);
            if (FROMDATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", DBNull.Value));
            }
            else {
                sqlParam.Add(new SqlParameter("@STARTDATE", FROMDATE));
            }

            if (TODATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@TODATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@TODATE", TODATE));
            }

            if (ADJUSTMENTID.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@ADJUSTMENTID", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@ADJUSTMENTID", ADJUSTMENTID));
            }

            if (ITEMID.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@ITEMID", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@ITEMID", ITEMID));
            }

            conn.fillDataSetQuery(ref ds, "SELECT_DATA_ADJUSTMENT", sqlParam, "ListAdjustmentDataTable");

            ListAdjustmentCR listadjustmentCR = new ListAdjustmentCR();
            FROMDATE = FROMDATE.Equals("") ? "" : DateTime.Parse(FROMDATE).ToString("dd/MM/yyyy");
            TODATE = TODATE.Equals("") ? "" : DateTime.Parse(TODATE).ToString("dd/MM/yyyy");
            Object parameter = (FROMDATE.Equals("") || TODATE.Equals("")) ? "-" : String.Format("{0}-{1}", FROMDATE, TODATE);

            listadjustmentCR.SetDataSource(ds);
            listadjustmentCR.SetParameterValue(0, parameter);
            crystalReportViewer.ReportSource = listadjustmentCR;
            crystalReportViewer.Refresh();
        }
    }
}
