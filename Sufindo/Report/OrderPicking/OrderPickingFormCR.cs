﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace Sufindo.Report.OrderPicking
{
    public partial class OrderPickingFormCR : Form
    {
        Connection conn;
        public OrderPickingFormCR()
        {
            InitializeComponent();
        }

        public OrderPickingFormCR(String FROMDATE, String TODATE, String SALESORDERID, Boolean open, Boolean cancel, Boolean closed, String ITEMID)
        {
            InitializeComponent();

            OrderPickingDS orderpickingds = new OrderPickingDS();
            DataSet ds = orderpickingds;

            conn = new Connection();

            List<SqlParameter> sqlParam = new List<SqlParameter>();
            if (FROMDATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", FROMDATE));
            }

            if (TODATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@TODATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@TODATE", TODATE));
            }

            if (SALESORDERID.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@SALESORDERID", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@SALESORDERID", SALESORDERID));
            }

            SqlParameter sqlopen = !open ? new SqlParameter("@STATUSOPEN", DBNull.Value) : new SqlParameter("@STATUSOPEN", @"Open");
            sqlParam.Add(sqlopen);

            SqlParameter sqlcancel = !cancel ? new SqlParameter("@STATUSCANCEL", DBNull.Value) : new SqlParameter("@STATUSCANCEL", @"Cancel");
            sqlParam.Add(sqlcancel);

            //SqlParameter sqlclose = !closed ? new SqlParameter("@STATUSCLOSED", DBNull.Value) : new SqlParameter("@STATUSCLOSED", @"Close");
            //sqlParam.Add(sqlclose);

            if (ITEMID.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@ITEMID", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@ITEMID", ITEMID));
            }

            conn.fillDataSetQuery(ref ds, "SELECT_DATA_ORDERPICKING", sqlParam, "OrderPickingDataTable");

            OrderPickingCR orderpickingcr = new OrderPickingCR();
            FROMDATE = FROMDATE.Equals("") ? "" : DateTime.Parse(FROMDATE).ToString("dd/MM/yyyy");
            TODATE = TODATE.Equals("") ? "" : DateTime.Parse(TODATE).ToString("dd/MM/yyyy");
            Object parameter = (FROMDATE.Equals("") || TODATE.Equals("")) ? "-" : String.Format("{0} - {1}", FROMDATE, TODATE);

            orderpickingcr.SetDataSource(ds);
            orderpickingcr.SetParameterValue(0, parameter);
            
            crystalReportViewer.ReportSource = orderpickingcr;
            crystalReportViewer.Refresh();
        }
    }
}
