﻿namespace Sufindo.Report.OrderPicking
{
    partial class OrderPickingReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SONotextBox = new System.Windows.Forms.TextBox();
            this.searchSONobutton = new System.Windows.Forms.Button();
            this.checkBoxDate = new System.Windows.Forms.CheckBox();
            this.labelFromDate = new System.Windows.Forms.Label();
            this.dateTimePickerStart = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.PartNotextBox = new System.Windows.Forms.TextBox();
            this.searchPartNobutton = new System.Windows.Forms.Button();
            this.checkBoxCancel = new System.Windows.Forms.CheckBox();
            this.checkBoxClose = new System.Windows.Forms.CheckBox();
            this.checkBoxOpen = new System.Windows.Forms.CheckBox();
            this.dateTimePickerEnd = new System.Windows.Forms.DateTimePicker();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.SONotextBox, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.searchSONobutton, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxDate, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.labelFromDate, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.dateTimePickerStart, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.label4, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.PartNotextBox, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.searchPartNobutton, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxOpen, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.dateTimePickerEnd, 5, 3);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxCancel, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxClose, 5, 2);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(1, 1);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(447, 133);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label5, 2);
            this.label5.Location = new System.Drawing.Point(3, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 20);
            this.label5.TabIndex = 24;
            this.label5.Text = "Part No";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label1, 2);
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 20);
            this.label1.TabIndex = 8;
            this.label1.Text = "Sales Order No";
            // 
            // SONotextBox
            // 
            this.SONotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel1.SetColumnSpan(this.SONotextBox, 3);
            this.SONotextBox.Location = new System.Drawing.Point(126, 3);
            this.SONotextBox.Name = "SONotextBox";
            this.SONotextBox.Size = new System.Drawing.Size(183, 26);
            this.SONotextBox.TabIndex = 18;
            // 
            // searchSONobutton
            // 
            this.searchSONobutton.BackColor = System.Drawing.Color.Transparent;
            this.searchSONobutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchSONobutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchSONobutton.FlatAppearance.BorderSize = 0;
            this.searchSONobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchSONobutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchSONobutton.Location = new System.Drawing.Point(331, 1);
            this.searchSONobutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.searchSONobutton.Name = "searchSONobutton";
            this.searchSONobutton.Size = new System.Drawing.Size(24, 24);
            this.searchSONobutton.TabIndex = 20;
            this.searchSONobutton.UseVisualStyleBackColor = false;
            this.searchSONobutton.Click += new System.EventHandler(this.searchSONobutton_Click);
            // 
            // checkBoxDate
            // 
            this.checkBoxDate.AutoSize = true;
            this.checkBoxDate.Location = new System.Drawing.Point(3, 104);
            this.checkBoxDate.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.checkBoxDate.Name = "checkBoxDate";
            this.checkBoxDate.Size = new System.Drawing.Size(15, 14);
            this.checkBoxDate.TabIndex = 12;
            this.checkBoxDate.UseVisualStyleBackColor = true;
            this.checkBoxDate.CheckedChanged += new System.EventHandler(this.checkBoxDate_CheckedChanged);
            // 
            // labelFromDate
            // 
            this.labelFromDate.AutoSize = true;
            this.labelFromDate.Location = new System.Drawing.Point(24, 100);
            this.labelFromDate.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.labelFromDate.Name = "labelFromDate";
            this.labelFromDate.Size = new System.Drawing.Size(85, 20);
            this.labelFromDate.TabIndex = 13;
            this.labelFromDate.Text = "From Date";
            this.labelFromDate.Click += new System.EventHandler(this.labelFromDate_Click);
            // 
            // dateTimePickerStart
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.dateTimePickerStart, 2);
            this.dateTimePickerStart.CustomFormat = "dd/MM/yyyy";
            this.dateTimePickerStart.Enabled = false;
            this.dateTimePickerStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerStart.Location = new System.Drawing.Point(126, 97);
            this.dateTimePickerStart.Name = "dateTimePickerStart";
            this.dateTimePickerStart.Size = new System.Drawing.Size(116, 26);
            this.dateTimePickerStart.TabIndex = 15;
            this.dateTimePickerStart.ValueChanged += new System.EventHandler(this.dateTimePickerStart_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(248, 100);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 20);
            this.label4.TabIndex = 14;
            this.label4.Text = "To Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label3, 3);
            this.label3.Location = new System.Drawing.Point(3, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(154, 20);
            this.label3.TabIndex = 9;
            this.label3.Text = "Order Picking Status";
            // 
            // PartNotextBox
            // 
            this.PartNotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel1.SetColumnSpan(this.PartNotextBox, 3);
            this.PartNotextBox.Location = new System.Drawing.Point(126, 35);
            this.PartNotextBox.Name = "PartNotextBox";
            this.PartNotextBox.Size = new System.Drawing.Size(183, 26);
            this.PartNotextBox.TabIndex = 25;
            // 
            // searchPartNobutton
            // 
            this.searchPartNobutton.BackColor = System.Drawing.Color.Transparent;
            this.searchPartNobutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchPartNobutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchPartNobutton.FlatAppearance.BorderSize = 0;
            this.searchPartNobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchPartNobutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchPartNobutton.Location = new System.Drawing.Point(331, 33);
            this.searchPartNobutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.searchPartNobutton.Name = "searchPartNobutton";
            this.searchPartNobutton.Size = new System.Drawing.Size(24, 24);
            this.searchPartNobutton.TabIndex = 26;
            this.searchPartNobutton.UseVisualStyleBackColor = false;
            this.searchPartNobutton.Click += new System.EventHandler(this.searchPartNobutton_Click);
            // 
            // checkBoxCancel
            // 
            this.checkBoxCancel.AutoSize = true;
            this.checkBoxCancel.Location = new System.Drawing.Point(248, 67);
            this.checkBoxCancel.Name = "checkBoxCancel";
            this.checkBoxCancel.Size = new System.Drawing.Size(77, 24);
            this.checkBoxCancel.TabIndex = 23;
            this.checkBoxCancel.Text = "Cancel";
            this.checkBoxCancel.UseVisualStyleBackColor = true;
            // 
            // checkBoxClose
            // 
            this.checkBoxClose.AutoSize = true;
            this.checkBoxClose.Location = new System.Drawing.Point(331, 67);
            this.checkBoxClose.Name = "checkBoxClose";
            this.checkBoxClose.Size = new System.Drawing.Size(68, 24);
            this.checkBoxClose.TabIndex = 22;
            this.checkBoxClose.Text = "Close";
            this.checkBoxClose.UseVisualStyleBackColor = true;
            this.checkBoxClose.Visible = false;
            // 
            // checkBoxOpen
            // 
            this.checkBoxOpen.AutoSize = true;
            this.checkBoxOpen.Location = new System.Drawing.Point(175, 67);
            this.checkBoxOpen.Name = "checkBoxOpen";
            this.checkBoxOpen.Size = new System.Drawing.Size(67, 24);
            this.checkBoxOpen.TabIndex = 21;
            this.checkBoxOpen.Text = "Open";
            this.checkBoxOpen.UseVisualStyleBackColor = true;
            // 
            // dateTimePickerEnd
            // 
            this.dateTimePickerEnd.CustomFormat = "dd/MM/yyyy";
            this.dateTimePickerEnd.Enabled = false;
            this.dateTimePickerEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerEnd.Location = new System.Drawing.Point(331, 97);
            this.dateTimePickerEnd.Name = "dateTimePickerEnd";
            this.dateTimePickerEnd.Size = new System.Drawing.Size(110, 26);
            this.dateTimePickerEnd.TabIndex = 16;
            // 
            // OrderPickingReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 136);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OrderPickingReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Order Picking Report";
            this.Activated += new System.EventHandler(this.OrderPickingReport_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OrderPickingReport_FormClosed);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBoxDate;
        private System.Windows.Forms.Label labelFromDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dateTimePickerStart;
        private System.Windows.Forms.DateTimePicker dateTimePickerEnd;
        private System.Windows.Forms.TextBox SONotextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkBoxOpen;
        private System.Windows.Forms.CheckBox checkBoxClose;
        private System.Windows.Forms.CheckBox checkBoxCancel;
        private System.Windows.Forms.Button searchSONobutton;
        private System.Windows.Forms.Button searchPartNobutton;
        private System.Windows.Forms.TextBox PartNotextBox;
    }
}