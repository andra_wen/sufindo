﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Sufindo.Report.OrderPicking
{
    public partial class OrderPickingReport : Form
    {
        private delegate void fillAutoCompleteTextBox();
        private Itemdatagridview itemdatagridview;
        private OrderPickingdatagridview orderpickingdatagridview;
        private SalesOrderdatagridview salesorderdatagridview;
        private Connection connection;

        private MenuStrip menustripAction;
        private List<String> PartNo;
        private List<String> OrderPickingNo;
        private List<String> SalesOrderNo;

        public OrderPickingReport()
        {
            InitializeComponent();
        }

        public OrderPickingReport(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            this.menustripAction = menustripAction;
            if (connection == null)
            {
                connection = new Connection();
            }
            salesorderdatagridview = null;
            orderpickingdatagridview = null;
            itemdatagridview = null;
        }

        private void dateTimePickerStart_ValueChanged(object sender, EventArgs e)
        {

        }

        private void searchPartNobutton_Click(object sender, EventArgs e)
        {
            if (itemdatagridview == null)
            {
                itemdatagridview = new Itemdatagridview("MASTERITEM");
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
            else if (itemdatagridview.IsDisposed)
            {
                itemdatagridview = new Itemdatagridview("MASTERITEM");
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
        }

        private void MasterItemPassingData(DataTable sender)
        {
            PartNotextBox.Text = sender.Rows[0]["ITEMID"].ToString();
        }

        private void labelFromDate_Click(object sender, EventArgs e)
        {
            checkBoxDate.Checked = !checkBoxDate.Checked;
        }

        private void checkBoxDate_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePickerEnd.Enabled = checkBoxDate.Checked;
            dateTimePickerStart.Enabled = checkBoxDate.Checked;
        }

        private void searchSONobutton_Click(object sender, EventArgs e)
        {
            if (salesorderdatagridview == null)
            {
                salesorderdatagridview = new SalesOrderdatagridview("OrderPickingReport");
                salesorderdatagridview.salesOrderPassingData = new SalesOrderdatagridview.SalesOrderPassingData(SalesOrderPassingData);
                salesorderdatagridview.ShowDialog();
            }
            else if (salesorderdatagridview.IsDisposed)
            {
                salesorderdatagridview = new SalesOrderdatagridview("OrderPickingReport");
                salesorderdatagridview.salesOrderPassingData = new SalesOrderdatagridview.SalesOrderPassingData(SalesOrderPassingData);
                salesorderdatagridview.ShowDialog();
            }
        }
        private void SalesOrderPassingData(DataTable sender)
        {
            SONotextBox.Text = sender.Rows[0]["SALESORDERID"].ToString();
        }


        private void OrderPickingReport_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }
        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (OrderPickingNo == null)
            {
                OrderPickingNo = new List<string>();
            }
            else
            {
                OrderPickingNo.Clear();
            }
            SqlDataReader sqldataReader = connection.sqlDataReaders(String.Format("SELECT ORDERPICKINGID FROM H_ORDER_PICKING"));

            while (sqldataReader.Read())
            {
                OrderPickingNo.Add(sqldataReader.GetString(0));
            }

            if (SalesOrderNo == null)
            {
                SalesOrderNo = new List<string>();
            }
            else
            {
                SalesOrderNo.Clear();
            }
            sqldataReader = connection.sqlDataReaders(String.Format("SELECT SALESORDERID FROM H_SALES_ORDER WHERE STATUS != 'Deleted'"));

            while (sqldataReader.Read())
            {
                SalesOrderNo.Add(sqldataReader.GetString(0));
            }

            if (PartNo == null)
            {
                PartNo = new List<string>();
            }
            else
            {
                PartNo.Clear();
            }
            String query = String.Format("SELECT ITEMID FROM M_ITEM");
            sqldataReader = connection.sqlDataReaders(query);

            while (sqldataReader.Read())
            {
                PartNo.Add(sqldataReader.GetString(0));
            }

            if (IsHandleCreated) BeginInvoke(new fillAutoCompleteTextBox(this.fillAutoComplete));
        }

        private void fillAutoComplete()
        {
            SONotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            SONotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            SONotextBox.AutoCompleteCustomSource.Clear();
            SONotextBox.AutoCompleteCustomSource.AddRange(SalesOrderNo.ToArray());
            SONotextBox.Text = "";

            PartNotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            PartNotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            PartNotextBox.AutoCompleteCustomSource.Clear();
            PartNotextBox.AutoCompleteCustomSource.AddRange(PartNo.ToArray());
            PartNotextBox.Text = "";
        }

        public void print()
        {
            String fromDate = "";
            String toDate = "";

            if (checkBoxDate.Checked)
            {
                fromDate = dateTimePickerStart.Value.ToString("yyyy-MM-dd");
                toDate = dateTimePickerEnd.Value.ToString("yyyy-MM-dd");
            }
            OrderPickingFormCR orderpickingformcr = new OrderPickingFormCR(fromDate, toDate, SONotextBox.Text, checkBoxOpen.Checked, checkBoxCancel.Checked, checkBoxClose.Checked, PartNotextBox.Text);
            orderpickingformcr.ShowDialog();
        }

        private void OrderPickingReport_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            Main.ACTIVEFORM = this;
            reloadAllData();
        }

        
    }
}
