﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace Sufindo.Report.ListSpoil
{
    public partial class ListSpoilFormCR : Form
    {
        Connection conn;
        public ListSpoilFormCR()
        {
            InitializeComponent();
        }

        public ListSpoilFormCR(String FROMDATE, String TODATE, String SPOILID, String ITEMID)
        {
            InitializeComponent();

            ListSpoilDS listadjustmentds = new ListSpoilDS();
            DataSet ds = listadjustmentds;

            conn = new Connection();

            List<SqlParameter> sqlParam = new List<SqlParameter>();
            if (FROMDATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", DBNull.Value));
            }
            else {
                sqlParam.Add(new SqlParameter("@STARTDATE", FROMDATE));
            }

            if (TODATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@TODATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@TODATE", TODATE));
            }

            if (SPOILID.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@SPOILID", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@SPOILID", SPOILID));
            }

            if (ITEMID.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@ITEMID", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@ITEMID", ITEMID));
            }

            conn.fillDataSetQuery(ref ds, "SELECT_DATA_SPOIL", sqlParam, "ListSpoilDataTable");

            ListSpoilCR listspoilCR = new ListSpoilCR();
            FROMDATE = FROMDATE.Equals("") ? "" : DateTime.Parse(FROMDATE).ToString("dd/MM/yyyy");
            TODATE = TODATE.Equals("") ? "" : DateTime.Parse(TODATE).ToString("dd/MM/yyyy");
            Object parameter = (FROMDATE.Equals("") || TODATE.Equals("")) ? "-" : String.Format("{0}-{1}", FROMDATE, TODATE);

            listspoilCR.SetDataSource(ds);
            listspoilCR.SetParameterValue(0, parameter);

            crystalReportViewer.ReportSource = listspoilCR;
            crystalReportViewer.Refresh();
        }
    }
}
