﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Sufindo.Report.ListSpoil
{
    public partial class ListSpoilReport : Form
    {
        private delegate void fillAutoCompleteTextBox();

        private Itemdatagridview itemdatagridview;
        private AdjustSpoildatagridview spoilDatagridview;
        private Connection connection;

        private MenuStrip menustripAction;
        private List<String> PartNo;
        private List<String> spoilID;

        public ListSpoilReport()
        {
            InitializeComponent();
            if (connection == null) {
                connection = new Connection();
            }
        }

        public ListSpoilReport(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            this.menustripAction = menustripAction;
            if (connection == null)
            {
                connection = new Connection();
            }
        }

        private void checkBoxDate_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePickerEnd.Enabled = checkBoxDate.Checked;
            dateTimePickerStart.Enabled = checkBoxDate.Checked;
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }
        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e){

        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e) {
            if (spoilID == null)
            {
                spoilID = new List<string>();
            }
            else
            {
                spoilID.Clear();
            }
            SqlDataReader sqldataReader = connection.sqlDataReaders(String.Format("SELECT SPOILID FROM H_SPOIL_ITEM WHERE [STATUS] = '{0}'", Status.CONFIRM));

            while (sqldataReader.Read())
            {
                spoilID.Add(sqldataReader.GetString(0));
            }

            if (PartNo == null)
            {
                PartNo = new List<string>();
            }
            else
            {
                PartNo.Clear();
            }
            String query = String.Format("SELECT ITEMID FROM M_ITEM");
            sqldataReader = connection.sqlDataReaders(query);

            while (sqldataReader.Read())
            {
                PartNo.Add(sqldataReader.GetString(0));
            }

            if (IsHandleCreated) BeginInvoke(new fillAutoCompleteTextBox(this.fillAutoComplete));
        }

        private void fillAutoComplete()
        {
            SpoilIDtextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            SpoilIDtextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            SpoilIDtextBox.AutoCompleteCustomSource.AddRange(spoilID.ToArray());
            SpoilIDtextBox.Text = "";

            PartNotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            PartNotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            PartNotextBox.AutoCompleteCustomSource.Clear();
            PartNotextBox.AutoCompleteCustomSource.AddRange(PartNo.ToArray());
        }

        public void print() {
            String fromDate = "";
            String toDate = "";

            if (checkBoxDate.Checked) {
                fromDate = dateTimePickerStart.Value.ToString("yyyy-MM-dd");
                toDate = dateTimePickerEnd.Value.ToString("yyyy-MM-dd");
            }

            ListSpoilFormCR listspoilformCR = new ListSpoilFormCR(fromDate, toDate, SpoilIDtextBox.Text, PartNotextBox.Text);
            listspoilformCR.ShowDialog();
        }

        private void searchPartNobutton_Click(object sender, EventArgs e)
        {
            if (itemdatagridview == null)
            {
                itemdatagridview = new Itemdatagridview("MASTERITEM");
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
            else if (itemdatagridview.IsDisposed)
            {
                itemdatagridview = new Itemdatagridview("MASTERITEM");
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
        }

        private void MasterItemPassingData(DataTable sender) {
            PartNotextBox.Text = sender.Rows[0]["ITEMID"].ToString();
        }

        private void searchSpoilIDbutton_Click(object sender, EventArgs e)
        {
            if (spoilDatagridview == null)
            {
                spoilDatagridview = new AdjustSpoildatagridview("Spoil");
                spoilDatagridview.Text = "Spoil";
                spoilDatagridview.adjustmentSpoilPassingData = new AdjustSpoildatagridview.AdjustmentSpoilPassingData(AdjustSpoilPassingData);
                spoilDatagridview.ShowDialog();
            }
            else if (spoilDatagridview.IsDisposed)
            {
                spoilDatagridview = new AdjustSpoildatagridview("Spoil");
                spoilDatagridview.Text = "Spoil";
                spoilDatagridview.adjustmentSpoilPassingData = new AdjustSpoildatagridview.AdjustmentSpoilPassingData(AdjustSpoilPassingData);
                spoilDatagridview.ShowDialog();
            }
        }

        private void AdjustSpoilPassingData(DataTable sender)
        {
            if (sender.Rows.Count > 0)
            {
                SpoilIDtextBox.Text = sender.Rows[0]["SPOILID"].ToString();
            }
        }

        private void labelFromDate_Click(object sender, EventArgs e)
        {
            checkBoxDate.Checked = !checkBoxDate.Checked;
        }

        private void InventoryAuditReport_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            Main.ACTIVEFORM = this;
            reloadAllData();
        }

        private void PartNotextBox_Leave(object sender, EventArgs e)
        {
            Boolean isExistingPartNo = false;
            foreach (String item in PartNo)
            {
                if (item.ToUpper().Equals(PartNotextBox.Text.ToUpper()))
                {
                    isExistingPartNo = true;
                    PartNotextBox.Text = PartNotextBox.Text.ToUpper();
                    break;
                }
            }
            if (!isExistingPartNo)
            {
                PartNotextBox.Text = "";
            }
        }

        private void PartNotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in PartNo)
                {
                    if (item.ToUpper().Equals(PartNotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        PartNotextBox.Text = PartNotextBox.Text.ToUpper();
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    PartNotextBox.Text = "";
                }
            }
        }

        private void SupplierIDtextBox_Leave(object sender, EventArgs e)
        {
            Boolean isExistingPartNo = false;
            foreach (String item in spoilID)
            {
                if (item.ToUpper().Equals(SpoilIDtextBox.Text.ToUpper()))
                {
                    isExistingPartNo = true;
                    SpoilIDtextBox.Text = SpoilIDtextBox.Text.ToUpper();
                    break;
                }
            }
            if (!isExistingPartNo)
            {
                SpoilIDtextBox.Text = "";
            }
        }

        private void SupplierIDtextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in spoilID)
                {
                    if (item.ToUpper().Equals(SpoilIDtextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        SpoilIDtextBox.Text = SpoilIDtextBox.Text.ToUpper();
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    SpoilIDtextBox.Text = "";
                }
            }
        }

        private void InventoryAuditReport_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }
    }
}
