﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sufindo.Report.Purchase.PurchaseOrder;
using Sufindo.Report.Purchase.PurchaseReturn;

namespace Sufindo.Report
{
    public partial class AllReportForm : Form
    {
        /*
            -- Select --
            1 Purchase Order
            2 Purchase Return
            3 Order Picking
            4 Delivery Order
            5 Invoice
            6 Quotation
            7 SalesReturn
         */
        private delegate void fillDatatableCallBack(DataTable datatable);
        Connection connection;
        MenuStrip menustripAction;
        public AllReportForm()
        {
            InitializeComponent();
            if (connection == null) {
                connection = new Connection();
            }
            comboBoxReport.SelectedIndex = 0;
        }

        public AllReportForm(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            this.menustripAction = menustripAction;
            if (connection == null)
            {
                connection = new Connection();
            }
            comboBoxReport.SelectedIndex = 0;
        }

        private void addColumnsForPurchaseOrder()
        {
            dataGridView.Columns.Clear();
            DataGridViewTextBoxColumn NO = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn PURCHASEORDERID = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn PURCHASEORDERDATE = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn SUPPLIERNAME = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn CONTACTPERSONSUPPLIER = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn KURS = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn RATE = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn REMARK = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn STATUS = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn CREATEDBY = new DataGridViewTextBoxColumn();

            // 
            // NO
            // 
            NO.DataPropertyName = "NO";
            NO.HeaderText = "NO";
            NO.Name = "NO";
            NO.ReadOnly = true;
            NO.Width = 40;
            // 
            // PURCHASEORDERID
            // 
            PURCHASEORDERID.DataPropertyName = "PURCHASEORDERID";
            PURCHASEORDERID.HeaderText = "PO NUMBER";
            PURCHASEORDERID.Name = "PURCHASEORDERID";
            PURCHASEORDERID.ReadOnly = true;
            PURCHASEORDERID.Width = 120;
            // 
            // PURCHASEORDERDATE
            // 
            PURCHASEORDERDATE.DataPropertyName = "PURCHASEORDERDATE";
            PURCHASEORDERDATE.FillWeight = 90F;
            PURCHASEORDERDATE.HeaderText = "PO DATE";
            PURCHASEORDERDATE.Name = "PURCHASEORDERDATE";
            PURCHASEORDERDATE.ReadOnly = true;
            PURCHASEORDERDATE.Width = 90;
            // SUPPLIERNAME
            // 
            SUPPLIERNAME.DataPropertyName = "SUPPLIERNAME";
            SUPPLIERNAME.FillWeight = 120F;
            SUPPLIERNAME.HeaderText = "SUPPLIER NAME";
            SUPPLIERNAME.Name = "SUPPLIERNAME";
            SUPPLIERNAME.ReadOnly = true;
            SUPPLIERNAME.Width = 120;
            // 
            // CONTACTPERSONSUPPLIER
            // 
            CONTACTPERSONSUPPLIER.DataPropertyName = "CONTACTPERSONSUPPLIER";
            CONTACTPERSONSUPPLIER.FillWeight = 120F;
            CONTACTPERSONSUPPLIER.HeaderText = "CP SUPPLIER";
            CONTACTPERSONSUPPLIER.Name = "CONTACTPERSONSUPPLIER";
            CONTACTPERSONSUPPLIER.ReadOnly = true;
            CONTACTPERSONSUPPLIER.Width = 120;
            // 
            // KURS
            // 
            KURS.DataPropertyName = "KURSID";
            KURS.HeaderText = "CURRENCY";
            KURS.Name = "KURS";
            KURS.ReadOnly = true;
            // 
            // RATE
            // 
            RATE.DataPropertyName = "RATE";
            RATE.HeaderText = "RATE";
            RATE.Name = "RATE";
            RATE.ReadOnly = true;
            RATE.DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
            RATE.DefaultCellStyle.Format = "N2";
            // 
            // REMARK
            // 
            REMARK.DataPropertyName = "REMARK";
            REMARK.HeaderText = "REMARK";
            REMARK.Name = "REMARK";
            REMARK.ReadOnly = true;
            // 
            // STATUS
            // 
            STATUS.DataPropertyName = "STATUS";
            STATUS.HeaderText = "STATUS";
            STATUS.Name = "STATUS";
            STATUS.ReadOnly = true;
            // 
            // CREATEDBY
            // 
            CREATEDBY.DataPropertyName = "CREATEDBY";
            CREATEDBY.HeaderText = "CREATED BY";
            CREATEDBY.Name = "CREATEDBY";
            CREATEDBY.ReadOnly = true;

            dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
                NO,
                PURCHASEORDERID,
                PURCHASEORDERDATE,
                SUPPLIERNAME,
                CONTACTPERSONSUPPLIER,
                KURS,
                RATE,
                REMARK,
                STATUS,
                CREATEDBY});
        }

        private void addColumnsForPurchase()
        {
            dataGridView.Columns.Clear();
            DataGridViewTextBoxColumn NO = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn PURCHASEID = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn PURCHASEDATE = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn PURCHASEORDERID = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn PURCHASEORDERDATE = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn SUPPLIERNAME = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn CONTACTPERSONSUPPLIER = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn KURS = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn RATE = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn REMARK = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn STATUS = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn CREATEDBY = new DataGridViewTextBoxColumn();            

            // 
            // NO
            // 
            NO.DataPropertyName = "NO";
            NO.HeaderText = "NO";
            NO.Name = "NO";
            NO.ReadOnly = true;
            NO.Width = 40;
            // 
            // PURCHASEID
            // 
            PURCHASEID.DataPropertyName = "PURCHASEID";
            PURCHASEID.HeaderText = "RECEIPT NO";
            PURCHASEID.Name = "PURCHASEID";
            PURCHASEID.ReadOnly = true;
            PURCHASEID.Width = 120;
            // 
            // PURCHASEDATE
            // 
            PURCHASEDATE.DataPropertyName = "PURCHASEDATE";
            PURCHASEDATE.FillWeight = 120F;
            PURCHASEDATE.HeaderText = "RECEIPT DATE";
            PURCHASEDATE.Name = "PURCHASEDATE";
            PURCHASEDATE.ReadOnly = true;
            PURCHASEDATE.Width = 120;
            // 
            // PURCHASEORDERID
            // 
            PURCHASEORDERID.DataPropertyName = "PURCHASEORDERID";
            PURCHASEORDERID.HeaderText = "PO ID";
            PURCHASEORDERID.Name = "PURCHASEORDERID";
            PURCHASEORDERID.ReadOnly = true;
            // 
            // PURCHASEORDERDATE
            // 
            PURCHASEORDERDATE.DataPropertyName = "PURCHASEORDERDATE";
            PURCHASEORDERDATE.HeaderText = "PO DATE";
            PURCHASEORDERDATE.Name = "PURCHASEORDERDATE";
            PURCHASEORDERDATE.ReadOnly = true;
            // 
            // SUPPLIERNAME
            // 
            SUPPLIERNAME.DataPropertyName = "SUPPLIERNAME";
            SUPPLIERNAME.FillWeight = 120F;
            SUPPLIERNAME.HeaderText = "SUPPLIER NAME";
            SUPPLIERNAME.Name = "SUPPLIERNAME";
            SUPPLIERNAME.ReadOnly = true;
            SUPPLIERNAME.Width = 120;
            // 
            // CONTACTPERSONSUPPLIER
            // 
            CONTACTPERSONSUPPLIER.DataPropertyName = "CONTACTPERSONSUPPLIER";
            CONTACTPERSONSUPPLIER.FillWeight = 120F;
            CONTACTPERSONSUPPLIER.HeaderText = "CP SUPPLIER";
            CONTACTPERSONSUPPLIER.Name = "CONTACTPERSONSUPPLIER";
            CONTACTPERSONSUPPLIER.ReadOnly = true;
            CONTACTPERSONSUPPLIER.Width = 120;
            // 
            // KURS
            // 
            KURS.DataPropertyName = "KURSID";
            KURS.HeaderText = "CURRENCY";
            KURS.Name = "KURS";
            KURS.ReadOnly = true;
            // 
            // RATE
            // 
            RATE.DataPropertyName = "RATE";
            RATE.HeaderText = "RATE";
            RATE.Name = "RATE";
            RATE.ReadOnly = true;
            RATE.DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
            RATE.DefaultCellStyle.Format = "N2";
            // 
            // REMARK
            // 
            REMARK.DataPropertyName = "REMARK";
            REMARK.HeaderText = "REMARK";
            REMARK.Name = "REMARK";
            REMARK.ReadOnly = true;
            // 
            // STATUS
            // 
            STATUS.DataPropertyName = "STATUS";
            STATUS.HeaderText = "STATUS";
            STATUS.Name = "STATUS";
            STATUS.ReadOnly = true;
            STATUS.Visible = false;
            // 
            // CREATEDBY
            // 
            CREATEDBY.DataPropertyName = "CREATEDBY";
            CREATEDBY.HeaderText = "CREATED BY";
            CREATEDBY.Name = "CREATEDBY";
            CREATEDBY.ReadOnly = true;

            dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
                NO,
                PURCHASEID,
                PURCHASEDATE,
                PURCHASEORDERID,
                PURCHASEORDERDATE,
                SUPPLIERNAME,
                CONTACTPERSONSUPPLIER,
                KURS,
                RATE,
                REMARK,
                STATUS,
                CREATEDBY});
        }

        /*recheck*/
        private void addColumnsForPurchaseReturn()
        {
            dataGridView.Columns.Clear();
            DataGridViewTextBoxColumn NO = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn PURCHASERETURNID = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn PURCHASERETURNDATE = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn SUPPLIERID = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn REMARK = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn STATUS = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn CREATEDBY = new DataGridViewTextBoxColumn();

            // 
            // NO
            // 
            NO.DataPropertyName = "NO";
            NO.HeaderText = "NO";
            NO.Name = "NO";
            NO.ReadOnly = true;
            NO.Width = 40;
            // 
            // PURCHASERETURNID
            // 
            PURCHASERETURNID.DataPropertyName = "PURCHASERETURNID";
            PURCHASERETURNID.HeaderText = "PR NO";
            PURCHASERETURNID.Name = "PURCHASERETURNID";
            PURCHASERETURNID.ReadOnly = true;
            PURCHASERETURNID.Width = 120;
            // 
            // PURCHASERETURNDATE
            // 
            PURCHASERETURNDATE.DataPropertyName = "PURCHASERETURNDATE";
            PURCHASERETURNDATE.FillWeight = 120;
            PURCHASERETURNDATE.HeaderText = "PR DATE";
            PURCHASERETURNDATE.Name = "PURCHASEDATE";
            PURCHASERETURNDATE.ReadOnly = true;
            PURCHASERETURNDATE.Width = 120;
            // 
            // PURCHASEID
            // 
            //PURCHASEID.DataPropertyName = "PURCHASEID";
            //PURCHASEID.HeaderText = "RECEIPT ID";
            //PURCHASEID.Name = "PURCHASEID";
            //PURCHASEID.ReadOnly = true;
            // 
            // SUPPLIERNAME
            // 
            SUPPLIERID.DataPropertyName = "SUPPLIERNAME";
            SUPPLIERID.FillWeight = 120F;
            SUPPLIERID.HeaderText = "SUPPLIER NAME";
            SUPPLIERID.Name = "SUPPLIERNAME";
            SUPPLIERID.ReadOnly = true;
            SUPPLIERID.Width = 120;
            // 
            // REMARK
            // 
            REMARK.DataPropertyName = "REMARK";
            REMARK.HeaderText = "REMARK";
            REMARK.Name = "REMARK";
            REMARK.ReadOnly = true;
            // 
            // STATUS
            // 
            STATUS.DataPropertyName = "STATUS";
            STATUS.HeaderText = "STATUS";
            STATUS.Name = "STATUS";
            STATUS.ReadOnly = true;
            STATUS.Visible = false;
            // 
            // CREATEDBY
            // 
            CREATEDBY.DataPropertyName = "CREATEDBY";
            CREATEDBY.HeaderText = "CREATED BY";
            CREATEDBY.Name = "CREATEDBY";
            CREATEDBY.ReadOnly = true;

            dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
                NO,
                PURCHASERETURNID,
                PURCHASERETURNDATE,
                SUPPLIERID,
                REMARK,
                STATUS,
                CREATEDBY});
        }

        private void addColumnsForOrderPicking()
        {
            dataGridView.Columns.Clear();
            DataGridViewTextBoxColumn NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn ORDERPICKINGID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn ORDERPICKINGDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn SALESORDERID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn SALESORDERDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn CUSTOMERNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn CREATEDBY = new System.Windows.Forms.DataGridViewTextBoxColumn();

            // 
            // NO
            // 
            NO.DataPropertyName = "NO";
            NO.HeaderText = "NO";
            NO.Name = "NO";
            NO.ReadOnly = true;
            NO.Width = 40;
            // 
            // ORDERPICKINGID
            // 
            ORDERPICKINGID.DataPropertyName = "ORDERPICKINGID";
            ORDERPICKINGID.HeaderText = "OP NO";
            ORDERPICKINGID.Name = "ORDERPICKINGID";
            ORDERPICKINGID.ReadOnly = true;
            ORDERPICKINGID.Width = 80;
            // 
            // ORDERPICKINGDATE
            // 
            ORDERPICKINGDATE.DataPropertyName = "ORDERPICKINGDATE";
            ORDERPICKINGDATE.HeaderText = "OP DATE";
            ORDERPICKINGDATE.Name = "ORDERPICKINGDATE";
            ORDERPICKINGDATE.ReadOnly = true;
            ORDERPICKINGDATE.Width = 90;
            // 
            // SALESORDERID
            // 
            SALESORDERID.DataPropertyName = "SALESORDERID";
            SALESORDERID.HeaderText = "SO NO";
            SALESORDERID.Name = "SALESORDERID";
            SALESORDERID.ReadOnly = true;
            SALESORDERID.Width = 80;
            // 
            // SALESORDERDATE
            // 
            SALESORDERDATE.DataPropertyName = "SALESORDERDATE";
            SALESORDERDATE.FillWeight = 90F;
            SALESORDERDATE.HeaderText = "SO DATE";
            SALESORDERDATE.Name = "SALESORDERDATE";
            SALESORDERDATE.ReadOnly = true;
            SALESORDERDATE.Width = 90;
            // 
            // CUSTOMERID
            // 
            CUSTOMERNAME.DataPropertyName = "CUSTOMERNAME";
            CUSTOMERNAME.FillWeight = 110F;
            CUSTOMERNAME.HeaderText = "CUSTOMER NAME";
            CUSTOMERNAME.Name = "CUSTOMERNAME";
            CUSTOMERNAME.ReadOnly = true;
            CUSTOMERNAME.Width = 110;
            // 
            // STATUS
            // 
            STATUS.DataPropertyName = "STATUS";
            STATUS.HeaderText = "STATUS";
            STATUS.Name = "STATUS";
            STATUS.ReadOnly = true;
            // 
            // CREATEDBY
            // 
            CREATEDBY.DataPropertyName = "CREATEDBY";
            CREATEDBY.HeaderText = "CREATED BY";
            CREATEDBY.Name = "CREATEDBY";
            CREATEDBY.ReadOnly = true;

            dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
                NO,
                ORDERPICKINGID,
                ORDERPICKINGDATE,
                SALESORDERID,
                SALESORDERDATE,
                CUSTOMERNAME,
                STATUS,
                CREATEDBY});
        }

        private void addColumnsForSalesOrder() {
            dataGridView.Columns.Clear();
            DataGridViewTextBoxColumn NO = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn SALESORDERID = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn SALESORDERDATE = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn CUSTOMERID = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn CURRENCYID = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn RATE = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn PAYMENTMETHOD = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn DISCOUNT = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn REMARK = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn STATUS = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn CREATEDBY = new DataGridViewTextBoxColumn();
 
            // 
            // NO
            // 
            NO.DataPropertyName = "NO";
            NO.HeaderText = "NO";
            NO.Name = "NO";
            NO.ReadOnly = true;
            NO.Width = 40;
            // 
            // SALESORDERID
            // 
            SALESORDERID.DataPropertyName = "SALESORDERID";
            SALESORDERID.HeaderText = "SO NO";
            SALESORDERID.Name = "SALESORDERID";
            SALESORDERID.ReadOnly = true;
            SALESORDERID.Width = 120;
            // 
            // SALESORDERDATE
            // 
            SALESORDERDATE.DataPropertyName = "SALESORDERDATE";
            SALESORDERDATE.FillWeight = 90F;
            SALESORDERDATE.HeaderText = "SO DATE";
            SALESORDERDATE.Name = "SALESORDERDATE";
            SALESORDERDATE.ReadOnly = true;
            SALESORDERDATE.Width = 90;
            // 
            // CUSTOMERID
            // 
            CUSTOMERID.DataPropertyName = "CUSTOMERID";
            CUSTOMERID.FillWeight = 110F;
            CUSTOMERID.HeaderText = "CUSTOMER ID";
            CUSTOMERID.Name = "CUSTOMERID";
            CUSTOMERID.ReadOnly = true;
            CUSTOMERID.Width = 110;
            // 
            // CURRENCYID
            // 
            CURRENCYID.DataPropertyName = "CURRENCYID";
            CURRENCYID.HeaderText = "CURRENCY";
            CURRENCYID.Name = "CURRENCYID";
            CURRENCYID.ReadOnly = true;
            // 
            // RATE
            // 
            RATE.DataPropertyName = "RATE";
            RATE.HeaderText = "RATE";
            RATE.Name = "RATE";
            RATE.ReadOnly = true;
            RATE.DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
            RATE.DefaultCellStyle.Format = "N2";
            // 
            // PAYMENTMETHOD
            // 
            PAYMENTMETHOD.DataPropertyName = "PAYMENTMETHOD";
            PAYMENTMETHOD.FillWeight = 140F;
            PAYMENTMETHOD.HeaderText = "PAYMENT METHOD";
            PAYMENTMETHOD.Name = "PAYMENTMETHOD";
            PAYMENTMETHOD.ReadOnly = true;
            PAYMENTMETHOD.Width = 140;
            // 
            // DISCOUNT
            // 
            DISCOUNT.DataPropertyName = "DISCOUNT";
            DISCOUNT.HeaderText = "DISCOUNT";
            DISCOUNT.Name = "DISCOUNT";
            DISCOUNT.ReadOnly = true;
            DISCOUNT.DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
            DISCOUNT.DefaultCellStyle.Format = "N2";
            // 
            // REMARK
            // 
            REMARK.DataPropertyName = "REMARK";
            REMARK.HeaderText = "REMARK";
            REMARK.Name = "REMARK";
            REMARK.ReadOnly = true;
            // 
            // STATUS
            // 
            STATUS.DataPropertyName = "STATUS";
            STATUS.HeaderText = "STATUS";
            STATUS.Name = "STATUS";
            STATUS.ReadOnly = true;
            // 
            // CREATEDBY
            // 
            CREATEDBY.DataPropertyName = "CREATEDBY";
            CREATEDBY.HeaderText = "CREATED BY";
            CREATEDBY.Name = "CREATEDBY";
            CREATEDBY.ReadOnly = true;

            dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
                NO,
                SALESORDERID,
                SALESORDERDATE,
                CUSTOMERID,
                CURRENCYID,
                RATE,
                PAYMENTMETHOD,
                DISCOUNT,
                REMARK,
                STATUS,
                CREATEDBY});
        }

        private void addColumnsForQuotation()
        {
            dataGridView.Columns.Clear();
            DataGridViewTextBoxColumn NO = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn QUOTATIONID = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn QUOTATIONDATE = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn CUSTOMERNAME = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn CURRENCYID = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn RATE = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn PAYMENTMETHOD = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn DISCOUNT = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn REMARK = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn STATUS = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn CREATEDBY = new DataGridViewTextBoxColumn();

            // 
            // NO
            // 
            NO.DataPropertyName = "NO";
            NO.HeaderText = "NO";
            NO.Name = "NO";
            NO.ReadOnly = true;
            NO.Width = 40;
            // 
            // QUOTATIONID
            // 
            QUOTATIONID.DataPropertyName = "QUOTATIONID";
            QUOTATIONID.HeaderText = "QUOTATION NO";
            QUOTATIONID.Name = "QUOTATIONID";
            QUOTATIONID.ReadOnly = true;
            QUOTATIONID.Width = 100;
            // 
            // QUOTATIONDATE
            // 
            QUOTATIONDATE.DataPropertyName = "QUOTATIONDATE";
            QUOTATIONDATE.FillWeight = 90F;
            QUOTATIONDATE.HeaderText = "QUOTATION DATE";
            QUOTATIONDATE.Name = "QUOTATIONDATE";
            QUOTATIONDATE.ReadOnly = true;
            QUOTATIONDATE.Width = 110;
            // 
            // CUSTOMERID
            // 
            CUSTOMERNAME.DataPropertyName = "CUSTOMERNAME";
            CUSTOMERNAME.FillWeight = 110F;
            CUSTOMERNAME.HeaderText = "CUSTOMER NAME";
            CUSTOMERNAME.Name = "CUSTOMERNAME";
            CUSTOMERNAME.ReadOnly = true;
            CUSTOMERNAME.Width = 120;
            // 
            // CURRENCYID
            // 
            CURRENCYID.DataPropertyName = "CURRENCYID";
            CURRENCYID.HeaderText = "CURRENCY";
            CURRENCYID.Name = "CURRENCYID";
            CURRENCYID.ReadOnly = true;
            CURRENCYID.Width = 80;
            // 
            // RATE
            // 
            RATE.DataPropertyName = "RATE";
            RATE.HeaderText = "RATE";
            RATE.Name = "RATE";
            RATE.ReadOnly = true;
            RATE.DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
            RATE.DefaultCellStyle.Format = "N2";
            RATE.Width = 70;
            // 
            // PAYMENTMETHOD
            // 
            PAYMENTMETHOD.DataPropertyName = "PAYMENTMETHOD";
            PAYMENTMETHOD.FillWeight = 140F;
            PAYMENTMETHOD.HeaderText = "PAYMENT METHOD";
            PAYMENTMETHOD.Name = "PAYMENTMETHOD";
            PAYMENTMETHOD.ReadOnly = true;
            PAYMENTMETHOD.Width = 120;
            // 
            // DISCOUNT
            // 
            DISCOUNT.DataPropertyName = "DISCOUNT";
            DISCOUNT.HeaderText = "DISCOUNT";
            DISCOUNT.Name = "DISCOUNT";
            DISCOUNT.ReadOnly = true;
            DISCOUNT.DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
            DISCOUNT.DefaultCellStyle.Format = "N2";
            DISCOUNT.Width = 70;
            // 
            // REMARK
            // 
            REMARK.DataPropertyName = "REMARK";
            REMARK.HeaderText = "REMARK";
            REMARK.Name = "REMARK";
            REMARK.ReadOnly = true;
            REMARK.Width = 80;
            // 
            // STATUS
            // 
            STATUS.DataPropertyName = "STATUS";
            STATUS.HeaderText = "STATUS";
            STATUS.Name = "STATUS";
            STATUS.ReadOnly = true;
            STATUS.Width = 80;
            // 
            // CREATEDBY
            // 
            CREATEDBY.DataPropertyName = "CREATEDBY";
            CREATEDBY.HeaderText = "CREATED BY";
            CREATEDBY.Name = "CREATEDBY";
            CREATEDBY.ReadOnly = true;

            dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
                NO,
                QUOTATIONID,
                QUOTATIONDATE,
                CUSTOMERNAME,
                CURRENCYID,
                RATE,
                PAYMENTMETHOD,
                DISCOUNT,
                REMARK,
                STATUS,
                CREATEDBY});
        }

        private void addColumnsForDeliveryOrder()
        {
            dataGridView.Columns.Clear();
            DataGridViewTextBoxColumn NO = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn DELIVERYORDERID = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn DELIVERYORDERDATE = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn SALESORDERID = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn INVOICEID = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn CUSTOMERNAME = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn STATUS = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn CREATEDBY = new DataGridViewTextBoxColumn();

            // 
            // NO
            // 
            NO.DataPropertyName = "NO";
            NO.HeaderText = "NO";
            NO.Name = "NO";
            NO.ReadOnly = true;
            NO.Width = 40;
            // 
            // DELIVERYORDERID
            // 
            DELIVERYORDERID.DataPropertyName = "DELIVERYORDERID";
            DELIVERYORDERID.FillWeight = 90F;
            DELIVERYORDERID.HeaderText = "DO NO";
            DELIVERYORDERID.Name = "DELIVERYORDERID";
            DELIVERYORDERID.ReadOnly = true;
            DELIVERYORDERID.Width = 80;
            // 
            // DELIVERYORDERDATE
            // 
            DELIVERYORDERDATE.DataPropertyName = "DELIVERYORDERDATE";
            DELIVERYORDERDATE.FillWeight = 70F;
            DELIVERYORDERDATE.HeaderText = "DO DATE";
            DELIVERYORDERDATE.Name = "DELIVERYORDERDATE";
            DELIVERYORDERDATE.ReadOnly = true;
            DELIVERYORDERDATE.Width = 80;
            // 
            // SALESORDERID
            // 
            SALESORDERID.DataPropertyName = "SALESORDERID";
            SALESORDERID.HeaderText = "SO NO";
            SALESORDERID.Name = "SALESORDERID";
            SALESORDERID.ReadOnly = true;
            SALESORDERID.Width = 80;
            // 
            // INVOICEID
            // 
            INVOICEID.DataPropertyName = "INVOICEID";
            INVOICEID.HeaderText = "INVOICE NO";
            INVOICEID.Name = "INVOICEID";
            INVOICEID.ReadOnly = true;
            INVOICEID.Width = 80;
            // CUSTOMERID
            // 
            CUSTOMERNAME.DataPropertyName = "CUSTOMERNAME";
            CUSTOMERNAME.FillWeight = 110F;
            CUSTOMERNAME.HeaderText = "CUSTOMER NAME";
            CUSTOMERNAME.Name = "CUSTOMERNAME";
            CUSTOMERNAME.ReadOnly = true;
            CUSTOMERNAME.Width = 130;
            // 
            // 
            // STATUS
            // 
            STATUS.DataPropertyName = "STATUS";
            STATUS.FillWeight = 70F;
            STATUS.HeaderText = "STATUS";
            STATUS.Name = "STATUS";
            STATUS.ReadOnly = true;
            STATUS.Width = 70;
            // 
            // CREATEDBY
            // 
            CREATEDBY.DataPropertyName = "CREATEDBY";
            CREATEDBY.HeaderText = "CREATED BY";
            CREATEDBY.Name = "CREATEDBY";
            CREATEDBY.ReadOnly = true;

            dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
                NO,
                DELIVERYORDERID,
                DELIVERYORDERDATE,
                SALESORDERID,
                INVOICEID,
                CUSTOMERNAME,
                STATUS,
                CREATEDBY});
        }

        private void addColumnsForInvoice()
        {
            dataGridView.Columns.Clear();
            DataGridViewTextBoxColumn NO = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn INVOICEID = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn INVOICEDATE = new DataGridViewTextBoxColumn();
            //DataGridViewTextBoxColumn DELIVERYORDERID = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn CUSTOMERNAME = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn SALESORDERID = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn STATUS = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn CREATEDBY = new DataGridViewTextBoxColumn();

            // 
            // NO
            // 
            NO.DataPropertyName = "NO";
            NO.HeaderText = "NO";
            NO.Name = "NO";
            NO.ReadOnly = true;
            NO.Width = 40;
            // 
            // DELIVERYORDERID
            // 
            CUSTOMERNAME.DataPropertyName = "CUSTOMERNAME";
            CUSTOMERNAME.FillWeight = 90F;
            CUSTOMERNAME.HeaderText = "CUSTOMER NAME";
            CUSTOMERNAME.Name = "CUSTOMERNAME";
            CUSTOMERNAME.ReadOnly = true;
            CUSTOMERNAME.Width = 120;
            // 
            // DELIVERYORDERDATE
            // 
            INVOICEDATE.DataPropertyName = "INVOICEDATE";
            INVOICEDATE.FillWeight = 90F;
            INVOICEDATE.HeaderText = "INVOICE DATE";
            INVOICEDATE.Name = "INVOICEDATE";
            INVOICEDATE.ReadOnly = true;
            INVOICEDATE.Width = 90;
            // 
            // SALESORDERID
            // 
            SALESORDERID.DataPropertyName = "SALESORDERID";
            SALESORDERID.HeaderText = "SO NO";
            SALESORDERID.Name = "SALESORDERID";
            SALESORDERID.ReadOnly = true;
            SALESORDERID.Width = 80;
            // 
            // INVOICEID
            // 
            INVOICEID.DataPropertyName = "INVOICEID";
            INVOICEID.HeaderText = "INVOICE NO";
            INVOICEID.Name = "INVOICEID";
            INVOICEID.ReadOnly = true;
            INVOICEID.Width = 90;
            // 
            // STATUS
            // 
            STATUS.DataPropertyName = "STATUS";
            STATUS.HeaderText = "STATUS";
            STATUS.Name = "STATUS";
            STATUS.ReadOnly = true;
            // 
            // CREATEDBY
            // 
            CREATEDBY.DataPropertyName = "CREATEDBY";
            CREATEDBY.HeaderText = "CREATED BY";
            CREATEDBY.Name = "CREATEDBY";
            CREATEDBY.ReadOnly = true;

            dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
                NO,
                INVOICEID,
                INVOICEDATE,
                SALESORDERID,
                CUSTOMERNAME,
                STATUS,
                CREATEDBY});
        }

        private void addColumnsForSalesReturn() {
            dataGridView.Columns.Clear();
            DataGridViewTextBoxColumn NO = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn TOTAL = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn SALESRETURNID = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn SALESRETURNDATE = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn INVOICEID = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn INVOICEDATE = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn TOTALINVOICE = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn CUSTOMERNAME = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn CURRENCYID = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn RATE = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn REBATE = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn REMARK = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn STATUS = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn CREATEDBY = new DataGridViewTextBoxColumn();           

            // 
            // NO
            // 
            NO.DataPropertyName = "NO";
            NO.HeaderText = "NO";
            NO.Name = "NO";
            NO.ReadOnly = true;
            NO.Width = 40;
            // 
            // SALESRETURNID
            // 
            SALESRETURNID.DataPropertyName = "SALESRETURNID";
            SALESRETURNID.HeaderText = "SR NO";
            SALESRETURNID.Name = "SALESRETURNID";
            SALESRETURNID.ReadOnly = true;
            SALESRETURNID.Width = 80;
            // 
            // SALESRETURNDATE
            // 
            SALESRETURNDATE.DataPropertyName = "SALESRETURNDATE";
            SALESRETURNDATE.HeaderText = "SR DATE";
            SALESRETURNDATE.Name = "SALESRETURNDATE";
            SALESRETURNDATE.ReadOnly = true;
            SALESRETURNDATE.Width = 80;
            // 
            // INVOICEID
            // 
            INVOICEID.DataPropertyName = "INVOICEID";
            INVOICEID.HeaderText = "INVOICE NO";
            INVOICEID.Name = "INVOICEID";
            INVOICEID.ReadOnly = true;
            INVOICEID.Width = 80;
            // 
            // INVOICEDATE
            // 
            INVOICEDATE.DataPropertyName = "INVOICEDATE";
            INVOICEDATE.HeaderText = "INVOICE DATE";
            INVOICEDATE.Name = "INVOICEDATE";
            INVOICEDATE.ReadOnly = true;
            INVOICEDATE.Width = 100;
            // 
            // CUSTOMERID
            // 
            CUSTOMERNAME.DataPropertyName = "CUSTOMERNAME";
            CUSTOMERNAME.FillWeight = 110F;
            CUSTOMERNAME.HeaderText = "CUSTOMER NAME";
            CUSTOMERNAME.Name = "CUSTOMERNAME";
            CUSTOMERNAME.ReadOnly = true;
            CUSTOMERNAME.Width = 120;
            // 
            // CURRENCYID
            // 
            CURRENCYID.DataPropertyName = "CURRENCYID";
            CURRENCYID.HeaderText = "CURRENCY";
            CURRENCYID.Name = "CURRENCYID";
            CURRENCYID.ReadOnly = true;
            CURRENCYID.Width = 80;
            // 
            // RATE
            // 
            RATE.DataPropertyName = "RATE";
            RATE.HeaderText = "RATE";
            RATE.Name = "RATE";
            RATE.ReadOnly = true;
            RATE.DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
            RATE.DefaultCellStyle.Format = "N2";
            RATE.Width = 80;
            // 
            // REBATE
            // 
            REBATE.DataPropertyName = "REBATE";
            REBATE.HeaderText = "REBATE";
            REBATE.Name = "REBATE";
            REBATE.ReadOnly = true;
            REBATE.DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
            REBATE.DefaultCellStyle.Format = "N2";
            REBATE.Width = 80;
            // 
            // REMARK
            // 
            REMARK.DataPropertyName = "REMARK";
            REMARK.HeaderText = "REMARK";
            REMARK.Name = "REMARK";
            REMARK.ReadOnly = true;
            REMARK.Width = 80;
            // 
            // STATUS
            // 
            STATUS.DataPropertyName = "STATUS";
            STATUS.HeaderText = "STATUS";
            STATUS.Name = "STATUS";
            STATUS.ReadOnly = true;
            STATUS.Width = 80;
            // 
            // CREATEDBY
            // 
            CREATEDBY.DataPropertyName = "CREATEDBY";
            CREATEDBY.HeaderText = "CREATED BY";
            CREATEDBY.Name = "CREATEDBY";
            CREATEDBY.ReadOnly = true;
            
            dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
               NO,
               SALESRETURNID,
               SALESRETURNDATE,
               INVOICEID,
               INVOICEDATE,
               CUSTOMERNAME,
               REBATE,
               REMARK,
               STATUS,
               CREATEDBY
            });
        }

        private void comboBoxReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridView.DataSource = null;
            switch (comboBoxReport.SelectedIndex)
            {
                case 0:
                    dataGridView.Columns.Clear();
                    break;
                case 1://Purchase Report
                    addColumnsForPurchaseOrder();
                    break;
                case 2://Purchase Return
                    addColumnsForPurchaseReturn();
                    break;
                case 3://Order Picking
                    addColumnsForOrderPicking();
                    break;
                case 4://Delivery Order
                    addColumnsForDeliveryOrder();
                    break;
                case 5://Invoice
                    addColumnsForInvoice();
                    break;
                case 6:// Quotation
                    addColumnsForQuotation();
                    break;
                case 7://SalesReturn
                    addColumnsForSalesReturn();
                    break;
                default:
                    break;
            }
        }

        private void reloadAllData(String typeReport)
        {
            try
            {
                String query = resultQuery(typeReport);
                if (query.Equals("")) {
                    return;
                }
                String[] argument = { query };
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync(argument);
            }
            catch (Exception ex)
            {
                MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            
        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                String[] obj = e.Argument as String[];
                String query = (String)obj[0];
                BeginInvoke(new fillDatatableCallBack(this.fillDatatable), connection.openDataTableQuery(query));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void fillDatatable(DataTable datatable)
        {
            dataGridView.DataSource = datatable;
        }

        String resultQuery(String typeReport) {
            String query = "";
            if (typeReport.Equals("Purchase Order"))
            {
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY PURCHASEORDERID) AS 'NO', PURCHASEORDERID, CONVERT(VARCHAR(10), PURCHASEORDERDATE, 103) AS [PURCHASEORDERDATE], " +
                                      "SUPPLIERNAME, CONTACTPERSONSUPPLIER, KURSID, RATE, A.STATUS, REMARK, A.CREATEDBY " +
                                      "FROM H_PURCHASE_ORDER A " +
                                      "JOIN M_SUPPLIER B " +
                                      "ON A.SUPPLIERID = B.SUPPLIERID " +
                                      "WHERE CONVERT(char(10), PURCHASEORDERDATE,126) BETWEEN '{0}' AND '{1}' " +
                                      "ORDER BY PURCHASEORDERID ASC", dateTimePickerFrom.Value.ToString("yyyy-MM-dd"), dateTimePickerTo.Value.ToString("yyyy-MM-dd"));
            }
            else if (typeReport.Equals("Purchase Return"))
            {
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY PURCHASERETURNID) AS 'NO', PURCHASERETURNID, CONVERT(VARCHAR(10), PURCHASERETURNDATE, 103) AS PURCHASERETURNDATE, " +
                                      "SUPPLIERNAME, A.STATUS, REMARK, A.CREATEDBY FROM H_PURCHASE_RETURN A " +
                                      "JOIN M_SUPPLIER B " +
                                      "ON A.SUPPLIERID = B.SUPPLIERID " +
                                      "WHERE CONVERT(char(10), PURCHASERETURNDATE,126) BETWEEN '{0}' AND '{1}' " +
                                      "ORDER BY PURCHASERETURNID ASC", dateTimePickerFrom.Value.ToString("yyyy-MM-dd"), dateTimePickerTo.Value.ToString("yyyy-MM-dd"));
            }
            else if (typeReport.Equals("Order Picking"))
            {
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY ORDERPICKINGID) AS 'NO', ORDERPICKINGID, " +
                                      "CONVERT(VARCHAR(10), [ORDERPICKINGDATE], 103) AS [ORDERPICKINGDATE], A.SALESORDERID, " +
                                      "CONVERT(VARCHAR(10), SALESORDERDATE, 103) AS [SALESORDERDATE], " +
                                      "CUSTOMERNAME, A.STATUS, A.CREATEDBY " +
                                      "FROM H_ORDER_PICKING A " +
                                      "JOIN H_SALES_ORDER B " +
                                      "ON A.SALESORDERID = B.SALESORDERID " +
                                      "JOIN M_CUSTOMER C ON B.CUSTOMERID = C.CUSTOMERID " +
                                      "WHERE CONVERT(char(10), ORDERPICKINGDATE,126) BETWEEN '{0}' AND '{1}' " +
                                      "ORDER BY ORDERPICKINGID ASC", dateTimePickerFrom.Value.ToString("yyyy-MM-dd"), dateTimePickerTo.Value.ToString("yyyy-MM-dd"));
            }
            else if (typeReport.Equals("Delivery Order"))
            {
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY DELIVERYORDERID) AS 'NO', A.DELIVERYORDERID, CONVERT(VARCHAR(10), A.DELIVERYORDERDATE, 103) AS DELIVERYORDERDATE, " +
                                      "B.SALESORDERID, A.INVOICEID, CUSTOMERNAME, A.STATUS, A.CREATEDBY FROM H_DELIVERY_ORDER A " +
                                      "JOIN H_INVOICE B " +
                                      "ON A.INVOICEID = B.INVOICEID " +
                                      "JOIN M_CUSTOMER C " +
                                      "ON C.CUSTOMERID = B.CUSTOMERID " +
                                      "WHERE CONVERT(char(10), A.DELIVERYORDERDATE, 126) BETWEEN '{0}' AND '{1}' " +
                                      "ORDER BY A.DELIVERYORDERID ASC", dateTimePickerFrom.Value.ToString("yyyy-MM-dd"), dateTimePickerTo.Value.ToString("yyyy-MM-dd"));
            }
            else if (typeReport.Equals("Invoice"))
            {
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.INVOICEID) AS 'NO', A.INVOICEID, CONVERT(VARCHAR(10), A.INVOICEDATE, 103) AS INVOICEDATE, " +
                                     "A.SALESORDERID,CUSTOMERNAME, A.STATUS, A.CREATEDBY " +
                                      "FROM H_INVOICE A JOIN M_CUSTOMER B ON A.CUSTOMERID = B.CUSTOMERID " +
                                      "WHERE CONVERT(char(10), A.INVOICEDATE, 126) BETWEEN '{0}' AND '{1}' " +
                                      "ORDER BY A.INVOICEID ASC", dateTimePickerFrom.Value.ToString("yyyy-MM-dd"), dateTimePickerTo.Value.ToString("yyyy-MM-dd"));
            }
            else if (typeReport.Equals("Quotation"))
            {
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY QUOTATIONID) AS 'NO', QUOTATIONID, CONVERT(VARCHAR(10), QUOTATIONDATE, 103) AS QUOTATIONDATE, " +
                                      "CUSTOMERNAME, CURRENCYID, RATE, PAYMENTMETHOD, REMARK, DISCOUNT, A.STATUS, A.CREATEDBY " +
                                      "FROM H_QUOTATION A JOIN M_CUSTOMER B " +
                                      "ON A.CUSTOMERID = B.CUSTOMERID " +
                                      "WHERE CONVERT(char(10), QUOTATIONDATE, 126) BETWEEN '{0}' AND '{1}' " +
                                      "ORDER BY QUOTATIONID ASC", dateTimePickerFrom.Value.ToString("yyyy-MM-dd"), dateTimePickerTo.Value.ToString("yyyy-MM-dd"));
            }
            else if (typeReport.Equals("Sales Return"))
            {
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SALESRETURNID) AS 'NO',SALESRETURNID, CONVERT(VARCHAR(10), SALESRETURNDATE, 103) AS SALESRETURNDATE, " +
                                      "A.INVOICEID, CONVERT(VARCHAR(10), INVOICEDATE, 103) AS INVOICEDATE, " +
                                      "C.CUSTOMERNAME, A.REBATE, A.REMARK, A.STATUS, A.CREATEDBY " +
                                      "FROM H_SALES_RETURN A " +
                                      "JOIN H_INVOICE B " +
                                      "ON A.INVOICEID = B.INVOICEID " +
                                      "JOIN M_CUSTOMER C " +
                                      "ON B.CUSTOMERID = C.CUSTOMERID " +
                                      "WHERE CONVERT(char(10), SALESRETURNDATE,126) BETWEEN '{0}' AND '{1}' " +
                                      "AND A.STATUS != 'Deleted' " +
                                      "ORDER BY SALESRETURNID ASC", dateTimePickerFrom.Value.ToString("yyyy-MM-dd"), dateTimePickerTo.Value.ToString("yyyy-MM-dd"));
            }
            return query;
        }

        private void dataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            { 
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(comboBoxReport.Items[comboBoxReport.SelectedIndex].ToString());
            if (comboBoxReport.SelectedIndex != 0)
            {
                reloadAllData(comboBoxReport.Items[comboBoxReport.SelectedIndex].ToString());
            }
        }

        public void print() {
            if (dataGridView.RowCount > 0)
            {
                String id = dataGridView.SelectedCells[1].Value.ToString();
                switch (comboBoxReport.SelectedIndex)
                {
                    case 1://Purchase Report
                        DialogResult r = MessageBox.Show("Apakah Anda mau export PO dengan menggunakan Part No QTY ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (r == DialogResult.Yes)
                        {
                            PurchaseOrderReportForm purchaseorderreportform = new PurchaseOrderReportForm(id, true);
                            purchaseorderreportform.ShowDialog();
                        }
                        else if (r == DialogResult.No)
                        {
                            PurchaseOrderReportForm purchaseorderreportform = new PurchaseOrderReportForm(id, false);
                            purchaseorderreportform.ShowDialog();
                        }
                        break;
                    case 2://Purchase Return
                        PurchaseReturnReportForm purchasereturnReportForm = new PurchaseReturnReportForm(id, false);
                        purchasereturnReportForm.ShowDialog();
                        break;
                    case 3://Order Picking
                        OrderPickingReportForm orderpickingReportForm = new OrderPickingReportForm("AllReportForm", id, false);
                        orderpickingReportForm.ShowDialog();
                        break;
                    case 4://Delivery Order
                        DeliveryOrderReportForm deliveryOrderReportForm = new DeliveryOrderReportForm(id);
                        deliveryOrderReportForm.ShowDialog();
                        break;
                    case 5://Invoice
                        InvoiceReportForm invoiceReportForm = new InvoiceReportForm(id);
                        invoiceReportForm.ShowDialog();
                        break;
                    case 6://quotation
                        QuotationReportForm quotationReportForm = new QuotationReportForm(id);
                        quotationReportForm.ShowDialog();
                        break;
                    case 7://sales return
                        r = MessageBox.Show("Apakah Anda mau cetak retur penjualan dengan menggunakan PART NO QTY ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                        SalesReturnReportForm salesReturnReportForm = new SalesReturnReportForm(id, (r == DialogResult.Yes));
                        salesReturnReportForm.ShowDialog();
                        break;
                    default:
                        break;
                }
            }
        }

        private void AllReportForm_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            Main.ACTIVEFORM = this;
        }

        private void AllReportForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }
    }
}
