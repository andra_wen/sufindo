﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace Sufindo.Report.ListEOBI
{
    public partial class ListEOBIFormCR : Form
    {
        public ListEOBIFormCR()
        {
            InitializeComponent();
        }

        public ListEOBIFormCR(String FROMDATE, String TODATE, String ITEMID, String TYPE)
        {
            InitializeComponent();

            ListEOBIDS listds = new ListEOBIDS();
            DataSet ds = listds;

            Connection conn = new Connection();

            List<SqlParameter> sqlParam = new List<SqlParameter>();
            if (FROMDATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", FROMDATE));
            }

            if (TODATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@TODATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@TODATE", TODATE));
            }

            if (TYPE.Equals("Extra Out"))
            {
                sqlParam.Add(new SqlParameter("@TYPE", "EO"));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@TYPE", "BI"));
            }

            if (ITEMID.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@ITEMID", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@ITEMID", ITEMID));
            }

            conn.fillDataSetQuery(ref ds, "SELECT_DATA_EXTRACTOUTBUILDIN", sqlParam, "ListEOBIDataTable");

            ListEOBIReportCR cr = new ListEOBIReportCR();
            FROMDATE = FROMDATE.Equals("") ? "" : DateTime.Parse(FROMDATE).ToString("dd/MM/yyyy");
            TODATE = TODATE.Equals("") ? "" : DateTime.Parse(TODATE).ToString("dd/MM/yyyy");
            Object parameter0 = TYPE.Equals("Extra Out") ? "Extra Out Report" : "Build In Report";
            Object parameter1 = TYPE.Equals("Extra Out") ? "Extra Out To" : "Build In From";
            Object parameter2 = (FROMDATE.Equals("") || TODATE.Equals("")) ? "-" : String.Format("{0}-{1}", FROMDATE, TODATE);

            cr.SetDataSource(ds);
            cr.SetParameterValue(0, parameter0);
            cr.SetParameterValue(1, parameter1);
            cr.SetParameterValue(2, parameter2);
            crystalReportViewer.ReportSource = cr;
            crystalReportViewer.Refresh();

        }
    }
}
