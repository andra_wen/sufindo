﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Sufindo.Report.ListSalesOrder
{
    public partial class ListSalesOrderReport : Form
    {
        private delegate void fillAutoCompleteTextBox();

        private SalesOrderdatagridview salesorderdatagridview;
        private Customerdatagridview customerdatagridview;
        private Connection connection;

        private MenuStrip menustripAction;
        private List<String> salesreturnID;
        private List<String> customerID;

        public ListSalesOrderReport()
        {
            InitializeComponent();
            if (connection == null) {
                connection = new Connection();
            }
        }

        public ListSalesOrderReport(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            this.menustripAction = menustripAction;
            if (connection == null)
            {
                connection = new Connection();
            }
        }

        private void checkBoxDate_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePickerEnd.Enabled = checkBoxDate.Checked;
            dateTimePickerStart.Enabled = checkBoxDate.Checked;
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }
        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e){

        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e) {
            if (salesreturnID == null)
            {
                salesreturnID = new List<string>();
            }
            else
            {
                salesreturnID.Clear();
            }
            SqlDataReader sqldataReader = connection.sqlDataReaders(String.Format("SELECT SALESORDERID FROM H_SALES_ORDER"));

            while (sqldataReader.Read())
            {
                salesreturnID.Add(sqldataReader.GetString(0));
            }

            if (customerID == null)
            {
                customerID = new List<string>();
            }
            else
            {
                customerID.Clear();
            }
            sqldataReader = connection.sqlDataReaders(String.Format("SELECT CUSTOMERID FROM M_CUSTOMER"));

            while (sqldataReader.Read())
            {
                customerID.Add(sqldataReader.GetString(0));
            }

            if (IsHandleCreated) BeginInvoke(new fillAutoCompleteTextBox(this.fillAutoComplete));
        }

        private void fillAutoComplete()
        {
            SalesReturnIDtextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            SalesReturnIDtextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            SalesReturnIDtextBox.AutoCompleteCustomSource.AddRange(salesreturnID.ToArray());

            CustomerIDtextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            CustomerIDtextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            CustomerIDtextBox.AutoCompleteCustomSource.AddRange(customerID.ToArray());
        }

        public void print() {
            String fromDate = "";
            String toDate = "";

            if (checkBoxDate.Checked) {
                fromDate = dateTimePickerStart.Value.ToString("yyyy-MM-dd");
                toDate = dateTimePickerEnd.Value.ToString("yyyy-MM-dd");
            }

            ListSalesOrderFormCR listsalesorderformcr = new ListSalesOrderFormCR(fromDate, toDate, SalesReturnIDtextBox.Text, CustomerIDtextBox.Text);
            listsalesorderformcr.ShowDialog();
        }

        private void labelFromDate_Click(object sender, EventArgs e)
        {
            checkBoxDate.Checked = !checkBoxDate.Checked;
        }
        private void searchInvoiceIDbutton_Click(object sender, EventArgs e)
        {
            if (salesorderdatagridview == null)
            {
                salesorderdatagridview = new SalesOrderdatagridview("ListSalesOrderReport" ,true);
                salesorderdatagridview.salesOrderPassingData = new SalesOrderdatagridview.SalesOrderPassingData(SalesOrderPassingData);
                //salesorderdatagridview.MdiParent = this.MdiParent;
                salesorderdatagridview.ShowDialog();
            }
            else if (salesorderdatagridview.IsDisposed)
            {
                salesorderdatagridview = new SalesOrderdatagridview("ListSalesOrderReport" ,true);
                salesorderdatagridview.salesOrderPassingData = new SalesOrderdatagridview.SalesOrderPassingData(SalesOrderPassingData);
                //salesorderdatagridview.MdiParent = this.MdiParent;
                salesorderdatagridview.ShowDialog();
            }
        }

        private void SalesOrderPassingData(DataTable sender)
        {
            SalesReturnIDtextBox.Text = sender.Rows[0]["SALESORDERID"].ToString();
        }

        private void ListInvoiceReport_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }

        private void ListInvoiceReport_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            Main.ACTIVEFORM = this;
            reloadAllData();
        }

        private void InvoiceIDtextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in salesreturnID)
                {
                    if (item.ToUpper().Equals(SalesReturnIDtextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        SalesReturnIDtextBox.Text = SalesReturnIDtextBox.Text.ToUpper();
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    SalesReturnIDtextBox.Text = "";
                }
            }
        }

        private void InvoiceIDtextBox_Leave(object sender, EventArgs e)
        {
            Boolean isExistingPartNo = false;
            foreach (String item in salesreturnID)
            {
                if (item.ToUpper().Equals(SalesReturnIDtextBox.Text.ToUpper()))
                {
                    isExistingPartNo = true;
                    SalesReturnIDtextBox.Text = SalesReturnIDtextBox.Text.ToUpper();
                    break;
                }
            }
            if (!isExistingPartNo)
            {
                SalesReturnIDtextBox.Text = "";
            }
        }

        private void searchCustomerIDbutton_Click(object sender, EventArgs e)
        {
            if (salesorderdatagridview == null)
            {
                customerdatagridview = new Customerdatagridview("ListInvoicePerCustomerReport");
                customerdatagridview.masterCustomerPassingData = new Customerdatagridview.MasterCustomerPassingData(MasterCustomerPassingData);
                //salesorderdatagridview.MdiParent = this.MdiParent;
                customerdatagridview.ShowDialog();
            }
            else if (salesorderdatagridview.IsDisposed)
            {
                customerdatagridview = new Customerdatagridview("ListInvoicePerCustomerReport");
                customerdatagridview.masterCustomerPassingData = new Customerdatagridview.MasterCustomerPassingData(MasterCustomerPassingData);
                //salesorderdatagridview.MdiParent = this.MdiParent;
                customerdatagridview.ShowDialog();
            }
        }

        private void MasterCustomerPassingData(DataTable sender)
        {
            CustomerIDtextBox.Text = sender.Rows[0]["CUSTOMERID"].ToString();
        }
    }
}
