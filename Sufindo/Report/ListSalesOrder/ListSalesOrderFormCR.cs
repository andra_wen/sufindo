﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace Sufindo.Report.ListSalesOrder
{
    public partial class ListSalesOrderFormCR : Form
    {
        Connection conn;
        public ListSalesOrderFormCR()
        {
            InitializeComponent();
        }

        public ListSalesOrderFormCR(String FROMDATE, String TODATE, String SALESORDERID, String CUSTOMERID)
        {
            InitializeComponent();

            ListSalesOrderDS listsalesorderds = new ListSalesOrderDS();
            DataSet ds = listsalesorderds;

            conn = new Connection();

            List<SqlParameter> sqlParam = new List<SqlParameter>();
            if (FROMDATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", FROMDATE));
            }

            if (TODATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@TODATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@TODATE", TODATE));
            }

            if (SALESORDERID.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@SALESORDERID", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@SALESORDERID", SALESORDERID));
            }

            if (CUSTOMERID.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@CUSTOMER", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@CUSTOMER", CUSTOMERID));
            }

            conn.fillDataSetQuery(ref ds, "SELECT_DATA_SALESORDER", sqlParam, "ListSalesOrderDataTable");

            ListSalesOrderCR listsalesordercr = new ListSalesOrderCR();
            FROMDATE = FROMDATE.Equals("") ? "" : DateTime.Parse(FROMDATE).ToString("dd/MM/yyyy");
            TODATE = TODATE.Equals("") ? "" : DateTime.Parse(TODATE).ToString("dd/MM/yyyy");
            Object parameter = (FROMDATE.Equals("") || TODATE.Equals("")) ? "-" : String.Format("{0}-{1}", FROMDATE, TODATE);

            listsalesordercr.SetDataSource(ds);
            listsalesordercr.SetParameterValue(0, parameter);

            crystalReportViewer.ReportSource = listsalesordercr;
            crystalReportViewer.Refresh();
        }
    }
}
