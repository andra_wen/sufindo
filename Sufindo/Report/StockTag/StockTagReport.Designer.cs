﻿namespace Sufindo.Report.StockTag
{
    partial class StockTagReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.searchRackIDbutton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.PartNotextBox = new System.Windows.Forms.TextBox();
            this.RackIDtextBox = new System.Windows.Forms.TextBox();
            this.labelFromDate = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.searchPartNobutton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxMaxQTY = new System.Windows.Forms.TextBox();
            this.textBoxMinQTY = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // searchRackIDbutton
            // 
            this.searchRackIDbutton.BackColor = System.Drawing.Color.Transparent;
            this.searchRackIDbutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchRackIDbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchRackIDbutton.FlatAppearance.BorderSize = 0;
            this.searchRackIDbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchRackIDbutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchRackIDbutton.Location = new System.Drawing.Point(254, 33);
            this.searchRackIDbutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.searchRackIDbutton.Name = "searchRackIDbutton";
            this.searchRackIDbutton.Size = new System.Drawing.Size(24, 24);
            this.searchRackIDbutton.TabIndex = 17;
            this.searchRackIDbutton.UseVisualStyleBackColor = false;
            this.searchRackIDbutton.Click += new System.EventHandler(this.searchRackIDbutton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(163, 70);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 20);
            this.label4.TabIndex = 10;
            this.label4.Text = "Max QTY";
            // 
            // PartNotextBox
            // 
            this.PartNotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel1.SetColumnSpan(this.PartNotextBox, 2);
            this.PartNotextBox.Location = new System.Drawing.Point(79, 3);
            this.PartNotextBox.Name = "PartNotextBox";
            this.PartNotextBox.Size = new System.Drawing.Size(169, 26);
            this.PartNotextBox.TabIndex = 1;
            this.PartNotextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PartNotextBox_KeyDown);
            this.PartNotextBox.Leave += new System.EventHandler(this.PartNotextBox_Leave);
            // 
            // RackIDtextBox
            // 
            this.RackIDtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel1.SetColumnSpan(this.RackIDtextBox, 2);
            this.RackIDtextBox.Location = new System.Drawing.Point(79, 35);
            this.RackIDtextBox.Name = "RackIDtextBox";
            this.RackIDtextBox.Size = new System.Drawing.Size(169, 26);
            this.RackIDtextBox.TabIndex = 2;
            this.RackIDtextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RackIDtextBox_KeyDown);
            this.RackIDtextBox.Leave += new System.EventHandler(this.RackIDtextBox_Leave);
            // 
            // labelFromDate
            // 
            this.labelFromDate.AutoSize = true;
            this.labelFromDate.Location = new System.Drawing.Point(3, 70);
            this.labelFromDate.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.labelFromDate.Name = "labelFromDate";
            this.labelFromDate.Size = new System.Drawing.Size(70, 20);
            this.labelFromDate.TabIndex = 8;
            this.labelFromDate.Text = "Min QTY";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "Rack";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "Part No";
            // 
            // searchPartNobutton
            // 
            this.searchPartNobutton.BackColor = System.Drawing.Color.Transparent;
            this.searchPartNobutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchPartNobutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchPartNobutton.FlatAppearance.BorderSize = 0;
            this.searchPartNobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchPartNobutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchPartNobutton.Location = new System.Drawing.Point(254, 1);
            this.searchPartNobutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.searchPartNobutton.Name = "searchPartNobutton";
            this.searchPartNobutton.Size = new System.Drawing.Size(24, 24);
            this.searchPartNobutton.TabIndex = 16;
            this.searchPartNobutton.UseVisualStyleBackColor = false;
            this.searchPartNobutton.Click += new System.EventHandler(this.searchPartNobutton_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.searchPartNobutton, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelFromDate, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.RackIDtextBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.PartNotextBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.searchRackIDbutton, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBoxMaxQTY, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBoxMinQTY, 1, 2);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 1);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(334, 98);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // textBoxMaxQTY
            // 
            this.textBoxMaxQTY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel1.SetColumnSpan(this.textBoxMaxQTY, 2);
            this.textBoxMaxQTY.Location = new System.Drawing.Point(254, 67);
            this.textBoxMaxQTY.Name = "textBoxMaxQTY";
            this.textBoxMaxQTY.Size = new System.Drawing.Size(77, 26);
            this.textBoxMaxQTY.TabIndex = 18;
            this.textBoxMaxQTY.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxMaxQTY_KeyPress);
            // 
            // textBoxMinQTY
            // 
            this.textBoxMinQTY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxMinQTY.Location = new System.Drawing.Point(79, 67);
            this.textBoxMinQTY.Name = "textBoxMinQTY";
            this.textBoxMinQTY.Size = new System.Drawing.Size(78, 26);
            this.textBoxMinQTY.TabIndex = 19;
            this.textBoxMinQTY.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxMinQTY_KeyPress);
            // 
            // StoctTagReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 99);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StoctTagReport";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Stock Tag Report";
            this.Activated += new System.EventHandler(this.InventoryAuditReport_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.InventoryAuditReport_FormClosed);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button searchRackIDbutton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button searchPartNobutton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelFromDate;
        private System.Windows.Forms.TextBox RackIDtextBox;
        private System.Windows.Forms.TextBox PartNotextBox;
        private System.Windows.Forms.TextBox textBoxMaxQTY;
        private System.Windows.Forms.TextBox textBoxMinQTY;

    }
}