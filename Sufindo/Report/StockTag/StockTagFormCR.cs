﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace Sufindo.Report.StockTag
{
    public partial class StockTagFormCR : Form
    {
        Connection conn;
        public StockTagFormCR()
        {
            InitializeComponent();
        }

        public StockTagFormCR(String ITEMCODE, String RAK, String MAXQTY, String MINQTY)
        {
            InitializeComponent();

            StockTagDS listadjustmentds = new StockTagDS();
            DataSet ds = listadjustmentds;

            conn = new Connection();

            List<SqlParameter> sqlParam = new List<SqlParameter>();
            if (ITEMCODE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@ITEMCODE", DBNull.Value));
            }
            else {
                sqlParam.Add(new SqlParameter("@ITEMCODE", ITEMCODE));
            }

            if (RAK.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@RAK", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@RAK", RAK));
            }

            Object max = MAXQTY.Equals("") ? (Object)DBNull.Value : Int64.Parse(MAXQTY);
            Object min = MINQTY.Equals("") ? (Object)DBNull.Value : Int64.Parse(MINQTY);
            sqlParam.Add(new SqlParameter("@MAXQTY", max));
            sqlParam.Add(new SqlParameter("@MINQTY", min));

            conn.fillDataSetQuery(ref ds, "SELECT_DATA_STOCKTAG", sqlParam, "StockTagDataTable");

            StockTagCR stocktagCR = new StockTagCR();

            stocktagCR.SetDataSource(ds);

            crystalReportViewer.ReportSource = stocktagCR;
            crystalReportViewer.Refresh();
        }
    }
}
