﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace Sufindo.Report.ListInvoicePerCustomer
{
    public partial class ListInvoicePerCustomerFormCR : Form
    {
        Connection conn;
        public ListInvoicePerCustomerFormCR()
        {
            InitializeComponent();
        }

        public ListInvoicePerCustomerFormCR(String FROMDATE, String TODATE, String INVOICEID, String CUSTOMERID)
        {
            InitializeComponent();

            ListInvoicePerCustomerDS listinvoicepercustomerds = new ListInvoicePerCustomerDS();
            DataSet ds = listinvoicepercustomerds;

            conn = new Connection();

            List<SqlParameter> sqlParam = new List<SqlParameter>();
            if (FROMDATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", FROMDATE));
            }

            if (TODATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@TODATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@TODATE", TODATE));
            }

            if (INVOICEID.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@INVOICEID", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@INVOICEID", INVOICEID));
            }

            if (CUSTOMERID.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@CUSTOMER", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@CUSTOMER", CUSTOMERID));
            }

            conn.fillDataSetQuery(ref ds, "SELECT_DATA_INVOICECUSTOMER", sqlParam, "ListInvoicePerCustomerDataTable");

            ListInvoicePerCustomerCR listinvoicepercustomerCR = new ListInvoicePerCustomerCR();
            FROMDATE = FROMDATE.Equals("") ? "" : DateTime.Parse(FROMDATE).ToString("dd/MM/yyyy");
            TODATE = TODATE.Equals("") ? "" : DateTime.Parse(TODATE).ToString("dd/MM/yyyy");
            Object parameter = (FROMDATE.Equals("") || TODATE.Equals("")) ? "-" : String.Format("{0}-{1}", FROMDATE, TODATE);

            listinvoicepercustomerCR.SetDataSource(ds);
            listinvoicepercustomerCR.SetParameterValue(0, parameter);

            crystalReportViewer.ReportSource = listinvoicepercustomerCR;
            crystalReportViewer.Refresh();
        }
    }
}
