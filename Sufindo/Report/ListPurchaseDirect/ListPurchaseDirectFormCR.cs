﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace Sufindo.Report.ListPurchaseDirect
{
    public partial class ListPurchaseDirectFormCR : Form
    {
        Connection conn;
        public ListPurchaseDirectFormCR()
        {
            InitializeComponent();
        }

        public ListPurchaseDirectFormCR(String FROMDATE, String TODATE, String PURCHASEID, Boolean groupby)
        {
            InitializeComponent();

            ListPurchaseDirectDS listpurchasedirectds = new ListPurchaseDirectDS();
            DataSet ds = listpurchasedirectds;

            conn = new Connection();

            List<SqlParameter> sqlParam = new List<SqlParameter>();
            if (FROMDATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@STARTDATE", FROMDATE));
            }

            if (TODATE.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@TODATE", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@TODATE", TODATE));
            }

            if (PURCHASEID.Equals(""))
            {
                sqlParam.Add(new SqlParameter("@PURCHASEID", DBNull.Value));
            }
            else
            {
                sqlParam.Add(new SqlParameter("@PURCHASEID", PURCHASEID));
            }

            sqlParam.Add(new SqlParameter("@GROUPBY", groupby ? 1 : 0));

            conn.fillDataSetQuery(ref ds, "SELECT_DATA_PURCHASEDIRECT", sqlParam, "ListPurchaseDirectDataTable");

            if (groupby)
            {
                ListPurchaseDirectPerSupplierCR listpurchasedirectsupplier = new ListPurchaseDirectPerSupplierCR();
                FROMDATE = FROMDATE.Equals("") ? "" : DateTime.Parse(FROMDATE).ToString("dd/MM/yyyy");
                TODATE = TODATE.Equals("") ? "" : DateTime.Parse(TODATE).ToString("dd/MM/yyyy");
                Object parameter = (FROMDATE.Equals("") || TODATE.Equals("")) ? "-" : String.Format("{0} - {1}", FROMDATE, TODATE);

                listpurchasedirectsupplier.SetDataSource(ds);
                listpurchasedirectsupplier.SetParameterValue(0, parameter);

                crystalReportViewer.ReportSource = listpurchasedirectsupplier;
            }
            else
            {
                ListPurchaseDirectCR listpurchasedirectcr = new ListPurchaseDirectCR();
                FROMDATE = FROMDATE.Equals("") ? "" : DateTime.Parse(FROMDATE).ToString("dd/MM/yyyy");
                TODATE = TODATE.Equals("") ? "" : DateTime.Parse(TODATE).ToString("dd/MM/yyyy");
                Object parameter = (FROMDATE.Equals("") || TODATE.Equals("")) ? "-" : String.Format("{0} - {1}", FROMDATE, TODATE);

                listpurchasedirectcr.SetDataSource(ds);
                listpurchasedirectcr.SetParameterValue(0, parameter);

                crystalReportViewer.ReportSource = listpurchasedirectcr;
            }
            crystalReportViewer.Refresh();
        }
    }
}
