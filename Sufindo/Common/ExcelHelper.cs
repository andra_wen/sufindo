﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.OleDb;
using SQL = System.Data;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;
using Sufindo.Common;

namespace Sufindo
{
    class ExcelHelper
    {
        public delegate void ExcelHelperRunningProgressBar(long val, long total, String type);

        public ExcelHelperRunningProgressBar excelHelperRunningProgressBar;

        private Connection _connectionSQL = null;

        private OleDbConnection _connection = null;
        private OleDbCommand _cmd = null;
        private OleDbDataReader _datareader = null;
        private OleDbDataAdapter _dataadapter = null;
        
        private SqlBulkCopy _builkcopy = null;
        private String _connectionstring = String.Empty;
        private String _f = String.Empty;
        private XMLHelper _xmlhelper;

        public ExcelHelper() {
            
        }

        public ExcelHelper(String FilePath) {
            try
            {
                if (!FilePath.Equals(String.Empty))
                {
                    _f = FilePath;

                    _xmlhelper = new XMLHelper();
                    _xmlhelper.ConnectionStringExcel = String.Format(_xmlhelper.ConnectionStringExcel, _f);

                    _connection = new OleDbConnection(_xmlhelper.ConnectionStringExcel);
                    _connection.Close();

                    _connectionSQL = new Connection();
                }
            }
            catch (Exception)
            {
                
            }
        }

        public void importToSql(String tablename){
            try
            {
                Console.WriteLine("Starting");
                String[] column = _xmlhelper.columnsExcel(tablename);
//                String query = String.Format("SELECT {0} FROM [{1}$]", String.Join(",", column), tablename);
                String query = String.Format("SELECT * FROM [{0}$]", tablename);
                Console.WriteLine(query);
                Console.WriteLine(_connection.ConnectionString);
                _cmd = new OleDbCommand(query, _connection);
                Console.WriteLine(_cmd.CommandText);
                if (_connection.State == SQL.ConnectionState.Closed)    
                {
                    _connection.Open();
                }
                
                SQL.DataTable dt = new System.Data.DataTable();
                Console.WriteLine(dt.Rows.Count);
                _dataadapter = new OleDbDataAdapter(_cmd);
                _dataadapter.Fill(dt);
                Console.WriteLine(dt.Rows.Count);
                //MessageBox.Show(dt.Rows.Count.ToString());
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Console.WriteLine(dt.Rows[i][0].ToString());
                }
                //dbDataReader = dbcmd.ExecuteReader();
                _builkcopy = new SqlBulkCopy(_xmlhelper.ConnectionStringSql);
                _builkcopy.DestinationTableName = tablename;
                _builkcopy.NotifyAfter = 1;
                _builkcopy.SqlRowsCopied += new SqlRowsCopiedEventHandler((sender, e) => _builkcopy_SqlRowsCopied(sender, e, dt));
                _builkcopy.WriteToServer(dt);
                //System.Windows.Forms.MessageBox.Show(String.Format("SUCCESS TO IMPORT EXCEL IN SHEET {0} TO SQL SERVER ON TABLE {0}", sheet));
                _connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void exportToExcel(String tablename) {
            Excel.Application xlApp = null;
            Excel.Workbook xlWorkBook = null;
            Excel.Worksheet xlWorkSheet = null;
            object misValue = System.Reflection.Missing.Value;
            try
            {
                String[] column = _xmlhelper.columnsExcel(tablename);
                String query = String.Format("SELECT * FROM {0}", tablename);
                SQL.DataTable dt = _connectionSQL.openDataTableQuery(query);

                xlApp = new Excel.ApplicationClass();
                xlWorkBook = xlApp.Workbooks.Add(misValue);

                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                for (int i = 1; i < dt.Columns.Count + 1; i++)
                {
                    Excel.Range NewCell = (Excel.Range)xlWorkSheet.Cells[1, i];
                    NewCell.Value2 = dt.Columns[i - 1].ColumnName;
                }

                for (int i = 1; i < dt.Rows.Count; i++)
                {
                    xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                    for (int j = 1; j < dt.Columns.Count + 1; j++)
                    {
                        Console.WriteLine(j + " " + dt.Rows[i][j - 1].ToString());
                        String columnVal = dt.Rows[i][j - 1].ToString();
                        Excel.Range NewCell = (Excel.Range)xlWorkSheet.Cells[i + 2, j];
                        Console.WriteLine(columnVal);
                        if (!columnVal.Equals(""))
                        {
                            if (columnVal[0].ToString().Equals("="))
                            {
                                columnVal = String.Format("'{0}", columnVal);
                            }
                        }
                        NewCell.Value2 = columnVal;
                    }
                    excelHelperRunningProgressBar(i, dt.Rows.Count, "Exporting");
                }

                xlWorkBook.SaveAs(_f, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace);
            }
            finally {
                releaseObject(xlWorkSheet);
                releaseObject(xlWorkBook);
                releaseObject(xlApp);
            }
            
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private void _builkcopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e, System.Data.DataTable dt)
        {
            Console.WriteLine(e.RowsCopied);
            excelHelperRunningProgressBar(e.RowsCopied, dt.Rows.Count, "Importing");
        }

        public String FilePath {
            get {
                return _f;
            }
            set {
                _f = value;
            }
        }
    }
}
