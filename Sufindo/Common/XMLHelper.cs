﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;

namespace Sufindo.Common
{
    class XMLHelper
    {
        private String _pathapplication = System.Windows.Forms.Application.StartupPath;
        private String _pathConnectionXML = String.Format("{0}/{1}", System.Windows.Forms.Application.StartupPath, "Connection.xml");
        private String _pathExcelXML = String.Format("{0}/{1}", System.Windows.Forms.Application.StartupPath, "Excel.xml");
        
        private String _connectionStringSql = String.Empty;
        private String _connectionStringExcel = String.Empty;

        public XMLHelper() {
            connection();
        }

        private void connection(){
            try
            {
                XmlDocument xmlConfigurations = new XmlDocument();

                xmlConfigurations.Load(_pathConnectionXML);
                XmlElement root = xmlConfigurations.DocumentElement;

                foreach (XmlNode parentNode in root.ChildNodes)
                {
                    if (parentNode.Attributes["type"].Value.ToString().Equals("sql"))
                    {
                        ConnectionStringSql = parentNode.InnerText;
                        ConnectionStringSql = String.Format(_connectionStringSql, "sufindo", "sufindo");
                    }
                    else if (parentNode.Attributes["type"].Value.ToString().Equals("excel"))
                    {
                        ConnectionStringExcel = parentNode.InnerText;
                    }
                    //if (parentNode.Attributes["name"].Value.ToString().Equals("connectionStrings"))
                    //{
                    //    connectionStrings = parentNode.SelectSingleNode("connectionStrings").InnerText;
                    //    connectionStrings = String.Format(connectionStrings, "sufindo", "sufindo");
                    //    break;
                    //}
                }

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("[XMLHelper01]: Please Check Your Configuration xml " + ex.Message);
            }
        }

        

        public String ConnectionStringSql {
            get {
                return _connectionStringSql;
            }
            set {
                _connectionStringSql = value;
            }
        }

        public String ConnectionStringExcel {
            get {
                return _connectionStringExcel;
            }
            set {
                _connectionStringExcel = value;
            }
        }

        public String[] columnsExcel(String sheet) 
        {
            try
            {
                List<String> columns = new List<String>();
                XmlDocument xmlConfigurations = new XmlDocument();

                xmlConfigurations.Load(_pathExcelXML);
                XmlElement root = xmlConfigurations.DocumentElement;
                foreach (XmlNode parentNode in root.ChildNodes)
                {
                    if (parentNode.Attributes["name"].Value.ToString().Equals(sheet))
                    {
                        for (int i = 0; i < parentNode.ChildNodes.Count; i++)
                        {
                            columns.Add(parentNode.ChildNodes[i].FirstChild.InnerText);
                        }
                    }
                }
                return columns.ToArray();
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("[XMLHELPER02]: Please Check Your Configuration xml " + ex.Message);
                return null;
            }
        }
        public DataTable DisplayValueEntityExcel() {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Displaymember", typeof(string));
                dt.Columns.Add("Valuemember", typeof(string));

                List<String> column = new List<String>();
                XmlDocument xmlConfigurations = new XmlDocument();

                xmlConfigurations.Load(_pathExcelXML);
                XmlElement root = xmlConfigurations.DocumentElement;
                foreach (XmlNode parentNode in root.ChildNodes)
                {
                    dt.Rows.Add(parentNode.Attributes["display"].Value.ToString(), parentNode.Attributes["name"].Value.ToString());
                }
                return dt;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("[XMLHELPER03]: Please Check Your Configuration xml " + ex.Message);
                return null;
            }
        }

    }
}
