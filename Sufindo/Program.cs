﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Sufindo
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Console.WriteLine(Utilities.EncryptPassword("a"));
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                //new Login() Login.sharedInstance
                //new Report.AllReportForm()5
                Application.Run(Login.sharedInstance);
                //Application.Run(new Report.InventoryAudit.InventoryAuditReport());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //MessageBox.Show(ex.Message);
            }
           
        }
    }
}
