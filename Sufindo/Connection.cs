﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Sufindo
{
    class Connection
    {
        private SqlConnection sqlcon = null;
        private SqlDataAdapter sqldataAdapter = null;
        private SqlCommand sqlcmd = null;
        private SqlDataReader sqldataReader = null;
        private DataTable dtTable = null;
        private DataSet dtSet = null;

        Configurations config;

        public Connection()
        {
            try{
                config = null;
                config = new Configurations();
                sqlcon = new SqlConnection(config.connectionStrings);
                CloseConnection();
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Connection01]: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //MessageBox.Show("[Connection01]: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CloseConnection()
        {
            if (sqlcon.State == ConnectionState.Open)
            {
                sqlcon.Close();
            }
        }

        //public void openReaderQuery(String query)
        //{
        //    try
        //    {
        //        CloseConnection();
        //        sqlcon.Open();
        //        sqlcmd = sqlcon.CreateCommand();
        //        sqlcmd.CommandText = query;
        //        sqldataReader = sqlcmd.ExecuteReader();
        //    }
        //    catch (Exception ex)
        //    {
        //        CloseConnection();
        //        MessageBox.Show("[Connection02]: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}

        public Boolean openReaderQuery(String query)
        {
            try
            {
                CloseConnection();
                if (sqlcon.State != ConnectionState.Open)
                {
                    sqlcon.Open();
                }
                sqlcmd = sqlcon.CreateCommand();
                sqlcmd.CommandText = query;
                sqldataReader = sqlcmd.ExecuteReader();
                return true;
            }
            catch (Exception ex)
            {
                CloseConnection();
                Console.WriteLine("[Connection02]: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //MessageBox.Show("[Connection02]: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public SqlDataReader sqlDataReaders(String query)
        {
            try
            {
                CloseConnection();
                if (sqlcon.State == ConnectionState.Closed) {
                    sqlcon.Open();
                }
                sqlcmd = sqlcon.CreateCommand();
                sqlcmd.CommandText = query;
                sqldataReader = sqlcmd.ExecuteReader();
                return sqldataReader;
            }
            catch (Exception ex)
            {
                CloseConnection();
                Console.WriteLine("[Connection04]: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //MessageBox.Show("[Connection04]: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        public DataSet openDataSetQuery(String query) {
            try
            {
                CloseConnection();
                if (sqlcon.State != ConnectionState.Open)
                {
                    sqlcon.Open();
                }
                dtSet = new DataSet();
                dtSet.Clear();
                sqldataAdapter = new SqlDataAdapter(query, config.connectionStrings);
                sqldataAdapter.Fill(dtSet);
            }
            catch (Exception ex)
            {
                CloseConnection();
                Console.WriteLine("[Connection08]: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //MessageBox.Show("[Connection08]: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            return dtSet;
        }

        public void fillDataSetQuery(ref DataSet dtSet, String storeProcedureName, List<SqlParameter> sqlParam, String tables)
        {
            try
            {
                CloseConnection();
                if (sqlcon.State != ConnectionState.Open)
                {
                    sqlcon.Open();
                }
                sqlcmd = new SqlCommand(storeProcedureName, sqlcon);

                sqlcmd.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter item in sqlParam)
                {
                    sqlcmd.Parameters.Add(item);
                }
                sqldataAdapter = new SqlDataAdapter(sqlcmd);
                //sqldataAdapter = new SqlDataAdapter(query, config.connectionStrings);
                sqldataAdapter.Fill(dtSet.Tables[tables]);
            }
            catch (Exception ex)
            {
                CloseConnection();
                Console.WriteLine("[Connection08]: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //MessageBox.Show("[Connection08]: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        public DataTable openDataTableQuery(String query){
            try
            {
                CloseConnection();
                if (sqlcon.State != ConnectionState.Open)
                {
                    sqlcon.Open();
                }
                dtTable = new DataTable();
                dtTable.Clear();
                sqldataAdapter = new SqlDataAdapter(query, config.connectionStrings);
                sqldataAdapter.Fill(dtTable);
            }
            catch (Exception ex)
            {
                CloseConnection();
                Console.WriteLine("[Connection05]: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //MessageBox.Show("[Connection05]: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return dtTable;
        }

        public void bindAutoCompleteComboBox(String query,ref ComboBox combobox, String Valuemember, String Displaymember) {
            DataTable datatabel = openDataTableQuery(query);
            combobox.AutoCompleteSource = AutoCompleteSource.ListItems;
            combobox.AutoCompleteMode = AutoCompleteMode.Suggest;
            combobox.DataSource = datatabel;
            combobox.DisplayMember = Displaymember;
            combobox.ValueMember = Valuemember;
        }

        public Boolean callProcedure(String storeProcedureName, List<SqlParameter> sqlParam) {
            try
            {
                CloseConnection();
                if (sqlcon.State != ConnectionState.Open)
                {
                    sqlcon.Open();
                }
                sqlcmd = new SqlCommand(storeProcedureName, sqlcon);

                sqlcmd.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter item in sqlParam){
                    sqlcmd.Parameters.Add(item);
                }

                sqldataReader = sqlcmd.ExecuteReader();
                return (sqldataReader.RecordsAffected == -1) ? false : true;
                /*
                    cmd.Parameters.Add(new SqlParameter("@CustomerID", custId));
                 */
            }
            catch (Exception ex)
            {
                CloseConnection();
                Console.WriteLine("[Connection06]: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //MessageBox.Show("[Connection06]: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public DataTable callProcedureDatatable(String storeProcedureName, List<SqlParameter> sqlParam)
        {
            try
            {
                CloseConnection();
                if (sqlcon.State != ConnectionState.Open)
                {
                    sqlcon.Open();
                }
                sqlcmd = new SqlCommand(storeProcedureName, sqlcon);

                sqlcmd.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter item in sqlParam)
                {
                    sqlcmd.Parameters.Add(item);
                }

                dtTable = new DataTable();
                dtTable.Clear();
                sqldataAdapter = new SqlDataAdapter(sqlcmd);
                sqldataAdapter.Fill(dtTable);

                return dtTable;
                /*
                    cmd.Parameters.Add(new SqlParameter("@CustomerID", custId));
                 */
            }
            catch (Exception ex)
            {
                CloseConnection();
                Console.WriteLine("[Connection07]: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //MessageBox.Show("[Connection07]: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }
    }
}
