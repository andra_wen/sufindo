﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Sufindo
{
    public partial class ChangePassword : Form
    {
        MenuStrip menustripAction;
        Connection connection = null;
        public ChangePassword()
        {
            InitializeComponent();
        }

        public ChangePassword(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            this.menustripAction = menustripAction;
            if (connection == null) connection = new Connection();
        }

        public void save() {
            if (Utilities.isEmptyString(oldpasswordtextBox.Text)) {
                MessageBox.Show("Old Password Must Fill");
            }
            else if (!oldpasswordtextBox.Text.Equals(Utilities.DecryptPassword(AuthorizeUser.sharedInstance.userdata["PASSWORD"].ToString()))) {
                MessageBox.Show("Old Password Invalid");
            }
            else if (Utilities.isEmptyString(newpasswordtextBox.Text)) {
                MessageBox.Show("New Password Must Fill");
            }
            else if (Utilities.isEmptyString(confirmnewpasswordtextBox.Text)){
                MessageBox.Show("Confirm New Password Must Fill");
            }
            else if (!newpasswordtextBox.Text.Equals(confirmnewpasswordtextBox.Text))
            {
                MessageBox.Show("New Password and Confirm New Password Must Same");
            }
            else {
                List<SqlParameter> sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@EMPLOYEEID", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString() ));
                sqlParam.Add(new SqlParameter("@PASSWORD", Utilities.EncryptPassword(newpasswordtextBox.Text)));

                if (connection.callProcedure("UPDATE_PASSWORD", sqlParam))
                {
                    AuthorizeUser.sharedInstance.userdata["PASSWORD"] = AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString();
                    MessageBox.Show("Change Password Success");
                }
                
            }
        }

        private void ChangePassword_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            Main.ACTIVEFORM = this;
        }

        private void ChangePassword_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }
    }
}
