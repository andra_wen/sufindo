﻿namespace Sufindo
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.masterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStripAction = new System.Windows.Forms.MenuStrip();
            this.NewtoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SavetoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FindtoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EdittoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DeletetoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FirsttoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PrevtoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NexttoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LasttoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PrinttoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ClosetoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ImporttoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExporttoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ConfirmtoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CanceltoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrips = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItems = new System.Windows.Forms.ToolStripMenuItem();
            this.ChangePasswordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LogOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.masterToolStripMenuItems = new System.Windows.Forms.ToolStripMenuItem();
            this.ItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildInItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExtractOutItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.UOMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BrandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SupplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SupplierItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CurrencyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EmployeeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CustomerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseToolStripMenuItems = new System.Windows.Forms.ToolStripMenuItem();
            this.PurchaseOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.POViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReceiptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PurchaseReturnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReceiptRevisionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesToolStripMenuItems = new System.Windows.Forms.ToolStripMenuItem();
            this.SalesOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OrderPickingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.QuotationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CostToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SalesReturnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.InventoryAuditToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AdjustmentSpoilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.InventoryAuditReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orderPickingReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orderPickingWithInvoiceReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ListInvoiceReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listAdjustmentReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listSpoilReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockTagReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listPartNoByRackReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listPurchaseDirectReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listPurchaseIndirectReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listReceiptRevisionReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listInvoicePerCustomerReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listPurchaseReturnReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listInvoiceTotalReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listSalesReturnReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listSalesOrderReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listEOBIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingToolStripMenuItems = new System.Windows.Forms.ToolStripMenuItem();
            this.AuthenticationUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AuthenticationPriceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.advancedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ImportExportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStripAction.SuspendLayout();
            this.menuStrips.SuspendLayout();
            this.SuspendLayout();
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // masterToolStripMenuItem
            // 
            this.masterToolStripMenuItem.Name = "masterToolStripMenuItem";
            this.masterToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(32, 19);
            // 
            // menuStripAction
            // 
            this.menuStripAction.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.menuStripAction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NewtoolStripMenuItem,
            this.SavetoolStripMenuItem,
            this.FindtoolStripMenuItem,
            this.EdittoolStripMenuItem,
            this.DeletetoolStripMenuItem,
            this.FirsttoolStripMenuItem,
            this.PrevtoolStripMenuItem,
            this.NexttoolStripMenuItem,
            this.LasttoolStripMenuItem,
            this.PrinttoolStripMenuItem,
            this.ClosetoolStripMenuItem,
            this.ImporttoolStripMenuItem,
            this.ExporttoolStripMenuItem,
            this.ConfirmtoolStripMenuItem,
            this.CanceltoolStripMenuItem});
            this.menuStripAction.Location = new System.Drawing.Point(0, 28);
            this.menuStripAction.Name = "menuStripAction";
            this.menuStripAction.Size = new System.Drawing.Size(564, 32);
            this.menuStripAction.TabIndex = 4;
            // 
            // NewtoolStripMenuItem
            // 
            this.NewtoolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.NewtoolStripMenuItem.Image = global::Sufindo.Properties.Resources.add;
            this.NewtoolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.NewtoolStripMenuItem.Name = "NewtoolStripMenuItem";
            this.NewtoolStripMenuItem.ShortcutKeyDisplayString = "New (F2)";
            this.NewtoolStripMenuItem.Size = new System.Drawing.Size(36, 28);
            this.NewtoolStripMenuItem.Tag = "1";
            this.NewtoolStripMenuItem.ToolTipText = "New";
            this.NewtoolStripMenuItem.Click += new System.EventHandler(this.NewtoolStripMenuItem_Click);
            // 
            // SavetoolStripMenuItem
            // 
            this.SavetoolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.SavetoolStripMenuItem.Image = global::Sufindo.Properties.Resources.save;
            this.SavetoolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.SavetoolStripMenuItem.Name = "SavetoolStripMenuItem";
            this.SavetoolStripMenuItem.Size = new System.Drawing.Size(36, 28);
            this.SavetoolStripMenuItem.Tag = "2";
            this.SavetoolStripMenuItem.Click += new System.EventHandler(this.SavetoolStripMenuItem_Click);
            // 
            // FindtoolStripMenuItem
            // 
            this.FindtoolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.FindtoolStripMenuItem.Image = global::Sufindo.Properties.Resources.find;
            this.FindtoolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.FindtoolStripMenuItem.Name = "FindtoolStripMenuItem";
            this.FindtoolStripMenuItem.Size = new System.Drawing.Size(36, 28);
            this.FindtoolStripMenuItem.Tag = "3";
            this.FindtoolStripMenuItem.Click += new System.EventHandler(this.FindtoolStripMenuItem_Click);
            // 
            // EdittoolStripMenuItem
            // 
            this.EdittoolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.EdittoolStripMenuItem.Image = global::Sufindo.Properties.Resources.edit;
            this.EdittoolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.EdittoolStripMenuItem.Name = "EdittoolStripMenuItem";
            this.EdittoolStripMenuItem.Size = new System.Drawing.Size(36, 28);
            this.EdittoolStripMenuItem.Tag = "4";
            this.EdittoolStripMenuItem.Click += new System.EventHandler(this.EdittoolStripMenuItem_Click);
            // 
            // DeletetoolStripMenuItem
            // 
            this.DeletetoolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.DeletetoolStripMenuItem.Image = global::Sufindo.Properties.Resources.delete;
            this.DeletetoolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.DeletetoolStripMenuItem.Name = "DeletetoolStripMenuItem";
            this.DeletetoolStripMenuItem.Size = new System.Drawing.Size(36, 28);
            this.DeletetoolStripMenuItem.Tag = "5";
            this.DeletetoolStripMenuItem.Click += new System.EventHandler(this.DeletetoolStripMenuItem_Click);
            // 
            // FirsttoolStripMenuItem
            // 
            this.FirsttoolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.FirsttoolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FirsttoolStripMenuItem.Image = global::Sufindo.Properties.Resources.first;
            this.FirsttoolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.FirsttoolStripMenuItem.Name = "FirsttoolStripMenuItem";
            this.FirsttoolStripMenuItem.Size = new System.Drawing.Size(36, 28);
            this.FirsttoolStripMenuItem.Tag = "6";
            // 
            // PrevtoolStripMenuItem
            // 
            this.PrevtoolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.PrevtoolStripMenuItem.Image = global::Sufindo.Properties.Resources.prev;
            this.PrevtoolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.PrevtoolStripMenuItem.Name = "PrevtoolStripMenuItem";
            this.PrevtoolStripMenuItem.Size = new System.Drawing.Size(36, 28);
            this.PrevtoolStripMenuItem.Tag = "7";
            // 
            // NexttoolStripMenuItem
            // 
            this.NexttoolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.NexttoolStripMenuItem.Image = global::Sufindo.Properties.Resources.next;
            this.NexttoolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.NexttoolStripMenuItem.Name = "NexttoolStripMenuItem";
            this.NexttoolStripMenuItem.Size = new System.Drawing.Size(36, 28);
            this.NexttoolStripMenuItem.Tag = "8";
            // 
            // LasttoolStripMenuItem
            // 
            this.LasttoolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.LasttoolStripMenuItem.Image = global::Sufindo.Properties.Resources.last;
            this.LasttoolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.LasttoolStripMenuItem.Name = "LasttoolStripMenuItem";
            this.LasttoolStripMenuItem.Size = new System.Drawing.Size(36, 28);
            this.LasttoolStripMenuItem.Tag = "9";
            // 
            // PrinttoolStripMenuItem
            // 
            this.PrinttoolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.PrinttoolStripMenuItem.Image = global::Sufindo.Properties.Resources.print;
            this.PrinttoolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.PrinttoolStripMenuItem.Name = "PrinttoolStripMenuItem";
            this.PrinttoolStripMenuItem.Size = new System.Drawing.Size(36, 28);
            this.PrinttoolStripMenuItem.Tag = "10";
            this.PrinttoolStripMenuItem.Click += new System.EventHandler(this.PrinttoolStripMenuItem_Click);
            // 
            // ClosetoolStripMenuItem
            // 
            this.ClosetoolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.ClosetoolStripMenuItem.Image = global::Sufindo.Properties.Resources.close;
            this.ClosetoolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ClosetoolStripMenuItem.Name = "ClosetoolStripMenuItem";
            this.ClosetoolStripMenuItem.Size = new System.Drawing.Size(36, 28);
            this.ClosetoolStripMenuItem.Tag = "11";
            this.ClosetoolStripMenuItem.Click += new System.EventHandler(this.ClosetoolStripMenuItem_Click);
            // 
            // ImporttoolStripMenuItem
            // 
            this.ImporttoolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.ImporttoolStripMenuItem.Image = global::Sufindo.Properties.Resources.import;
            this.ImporttoolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ImporttoolStripMenuItem.Name = "ImporttoolStripMenuItem";
            this.ImporttoolStripMenuItem.Size = new System.Drawing.Size(36, 28);
            this.ImporttoolStripMenuItem.Tag = "12";
            this.ImporttoolStripMenuItem.Click += new System.EventHandler(this.ImporttoolStripMenuItem_Click);
            // 
            // ExporttoolStripMenuItem
            // 
            this.ExporttoolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.ExporttoolStripMenuItem.Image = global::Sufindo.Properties.Resources.export;
            this.ExporttoolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ExporttoolStripMenuItem.Name = "ExporttoolStripMenuItem";
            this.ExporttoolStripMenuItem.Size = new System.Drawing.Size(36, 28);
            this.ExporttoolStripMenuItem.Tag = "13";
            this.ExporttoolStripMenuItem.Click += new System.EventHandler(this.ExporttoolStripMenuItem_Click);
            // 
            // ConfirmtoolStripMenuItem
            // 
            this.ConfirmtoolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.ConfirmtoolStripMenuItem.Image = global::Sufindo.Properties.Resources.confirm;
            this.ConfirmtoolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ConfirmtoolStripMenuItem.Name = "ConfirmtoolStripMenuItem";
            this.ConfirmtoolStripMenuItem.Size = new System.Drawing.Size(36, 28);
            this.ConfirmtoolStripMenuItem.Tag = "13";
            this.ConfirmtoolStripMenuItem.Click += new System.EventHandler(this.ConfirmtoolStripMenuItem_Click);
            // 
            // CanceltoolStripMenuItem
            // 
            this.CanceltoolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.CanceltoolStripMenuItem.Image = global::Sufindo.Properties.Resources.cancel;
            this.CanceltoolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.CanceltoolStripMenuItem.Name = "CanceltoolStripMenuItem";
            this.CanceltoolStripMenuItem.Size = new System.Drawing.Size(36, 28);
            this.CanceltoolStripMenuItem.Tag = "13";
            this.CanceltoolStripMenuItem.Click += new System.EventHandler(this.CanceltoolStripMenuItem_Click);
            // 
            // menuStrips
            // 
            this.menuStrips.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.menuStrips.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItems,
            this.masterToolStripMenuItems,
            this.purchaseToolStripMenuItems,
            this.salesToolStripMenuItems,
            this.inventoryToolStripMenuItem,
            this.reportsToolStripMenuItem,
            this.settingToolStripMenuItems,
            this.advancedToolStripMenuItem});
            this.menuStrips.Location = new System.Drawing.Point(0, 0);
            this.menuStrips.Name = "menuStrips";
            this.menuStrips.Size = new System.Drawing.Size(564, 28);
            this.menuStrips.TabIndex = 5;
            // 
            // fileToolStripMenuItems
            // 
            this.fileToolStripMenuItems.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ChangePasswordToolStripMenuItem,
            this.LogOutToolStripMenuItem,
            this.ExitToolStripMenuItem});
            this.fileToolStripMenuItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileToolStripMenuItems.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.fileToolStripMenuItems.Name = "fileToolStripMenuItems";
            this.fileToolStripMenuItems.Size = new System.Drawing.Size(50, 24);
            this.fileToolStripMenuItems.Text = "File";
            // 
            // ChangePasswordToolStripMenuItem
            // 
            this.ChangePasswordToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChangePasswordToolStripMenuItem.Name = "ChangePasswordToolStripMenuItem";
            this.ChangePasswordToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.ChangePasswordToolStripMenuItem.Text = "Change Password";
            this.ChangePasswordToolStripMenuItem.Click += new System.EventHandler(this.ChangePasswordToolStripMenuItem_Click);
            // 
            // LogOutToolStripMenuItem
            // 
            this.LogOutToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LogOutToolStripMenuItem.Name = "LogOutToolStripMenuItem";
            this.LogOutToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.LogOutToolStripMenuItem.Text = "Log Out";
            this.LogOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // ExitToolStripMenuItem
            // 
            this.ExitToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem";
            this.ExitToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.ExitToolStripMenuItem.Text = "Exit";
            this.ExitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // masterToolStripMenuItems
            // 
            this.masterToolStripMenuItems.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ItemToolStripMenuItem,
            this.BuildInItemToolStripMenuItem,
            this.ExtractOutItemToolStripMenuItem,
            this.UOMToolStripMenuItem,
            this.BrandToolStripMenuItem,
            this.SupplierToolStripMenuItem,
            this.SupplierItemToolStripMenuItem,
            this.RackToolStripMenuItem,
            this.CurrencyToolStripMenuItem,
            this.EmployeeToolStripMenuItem,
            this.CustomerToolStripMenuItem});
            this.masterToolStripMenuItems.Name = "masterToolStripMenuItems";
            this.masterToolStripMenuItems.Size = new System.Drawing.Size(51, 24);
            this.masterToolStripMenuItems.Text = "Master";
            this.masterToolStripMenuItems.Visible = false;
            // 
            // ItemToolStripMenuItem
            // 
            this.ItemToolStripMenuItem.Name = "ItemToolStripMenuItem";
            this.ItemToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.ItemToolStripMenuItem.Text = "Item";
            this.ItemToolStripMenuItem.Click += new System.EventHandler(this.ItemToolStripMenuItem_Click);
            // 
            // BuildInItemToolStripMenuItem
            // 
            this.BuildInItemToolStripMenuItem.Name = "BuildInItemToolStripMenuItem";
            this.BuildInItemToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.BuildInItemToolStripMenuItem.Text = "Build In Item";
            this.BuildInItemToolStripMenuItem.Click += new System.EventHandler(this.BuildInItemToolStripMenuItem_Click);
            // 
            // ExtractOutItemToolStripMenuItem
            // 
            this.ExtractOutItemToolStripMenuItem.Name = "ExtractOutItemToolStripMenuItem";
            this.ExtractOutItemToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.ExtractOutItemToolStripMenuItem.Text = "Extract Out Item";
            this.ExtractOutItemToolStripMenuItem.Click += new System.EventHandler(this.ExtractOutItemToolStripMenuItem_Click);
            // 
            // UOMToolStripMenuItem
            // 
            this.UOMToolStripMenuItem.Name = "UOMToolStripMenuItem";
            this.UOMToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.UOMToolStripMenuItem.Text = "UOM";
            this.UOMToolStripMenuItem.Click += new System.EventHandler(this.UOMToolStripMenuItem_Click);
            // 
            // BrandToolStripMenuItem
            // 
            this.BrandToolStripMenuItem.Name = "BrandToolStripMenuItem";
            this.BrandToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.BrandToolStripMenuItem.Text = "Brand";
            this.BrandToolStripMenuItem.Click += new System.EventHandler(this.BrandToolStripMenuItem_Click);
            // 
            // SupplierToolStripMenuItem
            // 
            this.SupplierToolStripMenuItem.Name = "SupplierToolStripMenuItem";
            this.SupplierToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.SupplierToolStripMenuItem.Text = "Supplier";
            this.SupplierToolStripMenuItem.Click += new System.EventHandler(this.SupplierToolStripMenuItem_Click);
            // 
            // SupplierItemToolStripMenuItem
            // 
            this.SupplierItemToolStripMenuItem.Name = "SupplierItemToolStripMenuItem";
            this.SupplierItemToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.SupplierItemToolStripMenuItem.Text = "Supplier Item";
            this.SupplierItemToolStripMenuItem.Click += new System.EventHandler(this.SupplierItemToolStripMenuItem_Click);
            // 
            // RackToolStripMenuItem
            // 
            this.RackToolStripMenuItem.Name = "RackToolStripMenuItem";
            this.RackToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.RackToolStripMenuItem.Text = "Rack";
            this.RackToolStripMenuItem.Click += new System.EventHandler(this.RackToolStripMenuItem_Click);
            // 
            // CurrencyToolStripMenuItem
            // 
            this.CurrencyToolStripMenuItem.Name = "CurrencyToolStripMenuItem";
            this.CurrencyToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.CurrencyToolStripMenuItem.Text = "Currency";
            this.CurrencyToolStripMenuItem.Click += new System.EventHandler(this.CurrencyToolStripMenuItem_Click);
            // 
            // EmployeeToolStripMenuItem
            // 
            this.EmployeeToolStripMenuItem.Name = "EmployeeToolStripMenuItem";
            this.EmployeeToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.EmployeeToolStripMenuItem.Text = "Employee";
            this.EmployeeToolStripMenuItem.Click += new System.EventHandler(this.EmployeeToolStripMenuItem_Click);
            // 
            // CustomerToolStripMenuItem
            // 
            this.CustomerToolStripMenuItem.Name = "CustomerToolStripMenuItem";
            this.CustomerToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.CustomerToolStripMenuItem.Text = "Customer";
            this.CustomerToolStripMenuItem.Click += new System.EventHandler(this.CustomerToolStripMenuItem_Click);
            // 
            // purchaseToolStripMenuItems
            // 
            this.purchaseToolStripMenuItems.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PurchaseOrderToolStripMenuItem,
            this.POViewToolStripMenuItem,
            this.ReceiptToolStripMenuItem,
            this.PurchaseReturnToolStripMenuItem,
            this.ReceiptRevisionToolStripMenuItem});
            this.purchaseToolStripMenuItems.Name = "purchaseToolStripMenuItems";
            this.purchaseToolStripMenuItems.Size = new System.Drawing.Size(64, 24);
            this.purchaseToolStripMenuItems.Text = "Purchase";
            this.purchaseToolStripMenuItems.Visible = false;
            // 
            // PurchaseOrderToolStripMenuItem
            // 
            this.PurchaseOrderToolStripMenuItem.Name = "PurchaseOrderToolStripMenuItem";
            this.PurchaseOrderToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.PurchaseOrderToolStripMenuItem.Text = "Purchase Order";
            this.PurchaseOrderToolStripMenuItem.Click += new System.EventHandler(this.PurchaseOrderToolStripMenuItem_Click);
            // 
            // POViewToolStripMenuItem
            // 
            this.POViewToolStripMenuItem.Name = "POViewToolStripMenuItem";
            this.POViewToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.POViewToolStripMenuItem.Text = "PO View";
            this.POViewToolStripMenuItem.Click += new System.EventHandler(this.POViewToolStripMenuItem_Click);
            // 
            // ReceiptToolStripMenuItem
            // 
            this.ReceiptToolStripMenuItem.Name = "ReceiptToolStripMenuItem";
            this.ReceiptToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.ReceiptToolStripMenuItem.Text = "Receipt";
            this.ReceiptToolStripMenuItem.Click += new System.EventHandler(this.ReceiptToolStripMenuItem_Click);
            // 
            // PurchaseReturnToolStripMenuItem
            // 
            this.PurchaseReturnToolStripMenuItem.Name = "PurchaseReturnToolStripMenuItem";
            this.PurchaseReturnToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.PurchaseReturnToolStripMenuItem.Text = "Purchase Return";
            this.PurchaseReturnToolStripMenuItem.Click += new System.EventHandler(this.PurchaseReturnToolStripMenuItem_Click);
            // 
            // ReceiptRevisionToolStripMenuItem
            // 
            this.ReceiptRevisionToolStripMenuItem.Name = "ReceiptRevisionToolStripMenuItem";
            this.ReceiptRevisionToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.ReceiptRevisionToolStripMenuItem.Text = "Receipt Revision";
            this.ReceiptRevisionToolStripMenuItem.Click += new System.EventHandler(this.ReceiptRevisionToolStripMenuItem_Click);
            // 
            // salesToolStripMenuItems
            // 
            this.salesToolStripMenuItems.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SalesOrderToolStripMenuItem,
            this.OrderPickingToolStripMenuItem,
            this.QuotationToolStripMenuItem,
            this.CostToolStripMenuItem,
            this.SalesReturnToolStripMenuItem});
            this.salesToolStripMenuItems.Name = "salesToolStripMenuItems";
            this.salesToolStripMenuItems.Size = new System.Drawing.Size(45, 24);
            this.salesToolStripMenuItems.Text = "Sales";
            this.salesToolStripMenuItems.Visible = false;
            // 
            // SalesOrderToolStripMenuItem
            // 
            this.SalesOrderToolStripMenuItem.Name = "SalesOrderToolStripMenuItem";
            this.SalesOrderToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.SalesOrderToolStripMenuItem.Text = "Sales Order";
            this.SalesOrderToolStripMenuItem.Click += new System.EventHandler(this.SalesOrderToolStripMenuItem_Click);
            // 
            // OrderPickingToolStripMenuItem
            // 
            this.OrderPickingToolStripMenuItem.Name = "OrderPickingToolStripMenuItem";
            this.OrderPickingToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.OrderPickingToolStripMenuItem.Text = "Order Picking";
            this.OrderPickingToolStripMenuItem.Click += new System.EventHandler(this.OrderPickingToolStripMenuItem_Click);
            // 
            // QuotationToolStripMenuItem
            // 
            this.QuotationToolStripMenuItem.Name = "QuotationToolStripMenuItem";
            this.QuotationToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.QuotationToolStripMenuItem.Text = "Quotation";
            this.QuotationToolStripMenuItem.Click += new System.EventHandler(this.QuotationToolStripMenuItem_Click);
            // 
            // CostToolStripMenuItem
            // 
            this.CostToolStripMenuItem.Name = "CostToolStripMenuItem";
            this.CostToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.CostToolStripMenuItem.Text = "Cost";
            this.CostToolStripMenuItem.Click += new System.EventHandler(this.CostToolStripMenuItem_Click);
            // 
            // SalesReturnToolStripMenuItem
            // 
            this.SalesReturnToolStripMenuItem.Name = "SalesReturnToolStripMenuItem";
            this.SalesReturnToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.SalesReturnToolStripMenuItem.Text = "Sales Return";
            this.SalesReturnToolStripMenuItem.Click += new System.EventHandler(this.SalesReturnToolStripMenuItem_Click);
            // 
            // inventoryToolStripMenuItem
            // 
            this.inventoryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.InventoryAuditToolStripMenuItem,
            this.AdjustmentSpoilToolStripMenuItem});
            this.inventoryToolStripMenuItem.Name = "inventoryToolStripMenuItem";
            this.inventoryToolStripMenuItem.Size = new System.Drawing.Size(63, 24);
            this.inventoryToolStripMenuItem.Text = "Inventory";
            this.inventoryToolStripMenuItem.Visible = false;
            // 
            // InventoryAuditToolStripMenuItem
            // 
            this.InventoryAuditToolStripMenuItem.Name = "InventoryAuditToolStripMenuItem";
            this.InventoryAuditToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.InventoryAuditToolStripMenuItem.Text = "Inventory Audit";
            this.InventoryAuditToolStripMenuItem.Visible = false;
            this.InventoryAuditToolStripMenuItem.Click += new System.EventHandler(this.InventoryAuditToolStripMenuItem_Click);
            // 
            // AdjustmentSpoilToolStripMenuItem
            // 
            this.AdjustmentSpoilToolStripMenuItem.Name = "AdjustmentSpoilToolStripMenuItem";
            this.AdjustmentSpoilToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.AdjustmentSpoilToolStripMenuItem.Text = "Adjustment/Spoil";
            this.AdjustmentSpoilToolStripMenuItem.Click += new System.EventHandler(this.AdjustmentSpoilToolStripMenuItem_Click);
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.allReportToolStripMenuItem,
            this.InventoryAuditReportToolStripMenuItem,
            this.orderPickingReportToolStripMenuItem,
            this.orderPickingWithInvoiceReportToolStripMenuItem,
            this.ListInvoiceReportToolStripMenuItem,
            this.listAdjustmentReportToolStripMenuItem,
            this.listSpoilReportToolStripMenuItem,
            this.stockTagReportToolStripMenuItem,
            this.listPartNoByRackReportToolStripMenuItem,
            this.listPurchaseDirectReportToolStripMenuItem,
            this.listPurchaseIndirectReportToolStripMenuItem,
            this.listReceiptRevisionReportToolStripMenuItem,
            this.listInvoicePerCustomerReportToolStripMenuItem,
            this.listPurchaseReturnReportToolStripMenuItem,
            this.listInvoiceTotalReportToolStripMenuItem,
            this.listSalesReturnReportToolStripMenuItem,
            this.listSalesOrderReportToolStripMenuItem,
            this.listEOBIToolStripMenuItem});
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(51, 24);
            this.reportsToolStripMenuItem.Text = "Report";
            this.reportsToolStripMenuItem.Visible = false;
            // 
            // allReportToolStripMenuItem
            // 
            this.allReportToolStripMenuItem.Name = "allReportToolStripMenuItem";
            this.allReportToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.allReportToolStripMenuItem.Text = "All Report";
            this.allReportToolStripMenuItem.Click += new System.EventHandler(this.AllReportToolStripMenuItem_Click);
            // 
            // InventoryAuditReportToolStripMenuItem
            // 
            this.InventoryAuditReportToolStripMenuItem.Name = "InventoryAuditReportToolStripMenuItem";
            this.InventoryAuditReportToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.InventoryAuditReportToolStripMenuItem.Text = "Inventory Audit Report";
            this.InventoryAuditReportToolStripMenuItem.Click += new System.EventHandler(this.InventoryAuditReportToolStripMenuItem_Click);
            // 
            // orderPickingReportToolStripMenuItem
            // 
            this.orderPickingReportToolStripMenuItem.Name = "orderPickingReportToolStripMenuItem";
            this.orderPickingReportToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.orderPickingReportToolStripMenuItem.Text = "Order Picking Report";
            this.orderPickingReportToolStripMenuItem.Click += new System.EventHandler(this.orderPickingReportToolStripMenuItem_Click);
            // 
            // orderPickingWithInvoiceReportToolStripMenuItem
            // 
            this.orderPickingWithInvoiceReportToolStripMenuItem.Name = "orderPickingWithInvoiceReportToolStripMenuItem";
            this.orderPickingWithInvoiceReportToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.orderPickingWithInvoiceReportToolStripMenuItem.Text = "Order Picking With Invoice Report";
            this.orderPickingWithInvoiceReportToolStripMenuItem.Click += new System.EventHandler(this.orderPickingWithInvoiceReportToolStripMenuItem_Click);
            // 
            // ListInvoiceReportToolStripMenuItem
            // 
            this.ListInvoiceReportToolStripMenuItem.Name = "ListInvoiceReportToolStripMenuItem";
            this.ListInvoiceReportToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.ListInvoiceReportToolStripMenuItem.Text = "List Invoice Report";
            this.ListInvoiceReportToolStripMenuItem.Click += new System.EventHandler(this.ListInvoiceReportToolStripMenuItem_Click);
            // 
            // listAdjustmentReportToolStripMenuItem
            // 
            this.listAdjustmentReportToolStripMenuItem.Name = "listAdjustmentReportToolStripMenuItem";
            this.listAdjustmentReportToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.listAdjustmentReportToolStripMenuItem.Text = "List Adjustment Report";
            this.listAdjustmentReportToolStripMenuItem.Click += new System.EventHandler(this.listAdjustmentReportToolStripMenuItem_Click);
            // 
            // listSpoilReportToolStripMenuItem
            // 
            this.listSpoilReportToolStripMenuItem.Name = "listSpoilReportToolStripMenuItem";
            this.listSpoilReportToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.listSpoilReportToolStripMenuItem.Text = "List Spoil Report";
            this.listSpoilReportToolStripMenuItem.Click += new System.EventHandler(this.listSpoilReportToolStripMenuItem_Click);
            // 
            // stockTagReportToolStripMenuItem
            // 
            this.stockTagReportToolStripMenuItem.Name = "stockTagReportToolStripMenuItem";
            this.stockTagReportToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.stockTagReportToolStripMenuItem.Text = "Stock Tag Report";
            this.stockTagReportToolStripMenuItem.Click += new System.EventHandler(this.stockTagReportToolStripMenuItem_Click);
            // 
            // listPartNoByRackReportToolStripMenuItem
            // 
            this.listPartNoByRackReportToolStripMenuItem.Name = "listPartNoByRackReportToolStripMenuItem";
            this.listPartNoByRackReportToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.listPartNoByRackReportToolStripMenuItem.Text = "List Part No By Rack Report";
            this.listPartNoByRackReportToolStripMenuItem.Click += new System.EventHandler(this.listPartNoByRackReportToolStripMenuItem_Click);
            // 
            // listPurchaseDirectReportToolStripMenuItem
            // 
            this.listPurchaseDirectReportToolStripMenuItem.Name = "listPurchaseDirectReportToolStripMenuItem";
            this.listPurchaseDirectReportToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.listPurchaseDirectReportToolStripMenuItem.Text = "List Purchase Direct Report";
            this.listPurchaseDirectReportToolStripMenuItem.Click += new System.EventHandler(this.listPurchaseDirectReportToolStripMenuItem_Click);
            // 
            // listPurchaseIndirectReportToolStripMenuItem
            // 
            this.listPurchaseIndirectReportToolStripMenuItem.Name = "listPurchaseIndirectReportToolStripMenuItem";
            this.listPurchaseIndirectReportToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.listPurchaseIndirectReportToolStripMenuItem.Text = "List Purchase Indirect Report";
            this.listPurchaseIndirectReportToolStripMenuItem.Click += new System.EventHandler(this.listPurchaseIndirectReportToolStripMenuItem_Click);
            // 
            // listReceiptRevisionReportToolStripMenuItem
            // 
            this.listReceiptRevisionReportToolStripMenuItem.Name = "listReceiptRevisionReportToolStripMenuItem";
            this.listReceiptRevisionReportToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.listReceiptRevisionReportToolStripMenuItem.Text = "List Receipt Revision Report";
            this.listReceiptRevisionReportToolStripMenuItem.Click += new System.EventHandler(this.listReceiptRevisionReportToolStripMenuItem_Click);
            // 
            // listInvoicePerCustomerReportToolStripMenuItem
            // 
            this.listInvoicePerCustomerReportToolStripMenuItem.Name = "listInvoicePerCustomerReportToolStripMenuItem";
            this.listInvoicePerCustomerReportToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.listInvoicePerCustomerReportToolStripMenuItem.Text = "List Invoice PerCustomer Report";
            this.listInvoicePerCustomerReportToolStripMenuItem.Click += new System.EventHandler(this.listInvoicePerCustomerReportToolStripMenuItem_Click);
            // 
            // listPurchaseReturnReportToolStripMenuItem
            // 
            this.listPurchaseReturnReportToolStripMenuItem.Name = "listPurchaseReturnReportToolStripMenuItem";
            this.listPurchaseReturnReportToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.listPurchaseReturnReportToolStripMenuItem.Text = "List Purchase Return Report";
            this.listPurchaseReturnReportToolStripMenuItem.Click += new System.EventHandler(this.listPurchaseReturnReportToolStripMenuItem_Click);
            // 
            // listInvoiceTotalReportToolStripMenuItem
            // 
            this.listInvoiceTotalReportToolStripMenuItem.Name = "listInvoiceTotalReportToolStripMenuItem";
            this.listInvoiceTotalReportToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.listInvoiceTotalReportToolStripMenuItem.Text = "List Invoice Total Report";
            this.listInvoiceTotalReportToolStripMenuItem.Click += new System.EventHandler(this.listInvoiceTotalReportToolStripMenuItem_Click);
            // 
            // listSalesReturnReportToolStripMenuItem
            // 
            this.listSalesReturnReportToolStripMenuItem.Name = "listSalesReturnReportToolStripMenuItem";
            this.listSalesReturnReportToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.listSalesReturnReportToolStripMenuItem.Text = "List Sales Return Report";
            this.listSalesReturnReportToolStripMenuItem.Click += new System.EventHandler(this.listSalesReturnReportToolStripMenuItem_Click);
            // 
            // listSalesOrderReportToolStripMenuItem
            // 
            this.listSalesOrderReportToolStripMenuItem.Name = "listSalesOrderReportToolStripMenuItem";
            this.listSalesOrderReportToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.listSalesOrderReportToolStripMenuItem.Text = "List Sales Order Report";
            this.listSalesOrderReportToolStripMenuItem.Click += new System.EventHandler(this.listSalesOrderReportToolStripMenuItem_Click);
            // 
            // listEOBIToolStripMenuItem
            // 
            this.listEOBIToolStripMenuItem.Name = "listEOBIToolStripMenuItem";
            this.listEOBIToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.listEOBIToolStripMenuItem.Text = "List Extra Out / Build in Report";
            this.listEOBIToolStripMenuItem.Click += new System.EventHandler(this.listEOBIToolStripMenuItem_Click);
            // 
            // settingToolStripMenuItems
            // 
            this.settingToolStripMenuItems.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AuthenticationUserToolStripMenuItem,
            this.AuthenticationPriceToolStripMenuItem});
            this.settingToolStripMenuItems.Name = "settingToolStripMenuItems";
            this.settingToolStripMenuItems.Size = new System.Drawing.Size(52, 24);
            this.settingToolStripMenuItems.Text = "Setting";
            this.settingToolStripMenuItems.Visible = false;
            // 
            // AuthenticationUserToolStripMenuItem
            // 
            this.AuthenticationUserToolStripMenuItem.Name = "AuthenticationUserToolStripMenuItem";
            this.AuthenticationUserToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.AuthenticationUserToolStripMenuItem.Text = "Authentication User";
            this.AuthenticationUserToolStripMenuItem.Click += new System.EventHandler(this.AuthenticationUserToolStripMenuItem_Click);
            // 
            // AuthenticationPriceToolStripMenuItem
            // 
            this.AuthenticationPriceToolStripMenuItem.Name = "AuthenticationPriceToolStripMenuItem";
            this.AuthenticationPriceToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.AuthenticationPriceToolStripMenuItem.Text = "Authentication Price";
            this.AuthenticationPriceToolStripMenuItem.Click += new System.EventHandler(this.AuthenticationPriceToolStripMenuItem_Click);
            // 
            // advancedToolStripMenuItem
            // 
            this.advancedToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ImportExportToolStripMenuItem});
            this.advancedToolStripMenuItem.Name = "advancedToolStripMenuItem";
            this.advancedToolStripMenuItem.Size = new System.Drawing.Size(68, 24);
            this.advancedToolStripMenuItem.Text = "Advanced";
            this.advancedToolStripMenuItem.Visible = false;
            // 
            // ImportExportToolStripMenuItem
            // 
            this.ImportExportToolStripMenuItem.Name = "ImportExportToolStripMenuItem";
            this.ImportExportToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.ImportExportToolStripMenuItem.Text = "Import/Export";
            this.ImportExportToolStripMenuItem.Click += new System.EventHandler(this.ImportExportToolStripMenuItem_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 326);
            this.Controls.Add(this.menuStripAction);
            this.Controls.Add(this.menuStrips);
            this.IsMdiContainer = true;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SUFINDO";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_FormClosed);
            this.menuStripAction.ResumeLayout(false);
            this.menuStripAction.PerformLayout();
            this.menuStrips.ResumeLayout(false);
            this.menuStrips.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem masterToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStripAction;
        private System.Windows.Forms.ToolStripMenuItem NewtoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem FindtoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SavetoolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrips;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItems;
        private System.Windows.Forms.ToolStripMenuItem masterToolStripMenuItems;
        private System.Windows.Forms.ToolStripMenuItem ItemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem FirsttoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PrevtoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem NexttoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem LasttoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem EdittoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DeletetoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SupplierItemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SupplierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem BrandToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem UOMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseToolStripMenuItems;
        private System.Windows.Forms.ToolStripMenuItem PurchaseOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CurrencyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReceiptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem POViewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem BuildInItemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ExtractOutItemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PurchaseReturnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem EmployeeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CustomerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesToolStripMenuItems;
        private System.Windows.Forms.ToolStripMenuItem SalesOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OrderPickingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PrinttoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ClosetoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem QuotationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ChangePasswordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem LogOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ExitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CostToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SalesReturnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ExporttoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ImporttoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingToolStripMenuItems;
        private System.Windows.Forms.ToolStripMenuItem AuthenticationUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ConfirmtoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CanceltoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem advancedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AdjustmentSpoilToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ImportExportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem InventoryAuditToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AuthenticationPriceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReceiptRevisionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem InventoryAuditReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem orderPickingReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ListInvoiceReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listAdjustmentReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listSpoilReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stockTagReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem orderPickingWithInvoiceReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listPartNoByRackReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listPurchaseDirectReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listPurchaseIndirectReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listInvoicePerCustomerReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listPurchaseReturnReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listInvoiceTotalReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listSalesReturnReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listSalesOrderReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listReceiptRevisionReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listEOBIToolStripMenuItem;
    }
}

