﻿namespace Sufindo
{
    partial class UOMdatagridview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.FindtextBox = new System.Windows.Forms.TextBox();
            this.datagridviewpanel = new System.Windows.Forms.Panel();
            this.MasterUOMdataGridView = new System.Windows.Forms.DataGridView();
            this.NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INFORMATION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATEDBY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UPDATEDDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datagridviewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MasterUOMdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Find";
            // 
            // FindtextBox
            // 
            this.FindtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FindtextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FindtextBox.Location = new System.Drawing.Point(67, 4);
            this.FindtextBox.Name = "FindtextBox";
            this.FindtextBox.Size = new System.Drawing.Size(212, 31);
            this.FindtextBox.TabIndex = 3;
            this.FindtextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FindtextBox_KeyPress);
            // 
            // datagridviewpanel
            // 
            this.datagridviewpanel.Controls.Add(this.MasterUOMdataGridView);
            this.datagridviewpanel.Location = new System.Drawing.Point(3, 39);
            this.datagridviewpanel.Name = "datagridviewpanel";
            this.datagridviewpanel.Size = new System.Drawing.Size(475, 220);
            this.datagridviewpanel.TabIndex = 7;
            // 
            // MasterUOMdataGridView
            // 
            this.MasterUOMdataGridView.AllowUserToAddRows = false;
            this.MasterUOMdataGridView.AllowUserToDeleteRows = false;
            this.MasterUOMdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.MasterUOMdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NO,
            this.UOMID,
            this.UOMNAME,
            this.INFORMATION,
            this.CREATEDBY,
            this.UPDATEDDATE});
            this.MasterUOMdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MasterUOMdataGridView.Location = new System.Drawing.Point(0, 0);
            this.MasterUOMdataGridView.MultiSelect = false;
            this.MasterUOMdataGridView.Name = "MasterUOMdataGridView";
            this.MasterUOMdataGridView.ReadOnly = true;
            this.MasterUOMdataGridView.Size = new System.Drawing.Size(475, 220);
            this.MasterUOMdataGridView.TabIndex = 0;
            this.MasterUOMdataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MasterUOMdataGridView_CellDoubleClick);
            this.MasterUOMdataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.MasterUOMdataGridView_ColumnHeaderMouseClick);
            // 
            // NO
            // 
            this.NO.DataPropertyName = "NO";
            this.NO.HeaderText = "NO";
            this.NO.Name = "NO";
            this.NO.ReadOnly = true;
            this.NO.Width = 40;
            // 
            // UOMID
            // 
            this.UOMID.DataPropertyName = "UOMID";
            this.UOMID.HeaderText = "UOM ID";
            this.UOMID.Name = "UOMID";
            this.UOMID.ReadOnly = true;
            // 
            // UOMNAME
            // 
            this.UOMNAME.DataPropertyName = "UOMNAME";
            this.UOMNAME.FillWeight = 120F;
            this.UOMNAME.HeaderText = "UOM NAME";
            this.UOMNAME.Name = "UOMNAME";
            this.UOMNAME.ReadOnly = true;
            this.UOMNAME.Width = 120;
            // 
            // INFORMATION
            // 
            this.INFORMATION.DataPropertyName = "INFORMATION";
            this.INFORMATION.HeaderText = "INFORMATION";
            this.INFORMATION.Name = "INFORMATION";
            this.INFORMATION.ReadOnly = true;
            // 
            // CREATEDBY
            // 
            this.CREATEDBY.DataPropertyName = "CREATEDBY";
            this.CREATEDBY.HeaderText = "CREATEDBY";
            this.CREATEDBY.Name = "CREATEDBY";
            this.CREATEDBY.ReadOnly = true;
            this.CREATEDBY.Visible = false;
            // 
            // UPDATEDDATE
            // 
            this.UPDATEDDATE.DataPropertyName = "UPDATEDDATE";
            this.UPDATEDDATE.HeaderText = "UPDATEDDATE";
            this.UPDATEDDATE.Name = "UPDATEDDATE";
            this.UPDATEDDATE.ReadOnly = true;
            this.UPDATEDDATE.Visible = false;
            // 
            // UOMdatagridview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(480, 261);
            this.Controls.Add(this.datagridviewpanel);
            this.Controls.Add(this.FindtextBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UOMdatagridview";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "List Of UOM";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UOMdatagridview_FormClosing);
            this.datagridviewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MasterUOMdataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FindtextBox;
        private System.Windows.Forms.Panel datagridviewpanel;
        private System.Windows.Forms.DataGridView MasterUOMdataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn INFORMATION;
        private System.Windows.Forms.DataGridViewTextBoxColumn CREATEDBY;
        private System.Windows.Forms.DataGridViewTextBoxColumn UPDATEDDATE;
    }
}