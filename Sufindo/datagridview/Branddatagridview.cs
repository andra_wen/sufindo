﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sufindo.Master;

namespace Sufindo
{
    public partial class Branddatagridview : Form
    {
        public delegate void MasterBrandPassingData(DataTable datatable);

        private delegate void fillDatatableCallBack(DataTable datatable);

        public MasterBrandPassingData masterBrandPassingData;

        private readonly string[] columnMasterBrand = { "BRANDID", "BRANDNAME"};
        private Connection con;
        private String columnMasterBrandFind = "BRANDID";

        private String calledForm;

        private DataTable itemSupplierDataTable = null;

        public Branddatagridview(){
            InitializeComponent();
        }

        public Branddatagridview(String calledForm)
        {
            InitializeComponent();
            
            if (con == null) {
                con = new Connection();
            }
            this.calledForm = calledForm;
            String query =  String.Format("SELECT ROW_NUMBER() OVER (ORDER BY BRANDID) AS 'No', BRANDID, BRANDNAME, INFORMATION, CREATEDBY, UPDATEDDATE " +
                                          "FROM M_BRAND " +
                                          "WHERE [STATUS] = '{0}' " +
                                          "ORDER BY BRANDID ASC", Status.ACTIVE);
            createBackgroundWorkerFillDatagridView(query);
        }


        void createBackgroundWorkerFillDatagridView(String query) {

            for (int i = 0; i < MasterBranddataGridView.Columns.Count; i++)
            {
                if (MasterBranddataGridView.Columns[i].DataPropertyName.Equals(columnMasterBrandFind))
                {
                    DataGridViewColumn dataGridViewColumn = MasterBranddataGridView.Columns[i];
                    dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                    break;
                }
            }
            
            BackgroundWorker bgworker = new BackgroundWorker();
            bgworker.DoWork += new DoWorkEventHandler(bgworker_DoWork);
            bgworker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgworker_RunWorkerCompleted);
            bgworker.RunWorkerAsync(query);
        }

        void bgworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //MessageBox.Show("completed");
            
        }

        void bgworker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                String query = e.Argument as String;

                MasterBranddataGridView.Invoke(new fillDatatableCallBack(this.fillDatatable), con.openDataTableQuery(query));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void fillDatatable(DataTable datatable){
            if (itemSupplierDataTable == null) MasterBranddataGridView.DataSource = datatable;
            else {
                
                foreach (DataRow row in itemSupplierDataTable.Rows){
                    var query = datatable.Rows.Cast<DataRow>().Where(r => r["ITEMID"].Equals(row[1].ToString())).ToArray();
                        
                   //datatable.AsEnumerable().Where(r => r.Field<string>("ITEMID").Equals(row[1].ToString())).ToArray();
                    
                    foreach (DataRow r in query){
                        datatable.Rows.Remove(r);
                    }
                }
                Utilities.generateNoDataTable(ref datatable);

                MasterBranddataGridView.DataSource = datatable;

            }
        }

        #region Method MasterBranddataGridview

        private void MasterBranddataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (masterBrandPassingData != null)
                {
                    MasterBranddataGridViewCallPassingData(e.RowIndex);
                }
            }
        }

        private void MasterBranddataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                foreach (String item in columnMasterBrand)
                {
                    if (MasterBranddataGridView.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        for (int i = 0; i < MasterBranddataGridView.Columns.Count; i++)
                        {
                            if (MasterBranddataGridView.Columns[i].DataPropertyName.Equals(columnMasterBrandFind))
                            {
                                DataGridViewColumn dataGridViewColumn = MasterBranddataGridView.Columns[i];
                                dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Control;
                            }
                        }
                        break;
                    }
                }


                foreach (String item in columnMasterBrand)
                {
                    if (MasterBranddataGridView.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        columnMasterBrandFind = MasterBranddataGridView.Columns[e.ColumnIndex].DataPropertyName;
                        DataGridViewColumn dataGridViewColumn = MasterBranddataGridView.Columns[e.ColumnIndex];
                        dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                        break;
                    }
                }
            }
        }


        #endregion

        private void MasterBranddataGridViewCallPassingData(int currentRow) {
            DataTable datatable = new DataTable();
            for (int i = 0; i < MasterBranddataGridView.Columns.Count; i++){
                datatable.Columns.Add(MasterBranddataGridView.Columns[i].DataPropertyName);
            }
            
            DataRow r = datatable.NewRow();
            DataGridViewRow row = MasterBranddataGridView.Rows[currentRow];
            for (int i = 0; i < MasterBranddataGridView.Columns.Count; i++){
                r[i] = row.Cells[i].Value;
            }
            datatable.Rows.Add(r);
            masterBrandPassingData(datatable);
            this.Close();
        }

        #region Method FindTextBox
        private void FindtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) {
                
                String strFind = FindtextBox.Text.Contains('*') ? FindtextBox.Text.Replace('*', '%') : (FindtextBox.Text + '%');

                String query =  String.Format("SELECT ROW_NUMBER() OVER (ORDER BY BRANDID) AS 'No', BRANDID, BRANDNAME, " +
                                              "INFORMATION, CREATEDBY, UPDATEDDATE " +
                                              "FROM M_BRAND " +
                                              "WHERE {0} LIKE '{1}' AND [STATUS] = '{2}' " +
                                              "ORDER BY BRANDID ASC", columnMasterBrandFind, strFind, Status.ACTIVE);
                createBackgroundWorkerFillDatagridView(query);
            }
        }
        #endregion

        private void Branddatagridview_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }

        
        

    }
}
