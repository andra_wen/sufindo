﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sufindo.Master;

namespace Sufindo
{
    public partial class Quotationdatagridview : Form
    {
        public delegate void QuotationPassingData(DataTable Quotation);

        private delegate void fillDatatableCallBack(DataTable datatable, String called);

        public QuotationPassingData quotationPassingData;

        private readonly string[] columnQuotation = { "QUOTATIONID", "CUSTOMERNAME" };
        private Connection con;
        private String columnSalesOrderFind = "QUOTATIONID";

        private String calledForm;

        public Quotationdatagridview(){
            InitializeComponent();
        }

        public Quotationdatagridview(String calledForm)
        {
            InitializeComponent();
            if (con == null)
            {
                con = new Connection();
            }
            this.calledForm = calledForm;
            String query = "";
            if (this.calledForm.Equals("Quotation"))
            {
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY QUOTATIONID) AS 'NO', QUOTATIONID, " +
                                      "CONVERT(VARCHAR(10), QUOTATIONDATE, 111) AS [QUOTATIONDATE], " +
                                      "A.CUSTOMERID, CUSTOMERNAME, PAYMENTMETHOD, CURRENCYID, RATE, DISCOUNT, REMARK, A.STATUS, A.CREATEDBY " +
                                      "FROM H_QUOTATION A JOIN M_CUSTOMER B " +
                                      "ON A.CUSTOMERID = B.CUSTOMERID " +
                                      "WHERE (A.STATUS = '{0}' OR A.STATUS = '{1}') AND A.CREATEDBY = '{2}' " +
                                      "ORDER BY QUOTATIONID ASC", Status.READY, Status.RELEASE, AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString());
            }
            else
            {
            }
            createBackgroundWorkerFillDatagridView(query, "Quotation");
        }

        void createBackgroundWorkerFillDatagridView(String query, String called)
        {

            for (int i = 0; i < TransactionSOdatagridview.Columns.Count; i++)
            {
                if (TransactionSOdatagridview.Columns[i].DataPropertyName.Equals(columnSalesOrderFind))
                {
                    DataGridViewColumn dataGridViewColumn = TransactionSOdatagridview.Columns[i];
                    dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                    break;
                }
            }

            String[] argument = { query, called };
            
            BackgroundWorker bgworker = new BackgroundWorker();
            bgworker.DoWork += new DoWorkEventHandler(bgworker_DoWork);
            bgworker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgworker_RunWorkerCompleted);
            bgworker.RunWorkerAsync(argument);
        }

        void bgworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {

        }

        void bgworker_DoWork(object sender, DoWorkEventArgs e) {
            try
            {
                String[] obj = e.Argument as String[];
                String query = (String) obj[0];
                String called = (String) obj[1];

                BeginInvoke(new fillDatatableCallBack(this.fillDatatable), con.openDataTableQuery(query), called);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void fillDatatable(DataTable datatable, String called)
        {
            if (called.Equals("Quotation")) {
                TransactionSOdatagridview.DataSource = datatable;
                this.showTransactionSOdatagridview();
            } else if (called.Equals("TransactionQOItemdataGridView")) {
                TransactionSOItemdataGridView.DataSource = datatable;
            }
        }

        #region Method TransactionSOdatagridview
        private void TransactionSOdatagridview_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.RowIndex != -1)
            {
                if (quotationPassingData != null)
                {
                    TransactionSOdatagridviewCallPassingData(e.RowIndex);
                }
            }
        }

        private void TransactionSOdatagridview_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                foreach (String item in columnQuotation)
                {
                    if (TransactionSOdatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        for (int i = 0; i < TransactionSOdatagridview.Columns.Count; i++)
                        {
                            if (TransactionSOdatagridview.Columns[i].DataPropertyName.Equals(columnSalesOrderFind))
                            {
                                DataGridViewColumn dataGridViewColumn = TransactionSOdatagridview.Columns[i];
                                dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Control;
                            }
                        }
                        break;
                    }
                }

                foreach (String item in columnQuotation)
                {
                    if (TransactionSOdatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        columnSalesOrderFind = TransactionSOdatagridview.Columns[e.ColumnIndex].DataPropertyName;
                        DataGridViewColumn dataGridViewColumn = TransactionSOdatagridview.Columns[e.ColumnIndex];
                        dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                        break;
                    }
                }
            }
        }

        private void TransactionSOdatagridview_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                if (TransactionSOdatagridview.Rows.Count != 0)
                {
                    e.SuppressKeyPress = true;
                    int currentColumn = TransactionSOdatagridview.CurrentCell.ColumnIndex;
                    int currentRow = TransactionSOdatagridview.CurrentCell.RowIndex;
                    TransactionSOdatagridview.CurrentCell = TransactionSOdatagridview[currentColumn, currentRow];

                    TransactionSOdatagridviewCallPassingData(currentRow);
                }
            }
            else
            {
                if (!(e.KeyCode == Keys.Down || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right || e.KeyCode == Keys.Up))
                {
                    if (char.IsLetterOrDigit((char)e.KeyCode))
                    {
                        char keysCode = (char)e.KeyCode;

                        FindtextBox.Focus();
                        FindtextBox.Text = keysCode.ToString();
                        FindtextBox.SelectionStart = FindtextBox.Text.Length;
                    }
                }
                else
                {
                    int currentRow = TransactionSOdatagridview.CurrentCell.RowIndex;
                    TransactionSOdatagridview.Rows[currentRow].Selected = true;
                }
            }
        }

        private void TransactionSOdatagridview_KeyUp(object sender, KeyEventArgs e)
        {
            this.showTransactionSOdatagridview();
        }

        private void TransactionSOdatagridview_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                this.showTransactionSOdatagridview();
            }
        }

        private void showTransactionSOdatagridview() {
            if (TransactionSOdatagridview.Rows.Count == 0)
            {
                while (this.TransactionSOItemdataGridView.Rows.Count > 0)
                {
                    this.TransactionSOItemdataGridView.Rows.RemoveAt(0);
                }
                return;
            }
            int currentRow = TransactionSOdatagridview.CurrentCell.RowIndex;
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'No', A.ITEMID, B.ITEMNAME, " +
                                         "A.ITEMIDQTY,  " +
                                         "( " +
                                         "   SELECT ITEMNAME FROM M_ITEM " +
                                         "   WHERE ITEMID = A.ITEMID " +
                                         ") AS ITEMNAMEQTY, " +
                                         "A.QUANTITY, UOMID, PRICE " +
                                         "FROM D_QUOTATION A " +
                                         "JOIN M_ITEM B " +
                                         "ON A.ITEMID = B.ITEMID " +
                                         "WHERE QUOTATIONID = '{0}'", TransactionSOdatagridview.Rows[currentRow].Cells[1].Value.ToString());
            createBackgroundWorkerFillDatagridView(query, "TransactionQOItemdataGridView");
        }

        #endregion

        private void TransactionSOdatagridviewCallPassingData(int currentRow) {

            DataTable datatable = new DataTable();
            for (int i = 0; i < TransactionSOdatagridview.Columns.Count; i++){
                
                datatable.Columns.Add(TransactionSOdatagridview.Columns[i].DataPropertyName);
            }
            
            /*check multiple selected or not*/
            if (TransactionSOdatagridview.SelectedRows.Count != 0){
                foreach (var item in TransactionSOdatagridview.SelectedRows){
                    DataRow r = datatable.NewRow();
                    DataGridViewRow row = (DataGridViewRow)item;
                    for (int i = 0; i < TransactionSOdatagridview.Columns.Count; i++){
                        r[i] = row.Cells[i].Value;
                    }
                    datatable.Rows.Add(r);
                }
            }
            else{
                DataRow r = datatable.NewRow();
                DataGridViewRow row = TransactionSOdatagridview.Rows[currentRow];
                for (int i = 0; i < TransactionSOdatagridview.Columns.Count; i++){
                    r[i] = row.Cells[i].Value;
                }
                datatable.Rows.Add(r);
            }
            quotationPassingData(datatable);
            this.Close();
        }

        #region Method FindTextBox
        private void FindtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) {
                
                String strFind = FindtextBox.Text.Contains('*') ? FindtextBox.Text.Replace('*', '%') : (FindtextBox.Text + '%');
                String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY QUOTATIONID) AS 'NO', QUOTATIONID, " +
                                             "CONVERT(VARCHAR(10), QUOTATIONDATE, 111) AS [QUOTATIONDATE], " +
                                             "A.CUSTOMERID, CUSTOMERNAME, PAYMENTMETHOD, CURRENCYID, RATE, DISCOUNT, REMARK, A.STATUS, A.CREATEDBY " +
                                             "FROM H_QUOTATION A JOIN M_CUSTOMER B " +
                                             "ON A.CUSTOMERID = B.CUSTOMERID " +
                                             "WHERE {0} LIKE '{1}' AND (A.STATUS = '{2}' OR A.STATUS = '{3}') AND A.CREATEDBY = '{4}' " +
                                             "ORDER BY QUOTATIONID ASC", columnSalesOrderFind, strFind, Status.READY, Status.RELEASE, AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString());

                createBackgroundWorkerFillDatagridView(query, "Quotation");
            }
        }
        #endregion

        private void SalesOrderdatagridview_FormClosing(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }
    }
}
