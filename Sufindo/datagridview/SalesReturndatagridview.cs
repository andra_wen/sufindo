﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sufindo.Master;

namespace Sufindo
{
    public partial class SalesReturndatagridview : Form
    {
        public delegate void SalesReturnPassingData(DataTable salesOrder);

        private delegate void fillDatatableCallBack(DataTable datatable, String called);

        public SalesReturnPassingData salesreturnPassingData;

        private readonly string[] columnSalesReturn = { "SALESRETURNID", "INVOICEID", "CUSTOMERNAME" };
        private Connection con;
        private String columnSalesReturnFind = "SALESRETURNID";

        private String calledForm;

        public SalesReturndatagridview()
        {
            InitializeComponent();
        }

        public SalesReturndatagridview(String calledForm)
        {
            InitializeComponent();

            if (con == null) {
                con = new Connection();
            }
            this.calledForm = calledForm;
            String query = "";
            if(this.calledForm.Equals("SalesReturn")){
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SALESRETURNID) AS 'NO',SALESRETURNID, CONVERT(VARCHAR(10), SALESRETURNDATE, 103) AS SALESRETURNDATE, " +
                                      "A.INVOICEID, CONVERT(VARCHAR(10), INVOICEDATE, 103) AS INVOICEDATE, " +
                                      "C.CUSTOMERID, C.CUSTOMERNAME, B.CURRENCYID, B.RATE, A.REBATE, B.TOTAL AS TOTALINVOICE, A.TOTAL, A.REMARK, A.STATUS, A.CREATEDBY " +
                                      "FROM H_SALES_RETURN A " +
                                      "JOIN H_INVOICE B " +
                                      "ON A.INVOICEID = B.INVOICEID " +
                                      "JOIN M_CUSTOMER C " +
                                      "ON B.CUSTOMERID = C.CUSTOMERID " +
                                      "WHERE A.STATUS = '{0}' " +
                                      "ORDER BY SALESRETURNID ASC", Status.READY);
            }
            else if(this.calledForm.Equals("SalesReturnReport")){
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SALESRETURNID) AS 'NO',SALESRETURNID, CONVERT(VARCHAR(10), SALESRETURNDATE, 103) AS SALESRETURNDATE, " +
                                      "A.INVOICEID, CONVERT(VARCHAR(10), INVOICEDATE, 103) AS INVOICEDATE, " +
                                      "C.CUSTOMERID, C.CUSTOMERNAME, B.CURRENCYID, B.RATE, A.REBATE, B.TOTAL AS TOTALINVOICE, A.TOTAL, A.REMARK, A.STATUS, A.CREATEDBY " +
                                      "FROM H_SALES_RETURN A " +
                                      "JOIN H_INVOICE B " +
                                      "ON A.INVOICEID = B.INVOICEID " +
                                      "JOIN M_CUSTOMER C " +
                                      "ON B.CUSTOMERID = C.CUSTOMERID " +
                                      "ORDER BY SALESRETURNID ASC");
            }
            createBackgroundWorkerFillDatagridView(query, "TransactionSalesReturndatagridview");
        }

        void createBackgroundWorkerFillDatagridView(String query, String called)
        {

            for (int i = 0; i < TransactionSalesReturndatagridview.Columns.Count; i++)
            {
                if (TransactionSalesReturndatagridview.Columns[i].DataPropertyName.Equals(columnSalesReturnFind))
                {
                    DataGridViewColumn dataGridViewColumn = TransactionSalesReturndatagridview.Columns[i];
                    dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                    break;
                }
            }

            String[] argument = { query, called };
            
            BackgroundWorker bgworker = new BackgroundWorker();
            bgworker.DoWork += new DoWorkEventHandler(bgworker_DoWork);
            bgworker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgworker_RunWorkerCompleted);
            bgworker.RunWorkerAsync(argument);
        }

        void bgworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {

        }

        void bgworker_DoWork(object sender, DoWorkEventArgs e) {
            try
            {
                String[] obj = e.Argument as String[];
                String query = (String) obj[0];
                String called = (String) obj[1];

                BeginInvoke(new fillDatatableCallBack(this.fillDatatable), con.openDataTableQuery(query), called);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void fillDatatable(DataTable datatable, String called)
        {
            if (called.Equals("TransactionSalesReturndatagridview"))
            {
                TransactionSalesReturndatagridview.DataSource = datatable;
                this.showTransactionSalesReturnItemdataGridView();
            }
            else if (called.Equals("TransactionSalesReturnItemdataGridView"))
            {
                TransactionSalesReturnItemdataGridView.DataSource = datatable;
            }
        }

        #region Method TransactionSalesReturndatagridview
        private void TransactionSalesReturndatagridview_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                if (salesreturnPassingData != null)
                {
                    TransactionSalesReturndatagridviewCallPassingData(e.RowIndex);
                }
            }
        }

        private void TransactionSalesReturndatagridview_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                foreach (String item in columnSalesReturn)
                {
                    if (TransactionSalesReturndatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        for (int i = 0; i < TransactionSalesReturndatagridview.Columns.Count; i++)
                        {
                            if (TransactionSalesReturndatagridview.Columns[i].DataPropertyName.Equals(columnSalesReturnFind))
                            {
                                DataGridViewColumn dataGridViewColumn = TransactionSalesReturndatagridview.Columns[i];
                                dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Control;
                            }
                        }
                        break;
                    }
                }

                foreach (String item in columnSalesReturn)
                {
                    if (TransactionSalesReturndatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        columnSalesReturnFind = TransactionSalesReturndatagridview.Columns[e.ColumnIndex].DataPropertyName;
                        DataGridViewColumn dataGridViewColumn = TransactionSalesReturndatagridview.Columns[e.ColumnIndex];
                        dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                        break;
                    }
                }
            }
        }

        private void TransactionSalesReturndatagridview_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                this.showTransactionSalesReturnItemdataGridView();
            }
        }

        private void TransactionSalesReturndatagridview_KeyUp(object sender, KeyEventArgs e)
        {
            this.showTransactionSalesReturnItemdataGridView();
        }

        private void showTransactionSalesReturnItemdataGridView() {
            if (TransactionSalesReturndatagridview.Rows.Count == 0)
            {
                while (this.TransactionSalesReturnItemdataGridView.Rows.Count > 0)
                {
                    this.TransactionSalesReturnItemdataGridView.Rows.RemoveAt(0);
                }
                return;
            }
            int currentRow = TransactionSalesReturndatagridview.CurrentCell.RowIndex;
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'No', A.ITEMID, B.ITEMNAME, " +
                                         "A.ITEMIDQTY,  " +
                                         "( " +
                                         "   SELECT ITEMNAME FROM M_ITEM " +
                                         "   WHERE ITEMID = A.ITEMID " +
                                         ") AS ITEMNAMEQTY, " +
                                         "A.QUANTITY, UOMID, PRICE " +
                                         "FROM D_SALES_RETURN A " +
                                         "JOIN M_ITEM B " +
                                         "ON A.ITEMID = B.ITEMID " +
                                         "WHERE SALESRETURNID = '{0}'", TransactionSalesReturndatagridview.Rows[currentRow].Cells["SALESRETURNID"].Value.ToString());
            createBackgroundWorkerFillDatagridView(query, "TransactionSalesReturnItemdataGridView");
        }

        private void TransactionSalesReturndatagridview_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                if (TransactionSalesReturndatagridview.Rows.Count != 0)
                {
                    e.SuppressKeyPress = true;
                    int currentColumn = TransactionSalesReturndatagridview.CurrentCell.ColumnIndex;
                    int currentRow = TransactionSalesReturndatagridview.CurrentCell.RowIndex;
                    TransactionSalesReturndatagridview.CurrentCell = TransactionSalesReturndatagridview[currentColumn, currentRow];

                    TransactionSalesReturndatagridviewCallPassingData(currentRow);
                }
            }
            else
            {
                if (!(e.KeyCode == Keys.Down || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right || e.KeyCode == Keys.Up))
                {
                    if (char.IsLetterOrDigit((char)e.KeyCode))
                    {
                        char keysCode = (char)e.KeyCode;

                        FindtextBox.Focus();
                        FindtextBox.Text = keysCode.ToString();
                        FindtextBox.SelectionStart = FindtextBox.Text.Length;
                    }
                }
                else
                {
                    int currentRow = TransactionSalesReturndatagridview.CurrentCell.RowIndex;
                    TransactionSalesReturndatagridview.Rows[currentRow].Selected = true;
                }
            }
        }

        #endregion

        private void TransactionSalesReturndatagridviewCallPassingData(int currentRow) {

            DataTable datatable = new DataTable();
            for (int i = 0; i < TransactionSalesReturndatagridview.Columns.Count; i++){
                
                datatable.Columns.Add(TransactionSalesReturndatagridview.Columns[i].DataPropertyName);
            }
            
            /*check multiple selected or not*/
            if (TransactionSalesReturndatagridview.SelectedRows.Count != 0){
                foreach (var item in TransactionSalesReturndatagridview.SelectedRows){
                    DataRow r = datatable.NewRow();
                    DataGridViewRow row = (DataGridViewRow)item;
                    for (int i = 0; i < TransactionSalesReturndatagridview.Columns.Count; i++){
                        r[i] = row.Cells[i].Value;
                    }
                    datatable.Rows.Add(r);
                }
            }
            else{
                DataRow r = datatable.NewRow();
                DataGridViewRow row = TransactionSalesReturndatagridview.Rows[currentRow];
                for (int i = 0; i < TransactionSalesReturndatagridview.Columns.Count; i++){
                    r[i] = row.Cells[i].Value;
                }
                datatable.Rows.Add(r);
            }
            salesreturnPassingData(datatable);
            this.Close();
        }

        #region Method FindTextBox
        private void FindtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) {
                
                String strFind = FindtextBox.Text.Contains('*') ? FindtextBox.Text.Replace('*', '%') : ('%' + FindtextBox.Text + '%');
                String query = "";
                if (this.calledForm.Equals("SalesReturn")) {
                    query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SALESRETURNID) AS 'NO',SALESRETURNID,CONVERT(VARCHAR(10), SALESRETURNDATE, 103) AS SALESRETURNDATE, " +
                                          "A.INVOICEID, CONVERT(VARCHAR(10), INVOICEDATE, 103) AS INVOICEDATE, " +
                                          "C.CUSTOMERID, C.CUSTOMERNAME,B.CURRENCYID, B.RATE, A.REBATE, B.TOTAL AS TOTALINVOICE, A.TOTAL, A.REMARK, A.STATUS, A.CREATEDBY " +
                                          "FROM H_SALES_RETURN A " +
                                          "JOIN H_INVOICE B " +
                                          "ON A.INVOICEID = B.INVOICEID " +
                                          "JOIN M_CUSTOMER C " +
                                          "ON B.CUSTOMERID = C.CUSTOMERID " +
                                          "WHERE {0} LIKE '{1}' AND A.STATUS = '{2}' " +
                                          "ORDER BY SALESRETURNID ASC", columnSalesReturnFind, strFind, Status.READY);

                }
                else if (this.calledForm.Equals("SalesReturnReport")) {
                    query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SALESRETURNID) AS 'NO',SALESRETURNID,CONVERT(VARCHAR(10), SALESRETURNDATE, 103) AS SALESRETURNDATE, " +
                                          "A.INVOICEID, CONVERT(VARCHAR(10), INVOICEDATE, 103) AS INVOICEDATE, " +
                                          "C.CUSTOMERID, C.CUSTOMERNAME, B.CURRENCYID, B.RATE, A.REBATE, B.TOTAL AS TOTALINVOICE, A.TOTAL, A.REMARK, A.STATUS, A.CREATEDBY " +
                                          "FROM H_SALES_RETURN A " +
                                          "JOIN H_INVOICE B " +
                                          "ON A.INVOICEID = B.INVOICEID " +
                                          "JOIN M_CUSTOMER C " +
                                          "ON B.CUSTOMERID = C.CUSTOMERID " +
                                          "WHERE {0} LIKE '{1}' " +
                                          "ORDER BY SALESRETURNID ASC", columnSalesReturnFind, strFind);
                }
                

                createBackgroundWorkerFillDatagridView(query, "TransactionSalesReturndatagridview");
            }
        }
        #endregion

        private void SalesReturndatagridview_FormClosing(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }

    }
}
