﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sufindo.Master;

namespace Sufindo
{
    public partial class ReturnPurchasedatagridview : Form
    {
        public delegate void ReturnPurchasePassingData(DataTable masterItem);

        private delegate void fillDatatableCallBack(DataTable datatable, String called);

        public ReturnPurchasePassingData returnpurchasePassingData;

        private readonly string[] columnMasterItem = { "PURCHASERETURNID", "PURCHASERETURNID" };
        private Connection con;
        private String columnMasterItemFind = "PURCHASERETURNID";

        private String calledForm;

        public ReturnPurchasedatagridview(){
            InitializeComponent();
        }

        public ReturnPurchasedatagridview(String calledForm)
        {
            InitializeComponent();

            if (con == null) {
                con = new Connection();
            }
            this.calledForm = calledForm;
            String query = "";
            if (this.calledForm.Equals("ListPurchaseReturnReport")){
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY PURCHASERETURNID) AS 'NO', PURCHASERETURNID, CONVERT(VARCHAR(10), PURCHASERETURNDATE, 103) AS PURCHASERETURNDATE, SUPPLIERID, REMARK, " +
                                         "STATUS, CREATEDBY FROM H_PURCHASE_RETURN " +
                                         "WHERE STATUS = '{0}' " +
                                         "ORDER BY PURCHASERETURNID ASC", Status.CONFIRM);
            }
            else if (this.calledForm.Equals("PurchaseReturn")){
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY PURCHASERETURNID) AS 'NO', PURCHASERETURNID, CONVERT(VARCHAR(10), PURCHASERETURNDATE, 103) AS PURCHASERETURNDATE, SUPPLIERID, REMARK, " +
                                             "STATUS, CREATEDBY FROM H_PURCHASE_RETURN " +
                                             "WHERE STATUS = '{0}' " +
                                             "ORDER BY PURCHASERETURNID ASC", Status.ACTIVE);
            }

            createBackgroundWorkerFillDatagridView(query, "TransactionPdatagridview");
        }

        void createBackgroundWorkerFillDatagridView(String query, String called)
        {

            for (int i = 0; i < TransactionPdatagridview.Columns.Count; i++)
            {
                if (TransactionPdatagridview.Columns[i].DataPropertyName.Equals(columnMasterItemFind))
                {
                    DataGridViewColumn dataGridViewColumn = TransactionPdatagridview.Columns[i];
                    dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                    break;
                }
            }

            String[] argument = { query, called };
            
            BackgroundWorker bgworker = new BackgroundWorker();
            bgworker.DoWork += new DoWorkEventHandler(bgworker_DoWork);
            bgworker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgworker_RunWorkerCompleted);
            bgworker.RunWorkerAsync(argument);
        }

        void bgworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {

        }

        void bgworker_DoWork(object sender, DoWorkEventArgs e) {
            try
            {
                String[] obj = e.Argument as String[];
                String query = (String) obj[0];
                String called = (String)obj[1];
                BeginInvoke(new fillDatatableCallBack(this.fillDatatable), con.openDataTableQuery(query), called);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void fillDatatable(DataTable datatable, String called)
        {
            if (called.Equals("TransactionPdatagridview"))
            {
                TransactionPdatagridview.DataSource = datatable;
                this.showTransactionPOItemdataGridView();
            }
            else if (called.Equals("TransactionPItemdataGridView"))
            {
                TransactionPOItemdataGridView.DataSource = datatable;
            }

        }

        #region Method TransactionPdatagridview
        private void TransactionPdatagridview_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (returnpurchasePassingData != null)
                {
                    TransactionPdatagridviewCallPassingData(e.RowIndex);
                }
            }
        }

        private void TransactionPdatagridview_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                foreach (String item in columnMasterItem)
                {
                    if (TransactionPdatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        for (int i = 0; i < TransactionPdatagridview.Columns.Count; i++)
                        {
                            if (TransactionPdatagridview.Columns[i].DataPropertyName.Equals(columnMasterItemFind))
                            {
                                DataGridViewColumn dataGridViewColumn = TransactionPdatagridview.Columns[i];
                                dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Control;
                            }
                        }
                        break;
                    }
                }

                foreach (String item in columnMasterItem)
                {
                    if (TransactionPdatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        columnMasterItemFind = TransactionPdatagridview.Columns[e.ColumnIndex].DataPropertyName;
                        DataGridViewColumn dataGridViewColumn = TransactionPdatagridview.Columns[e.ColumnIndex];
                        dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                        break;
                    }
                }
            }
        }

        private void TransactionPdatagridview_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                if (TransactionPdatagridview.Rows.Count != 0)
                {
                    e.SuppressKeyPress = true;
                    int currentColumn = TransactionPdatagridview.CurrentCell.ColumnIndex;
                    int currentRow = TransactionPdatagridview.CurrentCell.RowIndex;
                    TransactionPdatagridview.CurrentCell = TransactionPdatagridview[currentColumn, currentRow];

                    TransactionPdatagridviewCallPassingData(currentRow);
                }
            }
            else
            {
                if (!(e.KeyCode == Keys.Down || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right || e.KeyCode == Keys.Up))
                {
                    if (char.IsLetterOrDigit((char)e.KeyCode))
                    {
                        char keysCode = (char)e.KeyCode;

                        FindtextBox.Focus();
                        FindtextBox.Text = keysCode.ToString();
                        FindtextBox.SelectionStart = FindtextBox.Text.Length;
                    }
                }
                else
                {
                    int currentRow = TransactionPdatagridview.CurrentCell.RowIndex;
                    TransactionPdatagridview.Rows[currentRow].Selected = true;
                }
            }
        }

        private void TransactionPdatagridview_KeyUp(object sender, KeyEventArgs e)
        {
            this.showTransactionPOItemdataGridView();
        }

        private void TransactionPdatagridview_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                this.showTransactionPOItemdataGridView();
            }
        }

        private void showTransactionPOItemdataGridView() {
            if (TransactionPdatagridview.Rows.Count == 0)
            {
                while (this.TransactionPOItemdataGridView.Rows.Count > 0)
                {
                    this.TransactionPOItemdataGridView.Rows.RemoveAt(0);
                }
                return;
            }
            int currentRow = TransactionPdatagridview.CurrentCell.RowIndex;
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO',A.ITEMID,  B.ITEMNAME, " +
                                         "ITEMIDQTY, A.PURCHASEID, CONVERT(VARCHAR(10), C.PURCHASEDATE, 103) AS PURCHASEDATE, " +
                                         "QUANTITYRCV, QUANTITYRTR " +
                                         "FROM D_PURCHASE_RETURN A " +
                                         "JOIN M_ITEM B " +
                                         "ON A.ITEMID = B.ITEMID " +
                                         "JOIN H_PURCHASE C " +
                                         "ON A.PURCHASEID = C.PURCHASEID " +
                                         "WHERE PURCHASERETURNID = '{0}'", TransactionPdatagridview.Rows[currentRow].Cells[1].Value.ToString());
            createBackgroundWorkerFillDatagridView(query, "TransactionPItemdataGridView");
        }

        #endregion

        private void TransactionPdatagridviewCallPassingData(int currentRow) {

            DataTable datatable = new DataTable();
            for (int i = 0; i < TransactionPdatagridview.Columns.Count; i++){
                datatable.Columns.Add(TransactionPdatagridview.Columns[i].DataPropertyName);
            }
            
            /*check multiple selected or not*/
            if (TransactionPdatagridview.SelectedRows.Count != 0){
                foreach (var item in TransactionPdatagridview.SelectedRows){
                    DataRow r = datatable.NewRow();
                    DataGridViewRow row = (DataGridViewRow)item;
                    for (int i = 0; i < TransactionPdatagridview.Columns.Count; i++){
                        r[i] = row.Cells[i].Value;
                    }
                    datatable.Rows.Add(r);
                }
            }
            else{
                DataRow r = datatable.NewRow();
                DataGridViewRow row = TransactionPdatagridview.Rows[currentRow];
                for (int i = 0; i < TransactionPdatagridview.Columns.Count; i++){
                    r[i] = row.Cells[i].Value;
                }
                datatable.Rows.Add(r);
            }
            returnpurchasePassingData(datatable);
            this.Close();
        }

        #region Method FindTextBox
        private void FindtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                String strFind = FindtextBox.Text.Contains('*') ? FindtextBox.Text.Replace('*', '%') : (FindtextBox.Text + '%');
                String query = "";
                if (this.calledForm.Equals("ListPurchaseReturnReport")){
                    query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY PURCHASERETURNID) AS 'NO', PURCHASERETURNID, CONVERT(VARCHAR(10), PURCHASERETURNDATE, 103) AS PURCHASERETURNDATE, REMARK, " +
                                          "STATUS, CREATEDBY FROM H_PURCHASE_RETURN " +
                                          "WHERE STATUS = '{0}' AND {1} LIKE '{2}' " +
                                          "ORDER BY PURCHASERETURNID ASC", Status.CONFIRM, columnMasterItemFind, strFind);
                }
                else if (this.calledForm.Equals("PurchaseReturn"))
                {
                    query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY PURCHASERETURNID) AS 'NO', PURCHASERETURNID, CONVERT(VARCHAR(10), PURCHASERETURNDATE, 103) AS PURCHASERETURNDATE, REMARK, " +
                                          "STATUS, CREATEDBY FROM H_PURCHASE_RETURN " +
                                          "WHERE STATUS = '{0}' AND {1} LIKE '{2}' " +
                                          "ORDER BY PURCHASERETURNID ASC", Status.ACTIVE, columnMasterItemFind, strFind);
                }

                createBackgroundWorkerFillDatagridView(query, "TransactionPdatagridview");
            }
        }
        #endregion
      
        private void Purchasedatagridview_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }
    }
}
