﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sufindo.Master;

namespace Sufindo
{
    public partial class Rackdatagridview : Form
    {
        public delegate void MasterRackPassingData(DataTable datatable);

        private delegate void fillDatatableCallBack(DataTable datatable);

        public MasterRackPassingData masterRackPassingData;

        private readonly string[] columnMasterRack = { "RACKID", "RACKNAME"};
        private Connection con;
        private String columnMasterRackFind = "RACKID";

        private String calledForm;

        public Rackdatagridview(){
            InitializeComponent();
        }

        public Rackdatagridview(String calledForm)
        {
            InitializeComponent();
            
            if (con == null) {
                con = new Connection();
            }
            this.calledForm = calledForm;
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY RACKID) AS 'No', RACKID, RACKNAME, INFORMATION, CREATEDBY, UPDATEDDATE " +
                                         "FROM M_RACK " +
                                         "WHERE [STATUS] = '{0}' " +
                                         "ORDER BY RACKID ASC", Status.ACTIVE);
            createBackgroundWorkerFillDatagridView(query);
            
        }

        void createBackgroundWorkerFillDatagridView(String query) {

            for (int i = 0; i < MasterRackdataGridView.Columns.Count; i++)
            {
                if (MasterRackdataGridView.Columns[i].DataPropertyName.Equals(columnMasterRackFind))
                {
                    DataGridViewColumn dataGridViewColumn = MasterRackdataGridView.Columns[i];
                    dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                    break;
                }
            }
            
            BackgroundWorker bgworker = new BackgroundWorker();
            bgworker.DoWork += new DoWorkEventHandler(bgworker_DoWork);
            bgworker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgworker_RunWorkerCompleted);
            bgworker.RunWorkerAsync(query);
        }

        void bgworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //MessageBox.Show("completed");
            
        }

        void bgworker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                String query = e.Argument as String;

                MasterRackdataGridView.Invoke(new fillDatatableCallBack(this.fillDatatable), con.openDataTableQuery(query));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void fillDatatable(DataTable datatable){
            MasterRackdataGridView.DataSource = datatable;
        }

        private void MasterRackdataGridViewCallPassingData(int currentRow)
        {
            DataTable datatable = new DataTable();
            for (int i = 0; i < MasterRackdataGridView.Columns.Count; i++)
            {
                datatable.Columns.Add(MasterRackdataGridView.Columns[i].DataPropertyName);
            }

            DataRow r = datatable.NewRow();
            DataGridViewRow row = MasterRackdataGridView.Rows[currentRow];
            for (int i = 0; i < MasterRackdataGridView.Columns.Count; i++)
            {
                r[i] = row.Cells[i].Value;
            }
            datatable.Rows.Add(r);
            masterRackPassingData(datatable);
            this.Close();
        }

        #region Method MasterRackdataGridview
       
        private void MasterRackdataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (masterRackPassingData != null)
                {
                    MasterRackdataGridViewCallPassingData(e.RowIndex);
                }
            }
        }

        private void MasterRackdataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                foreach (String item in columnMasterRack)
                {
                    if (MasterRackdataGridView.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        for (int i = 0; i < MasterRackdataGridView.Columns.Count; i++)
                        {
                            if (MasterRackdataGridView.Columns[i].DataPropertyName.Equals(columnMasterRackFind))
                            {
                                DataGridViewColumn dataGridViewColumn = MasterRackdataGridView.Columns[i];
                                dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Control;
                            }
                        }
                        break;
                    }
                }


                foreach (String item in columnMasterRack)
                {
                    if (MasterRackdataGridView.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        columnMasterRackFind = MasterRackdataGridView.Columns[e.ColumnIndex].DataPropertyName;
                        DataGridViewColumn dataGridViewColumn = MasterRackdataGridView.Columns[e.ColumnIndex];
                        dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                        break;
                    }
                }
            }
        }
        #endregion

        #region Method FindTextBox
        private void FindtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) {
                
                String strFind = FindtextBox.Text.Contains('*') ? FindtextBox.Text.Replace('*', '%') : (FindtextBox.Text + '%');

                String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY RACKID) AS 'No', RACKID, RACKNAME, " +
                                             "INFORMATION, CREATEDBY, UPDATEDDATE " +
                                             "FROM M_RACK " +
                                             "WHERE {0} LIKE '{1}' AND [STATUS] = '{2}' " +
                                             "ORDER BY RACKID ASC", columnMasterRackFind, strFind, Status.ACTIVE);
                createBackgroundWorkerFillDatagridView(query);
            }
        }
        #endregion

        private void Rackdatagridview_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }

    }
}
