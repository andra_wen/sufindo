﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sufindo.Master;

namespace Sufindo
{
    public partial class Kursdatagridview : Form
    {
        public delegate void MasterKursPassingData(DataTable datatable);

        private delegate void fillDatatableCallBack(DataTable datatable);

        public MasterKursPassingData masterKursPassingData;

        private readonly string[] columnMasterKurs = { "KURSID", "KURSNAME"};
        private Connection con;
        private String columnMasterKursFind = "KURSID";

        private String calledForm;

        public Kursdatagridview(){
            InitializeComponent();
        }

        public Kursdatagridview(String calledForm)
        {
            InitializeComponent();
            
            if (con == null) {
                con = new Connection();
            }
            this.calledForm = calledForm;
            String query =  String.Format("SELECT ROW_NUMBER() OVER (ORDER BY KURSID) AS 'No', KURSID, KURSNAME, RATE, CREATEDBY, UPDATEDDATE " +
                                          "FROM M_KURS " +
                                          "WHERE [STATUS] = '{0}' " +
                                          "ORDER BY KURSID ASC", Status.ACTIVE);
            createBackgroundWorkerFillDatagridView(query);
        }


        void createBackgroundWorkerFillDatagridView(String query) {

            for (int i = 0; i < MasterKursdataGridView.Columns.Count; i++)
            {
                if (MasterKursdataGridView.Columns[i].DataPropertyName.Equals(columnMasterKursFind))
                {
                    DataGridViewColumn dataGridViewColumn = MasterKursdataGridView.Columns[i];
                    dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                    break;
                }
            }
            
            BackgroundWorker bgworker = new BackgroundWorker();
            bgworker.DoWork += new DoWorkEventHandler(bgworker_DoWork);
            bgworker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgworker_RunWorkerCompleted);
            bgworker.RunWorkerAsync(query);
        }

        void bgworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //MessageBox.Show("completed");
            
        }

        void bgworker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                String query = e.Argument as String;

                MasterKursdataGridView.Invoke(new fillDatatableCallBack(this.fillDatatable), con.openDataTableQuery(query));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void fillDatatable(DataTable datatable){
            MasterKursdataGridView.DataSource = datatable;
        }

        #region Method MasterKursdataGridview

        private void MasterKursdataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (masterKursPassingData != null)
                {
                    MasterKursdataGridViewCallPassingData(e.RowIndex);
                }
            }
        }

        private void MasterKursdataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                foreach (String item in columnMasterKurs)
                {
                    if (MasterKursdataGridView.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        for (int i = 0; i < MasterKursdataGridView.Columns.Count; i++)
                        {
                            if (MasterKursdataGridView.Columns[i].DataPropertyName.Equals(columnMasterKursFind))
                            {
                                DataGridViewColumn dataGridViewColumn = MasterKursdataGridView.Columns[i];
                                dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Control;
                            }
                        }
                        break;
                    }
                }


                foreach (String item in columnMasterKurs)
                {
                    if (MasterKursdataGridView.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        columnMasterKursFind = MasterKursdataGridView.Columns[e.ColumnIndex].DataPropertyName;
                        DataGridViewColumn dataGridViewColumn = MasterKursdataGridView.Columns[e.ColumnIndex];
                        dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                        break;
                    }
                }
            }
        }

        #endregion

        private void MasterKursdataGridViewCallPassingData(int currentRow) {
            DataTable datatable = new DataTable();
            for (int i = 0; i < MasterKursdataGridView.Columns.Count; i++){
                datatable.Columns.Add(MasterKursdataGridView.Columns[i].DataPropertyName);
            }
            
            DataRow r = datatable.NewRow();
            DataGridViewRow row = MasterKursdataGridView.Rows[currentRow];
            for (int i = 0; i < MasterKursdataGridView.Columns.Count; i++){
                r[i] = row.Cells[i].Value;
            }
            datatable.Rows.Add(r);
            masterKursPassingData(datatable);
            this.Close();
        }

        #region Method FindTextBox
        private void FindtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) {
                
                String strFind = FindtextBox.Text.Contains('*') ? FindtextBox.Text.Replace('*', '%') : (FindtextBox.Text + '%');

                String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY KURSID) AS 'No', KURSID, KURSNAME, " +
                                              "RATE, CREATEDBY, UPDATEDDATE " +
                                              "FROM M_KURS " +
                                              "WHERE {0} LIKE '{1}' AND [STATUS] = '{2}' " +
                                              "ORDER BY KURSID ASC", columnMasterKursFind, strFind, Status.ACTIVE);
                createBackgroundWorkerFillDatagridView(query);
            }
        }
        #endregion

        private void Kursdatagridview_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }

        private void MasterKursdataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                if (MasterKursdataGridView.Rows.Count != 0)
                {
                    e.SuppressKeyPress = true;
                    int currentColumn = MasterKursdataGridView.CurrentCell.ColumnIndex;
                    int currentRow = MasterKursdataGridView.CurrentCell.RowIndex;
                    MasterKursdataGridView.CurrentCell = MasterKursdataGridView[currentColumn, currentRow];

                    MasterKursdataGridViewCallPassingData(currentRow);
                }
            }
            else
            {
                if (!(e.KeyCode == Keys.Down || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right || e.KeyCode == Keys.Up))
                {
                    if (char.IsLetterOrDigit((char)e.KeyCode))
                    {
                        char keysCode = (char)e.KeyCode;

                        FindtextBox.Focus();
                        FindtextBox.Text = keysCode.ToString();
                        FindtextBox.SelectionStart = FindtextBox.Text.Length;
                    }
                }
                else
                {
                    int currentRow = MasterKursdataGridView.CurrentCell.RowIndex;
                    MasterKursdataGridView.Rows[currentRow].Selected = true;
                }
            }
        }

    }
}
