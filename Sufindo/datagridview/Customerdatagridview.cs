﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sufindo.Master;

namespace Sufindo
{
    public partial class Customerdatagridview : Form
    {
        public delegate void MasterCustomerPassingData(DataTable masterCustomer);

        private delegate void fillDatatableCallBack(DataTable datatable, String called);

        public MasterCustomerPassingData masterCustomerPassingData;

        private readonly string[] columnMasterCustomer = { "CUSTOMERID", "CUSTOMERNAME"};
        private Connection con;
        private String columnMasterCustomerFind = "CUSTOMERID";

        private String calledForm;

        public Customerdatagridview(){
            InitializeComponent();
        }

        public Customerdatagridview(String calledForm)
        {
            InitializeComponent();

            if (con == null) {
                con = new Connection();
            }
            this.calledForm = calledForm;
            String query =  String.Format("SELECT ROW_NUMBER() OVER (ORDER BY CUSTOMERID) AS 'No', CUSTOMERID, CUSTOMERNAME, " +
                                          "[ADDRESS], ZONE, NOTELP, FAX, EMAIL, CONTACTPERSON, NOHP, SALESID, STATUS, CREATEDBY " +
                                          "FROM M_CUSTOMER WHERE STATUS = '{0}' " +
                                          "ORDER BY CUSTOMERID ASC",Status.ACTIVE);
            createBackgroundWorkerFillDatagridView(query, "MasterCustomerdatagridview");
        }

        void createBackgroundWorkerFillDatagridView(String query, String called)
        {

            for (int i = 0; i < MasterCustomerdatagridview.Columns.Count; i++)
            {
                if (MasterCustomerdatagridview.Columns[i].DataPropertyName.Equals(columnMasterCustomerFind))
                {
                    DataGridViewColumn dataGridViewColumn = MasterCustomerdatagridview.Columns[i];
                    dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                    break;
                }
            }

            String[] argument = { query, called };
            
            BackgroundWorker bgworker = new BackgroundWorker();
            bgworker.DoWork += new DoWorkEventHandler(bgworker_DoWork);
            bgworker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgworker_RunWorkerCompleted);
            bgworker.RunWorkerAsync(argument);
        }

        void bgworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {

        }

        void bgworker_DoWork(object sender, DoWorkEventArgs e) {
            try
            {
                String[] obj = e.Argument as String[];
                String query = (String) obj[0];
                String called = (String) obj[1];

                BeginInvoke(new fillDatatableCallBack(this.fillDatatable), con.openDataTableQuery(query), called);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void fillDatatable(DataTable datatable, String called)
        {
            if (called.Equals("MasterCustomerdatagridview")) MasterCustomerdatagridview.DataSource = datatable;
        }

        #region Method MasterCustomerdatagridview
        private void MasterCustomerdatagridview_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (masterCustomerPassingData != null)
                {
                    MasterCustomerdatagridviewCallPassingData(e.RowIndex);
                }
            }
        }

        private void MasterCustomerdatagridview_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                foreach (String item in columnMasterCustomer)
                {
                    if (MasterCustomerdatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        for (int i = 0; i < MasterCustomerdatagridview.Columns.Count; i++)
                        {
                            if (MasterCustomerdatagridview.Columns[i].DataPropertyName.Equals(columnMasterCustomerFind))
                            {
                                DataGridViewColumn dataGridViewColumn = MasterCustomerdatagridview.Columns[i];
                                dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Control;
                            }
                        }
                        break;
                    }
                }

                foreach (String item in columnMasterCustomer)
                {
                    if (MasterCustomerdatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        columnMasterCustomerFind = MasterCustomerdatagridview.Columns[e.ColumnIndex].DataPropertyName;
                        DataGridViewColumn dataGridViewColumn = MasterCustomerdatagridview.Columns[e.ColumnIndex];
                        dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                        break;
                    }
                }
            }
        }

        private void MasterCustomerdatagridview_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                if (MasterCustomerdatagridview.Rows.Count != 0)
                {
                    e.SuppressKeyPress = true;
                    int currentColumn = MasterCustomerdatagridview.CurrentCell.ColumnIndex;
                    int currentRow = MasterCustomerdatagridview.CurrentCell.RowIndex;
                    MasterCustomerdatagridview.CurrentCell = MasterCustomerdatagridview[currentColumn, currentRow];

                    MasterCustomerdatagridviewCallPassingData(currentRow);
                }
            }
            else
            {
                if (!(e.KeyCode == Keys.Down || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right || e.KeyCode == Keys.Up))
                {
                    if (char.IsLetterOrDigit((char)e.KeyCode))
                    {
                        char keysCode = (char)e.KeyCode;

                        FindtextBox.Focus();
                        FindtextBox.Text = keysCode.ToString();
                        FindtextBox.SelectionStart = FindtextBox.Text.Length;
                    }
                }
                else
                {
                    int currentRow = MasterCustomerdatagridview.CurrentCell.RowIndex;
                    MasterCustomerdatagridview.Rows[currentRow].Selected = true;
                }
            }
        }

        #endregion

        private void MasterCustomerdatagridviewCallPassingData(int currentRow) {

            
            DataTable datatable = new DataTable();
            for (int i = 0; i < MasterCustomerdatagridview.Columns.Count; i++){
                datatable.Columns.Add(MasterCustomerdatagridview.Columns[i].DataPropertyName);
            }
            
            /*check multiple selected or not*/
            if (MasterCustomerdatagridview.SelectedRows.Count != 0){
                foreach (var item in MasterCustomerdatagridview.SelectedRows){
                    DataRow r = datatable.NewRow();
                    DataGridViewRow row = (DataGridViewRow)item;
                    for (int i = 0; i < MasterCustomerdatagridview.Columns.Count; i++){
                        r[i] = row.Cells[i].Value;
                    }
                    datatable.Rows.Add(r);
                }
                Boolean flagColumn = false;
                foreach (DataColumn column in datatable.Columns)
                {
                    if (column.ColumnName.Equals("CONTACTPERSON"))
                    {
                        flagColumn = true;
                        break;
                    }
                }
                if (!flagColumn) datatable.Columns.Add("CONTACTPERSON");
            }
            else{
                DataRow r = datatable.NewRow();
                DataGridViewRow row = MasterCustomerdatagridview.Rows[currentRow];
                for (int i = 0; i < MasterCustomerdatagridview.Columns.Count; i++){
                    r[i] = row.Cells[i].Value;
                }
                datatable.Rows.Add(r);
                
            }
            masterCustomerPassingData(datatable);
            this.Close();
        }

        #region Method FindTextBox
        private void FindtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) {
                
                String strFind = FindtextBox.Text.Contains('*') ? FindtextBox.Text.Replace('*', '%') : (FindtextBox.Text + '%');

                String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY CUSTOMERID) AS 'No', CUSTOMERID, CUSTOMERNAME, " +
                                             "[ADDRESS], ZONE, NOTELP, FAX, EMAIL, CONTACTPERSON, NOHP, SALESID, STATUS, CREATEDBY " +
                                             "FROM M_CUSTOMER " +
                                             "WHERE {0} LIKE '{1}' AND STATUS = '{2}' " +
                                             "ORDER BY CUSTOMERID ASC", columnMasterCustomerFind, strFind, Status.ACTIVE);
                
                createBackgroundWorkerFillDatagridView(query, "MasterCustomerdatagridview");
            }
        }

        #endregion

        private void Supplierdatagridview_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }

    }
}
