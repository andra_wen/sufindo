﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sufindo.Master;

namespace Sufindo
{
    public partial class OrderPickingdatagridview : Form
    {
        public delegate void OrderPickingPassingData(DataTable orderPicking);

        private delegate void fillDatatableCallBack(DataTable datatable, String called);

        public OrderPickingPassingData salesOrderPassingData;

        private readonly string[] columnOrderPicking = { "ORDERPICKINGID", "SALESORDERID", "CUSTOMERNAME" };
        private Connection con;
        private String columnOrderPickingFind = "ORDERPICKINGID";

        private String calledForm;

        public OrderPickingdatagridview(){
            InitializeComponent();
        }

        public OrderPickingdatagridview(String calledForm)
        {
            InitializeComponent();

            if (con == null) {
                con = new Connection();
            }
            this.calledForm = calledForm;
            String query = "";
            if(this.calledForm.Equals("OrderPicking")){
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY ORDERPICKINGID) AS 'NO', ORDERPICKINGID, " +
                                      "CONVERT(VARCHAR(10), [ORDERPICKINGDATE], 103) AS [ORDERPICKINGDATE], A.SALESORDERID, " +
                                      "CONVERT(VARCHAR(10), SALESORDERDATE, 103) AS [SALESORDERDATE], " +
                                      "CUSTOMERNAME, A.STATUS, A.CREATEDBY " +
                                      "FROM H_ORDER_PICKING A " +
                                      "JOIN H_SALES_ORDER B " +
                                      "ON A.SALESORDERID = B.SALESORDERID " +
                                      "JOIN M_CUSTOMER C ON B.CUSTOMERID = C.CUSTOMERID " +
                                      "WHERE A.STATUS = '{0}' OR A.STATUS = '{1}'", Status.OPEN, Status.CANCEL);
            }
            else if (this.calledForm.Equals("OrderPickingReport"))
            {
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY ORDERPICKINGID) AS 'NO', ORDERPICKINGID, " +
                                      "CONVERT(VARCHAR(10), [ORDERPICKINGDATE], 103) AS [ORDERPICKINGDATE], A.SALESORDERID, " +
                                      "CONVERT(VARCHAR(10), SALESORDERDATE, 103) AS [SALESORDERDATE], " +
                                      "CUSTOMERNAME, A.STATUS, A.CREATEDBY " +
                                      "FROM H_ORDER_PICKING A " +
                                      "JOIN H_SALES_ORDER B " +
                                      "JOIN M_CUSTOMER C ON B.CUSTOMERID = C.CUSTOMERID " +
                                      "ON A.SALESORDERID = B.SALESORDERID ");
            }
            createBackgroundWorkerFillDatagridView(query, "TransactionOPdatagridview");
        }

        void createBackgroundWorkerFillDatagridView(String query, String called)
        {

            for (int i = 0; i < TransactionOPdatagridview.Columns.Count; i++)
            {
                if (TransactionOPdatagridview.Columns[i].DataPropertyName.Equals(columnOrderPickingFind))
                {
                    DataGridViewColumn dataGridViewColumn = TransactionOPdatagridview.Columns[i];
                    dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                    break;
                }
            }

            String[] argument = { query, called };
            
            BackgroundWorker bgworker = new BackgroundWorker();
            bgworker.DoWork += new DoWorkEventHandler(bgworker_DoWork);
            bgworker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgworker_RunWorkerCompleted);
            bgworker.RunWorkerAsync(argument);
        }

        void bgworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {

        }

        void bgworker_DoWork(object sender, DoWorkEventArgs e) {
            try
            {
                String[] obj = e.Argument as String[];
                String query = (String) obj[0];
                String called = (String) obj[1];

                BeginInvoke(new fillDatatableCallBack(this.fillDatatable), con.openDataTableQuery(query), called);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void fillDatatable(DataTable datatable, String called)
        {
            if (called.Equals("TransactionOPdatagridview"))
            {
                TransactionOPdatagridview.DataSource = datatable;
                this.showTransactionOPItemdataGridView();
            }
            else if (called.Equals("TransactionOPItemdataGridView"))
            {
                TransactionOPItemdataGridView.DataSource = datatable;
            }
        }

        #region Method TransactionSOdatagridview
        private void TransactionOPdatagridview_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (salesOrderPassingData != null)
                {
                    TransactionSOdatagridviewCallPassingData(e.RowIndex);
                }
            }
        }

        private void TransactionOPdatagridview_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                foreach (String item in columnOrderPicking)
                {
                    if (TransactionOPdatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        for (int i = 0; i < TransactionOPdatagridview.Columns.Count; i++)
                        {
                            if (TransactionOPdatagridview.Columns[i].DataPropertyName.Equals(columnOrderPickingFind))
                            {
                                DataGridViewColumn dataGridViewColumn = TransactionOPdatagridview.Columns[i];
                                dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Control;
                            }
                        }
                        break;
                    }
                }

                foreach (String item in columnOrderPicking)
                {
                    if (TransactionOPdatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        columnOrderPickingFind = TransactionOPdatagridview.Columns[e.ColumnIndex].DataPropertyName;
                        DataGridViewColumn dataGridViewColumn = TransactionOPdatagridview.Columns[e.ColumnIndex];
                        dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                        break;
                    }
                }
            }
        }


        private void TransactionOPdatagridview_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                if (TransactionOPdatagridview.Rows.Count != 0)
                {
                    e.SuppressKeyPress = true;
                    int currentColumn = TransactionOPdatagridview.CurrentCell.ColumnIndex;
                    int currentRow = TransactionOPdatagridview.CurrentCell.RowIndex;
                    TransactionOPdatagridview.CurrentCell = TransactionOPdatagridview[currentColumn, currentRow];

                    TransactionSOdatagridviewCallPassingData(currentRow); 
                }
            }
            else
            {
                if (!(e.KeyCode == Keys.Down || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right || e.KeyCode == Keys.Up))
                {
                    if (char.IsLetterOrDigit((char)e.KeyCode))
                    {
                        char keysCode = (char)e.KeyCode;

                        FindtextBox.Focus();
                        FindtextBox.Text = keysCode.ToString();
                        FindtextBox.SelectionStart = FindtextBox.Text.Length;
                    }
                }
                else
                {
                    int currentRow = TransactionOPdatagridview.CurrentCell.RowIndex;
                    TransactionOPdatagridview.Rows[currentRow].Selected = true;
                }
            }
        }

        private void TransactionOPdatagridview_KeyUp(object sender, KeyEventArgs e)
        {
            this.showTransactionOPItemdataGridView();
        }

        private void TransactionOPdatagridview_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                this.showTransactionOPItemdataGridView();
            }
        }

        private void showTransactionOPItemdataGridView() {
            if (TransactionOPdatagridview.Rows.Count == 0)
            {
                while (this.TransactionOPItemdataGridView.Rows.Count > 0)
                {
                    this.TransactionOPItemdataGridView.Rows.RemoveAt(0);
                }
                return;
            }
            int currentRow = TransactionOPdatagridview.CurrentCell.RowIndex;
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'No', A.ITEMID, B.ITEMNAME, " +
                                         "A.ITEMIDQTY,  " +
                                         "( " +
                                         "   SELECT ITEMNAME FROM M_ITEM " +
                                         "   WHERE ITEMID = A.ITEMID " +
                                         ") AS ITEMNAMEQTY, " +
                                         "A.QUANTITY, UOMID " +
                                         "FROM D_ORDER_PICKING A " +
                                         "JOIN M_ITEM B " +
                                         "ON A.ITEMID = B.ITEMID " +
                                         "WHERE ORDERPICKINGID = '{0}'", TransactionOPdatagridview.Rows[currentRow].Cells["ORDERPICKINGID"].Value.ToString());
            createBackgroundWorkerFillDatagridView(query, "TransactionOPItemdataGridView");
        }

        #endregion

        private void TransactionSOdatagridviewCallPassingData(int currentRow) {

            DataTable datatable = new DataTable();
            for (int i = 0; i < TransactionOPdatagridview.Columns.Count; i++){
                
                datatable.Columns.Add(TransactionOPdatagridview.Columns[i].DataPropertyName);
            }
            
            /*check multiple selected or not*/
            if (TransactionOPdatagridview.SelectedRows.Count != 0){
                foreach (var item in TransactionOPdatagridview.SelectedRows){
                    DataRow r = datatable.NewRow();
                    DataGridViewRow row = (DataGridViewRow)item;
                    for (int i = 0; i < TransactionOPdatagridview.Columns.Count; i++){
                        r[i] = row.Cells[i].Value;
                    }
                    datatable.Rows.Add(r);
                }
            }
            else{
                DataRow r = datatable.NewRow();
                DataGridViewRow row = TransactionOPdatagridview.Rows[currentRow];
                for (int i = 0; i < TransactionOPdatagridview.Columns.Count; i++){
                    r[i] = row.Cells[i].Value;
                }
                datatable.Rows.Add(r);
            }
            salesOrderPassingData(datatable);
            this.Close();
        }

        #region Method FindTextBox
        private void FindtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) {
                
                String strFind = FindtextBox.Text.Contains('*') ? FindtextBox.Text.Replace('*', '%') : ('%' + FindtextBox.Text + '%');
                String query = "";
                if (this.calledForm.Equals("OrderPicking"))
                {
                    query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY ORDERPICKINGID) AS 'NO', ORDERPICKINGID, " +
                                          "CONVERT(VARCHAR(10), [ORDERPICKINGDATE], 103) AS [ORDERPICKINGDATE], A.SALESORDERID, " +
                                          "CONVERT(VARCHAR(10), SALESORDERDATE, 103) AS [SALESORDERDATE], " +
                                          "CUSTOMERNAME, A.STATUS, A.CREATEDBY " +
                                          "FROM H_ORDER_PICKING A " +
                                          "JOIN H_SALES_ORDER B " +
                                          "ON A.SALESORDERID = B.SALESORDERID " +
                                          "JOIN M_CUSTOMER C ON B.CUSTOMERID = C.CUSTOMERID " +
                                          "WHERE A.{0} LIKE '{1}' AND (A.STATUS = '{2}' OR A.STATUS = '{3}')" +
                                          "ORDER BY A.ORDERPICKINGID ASC", columnOrderPickingFind, strFind, Status.OPEN, Status.CANCEL);
                }
                else if (this.calledForm.Equals("OrderPickingReport"))
                {
                    query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY ORDERPICKINGID) AS 'NO', ORDERPICKINGID, " +
                                          "CONVERT(VARCHAR(10), [ORDERPICKINGDATE], 103) AS [ORDERPICKINGDATE], A.SALESORDERID, " +
                                          "CONVERT(VARCHAR(10), SALESORDERDATE, 103) AS [SALESORDERDATE], " +
                                          "CUSTOMERNAME, A.STATUS, A.CREATEDBY " +
                                          "FROM H_ORDER_PICKING A " +
                                          "JOIN H_SALES_ORDER B " +
                                          "ON A.SALESORDERID = B.SALESORDERID " +
                                          "JOIN M_CUSTOMER C ON B.CUSTOMERID = C.CUSTOMERID " +
                                          "WHERE A.{0} LIKE '{1}' " +
                                          "ORDER BY A.ORDERPICKINGID ASC", columnOrderPickingFind, strFind);
                }

                createBackgroundWorkerFillDatagridView(query, "TransactionOPdatagridview");
            }
        }
        #endregion

        private void OrderPickingdatagridview_FormClosing(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }

    }
}
