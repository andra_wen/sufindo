﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sufindo.Master;

namespace Sufindo
{
    public partial class AdjustSpoildatagridview : Form
    {
        public delegate void AdjustmentSpoilPassingData(DataTable adjustmentspoil);

        private delegate void fillDatatableCallBack(DataTable datatable, String called);

        public AdjustmentSpoilPassingData adjustmentSpoilPassingData;

        private readonly string[] columnAdjustmentSpoil = new string[2];
        private Connection con;
        private String columnAdjustmentSpoilFind = "";

        private String calledForm;

        public AdjustSpoildatagridview(){
            InitializeComponent();
        }

        public AdjustSpoildatagridview(String calledForm)
        {
            InitializeComponent();

            if (con == null) {
                con = new Connection();
            }
            this.calledForm = calledForm;
            String query = "";
            if(this.calledForm.Equals("Adjustment")){
                addColumnsForAdjustmentBuild();
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY ADJUSTMENTID) AS 'NO', ADJUSTMENTID, " +
                                      "CONVERT(VARCHAR(10), ADJUSTMENTDATE, 103) AS ADJUSTMENTDATE, REMARK, STATUS, CREATEDBY " +
                                      "FROM H_ADJUSTMENT_ITEM " +
                                      "ORDER BY ADJUSTMENTID ASC");
                                      //"WHERE STATUS = '{0}' " +
                                      //"ORDER BY ADJUSTMENTID ASC", Status.READY);
                columnAdjustmentSpoil[0] = "ADJUSTMENTID";
                columnAdjustmentSpoil[1] = "CREATEDBY";
                columnAdjustmentSpoilFind = "ADJUSTMENTID";
            }
            else if (this.calledForm.Equals("Spoil"))
            {
                addColumnsForSpoilBuild();
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SPOILID) AS 'NO', SPOILID, " +
                                      "CONVERT(VARCHAR(10), SPOILDATE, 103) AS SPOILDATE, REMARK, STATUS, CREATEDBY " +
                                      "FROM H_SPOIL_ITEM " +
                                      "ORDER BY SPOILID ASC");
                                      //"WHERE STATUS = '{0}' " +
                                      //"ORDER BY SPOILID ASC",Status.READY);
                columnAdjustmentSpoil[0] = "SPOILID";
                columnAdjustmentSpoil[1] = "CREATEDBY";
                columnAdjustmentSpoilFind = "SPOILID";
            }
            createBackgroundWorkerFillDatagridView(query, "Transactiondatagridview");
        }

        void createBackgroundWorkerFillDatagridView(String query, String called)
        {

            for (int i = 0; i < Transactiondatagridview.Columns.Count; i++)
            {
                if (Transactiondatagridview.Columns[i].DataPropertyName.Equals(columnAdjustmentSpoilFind))
                {
                    DataGridViewColumn dataGridViewColumn = Transactiondatagridview.Columns[i];
                    dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                    break;
                }
            }

            String[] argument = { query, called };
            
            BackgroundWorker bgworker = new BackgroundWorker();
            bgworker.DoWork += new DoWorkEventHandler(bgworker_DoWork);
            bgworker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgworker_RunWorkerCompleted);
            bgworker.RunWorkerAsync(argument);
        }

        void bgworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {

        }

        void bgworker_DoWork(object sender, DoWorkEventArgs e) {
            try
            {
                String[] obj = e.Argument as String[];
                String query = (String) obj[0];
                String called = (String) obj[1];

                BeginInvoke(new fillDatatableCallBack(this.fillDatatable), con.openDataTableQuery(query), called);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void fillDatatable(DataTable datatable, String called)
        {
            if (called.Equals("Transactiondatagridview")) Transactiondatagridview.DataSource = datatable;
            else if (called.Equals("TransactionItemdataGridView")) TransactionItemdataGridView.DataSource = datatable;
        }

        #region Method Transactiondatagridview
        private void Transactiondatagridview_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.RowIndex != -1)
            {
                if (adjustmentSpoilPassingData != null)
                {
                   TransactiondatagridviewCallPassingData(e.RowIndex);
                }
            }
        }

        private void Transactiondatagridview_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                foreach (String item in columnAdjustmentSpoil)
                {
                    if (Transactiondatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        for (int i = 0; i < Transactiondatagridview.Columns.Count; i++)
                        {
                            if (Transactiondatagridview.Columns[i].DataPropertyName.Equals(columnAdjustmentSpoilFind))
                            {
                                DataGridViewColumn dataGridViewColumn = Transactiondatagridview.Columns[i];
                                dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Control;
                            }
                        }
                        break;
                    }
                }

                foreach (String item in columnAdjustmentSpoil)
                {
                    if (Transactiondatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        columnAdjustmentSpoilFind = Transactiondatagridview.Columns[e.ColumnIndex].DataPropertyName;
                        DataGridViewColumn dataGridViewColumn = Transactiondatagridview.Columns[e.ColumnIndex];
                        dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                        break;
                    }
                }
            }
        }

        private void Transactiondatagridview_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (Transactiondatagridview.Rows.Count != 0)
                {
                    e.SuppressKeyPress = true;
                    int currentColumn = Transactiondatagridview.CurrentCell.ColumnIndex;
                    int currentRow = Transactiondatagridview.CurrentCell.RowIndex;
                    Transactiondatagridview.CurrentCell = Transactiondatagridview[currentColumn, currentRow];

                    if (this.calledForm.Equals("OrderPicking")) {
                        if (Transactiondatagridview.Rows[currentRow].Cells["STATUS"].Value.ToString().Equals(Status.READY))
                        {
                            TransactiondatagridviewCallPassingData(currentRow);
                        }
                    }
                    else TransactiondatagridviewCallPassingData(currentRow);
                }
            }
            else
            {
                if (!(e.KeyCode == Keys.Down || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right || e.KeyCode == Keys.Up))
                {
                    if (char.IsLetterOrDigit((char)e.KeyCode))
                    {
                        char keysCode = (char)e.KeyCode;

                        FindtextBox.Focus();
                        FindtextBox.Text = keysCode.ToString();
                        FindtextBox.SelectionStart = FindtextBox.Text.Length;
                    }
                }
                else
                {
                    int currentRow = Transactiondatagridview.CurrentCell.RowIndex;
                    Transactiondatagridview.Rows[currentRow].Selected = true;
                }
            }
        }

        private void Transactiondatagridview_KeyUp(object sender, KeyEventArgs e)
        {
            int currentRow = Transactiondatagridview.CurrentCell.RowIndex;
            String query = "";
            if (this.calledForm.Equals("Spoil"))
            {
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMID, B.ITEMNAME, A.QUANTITY, A.SPOIL, (A.QUANTITY - A.SPOIL) AS LASTQTY " +
                                      "FROM D_SPOIL_ITEM A JOIN M_ITEM B " +
                                      "ON A.ITEMID = B.ITEMID " +
                                      "WHERE SPOILID = '{0}'", Transactiondatagridview.Rows[currentRow].Cells[1].Value.ToString());
            }
            else {
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMID, B.ITEMNAME, A.QUANTITY, A.ADJUSTMENT, (A.ADJUSTMENT) AS LASTQTY " +
                                      "FROM D_ADJUSTMENT_ITEM A " +
                                      "JOIN M_ITEM B ON A.ITEMID = B.ITEMID " +
                                      "WHERE ADJUSTMENTID = '{0}'", Transactiondatagridview.Rows[currentRow].Cells[1].Value.ToString());
            }
            createBackgroundWorkerFillDatagridView(query, "TransactionItemdataGridView");
        }

        private void Transactiondatagridview_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                String query = "";
                if (this.calledForm.Equals("Spoil"))
                {
                    query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMID, B.ITEMNAME, A.QUANTITY, A.SPOIL, (A.QUANTITY - A.SPOIL) AS LASTQTY " +
                                          "FROM D_SPOIL_ITEM A JOIN M_ITEM B " +
                                          "ON A.ITEMID = B.ITEMID " +
                                          "WHERE SPOILID = '{0}'", Transactiondatagridview.Rows[e.RowIndex].Cells[1].Value.ToString());
                }
                else
                {
                    query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMID, B.ITEMNAME, A.QUANTITY, A.ADJUSTMENT, (A.ADJUSTMENT) AS LASTQTY " +
                                          "FROM D_ADJUSTMENT_ITEM A " +
                                          "JOIN M_ITEM B ON A.ITEMID = B.ITEMID " +
                                          "WHERE ADJUSTMENTID = '{0}'", Transactiondatagridview.Rows[e.RowIndex].Cells[1].Value.ToString());
                }
                createBackgroundWorkerFillDatagridView(query, "TransactionItemdataGridView");
            }
        }

        #endregion

        private void TransactiondatagridviewCallPassingData(int currentRow) {

            DataTable datatable = new DataTable();
            for (int i = 0; i < Transactiondatagridview.Columns.Count; i++){
                
                datatable.Columns.Add(Transactiondatagridview.Columns[i].DataPropertyName);
            }
            
            /*check multiple selected or not*/
            if (Transactiondatagridview.SelectedRows.Count != 0){
                foreach (var item in Transactiondatagridview.SelectedRows){
                    DataRow r = datatable.NewRow();
                    DataGridViewRow row = (DataGridViewRow)item;
                    for (int i = 0; i < Transactiondatagridview.Columns.Count; i++){
                        r[i] = row.Cells[i].Value;
                    }
                    datatable.Rows.Add(r);
                }
            }
            else{
                DataRow r = datatable.NewRow();
                DataGridViewRow row = Transactiondatagridview.Rows[currentRow];
                for (int i = 0; i < Transactiondatagridview.Columns.Count; i++){
                    r[i] = row.Cells[i].Value;
                }
                datatable.Rows.Add(r);
            }
            adjustmentSpoilPassingData(datatable);
            this.Close();
        }

        #region Method FindTextBox
        private void FindtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) {
                
                String strFind = FindtextBox.Text.Contains('*') ? FindtextBox.Text.Replace('*', '%') : (FindtextBox.Text + '%');
                String query = "";
                if (this.calledForm.Equals("Adjustment"))
                {
                    addColumnsForAdjustmentBuild();
                    query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY ADJUSTMENTID) AS 'NO', ADJUSTMENTID, " +
                                          "CONVERT(VARCHAR(10), ADJUSTMENTDATE, 111) AS ADJUSTMENTDATE, REMARK, STATUS, CREATEDBY " +
                                          "FROM H_ADJUSTMENT_ITEM " +
                                          "WHERE {0} LIKE '{1}' AND ( STATUS = '{2}' OR STATUS = '{3}' )" +
                                          "ORDER BY ADJUSTMENTID ASC",columnAdjustmentSpoilFind, strFind, Status.READY, Status.READY);
                }
                else if (this.calledForm.Equals("Spoil"))
                {
                    addColumnsForSpoilBuild();
                    query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SPOILID) AS 'NO', SPOILID, " +
                                          "CONVERT(VARCHAR(10), SPOILDATE, 111) AS SPOILDATE, REMARK, STATUS, CREATEDBY " +
                                          "FROM H_SPOIL_ITEM " +
                                          "WHERE {0} LIKE '{1}' AND ( STATUS = '{2}' OR STATUS = '{3}' )" +
                                          "ORDER BY SPOILID ASC", columnAdjustmentSpoilFind, strFind, Status.READY, Status.READY);
                }

                createBackgroundWorkerFillDatagridView(query, "Transactiondatagridview");
            }
        }
        #endregion

        private void addColumnsForAdjustmentBuild()
        {
            Transactiondatagridview.Columns.Clear();
            var NO = new DataGridViewTextBoxColumn();
            NO.Name = "NO";
            NO.HeaderText = "NO";
            NO.DataPropertyName = "NO";
            NO.Width = 40;

            var ADJUSTMENTID = new DataGridViewTextBoxColumn();
            ADJUSTMENTID.Name = "ADJUSTMENTID";
            ADJUSTMENTID.HeaderText = "ADJUSTMENT NO";
            ADJUSTMENTID.DataPropertyName = "ADJUSTMENTID";
            ADJUSTMENTID.Width = 150;

            var ADJUSTMENTDATE = new DataGridViewTextBoxColumn();
            ADJUSTMENTDATE.Name = "ADJUSTMENTDATE";
            ADJUSTMENTDATE.HeaderText = "ADJUSTMENT DATE";
            ADJUSTMENTDATE.DataPropertyName = "ADJUSTMENTDATE";
            ADJUSTMENTDATE.Width = 150;

            var REMARK = new DataGridViewTextBoxColumn();
            REMARK.Name = "REMARK";
            REMARK.HeaderText = "REMARK";
            REMARK.DataPropertyName = "REMARK";
            REMARK.Width = 100;

            var CREATEDBY = new DataGridViewTextBoxColumn();
            CREATEDBY.Name = "CREATEDBY";
            CREATEDBY.HeaderText = "CREATEDBY";
            CREATEDBY.DataPropertyName = "CREATEDBY";
            CREATEDBY.Width = 100;

            Transactiondatagridview.Columns.AddRange(
                                                        new DataGridViewColumn[]{ 
                                                                                    NO, ADJUSTMENTID, ADJUSTMENTDATE, REMARK, CREATEDBY
                                                                                }
                                                   );

            TransactionItemdataGridView.Columns.Clear();
            var NO1 = new DataGridViewTextBoxColumn();
            NO1.Name = "NO";
            NO1.HeaderText = "NO";
            NO1.DataPropertyName = "NO";
            NO1.Width = 40;

            var ITEMID = new DataGridViewTextBoxColumn();
            ITEMID.Name = "ITEMID";
            ITEMID.HeaderText = "PART NO";
            ITEMID.DataPropertyName = "ITEMID";
            ITEMID.Width = 100;

            var ITEMNAME = new DataGridViewTextBoxColumn();
            ITEMNAME.Name = "ITEMNAME";
            ITEMNAME.HeaderText = "PART NAME";
            ITEMNAME.DataPropertyName = "ITEMNAME";
            ITEMNAME.Width = 100;

            var QUANTITY = new DataGridViewTextBoxColumn();
            QUANTITY.Name = "QUANTITY";
            QUANTITY.HeaderText = "QUANTITY";
            QUANTITY.DataPropertyName = "QUANTITY";
            QUANTITY.Width = 100;

            var ADJUSTMENT = new DataGridViewTextBoxColumn();
            ADJUSTMENT.Name = "ADJUSTMENT";
            ADJUSTMENT.HeaderText = "ADJUSTMENT";
            ADJUSTMENT.DataPropertyName = "ADJUSTMENT";
            ADJUSTMENT.Width = 100;

            var LASTQTY = new DataGridViewTextBoxColumn();
            LASTQTY.Name = "LASTQTY";
            LASTQTY.HeaderText = "LAST QTY";
            LASTQTY.DataPropertyName = "LASTQTY";
            LASTQTY.Width = 100;

            TransactionItemdataGridView.Columns.AddRange(
                                                        new DataGridViewColumn[]{ 
                                                                                    NO1, ITEMID, ITEMNAME, QUANTITY, ADJUSTMENT, LASTQTY
                                                                                }
                                                   );
        }

        private void addColumnsForSpoilBuild()
        {
            Transactiondatagridview.Columns.Clear();
            var NO = new DataGridViewTextBoxColumn();
            NO.Name = "NO";
            NO.HeaderText = "NO";
            NO.DataPropertyName = "NO";
            NO.Width = 40;

            var SPOILID = new DataGridViewTextBoxColumn();
            SPOILID.Name = "SPOILID";
            SPOILID.HeaderText = "SPOIL NO";
            SPOILID.DataPropertyName = "SPOILID";
            SPOILID.Width = 100;

            var SPOILDATE = new DataGridViewTextBoxColumn();
            SPOILDATE.Name = "SPOILDATE";
            SPOILDATE.HeaderText = "SPOIL DATE";
            SPOILDATE.DataPropertyName = "SPOILDATE";
            SPOILDATE.Width = 150;

            var REMARK = new DataGridViewTextBoxColumn();
            REMARK.Name = "REMARK";
            REMARK.HeaderText = "REMARK";
            REMARK.DataPropertyName = "REMARK";
            REMARK.Width = 100;

            var CREATEDBY = new DataGridViewTextBoxColumn();
            CREATEDBY.Name = "CREATEDBY";
            CREATEDBY.HeaderText = "CREATEDBY";
            CREATEDBY.DataPropertyName = "CREATEDBY";
            CREATEDBY.Width = 100;

            Transactiondatagridview.Columns.AddRange(
                                                        new DataGridViewColumn[]{ 
                                                                                    NO, SPOILID, SPOILDATE, REMARK, CREATEDBY
                                                                                }
                                                   );

            TransactionItemdataGridView.Columns.Clear();
            var NO1 = new DataGridViewTextBoxColumn();
            NO1.Name = "NO";
            NO1.HeaderText = "NO";
            NO1.DataPropertyName = "NO";
            NO1.Width = 40;

            var ITEMID = new DataGridViewTextBoxColumn();
            ITEMID.Name = "ITEMID";
            ITEMID.HeaderText = "PART NO";
            ITEMID.DataPropertyName = "ITEMID";
            ITEMID.Width = 100;

            var ITEMNAME = new DataGridViewTextBoxColumn();
            ITEMNAME.Name = "ITEMNAME";
            ITEMNAME.HeaderText = "PART NAME";
            ITEMNAME.DataPropertyName = "ITEMNAME";
            ITEMNAME.Width = 100;

            var QUANTITY = new DataGridViewTextBoxColumn();
            QUANTITY.Name = "QUANTITY";
            QUANTITY.HeaderText = "QUANTITY";
            QUANTITY.DataPropertyName = "QUANTITY";
            QUANTITY.Width = 100;

            var SPOIL = new DataGridViewTextBoxColumn();
            SPOIL.Name = "SPOIL";
            SPOIL.HeaderText = "SPOIL";
            SPOIL.DataPropertyName = "SPOIL";
            SPOIL.Width = 100;

            var LASTQTY = new DataGridViewTextBoxColumn();
            LASTQTY.Name = "LASTQTY";
            LASTQTY.HeaderText = "LAST QTY";
            LASTQTY.DataPropertyName = "LASTQTY";
            LASTQTY.Width = 100;

            TransactionItemdataGridView.Columns.AddRange(
                                                        new DataGridViewColumn[]{ 
                                                                                    NO1, ITEMID, ITEMNAME, QUANTITY, SPOIL, LASTQTY
                                                                                }
                                                   );
        }

        private void AdjustSpoildatagridview_FormClosing(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }

    }
}
