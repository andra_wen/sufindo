﻿namespace Sufindo
{
    partial class SalesReturndatagridview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.FindtextBox = new System.Windows.Forms.TextBox();
            this.datagridviewpanel = new System.Windows.Forms.Panel();
            this.TransactionSalesReturndatagridview = new System.Windows.Forms.DataGridView();
            this.ContactPersonpanel = new System.Windows.Forms.Panel();
            this.TransactionSalesReturnItemdataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMIDQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PARTNAMEQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUANTITY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UNITPRICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SALESRETURNID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SALESRETURNDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICEID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICEDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMERNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMERID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCYID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REBATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTALINVOICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REMARK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATEDBY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datagridviewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionSalesReturndatagridview)).BeginInit();
            this.ContactPersonpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionSalesReturnItemdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Find";
            // 
            // FindtextBox
            // 
            this.FindtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FindtextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FindtextBox.Location = new System.Drawing.Point(70, 3);
            this.FindtextBox.Name = "FindtextBox";
            this.FindtextBox.Size = new System.Drawing.Size(212, 31);
            this.FindtextBox.TabIndex = 3;
            this.FindtextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FindtextBox_KeyPress);
            // 
            // datagridviewpanel
            // 
            this.datagridviewpanel.Controls.Add(this.TransactionSalesReturndatagridview);
            this.datagridviewpanel.Location = new System.Drawing.Point(4, 37);
            this.datagridviewpanel.Name = "datagridviewpanel";
            this.datagridviewpanel.Size = new System.Drawing.Size(1026, 233);
            this.datagridviewpanel.TabIndex = 7;
            // 
            // TransactionSalesReturndatagridview
            // 
            this.TransactionSalesReturndatagridview.AllowUserToAddRows = false;
            this.TransactionSalesReturndatagridview.AllowUserToDeleteRows = false;
            this.TransactionSalesReturndatagridview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.TransactionSalesReturndatagridview.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NO,
            this.SALESRETURNID,
            this.SALESRETURNDATE,
            this.INVOICEID,
            this.INVOICEDATE,
            this.CUSTOMERNAME,
            this.CUSTOMERID,
            this.CURRENCYID,
            this.RATE,
            this.REBATE,
            this.TOTALINVOICE,
            this.TOTAL,
            this.REMARK,
            this.STATUS,
            this.CREATEDBY});
            this.TransactionSalesReturndatagridview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TransactionSalesReturndatagridview.Location = new System.Drawing.Point(0, 0);
            this.TransactionSalesReturndatagridview.MultiSelect = false;
            this.TransactionSalesReturndatagridview.Name = "TransactionSalesReturndatagridview";
            this.TransactionSalesReturndatagridview.ReadOnly = true;
            this.TransactionSalesReturndatagridview.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TransactionSalesReturndatagridview.Size = new System.Drawing.Size(1026, 233);
            this.TransactionSalesReturndatagridview.TabIndex = 0;
            this.TransactionSalesReturndatagridview.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TransactionSalesReturndatagridview_CellDoubleClick);
            this.TransactionSalesReturndatagridview.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.TransactionSalesReturndatagridview_ColumnHeaderMouseClick);
            this.TransactionSalesReturndatagridview.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TransactionSalesReturndatagridview_CellClick);
            this.TransactionSalesReturndatagridview.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TransactionSalesReturndatagridview_KeyDown);
            this.TransactionSalesReturndatagridview.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TransactionSalesReturndatagridview_KeyUp);
            // 
            // ContactPersonpanel
            // 
            this.ContactPersonpanel.Controls.Add(this.TransactionSalesReturnItemdataGridView);
            this.ContactPersonpanel.Location = new System.Drawing.Point(4, 275);
            this.ContactPersonpanel.Name = "ContactPersonpanel";
            this.ContactPersonpanel.Size = new System.Drawing.Size(816, 226);
            this.ContactPersonpanel.TabIndex = 24;
            // 
            // TransactionSalesReturnItemdataGridView
            // 
            this.TransactionSalesReturnItemdataGridView.AllowUserToAddRows = false;
            this.TransactionSalesReturnItemdataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TransactionSalesReturnItemdataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.TransactionSalesReturnItemdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.TransactionSalesReturnItemdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.ITEMID,
            this.ITEMNAME,
            this.ITEMIDQTY,
            this.PARTNAMEQTY,
            this.QUANTITY,
            this.UOMID,
            this.UNITPRICE,
            this.AMOUNT});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.TransactionSalesReturnItemdataGridView.DefaultCellStyle = dataGridViewCellStyle5;
            this.TransactionSalesReturnItemdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TransactionSalesReturnItemdataGridView.Location = new System.Drawing.Point(0, 0);
            this.TransactionSalesReturnItemdataGridView.Name = "TransactionSalesReturnItemdataGridView";
            this.TransactionSalesReturnItemdataGridView.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TransactionSalesReturnItemdataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.TransactionSalesReturnItemdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TransactionSalesReturnItemdataGridView.Size = new System.Drawing.Size(816, 226);
            this.TransactionSalesReturnItemdataGridView.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "NO";
            this.dataGridViewTextBoxColumn1.FillWeight = 40F;
            this.dataGridViewTextBoxColumn1.HeaderText = "NO";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 40;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 40;
            // 
            // ITEMID
            // 
            this.ITEMID.DataPropertyName = "ITEMID";
            this.ITEMID.HeaderText = "PART NO";
            this.ITEMID.Name = "ITEMID";
            this.ITEMID.ReadOnly = true;
            // 
            // ITEMNAME
            // 
            this.ITEMNAME.DataPropertyName = "ITEMNAME";
            this.ITEMNAME.HeaderText = "PART NAME";
            this.ITEMNAME.Name = "ITEMNAME";
            this.ITEMNAME.ReadOnly = true;
            // 
            // ITEMIDQTY
            // 
            this.ITEMIDQTY.DataPropertyName = "ITEMIDQTY";
            this.ITEMIDQTY.FillWeight = 105F;
            this.ITEMIDQTY.HeaderText = "PART NO QTY";
            this.ITEMIDQTY.Name = "ITEMIDQTY";
            this.ITEMIDQTY.ReadOnly = true;
            this.ITEMIDQTY.Width = 105;
            // 
            // PARTNAMEQTY
            // 
            this.PARTNAMEQTY.DataPropertyName = "ITEMNAMEQTY";
            this.PARTNAMEQTY.FillWeight = 120F;
            this.PARTNAMEQTY.HeaderText = "PART NAME QTY";
            this.PARTNAMEQTY.Name = "PARTNAMEQTY";
            this.PARTNAMEQTY.ReadOnly = true;
            this.PARTNAMEQTY.Width = 120;
            // 
            // QUANTITY
            // 
            this.QUANTITY.DataPropertyName = "QUANTITY";
            this.QUANTITY.HeaderText = "QUANTITY";
            this.QUANTITY.Name = "QUANTITY";
            this.QUANTITY.ReadOnly = true;
            // 
            // UOMID
            // 
            this.UOMID.DataPropertyName = "UOMID";
            this.UOMID.HeaderText = "UOM";
            this.UOMID.Name = "UOMID";
            this.UOMID.ReadOnly = true;
            // 
            // UNITPRICE
            // 
            this.UNITPRICE.DataPropertyName = "PRICE";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle3.Format = "N2";
            this.UNITPRICE.DefaultCellStyle = dataGridViewCellStyle3;
            this.UNITPRICE.HeaderText = "UNIT PRICE";
            this.UNITPRICE.Name = "UNITPRICE";
            this.UNITPRICE.ReadOnly = true;
            // 
            // AMOUNT
            // 
            this.AMOUNT.DataPropertyName = "AMOUNT";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle4.Format = "N2";
            this.AMOUNT.DefaultCellStyle = dataGridViewCellStyle4;
            this.AMOUNT.HeaderText = "AMOUNT";
            this.AMOUNT.Name = "AMOUNT";
            this.AMOUNT.ReadOnly = true;
            this.AMOUNT.Visible = false;
            // 
            // NO
            // 
            this.NO.DataPropertyName = "NO";
            this.NO.HeaderText = "NO";
            this.NO.Name = "NO";
            this.NO.ReadOnly = true;
            this.NO.Width = 40;
            // 
            // SALESRETURNID
            // 
            this.SALESRETURNID.DataPropertyName = "SALESRETURNID";
            this.SALESRETURNID.HeaderText = "SR NO";
            this.SALESRETURNID.Name = "SALESRETURNID";
            this.SALESRETURNID.ReadOnly = true;
            this.SALESRETURNID.Width = 80;
            // 
            // SALESRETURNDATE
            // 
            this.SALESRETURNDATE.DataPropertyName = "SALESRETURNDATE";
            this.SALESRETURNDATE.HeaderText = "SR DATE";
            this.SALESRETURNDATE.Name = "SALESRETURNDATE";
            this.SALESRETURNDATE.ReadOnly = true;
            this.SALESRETURNDATE.Width = 80;
            // 
            // INVOICEID
            // 
            this.INVOICEID.DataPropertyName = "INVOICEID";
            this.INVOICEID.HeaderText = "INVOICE NO";
            this.INVOICEID.Name = "INVOICEID";
            this.INVOICEID.ReadOnly = true;
            this.INVOICEID.Width = 80;
            // 
            // INVOICEDATE
            // 
            this.INVOICEDATE.DataPropertyName = "INVOICEDATE";
            this.INVOICEDATE.HeaderText = "INVOICE DATE";
            this.INVOICEDATE.Name = "INVOICEDATE";
            this.INVOICEDATE.ReadOnly = true;
            // 
            // CUSTOMERNAME
            // 
            this.CUSTOMERNAME.DataPropertyName = "CUSTOMERNAME";
            this.CUSTOMERNAME.HeaderText = "CUSTOMER NAME";
            this.CUSTOMERNAME.Name = "CUSTOMERNAME";
            this.CUSTOMERNAME.ReadOnly = true;
            this.CUSTOMERNAME.Width = 120;
            // 
            // CUSTOMERID
            // 
            this.CUSTOMERID.DataPropertyName = "CUSTOMERID";
            this.CUSTOMERID.HeaderText = "CUSTOMERID";
            this.CUSTOMERID.Name = "CUSTOMERID";
            this.CUSTOMERID.ReadOnly = true;
            this.CUSTOMERID.Visible = false;
            // 
            // CURRENCYID
            // 
            this.CURRENCYID.DataPropertyName = "CURRENCYID";
            this.CURRENCYID.HeaderText = "CURRENCY";
            this.CURRENCYID.Name = "CURRENCYID";
            this.CURRENCYID.ReadOnly = true;
            this.CURRENCYID.Width = 80;
            // 
            // RATE
            // 
            this.RATE.DataPropertyName = "RATE";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle1.Format = "N2";
            this.RATE.DefaultCellStyle = dataGridViewCellStyle1;
            this.RATE.HeaderText = "RATE";
            this.RATE.Name = "RATE";
            this.RATE.ReadOnly = true;
            this.RATE.Width = 80;
            // 
            // REBATE
            // 
            this.REBATE.DataPropertyName = "REBATE";
            this.REBATE.HeaderText = "REBATE";
            this.REBATE.Name = "REBATE";
            this.REBATE.ReadOnly = true;
            this.REBATE.Width = 80;
            // 
            // TOTALINVOICE
            // 
            this.TOTALINVOICE.DataPropertyName = "TOTALINVOICE";
            this.TOTALINVOICE.HeaderText = "TOTALINVOICE";
            this.TOTALINVOICE.Name = "TOTALINVOICE";
            this.TOTALINVOICE.ReadOnly = true;
            this.TOTALINVOICE.Visible = false;
            // 
            // TOTAL
            // 
            this.TOTAL.DataPropertyName = "TOTAL";
            this.TOTAL.HeaderText = "TOTAL";
            this.TOTAL.Name = "TOTAL";
            this.TOTAL.ReadOnly = true;
            this.TOTAL.Visible = false;
            // 
            // REMARK
            // 
            this.REMARK.DataPropertyName = "REMARK";
            this.REMARK.HeaderText = "REMARK";
            this.REMARK.Name = "REMARK";
            this.REMARK.ReadOnly = true;
            // 
            // STATUS
            // 
            this.STATUS.DataPropertyName = "STATUS";
            this.STATUS.HeaderText = "STATUS";
            this.STATUS.Name = "STATUS";
            this.STATUS.ReadOnly = true;
            // 
            // CREATEDBY
            // 
            this.CREATEDBY.DataPropertyName = "CREATEDBY";
            this.CREATEDBY.HeaderText = "CREATED BY";
            this.CREATEDBY.Name = "CREATEDBY";
            this.CREATEDBY.ReadOnly = true;
            // 
            // SalesReturndatagridview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1069, 516);
            this.Controls.Add(this.ContactPersonpanel);
            this.Controls.Add(this.datagridviewpanel);
            this.Controls.Add(this.FindtextBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "SalesReturndatagridview";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "List Of Sales Return";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SalesReturndatagridview_FormClosing);
            this.datagridviewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TransactionSalesReturndatagridview)).EndInit();
            this.ContactPersonpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TransactionSalesReturnItemdataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FindtextBox;
        private System.Windows.Forms.Panel datagridviewpanel;
        private System.Windows.Forms.DataGridView TransactionSalesReturndatagridview;
        private System.Windows.Forms.Panel ContactPersonpanel;
        private System.Windows.Forms.DataGridView TransactionSalesReturnItemdataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMIDQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn PARTNAMEQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn QUANTITY;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn UNITPRICE;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn SALESRETURNID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SALESRETURNDATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn INVOICEID;
        private System.Windows.Forms.DataGridViewTextBoxColumn INVOICEDATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUSTOMERNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUSTOMERID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CURRENCYID;
        private System.Windows.Forms.DataGridViewTextBoxColumn RATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn REBATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn TOTALINVOICE;
        private System.Windows.Forms.DataGridViewTextBoxColumn TOTAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn REMARK;
        private System.Windows.Forms.DataGridViewTextBoxColumn STATUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn CREATEDBY;
    }
}