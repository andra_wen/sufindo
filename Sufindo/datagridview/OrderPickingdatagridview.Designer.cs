﻿namespace Sufindo
{
    partial class OrderPickingdatagridview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.FindtextBox = new System.Windows.Forms.TextBox();
            this.datagridviewpanel = new System.Windows.Forms.Panel();
            this.TransactionOPdatagridview = new System.Windows.Forms.DataGridView();
            this.ContactPersonpanel = new System.Windows.Forms.Panel();
            this.TransactionOPItemdataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMIDQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PARTNAMEQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUANTITY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORDERPICKINGID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORDERPICKINGDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SALESORDERID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SALESORDERDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMERNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATEDBY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datagridviewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionOPdatagridview)).BeginInit();
            this.ContactPersonpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionOPItemdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Find";
            // 
            // FindtextBox
            // 
            this.FindtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FindtextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FindtextBox.Location = new System.Drawing.Point(70, 3);
            this.FindtextBox.Name = "FindtextBox";
            this.FindtextBox.Size = new System.Drawing.Size(212, 31);
            this.FindtextBox.TabIndex = 3;
            this.FindtextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FindtextBox_KeyPress);
            // 
            // datagridviewpanel
            // 
            this.datagridviewpanel.Controls.Add(this.TransactionOPdatagridview);
            this.datagridviewpanel.Location = new System.Drawing.Point(4, 36);
            this.datagridviewpanel.Name = "datagridviewpanel";
            this.datagridviewpanel.Size = new System.Drawing.Size(808, 233);
            this.datagridviewpanel.TabIndex = 7;
            // 
            // TransactionOPdatagridview
            // 
            this.TransactionOPdatagridview.AllowUserToAddRows = false;
            this.TransactionOPdatagridview.AllowUserToDeleteRows = false;
            this.TransactionOPdatagridview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.TransactionOPdatagridview.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NO,
            this.ORDERPICKINGID,
            this.ORDERPICKINGDATE,
            this.SALESORDERID,
            this.SALESORDERDATE,
            this.CUSTOMERNAME,
            this.STATUS,
            this.CREATEDBY});
            this.TransactionOPdatagridview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TransactionOPdatagridview.Location = new System.Drawing.Point(0, 0);
            this.TransactionOPdatagridview.MultiSelect = false;
            this.TransactionOPdatagridview.Name = "TransactionOPdatagridview";
            this.TransactionOPdatagridview.ReadOnly = true;
            this.TransactionOPdatagridview.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TransactionOPdatagridview.Size = new System.Drawing.Size(808, 233);
            this.TransactionOPdatagridview.TabIndex = 0;
            this.TransactionOPdatagridview.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TransactionOPdatagridview_CellDoubleClick);
            this.TransactionOPdatagridview.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.TransactionOPdatagridview_ColumnHeaderMouseClick);
            this.TransactionOPdatagridview.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TransactionOPdatagridview_CellClick);
            this.TransactionOPdatagridview.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TransactionOPdatagridview_KeyDown);
            this.TransactionOPdatagridview.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TransactionOPdatagridview_KeyUp);
            // 
            // ContactPersonpanel
            // 
            this.ContactPersonpanel.Controls.Add(this.TransactionOPItemdataGridView);
            this.ContactPersonpanel.Location = new System.Drawing.Point(4, 276);
            this.ContactPersonpanel.Name = "ContactPersonpanel";
            this.ContactPersonpanel.Size = new System.Drawing.Size(711, 226);
            this.ContactPersonpanel.TabIndex = 24;
            // 
            // TransactionOPItemdataGridView
            // 
            this.TransactionOPItemdataGridView.AllowUserToAddRows = false;
            this.TransactionOPItemdataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TransactionOPItemdataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.TransactionOPItemdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.TransactionOPItemdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.ITEMID,
            this.ITEMNAME,
            this.ITEMIDQTY,
            this.PARTNAMEQTY,
            this.QUANTITY,
            this.UOMID});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.TransactionOPItemdataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.TransactionOPItemdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TransactionOPItemdataGridView.Location = new System.Drawing.Point(0, 0);
            this.TransactionOPItemdataGridView.Name = "TransactionOPItemdataGridView";
            this.TransactionOPItemdataGridView.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TransactionOPItemdataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.TransactionOPItemdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TransactionOPItemdataGridView.Size = new System.Drawing.Size(711, 226);
            this.TransactionOPItemdataGridView.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "NO";
            this.dataGridViewTextBoxColumn1.FillWeight = 40F;
            this.dataGridViewTextBoxColumn1.HeaderText = "NO";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 40;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 40;
            // 
            // ITEMID
            // 
            this.ITEMID.DataPropertyName = "ITEMID";
            this.ITEMID.HeaderText = "PART NO";
            this.ITEMID.Name = "ITEMID";
            this.ITEMID.ReadOnly = true;
            // 
            // ITEMNAME
            // 
            this.ITEMNAME.DataPropertyName = "ITEMNAME";
            this.ITEMNAME.HeaderText = "PART NAME";
            this.ITEMNAME.Name = "ITEMNAME";
            this.ITEMNAME.ReadOnly = true;
            // 
            // ITEMIDQTY
            // 
            this.ITEMIDQTY.DataPropertyName = "ITEMIDQTY";
            this.ITEMIDQTY.FillWeight = 105F;
            this.ITEMIDQTY.HeaderText = "PART NO QTY";
            this.ITEMIDQTY.Name = "ITEMIDQTY";
            this.ITEMIDQTY.ReadOnly = true;
            this.ITEMIDQTY.Width = 105;
            // 
            // PARTNAMEQTY
            // 
            this.PARTNAMEQTY.DataPropertyName = "ITEMNAMEQTY";
            this.PARTNAMEQTY.FillWeight = 120F;
            this.PARTNAMEQTY.HeaderText = "PART NAME QTY";
            this.PARTNAMEQTY.Name = "PARTNAMEQTY";
            this.PARTNAMEQTY.ReadOnly = true;
            this.PARTNAMEQTY.Width = 120;
            // 
            // QUANTITY
            // 
            this.QUANTITY.DataPropertyName = "QUANTITY";
            this.QUANTITY.HeaderText = "QUANTITY";
            this.QUANTITY.Name = "QUANTITY";
            this.QUANTITY.ReadOnly = true;
            // 
            // UOMID
            // 
            this.UOMID.DataPropertyName = "UOMID";
            this.UOMID.HeaderText = "UOM";
            this.UOMID.Name = "UOMID";
            this.UOMID.ReadOnly = true;
            // 
            // NO
            // 
            this.NO.DataPropertyName = "NO";
            this.NO.HeaderText = "NO";
            this.NO.Name = "NO";
            this.NO.ReadOnly = true;
            this.NO.Width = 40;
            // 
            // ORDERPICKINGID
            // 
            this.ORDERPICKINGID.DataPropertyName = "ORDERPICKINGID";
            this.ORDERPICKINGID.HeaderText = "OP NO";
            this.ORDERPICKINGID.Name = "ORDERPICKINGID";
            this.ORDERPICKINGID.ReadOnly = true;
            this.ORDERPICKINGID.Width = 80;
            // 
            // ORDERPICKINGDATE
            // 
            this.ORDERPICKINGDATE.DataPropertyName = "ORDERPICKINGDATE";
            this.ORDERPICKINGDATE.HeaderText = "OP DATE";
            this.ORDERPICKINGDATE.Name = "ORDERPICKINGDATE";
            this.ORDERPICKINGDATE.ReadOnly = true;
            // 
            // SALESORDERID
            // 
            this.SALESORDERID.DataPropertyName = "SALESORDERID";
            this.SALESORDERID.HeaderText = "SO NO";
            this.SALESORDERID.Name = "SALESORDERID";
            this.SALESORDERID.ReadOnly = true;
            this.SALESORDERID.Width = 80;
            // 
            // SALESORDERDATE
            // 
            this.SALESORDERDATE.DataPropertyName = "SALESORDERDATE";
            this.SALESORDERDATE.FillWeight = 90F;
            this.SALESORDERDATE.HeaderText = "SO DATE";
            this.SALESORDERDATE.Name = "SALESORDERDATE";
            this.SALESORDERDATE.ReadOnly = true;
            this.SALESORDERDATE.Width = 90;
            // 
            // CUSTOMERNAME
            // 
            this.CUSTOMERNAME.DataPropertyName = "CUSTOMERNAME";
            this.CUSTOMERNAME.HeaderText = "CUSTOMER NAME";
            this.CUSTOMERNAME.Name = "CUSTOMERNAME";
            this.CUSTOMERNAME.ReadOnly = true;
            this.CUSTOMERNAME.Width = 120;
            // 
            // STATUS
            // 
            this.STATUS.DataPropertyName = "STATUS";
            this.STATUS.HeaderText = "STATUS";
            this.STATUS.Name = "STATUS";
            this.STATUS.ReadOnly = true;
            this.STATUS.Width = 80;
            // 
            // CREATEDBY
            // 
            this.CREATEDBY.DataPropertyName = "CREATEDBY";
            this.CREATEDBY.HeaderText = "CREATED BY";
            this.CREATEDBY.Name = "CREATEDBY";
            this.CREATEDBY.ReadOnly = true;
            // 
            // OrderPickingdatagridview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(816, 505);
            this.Controls.Add(this.ContactPersonpanel);
            this.Controls.Add(this.datagridviewpanel);
            this.Controls.Add(this.FindtextBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OrderPickingdatagridview";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "List Of Order Picking";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OrderPickingdatagridview_FormClosing);
            this.datagridviewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TransactionOPdatagridview)).EndInit();
            this.ContactPersonpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TransactionOPItemdataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FindtextBox;
        private System.Windows.Forms.Panel datagridviewpanel;
        private System.Windows.Forms.DataGridView TransactionOPdatagridview;
        private System.Windows.Forms.Panel ContactPersonpanel;
        private System.Windows.Forms.DataGridView TransactionOPItemdataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMIDQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn PARTNAMEQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn QUANTITY;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDERPICKINGID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDERPICKINGDATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn SALESORDERID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SALESORDERDATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUSTOMERNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn STATUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn CREATEDBY;
    }
}