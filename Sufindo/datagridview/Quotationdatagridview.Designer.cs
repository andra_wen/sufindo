﻿namespace Sufindo
{
    partial class Quotationdatagridview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.FindtextBox = new System.Windows.Forms.TextBox();
            this.datagridviewpanel = new System.Windows.Forms.Panel();
            this.TransactionSOdatagridview = new System.Windows.Forms.DataGridView();
            this.Detailpanel = new System.Windows.Forms.Panel();
            this.TransactionSOItemdataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMIDQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PARTNAMEQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUANTITY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UNITPRICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUOTATIONID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUOTATIONDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMERNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMERID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAYMENTMETHOD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCYID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DISCOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REMARK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATEDBY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datagridviewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionSOdatagridview)).BeginInit();
            this.Detailpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionSOItemdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Find";
            // 
            // FindtextBox
            // 
            this.FindtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FindtextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FindtextBox.Location = new System.Drawing.Point(69, 3);
            this.FindtextBox.Name = "FindtextBox";
            this.FindtextBox.Size = new System.Drawing.Size(212, 31);
            this.FindtextBox.TabIndex = 3;
            this.FindtextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FindtextBox_KeyPress);
            // 
            // datagridviewpanel
            // 
            this.datagridviewpanel.Controls.Add(this.TransactionSOdatagridview);
            this.datagridviewpanel.Location = new System.Drawing.Point(4, 37);
            this.datagridviewpanel.Name = "datagridviewpanel";
            this.datagridviewpanel.Size = new System.Drawing.Size(942, 233);
            this.datagridviewpanel.TabIndex = 7;
            // 
            // TransactionSOdatagridview
            // 
            this.TransactionSOdatagridview.AllowUserToAddRows = false;
            this.TransactionSOdatagridview.AllowUserToDeleteRows = false;
            this.TransactionSOdatagridview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.TransactionSOdatagridview.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NO,
            this.QUOTATIONID,
            this.QUOTATIONDATE,
            this.CUSTOMERNAME,
            this.CUSTOMERID,
            this.PAYMENTMETHOD,
            this.CURRENCYID,
            this.RATE,
            this.DISCOUNT,
            this.REMARK,
            this.STATUS,
            this.CREATEDBY});
            this.TransactionSOdatagridview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TransactionSOdatagridview.Location = new System.Drawing.Point(0, 0);
            this.TransactionSOdatagridview.MultiSelect = false;
            this.TransactionSOdatagridview.Name = "TransactionSOdatagridview";
            this.TransactionSOdatagridview.ReadOnly = true;
            this.TransactionSOdatagridview.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TransactionSOdatagridview.Size = new System.Drawing.Size(942, 233);
            this.TransactionSOdatagridview.TabIndex = 0;
            this.TransactionSOdatagridview.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TransactionSOdatagridview_CellDoubleClick);
            this.TransactionSOdatagridview.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.TransactionSOdatagridview_ColumnHeaderMouseClick);
            this.TransactionSOdatagridview.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TransactionSOdatagridview_CellClick);
            this.TransactionSOdatagridview.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TransactionSOdatagridview_KeyDown);
            this.TransactionSOdatagridview.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TransactionSOdatagridview_KeyUp);
            // 
            // Detailpanel
            // 
            this.Detailpanel.Controls.Add(this.TransactionSOItemdataGridView);
            this.Detailpanel.Location = new System.Drawing.Point(4, 277);
            this.Detailpanel.Name = "Detailpanel";
            this.Detailpanel.Size = new System.Drawing.Size(811, 226);
            this.Detailpanel.TabIndex = 24;
            // 
            // TransactionSOItemdataGridView
            // 
            this.TransactionSOItemdataGridView.AllowUserToAddRows = false;
            this.TransactionSOItemdataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TransactionSOItemdataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.TransactionSOItemdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.TransactionSOItemdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.ITEMID,
            this.ITEMNAME,
            this.ITEMIDQTY,
            this.PARTNAMEQTY,
            this.QUANTITY,
            this.UOMID,
            this.UNITPRICE,
            this.AMOUNT});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.TransactionSOItemdataGridView.DefaultCellStyle = dataGridViewCellStyle6;
            this.TransactionSOItemdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TransactionSOItemdataGridView.Location = new System.Drawing.Point(0, 0);
            this.TransactionSOItemdataGridView.Name = "TransactionSOItemdataGridView";
            this.TransactionSOItemdataGridView.ReadOnly = true;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TransactionSOItemdataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.TransactionSOItemdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TransactionSOItemdataGridView.Size = new System.Drawing.Size(811, 226);
            this.TransactionSOItemdataGridView.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "NO";
            this.dataGridViewTextBoxColumn1.FillWeight = 40F;
            this.dataGridViewTextBoxColumn1.HeaderText = "NO";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 40;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 40;
            // 
            // ITEMID
            // 
            this.ITEMID.DataPropertyName = "ITEMID";
            this.ITEMID.HeaderText = "PART NO";
            this.ITEMID.Name = "ITEMID";
            this.ITEMID.ReadOnly = true;
            // 
            // ITEMNAME
            // 
            this.ITEMNAME.DataPropertyName = "ITEMNAME";
            this.ITEMNAME.HeaderText = "PART NAME";
            this.ITEMNAME.Name = "ITEMNAME";
            this.ITEMNAME.ReadOnly = true;
            // 
            // ITEMIDQTY
            // 
            this.ITEMIDQTY.DataPropertyName = "ITEMIDQTY";
            this.ITEMIDQTY.FillWeight = 105F;
            this.ITEMIDQTY.HeaderText = "PART NO QTY";
            this.ITEMIDQTY.Name = "ITEMIDQTY";
            this.ITEMIDQTY.ReadOnly = true;
            this.ITEMIDQTY.Width = 105;
            // 
            // PARTNAMEQTY
            // 
            this.PARTNAMEQTY.DataPropertyName = "ITEMNAMEQTY";
            this.PARTNAMEQTY.FillWeight = 120F;
            this.PARTNAMEQTY.HeaderText = "PART NAME QTY";
            this.PARTNAMEQTY.Name = "PARTNAMEQTY";
            this.PARTNAMEQTY.ReadOnly = true;
            this.PARTNAMEQTY.Width = 120;
            // 
            // QUANTITY
            // 
            this.QUANTITY.DataPropertyName = "QUANTITY";
            this.QUANTITY.HeaderText = "QUANTITY";
            this.QUANTITY.Name = "QUANTITY";
            this.QUANTITY.ReadOnly = true;
            // 
            // UOMID
            // 
            this.UOMID.DataPropertyName = "UOMID";
            this.UOMID.HeaderText = "UOM";
            this.UOMID.Name = "UOMID";
            this.UOMID.ReadOnly = true;
            // 
            // UNITPRICE
            // 
            this.UNITPRICE.DataPropertyName = "PRICE";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle4.Format = "N2";
            this.UNITPRICE.DefaultCellStyle = dataGridViewCellStyle4;
            this.UNITPRICE.HeaderText = "UNIT PRICE";
            this.UNITPRICE.Name = "UNITPRICE";
            this.UNITPRICE.ReadOnly = true;
            // 
            // AMOUNT
            // 
            this.AMOUNT.DataPropertyName = "AMOUNT";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle5.Format = "N2";
            this.AMOUNT.DefaultCellStyle = dataGridViewCellStyle5;
            this.AMOUNT.HeaderText = "AMOUNT";
            this.AMOUNT.Name = "AMOUNT";
            this.AMOUNT.ReadOnly = true;
            this.AMOUNT.Visible = false;
            // 
            // NO
            // 
            this.NO.DataPropertyName = "NO";
            this.NO.HeaderText = "NO";
            this.NO.Name = "NO";
            this.NO.ReadOnly = true;
            this.NO.Width = 40;
            // 
            // QUOTATIONID
            // 
            this.QUOTATIONID.DataPropertyName = "QUOTATIONID";
            this.QUOTATIONID.HeaderText = "QUOTATION NO";
            this.QUOTATIONID.Name = "QUOTATIONID";
            this.QUOTATIONID.ReadOnly = true;
            // 
            // QUOTATIONDATE
            // 
            this.QUOTATIONDATE.DataPropertyName = "QUOTATIONDATE";
            this.QUOTATIONDATE.FillWeight = 135F;
            this.QUOTATIONDATE.HeaderText = "QUOTATION DATE";
            this.QUOTATIONDATE.Name = "QUOTATIONDATE";
            this.QUOTATIONDATE.ReadOnly = true;
            this.QUOTATIONDATE.Width = 110;
            // 
            // CUSTOMERNAME
            // 
            this.CUSTOMERNAME.DataPropertyName = "CUSTOMERNAME";
            this.CUSTOMERNAME.FillWeight = 110F;
            this.CUSTOMERNAME.HeaderText = "CUSTOMER NAME";
            this.CUSTOMERNAME.Name = "CUSTOMERNAME";
            this.CUSTOMERNAME.ReadOnly = true;
            this.CUSTOMERNAME.Width = 110;
            // 
            // CUSTOMERID
            // 
            this.CUSTOMERID.DataPropertyName = "CUSTOMERID";
            this.CUSTOMERID.HeaderText = "CUSTOMERID";
            this.CUSTOMERID.Name = "CUSTOMERID";
            this.CUSTOMERID.ReadOnly = true;
            this.CUSTOMERID.Visible = false;
            // 
            // PAYMENTMETHOD
            // 
            this.PAYMENTMETHOD.DataPropertyName = "PAYMENTMETHOD";
            this.PAYMENTMETHOD.HeaderText = "PAYMENTMETHOD";
            this.PAYMENTMETHOD.Name = "PAYMENTMETHOD";
            this.PAYMENTMETHOD.ReadOnly = true;
            this.PAYMENTMETHOD.Visible = false;
            // 
            // CURRENCYID
            // 
            this.CURRENCYID.DataPropertyName = "CURRENCYID";
            this.CURRENCYID.HeaderText = "CURRENCY";
            this.CURRENCYID.Name = "CURRENCYID";
            this.CURRENCYID.ReadOnly = true;
            this.CURRENCYID.Width = 80;
            // 
            // RATE
            // 
            this.RATE.DataPropertyName = "RATE";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle1.Format = "N2";
            this.RATE.DefaultCellStyle = dataGridViewCellStyle1;
            this.RATE.HeaderText = "RATE";
            this.RATE.Name = "RATE";
            this.RATE.ReadOnly = true;
            this.RATE.Width = 80;
            // 
            // DISCOUNT
            // 
            this.DISCOUNT.DataPropertyName = "DISCOUNT";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle2.Format = "N2";
            this.DISCOUNT.DefaultCellStyle = dataGridViewCellStyle2;
            this.DISCOUNT.HeaderText = "DISCOUNT";
            this.DISCOUNT.Name = "DISCOUNT";
            this.DISCOUNT.ReadOnly = true;
            this.DISCOUNT.Width = 80;
            // 
            // REMARK
            // 
            this.REMARK.DataPropertyName = "REMARK";
            this.REMARK.HeaderText = "REMARK";
            this.REMARK.Name = "REMARK";
            this.REMARK.ReadOnly = true;
            this.REMARK.Width = 80;
            // 
            // STATUS
            // 
            this.STATUS.DataPropertyName = "STATUS";
            this.STATUS.HeaderText = "STATUS";
            this.STATUS.Name = "STATUS";
            this.STATUS.ReadOnly = true;
            this.STATUS.Width = 80;
            // 
            // CREATEDBY
            // 
            this.CREATEDBY.DataPropertyName = "CREATEDBY";
            this.CREATEDBY.HeaderText = "CREATED BY";
            this.CREATEDBY.Name = "CREATEDBY";
            this.CREATEDBY.ReadOnly = true;
            // 
            // Quotationdatagridview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(949, 507);
            this.Controls.Add(this.Detailpanel);
            this.Controls.Add(this.datagridviewpanel);
            this.Controls.Add(this.FindtextBox);
            this.Controls.Add(this.label1);
            this.MinimizeBox = false;
            this.Name = "Quotationdatagridview";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "List Of Quotation";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SalesOrderdatagridview_FormClosing);
            this.datagridviewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TransactionSOdatagridview)).EndInit();
            this.Detailpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TransactionSOItemdataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FindtextBox;
        private System.Windows.Forms.Panel datagridviewpanel;
        private System.Windows.Forms.DataGridView TransactionSOdatagridview;
        private System.Windows.Forms.Panel Detailpanel;
        private System.Windows.Forms.DataGridView TransactionSOItemdataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMIDQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn PARTNAMEQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn QUANTITY;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn UNITPRICE;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn QUOTATIONID;
        private System.Windows.Forms.DataGridViewTextBoxColumn QUOTATIONDATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUSTOMERNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUSTOMERID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAYMENTMETHOD;
        private System.Windows.Forms.DataGridViewTextBoxColumn CURRENCYID;
        private System.Windows.Forms.DataGridViewTextBoxColumn RATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn DISCOUNT;
        private System.Windows.Forms.DataGridViewTextBoxColumn REMARK;
        private System.Windows.Forms.DataGridViewTextBoxColumn STATUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn CREATEDBY;
    }
}