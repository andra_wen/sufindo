﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sufindo.Master;

namespace Sufindo
{
    public partial class Purchasedatagridview : Form
    {
        public delegate void PurchasePassingData(DataTable masterItem);

        private delegate void fillDatatableCallBack(DataTable datatable, String called);

        public PurchasePassingData purchasePassingData;

        private readonly string[] columnMasterItem = { "PURCHASEID", "PURCHASEDATE", "PURCHASEORDERID", "PURCHASEORDERDATE" };
        private Connection con;
        private String columnMasterItemFind = "PURCHASEID";

        private String calledForm;

        public Purchasedatagridview(){
            InitializeComponent();
        }

        public Purchasedatagridview(String calledForm)
        {
            InitializeComponent();

            if (con == null) {
                con = new Connection();
            }
            this.calledForm = calledForm;
            String query = "";
            if (this.calledForm.Equals("ListPurchaseDirectReport"))
            {
                query = String.Format("SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY PURCHASEID) AS 'No', " +
                                      "PURCHASEID, CONVERT(VARCHAR(10), PURCHASEDATE, 103) AS [PURCHASEDATE], " +
                                      "A.PURCHASEORDERID, " +
                                      "(SELECT CONVERT(VARCHAR(10), PURCHASEORDERDATE, 103) AS [PURCHASEORDERDATE] FROM H_PURCHASE_ORDER C WHERE C.PURCHASEORDERID = A.PURCHASEORDERID) AS [PURCHASEORDERDATE], " +
                                      "B.SUPPLIERNAME, A.CONTACTPERSONSUPPLIER, A.KURSID, A.RATE, A.STATUS, A.REMARK, A.CREATEDBY " +
                                      "FROM H_PURCHASE A, M_SUPPLIER B " +
                                      "WHERE A.SUPPLIERID = B.SUPPLIERID " +
                                      "AND A.STATUS = '{0}' AND PURCHASEORDERID = '-' " +
                                      "ORDER BY PURCHASEID ASC", Status.ACTIVE);
            }
            else
            {
                query = String.Format("SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY PURCHASEID) AS 'No', " +
                                      "PURCHASEID, CONVERT(VARCHAR(10), PURCHASEDATE, 103) AS [PURCHASEDATE], " +
                                      "A.PURCHASEORDERID, " +
                                      "(SELECT CONVERT(VARCHAR(10), PURCHASEORDERDATE, 103) AS [PURCHASEORDERDATE] FROM H_PURCHASE_ORDER C WHERE C.PURCHASEORDERID = A.PURCHASEORDERID) AS [PURCHASEORDERDATE], " +
                                      "B.SUPPLIERNAME, A.CONTACTPERSONSUPPLIER, A.KURSID, A.RATE, A.STATUS, A.REMARK, A.CREATEDBY " +
                                      "FROM H_PURCHASE A, M_SUPPLIER B " +
                                      "WHERE A.SUPPLIERID = B.SUPPLIERID " +
                                      "AND A.STATUS = '{0}' " +
                                      "ORDER BY PURCHASEID ASC", Status.ACTIVE);
            }
            createBackgroundWorkerFillDatagridView(query, "TransactionPdatagridview");
        }

        void createBackgroundWorkerFillDatagridView(String query, String called)
        {

            for (int i = 0; i < TransactionPdatagridview.Columns.Count; i++)
            {
                if (TransactionPdatagridview.Columns[i].DataPropertyName.Equals(columnMasterItemFind))
                {
                    DataGridViewColumn dataGridViewColumn = TransactionPdatagridview.Columns[i];
                    dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                    break;
                }
            }

            String[] argument = { query, called };
            
            BackgroundWorker bgworker = new BackgroundWorker();
            bgworker.DoWork += new DoWorkEventHandler(bgworker_DoWork);
            bgworker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgworker_RunWorkerCompleted);
            bgworker.RunWorkerAsync(argument);
        }

        void bgworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {

        }

        void bgworker_DoWork(object sender, DoWorkEventArgs e) {
            try
            {
                String[] obj = e.Argument as String[];
                String query = (String) obj[0];
                String called = (String) obj[1];

                BeginInvoke(new fillDatatableCallBack(this.fillDatatable), con.openDataTableQuery(query), called);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void fillDatatable(DataTable datatable, String called)
        {
            if (called.Equals("TransactionPdatagridview"))
            {
                TransactionPdatagridview.DataSource = datatable;
                this.showTransactionPOItemdataGridView();
            }
            else if (called.Equals("TransactionPItemdataGridView"))
            {
                TransactionPOItemdataGridView.DataSource = datatable;
            }
        }

        private void TransactionPdatagridview_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (purchasePassingData != null)
                {
                    TransactionPdatagridviewCallPassingData(e.RowIndex);
                }
            }
        }

        private void TransactionPdatagridview_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                foreach (String item in columnMasterItem)
                {
                    if (TransactionPdatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        for (int i = 0; i < TransactionPdatagridview.Columns.Count; i++)
                        {
                            if (TransactionPdatagridview.Columns[i].DataPropertyName.Equals(columnMasterItemFind))
                            {
                                DataGridViewColumn dataGridViewColumn = TransactionPdatagridview.Columns[i];
                                dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Control;
                            }
                        }
                        break;
                    }
                }

                foreach (String item in columnMasterItem)
                {
                    if (TransactionPdatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        columnMasterItemFind = TransactionPdatagridview.Columns[e.ColumnIndex].DataPropertyName;
                        DataGridViewColumn dataGridViewColumn = TransactionPdatagridview.Columns[e.ColumnIndex];
                        dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                        break;
                    }
                }
            }
        }

        private void TransactionPdatagridview_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                if (TransactionPdatagridview.Rows.Count != 0)
                {
                    e.SuppressKeyPress = true;
                    int currentColumn = TransactionPdatagridview.CurrentCell.ColumnIndex;
                    int currentRow = TransactionPdatagridview.CurrentCell.RowIndex;
                    TransactionPdatagridview.CurrentCell = TransactionPdatagridview[currentColumn, currentRow];

                    TransactionPdatagridviewCallPassingData(currentRow);
                }
            }
            else
            {
                if (!(e.KeyCode == Keys.Down || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right || e.KeyCode == Keys.Up))
                {
                    if (char.IsLetterOrDigit((char)e.KeyCode))
                    {
                        char keysCode = (char)e.KeyCode;

                        FindtextBox.Focus();
                        FindtextBox.Text = keysCode.ToString();
                        FindtextBox.SelectionStart = FindtextBox.Text.Length;
                    }
                }
                else
                {
                    int currentRow = TransactionPdatagridview.CurrentCell.RowIndex;
                    TransactionPdatagridview.Rows[currentRow].Selected = true;
                }
            }
        }

        private void TransactionPdatagridview_KeyUp(object sender, KeyEventArgs e)
        {
            this.showTransactionPOItemdataGridView();
        }

        private void TransactionPdatagridview_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.RowIndex != -1)
            {
                this.showTransactionPOItemdataGridView();
            }
        }

        private void showTransactionPOItemdataGridView() {
            if (TransactionPdatagridview.Rows.Count == 0)
            {
                while (this.TransactionPOItemdataGridView.Rows.Count > 0)
                {
                    this.TransactionPOItemdataGridView.Rows.RemoveAt(0);
                }
                return;
            }
            int currentRow = TransactionPdatagridview.CurrentCell.RowIndex;
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'No', A.ITEMID, ITEMNAME, A.QUANTITY, UOMID, PRICE " +
                                         "FROM D_PURCHASE A " +
                                         "JOIN M_ITEM B " +
                                         "ON A.ITEMID = B.ITEMID " +
                                         "WHERE PURCHASEID = '{0}'", TransactionPdatagridview.Rows[currentRow].Cells[1].Value.ToString());
            createBackgroundWorkerFillDatagridView(query, "TransactionPItemdataGridView");
        }

        private void TransactionPdatagridviewCallPassingData(int currentRow) {

            DataTable datatable = new DataTable();
            for (int i = 0; i < TransactionPdatagridview.Columns.Count; i++){
                datatable.Columns.Add(TransactionPdatagridview.Columns[i].DataPropertyName);
            }
            
            /*check multiple selected or not*/
            if (TransactionPdatagridview.SelectedRows.Count != 0){
                foreach (var item in TransactionPdatagridview.SelectedRows){
                    DataRow r = datatable.NewRow();
                    DataGridViewRow row = (DataGridViewRow)item;
                    for (int i = 0; i < TransactionPdatagridview.Columns.Count; i++){
                        r[i] = row.Cells[i].Value;
                    }
                    datatable.Rows.Add(r);
                }
            }
            else{
                DataRow r = datatable.NewRow();
                DataGridViewRow row = TransactionPdatagridview.Rows[currentRow];
                for (int i = 0; i < TransactionPdatagridview.Columns.Count; i++){
                    r[i] = row.Cells[i].Value;
                }
                datatable.Rows.Add(r);
            }
            purchasePassingData(datatable);
            this.Close();
        }

        private void FindtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {

                String strFind = FindtextBox.Text.Contains('*') ? FindtextBox.Text.Replace('*', '%') : (FindtextBox.Text + '%');
                String query = "";
                if (this.calledForm.Equals("ListPurchaseDirectReport"))
                {
                    query = String.Format("SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY PURCHASEID) AS 'No', " +
                                          "PURCHASEID, CONVERT(VARCHAR(10), PURCHASEDATE, 103) AS [PURCHASEDATE], " +
                                          "A.PURCHASEORDERID, " +
                                          "(SELECT CONVERT(VARCHAR(10), PURCHASEORDERDATE, 103) AS [PURCHASEORDERDATE] FROM H_PURCHASE_ORDER C WHERE C.PURCHASEORDERID = A.PURCHASEORDERID) AS [PURCHASEORDERDATE], " +
                                          "B.SUPPLIERNAME, A.CONTACTPERSONSUPPLIER, A.KURSID, A.RATE, A.STATUS, A.REMARK, A.CREATEDBY " +
                                          "FROM H_PURCHASE A, M_SUPPLIER B " +
                                          "WHERE A.SUPPLIERID = B.SUPPLIERID AND PURCHASEORDERID = '-' " +
                                          "AND A.STATUS = '{2}' AND {0} LIKE '{1}'" +
                                          "ORDER BY PURCHASEID ASC", columnMasterItemFind, strFind, Status.ACTIVE);
                }
                else
                {
                    query = String.Format("SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY PURCHASEID) AS 'No', " +
                                          "PURCHASEID, CONVERT(VARCHAR(10), PURCHASEDATE, 103) AS [PURCHASEDATE], " +
                                          "A.PURCHASEORDERID, " +
                                          "(SELECT CONVERT(VARCHAR(10), PURCHASEORDERDATE, 103) AS [PURCHASEORDERDATE] FROM H_PURCHASE_ORDER C WHERE C.PURCHASEORDERID = A.PURCHASEORDERID) AS [PURCHASEORDERDATE], " +
                                          "B.SUPPLIERNAME, A.CONTACTPERSONSUPPLIER, A.KURSID, A.RATE, A.STATUS, A.REMARK, A.CREATEDBY " +
                                          "FROM H_PURCHASE A, M_SUPPLIER B " +
                                          "WHERE A.SUPPLIERID = B.SUPPLIERID " +
                                          "AND A.STATUS = '{2}' AND {0} LIKE '{1}' " +
                                          "ORDER BY PURCHASEID ASC", columnMasterItemFind, strFind, Status.ACTIVE);
                }

                createBackgroundWorkerFillDatagridView(query, "TransactionPdatagridview");
            }
        }

        private void Purchasedatagridview_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }
    }
}