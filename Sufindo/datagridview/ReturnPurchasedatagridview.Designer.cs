﻿namespace Sufindo
{
    partial class ReturnPurchasedatagridview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.FindtextBox = new System.Windows.Forms.TextBox();
            this.datagridviewpanel = new System.Windows.Forms.Panel();
            this.TransactionPdatagridview = new System.Windows.Forms.DataGridView();
            this.detaildatagridviewpanel = new System.Windows.Forms.Panel();
            this.TransactionPOItemdataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PARTNOQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PURCHASEID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PURCHASEDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICEID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICEDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUANTITYRCV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUANITITYRTR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PURCHASERETURNID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PURCHASERETURNDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SUPPLIERID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REMARK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATEDBY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datagridviewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionPdatagridview)).BeginInit();
            this.detaildatagridviewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionPOItemdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Find";
            // 
            // FindtextBox
            // 
            this.FindtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FindtextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FindtextBox.Location = new System.Drawing.Point(69, 3);
            this.FindtextBox.Name = "FindtextBox";
            this.FindtextBox.Size = new System.Drawing.Size(212, 31);
            this.FindtextBox.TabIndex = 3;
            this.FindtextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FindtextBox_KeyPress);
            // 
            // datagridviewpanel
            // 
            this.datagridviewpanel.Controls.Add(this.TransactionPdatagridview);
            this.datagridviewpanel.Location = new System.Drawing.Point(3, 37);
            this.datagridviewpanel.Name = "datagridviewpanel";
            this.datagridviewpanel.Size = new System.Drawing.Size(574, 233);
            this.datagridviewpanel.TabIndex = 7;
            // 
            // TransactionPdatagridview
            // 
            this.TransactionPdatagridview.AllowUserToAddRows = false;
            this.TransactionPdatagridview.AllowUserToDeleteRows = false;
            this.TransactionPdatagridview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.TransactionPdatagridview.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NO,
            this.PURCHASERETURNID,
            this.PURCHASERETURNDATE,
            this.SUPPLIERID,
            this.REMARK,
            this.STATUS,
            this.CREATEDBY});
            this.TransactionPdatagridview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TransactionPdatagridview.Location = new System.Drawing.Point(0, 0);
            this.TransactionPdatagridview.MultiSelect = false;
            this.TransactionPdatagridview.Name = "TransactionPdatagridview";
            this.TransactionPdatagridview.ReadOnly = true;
            this.TransactionPdatagridview.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TransactionPdatagridview.Size = new System.Drawing.Size(574, 233);
            this.TransactionPdatagridview.TabIndex = 0;
            this.TransactionPdatagridview.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TransactionPdatagridview_CellDoubleClick);
            this.TransactionPdatagridview.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.TransactionPdatagridview_ColumnHeaderMouseClick);
            this.TransactionPdatagridview.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TransactionPdatagridview_CellClick);
            this.TransactionPdatagridview.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TransactionPdatagridview_KeyDown);
            this.TransactionPdatagridview.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TransactionPdatagridview_KeyUp);
            // 
            // detaildatagridviewpanel
            // 
            this.detaildatagridviewpanel.Controls.Add(this.TransactionPOItemdataGridView);
            this.detaildatagridviewpanel.Location = new System.Drawing.Point(3, 278);
            this.detaildatagridviewpanel.Name = "detaildatagridviewpanel";
            this.detaildatagridviewpanel.Size = new System.Drawing.Size(860, 226);
            this.detaildatagridviewpanel.TabIndex = 24;
            // 
            // TransactionPOItemdataGridView
            // 
            this.TransactionPOItemdataGridView.AllowUserToAddRows = false;
            this.TransactionPOItemdataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TransactionPOItemdataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.TransactionPOItemdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.TransactionPOItemdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.ITEMID,
            this.ITEMNAME,
            this.PARTNOQTY,
            this.PURCHASEID,
            this.PURCHASEDATE,
            this.INVOICEID,
            this.INVOICEDATE,
            this.QUANTITYRCV,
            this.QUANITITYRTR});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.TransactionPOItemdataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.TransactionPOItemdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TransactionPOItemdataGridView.Location = new System.Drawing.Point(0, 0);
            this.TransactionPOItemdataGridView.Name = "TransactionPOItemdataGridView";
            this.TransactionPOItemdataGridView.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TransactionPOItemdataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.TransactionPOItemdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TransactionPOItemdataGridView.Size = new System.Drawing.Size(860, 226);
            this.TransactionPOItemdataGridView.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "NO";
            this.dataGridViewTextBoxColumn1.FillWeight = 40F;
            this.dataGridViewTextBoxColumn1.HeaderText = "NO";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 40;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 40;
            // 
            // ITEMID
            // 
            this.ITEMID.DataPropertyName = "ITEMID";
            this.ITEMID.HeaderText = "PART NO";
            this.ITEMID.Name = "ITEMID";
            this.ITEMID.ReadOnly = true;
            // 
            // ITEMNAME
            // 
            this.ITEMNAME.DataPropertyName = "ITEMNAME";
            this.ITEMNAME.HeaderText = "PART NAME";
            this.ITEMNAME.Name = "ITEMNAME";
            this.ITEMNAME.ReadOnly = true;
            // 
            // PARTNOQTY
            // 
            this.PARTNOQTY.DataPropertyName = "ITEMIDQTY";
            this.PARTNOQTY.FillWeight = 110F;
            this.PARTNOQTY.HeaderText = "PART NO QTY";
            this.PARTNOQTY.Name = "PARTNOQTY";
            this.PARTNOQTY.ReadOnly = true;
            this.PARTNOQTY.Width = 110;
            // 
            // PURCHASEID
            // 
            this.PURCHASEID.DataPropertyName = "PURCHASEID";
            this.PURCHASEID.FillWeight = 110F;
            this.PURCHASEID.HeaderText = "RECEIPT NO";
            this.PURCHASEID.Name = "PURCHASEID";
            this.PURCHASEID.ReadOnly = true;
            this.PURCHASEID.Width = 110;
            // 
            // PURCHASEDATE
            // 
            this.PURCHASEDATE.DataPropertyName = "PURCHASEDATE";
            this.PURCHASEDATE.FillWeight = 125F;
            this.PURCHASEDATE.HeaderText = "PURCHASE DATE";
            this.PURCHASEDATE.Name = "PURCHASEDATE";
            this.PURCHASEDATE.ReadOnly = true;
            this.PURCHASEDATE.Width = 125;
            // 
            // INVOICEID
            // 
            this.INVOICEID.DataPropertyName = "INVOICEID";
            this.INVOICEID.HeaderText = "INVOICE NO";
            this.INVOICEID.Name = "INVOICEID";
            this.INVOICEID.ReadOnly = true;
            this.INVOICEID.Visible = false;
            // 
            // INVOICEDATE
            // 
            this.INVOICEDATE.DataPropertyName = "INVOICEDATE";
            this.INVOICEDATE.HeaderText = "INVOICE DATE";
            this.INVOICEDATE.Name = "INVOICEDATE";
            this.INVOICEDATE.ReadOnly = true;
            this.INVOICEDATE.Visible = false;
            // 
            // QUANTITYRCV
            // 
            this.QUANTITYRCV.DataPropertyName = "QUANTITYRCV";
            this.QUANTITYRCV.FillWeight = 110F;
            this.QUANTITYRCV.HeaderText = "QTY RECEIVE";
            this.QUANTITYRCV.Name = "QUANTITYRCV";
            this.QUANTITYRCV.ReadOnly = true;
            this.QUANTITYRCV.Width = 110;
            // 
            // QUANITITYRTR
            // 
            this.QUANITITYRTR.DataPropertyName = "QUANTITYRTR";
            this.QUANITITYRTR.FillWeight = 110F;
            this.QUANITITYRTR.HeaderText = "QTY RETURN";
            this.QUANITITYRTR.Name = "QUANITITYRTR";
            this.QUANITITYRTR.ReadOnly = true;
            this.QUANITITYRTR.Width = 110;
            // 
            // NO
            // 
            this.NO.DataPropertyName = "NO";
            this.NO.HeaderText = "NO";
            this.NO.Name = "NO";
            this.NO.ReadOnly = true;
            this.NO.Width = 40;
            // 
            // PURCHASERETURNID
            // 
            this.PURCHASERETURNID.DataPropertyName = "PURCHASERETURNID";
            this.PURCHASERETURNID.HeaderText = "RETURN NO";
            this.PURCHASERETURNID.Name = "PURCHASERETURNID";
            this.PURCHASERETURNID.ReadOnly = true;
            this.PURCHASERETURNID.Width = 120;
            // 
            // PURCHASERETURNDATE
            // 
            this.PURCHASERETURNDATE.DataPropertyName = "PURCHASERETURNDATE";
            this.PURCHASERETURNDATE.FillWeight = 120F;
            this.PURCHASERETURNDATE.HeaderText = "RETURN DATE";
            this.PURCHASERETURNDATE.Name = "PURCHASERETURNDATE";
            this.PURCHASERETURNDATE.ReadOnly = true;
            this.PURCHASERETURNDATE.Width = 120;
            // 
            // SUPPLIERID
            // 
            this.SUPPLIERID.DataPropertyName = "SUPPLIERID";
            this.SUPPLIERID.HeaderText = "SUPPLIERID";
            this.SUPPLIERID.Name = "SUPPLIERID";
            this.SUPPLIERID.ReadOnly = true;
            this.SUPPLIERID.Visible = false;
            // 
            // REMARK
            // 
            this.REMARK.DataPropertyName = "REMARK";
            this.REMARK.FillWeight = 150F;
            this.REMARK.HeaderText = "REMARK";
            this.REMARK.Name = "REMARK";
            this.REMARK.ReadOnly = true;
            this.REMARK.Width = 150;
            // 
            // STATUS
            // 
            this.STATUS.DataPropertyName = "STATUS";
            this.STATUS.HeaderText = "STATUS";
            this.STATUS.Name = "STATUS";
            this.STATUS.ReadOnly = true;
            // 
            // CREATEDBY
            // 
            this.CREATEDBY.DataPropertyName = "CREATEDBY";
            this.CREATEDBY.HeaderText = "CREATED BY";
            this.CREATEDBY.Name = "CREATEDBY";
            this.CREATEDBY.ReadOnly = true;
            this.CREATEDBY.Visible = false;
            // 
            // ReturnPurchasedatagridview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(865, 505);
            this.Controls.Add(this.detaildatagridviewpanel);
            this.Controls.Add(this.datagridviewpanel);
            this.Controls.Add(this.FindtextBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReturnPurchasedatagridview";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "List Of Return Purchase";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Purchasedatagridview_FormClosing);
            this.datagridviewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TransactionPdatagridview)).EndInit();
            this.detaildatagridviewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TransactionPOItemdataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FindtextBox;
        private System.Windows.Forms.Panel datagridviewpanel;
        private System.Windows.Forms.DataGridView TransactionPdatagridview;
        private System.Windows.Forms.Panel detaildatagridviewpanel;
        private System.Windows.Forms.DataGridView TransactionPOItemdataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn PARTNOQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn PURCHASEID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PURCHASEDATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn INVOICEID;
        private System.Windows.Forms.DataGridViewTextBoxColumn INVOICEDATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn QUANTITYRCV;
        private System.Windows.Forms.DataGridViewTextBoxColumn QUANITITYRTR;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PURCHASERETURNID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PURCHASERETURNDATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn SUPPLIERID;
        private System.Windows.Forms.DataGridViewTextBoxColumn REMARK;
        private System.Windows.Forms.DataGridViewTextBoxColumn STATUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn CREATEDBY;
    }
}