﻿namespace Sufindo
{
    partial class Employeedatagridview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.FindtextBox = new System.Windows.Forms.TextBox();
            this.datagridviewpanel = new System.Windows.Forms.Panel();
            this.MasterEmployeedatagridview = new System.Windows.Forms.DataGridView();
            this.NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMPLOYEEID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PASSWORD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMPLOYEENAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POSITION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ADDRESS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BOD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOHP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMAIL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JOINDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATEDBY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datagridviewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MasterEmployeedatagridview)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Find";
            // 
            // FindtextBox
            // 
            this.FindtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FindtextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FindtextBox.Location = new System.Drawing.Point(69, 3);
            this.FindtextBox.Name = "FindtextBox";
            this.FindtextBox.Size = new System.Drawing.Size(212, 31);
            this.FindtextBox.TabIndex = 3;
            this.FindtextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FindtextBox_KeyPress);
            // 
            // datagridviewpanel
            // 
            this.datagridviewpanel.Controls.Add(this.MasterEmployeedatagridview);
            this.datagridviewpanel.Location = new System.Drawing.Point(3, 38);
            this.datagridviewpanel.Name = "datagridviewpanel";
            this.datagridviewpanel.Size = new System.Drawing.Size(960, 233);
            this.datagridviewpanel.TabIndex = 7;
            // 
            // MasterEmployeedatagridview
            // 
            this.MasterEmployeedatagridview.AllowUserToAddRows = false;
            this.MasterEmployeedatagridview.AllowUserToDeleteRows = false;
            this.MasterEmployeedatagridview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.MasterEmployeedatagridview.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NO,
            this.EMPLOYEEID,
            this.PASSWORD,
            this.EMPLOYEENAME,
            this.POSITION,
            this.ADDRESS,
            this.BOD,
            this.NOHP,
            this.EMAIL,
            this.JOINDATE,
            this.STATUS,
            this.CREATEDBY});
            this.MasterEmployeedatagridview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MasterEmployeedatagridview.Location = new System.Drawing.Point(0, 0);
            this.MasterEmployeedatagridview.MultiSelect = false;
            this.MasterEmployeedatagridview.Name = "MasterEmployeedatagridview";
            this.MasterEmployeedatagridview.ReadOnly = true;
            this.MasterEmployeedatagridview.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.MasterEmployeedatagridview.Size = new System.Drawing.Size(960, 233);
            this.MasterEmployeedatagridview.TabIndex = 0;
            this.MasterEmployeedatagridview.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MasterCustomerdatagridview_CellDoubleClick);
            this.MasterEmployeedatagridview.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.MasterCustomerdatagridview_ColumnHeaderMouseClick);
            this.MasterEmployeedatagridview.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MasterCustomerdatagridview_KeyDown);
            // 
            // NO
            // 
            this.NO.DataPropertyName = "NO";
            this.NO.HeaderText = "NO";
            this.NO.Name = "NO";
            this.NO.ReadOnly = true;
            this.NO.Width = 40;
            // 
            // EMPLOYEEID
            // 
            this.EMPLOYEEID.DataPropertyName = "EMPLOYEEID";
            this.EMPLOYEEID.HeaderText = "EMPLOYEE ID";
            this.EMPLOYEEID.Name = "EMPLOYEEID";
            this.EMPLOYEEID.ReadOnly = true;
            this.EMPLOYEEID.Width = 120;
            // 
            // PASSWORD
            // 
            this.PASSWORD.DataPropertyName = "PASSWORD";
            this.PASSWORD.HeaderText = "PASSWORD";
            this.PASSWORD.Name = "PASSWORD";
            this.PASSWORD.ReadOnly = true;
            this.PASSWORD.Visible = false;
            // 
            // EMPLOYEENAME
            // 
            this.EMPLOYEENAME.DataPropertyName = "EMPLOYEENAME";
            this.EMPLOYEENAME.HeaderText = "EMPLOYEE NAME";
            this.EMPLOYEENAME.Name = "EMPLOYEENAME";
            this.EMPLOYEENAME.ReadOnly = true;
            this.EMPLOYEENAME.Width = 150;
            // 
            // POSITION
            // 
            this.POSITION.DataPropertyName = "POSITION";
            this.POSITION.HeaderText = "POSITION";
            this.POSITION.Name = "POSITION";
            this.POSITION.ReadOnly = true;
            // 
            // ADDRESS
            // 
            this.ADDRESS.DataPropertyName = "ADDRESS";
            this.ADDRESS.HeaderText = "ADDRESS";
            this.ADDRESS.Name = "ADDRESS";
            this.ADDRESS.ReadOnly = true;
            // 
            // BOD
            // 
            this.BOD.DataPropertyName = "BOD";
            this.BOD.HeaderText = "BOD";
            this.BOD.Name = "BOD";
            this.BOD.ReadOnly = true;
            // 
            // NOHP
            // 
            this.NOHP.DataPropertyName = "NOHP";
            this.NOHP.HeaderText = "NOHP";
            this.NOHP.Name = "NOHP";
            this.NOHP.ReadOnly = true;
            // 
            // EMAIL
            // 
            this.EMAIL.DataPropertyName = "EMAIL";
            this.EMAIL.HeaderText = "EMAIL";
            this.EMAIL.Name = "EMAIL";
            this.EMAIL.ReadOnly = true;
            // 
            // JOINDATE
            // 
            this.JOINDATE.DataPropertyName = "JOINDATE";
            this.JOINDATE.HeaderText = "JOIN DATE";
            this.JOINDATE.Name = "JOINDATE";
            this.JOINDATE.ReadOnly = true;
            // 
            // STATUS
            // 
            this.STATUS.DataPropertyName = "STATUS";
            this.STATUS.HeaderText = "STATUS";
            this.STATUS.Name = "STATUS";
            this.STATUS.ReadOnly = true;
            this.STATUS.Visible = false;
            // 
            // CREATEDBY
            // 
            this.CREATEDBY.DataPropertyName = "CREATEDBY";
            this.CREATEDBY.HeaderText = "CREATEDBY";
            this.CREATEDBY.Name = "CREATEDBY";
            this.CREATEDBY.ReadOnly = true;
            this.CREATEDBY.Visible = false;
            // 
            // Employeedatagridview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(967, 273);
            this.Controls.Add(this.datagridviewpanel);
            this.Controls.Add(this.FindtextBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Employeedatagridview";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "List Of Employee";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Supplierdatagridview_FormClosing);
            this.datagridviewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MasterEmployeedatagridview)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FindtextBox;
        private System.Windows.Forms.Panel datagridviewpanel;
        private System.Windows.Forms.DataGridView MasterEmployeedatagridview;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMPLOYEEID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PASSWORD;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMPLOYEENAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn POSITION;
        private System.Windows.Forms.DataGridViewTextBoxColumn ADDRESS;
        private System.Windows.Forms.DataGridViewTextBoxColumn BOD;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOHP;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMAIL;
        private System.Windows.Forms.DataGridViewTextBoxColumn JOINDATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn STATUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn CREATEDBY;
    }
}