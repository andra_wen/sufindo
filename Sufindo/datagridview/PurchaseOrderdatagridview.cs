﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sufindo.Master;

namespace Sufindo
{
    public partial class PurchaseOrderdatagridview : Form
    {
        public delegate void PurchaseOrderPassingData(DataTable masterItem);

        private delegate void fillDatatableCallBack(DataTable datatable, String called);

        public PurchaseOrderPassingData purchaseOrderPassingData;

        private readonly string[] columnMasterItem = { "PURCHASEORDERID", "PURCHASEORDERDATE" };
        private Connection con;
        private String columnMasterItemFind = "PURCHASEORDERID";

        private String calledForm;

        public PurchaseOrderdatagridview(){
            InitializeComponent();
        }

        public PurchaseOrderdatagridview(String calledForm)
        {
            InitializeComponent();

            if (con == null) {
                con = new Connection();
            }
            this.calledForm = calledForm;
            String query;
            if(this.calledForm.Equals("PurchaseReceipt")){
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY PURCHASEORDERID) AS 'NO', PURCHASEORDERID, CONVERT(VARCHAR(10), PURCHASEORDERDATE, 103) AS [PURCHASEORDERDATE], " +
                                      "A.SUPPLIERID, SUPPLIERNAME, CONTACTPERSONSUPPLIER, KURSID, RATE, A.STATUS, REMARK, A.CREATEDBY " +
                                      "FROM H_PURCHASE_ORDER A " +
                                      "JOIN M_SUPPLIER B " +
                                      "ON A.SUPPLIERID = B.SUPPLIERID " +
                                      "WHERE A.STATUS = '{0}' " +
                                      "ORDER BY PURCHASEORDERID ASC", Status.OPEN);
            }
            else{
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY PURCHASEORDERID) AS 'NO', PURCHASEORDERID, CONVERT(VARCHAR(10), PURCHASEORDERDATE, 103) AS [PURCHASEORDERDATE], " +
                                      "A.SUPPLIERID, SUPPLIERNAME, CONTACTPERSONSUPPLIER, KURSID, RATE, A.STATUS, REMARK, A.CREATEDBY " +
                                      "FROM H_PURCHASE_ORDER A " +
                                      "JOIN M_SUPPLIER B " +
                                      "ON A.SUPPLIERID = B.SUPPLIERID " +
                                      "WHERE A.STATUS = '{0}' " +
                                      "ORDER BY PURCHASEORDERID ASC",Status.OPEN);
            }
            createBackgroundWorkerFillDatagridView(query, "TransactionPOdatagridview");
            int authenticationprice = AuthorizeUser.sharedInstance.authenticationprice.Rows.Count;
            UNITPRICE.Visible = authenticationprice == 0 ? false : 
                AuthorizeUser.sharedInstance.authenticationprice.Rows[1]["VISIBLE"].ToString().Equals("1") ? true : false;
            AMOUNT.Visible = UNITPRICE.Visible;
        }

        void createBackgroundWorkerFillDatagridView(String query, String called)
        {

            for (int i = 0; i < TransactionPOdatagridview.Columns.Count; i++)
            {
                if (TransactionPOdatagridview.Columns[i].DataPropertyName.Equals(columnMasterItemFind))
                {
                    DataGridViewColumn dataGridViewColumn = TransactionPOdatagridview.Columns[i];
                    dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                    break;
                }
            }

            String[] argument = { query, called };
            
            BackgroundWorker bgworker = new BackgroundWorker();
            bgworker.DoWork += new DoWorkEventHandler(bgworker_DoWork);
            bgworker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgworker_RunWorkerCompleted);
            bgworker.RunWorkerAsync(argument);
        }

        void bgworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {

        }

        void bgworker_DoWork(object sender, DoWorkEventArgs e) {
            try
            {
                String[] obj = e.Argument as String[];
                String query = (String) obj[0];
                String called = (String) obj[1];

                BeginInvoke(new fillDatatableCallBack(this.fillDatatable), con.openDataTableQuery(query), called);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void fillDatatable(DataTable datatable, String called)
        {
            if (called.Equals("TransactionPOdatagridview"))
            {
                TransactionPOdatagridview.DataSource = datatable;
                this.showTransactionPOItemdataGridView();
            }
            else if (called.Equals("TransactionPOItemdataGridView"))
            {
                TransactionPOItemdataGridView.DataSource = datatable;
                Double result = 0;

                for (int i = 0; i < TransactionPOItemdataGridView.RowCount; i++)
                {
                    result += Double.Parse(TransactionPOItemdataGridView.Rows[i].Cells["AMOUNT"].Value.ToString());
                }
                TotaltextBox.Text = result.ToString();
            }
        }

        #region Method TransactionPOdatagridview
        private void TransactionPOdatagridview_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (purchaseOrderPassingData != null)
                {
                    TransactionPOdatagridviewCallPassingData(e.RowIndex);
                }
            }
        }

        private void TransactionPOdatagridview_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                foreach (String item in columnMasterItem)
                {
                    if (TransactionPOdatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        for (int i = 0; i < TransactionPOdatagridview.Columns.Count; i++)
                        {
                            if (TransactionPOdatagridview.Columns[i].DataPropertyName.Equals(columnMasterItemFind))
                            {
                                DataGridViewColumn dataGridViewColumn = TransactionPOdatagridview.Columns[i];
                                dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Control;
                            }
                        }
                        break;
                    }
                }

                foreach (String item in columnMasterItem)
                {
                    if (TransactionPOdatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        columnMasterItemFind = TransactionPOdatagridview.Columns[e.ColumnIndex].DataPropertyName;
                        DataGridViewColumn dataGridViewColumn = TransactionPOdatagridview.Columns[e.ColumnIndex];
                        dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                        break;
                    }
                }
            }
        }

        private void TransactionPOdatagridview_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                if (TransactionPOdatagridview.Rows.Count != 0)
                {
                    e.SuppressKeyPress = true;
                    int currentColumn = TransactionPOdatagridview.CurrentCell.ColumnIndex;
                    int currentRow = TransactionPOdatagridview.CurrentCell.RowIndex;
                    TransactionPOdatagridview.CurrentCell = TransactionPOdatagridview[currentColumn, currentRow];

                    TransactionPOdatagridviewCallPassingData(currentRow);
                }
            }
            else
            {
                if (!(e.KeyCode == Keys.Down || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right || e.KeyCode == Keys.Up))
                {
                    if (char.IsLetterOrDigit((char)e.KeyCode))
                    {
                        char keysCode = (char)e.KeyCode;

                        FindtextBox.Focus();
                        FindtextBox.Text = keysCode.ToString();
                        FindtextBox.SelectionStart = FindtextBox.Text.Length;
                    }
                }
                else
                {
                    int currentRow = TransactionPOdatagridview.CurrentCell.RowIndex;
                    TransactionPOdatagridview.Rows[currentRow].Selected = true;
                }
            }
        }

        private void TransactionPOdatagridview_KeyUp(object sender, KeyEventArgs e)
        {
            this.showTransactionPOItemdataGridView();
            //int currentRow = TransactionPOdatagridview.CurrentCell.RowIndex;
            //String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'No', A.ITEMID, ITEMNAME, A.QUANTITY, UOMID, PRICE, (A.QUANTITY * PRICE) AS AMOUNT " +
            //                             "FROM D_PURCHASE_ORDER A " +
            //                             "JOIN M_ITEM B " +
            //                             "ON A.ITEMID = B.ITEMID " +
            //                             "WHERE PURCHASEORDERID = '{0}'", TransactionPOdatagridview.Rows[currentRow].Cells[1].Value.ToString());
            //createBackgroundWorkerFillDatagridView(query, "TransactionPOItemdataGridView");
        }

        private void TransactionPOdatagridview_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                this.showTransactionPOItemdataGridView();
            }
        }

        private void showTransactionPOItemdataGridView() {
            if (TransactionPOdatagridview.Rows.Count == 0)
            {
                while (this.TransactionPOItemdataGridView.Rows.Count > 0)
                {
                    this.TransactionPOItemdataGridView.Rows.RemoveAt(0);
                }
                return;
            }
            int currentRow = TransactionPOdatagridview.CurrentCell.RowIndex;
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'No', A.ITEMID, ITEMNAME, A.QUANTITY, UOMID, PRICE, (A.QUANTITY * PRICE) AS AMOUNT " +
                                         "FROM D_PURCHASE_ORDER A " +
                                         "JOIN M_ITEM B " +
                                         "ON A.ITEMID = B.ITEMID " +
                                         "WHERE PURCHASEORDERID = '{0}'", TransactionPOdatagridview.Rows[currentRow].Cells[1].Value.ToString());
            createBackgroundWorkerFillDatagridView(query, "TransactionPOItemdataGridView");
        }

        #endregion

        private void TransactionPOdatagridviewCallPassingData(int currentRow) {

            DataTable datatable = new DataTable();
            for (int i = 0; i < TransactionPOdatagridview.Columns.Count; i++){
                datatable.Columns.Add(TransactionPOdatagridview.Columns[i].DataPropertyName);
            }
            
            /*check multiple selected or not*/
            if (TransactionPOdatagridview.SelectedRows.Count != 0){
                foreach (var item in TransactionPOdatagridview.SelectedRows){
                    DataRow r = datatable.NewRow();
                    DataGridViewRow row = (DataGridViewRow)item;
                    for (int i = 0; i < TransactionPOdatagridview.Columns.Count; i++){
                        r[i] = row.Cells[i].Value;
                    }
                    datatable.Rows.Add(r);
                }
            }
            else{
                DataRow r = datatable.NewRow();
                DataGridViewRow row = TransactionPOdatagridview.Rows[currentRow];
                for (int i = 0; i < TransactionPOdatagridview.Columns.Count; i++){
                    r[i] = row.Cells[i].Value;
                }
                datatable.Rows.Add(r);
            }
            purchaseOrderPassingData(datatable);
            this.Close();
        }

        #region Method FindTextBox
        private void FindtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) {
                
                String strFind = FindtextBox.Text.Contains('*') ? FindtextBox.Text.Replace('*', '%') : (FindtextBox.Text + '%');

                String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY PURCHASEORDERID) AS 'NO', PURCHASEORDERID, CONVERT(VARCHAR(10), PURCHASEORDERDATE, 103) AS [PURCHASEORDERDATE], " +
                                             "A.SUPPLIERID, SUPPLIERNAME, CONTACTPERSONSUPPLIER, KURSID, RATE, A.STATUS, REMARK, A.CREATEDBY " +
                                             "FROM H_PURCHASE_ORDER A " +
                                             "JOIN M_SUPPLIER B " +
                                             "ON A.SUPPLIERID = B.SUPPLIERID " +
                                             "WHERE {0} LIKE '{1}' AND A.STATUS <> '{2}' " +
                                             "ORDER BY PURCHASEORDERID ASC",columnMasterItemFind, strFind, Status.DELETE);

                createBackgroundWorkerFillDatagridView(query, "TransactionPOdatagridview");
            }
        }
        #endregion

        private void PurchaseOrderdatagridview_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }

        //protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        //{
        //    foreach (Keys arrows in arrowsList)
        //    {
        //        if (keyData == arrows) { 
                    
        //        }
        //    }

        //    if (keyData == (Keys.Control | Keys.F))
        //    {
        //        MessageBox.Show("What the Ctrl+F?");
        //        return true;
        //    }
        //    return base.ProcessCmdKey(ref msg, keyData);
        //}
    }
}
