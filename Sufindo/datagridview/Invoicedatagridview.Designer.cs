﻿namespace Sufindo
{
    partial class Invoicedatagridview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.FindtextBox = new System.Windows.Forms.TextBox();
            this.datagridviewpanel = new System.Windows.Forms.Panel();
            this.TransactionInvoicedatagridview = new System.Windows.Forms.DataGridView();
            this.ContactPersonpanel = new System.Windows.Forms.Panel();
            this.TransactionInvoiceItemdataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMIDQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PARTNAMEQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUANTITY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UNITPRICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICEID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICEDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SALESORDERID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SALESORDERDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMERID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMERNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCYID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAYMENTMETHOD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DISCOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REMARK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATEDBY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMPLOYEENAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datagridviewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionInvoicedatagridview)).BeginInit();
            this.ContactPersonpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionInvoiceItemdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Find";
            // 
            // FindtextBox
            // 
            this.FindtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FindtextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FindtextBox.Location = new System.Drawing.Point(69, 2);
            this.FindtextBox.Name = "FindtextBox";
            this.FindtextBox.Size = new System.Drawing.Size(212, 31);
            this.FindtextBox.TabIndex = 3;
            this.FindtextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FindtextBox_KeyPress);
            // 
            // datagridviewpanel
            // 
            this.datagridviewpanel.Controls.Add(this.TransactionInvoicedatagridview);
            this.datagridviewpanel.Location = new System.Drawing.Point(3, 36);
            this.datagridviewpanel.Name = "datagridviewpanel";
            this.datagridviewpanel.Size = new System.Drawing.Size(942, 233);
            this.datagridviewpanel.TabIndex = 7;
            // 
            // TransactionInvoicedatagridview
            // 
            this.TransactionInvoicedatagridview.AllowUserToAddRows = false;
            this.TransactionInvoicedatagridview.AllowUserToDeleteRows = false;
            this.TransactionInvoicedatagridview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.TransactionInvoicedatagridview.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NO,
            this.INVOICEID,
            this.INVOICEDATE,
            this.SALESORDERID,
            this.SALESORDERDATE,
            this.CUSTOMERID,
            this.CUSTOMERNAME,
            this.CURRENCYID,
            this.RATE,
            this.PAYMENTMETHOD,
            this.DISCOUNT,
            this.COST,
            this.TOTAL,
            this.REMARK,
            this.STATUS,
            this.CREATEDBY,
            this.EMPLOYEENAME});
            this.TransactionInvoicedatagridview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TransactionInvoicedatagridview.Location = new System.Drawing.Point(0, 0);
            this.TransactionInvoicedatagridview.MultiSelect = false;
            this.TransactionInvoicedatagridview.Name = "TransactionInvoicedatagridview";
            this.TransactionInvoicedatagridview.ReadOnly = true;
            this.TransactionInvoicedatagridview.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TransactionInvoicedatagridview.Size = new System.Drawing.Size(942, 233);
            this.TransactionInvoicedatagridview.TabIndex = 0;
            this.TransactionInvoicedatagridview.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TransactionSOdatagridview_CellDoubleClick);
            this.TransactionInvoicedatagridview.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.TransactionSOdatagridview_ColumnHeaderMouseClick);
            this.TransactionInvoicedatagridview.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TransactionSOdatagridview_CellClick);
            this.TransactionInvoicedatagridview.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TransactionSOdatagridview_KeyDown);
            this.TransactionInvoicedatagridview.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TransactionSOdatagridview_KeyUp);
            // 
            // ContactPersonpanel
            // 
            this.ContactPersonpanel.Controls.Add(this.TransactionInvoiceItemdataGridView);
            this.ContactPersonpanel.Location = new System.Drawing.Point(3, 278);
            this.ContactPersonpanel.Name = "ContactPersonpanel";
            this.ContactPersonpanel.Size = new System.Drawing.Size(811, 226);
            this.ContactPersonpanel.TabIndex = 24;
            // 
            // TransactionInvoiceItemdataGridView
            // 
            this.TransactionInvoiceItemdataGridView.AllowUserToAddRows = false;
            this.TransactionInvoiceItemdataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TransactionInvoiceItemdataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.TransactionInvoiceItemdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.TransactionInvoiceItemdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.ITEMID,
            this.ITEMNAME,
            this.ITEMIDQTY,
            this.PARTNAMEQTY,
            this.QUANTITY,
            this.UOMID,
            this.UNITPRICE,
            this.AMOUNT});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.TransactionInvoiceItemdataGridView.DefaultCellStyle = dataGridViewCellStyle7;
            this.TransactionInvoiceItemdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TransactionInvoiceItemdataGridView.Location = new System.Drawing.Point(0, 0);
            this.TransactionInvoiceItemdataGridView.Name = "TransactionInvoiceItemdataGridView";
            this.TransactionInvoiceItemdataGridView.ReadOnly = true;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TransactionInvoiceItemdataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.TransactionInvoiceItemdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TransactionInvoiceItemdataGridView.Size = new System.Drawing.Size(811, 226);
            this.TransactionInvoiceItemdataGridView.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "NO";
            this.dataGridViewTextBoxColumn1.FillWeight = 40F;
            this.dataGridViewTextBoxColumn1.HeaderText = "NO";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 40;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 40;
            // 
            // ITEMID
            // 
            this.ITEMID.DataPropertyName = "ITEMID";
            this.ITEMID.HeaderText = "PART NO";
            this.ITEMID.Name = "ITEMID";
            this.ITEMID.ReadOnly = true;
            // 
            // ITEMNAME
            // 
            this.ITEMNAME.DataPropertyName = "ITEMNAME";
            this.ITEMNAME.HeaderText = "PART NAME";
            this.ITEMNAME.Name = "ITEMNAME";
            this.ITEMNAME.ReadOnly = true;
            // 
            // ITEMIDQTY
            // 
            this.ITEMIDQTY.DataPropertyName = "ITEMIDQTY";
            this.ITEMIDQTY.FillWeight = 105F;
            this.ITEMIDQTY.HeaderText = "PART NO QTY";
            this.ITEMIDQTY.Name = "ITEMIDQTY";
            this.ITEMIDQTY.ReadOnly = true;
            this.ITEMIDQTY.Width = 105;
            // 
            // PARTNAMEQTY
            // 
            this.PARTNAMEQTY.DataPropertyName = "ITEMNAMEQTY";
            this.PARTNAMEQTY.FillWeight = 120F;
            this.PARTNAMEQTY.HeaderText = "PART NAME QTY";
            this.PARTNAMEQTY.Name = "PARTNAMEQTY";
            this.PARTNAMEQTY.ReadOnly = true;
            this.PARTNAMEQTY.Width = 120;
            // 
            // QUANTITY
            // 
            this.QUANTITY.DataPropertyName = "QUANTITY";
            this.QUANTITY.HeaderText = "QUANTITY";
            this.QUANTITY.Name = "QUANTITY";
            this.QUANTITY.ReadOnly = true;
            // 
            // UOMID
            // 
            this.UOMID.DataPropertyName = "UOMID";
            this.UOMID.HeaderText = "UOM";
            this.UOMID.Name = "UOMID";
            this.UOMID.ReadOnly = true;
            // 
            // UNITPRICE
            // 
            this.UNITPRICE.DataPropertyName = "PRICE";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle5.Format = "N2";
            this.UNITPRICE.DefaultCellStyle = dataGridViewCellStyle5;
            this.UNITPRICE.HeaderText = "UNIT PRICE";
            this.UNITPRICE.Name = "UNITPRICE";
            this.UNITPRICE.ReadOnly = true;
            // 
            // AMOUNT
            // 
            this.AMOUNT.DataPropertyName = "AMOUNT";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle6.Format = "N2";
            this.AMOUNT.DefaultCellStyle = dataGridViewCellStyle6;
            this.AMOUNT.HeaderText = "AMOUNT";
            this.AMOUNT.Name = "AMOUNT";
            this.AMOUNT.ReadOnly = true;
            this.AMOUNT.Visible = false;
            // 
            // NO
            // 
            this.NO.DataPropertyName = "NO";
            this.NO.HeaderText = "NO";
            this.NO.Name = "NO";
            this.NO.ReadOnly = true;
            this.NO.Width = 40;
            // 
            // INVOICEID
            // 
            this.INVOICEID.DataPropertyName = "INVOICEID";
            this.INVOICEID.HeaderText = "INVOICE NO";
            this.INVOICEID.Name = "INVOICEID";
            this.INVOICEID.ReadOnly = true;
            this.INVOICEID.Width = 80;
            // 
            // INVOICEDATE
            // 
            this.INVOICEDATE.DataPropertyName = "INVOICEDATE";
            this.INVOICEDATE.HeaderText = "INVOICE DATE";
            this.INVOICEDATE.Name = "INVOICEDATE";
            this.INVOICEDATE.ReadOnly = true;
            // 
            // SALESORDERID
            // 
            this.SALESORDERID.DataPropertyName = "SALESORDERID";
            this.SALESORDERID.HeaderText = "SO NO";
            this.SALESORDERID.Name = "SALESORDERID";
            this.SALESORDERID.ReadOnly = true;
            this.SALESORDERID.Width = 80;
            // 
            // SALESORDERDATE
            // 
            this.SALESORDERDATE.DataPropertyName = "SALESORDERDATE";
            this.SALESORDERDATE.FillWeight = 90F;
            this.SALESORDERDATE.HeaderText = "SO DATE";
            this.SALESORDERDATE.Name = "SALESORDERDATE";
            this.SALESORDERDATE.ReadOnly = true;
            this.SALESORDERDATE.Width = 80;
            // 
            // CUSTOMERID
            // 
            this.CUSTOMERID.DataPropertyName = "CUSTOMERID";
            this.CUSTOMERID.HeaderText = "CUSTOMER ID";
            this.CUSTOMERID.Name = "CUSTOMERID";
            this.CUSTOMERID.ReadOnly = true;
            this.CUSTOMERID.Visible = false;
            // 
            // CUSTOMERNAME
            // 
            this.CUSTOMERNAME.DataPropertyName = "CUSTOMERNAME";
            this.CUSTOMERNAME.HeaderText = "CUSTOMER NAME";
            this.CUSTOMERNAME.Name = "CUSTOMERNAME";
            this.CUSTOMERNAME.ReadOnly = true;
            this.CUSTOMERNAME.Width = 110;
            // 
            // CURRENCYID
            // 
            this.CURRENCYID.DataPropertyName = "CURRENCYID";
            this.CURRENCYID.HeaderText = "CURRENCY";
            this.CURRENCYID.Name = "CURRENCYID";
            this.CURRENCYID.ReadOnly = true;
            this.CURRENCYID.Width = 80;
            // 
            // RATE
            // 
            this.RATE.DataPropertyName = "RATE";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle1.Format = "N2";
            this.RATE.DefaultCellStyle = dataGridViewCellStyle1;
            this.RATE.HeaderText = "RATE";
            this.RATE.Name = "RATE";
            this.RATE.ReadOnly = true;
            this.RATE.Width = 80;
            // 
            // PAYMENTMETHOD
            // 
            this.PAYMENTMETHOD.DataPropertyName = "PAYMENTMETHOD";
            this.PAYMENTMETHOD.FillWeight = 140F;
            this.PAYMENTMETHOD.HeaderText = "PAYMENT METHOD";
            this.PAYMENTMETHOD.Name = "PAYMENTMETHOD";
            this.PAYMENTMETHOD.ReadOnly = true;
            this.PAYMENTMETHOD.Width = 140;
            // 
            // DISCOUNT
            // 
            this.DISCOUNT.DataPropertyName = "DISCOUNT";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle2.Format = "N2";
            this.DISCOUNT.DefaultCellStyle = dataGridViewCellStyle2;
            this.DISCOUNT.HeaderText = "DISCOUNT";
            this.DISCOUNT.Name = "DISCOUNT";
            this.DISCOUNT.ReadOnly = true;
            this.DISCOUNT.Width = 80;
            // 
            // COST
            // 
            this.COST.DataPropertyName = "COST";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle3.Format = "N2";
            this.COST.DefaultCellStyle = dataGridViewCellStyle3;
            this.COST.HeaderText = "COST";
            this.COST.Name = "COST";
            this.COST.ReadOnly = true;
            this.COST.Width = 80;
            // 
            // TOTAL
            // 
            this.TOTAL.DataPropertyName = "TOTAL";
            this.TOTAL.HeaderText = "TOTAL";
            this.TOTAL.Name = "TOTAL";
            this.TOTAL.ReadOnly = true;
            this.TOTAL.Visible = false;
            // 
            // REMARK
            // 
            this.REMARK.DataPropertyName = "REMARK";
            this.REMARK.HeaderText = "REMARK";
            this.REMARK.Name = "REMARK";
            this.REMARK.ReadOnly = true;
            // 
            // STATUS
            // 
            this.STATUS.DataPropertyName = "STATUS";
            this.STATUS.HeaderText = "STATUS";
            this.STATUS.Name = "STATUS";
            this.STATUS.ReadOnly = true;
            this.STATUS.Width = 80;
            // 
            // CREATEDBY
            // 
            this.CREATEDBY.DataPropertyName = "CREATEDBY";
            this.CREATEDBY.HeaderText = "CREATED BY";
            this.CREATEDBY.Name = "CREATEDBY";
            this.CREATEDBY.ReadOnly = true;
            // 
            // EMPLOYEENAME
            // 
            this.EMPLOYEENAME.DataPropertyName = "EMPLOYEENAME";
            this.EMPLOYEENAME.HeaderText = "EMPLOYEE NAME";
            this.EMPLOYEENAME.Name = "EMPLOYEENAME";
            this.EMPLOYEENAME.ReadOnly = true;
            this.EMPLOYEENAME.Visible = false;
            // 
            // Invoicedatagridview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(948, 507);
            this.Controls.Add(this.ContactPersonpanel);
            this.Controls.Add(this.datagridviewpanel);
            this.Controls.Add(this.FindtextBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Invoicedatagridview";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "List Of Invoice";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SalesOrderdatagridview_FormClosing);
            this.datagridviewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TransactionInvoicedatagridview)).EndInit();
            this.ContactPersonpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TransactionInvoiceItemdataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FindtextBox;
        private System.Windows.Forms.Panel datagridviewpanel;
        private System.Windows.Forms.DataGridView TransactionInvoicedatagridview;
        private System.Windows.Forms.Panel ContactPersonpanel;
        private System.Windows.Forms.DataGridView TransactionInvoiceItemdataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMIDQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn PARTNAMEQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn QUANTITY;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn UNITPRICE;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn INVOICEID;
        private System.Windows.Forms.DataGridViewTextBoxColumn INVOICEDATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn SALESORDERID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SALESORDERDATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUSTOMERID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUSTOMERNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CURRENCYID;
        private System.Windows.Forms.DataGridViewTextBoxColumn RATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAYMENTMETHOD;
        private System.Windows.Forms.DataGridViewTextBoxColumn DISCOUNT;
        private System.Windows.Forms.DataGridViewTextBoxColumn COST;
        private System.Windows.Forms.DataGridViewTextBoxColumn TOTAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn REMARK;
        private System.Windows.Forms.DataGridViewTextBoxColumn STATUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn CREATEDBY;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMPLOYEENAME;
    }
}