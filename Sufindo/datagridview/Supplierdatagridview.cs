﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sufindo.Master;

namespace Sufindo
{
    public partial class Supplierdatagridview : Form
    {
        public delegate void MasterItemPassingData(DataTable masterItem);

        private delegate void fillDatatableCallBack(DataTable datatable, String called);

        public MasterItemPassingData masterItemPassingData;

        private readonly string[] columnMasterItem = { "SUPPLIERID", "SUPPLIERNAME"};
        private Connection con;
        private String columnMasterItemFind = "SUPPLIERID";

        private String calledForm;
        private String partID;
        public Supplierdatagridview(){
            InitializeComponent();
        }

        public Supplierdatagridview(String calledForm) {
            InitializeComponent();

            if (con == null) {
                con = new Connection();
            }
            this.calledForm = calledForm;
            String query =  String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SUPPLIERID) AS 'No', SUPPLIERID, SUPPLIERNAME, " +
                                          "[ADDRESS], ZONE, NOTELP, FAX " +
                                          "FROM M_SUPPLIER " +
                                          "WHERE STATUS = '{0}' " +
                                          "ORDER BY SUPPLIERID ASC", Status.ACTIVE);
            createBackgroundWorkerFillDatagridView(query, "MasterSupplierdatagridview");
        }

        public Supplierdatagridview(String calledForm, String partID) {
            InitializeComponent();

            if (con == null)
            {
                con = new Connection();
            }
            this.calledForm = calledForm;
            this.partID = partID;
            String query = String.Format("SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY A.SUPPLIERID) AS 'No', A.SUPPLIERID, SUPPLIERNAME, " +
                                         "ADDRESS, ZONE, NOTELP, FAX FROM H_PURCHASE A " +
                                         "JOIN D_PURCHASE B " +
                                         "ON A.PURCHASEID = B.PURCHASEID " +
                                         "JOIN M_SUPPLIER C " +
                                         "ON A.SUPPLIERID = C.SUPPLIERID " +
                                         "WHERE B.ITEMID = '{0}' " +
                                         "AND " +
                                         "( " +
                                         "  A.PURCHASEID NOT IN " +
                                         "  ( " +
                                         "      SELECT PURCHASEID FROM H_RETURN_PURCHASE A " +
                                         "      JOIN D_RETURN_PURCHASE B " +
                                         "      ON A.RETURNPURCHASEID = B.RETURNPURCHASEID " +
                                         "      WHERE STATUS = '{1}' " +
                                         "  ) " +
                                         "  OR B.ITEMID NOT IN " +
                                         "  ( " +
                                         "      SELECT ITEMID FROM H_RETURN_PURCHASE A " +
                                         "      JOIN D_RETURN_PURCHASE B " +
                                         "      ON A.RETURNPURCHASEID = B.RETURNPURCHASEID " +
                                         "      WHERE STATUS = '{1}' " +
                                         "  ) " +
                                         ") " +
                                         "AND C.STATUS = '{1}' ", partID, Status.ACTIVE);
            createBackgroundWorkerFillDatagridView(query, "MasterSupplierdatagridview");
        }

        void createBackgroundWorkerFillDatagridView(String query, String called)
        {

            for (int i = 0; i < MasterSupplierdatagridview.Columns.Count; i++)
            {
                if (MasterSupplierdatagridview.Columns[i].DataPropertyName.Equals(columnMasterItemFind))
                {
                    DataGridViewColumn dataGridViewColumn = MasterSupplierdatagridview.Columns[i];
                    dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                    break;
                }
            }

            String[] argument = { query, called };
            
            BackgroundWorker bgworker = new BackgroundWorker();
            bgworker.DoWork += new DoWorkEventHandler(bgworker_DoWork);
            bgworker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgworker_RunWorkerCompleted);
            bgworker.RunWorkerAsync(argument);
        }

        void bgworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {

        }

        void bgworker_DoWork(object sender, DoWorkEventArgs e) {
            try
            {
                String[] obj = e.Argument as String[];
                String query = (String) obj[0];
                String called = (String) obj[1];

                BeginInvoke(new fillDatatableCallBack(this.fillDatatable), con.openDataTableQuery(query), called);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void fillDatatable(DataTable datatable, String called)
        {
            if (called.Equals("MasterSupplierdatagridview"))
            {
                MasterSupplierdatagridview.DataSource = datatable;
                this.showPersondataGridView();
            }
            else if (called.Equals("PersondataGridView"))
            {
                PersondataGridView.DataSource = datatable;
            }
        }

        #region Method MasterSupplierdatagridview
        private void MasterSupplierdatagridview_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (masterItemPassingData != null)
                {
                    MasterSupplierdatagridviewCallPassingData(e.RowIndex);
                }
            }
        }

        private void MasterSupplierdatagridview_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                foreach (String item in columnMasterItem)
                {
                    if (MasterSupplierdatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        for (int i = 0; i < MasterSupplierdatagridview.Columns.Count; i++)
                        {
                            if (MasterSupplierdatagridview.Columns[i].DataPropertyName.Equals(columnMasterItemFind))
                            {
                                DataGridViewColumn dataGridViewColumn = MasterSupplierdatagridview.Columns[i];
                                dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Control;
                            }
                        }
                        break;
                    }
                }

                foreach (String item in columnMasterItem)
                {
                    if (MasterSupplierdatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        columnMasterItemFind = MasterSupplierdatagridview.Columns[e.ColumnIndex].DataPropertyName;
                        DataGridViewColumn dataGridViewColumn = MasterSupplierdatagridview.Columns[e.ColumnIndex];
                        dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                        break;
                    }
                }
            }
        }

        private void MasterSupplierdatagridview_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                if (MasterSupplierdatagridview.Rows.Count != 0)
                {
                    e.SuppressKeyPress = true;
                    int currentColumn = MasterSupplierdatagridview.CurrentCell.ColumnIndex;
                    int currentRow = MasterSupplierdatagridview.CurrentCell.RowIndex;
                    MasterSupplierdatagridview.CurrentCell = MasterSupplierdatagridview[currentColumn, currentRow];

                    MasterSupplierdatagridviewCallPassingData(currentRow);
                }
            }
            else
            {
                if (!(e.KeyCode == Keys.Down || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right || e.KeyCode == Keys.Up))
                {
                    if (char.IsLetterOrDigit((char)e.KeyCode))
                    {
                        char keysCode = (char)e.KeyCode;

                        FindtextBox.Focus();
                        FindtextBox.Text = keysCode.ToString();
                        FindtextBox.SelectionStart = FindtextBox.Text.Length;
                    }
                }
                else
                {
                    int currentRow = MasterSupplierdatagridview.CurrentCell.RowIndex;
                    MasterSupplierdatagridview.Rows[currentRow].Selected = true;
                }
            }
        }

        private void MasterSupplierdatagridview_KeyUp(object sender, KeyEventArgs e)
        {
            this.showPersondataGridView();
        }

        private void MasterSupplierdatagridview_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                this.showPersondataGridView();
            }
        }

        private void showPersondataGridView() {
            if (MasterSupplierdatagridview.Rows.Count == 0)
            {
                while (this.PersondataGridView.Rows.Count > 0)
                {
                    this.PersondataGridView.Rows.RemoveAt(0);
                }
                return;
            }
            int currentRow = MasterSupplierdatagridview.CurrentCell.RowIndex;

            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SUPPLIERID) AS 'No', CONTACTPERSON, NOPHONE, EMAIL, POSITION " +
                                         "FROM D_PERSON_SUPPLIER " +
                                         "WHERE SUPPLIERID = '{0}'", MasterSupplierdatagridview.Rows[currentRow].Cells[1].Value.ToString());
            createBackgroundWorkerFillDatagridView(query, "PersondataGridView");
        }

        #endregion

        private void MasterSupplierdatagridviewCallPassingData(int currentRow) {
            if (PersondataGridView.CurrentCell != null)
            {
                int currRowPeson = PersondataGridView.CurrentCell.RowIndex;

                DataTable datatable = new DataTable();
                for (int i = 0; i < MasterSupplierdatagridview.Columns.Count; i++)
                {
                    datatable.Columns.Add(MasterSupplierdatagridview.Columns[i].DataPropertyName);
                }

                /*check multiple selected or not*/
                if (MasterSupplierdatagridview.SelectedRows.Count != 0)
                {
                    foreach (var item in MasterSupplierdatagridview.SelectedRows)
                    {
                        DataRow r = datatable.NewRow();
                        DataGridViewRow row = (DataGridViewRow)item;
                        for (int i = 0; i < MasterSupplierdatagridview.Columns.Count; i++)
                        {
                            r[i] = row.Cells[i].Value;
                        }
                        datatable.Rows.Add(r);
                    }
                    Boolean flagColumn = false;
                    foreach (DataColumn column in datatable.Columns)
                    {
                        if (column.ColumnName.Equals("CONTACTPERSON"))
                        {
                            flagColumn = true;
                            break;
                        }
                    }
                    if (!flagColumn) datatable.Columns.Add("CONTACTPERSON");
                    datatable.Rows[0]["CONTACTPERSON"] = PersondataGridView.Rows[currRowPeson].Cells["ContactPerson"].Value;
                }
                else
                {
                    DataRow r = datatable.NewRow();
                    DataGridViewRow row = MasterSupplierdatagridview.Rows[currentRow];
                    for (int i = 0; i < MasterSupplierdatagridview.Columns.Count; i++)
                    {
                        r[i] = row.Cells[i].Value;
                    }
                    datatable.Rows.Add(r);

                }
                masterItemPassingData(datatable);
                this.Close();
            }
        }

        #region Method FindTextBox
        private void FindtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) {
                
                String strFind = FindtextBox.Text.Contains('*') ? FindtextBox.Text.Replace('*', '%') : (FindtextBox.Text + '%');
                String query = "";
                if (!calledForm.Equals("PURCHASERETURN"))
                {
                    query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SUPPLIERID) AS 'NO', SUPPLIERID, SUPPLIERNAME, " +
                                          "[ADDRESS], ZONE, NOTELP, FAX " +
                                          "FROM M_SUPPLIER " +
                                          "WHERE {0} LIKE '{1}' AND STATUS = '{2}' " +
                                          "ORDER BY SUPPLIERID ASC", columnMasterItemFind, strFind, Status.ACTIVE);
                }
                else {
                    query = String.Format("SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY A.SUPPLIERID) AS 'No', A.SUPPLIERID, SUPPLIERNAME, " +
                                         "ADDRESS, ZONE, NOTELP, FAX FROM H_PURCHASE A " +
                                         "JOIN D_PURCHASE B " +
                                         "ON A.PURCHASEID = B.PURCHASEID " +
                                         "JOIN M_SUPPLIER C " +
                                         "ON A.SUPPLIERID = C.SUPPLIERID " +
                                         "WHERE C.{2} LIKE '{3}' AND B.ITEMID = '{0}' " +
                                         "AND " +
                                         "( " +
                                         "  A.PURCHASEID NOT IN " +
                                         "  ( " +
                                         "      SELECT PURCHASEID FROM H_RETURN_PURCHASE A " +
                                         "      JOIN D_RETURN_PURCHASE B " +
                                         "      ON A.RETURNPURCHASEID = B.RETURNPURCHASEID " +
                                         "      WHERE STATUS = '{1}' " +
                                         "  ) " +
                                         "  OR B.ITEMID NOT IN " +
                                         "  ( " +
                                         "      SELECT ITEMID FROM H_RETURN_PURCHASE A " +
                                         "      JOIN D_RETURN_PURCHASE B " +
                                         "      ON A.RETURNPURCHASEID = B.RETURNPURCHASEID " +
                                         "      WHERE STATUS = '{1}' " +
                                         "  ) " +
                                         ") AND C.STATUS = '{1}' ", partID, Status.ACTIVE, columnMasterItemFind, strFind);
                }
                
                createBackgroundWorkerFillDatagridView(query, "MasterSupplierdatagridview");
            }
        }

        private void MasterSupplierdatagridview_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            //if (MasterSupplierdatagridview.Rows.Count != 0)
            //{
            //    int currentRow = MasterSupplierdatagridview.CurrentCell.RowIndex;


            //    String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SUPPLIERID) AS 'No', CONTACTPERSON, NOPHONE, EMAIL, POSITION " +
            //                                 "FROM D_PERSON_SUPPLIER " +
            //                                 "WHERE SUPPLIERID = '{0}'", MasterSupplierdatagridview.Rows[currentRow].Cells[1].Value.ToString());
            //    createBackgroundWorkerFillDatagridView(query, "PersondataGridView");
            //}

        }
        #endregion

        private void PersondataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (masterItemPassingData != null)
                {
                    MasterSupplierdatagridviewCallPassingData(e.RowIndex);
                }
            }
        }

        private void Supplierdatagridview_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }


        //protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        //{
        //    foreach (Keys arrows in arrowsList)
        //    {
        //        if (keyData == arrows) { 
                    
        //        }
        //    }

        //    if (keyData == (Keys.Control | Keys.F))
        //    {
        //        MessageBox.Show("What the Ctrl+F?");
        //        return true;
        //    }
        //    return base.ProcessCmdKey(ref msg, keyData);
        //}
    }
}
