﻿namespace Sufindo
{
    partial class Supplierdatagridview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.FindtextBox = new System.Windows.Forms.TextBox();
            this.datagridviewpanel = new System.Windows.Forms.Panel();
            this.MasterSupplierdatagridview = new System.Windows.Forms.DataGridView();
            this.NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SUPPLIERID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SUPPLIERNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ADDRESS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ZONE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOTELP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FAX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContactPersonpanel = new System.Windows.Forms.Panel();
            this.PersondataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContactPerson = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoPhone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Position = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datagridviewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MasterSupplierdatagridview)).BeginInit();
            this.ContactPersonpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PersondataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Find";
            // 
            // FindtextBox
            // 
            this.FindtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FindtextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FindtextBox.Location = new System.Drawing.Point(70, 3);
            this.FindtextBox.Name = "FindtextBox";
            this.FindtextBox.Size = new System.Drawing.Size(212, 31);
            this.FindtextBox.TabIndex = 3;
            this.FindtextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FindtextBox_KeyPress);
            // 
            // datagridviewpanel
            // 
            this.datagridviewpanel.Controls.Add(this.MasterSupplierdatagridview);
            this.datagridviewpanel.Location = new System.Drawing.Point(4, 37);
            this.datagridviewpanel.Name = "datagridviewpanel";
            this.datagridviewpanel.Size = new System.Drawing.Size(858, 233);
            this.datagridviewpanel.TabIndex = 7;
            // 
            // MasterSupplierdatagridview
            // 
            this.MasterSupplierdatagridview.AllowUserToAddRows = false;
            this.MasterSupplierdatagridview.AllowUserToDeleteRows = false;
            this.MasterSupplierdatagridview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.MasterSupplierdatagridview.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NO,
            this.SUPPLIERID,
            this.SUPPLIERNAME,
            this.ADDRESS,
            this.ZONE,
            this.NOTELP,
            this.FAX});
            this.MasterSupplierdatagridview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MasterSupplierdatagridview.Location = new System.Drawing.Point(0, 0);
            this.MasterSupplierdatagridview.MultiSelect = false;
            this.MasterSupplierdatagridview.Name = "MasterSupplierdatagridview";
            this.MasterSupplierdatagridview.ReadOnly = true;
            this.MasterSupplierdatagridview.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.MasterSupplierdatagridview.Size = new System.Drawing.Size(858, 233);
            this.MasterSupplierdatagridview.TabIndex = 0;
            this.MasterSupplierdatagridview.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MasterSupplierdatagridview_CellDoubleClick);
            this.MasterSupplierdatagridview.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.MasterSupplierdatagridview_ColumnHeaderMouseClick);
            this.MasterSupplierdatagridview.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.MasterSupplierdatagridview_RowPrePaint);
            this.MasterSupplierdatagridview.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MasterSupplierdatagridview_CellClick);
            this.MasterSupplierdatagridview.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MasterSupplierdatagridview_KeyDown);
            this.MasterSupplierdatagridview.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MasterSupplierdatagridview_KeyUp);
            // 
            // NO
            // 
            this.NO.DataPropertyName = "NO";
            this.NO.HeaderText = "NO";
            this.NO.Name = "NO";
            this.NO.ReadOnly = true;
            this.NO.Width = 40;
            // 
            // SUPPLIERID
            // 
            this.SUPPLIERID.DataPropertyName = "SUPPLIERID";
            this.SUPPLIERID.HeaderText = "SUPPLIER ID";
            this.SUPPLIERID.Name = "SUPPLIERID";
            this.SUPPLIERID.ReadOnly = true;
            this.SUPPLIERID.Width = 120;
            // 
            // SUPPLIERNAME
            // 
            this.SUPPLIERNAME.DataPropertyName = "SUPPLIERNAME";
            this.SUPPLIERNAME.HeaderText = "SUPPLIER NAME";
            this.SUPPLIERNAME.Name = "SUPPLIERNAME";
            this.SUPPLIERNAME.ReadOnly = true;
            this.SUPPLIERNAME.Width = 150;
            // 
            // ADDRESS
            // 
            this.ADDRESS.DataPropertyName = "ADDRESS";
            this.ADDRESS.HeaderText = "ADDRESS";
            this.ADDRESS.Name = "ADDRESS";
            this.ADDRESS.ReadOnly = true;
            // 
            // ZONE
            // 
            this.ZONE.DataPropertyName = "ZONE";
            this.ZONE.HeaderText = "ZONE";
            this.ZONE.Name = "ZONE";
            this.ZONE.ReadOnly = true;
            // 
            // NOTELP
            // 
            this.NOTELP.DataPropertyName = "NOTELP";
            this.NOTELP.HeaderText = "NO TELP";
            this.NOTELP.Name = "NOTELP";
            this.NOTELP.ReadOnly = true;
            // 
            // FAX
            // 
            this.FAX.DataPropertyName = "FAX";
            this.FAX.HeaderText = "FAX";
            this.FAX.Name = "FAX";
            this.FAX.ReadOnly = true;
            // 
            // ContactPersonpanel
            // 
            this.ContactPersonpanel.Controls.Add(this.PersondataGridView);
            this.ContactPersonpanel.Location = new System.Drawing.Point(4, 275);
            this.ContactPersonpanel.Name = "ContactPersonpanel";
            this.ContactPersonpanel.Size = new System.Drawing.Size(495, 226);
            this.ContactPersonpanel.TabIndex = 24;
            // 
            // PersondataGridView
            // 
            this.PersondataGridView.AllowUserToAddRows = false;
            this.PersondataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PersondataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.PersondataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.PersondataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.ContactPerson,
            this.NoPhone,
            this.Email,
            this.Position});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.PersondataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.PersondataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PersondataGridView.Location = new System.Drawing.Point(0, 0);
            this.PersondataGridView.Name = "PersondataGridView";
            this.PersondataGridView.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PersondataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.PersondataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PersondataGridView.Size = new System.Drawing.Size(495, 226);
            this.PersondataGridView.TabIndex = 0;
            this.PersondataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.PersondataGridView_CellDoubleClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "NO";
            this.dataGridViewTextBoxColumn1.FillWeight = 40F;
            this.dataGridViewTextBoxColumn1.HeaderText = "NO";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 40;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 40;
            // 
            // ContactPerson
            // 
            this.ContactPerson.DataPropertyName = "CONTACTPERSON";
            this.ContactPerson.HeaderText = "Name";
            this.ContactPerson.Name = "ContactPerson";
            this.ContactPerson.ReadOnly = true;
            // 
            // NoPhone
            // 
            this.NoPhone.DataPropertyName = "NOPHONE";
            this.NoPhone.HeaderText = "No Phone";
            this.NoPhone.Name = "NoPhone";
            this.NoPhone.ReadOnly = true;
            // 
            // Email
            // 
            this.Email.DataPropertyName = "EMAIL";
            this.Email.HeaderText = "Email";
            this.Email.Name = "Email";
            this.Email.ReadOnly = true;
            // 
            // Position
            // 
            this.Position.DataPropertyName = "POSITION";
            this.Position.HeaderText = "Position";
            this.Position.Name = "Position";
            this.Position.ReadOnly = true;
            // 
            // Supplierdatagridview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 516);
            this.Controls.Add(this.ContactPersonpanel);
            this.Controls.Add(this.datagridviewpanel);
            this.Controls.Add(this.FindtextBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Supplierdatagridview";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "List Of Supplier";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Supplierdatagridview_FormClosing);
            this.datagridviewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MasterSupplierdatagridview)).EndInit();
            this.ContactPersonpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PersondataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FindtextBox;
        private System.Windows.Forms.Panel datagridviewpanel;
        private System.Windows.Forms.DataGridView MasterSupplierdatagridview;
        private System.Windows.Forms.Panel ContactPersonpanel;
        private System.Windows.Forms.DataGridView PersondataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ContactPerson;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoPhone;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn Position;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn SUPPLIERID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SUPPLIERNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn ADDRESS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ZONE;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOTELP;
        private System.Windows.Forms.DataGridViewTextBoxColumn FAX;
    }
}