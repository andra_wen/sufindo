﻿namespace Sufindo
{
    partial class Kursdatagridview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.FindtextBox = new System.Windows.Forms.TextBox();
            this.datagridviewpanel = new System.Windows.Forms.Panel();
            this.MasterKursdataGridView = new System.Windows.Forms.DataGridView();
            this.NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KURSID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KURSNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATEDBY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UPDATEDDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datagridviewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MasterKursdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Find";
            // 
            // FindtextBox
            // 
            this.FindtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FindtextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FindtextBox.Location = new System.Drawing.Point(68, 3);
            this.FindtextBox.Name = "FindtextBox";
            this.FindtextBox.Size = new System.Drawing.Size(212, 31);
            this.FindtextBox.TabIndex = 3;
            this.FindtextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FindtextBox_KeyPress);
            // 
            // datagridviewpanel
            // 
            this.datagridviewpanel.Controls.Add(this.MasterKursdataGridView);
            this.datagridviewpanel.Location = new System.Drawing.Point(3, 38);
            this.datagridviewpanel.Name = "datagridviewpanel";
            this.datagridviewpanel.Size = new System.Drawing.Size(491, 248);
            this.datagridviewpanel.TabIndex = 7;
            // 
            // MasterKursdataGridView
            // 
            this.MasterKursdataGridView.AllowUserToAddRows = false;
            this.MasterKursdataGridView.AllowUserToDeleteRows = false;
            this.MasterKursdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.MasterKursdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NO,
            this.KURSID,
            this.KURSNAME,
            this.RATE,
            this.CREATEDBY,
            this.UPDATEDDATE});
            this.MasterKursdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MasterKursdataGridView.Location = new System.Drawing.Point(0, 0);
            this.MasterKursdataGridView.MultiSelect = false;
            this.MasterKursdataGridView.Name = "MasterKursdataGridView";
            this.MasterKursdataGridView.ReadOnly = true;
            this.MasterKursdataGridView.Size = new System.Drawing.Size(491, 248);
            this.MasterKursdataGridView.TabIndex = 0;
            this.MasterKursdataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MasterKursdataGridView_CellDoubleClick);
            this.MasterKursdataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.MasterKursdataGridView_ColumnHeaderMouseClick);
            this.MasterKursdataGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MasterKursdataGridView_KeyDown);
            // 
            // NO
            // 
            this.NO.DataPropertyName = "NO";
            this.NO.HeaderText = "NO";
            this.NO.Name = "NO";
            this.NO.ReadOnly = true;
            this.NO.Width = 40;
            // 
            // KURSID
            // 
            this.KURSID.DataPropertyName = "KURSID";
            this.KURSID.HeaderText = "CURRENCY ID";
            this.KURSID.Name = "KURSID";
            this.KURSID.ReadOnly = true;
            // 
            // KURSNAME
            // 
            this.KURSNAME.DataPropertyName = "KURSNAME";
            this.KURSNAME.FillWeight = 120F;
            this.KURSNAME.HeaderText = "CURRENCY NAME";
            this.KURSNAME.Name = "KURSNAME";
            this.KURSNAME.ReadOnly = true;
            this.KURSNAME.Width = 120;
            // 
            // RATE
            // 
            this.RATE.DataPropertyName = "RATE";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle1.Format = "N2";
            this.RATE.DefaultCellStyle = dataGridViewCellStyle1;
            this.RATE.HeaderText = "RATE";
            this.RATE.Name = "RATE";
            this.RATE.ReadOnly = true;
            // 
            // CREATEDBY
            // 
            this.CREATEDBY.DataPropertyName = "CREATEDBY";
            this.CREATEDBY.HeaderText = "CREATEDBY";
            this.CREATEDBY.Name = "CREATEDBY";
            this.CREATEDBY.ReadOnly = true;
            this.CREATEDBY.Visible = false;
            // 
            // UPDATEDDATE
            // 
            this.UPDATEDDATE.DataPropertyName = "UPDATEDDATE";
            this.UPDATEDDATE.HeaderText = "UPDATEDDATE";
            this.UPDATEDDATE.Name = "UPDATEDDATE";
            this.UPDATEDDATE.ReadOnly = true;
            this.UPDATEDDATE.Visible = false;
            // 
            // Kursdatagridview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 291);
            this.Controls.Add(this.datagridviewpanel);
            this.Controls.Add(this.FindtextBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Kursdatagridview";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "List Of Currency";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Kursdatagridview_FormClosing);
            this.datagridviewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MasterKursdataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FindtextBox;
        private System.Windows.Forms.Panel datagridviewpanel;
        private System.Windows.Forms.DataGridView MasterKursdataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn KURSID;
        private System.Windows.Forms.DataGridViewTextBoxColumn KURSNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn RATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CREATEDBY;
        private System.Windows.Forms.DataGridViewTextBoxColumn UPDATEDDATE;
    }
}