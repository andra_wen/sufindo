﻿namespace Sufindo
{
    partial class ReceiptRevisiondatagridview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.FindtextBox = new System.Windows.Forms.TextBox();
            this.datagridviewpanel = new System.Windows.Forms.Panel();
            this.TransactionReceiveRevisiondatagridview = new System.Windows.Forms.DataGridView();
            this.NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REVISIONNO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REVISIONDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RECEIVENO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RECEIVEDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SUPPLIERNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SUPPLIERID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RECEIVEBY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CONTACTPERSON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KURSNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REMARK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContactPersonpanel = new System.Windows.Forms.Panel();
            this.TransactionReceiveRevisionItemdataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMIDQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PARTNAMEQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUANTITY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UNITPRICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datagridviewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionReceiveRevisiondatagridview)).BeginInit();
            this.ContactPersonpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionReceiveRevisionItemdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Find";
            // 
            // FindtextBox
            // 
            this.FindtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FindtextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FindtextBox.Location = new System.Drawing.Point(69, 2);
            this.FindtextBox.Name = "FindtextBox";
            this.FindtextBox.Size = new System.Drawing.Size(212, 31);
            this.FindtextBox.TabIndex = 3;
            this.FindtextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FindtextBox_KeyPress);
            // 
            // datagridviewpanel
            // 
            this.datagridviewpanel.Controls.Add(this.TransactionReceiveRevisiondatagridview);
            this.datagridviewpanel.Location = new System.Drawing.Point(3, 36);
            this.datagridviewpanel.Name = "datagridviewpanel";
            this.datagridviewpanel.Size = new System.Drawing.Size(942, 233);
            this.datagridviewpanel.TabIndex = 7;
            // 
            // TransactionReceiveRevisiondatagridview
            // 
            this.TransactionReceiveRevisiondatagridview.AllowUserToAddRows = false;
            this.TransactionReceiveRevisiondatagridview.AllowUserToDeleteRows = false;
            this.TransactionReceiveRevisiondatagridview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.TransactionReceiveRevisiondatagridview.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NO,
            this.REVISIONNO,
            this.REVISIONDATE,
            this.RECEIVENO,
            this.RECEIVEDATE,
            this.SUPPLIERNAME,
            this.SUPPLIERID,
            this.RECEIVEBY,
            this.CONTACTPERSON,
            this.KURSNAME,
            this.RATE,
            this.REMARK,
            this.STATUS});
            this.TransactionReceiveRevisiondatagridview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TransactionReceiveRevisiondatagridview.Location = new System.Drawing.Point(0, 0);
            this.TransactionReceiveRevisiondatagridview.MultiSelect = false;
            this.TransactionReceiveRevisiondatagridview.Name = "TransactionReceiveRevisiondatagridview";
            this.TransactionReceiveRevisiondatagridview.ReadOnly = true;
            this.TransactionReceiveRevisiondatagridview.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TransactionReceiveRevisiondatagridview.Size = new System.Drawing.Size(942, 233);
            this.TransactionReceiveRevisiondatagridview.TabIndex = 0;
            this.TransactionReceiveRevisiondatagridview.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TransactionSOdatagridview_CellDoubleClick);
            this.TransactionReceiveRevisiondatagridview.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.TransactionSOdatagridview_ColumnHeaderMouseClick);
            this.TransactionReceiveRevisiondatagridview.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TransactionSOdatagridview_KeyDown);
            this.TransactionReceiveRevisiondatagridview.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TransactionSOdatagridview_KeyUp);
            this.TransactionReceiveRevisiondatagridview.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TransactionReceiveRevisiondatagridview_CellContentClick);
            // 
            // NO
            // 
            this.NO.DataPropertyName = "NO";
            this.NO.HeaderText = "NO";
            this.NO.Name = "NO";
            this.NO.ReadOnly = true;
            this.NO.Width = 40;
            // 
            // REVISIONNO
            // 
            this.REVISIONNO.DataPropertyName = "REVISIONID";
            this.REVISIONNO.HeaderText = "REVISION NO";
            this.REVISIONNO.Name = "REVISIONNO";
            this.REVISIONNO.ReadOnly = true;
            // 
            // REVISIONDATE
            // 
            this.REVISIONDATE.DataPropertyName = "REVISIONDATE";
            this.REVISIONDATE.HeaderText = "REVISION DATE";
            this.REVISIONDATE.Name = "REVISIONDATE";
            this.REVISIONDATE.ReadOnly = true;
            // 
            // RECEIVENO
            // 
            this.RECEIVENO.DataPropertyName = "PURCHASEID";
            this.RECEIVENO.HeaderText = "RECEIVE NO";
            this.RECEIVENO.Name = "RECEIVENO";
            this.RECEIVENO.ReadOnly = true;
            // 
            // RECEIVEDATE
            // 
            this.RECEIVEDATE.DataPropertyName = "PURCHASEDATE";
            this.RECEIVEDATE.HeaderText = "RECEIVE DATE";
            this.RECEIVEDATE.Name = "RECEIVEDATE";
            this.RECEIVEDATE.ReadOnly = true;
            // 
            // SUPPLIERNAME
            // 
            this.SUPPLIERNAME.DataPropertyName = "SUPPLIERNAME";
            this.SUPPLIERNAME.HeaderText = "SUPPLIER NAME";
            this.SUPPLIERNAME.Name = "SUPPLIERNAME";
            this.SUPPLIERNAME.ReadOnly = true;
            // 
            // SUPPLIERID
            // 
            this.SUPPLIERID.DataPropertyName = "SUPPLIERID";
            this.SUPPLIERID.HeaderText = "SUPPLIERID";
            this.SUPPLIERID.Name = "SUPPLIERID";
            this.SUPPLIERID.ReadOnly = true;
            this.SUPPLIERID.Visible = false;
            // 
            // RECEIVEBY
            // 
            this.RECEIVEBY.DataPropertyName = "RECEIVE_BY";
            this.RECEIVEBY.HeaderText = "RECEIVE BY";
            this.RECEIVEBY.Name = "RECEIVEBY";
            this.RECEIVEBY.ReadOnly = true;
            this.RECEIVEBY.Width = 140;
            // 
            // CONTACTPERSON
            // 
            this.CONTACTPERSON.DataPropertyName = "CONTACTPERSONSUPPLIER";
            this.CONTACTPERSON.HeaderText = "CONTACT PERSON";
            this.CONTACTPERSON.Name = "CONTACTPERSON";
            this.CONTACTPERSON.ReadOnly = true;
            this.CONTACTPERSON.Width = 120;
            // 
            // KURSNAME
            // 
            this.KURSNAME.DataPropertyName = "KURSNAME";
            this.KURSNAME.FillWeight = 90F;
            this.KURSNAME.HeaderText = "KURS";
            this.KURSNAME.Name = "KURSNAME";
            this.KURSNAME.ReadOnly = true;
            this.KURSNAME.Width = 90;
            // 
            // RATE
            // 
            this.RATE.DataPropertyName = "RATE";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle1.Format = "N2";
            this.RATE.DefaultCellStyle = dataGridViewCellStyle1;
            this.RATE.FillWeight = 110F;
            this.RATE.HeaderText = "RATE";
            this.RATE.Name = "RATE";
            this.RATE.ReadOnly = true;
            this.RATE.Width = 110;
            // 
            // REMARK
            // 
            this.REMARK.DataPropertyName = "REMARK";
            this.REMARK.HeaderText = "REMARK";
            this.REMARK.Name = "REMARK";
            this.REMARK.ReadOnly = true;
            this.REMARK.Visible = false;
            // 
            // STATUS
            // 
            this.STATUS.DataPropertyName = "STATUS";
            this.STATUS.HeaderText = "STATUS";
            this.STATUS.Name = "STATUS";
            this.STATUS.ReadOnly = true;
            this.STATUS.Visible = false;
            // 
            // ContactPersonpanel
            // 
            this.ContactPersonpanel.Controls.Add(this.TransactionReceiveRevisionItemdataGridView);
            this.ContactPersonpanel.Location = new System.Drawing.Point(3, 278);
            this.ContactPersonpanel.Name = "ContactPersonpanel";
            this.ContactPersonpanel.Size = new System.Drawing.Size(811, 226);
            this.ContactPersonpanel.TabIndex = 24;
            // 
            // TransactionReceiveRevisionItemdataGridView
            // 
            this.TransactionReceiveRevisionItemdataGridView.AllowUserToAddRows = false;
            this.TransactionReceiveRevisionItemdataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TransactionReceiveRevisionItemdataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.TransactionReceiveRevisionItemdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.TransactionReceiveRevisionItemdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.ITEMID,
            this.ITEMNAME,
            this.ITEMIDQTY,
            this.PARTNAMEQTY,
            this.QUANTITY,
            this.UOMID,
            this.UNITPRICE,
            this.AMOUNT});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.TransactionReceiveRevisionItemdataGridView.DefaultCellStyle = dataGridViewCellStyle5;
            this.TransactionReceiveRevisionItemdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TransactionReceiveRevisionItemdataGridView.Location = new System.Drawing.Point(0, 0);
            this.TransactionReceiveRevisionItemdataGridView.Name = "TransactionReceiveRevisionItemdataGridView";
            this.TransactionReceiveRevisionItemdataGridView.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TransactionReceiveRevisionItemdataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.TransactionReceiveRevisionItemdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TransactionReceiveRevisionItemdataGridView.Size = new System.Drawing.Size(811, 226);
            this.TransactionReceiveRevisionItemdataGridView.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "NO";
            this.dataGridViewTextBoxColumn1.FillWeight = 40F;
            this.dataGridViewTextBoxColumn1.HeaderText = "NO";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 40;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 40;
            // 
            // ITEMID
            // 
            this.ITEMID.DataPropertyName = "ITEMID";
            this.ITEMID.HeaderText = "PART NO";
            this.ITEMID.Name = "ITEMID";
            this.ITEMID.ReadOnly = true;
            // 
            // ITEMNAME
            // 
            this.ITEMNAME.DataPropertyName = "ITEMNAME";
            this.ITEMNAME.HeaderText = "PART NAME";
            this.ITEMNAME.Name = "ITEMNAME";
            this.ITEMNAME.ReadOnly = true;
            // 
            // ITEMIDQTY
            // 
            this.ITEMIDQTY.DataPropertyName = "ITEMIDQTY";
            this.ITEMIDQTY.FillWeight = 105F;
            this.ITEMIDQTY.HeaderText = "PART NO QTY";
            this.ITEMIDQTY.Name = "ITEMIDQTY";
            this.ITEMIDQTY.ReadOnly = true;
            this.ITEMIDQTY.Width = 105;
            // 
            // PARTNAMEQTY
            // 
            this.PARTNAMEQTY.DataPropertyName = "ITEMNAMEQTY";
            this.PARTNAMEQTY.FillWeight = 120F;
            this.PARTNAMEQTY.HeaderText = "PART NAME QTY";
            this.PARTNAMEQTY.Name = "PARTNAMEQTY";
            this.PARTNAMEQTY.ReadOnly = true;
            this.PARTNAMEQTY.Width = 120;
            // 
            // QUANTITY
            // 
            this.QUANTITY.DataPropertyName = "QUANTITY";
            this.QUANTITY.HeaderText = "QUANTITY";
            this.QUANTITY.Name = "QUANTITY";
            this.QUANTITY.ReadOnly = true;
            // 
            // UOMID
            // 
            this.UOMID.DataPropertyName = "UOMID";
            this.UOMID.HeaderText = "UOM";
            this.UOMID.Name = "UOMID";
            this.UOMID.ReadOnly = true;
            // 
            // UNITPRICE
            // 
            this.UNITPRICE.DataPropertyName = "PRICE";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle3.Format = "N2";
            this.UNITPRICE.DefaultCellStyle = dataGridViewCellStyle3;
            this.UNITPRICE.HeaderText = "UNIT PRICE";
            this.UNITPRICE.Name = "UNITPRICE";
            this.UNITPRICE.ReadOnly = true;
            // 
            // AMOUNT
            // 
            this.AMOUNT.DataPropertyName = "AMOUNT";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle4.Format = "N2";
            this.AMOUNT.DefaultCellStyle = dataGridViewCellStyle4;
            this.AMOUNT.HeaderText = "AMOUNT";
            this.AMOUNT.Name = "AMOUNT";
            this.AMOUNT.ReadOnly = true;
            this.AMOUNT.Visible = false;
            // 
            // ReceiptRevisiondatagridview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(948, 507);
            this.Controls.Add(this.ContactPersonpanel);
            this.Controls.Add(this.datagridviewpanel);
            this.Controls.Add(this.FindtextBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReceiptRevisiondatagridview";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "List Of Purchase Revision";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SalesOrderdatagridview_FormClosing);
            this.datagridviewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TransactionReceiveRevisiondatagridview)).EndInit();
            this.ContactPersonpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TransactionReceiveRevisionItemdataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FindtextBox;
        private System.Windows.Forms.Panel datagridviewpanel;
        private System.Windows.Forms.DataGridView TransactionReceiveRevisiondatagridview;
        private System.Windows.Forms.Panel ContactPersonpanel;
        private System.Windows.Forms.DataGridView TransactionReceiveRevisionItemdataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMIDQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn PARTNAMEQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn QUANTITY;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn UNITPRICE;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn REVISIONNO;
        private System.Windows.Forms.DataGridViewTextBoxColumn REVISIONDATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn RECEIVENO;
        private System.Windows.Forms.DataGridViewTextBoxColumn RECEIVEDATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn SUPPLIERNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn SUPPLIERID;
        private System.Windows.Forms.DataGridViewTextBoxColumn RECEIVEBY;
        private System.Windows.Forms.DataGridViewTextBoxColumn CONTACTPERSON;
        private System.Windows.Forms.DataGridViewTextBoxColumn KURSNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn RATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn REMARK;
        private System.Windows.Forms.DataGridViewTextBoxColumn STATUS;
    }
}