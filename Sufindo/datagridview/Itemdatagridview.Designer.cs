﻿namespace Sufindo
{
    partial class Itemdatagridview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.FindtextBox = new System.Windows.Forms.TextBox();
            this.datagridviewpanel = new System.Windows.Forms.Panel();
            this.MasterItemdataGridView = new System.Windows.Forms.DataGridView();
            this.NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMID_ALIAS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMNAME_ALIAS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMALIASQUANTITY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BRANDID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SPEC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUANTITY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BESTPRICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UNITPRICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RACKID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMIMAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATEDBY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UPDATEDDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datagridviewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MasterItemdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Find";
            // 
            // FindtextBox
            // 
            this.FindtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FindtextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FindtextBox.Location = new System.Drawing.Point(66, 3);
            this.FindtextBox.Name = "FindtextBox";
            this.FindtextBox.Size = new System.Drawing.Size(212, 31);
            this.FindtextBox.TabIndex = 3;
            this.FindtextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FindtextBox_KeyPress);
            // 
            // datagridviewpanel
            // 
            this.datagridviewpanel.Controls.Add(this.MasterItemdataGridView);
            this.datagridviewpanel.Location = new System.Drawing.Point(7, 37);
            this.datagridviewpanel.Name = "datagridviewpanel";
            this.datagridviewpanel.Size = new System.Drawing.Size(997, 381);
            this.datagridviewpanel.TabIndex = 7;
            // 
            // MasterItemdataGridView
            // 
            this.MasterItemdataGridView.AllowUserToAddRows = false;
            this.MasterItemdataGridView.AllowUserToDeleteRows = false;
            this.MasterItemdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.MasterItemdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NO,
            this.ITEMID,
            this.ITEMNAME,
            this.ITEMID_ALIAS,
            this.ITEMNAME_ALIAS,
            this.ITEMALIASQUANTITY,
            this.BRANDID,
            this.SPEC,
            this.QUANTITY,
            this.UOMID,
            this.BESTPRICE,
            this.UNITPRICE,
            this.RACKID,
            this.ITEMIMAGE,
            this.CREATEDBY,
            this.UPDATEDDATE});
            this.MasterItemdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MasterItemdataGridView.Location = new System.Drawing.Point(0, 0);
            this.MasterItemdataGridView.MultiSelect = false;
            this.MasterItemdataGridView.Name = "MasterItemdataGridView";
            this.MasterItemdataGridView.ReadOnly = true;
            this.MasterItemdataGridView.Size = new System.Drawing.Size(997, 381);
            this.MasterItemdataGridView.TabIndex = 0;
            this.MasterItemdataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MasterItemdataGridView_CellDoubleClick);
            this.MasterItemdataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.MasterItemdataGridView_ColumnHeaderMouseClick);
            this.MasterItemdataGridView.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.MasterItemdataGridView_RowPostPaint);
            this.MasterItemdataGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MasterItemdataGridView_KeyDown);
            // 
            // NO
            // 
            this.NO.DataPropertyName = "NO";
            this.NO.FillWeight = 42.80618F;
            this.NO.HeaderText = "NO";
            this.NO.Name = "NO";
            this.NO.ReadOnly = true;
            this.NO.Width = 40;
            // 
            // ITEMID
            // 
            this.ITEMID.DataPropertyName = "ITEMID";
            this.ITEMID.FillWeight = 113.2076F;
            this.ITEMID.HeaderText = "PART NO";
            this.ITEMID.Name = "ITEMID";
            this.ITEMID.ReadOnly = true;
            this.ITEMID.Width = 106;
            // 
            // ITEMNAME
            // 
            this.ITEMNAME.DataPropertyName = "ITEMNAME";
            this.ITEMNAME.FillWeight = 111.0285F;
            this.ITEMNAME.HeaderText = "PART NAME";
            this.ITEMNAME.Name = "ITEMNAME";
            this.ITEMNAME.ReadOnly = true;
            this.ITEMNAME.Width = 104;
            // 
            // ITEMID_ALIAS
            // 
            this.ITEMID_ALIAS.DataPropertyName = "ITEMID_ALIAS";
            this.ITEMID_ALIAS.FillWeight = 109.1086F;
            this.ITEMID_ALIAS.HeaderText = "ALIAS";
            this.ITEMID_ALIAS.Name = "ITEMID_ALIAS";
            this.ITEMID_ALIAS.ReadOnly = true;
            this.ITEMID_ALIAS.Width = 101;
            // 
            // ITEMNAME_ALIAS
            // 
            this.ITEMNAME_ALIAS.DataPropertyName = "ITEMNAME_ALIAS";
            this.ITEMNAME_ALIAS.HeaderText = "ITEMNAME_ALIAS";
            this.ITEMNAME_ALIAS.Name = "ITEMNAME_ALIAS";
            this.ITEMNAME_ALIAS.ReadOnly = true;
            this.ITEMNAME_ALIAS.Visible = false;
            this.ITEMNAME_ALIAS.Width = 125;
            // 
            // ITEMALIASQUANTITY
            // 
            this.ITEMALIASQUANTITY.DataPropertyName = "ITEMALIASQUANTITY";
            this.ITEMALIASQUANTITY.HeaderText = "ITEMALIASQUANTITY";
            this.ITEMALIASQUANTITY.Name = "ITEMALIASQUANTITY";
            this.ITEMALIASQUANTITY.ReadOnly = true;
            this.ITEMALIASQUANTITY.Visible = false;
            this.ITEMALIASQUANTITY.Width = 143;
            // 
            // BRANDID
            // 
            this.BRANDID.DataPropertyName = "BRANDID";
            this.BRANDID.FillWeight = 107.4169F;
            this.BRANDID.HeaderText = "BRAND";
            this.BRANDID.Name = "BRANDID";
            this.BRANDID.ReadOnly = true;
            this.BRANDID.Width = 101;
            // 
            // SPEC
            // 
            this.SPEC.DataPropertyName = "SPEC";
            this.SPEC.FillWeight = 105.9264F;
            this.SPEC.HeaderText = "SPEC";
            this.SPEC.Name = "SPEC";
            this.SPEC.ReadOnly = true;
            this.SPEC.Width = 99;
            // 
            // QUANTITY
            // 
            this.QUANTITY.DataPropertyName = "QUANTITY";
            this.QUANTITY.FillWeight = 104.6132F;
            this.QUANTITY.HeaderText = "QUANTITY";
            this.QUANTITY.Name = "QUANTITY";
            this.QUANTITY.ReadOnly = true;
            this.QUANTITY.Width = 98;
            // 
            // UOMID
            // 
            this.UOMID.DataPropertyName = "UOMID";
            this.UOMID.FillWeight = 103.456F;
            this.UOMID.HeaderText = "UOM";
            this.UOMID.Name = "UOMID";
            this.UOMID.ReadOnly = true;
            this.UOMID.Width = 96;
            // 
            // BESTPRICE
            // 
            this.BESTPRICE.DataPropertyName = "BESTPRICE";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle1.Format = "N2";
            this.BESTPRICE.DefaultCellStyle = dataGridViewCellStyle1;
            this.BESTPRICE.HeaderText = "SALE PRICE";
            this.BESTPRICE.Name = "BESTPRICE";
            this.BESTPRICE.ReadOnly = true;
            this.BESTPRICE.Visible = false;
            this.BESTPRICE.Width = 94;
            // 
            // UNITPRICE
            // 
            this.UNITPRICE.DataPropertyName = "UNITPRICE";
            this.UNITPRICE.HeaderText = "UNITPRICE";
            this.UNITPRICE.Name = "UNITPRICE";
            this.UNITPRICE.ReadOnly = true;
            this.UNITPRICE.Visible = false;
            this.UNITPRICE.Width = 90;
            // 
            // RACKID
            // 
            this.RACKID.DataPropertyName = "RACKID";
            this.RACKID.FillWeight = 102.4365F;
            this.RACKID.HeaderText = "RACK";
            this.RACKID.Name = "RACKID";
            this.RACKID.ReadOnly = true;
            this.RACKID.Width = 96;
            // 
            // ITEMIMAGE
            // 
            this.ITEMIMAGE.DataPropertyName = "ITEMIMAGE";
            this.ITEMIMAGE.HeaderText = "ITEMIMAGE";
            this.ITEMIMAGE.Name = "ITEMIMAGE";
            this.ITEMIMAGE.ReadOnly = true;
            this.ITEMIMAGE.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ITEMIMAGE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ITEMIMAGE.Visible = false;
            this.ITEMIMAGE.Width = 73;
            // 
            // CREATEDBY
            // 
            this.CREATEDBY.DataPropertyName = "CREATEDBY";
            this.CREATEDBY.HeaderText = "createdby";
            this.CREATEDBY.Name = "CREATEDBY";
            this.CREATEDBY.ReadOnly = true;
            this.CREATEDBY.Visible = false;
            this.CREATEDBY.Width = 79;
            // 
            // UPDATEDDATE
            // 
            this.UPDATEDDATE.DataPropertyName = "UPDATEDDATE";
            this.UPDATEDDATE.HeaderText = "updateddate";
            this.UPDATEDDATE.Name = "UPDATEDDATE";
            this.UPDATEDDATE.ReadOnly = true;
            this.UPDATEDDATE.Visible = false;
            this.UPDATEDDATE.Width = 92;
            // 
            // Itemdatagridview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1009, 424);
            this.Controls.Add(this.datagridviewpanel);
            this.Controls.Add(this.FindtextBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Itemdatagridview";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "List Of Items";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Itemdatagridview_FormClosing);
            this.datagridviewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MasterItemdataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FindtextBox;
        private System.Windows.Forms.Panel datagridviewpanel;
        private System.Windows.Forms.DataGridView MasterItemdataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMID_ALIAS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMNAME_ALIAS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMALIASQUANTITY;
        private System.Windows.Forms.DataGridViewTextBoxColumn BRANDID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SPEC;
        private System.Windows.Forms.DataGridViewTextBoxColumn QUANTITY;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn BESTPRICE;
        private System.Windows.Forms.DataGridViewTextBoxColumn UNITPRICE;
        private System.Windows.Forms.DataGridViewTextBoxColumn RACKID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMIMAGE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CREATEDBY;
        private System.Windows.Forms.DataGridViewTextBoxColumn UPDATEDDATE;
    }
}