﻿namespace Sufindo
{
    partial class Customerdatagridview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.FindtextBox = new System.Windows.Forms.TextBox();
            this.datagridviewpanel = new System.Windows.Forms.Panel();
            this.MasterCustomerdatagridview = new System.Windows.Forms.DataGridView();
            this.NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMERID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMERNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ADDRESS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ZONE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOTELP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FAX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMAIL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CONTACTPERSON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOHP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SALESID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATEDBY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datagridviewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MasterCustomerdatagridview)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Find";
            // 
            // FindtextBox
            // 
            this.FindtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FindtextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FindtextBox.Location = new System.Drawing.Point(69, 3);
            this.FindtextBox.Name = "FindtextBox";
            this.FindtextBox.Size = new System.Drawing.Size(212, 31);
            this.FindtextBox.TabIndex = 3;
            this.FindtextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FindtextBox_KeyPress);
            // 
            // datagridviewpanel
            // 
            this.datagridviewpanel.Controls.Add(this.MasterCustomerdatagridview);
            this.datagridviewpanel.Location = new System.Drawing.Point(3, 39);
            this.datagridviewpanel.Name = "datagridviewpanel";
            this.datagridviewpanel.Size = new System.Drawing.Size(966, 233);
            this.datagridviewpanel.TabIndex = 7;
            // 
            // MasterCustomerdatagridview
            // 
            this.MasterCustomerdatagridview.AllowUserToAddRows = false;
            this.MasterCustomerdatagridview.AllowUserToDeleteRows = false;
            this.MasterCustomerdatagridview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.MasterCustomerdatagridview.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NO,
            this.CUSTOMERID,
            this.CUSTOMERNAME,
            this.ADDRESS,
            this.ZONE,
            this.NOTELP,
            this.FAX,
            this.EMAIL,
            this.CONTACTPERSON,
            this.NOHP,
            this.SALESID,
            this.STATUS,
            this.CREATEDBY});
            this.MasterCustomerdatagridview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MasterCustomerdatagridview.Location = new System.Drawing.Point(0, 0);
            this.MasterCustomerdatagridview.MultiSelect = false;
            this.MasterCustomerdatagridview.Name = "MasterCustomerdatagridview";
            this.MasterCustomerdatagridview.ReadOnly = true;
            this.MasterCustomerdatagridview.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.MasterCustomerdatagridview.Size = new System.Drawing.Size(966, 233);
            this.MasterCustomerdatagridview.TabIndex = 0;
            this.MasterCustomerdatagridview.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MasterCustomerdatagridview_CellDoubleClick);
            this.MasterCustomerdatagridview.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.MasterCustomerdatagridview_ColumnHeaderMouseClick);
            this.MasterCustomerdatagridview.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MasterCustomerdatagridview_KeyDown);
            // 
            // NO
            // 
            this.NO.DataPropertyName = "NO";
            this.NO.HeaderText = "NO";
            this.NO.Name = "NO";
            this.NO.ReadOnly = true;
            this.NO.Width = 40;
            // 
            // CUSTOMERID
            // 
            this.CUSTOMERID.DataPropertyName = "CUSTOMERID";
            this.CUSTOMERID.HeaderText = "CUSTOMER ID";
            this.CUSTOMERID.Name = "CUSTOMERID";
            this.CUSTOMERID.ReadOnly = true;
            this.CUSTOMERID.Width = 120;
            // 
            // CUSTOMERNAME
            // 
            this.CUSTOMERNAME.DataPropertyName = "CUSTOMERNAME";
            this.CUSTOMERNAME.HeaderText = "CUSTOMER NAME";
            this.CUSTOMERNAME.Name = "CUSTOMERNAME";
            this.CUSTOMERNAME.ReadOnly = true;
            this.CUSTOMERNAME.Width = 150;
            // 
            // ADDRESS
            // 
            this.ADDRESS.DataPropertyName = "ADDRESS";
            this.ADDRESS.HeaderText = "ADDRESS";
            this.ADDRESS.Name = "ADDRESS";
            this.ADDRESS.ReadOnly = true;
            // 
            // ZONE
            // 
            this.ZONE.DataPropertyName = "ZONE";
            this.ZONE.HeaderText = "ZONE";
            this.ZONE.Name = "ZONE";
            this.ZONE.ReadOnly = true;
            // 
            // NOTELP
            // 
            this.NOTELP.DataPropertyName = "NOTELP";
            this.NOTELP.HeaderText = "NO TELP";
            this.NOTELP.Name = "NOTELP";
            this.NOTELP.ReadOnly = true;
            // 
            // FAX
            // 
            this.FAX.DataPropertyName = "FAX";
            this.FAX.HeaderText = "FAX";
            this.FAX.Name = "FAX";
            this.FAX.ReadOnly = true;
            // 
            // EMAIL
            // 
            this.EMAIL.DataPropertyName = "EMAIL";
            this.EMAIL.HeaderText = "EMAIL";
            this.EMAIL.Name = "EMAIL";
            this.EMAIL.ReadOnly = true;
            // 
            // CONTACTPERSON
            // 
            this.CONTACTPERSON.DataPropertyName = "CONTACTPERSON";
            this.CONTACTPERSON.HeaderText = "CONTACTPERSON";
            this.CONTACTPERSON.Name = "CONTACTPERSON";
            this.CONTACTPERSON.ReadOnly = true;
            // 
            // NOHP
            // 
            this.NOHP.DataPropertyName = "NOHP";
            this.NOHP.HeaderText = "NOHP";
            this.NOHP.Name = "NOHP";
            this.NOHP.ReadOnly = true;
            // 
            // SALESID
            // 
            this.SALESID.DataPropertyName = "SALESID";
            this.SALESID.HeaderText = "SALESID";
            this.SALESID.Name = "SALESID";
            this.SALESID.ReadOnly = true;
            // 
            // STATUS
            // 
            this.STATUS.DataPropertyName = "STATUS";
            this.STATUS.HeaderText = "STATUS";
            this.STATUS.Name = "STATUS";
            this.STATUS.ReadOnly = true;
            this.STATUS.Visible = false;
            // 
            // CREATEDBY
            // 
            this.CREATEDBY.DataPropertyName = "CREATEDBY";
            this.CREATEDBY.HeaderText = "CREATEDBY";
            this.CREATEDBY.Name = "CREATEDBY";
            this.CREATEDBY.ReadOnly = true;
            this.CREATEDBY.Visible = false;
            // 
            // Customerdatagridview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(973, 276);
            this.Controls.Add(this.datagridviewpanel);
            this.Controls.Add(this.FindtextBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Customerdatagridview";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "List Of Customer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Supplierdatagridview_FormClosing);
            this.datagridviewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MasterCustomerdatagridview)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FindtextBox;
        private System.Windows.Forms.Panel datagridviewpanel;
        private System.Windows.Forms.DataGridView MasterCustomerdatagridview;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUSTOMERID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUSTOMERNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn ADDRESS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ZONE;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOTELP;
        private System.Windows.Forms.DataGridViewTextBoxColumn FAX;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMAIL;
        private System.Windows.Forms.DataGridViewTextBoxColumn CONTACTPERSON;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOHP;
        private System.Windows.Forms.DataGridViewTextBoxColumn SALESID;
        private System.Windows.Forms.DataGridViewTextBoxColumn STATUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn CREATEDBY;
    }
}