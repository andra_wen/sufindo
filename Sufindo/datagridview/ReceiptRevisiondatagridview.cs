﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sufindo.Master;

namespace Sufindo
{
    public partial class ReceiptRevisiondatagridview : Form
    {
        public delegate void ReceiveRevisionPassingData(DataTable receiveRevision);

        private delegate void fillDatatableCallBack(DataTable datatable, String called);

        public ReceiveRevisionPassingData receiverevisionPassingData;

        private readonly string[] columnRevision = { "REVISIONID", "PURCHASEID", "SUPPLIERNAME" };
        private Connection con;
        private String columnRevisionFind = "REVISIONID";

        private String calledForm;

        public ReceiptRevisiondatagridview()
        {
            InitializeComponent();
        }

        public ReceiptRevisiondatagridview(String calledForm)
        {
            InitializeComponent();

            if (con == null) {
                con = new Connection();
            }
            this.calledForm = calledForm;
            String query = "";
            if (this.calledForm.Equals("PurchaseReceiptRevision"))
            {
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY B.REVISIONID) AS 'NO', B.REVISIONID, CONVERT(VARCHAR(10), B.REVISSIONDATE, 103) AS REVISIONDATE, " +
                                    "A.PURCHASEID, CONVERT(VARCHAR(10), A.PURCHASEDATE, 103) AS PURCHASEDATE, A.SUPPLIERID, "+
                                    "C.SUPPLIERNAME, A.CREATEDBY AS RECEIVE_BY, A.CONTACTPERSONSUPPLIER, D.KURSNAME, A.RATE "+
                                    "FROM H_PURCHASE A "+
                                    "JOIN H_REVISI_PURCHASE B ON A.PURCHASEID = B.PURCHASEID "+
                                    "JOIN M_SUPPLIER C ON A.SUPPLIERID = C.SUPPLIERID "+
                                    "JOIN M_KURS D ON A.KURSID = D.KURSID "+
                                    "WHERE B.STATUS = '{0}'", Status.ACTIVE);
            }
            else if (this.calledForm.Equals("ListPurchaseRevisionReport"))
            {
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY B.REVISIONID) AS 'NO', B.REVISIONID, CONVERT(VARCHAR(10), B.REVISSIONDATE, 103) AS REVISIONDATE, " +
                                    "A.PURCHASEID, CONVERT(VARCHAR(10), A.PURCHASEDATE, 103) AS PURCHASEDATE, A.SUPPLIERID, " +
                                    "C.SUPPLIERNAME, A.CREATEDBY AS RECEIVE_BY, A.CONTACTPERSONSUPPLIER, D.KURSNAME, A.RATE " +
                                    "FROM H_PURCHASE A " +
                                    "JOIN H_REVISI_PURCHASE B ON A.PURCHASEID = B.PURCHASEID " +
                                    "JOIN M_SUPPLIER C ON A.SUPPLIERID = C.SUPPLIERID " +
                                    "JOIN M_KURS D ON A.KURSID = D.KURSID " +
                                    "WHERE B.STATUS = '{0}'", Status.CONFIRM);
                //query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY PURCHASEORDERID) AS 'NO', PURCHASEORDERID, CONVERT(VARCHAR(10), PURCHASEORDERDATE, 103) AS [PURCHASEORDERDATE], " +
                //                      "A.SUPPLIERID, SUPPLIERNAME, CONTACTPERSONSUPPLIER, KURSID, RATE, A.STATUS, REMARK, A.CREATEDBY " +
                //                      "FROM H_PURCHASE_ORDER A " +
                //                      "JOIN M_SUPPLIER B " +
                //                      "ON A.SUPPLIERID = B.SUPPLIERID " + 
                //                      "WHERE A.STATUS = '{0}' " +
                //                      "ORDER BY PURCHASEORDERID ASC",Status.OPEN);
            }
            createBackgroundWorkerFillDatagridView(query, "TransactionReceiveRevisiondatagridview");
        }

        void createBackgroundWorkerFillDatagridView(String query, String called)
        {

            for (int i = 0; i < TransactionReceiveRevisiondatagridview.Columns.Count; i++)
            {
                if (TransactionReceiveRevisiondatagridview.Columns[i].DataPropertyName.Equals(columnRevisionFind))
                {
                    DataGridViewColumn dataGridViewColumn = TransactionReceiveRevisiondatagridview.Columns[i];
                    dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                    break;
                }
            }

            String[] argument = { query, called };
            
            BackgroundWorker bgworker = new BackgroundWorker();
            bgworker.DoWork += new DoWorkEventHandler(bgworker_DoWork);
            bgworker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgworker_RunWorkerCompleted);
            bgworker.RunWorkerAsync(argument);
        }

        void bgworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {

        }

        void bgworker_DoWork(object sender, DoWorkEventArgs e) {
            try
            {
                String[] obj = e.Argument as String[];
                String query = (String) obj[0];
                String called = (String) obj[1];

                BeginInvoke(new fillDatatableCallBack(this.fillDatatable), con.openDataTableQuery(query), called);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void fillDatatable(DataTable datatable, String called)
        {
            if (called.Equals("TransactionReceiveRevisiondatagridview"))
            {
                TransactionReceiveRevisiondatagridview.DataSource = datatable;
                this.showTransactionReceiveRevisionItemdataGridView();
            }
            else if (called.Equals("TransactionReceiveRevisionItemdataGridView"))
            {
                TransactionReceiveRevisionItemdataGridView.DataSource = datatable;
            }
        }

        #region Method TransactionReceiveRevisiondatagridview
        private void TransactionSOdatagridview_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex !=-1)
            {
                if (receiverevisionPassingData != null)
                {
                    TransactionSOdatagridviewCallPassingData(e.RowIndex);
                }
            }
        }

        private void TransactionSOdatagridview_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                foreach (String item in columnRevision)
                {
                    if (TransactionReceiveRevisiondatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        for (int i = 0; i < TransactionReceiveRevisiondatagridview.Columns.Count; i++)
                        {
                            if (TransactionReceiveRevisiondatagridview.Columns[i].DataPropertyName.Equals(columnRevisionFind))
                            {
                                DataGridViewColumn dataGridViewColumn = TransactionReceiveRevisiondatagridview.Columns[i];
                                dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Control;
                            }
                        }
                        break;
                    }
                }

                foreach (String item in columnRevision)
                {
                    if (TransactionReceiveRevisiondatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        columnRevisionFind = TransactionReceiveRevisiondatagridview.Columns[e.ColumnIndex].DataPropertyName;
                        DataGridViewColumn dataGridViewColumn = TransactionReceiveRevisiondatagridview.Columns[e.ColumnIndex];
                        dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                        break;
                    }
                }
            }
        }

        private void TransactionSOdatagridview_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                if (TransactionReceiveRevisiondatagridview.Rows.Count != 0)
                {
                    e.SuppressKeyPress = true;
                    int currentColumn = TransactionReceiveRevisiondatagridview.CurrentCell.ColumnIndex;
                    int currentRow = TransactionReceiveRevisiondatagridview.CurrentCell.RowIndex;
                    TransactionReceiveRevisiondatagridview.CurrentCell = TransactionReceiveRevisiondatagridview[currentColumn, currentRow];

                    TransactionSOdatagridviewCallPassingData(currentRow);
                }
            }
            else
            {
                if (!(e.KeyCode == Keys.Down || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right || e.KeyCode == Keys.Up))
                {
                    if (char.IsLetterOrDigit((char)e.KeyCode))
                    {
                        char keysCode = (char)e.KeyCode;

                        FindtextBox.Focus();
                        FindtextBox.Text = keysCode.ToString();
                        FindtextBox.SelectionStart = FindtextBox.Text.Length;
                    }
                }
                else
                {
                    int currentRow = TransactionReceiveRevisiondatagridview.CurrentCell.RowIndex;
                    TransactionReceiveRevisiondatagridview.Rows[currentRow].Selected = true;
                }
            }
        }

        private void TransactionSOdatagridview_KeyUp(object sender, KeyEventArgs e)
        {
            this.showTransactionReceiveRevisionItemdataGridView();
        }

        private void TransactionReceiveRevisiondatagridview_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                this.showTransactionReceiveRevisionItemdataGridView();
            }
        }

        private void showTransactionReceiveRevisionItemdataGridView() {
            if (TransactionReceiveRevisiondatagridview.Rows.Count == 0)
            {
                while (this.TransactionReceiveRevisionItemdataGridView.Rows.Count > 0)
                {
                    this.TransactionReceiveRevisionItemdataGridView.Rows.RemoveAt(0);
                }
                return;
            }
            int currentRow = TransactionReceiveRevisiondatagridview.CurrentCell.RowIndex;
            Console.WriteLine(currentRow);
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'No', A.ITEMID, " +
                                            "B.ITEMNAME,A.ITEMIDQTY," +
                                            "(" +
                                            "SELECT ITEMNAME FROM M_ITEM " +
                                            "WHERE ITEMID = A.ITEMID " +
                                            ") AS ITEMNAMEQTY," +
                                            "A.QUANTITY, UOMID, PRICE " +
                                            "FROM D_REVISI_PURCHASE A " +
                                            "JOIN M_ITEM B " +
                                            "ON A.ITEMID = B.ITEMID " +
                                            "WHERE REVISIONID = '{0}'", TransactionReceiveRevisiondatagridview.Rows[currentRow].Cells["REVISIONNO"].Value.ToString());
            createBackgroundWorkerFillDatagridView(query, "TransactionReceiveRevisionItemdataGridView");
        }

        #endregion

        private void TransactionSOdatagridviewCallPassingData(int currentRow) {

            DataTable datatable = new DataTable();
            for (int i = 0; i < TransactionReceiveRevisiondatagridview.Columns.Count; i++){
                
                datatable.Columns.Add(TransactionReceiveRevisiondatagridview.Columns[i].DataPropertyName);
            }
            
            /*check multiple selected or not*/
            if (TransactionReceiveRevisiondatagridview.SelectedRows.Count != 0){
                foreach (var item in TransactionReceiveRevisiondatagridview.SelectedRows){
                    DataRow r = datatable.NewRow();
                    DataGridViewRow row = (DataGridViewRow)item;
                    for (int i = 0; i < TransactionReceiveRevisiondatagridview.Columns.Count; i++){
                        r[i] = row.Cells[i].Value;
                    }
                    datatable.Rows.Add(r);
                }
            }
            else{
                DataRow r = datatable.NewRow();
                DataGridViewRow row = TransactionReceiveRevisiondatagridview.Rows[currentRow];
                for (int i = 0; i < TransactionReceiveRevisiondatagridview.Columns.Count; i++){
                    r[i] = row.Cells[i].Value;
                    Console.WriteLine(row.Cells[i].Value);
                }
                datatable.Rows.Add(r);
            }
            receiverevisionPassingData(datatable);
            this.Close();
        }

        #region Method FindTextBox
        private void FindtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) {
                
                String strFind = FindtextBox.Text.Contains('*') ? FindtextBox.Text.Replace('*', '%') : (FindtextBox.Text + '%');
                String query = "";
                if (this.calledForm.Equals("PurchaseReceiptRevision")) {
                     query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY B.REVISIONID) AS 'NO', B.REVISIONID, CONVERT(VARCHAR(10), B.REVISSIONDATE, 103) AS REVISIONDATE, " +
                                            "A.PURCHASEID, CONVERT(VARCHAR(10), A.PURCHASEDATE, 103) AS PURCHASEDATE, A.SUPPLIERID, " +
                                            "C.SUPPLIERNAME, A.CREATEDBY AS RECEIVE_BY, A.CONTACTPERSONSUPPLIER, D.KURSNAME, A.RATE " +
                                            "FROM H_PURCHASE A " +
                                            "JOIN H_REVISI_PURCHASE B ON A.PURCHASEID = B.PURCHASEID " +
                                            "JOIN M_SUPPLIER C ON A.SUPPLIERID = C.SUPPLIERID " +
                                            "JOIN M_KURS D ON A.KURSID = D.KURSID " +
                                          "WHERE {0} LIKE '{1}' AND B.STATUS = '{2}' " +
                                          "ORDER BY B.REVISIONID ASC", columnRevisionFind, strFind, Status.ACTIVE);
                }
                else if (this.calledForm.Equals("ListPurchaseRevisionReport")) {
                     query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY B.REVISIONID) AS 'NO', B.REVISIONID, CONVERT(VARCHAR(10), B.REVISSIONDATE, 103) AS REVISIONDATE, " +
                                            "A.PURCHASEID, CONVERT(VARCHAR(10), A.PURCHASEDATE, 103) AS PURCHASEDATE, A.SUPPLIERID, " +
                                            "C.SUPPLIERNAME, A.CREATEDBY AS RECEIVE_BY, A.CONTACTPERSONSUPPLIER, D.KURSNAME, A.RATE " +
                                            "FROM H_PURCHASE A " +
                                            "JOIN H_REVISI_PURCHASE B ON A.PURCHASEID = B.PURCHASEID " +
                                            "JOIN M_SUPPLIER C ON A.SUPPLIERID = C.SUPPLIERID " +
                                            "JOIN M_KURS D ON A.KURSID = D.KURSID " +
                                          "WHERE {0} LIKE '{1}' AND B.STATUS = '{2}' " +
                                          "ORDER BY B.REVISIONID ASC", columnRevisionFind, strFind, Status.CONFIRM);
                     
                }

                createBackgroundWorkerFillDatagridView(query, "TransactionReceiveRevisiondatagridview");
            }
        }
        #endregion


        private void SalesOrderdatagridview_FormClosing(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }

        
    }
}
