﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sufindo.Master;

namespace Sufindo
{
    public partial class SalesOrderdatagridview : Form
    {
        public delegate void SalesOrderPassingData(DataTable salesOrder);

        private delegate void fillDatatableCallBack(DataTable datatable, String called);

        public SalesOrderPassingData salesOrderPassingData;

        private readonly string[] columnSalesOrder = { "SALESORDERID", "CUSTOMERNAME" };
        private Connection con;
        private String columnSalesOrderFind = "SALESORDERID";

        private String calledForm;

        public SalesOrderdatagridview(){
            InitializeComponent();
        }

        public SalesOrderdatagridview(String calledForm, Boolean isReadyOnly)
        {
            InitializeComponent();

            if (con == null)
            {
                con = new Connection();
            }
            this.calledForm = calledForm;
            String query = "";
            if (this.calledForm.Equals("SalesOrder"))
            {
                if (isReadyOnly)
                {
                    query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SALESORDERID) AS 'NO', SALESORDERID, " +
                                          "CONVERT(VARCHAR(10), SALESORDERDATE, 103) AS [SALESORDERDATE], " +
                                          "A.CUSTOMERID, CUSTOMERNAME, PAYMENTMETHOD, CURRENCYID, RATE, DISCOUNT, REMARK, A.STATUS, A.CREATEDBY " +
                                          "FROM H_SALES_ORDER A JOIN M_CUSTOMER B " +
                                          "ON A.CUSTOMERID = B.CUSTOMERID " +
                                          "WHERE A.STATUS = '{0}' AND A.CREATEDBY = '{1}'" +
                                          "ORDER BY SALESORDERID ASC", Status.READY, AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString());
                }
                else
                {
                    query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SALESORDERID) AS 'NO', SALESORDERID, " +
                                          "CONVERT(VARCHAR(10), SALESORDERDATE, 103) AS [SALESORDERDATE], " +
                                          "A.CUSTOMERID, CUSTOMERNAME, PAYMENTMETHOD, CURRENCYID, RATE, DISCOUNT, REMARK, A.STATUS, A.CREATEDBY " +
                                          "FROM H_SALES_ORDER A JOIN M_CUSTOMER B " +
                                          "ON A.CUSTOMERID = B.CUSTOMERID " +
                                          "WHERE (A.STATUS = '{0}' OR A.STATUS = '{1}') AND A.CREATEDBY = '{2}' " +
                                          "ORDER BY SALESORDERID ASC", Status.READY, Status.RELEASE, AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString());
                }
            }
            else if (this.calledForm.Equals("OrderPicking"))
            {
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SALESORDERID) AS 'NO', SALESORDERID, " +
                                      "CONVERT(VARCHAR(10), SALESORDERDATE, 103) AS [SALESORDERDATE], " +
                                      "A.CUSTOMERID, CUSTOMERNAME, PAYMENTMETHOD, CURRENCYID, RATE, DISCOUNT, REMARK, A.STATUS, A.CREATEDBY " +
                                      "FROM H_SALES_ORDER A JOIN M_CUSTOMER B " +
                                      "ON A.CUSTOMERID = B.CUSTOMERID " +
                                      "WHERE A.STATUS = '{0}' " +
                                      "ORDER BY SALESORDERID ASC", Status.READY);
            }
            else if (this.calledForm.Equals("OrderPickingReport") || this.calledForm.Equals("ListSalesOrderReport"))
            {
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SALESORDERID) AS 'NO', SALESORDERID, " +
                                      "CONVERT(VARCHAR(10), SALESORDERDATE, 103) AS [SALESORDERDATE], " +
                                      "A.CUSTOMERID, CUSTOMERNAME, PAYMENTMETHOD, CURRENCYID, RATE, DISCOUNT, REMARK, A.STATUS, A.CREATEDBY " +
                                      "FROM H_SALES_ORDER A JOIN M_CUSTOMER B " +
                                      "ON A.CUSTOMERID = B.CUSTOMERID " +
                                      "ORDER BY SALESORDERID ASC");
            }
            createBackgroundWorkerFillDatagridView(query, "TransactionSOdatagridview");
        }

        public SalesOrderdatagridview(String calledForm)
        {
            InitializeComponent();

            if (con == null) {
                con = new Connection();
            }
            this.calledForm = calledForm;
            String query = "";
            if(this.calledForm.Equals("SalesOrder")){
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SALESORDERID) AS 'NO', SALESORDERID, " +
                                      "CONVERT(VARCHAR(10), SALESORDERDATE, 103) AS [SALESORDERDATE], " +
                                      "A.CUSTOMERID, CUSTOMERNAME, PAYMENTMETHOD, CURRENCYID, RATE, DISCOUNT, REMARK, A.STATUS, A.CREATEDBY " +
                                      "FROM H_SALES_ORDER A JOIN M_CUSTOMER B " +
                                      "ON A.CUSTOMERID = B.CUSTOMERID " +
                                      "WHERE (A.STATUS = '{0}' OR A.STATUS = '{1}') AND A.CREATEDBY = '{2}'" +
                                      "ORDER BY SALESORDERID ASC", Status.READY, Status.RELEASE, AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString());
            }
            else if (this.calledForm.Equals("OrderPicking"))
            {
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SALESORDERID) AS 'NO', SALESORDERID, " +
                                      "CONVERT(VARCHAR(10), SALESORDERDATE, 103) AS [SALESORDERDATE], " +
                                      "A.CUSTOMERID, CUSTOMERNAME, PAYMENTMETHOD, CURRENCYID, RATE, DISCOUNT, REMARK, A.STATUS, A.CREATEDBY " +
                                      "FROM H_SALES_ORDER A JOIN M_CUSTOMER B " +
                                      "ON A.CUSTOMERID = B.CUSTOMERID " +
                                      "WHERE A.STATUS = '{0}' OR A.STATUS = '{1}' " +
                                      "ORDER BY SALESORDERID ASC", Status.READY, Status.ACTIVE);
            }
            else if (this.calledForm.Equals("OrderPickingReport"))
            {
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SALESORDERID) AS 'NO', SALESORDERID, " +
                                      "CONVERT(VARCHAR(10), SALESORDERDATE, 103) AS [SALESORDERDATE], " +
                                      "A.CUSTOMERID, CUSTOMERNAME, PAYMENTMETHOD, CURRENCYID, RATE, DISCOUNT, REMARK, A.STATUS, A.CREATEDBY " +
                                      "FROM H_SALES_ORDER A JOIN M_CUSTOMER B " +
                                      "ON A.CUSTOMERID = B.CUSTOMERID " +
                                      "ORDER BY SALESORDERID ASC");
            }
            createBackgroundWorkerFillDatagridView(query, "TransactionSOdatagridview");
        }

        void createBackgroundWorkerFillDatagridView(String query, String called)
        {

            for (int i = 0; i < TransactionSOdatagridview.Columns.Count; i++)
            {
                if (TransactionSOdatagridview.Columns[i].DataPropertyName.Equals(columnSalesOrderFind))
                {
                    DataGridViewColumn dataGridViewColumn = TransactionSOdatagridview.Columns[i];
                    dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                    break;
                }
            }

            String[] argument = { query, called };
            
            BackgroundWorker bgworker = new BackgroundWorker();
            bgworker.DoWork += new DoWorkEventHandler(bgworker_DoWork);
            bgworker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgworker_RunWorkerCompleted);
            bgworker.RunWorkerAsync(argument);
        }

        void bgworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {

        }

        void bgworker_DoWork(object sender, DoWorkEventArgs e) {
            try
            {
                String[] obj = e.Argument as String[];
                String query = (String) obj[0];
                String called = (String) obj[1];

                BeginInvoke(new fillDatatableCallBack(this.fillDatatable), con.openDataTableQuery(query), called);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void fillDatatable(DataTable datatable, String called)
        {
            if (called.Equals("TransactionSOdatagridview"))
            {
                TransactionSOdatagridview.DataSource = datatable;
                this.showTransactionSOItemDataGridView();
            }
            else if (called.Equals("TransactionSOItemdataGridView"))
            {
                TransactionSOItemdataGridView.DataSource = datatable;
            }
        }

        #region Method TransactionSOdatagridview
        private void TransactionSOdatagridview_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
            if (e.RowIndex != -1)
            {
                if (salesOrderPassingData != null)
                {
                    if (this.calledForm.Equals("OrderPicking"))
                    {
                        if (TransactionSOdatagridview.Rows[e.RowIndex].Cells["STATUS"].Value.ToString().Equals(Status.READY))
                        {
                            TransactionSOdatagridviewCallPassingData(e.RowIndex);
                        }
                    }
                    else
                    {
                        TransactionSOdatagridviewCallPassingData(e.RowIndex);
                    }
                }
            }
        }

        private void TransactionSOdatagridview_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                foreach (String item in columnSalesOrder)
                {
                    if (TransactionSOdatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        for (int i = 0; i < TransactionSOdatagridview.Columns.Count; i++)
                        {
                            if (TransactionSOdatagridview.Columns[i].DataPropertyName.Equals(columnSalesOrderFind))
                            {
                                DataGridViewColumn dataGridViewColumn = TransactionSOdatagridview.Columns[i];
                                dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Control;
                            }
                        }
                        break;
                    }
                }

                foreach (String item in columnSalesOrder)
                {
                    if (TransactionSOdatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        columnSalesOrderFind = TransactionSOdatagridview.Columns[e.ColumnIndex].DataPropertyName;
                        DataGridViewColumn dataGridViewColumn = TransactionSOdatagridview.Columns[e.ColumnIndex];
                        dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                        break;
                    }
                }
            }
        }

        private void TransactionSOdatagridview_KeyDown(object sender, KeyEventArgs e)
        {
            if (TransactionSOdatagridview.RowCount != 0)
            {
                if (e.KeyCode == Keys.Enter)
                {

                    if (TransactionSOdatagridview.Rows.Count != 0)
                    {
                        e.SuppressKeyPress = true;
                        int currentColumn = TransactionSOdatagridview.CurrentCell.ColumnIndex;
                        int currentRow = TransactionSOdatagridview.CurrentCell.RowIndex;
                        TransactionSOdatagridview.CurrentCell = TransactionSOdatagridview[currentColumn, currentRow];

                        if (this.calledForm.Equals("OrderPicking"))
                        {
                            if (TransactionSOdatagridview.Rows[currentRow].Cells["STATUS"].Value.ToString().Equals(Status.READY))
                            {
                                TransactionSOdatagridviewCallPassingData(currentRow);
                            }
                        }
                        else TransactionSOdatagridviewCallPassingData(currentRow);
                    }
                }
                else
                {
                    if (!(e.KeyCode == Keys.Down || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right || e.KeyCode == Keys.Up))
                    {
                        if (char.IsLetterOrDigit((char)e.KeyCode))
                        {
                            char keysCode = (char)e.KeyCode;

                            FindtextBox.Focus();
                            FindtextBox.Text = keysCode.ToString();
                            FindtextBox.SelectionStart = FindtextBox.Text.Length;
                        }
                    }
                    else
                    {
                        int currentRow = TransactionSOdatagridview.CurrentCell.RowIndex;
                        TransactionSOdatagridview.Rows[currentRow].Selected = true;
                    }
                }
            }
        }

        private void TransactionSOdatagridview_KeyUp(object sender, KeyEventArgs e)
        {
            this.showTransactionSOItemDataGridView();
            //}
        }

        private void TransactionSOdatagridview_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                this.showTransactionSOItemDataGridView();
            }
        }

        private void showTransactionSOItemDataGridView() {
            if (TransactionSOdatagridview.RowCount == 0)
            {
                while (this.TransactionSOItemdataGridView.Rows.Count > 0)
                {
                    this.TransactionSOItemdataGridView.Rows.RemoveAt(0);
                }
                return;
            }
            int currentRow = TransactionSOdatagridview.CurrentCell.RowIndex;
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'No', A.ITEMID, B.ITEMNAME, " +
                                         "A.ITEMIDQTY,  " +
                                         "( " +
                                         "   SELECT ITEMNAME FROM M_ITEM " +
                                         "   WHERE ITEMID = A.ITEMID " +
                                         ") AS ITEMNAMEQTY, " +
                                         "A.QUANTITY, UOMID, PRICE, C.RACKID " +
                                         "FROM D_SALES_ORDER A " +
                                         "JOIN M_ITEM B " +
                                         "ON A.ITEMID = B.ITEMID " +
                                         "JOIN M_RACK C " +
                                         "ON B.RACKID = C.RACKID " +
                                         "WHERE SALESORDERID = '{0}'", TransactionSOdatagridview.Rows[currentRow].Cells[1].Value.ToString());
            createBackgroundWorkerFillDatagridView(query, "TransactionSOItemdataGridView");
        }

        #endregion

        private void TransactionSOdatagridviewCallPassingData(int currentRow) {

            DataTable datatable = new DataTable();
            for (int i = 0; i < TransactionSOdatagridview.Columns.Count; i++){
                
                datatable.Columns.Add(TransactionSOdatagridview.Columns[i].DataPropertyName);
            }
            
            /*check multiple selected or not*/
            if (TransactionSOdatagridview.SelectedRows.Count != 0){
                foreach (var item in TransactionSOdatagridview.SelectedRows){
                    DataRow r = datatable.NewRow();
                    DataGridViewRow row = (DataGridViewRow)item;
                    for (int i = 0; i < TransactionSOdatagridview.Columns.Count; i++){
                        r[i] = row.Cells[i].Value;
                    }
                    datatable.Rows.Add(r);
                }
            }
            else{
                DataRow r = datatable.NewRow();
                DataGridViewRow row = TransactionSOdatagridview.Rows[currentRow];
                for (int i = 0; i < TransactionSOdatagridview.Columns.Count; i++){
                    r[i] = row.Cells[i].Value;
                }
                datatable.Rows.Add(r);
            }
            salesOrderPassingData(datatable);
            this.Close();
        }

        private void FindtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) {
                String strFind = FindtextBox.Text.Contains('*') ? FindtextBox.Text.Replace('*', '%') : (FindtextBox.Text + '%');
                String query = "";
                if (this.calledForm.Equals("SalesOrder"))
                {
                    query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SALESORDERID) AS 'NO', SALESORDERID, " +
                                       "CONVERT(VARCHAR(10), SALESORDERDATE, 103) AS [SALESORDERDATE], " +
                                       "A.CUSTOMERID, CUSTOMERNAME, CURRENCYID, PAYMENTMETHOD, RATE, DISCOUNT, REMARK, A.STATUS, A.CREATEDBY " +
                                       "FROM H_SALES_ORDER A JOIN M_CUSTOMER B " +
                                       "ON A.CUSTOMERID = B.CUSTOMERID " +
                                       "WHERE {0} LIKE '{1}' AND ( A.STATUS = '{2}' OR A.STATUS = '{3}' ) AND A.CREATEDBY = '{4}' " +
                                       "ORDER BY SALESORDERID ASC", columnSalesOrderFind, strFind, Status.READY, Status.RELEASE, AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString());
                }
                else if (this.calledForm.Equals("OrderPicking"))
                {
                    query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SALESORDERID) AS 'NO', SALESORDERID, " +
                                         "CONVERT(VARCHAR(10), SALESORDERDATE, 103) AS [SALESORDERDATE], " +
                                         "A.CUSTOMERID, CUSTOMERNAME, CURRENCYID, PAYMENTMETHOD, RATE, DISCOUNT, REMARK, A.STATUS, A.CREATEDBY " +
                                         "FROM H_SALES_ORDER A JOIN M_CUSTOMER B " +
                                         "ON A.CUSTOMERID = B.CUSTOMERID " +
                                         "WHERE {0} LIKE '{1}' AND A.STATUS = '{2}'" +
                                         "ORDER BY SALESORDERID ASC", columnSalesOrderFind, strFind, Status.READY);
                }
                else if (this.calledForm.Equals("OrderPickingReport") || this.calledForm.Equals("ListSalesOrderReport"))
                {
                    query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SALESORDERID) AS 'NO', SALESORDERID, " +
                                          "CONVERT(VARCHAR(10), SALESORDERDATE, 103) AS [SALESORDERDATE], " +
                                          "A.CUSTOMERID, CUSTOMERNAME, CURRENCYID, PAYMENTMETHOD, RATE, DISCOUNT, REMARK, A.STATUS, A.CREATEDBY " +
                                          "FROM H_SALES_ORDER A JOIN M_CUSTOMER B " +
                                          "ON A.CUSTOMERID = B.CUSTOMERID " +
                                          "WHERE {0} LIKE '{1}'" +
                                          "ORDER BY SALESORDERID ASC", columnSalesOrderFind, strFind);
                }

                createBackgroundWorkerFillDatagridView(query, "TransactionSOdatagridview");
            }
        }

        private void SalesOrderdatagridview_FormClosing(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }

        private void TransactionSOItemdataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
