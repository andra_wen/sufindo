﻿namespace Sufindo
{
    partial class AdjustSpoildatagridview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.FindtextBox = new System.Windows.Forms.TextBox();
            this.datagridviewpanel = new System.Windows.Forms.Panel();
            this.Transactiondatagridview = new System.Windows.Forms.DataGridView();
            this.panel = new System.Windows.Forms.Panel();
            this.TransactionItemdataGridView = new System.Windows.Forms.DataGridView();
            this.datagridviewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Transactiondatagridview)).BeginInit();
            this.panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionItemdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Find";
            // 
            // FindtextBox
            // 
            this.FindtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FindtextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FindtextBox.Location = new System.Drawing.Point(70, 4);
            this.FindtextBox.Name = "FindtextBox";
            this.FindtextBox.Size = new System.Drawing.Size(212, 31);
            this.FindtextBox.TabIndex = 3;
            this.FindtextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FindtextBox_KeyPress);
            // 
            // datagridviewpanel
            // 
            this.datagridviewpanel.Controls.Add(this.Transactiondatagridview);
            this.datagridviewpanel.Location = new System.Drawing.Point(4, 37);
            this.datagridviewpanel.Name = "datagridviewpanel";
            this.datagridviewpanel.Size = new System.Drawing.Size(775, 233);
            this.datagridviewpanel.TabIndex = 7;
            // 
            // Transactiondatagridview
            // 
            this.Transactiondatagridview.AllowUserToAddRows = false;
            this.Transactiondatagridview.AllowUserToDeleteRows = false;
            this.Transactiondatagridview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.Transactiondatagridview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Transactiondatagridview.Location = new System.Drawing.Point(0, 0);
            this.Transactiondatagridview.MultiSelect = false;
            this.Transactiondatagridview.Name = "Transactiondatagridview";
            this.Transactiondatagridview.ReadOnly = true;
            this.Transactiondatagridview.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Transactiondatagridview.Size = new System.Drawing.Size(775, 233);
            this.Transactiondatagridview.TabIndex = 0;
            this.Transactiondatagridview.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Transactiondatagridview_CellDoubleClick);
            this.Transactiondatagridview.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.Transactiondatagridview_ColumnHeaderMouseClick);
            this.Transactiondatagridview.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Transactiondatagridview_CellClick);
            this.Transactiondatagridview.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Transactiondatagridview_KeyDown);
            this.Transactiondatagridview.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Transactiondatagridview_KeyUp);
            // 
            // panel
            // 
            this.panel.Controls.Add(this.TransactionItemdataGridView);
            this.panel.Location = new System.Drawing.Point(4, 280);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(775, 226);
            this.panel.TabIndex = 24;
            // 
            // TransactionItemdataGridView
            // 
            this.TransactionItemdataGridView.AllowUserToAddRows = false;
            this.TransactionItemdataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TransactionItemdataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.TransactionItemdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.TransactionItemdataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.TransactionItemdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TransactionItemdataGridView.Location = new System.Drawing.Point(0, 0);
            this.TransactionItemdataGridView.Name = "TransactionItemdataGridView";
            this.TransactionItemdataGridView.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TransactionItemdataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.TransactionItemdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TransactionItemdataGridView.Size = new System.Drawing.Size(775, 226);
            this.TransactionItemdataGridView.TabIndex = 0;
            // 
            // AdjustSpoildatagridview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 510);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.datagridviewpanel);
            this.Controls.Add(this.FindtextBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AdjustSpoildatagridview";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "List Of Adjustment/Spoil";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AdjustSpoildatagridview_FormClosing);
            this.datagridviewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Transactiondatagridview)).EndInit();
            this.panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TransactionItemdataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FindtextBox;
        private System.Windows.Forms.Panel datagridviewpanel;
        private System.Windows.Forms.DataGridView Transactiondatagridview;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.DataGridView TransactionItemdataGridView;
    }
}