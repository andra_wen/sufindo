﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sufindo.Master;

namespace Sufindo
{
    public partial class Employeedatagridview : Form
    {
        public delegate void MasterEmployeePassingData(DataTable masterEmployee);

        private delegate void fillDatatableCallBack(DataTable datatable, String called);

        public MasterEmployeePassingData masterEmployeePassingData;

        private readonly string[] columnMasterEmployee = { "EMPLOYEEID", "EMPLOYEENAME"};
        private Connection con;
        private String columnMasterEmployeeFind = "EMPLOYEEID";

        private String calledForm;

        public Employeedatagridview(){
            InitializeComponent();
        }

        public Employeedatagridview(String calledForm)
        {
            InitializeComponent();

            if (con == null) {
                con = new Connection();
            }
            this.calledForm = calledForm;
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY EMPLOYEEID) AS 'No', EMPLOYEEID, PASSWORD, EMPLOYEENAME, POSITION, " +
                                         "[ADDRESS], CONVERT(VARCHAR(10), BOD, 103) AS [BOD], NOHP, EMAIL, CONVERT(VARCHAR(10), JOINDATE, 103) AS [JOINDATE], STATUS, CREATEDBY " +
                                         "FROM M_EMPLOYEE WHERE STATUS = '{0}' " +
                                         "ORDER BY EMPLOYEEID ASC", Status.ACTIVE);
            createBackgroundWorkerFillDatagridView(query, "MasterEmployeedatagridview");
        }

        void createBackgroundWorkerFillDatagridView(String query, String called)
        {

            for (int i = 0; i < MasterEmployeedatagridview.Columns.Count; i++)
            {
                if (MasterEmployeedatagridview.Columns[i].DataPropertyName.Equals(columnMasterEmployeeFind))
                {
                    DataGridViewColumn dataGridViewColumn = MasterEmployeedatagridview.Columns[i];
                    dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                    break;
                }
            }

            String[] argument = { query, called };
            
            BackgroundWorker bgworker = new BackgroundWorker();
            bgworker.DoWork += new DoWorkEventHandler(bgworker_DoWork);
            bgworker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgworker_RunWorkerCompleted);
            bgworker.RunWorkerAsync(argument);
        }

        void bgworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {

        }

        void bgworker_DoWork(object sender, DoWorkEventArgs e) {
            try
            {
                String[] obj = e.Argument as String[];
                String query = (String) obj[0];
                String called = (String) obj[1];

                BeginInvoke(new fillDatatableCallBack(this.fillDatatable), con.openDataTableQuery(query), called);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void fillDatatable(DataTable datatable, String called)
        {
            if (called.Equals("MasterEmployeedatagridview")) MasterEmployeedatagridview.DataSource = datatable;
        }

        #region Method MasterCustomerdatagridview
        private void MasterCustomerdatagridview_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (masterEmployeePassingData != null)
                {
                    MasterEmployeedatagridviewCallPassingData(e.RowIndex);
                }
            }
        }

        private void MasterCustomerdatagridview_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                foreach (String item in columnMasterEmployee)
                {
                    if (MasterEmployeedatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        for (int i = 0; i < MasterEmployeedatagridview.Columns.Count; i++)
                        {
                            if (MasterEmployeedatagridview.Columns[i].DataPropertyName.Equals(columnMasterEmployeeFind))
                            {
                                DataGridViewColumn dataGridViewColumn = MasterEmployeedatagridview.Columns[i];
                                dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Control;
                            }
                        }
                        break;
                    }
                }

                foreach (String item in columnMasterEmployee)
                {
                    if (MasterEmployeedatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        columnMasterEmployeeFind = MasterEmployeedatagridview.Columns[e.ColumnIndex].DataPropertyName;
                        DataGridViewColumn dataGridViewColumn = MasterEmployeedatagridview.Columns[e.ColumnIndex];
                        dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                        break;
                    }
                }
            }
        }

        private void MasterCustomerdatagridview_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                if (MasterEmployeedatagridview.Rows.Count != 0)
                {
                    e.SuppressKeyPress = true;
                    int currentColumn = MasterEmployeedatagridview.CurrentCell.ColumnIndex;
                    int currentRow = MasterEmployeedatagridview.CurrentCell.RowIndex;
                    MasterEmployeedatagridview.CurrentCell = MasterEmployeedatagridview[currentColumn, currentRow];

                    MasterEmployeedatagridviewCallPassingData(currentRow);
                }
            }
            else
            {
                if (!(e.KeyCode == Keys.Down || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right || e.KeyCode == Keys.Up))
                {
                    if (char.IsLetterOrDigit((char)e.KeyCode))
                    {
                        char keysCode = (char)e.KeyCode;

                        FindtextBox.Focus();
                        FindtextBox.Text = keysCode.ToString();
                        FindtextBox.SelectionStart = FindtextBox.Text.Length;
                    }
                }
                else
                {
                    int currentRow = MasterEmployeedatagridview.CurrentCell.RowIndex;
                    MasterEmployeedatagridview.Rows[currentRow].Selected = true;
                }
            }
        }

        #endregion

        private void MasterEmployeedatagridviewCallPassingData(int currentRow) {

            
            DataTable datatable = new DataTable();
            for (int i = 0; i < MasterEmployeedatagridview.Columns.Count; i++){
                datatable.Columns.Add(MasterEmployeedatagridview.Columns[i].DataPropertyName);
            }
            
            /*check multiple selected or not*/
            if (MasterEmployeedatagridview.SelectedRows.Count != 0){
                foreach (var item in MasterEmployeedatagridview.SelectedRows){
                    DataRow r = datatable.NewRow();
                    DataGridViewRow row = (DataGridViewRow)item;
                    for (int i = 0; i < MasterEmployeedatagridview.Columns.Count; i++){
                        r[i] = row.Cells[i].Value;
                    }
                    datatable.Rows.Add(r);
                }
                Boolean flagColumn = false;
                foreach (DataColumn column in datatable.Columns)
                {
                    if (column.ColumnName.Equals("CONTACTPERSON"))
                    {
                        flagColumn = true;
                        break;
                    }
                }
                if (!flagColumn) datatable.Columns.Add("CONTACTPERSON");
            }
            else{
                DataRow r = datatable.NewRow();
                DataGridViewRow row = MasterEmployeedatagridview.Rows[currentRow];
                for (int i = 0; i < MasterEmployeedatagridview.Columns.Count; i++){
                    r[i] = row.Cells[i].Value;
                }
                datatable.Rows.Add(r);
                
            }
            if (masterEmployeePassingData != null)
            {
                masterEmployeePassingData(datatable);
            }
            this.Close();
        }

        #region Method FindTextBox
        private void FindtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) {
                
                String strFind = FindtextBox.Text.Contains('*') ? FindtextBox.Text.Replace('*', '%') : (FindtextBox.Text + '%');

                String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY EMPLOYEEID) AS 'No', EMPLOYEEID, PASSWORD, EMPLOYEENAME, POSITION, " +
                                             "[ADDRESS], CONVERT(VARCHAR(10), BOD, 103) AS [BOD], NOHP,"+
                                             "EMAIL, CONVERT(VARCHAR(10), JOINDATE, 103) AS [JOINDATE], STATUS, CREATEDBY " +
                                             "FROM M_EMPLOYEE " +
                                             "WHERE {0} LIKE '{1}' AND STATUS = '{2}' " +
                                             "ORDER BY EMPLOYEEID ASC", columnMasterEmployeeFind, strFind, Status.ACTIVE);
                
                createBackgroundWorkerFillDatagridView(query, "MasterEmployeedatagridview");
            }
        }

        #endregion

        private void Supplierdatagridview_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }

    }
}
