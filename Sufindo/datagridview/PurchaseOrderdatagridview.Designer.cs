﻿namespace Sufindo
{
    partial class PurchaseOrderdatagridview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.FindtextBox = new System.Windows.Forms.TextBox();
            this.datagridviewpanel = new System.Windows.Forms.Panel();
            this.TransactionPOdatagridview = new System.Windows.Forms.DataGridView();
            this.NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PURCHASEORDERID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PURCHASEORDERDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SUPPLIERID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SUPPLIERNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CONTACTPERSONSUPPLIER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KURS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REMARK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATEDBY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContactPersonpanel = new System.Windows.Forms.Panel();
            this.TransactionPOItemdataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUANTITY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UNITPRICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.TotaltextBox = new System.Windows.Forms.TextBox();
            this.datagridviewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionPOdatagridview)).BeginInit();
            this.ContactPersonpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionPOItemdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Find";
            // 
            // FindtextBox
            // 
            this.FindtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FindtextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FindtextBox.Location = new System.Drawing.Point(70, 3);
            this.FindtextBox.Name = "FindtextBox";
            this.FindtextBox.Size = new System.Drawing.Size(212, 31);
            this.FindtextBox.TabIndex = 3;
            this.FindtextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FindtextBox_KeyPress);
            // 
            // datagridviewpanel
            // 
            this.datagridviewpanel.Controls.Add(this.TransactionPOdatagridview);
            this.datagridviewpanel.Location = new System.Drawing.Point(4, 37);
            this.datagridviewpanel.Name = "datagridviewpanel";
            this.datagridviewpanel.Size = new System.Drawing.Size(942, 233);
            this.datagridviewpanel.TabIndex = 7;
            // 
            // TransactionPOdatagridview
            // 
            this.TransactionPOdatagridview.AllowUserToAddRows = false;
            this.TransactionPOdatagridview.AllowUserToDeleteRows = false;
            this.TransactionPOdatagridview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.TransactionPOdatagridview.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NO,
            this.PURCHASEORDERID,
            this.PURCHASEORDERDATE,
            this.SUPPLIERID,
            this.SUPPLIERNAME,
            this.CONTACTPERSONSUPPLIER,
            this.KURS,
            this.RATE,
            this.REMARK,
            this.STATUS,
            this.CREATEDBY});
            this.TransactionPOdatagridview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TransactionPOdatagridview.Location = new System.Drawing.Point(0, 0);
            this.TransactionPOdatagridview.MultiSelect = false;
            this.TransactionPOdatagridview.Name = "TransactionPOdatagridview";
            this.TransactionPOdatagridview.ReadOnly = true;
            this.TransactionPOdatagridview.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TransactionPOdatagridview.Size = new System.Drawing.Size(942, 233);
            this.TransactionPOdatagridview.TabIndex = 0;
            this.TransactionPOdatagridview.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TransactionPOdatagridview_CellDoubleClick);
            this.TransactionPOdatagridview.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.TransactionPOdatagridview_ColumnHeaderMouseClick);
            this.TransactionPOdatagridview.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TransactionPOdatagridview_CellClick);
            this.TransactionPOdatagridview.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TransactionPOdatagridview_KeyDown);
            this.TransactionPOdatagridview.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TransactionPOdatagridview_KeyUp);
            // 
            // NO
            // 
            this.NO.DataPropertyName = "NO";
            this.NO.HeaderText = "NO";
            this.NO.Name = "NO";
            this.NO.ReadOnly = true;
            this.NO.Width = 40;
            // 
            // PURCHASEORDERID
            // 
            this.PURCHASEORDERID.DataPropertyName = "PURCHASEORDERID";
            this.PURCHASEORDERID.HeaderText = "PO NO";
            this.PURCHASEORDERID.Name = "PURCHASEORDERID";
            this.PURCHASEORDERID.ReadOnly = true;
            this.PURCHASEORDERID.Width = 120;
            // 
            // PURCHASEORDERDATE
            // 
            this.PURCHASEORDERDATE.DataPropertyName = "PURCHASEORDERDATE";
            this.PURCHASEORDERDATE.FillWeight = 90F;
            this.PURCHASEORDERDATE.HeaderText = "PO DATE";
            this.PURCHASEORDERDATE.Name = "PURCHASEORDERDATE";
            this.PURCHASEORDERDATE.ReadOnly = true;
            this.PURCHASEORDERDATE.Width = 90;
            // 
            // SUPPLIERID
            // 
            this.SUPPLIERID.DataPropertyName = "SUPPLIERID";
            this.SUPPLIERID.HeaderText = "SUPPLIER ID";
            this.SUPPLIERID.Name = "SUPPLIERID";
            this.SUPPLIERID.ReadOnly = true;
            // 
            // SUPPLIERNAME
            // 
            this.SUPPLIERNAME.DataPropertyName = "SUPPLIERNAME";
            this.SUPPLIERNAME.FillWeight = 120F;
            this.SUPPLIERNAME.HeaderText = "SUPPLIER NAME";
            this.SUPPLIERNAME.Name = "SUPPLIERNAME";
            this.SUPPLIERNAME.ReadOnly = true;
            this.SUPPLIERNAME.Width = 120;
            // 
            // CONTACTPERSONSUPPLIER
            // 
            this.CONTACTPERSONSUPPLIER.DataPropertyName = "CONTACTPERSONSUPPLIER";
            this.CONTACTPERSONSUPPLIER.FillWeight = 120F;
            this.CONTACTPERSONSUPPLIER.HeaderText = "CP SUPPLIER";
            this.CONTACTPERSONSUPPLIER.Name = "CONTACTPERSONSUPPLIER";
            this.CONTACTPERSONSUPPLIER.ReadOnly = true;
            this.CONTACTPERSONSUPPLIER.Width = 120;
            // 
            // KURS
            // 
            this.KURS.DataPropertyName = "KURSID";
            this.KURS.HeaderText = "KURS";
            this.KURS.Name = "KURS";
            this.KURS.ReadOnly = true;
            // 
            // RATE
            // 
            this.RATE.DataPropertyName = "RATE";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle1.Format = "N2";
            this.RATE.DefaultCellStyle = dataGridViewCellStyle1;
            this.RATE.HeaderText = "RATE";
            this.RATE.Name = "RATE";
            this.RATE.ReadOnly = true;
            // 
            // REMARK
            // 
            this.REMARK.DataPropertyName = "REMARK";
            this.REMARK.HeaderText = "REMARK";
            this.REMARK.Name = "REMARK";
            this.REMARK.ReadOnly = true;
            // 
            // STATUS
            // 
            this.STATUS.DataPropertyName = "STATUS";
            this.STATUS.HeaderText = "STATUS";
            this.STATUS.Name = "STATUS";
            this.STATUS.ReadOnly = true;
            // 
            // CREATEDBY
            // 
            this.CREATEDBY.DataPropertyName = "CREATEDBY";
            this.CREATEDBY.HeaderText = "CREATED BY";
            this.CREATEDBY.Name = "CREATEDBY";
            this.CREATEDBY.ReadOnly = true;
            // 
            // ContactPersonpanel
            // 
            this.ContactPersonpanel.Controls.Add(this.TransactionPOItemdataGridView);
            this.ContactPersonpanel.Location = new System.Drawing.Point(4, 277);
            this.ContactPersonpanel.Name = "ContactPersonpanel";
            this.ContactPersonpanel.Size = new System.Drawing.Size(720, 226);
            this.ContactPersonpanel.TabIndex = 24;
            // 
            // TransactionPOItemdataGridView
            // 
            this.TransactionPOItemdataGridView.AllowUserToAddRows = false;
            this.TransactionPOItemdataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TransactionPOItemdataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.TransactionPOItemdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.TransactionPOItemdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.ITEMID,
            this.ITEMNAME,
            this.QUANTITY,
            this.UOMID,
            this.UNITPRICE,
            this.AMOUNT});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.TransactionPOItemdataGridView.DefaultCellStyle = dataGridViewCellStyle5;
            this.TransactionPOItemdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TransactionPOItemdataGridView.Location = new System.Drawing.Point(0, 0);
            this.TransactionPOItemdataGridView.Name = "TransactionPOItemdataGridView";
            this.TransactionPOItemdataGridView.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TransactionPOItemdataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.TransactionPOItemdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TransactionPOItemdataGridView.Size = new System.Drawing.Size(720, 226);
            this.TransactionPOItemdataGridView.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "NO";
            this.dataGridViewTextBoxColumn1.FillWeight = 40F;
            this.dataGridViewTextBoxColumn1.HeaderText = "NO";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 40;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 40;
            // 
            // ITEMID
            // 
            this.ITEMID.DataPropertyName = "ITEMID";
            this.ITEMID.HeaderText = "PART NO";
            this.ITEMID.Name = "ITEMID";
            this.ITEMID.ReadOnly = true;
            // 
            // ITEMNAME
            // 
            this.ITEMNAME.DataPropertyName = "ITEMNAME";
            this.ITEMNAME.HeaderText = "PART NAME";
            this.ITEMNAME.Name = "ITEMNAME";
            this.ITEMNAME.ReadOnly = true;
            // 
            // QUANTITY
            // 
            this.QUANTITY.DataPropertyName = "QUANTITY";
            this.QUANTITY.HeaderText = "QUANTITY";
            this.QUANTITY.Name = "QUANTITY";
            this.QUANTITY.ReadOnly = true;
            // 
            // UOMID
            // 
            this.UOMID.DataPropertyName = "UOMID";
            this.UOMID.HeaderText = "UOM";
            this.UOMID.Name = "UOMID";
            this.UOMID.ReadOnly = true;
            // 
            // UNITPRICE
            // 
            this.UNITPRICE.DataPropertyName = "PRICE";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle3.Format = "N2";
            this.UNITPRICE.DefaultCellStyle = dataGridViewCellStyle3;
            this.UNITPRICE.HeaderText = "UNIT PRICE";
            this.UNITPRICE.Name = "UNITPRICE";
            this.UNITPRICE.ReadOnly = true;
            // 
            // AMOUNT
            // 
            this.AMOUNT.DataPropertyName = "AMOUNT";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle4.Format = "N2";
            this.AMOUNT.DefaultCellStyle = dataGridViewCellStyle4;
            this.AMOUNT.HeaderText = "AMOUNT";
            this.AMOUNT.Name = "AMOUNT";
            this.AMOUNT.ReadOnly = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(529, 509);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 20);
            this.label2.TabIndex = 25;
            this.label2.Text = "Total";
            this.label2.Visible = false;
            // 
            // TotaltextBox
            // 
            this.TotaltextBox.BackColor = System.Drawing.Color.White;
            this.TotaltextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TotaltextBox.Enabled = false;
            this.TotaltextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotaltextBox.Location = new System.Drawing.Point(579, 507);
            this.TotaltextBox.Name = "TotaltextBox";
            this.TotaltextBox.Size = new System.Drawing.Size(145, 26);
            this.TotaltextBox.TabIndex = 26;
            this.TotaltextBox.Visible = false;
            // 
            // PurchaseOrderdatagridview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(949, 538);
            this.Controls.Add(this.TotaltextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ContactPersonpanel);
            this.Controls.Add(this.datagridviewpanel);
            this.Controls.Add(this.FindtextBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PurchaseOrderdatagridview";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "List Of Purchase Order";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PurchaseOrderdatagridview_FormClosing);
            this.datagridviewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TransactionPOdatagridview)).EndInit();
            this.ContactPersonpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TransactionPOItemdataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FindtextBox;
        private System.Windows.Forms.Panel datagridviewpanel;
        private System.Windows.Forms.DataGridView TransactionPOdatagridview;
        private System.Windows.Forms.Panel ContactPersonpanel;
        private System.Windows.Forms.DataGridView TransactionPOItemdataGridView;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TotaltextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn QUANTITY;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn UNITPRICE;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PURCHASEORDERID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PURCHASEORDERDATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn SUPPLIERID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SUPPLIERNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CONTACTPERSONSUPPLIER;
        private System.Windows.Forms.DataGridViewTextBoxColumn KURS;
        private System.Windows.Forms.DataGridViewTextBoxColumn RATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn REMARK;
        private System.Windows.Forms.DataGridViewTextBoxColumn STATUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn CREATEDBY;
    }
}