﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sufindo.Master;

namespace Sufindo
{
    public partial class Invoicedatagridview : Form
    {
        public delegate void InvoicePassingData(DataTable salesOrder);

        private delegate void fillDatatableCallBack(DataTable datatable, String called);

        public InvoicePassingData invoicePassingData;

        private readonly string[] columnInvoice = { "INVOICEID", "SALESORDERID", "CUSTOMERNAME" };
        private Connection con;
        private String columnInvoiceFind = "INVOICEID";

        private String calledForm;

        public Invoicedatagridview()
        {
            InitializeComponent();
        }

        public Invoicedatagridview(String calledForm)
        {
            InitializeComponent();

            if (con == null) {
                con = new Connection();
            }
            this.calledForm = calledForm;
            String query = "";
            if(this.calledForm.Equals("Invoice")){
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.INVOICEID) AS 'NO',A.INVOICEID, CONVERT(VARCHAR(10), INVOICEDATE, 103) AS INVOICEDATE, " +
                                      "A.SALESORDERID, CONVERT(VARCHAR(10), SALESORDERDATE, 103) AS [SALESORDERDATE], " +
                                      "C.CUSTOMERID, C.CUSTOMERNAME, A.CURRENCYID, A.RATE, A.PAYMENTMETHOD, A.DISCOUNT, E.COST, A.TOTAL, A.REMARK, A.STATUS, A.CREATEDBY, D.EMPLOYEENAME " +
                                      "FROM H_INVOICE A " +
                                      "JOIN H_SALES_ORDER B " +
                                      "ON A.SALESORDERID = B.SALESORDERID " +
                                      "JOIN M_CUSTOMER C " +
                                      "ON A.CUSTOMERID = C.CUSTOMERID " +
                                      "JOIN M_EMPLOYEE D " +
                                      "ON A.CREATEDBY = D.EMPLOYEEID " +
                                      "LEFT JOIN COST_INVOICE E " +
                                      "ON E.INVOICEID = A.INVOICEID " +
                                      "WHERE A.STATUS = '{0}' " +
                                      "ORDER BY INVOICEID ASC", Status.ACTIVE);
            }
            else if (this.calledForm.Equals("ListInvoiceReport") || this.calledForm.Equals("OrderPickingReport"))
            {
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.INVOICEID) AS 'NO',A.INVOICEID, CONVERT(VARCHAR(10), INVOICEDATE, 103) AS INVOICEDATE, " +
                                      "A.SALESORDERID, CONVERT(VARCHAR(10), SALESORDERDATE, 103) AS [SALESORDERDATE], " +
                                      "C.CUSTOMERID, C.CUSTOMERNAME, D.EMPLOYEENAME, A.CURRENCYID, A.RATE, A.DISCOUNT, E.COST, A.TOTAL, A.REMARK, A.STATUS, A.CREATEDBY, D.EMPLOYEENAME " +
                                      "FROM H_INVOICE A " +
                                      "JOIN H_SALES_ORDER B " +
                                      "ON A.SALESORDERID = B.SALESORDERID " +
                                      "JOIN M_CUSTOMER C " +
                                      "ON A.CUSTOMERID = C.CUSTOMERID " +
                                      "JOIN M_EMPLOYEE D " +
                                      "ON A.CREATEDBY = D.EMPLOYEEID " +
                                      "LEFT JOIN COST_INVOICE E " +
                                      "ON E.INVOICEID = A.INVOICEID " +
                                      "ORDER BY INVOICEID ASC");
            }
            createBackgroundWorkerFillDatagridView(query, "TransactionInvoicedatagridview");
        }

        void createBackgroundWorkerFillDatagridView(String query, String called)
        {
            for (int i = 0; i < TransactionInvoicedatagridview.Columns.Count; i++)
            {
                if (TransactionInvoicedatagridview.Columns[i].DataPropertyName.Equals(columnInvoiceFind))
                {
                    DataGridViewColumn dataGridViewColumn = TransactionInvoicedatagridview.Columns[i];
                    dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                    break;
                }
            }

            String[] argument = { query, called };
            
            BackgroundWorker bgworker = new BackgroundWorker();
            bgworker.DoWork += new DoWorkEventHandler(bgworker_DoWork);
            bgworker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgworker_RunWorkerCompleted);
            bgworker.RunWorkerAsync(argument);
        }

        void bgworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {

        }

        void bgworker_DoWork(object sender, DoWorkEventArgs e) {
            try
            {
                String[] obj = e.Argument as String[];
                String query = (String) obj[0];
                String called = (String) obj[1];

                BeginInvoke(new fillDatatableCallBack(this.fillDatatable), con.openDataTableQuery(query), called);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void fillDatatable(DataTable datatable, String called)
        {
            if (called.Equals("TransactionInvoicedatagridview")) {
                TransactionInvoicedatagridview.DataSource = datatable;
                this.showTransactionInvoiceItemDataGridView();
            }
            else if (called.Equals("TransactionInvoiceItemdataGridView"))
            {
                TransactionInvoiceItemdataGridView.DataSource = datatable;
            }
        }

        #region Method TransactionInvoicedatagridview
        private void TransactionSOdatagridview_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (invoicePassingData != null)
                {
                    TransactionSOdatagridviewCallPassingData(e.RowIndex);
                }
            }
        }

        private void TransactionSOdatagridview_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                foreach (String item in columnInvoice)
                {
                    if (TransactionInvoicedatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        for (int i = 0; i < TransactionInvoicedatagridview.Columns.Count; i++)
                        {
                            if (TransactionInvoicedatagridview.Columns[i].DataPropertyName.Equals(columnInvoiceFind))
                            {
                                DataGridViewColumn dataGridViewColumn = TransactionInvoicedatagridview.Columns[i];
                                dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Control;
                            }
                        }
                        break;
                    }
                }

                foreach (String item in columnInvoice)
                {
                    if (TransactionInvoicedatagridview.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        columnInvoiceFind = TransactionInvoicedatagridview.Columns[e.ColumnIndex].DataPropertyName;
                        DataGridViewColumn dataGridViewColumn = TransactionInvoicedatagridview.Columns[e.ColumnIndex];
                        dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                        break;
                    }
                }
            }
        }

        private void TransactionSOdatagridview_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                if (TransactionInvoicedatagridview.Rows.Count != 0)
                {
                    e.SuppressKeyPress = true;
                    int currentColumn = TransactionInvoicedatagridview.CurrentCell.ColumnIndex;
                    int currentRow = TransactionInvoicedatagridview.CurrentCell.RowIndex;
                    TransactionInvoicedatagridview.CurrentCell = TransactionInvoicedatagridview[currentColumn, currentRow];

                    TransactionSOdatagridviewCallPassingData(currentRow);
                }
            }
            else
            {
                if (!(e.KeyCode == Keys.Down || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right || e.KeyCode == Keys.Up))
                {
                    if (char.IsLetterOrDigit((char)e.KeyCode))
                    {
                        char keysCode = (char)e.KeyCode;

                        FindtextBox.Focus();
                        FindtextBox.Text = keysCode.ToString();
                        FindtextBox.SelectionStart = FindtextBox.Text.Length;
                    }
                }
                else
                {
                    int currentRow = TransactionInvoicedatagridview.CurrentCell.RowIndex;
                    TransactionInvoicedatagridview.Rows[currentRow].Selected = true;
                }
            }
        }

        private void TransactionSOdatagridview_KeyUp(object sender, KeyEventArgs e)
        {
            this.showTransactionInvoiceItemDataGridView();
        }

        private void TransactionSOdatagridview_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                this.showTransactionInvoiceItemDataGridView();
            }
        }

        private void showTransactionInvoiceItemDataGridView() {
            if(TransactionInvoicedatagridview.Rows.Count == 0){
                while (this.TransactionInvoiceItemdataGridView.Rows.Count > 0)
                {
                    this.TransactionInvoiceItemdataGridView.Rows.RemoveAt(0);
                }
                return;
            }
            int currentRow = TransactionInvoicedatagridview.CurrentCell.RowIndex;
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'No', A.ITEMID, B.ITEMNAME, " +
                                         "A.ITEMIDQTY,  " +
                                         "( " +
                                         "   SELECT ITEMNAME FROM M_ITEM " +
                                         "   WHERE ITEMID = A.ITEMID " +
                                         ") AS ITEMNAMEQTY, " +
                                         "A.QUANTITY, UOMID, PRICE " +
                                         "FROM D_INVOICE A " +
                                         "JOIN M_ITEM B " +
                                         "ON A.ITEMID = B.ITEMID " +
                                         "WHERE INVOICEID = '{0}'", TransactionInvoicedatagridview.Rows[currentRow].Cells["INVOICEID"].Value.ToString());

            createBackgroundWorkerFillDatagridView(query, "TransactionInvoiceItemdataGridView");
        }

        #endregion

        private void TransactionSOdatagridviewCallPassingData(int currentRow) {

            DataTable datatable = new DataTable();
            for (int i = 0; i < TransactionInvoicedatagridview.Columns.Count; i++){
                
                datatable.Columns.Add(TransactionInvoicedatagridview.Columns[i].DataPropertyName);
            }
            
            /*check multiple selected or not*/
            if (TransactionInvoicedatagridview.SelectedRows.Count != 0){
                foreach (var item in TransactionInvoicedatagridview.SelectedRows){
                    DataRow r = datatable.NewRow();
                    DataGridViewRow row = (DataGridViewRow)item;
                    for (int i = 0; i < TransactionInvoicedatagridview.Columns.Count; i++){
                        r[i] = row.Cells[i].Value;
                    }
                    datatable.Rows.Add(r);
                }
            }
            else{
                DataRow r = datatable.NewRow();
                DataGridViewRow row = TransactionInvoicedatagridview.Rows[currentRow];
                for (int i = 0; i < TransactionInvoicedatagridview.Columns.Count; i++){
                    r[i] = row.Cells[i].Value;
                }
                datatable.Rows.Add(r);
            }
            invoicePassingData(datatable);
            this.Close();
        }

        private void FindtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) {
                
                String strFind = FindtextBox.Text.Contains('*') ? FindtextBox.Text.Replace('*', '%') : (FindtextBox.Text + '%');
                String query = "";
                if (this.calledForm.Equals("Invoice"))
                {
                    //columnInvoiceFind = String.Format("A.{0}", columnInvoiceFind);
                    query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.INVOICEID) AS 'NO',A.INVOICEID, CONVERT(VARCHAR(10), INVOICEDATE, 103) AS INVOICEDATE, " +
                                          "A.SALESORDERID, CONVERT(VARCHAR(10), SALESORDERDATE, 103) AS [SALESORDERDATE], " +
                                          "C.CUSTOMERID, C.CUSTOMERNAME, D.EMPLOYEENAME, A.CURRENCYID, A.RATE, A.PAYMENTMETHOD, A.DISCOUNT, E.COST, A.TOTAL, A.REMARK, A.STATUS, A.CREATEDBY, D.EMPLOYEEID " +
                                          "FROM H_INVOICE A " +
                                          "JOIN H_SALES_ORDER B " +
                                          "ON A.SALESORDERID = B.SALESORDERID " +
                                          "JOIN M_CUSTOMER C " +
                                          "ON A.CUSTOMERID = C.CUSTOMERID " +
                                          "JOIN M_EMPLOYEE D " +
                                          "ON A.CREATEDBY = D.EMPLOYEEID " +
                                          "LEFT JOIN COST_INVOICE E " +
                                          "ON E.INVOICEID = A.INVOICEID " +
                                          "WHERE A.{0} LIKE '{1}' AND A.STATUS = '{2}' " +
                                          "ORDER BY INVOICEID ASC", columnInvoiceFind, strFind, Status.ACTIVE);
                }
                else if (this.calledForm.Equals("ListInvoiceReport"))
                {
                    columnInvoiceFind = String.Format("{0}", columnInvoiceFind);
                    query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.INVOICEID) AS 'NO',A.INVOICEID, CONVERT(VARCHAR(10), INVOICEDATE, 103) AS INVOICEDATE, " +
                                          "A.SALESORDERID, CONVERT(VARCHAR(10), SALESORDERDATE, 103) AS [SALESORDERDATE], " +
                                          "C.CUSTOMERID, C.CUSTOMERNAME, D.EMPLOYEENAME, A.CURRENCYID, A.RATE, A.PAYMENTMETHOD, A.DISCOUNT, E.COST, A.TOTAL, A.REMARK, A.STATUS, A.CREATEDBY, D.EMPLOYEEID " +
                                          "FROM H_INVOICE A " +
                                          "JOIN H_SALES_ORDER B " +
                                          "ON A.SALESORDERID = B.SALESORDERID " +
                                          "JOIN M_CUSTOMER C " +
                                          "ON A.CUSTOMERID = C.CUSTOMERID " +
                                          "JOIN M_EMPLOYEE D " +
                                          "ON A.CREATEDBY = D.EMPLOYEEID " +
                                          "LEFT JOIN COST_INVOICE E " +
                                          "ON E.INVOICEID = A.INVOICEID " +
                                          "WHERE A.{0} LIKE '{1}' " +
                                          "ORDER BY INVOICEID ASC", columnInvoiceFind, strFind);
                }

                createBackgroundWorkerFillDatagridView(query, "TransactionInvoicedatagridview");
            }
        }


        private void SalesOrderdatagridview_FormClosing(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }
    }
}
