﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Sufindo.Master;


namespace Sufindo
{
    public partial class Itemdatagridview : Form
    {
        public delegate void AssignItemQuantityPassingData(DataTable assignIte);

        public AssignItemQuantityPassingData assignItemQuantityPassingData;

        public delegate void MasterItemPassingData(DataTable masterItem);

        private delegate void fillDatatableCallBack(DataTable datatable);

        public MasterItemPassingData masterItemPassingData;

        private readonly string[] columnMasterItem = { "ITEMID", "ITEMNAME", "ITEMID_ALIAS" };
        private Connection con;
        private String columnMasterItemFind = "ITEMID_ALIAS";

        private String _ITEMID_ALIAS = "";

        private String INVOICEID;
        private String CALLED = "";
        private String QUERY = "";
        private DataTable itemSupplierDataTable = null;
        private Boolean flagQuery = false;
        public Itemdatagridview(){
            InitializeComponent();
        }

        public Itemdatagridview(String CALLED, String SUPPLIERID, DataTable DATATABLE) {
            InitializeComponent();
            MasterItemdataGridView.MultiSelect = true;
            MasterItemdataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            itemSupplierDataTable = DATATABLE.Copy();
            if (con == null){
                con = new Connection();
            }

            this.CALLED = CALLED;

            if (this.CALLED.Equals("PURCHASEORDER")) {
                addColumnsForPurchaseOrder();
                this.QUERY = Query.SELECT_ITEM_FOR_PO_WITH_SUPPLIER(Status.ACTIVE, SUPPLIERID);
                createBackgroundWorkerFillDatagridView(this.QUERY);
            }
            else if (this.CALLED.Equals("PURCHASERECEIPT"))
            {
                addColumnsForPurchase();
                //this.QUERY = Query.SELECT_ITEM_FOR_PR_WITH_SUPPLIER(Status.ACTIVE, SUPPLIERID);
            }
            else if (this.CALLED.Equals("PURCHASERETURN")) {
                addColumnsForPurchaseOrder();
                //this.QUERY = Query.SELECT_ITEM_FOR_PRETURN(Status.ACTIVE, "");
            }
            else if (this.CALLED.Equals("SALESORDER"))
            {
                addColumnsForSalesOrder();
                //this.QUERY = Query.SELECT_ITEM_FOR_SO(Status.ACTIVE);
            }
            else if (this.CALLED.Equals("SALESRETURN"))
            {
                addColumnsForSalesReturn();
                //this.QUERY = Query.SELECT_ITEM_FOR_INVOICE(Status.ACTIVE, SUPPLIERID);
            }
            else if (this.CALLED.Equals("REVISIONRECEIPT"))
            {
                addColumnsForItemRevision();
                INVOICEID = SUPPLIERID;//purchaseid
                //this.QUERY = Query.SELECT_ITEM_FOR_REVISION(Status.ACTIVE, INVOICEID);
            }
            //createBackgroundWorkerFillDatagridView(this.QUERY);
            changeColorDataGridViewColumn();
        }

        public Itemdatagridview(String CALLED)
        {
            InitializeComponent();
            
            if (con == null) {
                con = new Connection();
            }
            this.CALLED = CALLED;
            String query = Query.SELECT_ITEM(Status.ACTIVE, "");
            addColumnsForMasterItem();
            
            //createBackgroundWorkerFillDatagridView(query);
            changeColorDataGridViewColumn();
        }

        public Itemdatagridview(String CALLED, DataTable DATATABLE)
        {
            InitializeComponent();

            this.CALLED = CALLED;
            MasterItemdataGridView.MultiSelect = true;

            MasterItemdataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            if (con == null)
            {
                con = new Connection();
            }
            itemSupplierDataTable = DATATABLE;
            if (this.CALLED.Equals("ITEMSUPPLIER"))
            {
                addColumnsForMasterItem();
                this.QUERY = Query.SELECT_ITEM(Status.ACTIVE, "");
            }
            else if (this.CALLED.Equals("ITEMBUILD")) {
                addColumnsForItemBuild();
                this.QUERY = Query.SELECT_ITEM_FOR_BUILD_ITEM(Status.ACTIVE, "");
            }
            //createBackgroundWorkerFillDatagridView(this.QUERY);
            changeColorDataGridViewColumn();
        }

        public Itemdatagridview(String CALLED, DataTable DATATABLE, Boolean MultiSelect)
        {
            InitializeComponent();

            this.CALLED = CALLED;

            MasterItemdataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            if (con == null)
            {
                con = new Connection();
            }
            itemSupplierDataTable = DATATABLE;
            
            addColumnsForItemBuild();
            this.QUERY = Query.SELECT_ITEM_FOR_BUILD_ITEM(Status.ACTIVE, "");
            MasterItemdataGridView.MultiSelect = MultiSelect;

            //createBackgroundWorkerFillDatagridView(this.QUERY);
            changeColorDataGridViewColumn();
        }

        public Itemdatagridview(String CALLED, String ITEMID_ALIAS)
        {
            InitializeComponent();
            this.CALLED = CALLED;
            if (con == null) con = new Connection();

            if (CALLED.Equals("MASTERITEMALIAS"))
            {
                addColumnsForMasterItem();
                this._ITEMID_ALIAS = ITEMID_ALIAS;

                this.QUERY = Query.SELECT_ITEM_WITH_ALIAS(Status.ACTIVE, this._ITEMID_ALIAS, "");

                //createBackgroundWorkerFillDatagridView(this.QUERY);
            }
            else if (CALLED.Equals("ASSIGNITEMPURCHASERECEIPT"))
            {
                addColumnsForMasterItem();
                String query = String.Format("SELECT ITEMID_ALIAS FROM M_ITEM " +
                                             "WHERE ITEMID = '{0}' AND [STATUS] = '{1}'", ITEMID_ALIAS, Status.ACTIVE);
                DataTable dt = con.openDataTableQuery(query);

                SqlDataReader sqldataReader = con.sqlDataReaders(query);

                while (sqldataReader.Read())
                {
                    if (!sqldataReader.IsDBNull(0)){
                        this._ITEMID_ALIAS = sqldataReader.GetString(0);
                    }
                    else{
                        this._ITEMID_ALIAS = "";
                    }
                }

                this.QUERY = Query.SELECT_ITEM_WITH_ALIAS(Status.ACTIVE, this._ITEMID_ALIAS, "");

                //createBackgroundWorkerFillDatagridView(this.QUERY);
            }
            changeColorDataGridViewColumn();
        }

        void createBackgroundWorkerFillDatagridView(String query) {

            for (int i = 0; i < MasterItemdataGridView.Columns.Count; i++)
            {
                if (MasterItemdataGridView.Columns[i].DataPropertyName.Equals(columnMasterItemFind))
                {
                    DataGridViewColumn dataGridViewColumn = MasterItemdataGridView.Columns[i];
                    dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                    break;
                }
            }
            //MasterItemdataGridView.DataSource = con.openDataTableQuery(query);
            BackgroundWorker bgworker = new BackgroundWorker();
            bgworker.DoWork += new DoWorkEventHandler(bgworker_DoWork);
            bgworker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgworker_RunWorkerCompleted);
            bgworker.RunWorkerAsync(query);
        }

        void changeColorDataGridViewColumn() {
            foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
            {
                if (column.DataPropertyName.Equals(columnMasterItemFind))
                {
                    column.HeaderCell.Style.BackColor = SystemColors.Info;
                    break;
                }
            }
        }

        void bgworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //MessageBox.Show("completed");
            
        }

        void bgworker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                System.Threading.Thread.Sleep(1);
                String query = e.Argument as String;
                if (IsHandleCreated)
                {
                    MasterItemdataGridView.Invoke(new fillDatatableCallBack(this.fillDatatable), con.openDataTableQuery(query));
                }
            }   
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void fillDatatable(DataTable datatable){
            if (itemSupplierDataTable == null)
            {
                MasterItemdataGridView.DataSource = datatable;
            }
            else
            {

                foreach (DataRow row in itemSupplierDataTable.Rows)
                {
                    var query = datatable.Rows.Cast<DataRow>().Where(r => r["ITEMID"].Equals(row["ITEMID"].ToString())).ToArray();

                    foreach (DataRow r in query)
                    {
                        datatable.Rows.Remove(r);
                    }
                }
                Utilities.generateNoDataTable(ref datatable);

                MasterItemdataGridView.DataSource = datatable;

                
            }
            this.flagQuery = false;
        }

        #region Method MasterItemdataGridview
        private void MasterItemdataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1){
                if (masterItemPassingData != null){
                    MasterItemdataGridViewCallPassingData(e.RowIndex);
                }
                else if (assignItemQuantityPassingData != null) {
                    MasterItemdataGridViewCallPassingData(e.RowIndex);
                }
            }
        }

        private void MasterItemdataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                foreach (String item in columnMasterItem){
                    if (MasterItemdataGridView.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        for (int i = 0; i < MasterItemdataGridView.Columns.Count; i++)
                        {
                            if (MasterItemdataGridView.Columns[i].DataPropertyName.Equals(columnMasterItemFind))
                            {
                                DataGridViewColumn dataGridViewColumn = MasterItemdataGridView.Columns[i];
                                dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Control;
                            }
                        }
                        break;
                    }
                }
                foreach (String item in columnMasterItem)
                {
                    if (MasterItemdataGridView.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        columnMasterItemFind = MasterItemdataGridView.Columns[e.ColumnIndex].DataPropertyName;
                        DataGridViewColumn dataGridViewColumn = MasterItemdataGridView.Columns[e.ColumnIndex];
                        dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                        break;
                    }
                }
            }
        }

        private void MasterItemdataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (MasterItemdataGridView.Rows.Count != 0)
                {
                    e.SuppressKeyPress = true;
                    int currentColumn = MasterItemdataGridView.CurrentCell.ColumnIndex;
                    int currentRow = MasterItemdataGridView.CurrentCell.RowIndex;
                    MasterItemdataGridView.CurrentCell = MasterItemdataGridView[currentColumn, currentRow];
                    MasterItemdataGridViewCallPassingData(currentRow);
                }
            }
            else
            {
                if (!(e.KeyCode == Keys.Down || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right || e.KeyCode == Keys.Up))
                {
                    if (char.IsLetterOrDigit((char)e.KeyCode) && !(e.Control || e.Alt))
                    {
                        char keysCode = (char)e.KeyCode;

                        FindtextBox.Focus();
                        FindtextBox.Text = keysCode.ToString();
                        FindtextBox.SelectionStart = FindtextBox.Text.Length;
                    }
                }
                else 
                {
                    int currentRow = MasterItemdataGridView.CurrentCell.RowIndex;
                    MasterItemdataGridView.Rows[currentRow].Selected = true;
                }
            }
        }
        #endregion

        private void MasterItemdataGridViewCallPassingData(int currentRow) {

            DataTable datatable = new DataTable();
            for (int i = 0; i < MasterItemdataGridView.Columns.Count; i++){

                if (MasterItemdataGridView.Columns[i].DataPropertyName.Equals("ITEMIMAGE")){
                    datatable.Columns.Add(MasterItemdataGridView.Columns[i].DataPropertyName, typeof(byte[]));
                }
                else datatable.Columns.Add(MasterItemdataGridView.Columns[i].DataPropertyName);
            }
            
            /*check multiple selected or not*/
            if (MasterItemdataGridView.SelectedRows.Count != 0){
                foreach (var item in MasterItemdataGridView.SelectedRows){
                    DataRow r = datatable.NewRow();
                    DataGridViewRow rowdatagridview = (DataGridViewRow)item;
                    //Console.WriteLine(rowdatagridview.Cells["QUANTITY"].Value);
                    DataGridViewRow row = (DataGridViewRow)item;
                    for (int i = 0; i < MasterItemdataGridView.Columns.Count; i++){
                        if (this.CALLED.Equals("SALESORDER") && MasterItemdataGridView.Columns[i].DataPropertyName.Equals("QUANTITY")) {
                            Console.WriteLine(MasterItemdataGridView.Columns[i].DataPropertyName);
                            row.Cells[i].Value = 1;
                        }
                        r[i] = row.Cells[i].Value;
                    }
                    datatable.Rows.Add(r);
                }
            }
            else{
                DataRow r = datatable.NewRow();
                DataGridViewRow row = MasterItemdataGridView.Rows[currentRow];
                for (int i = 0; i < MasterItemdataGridView.Columns.Count; i++){
                    if (this.CALLED.Equals("SALESORDER") && MasterItemdataGridView.Columns[i].DataPropertyName.Equals("QUANTITY"))
                    {
                        Console.WriteLine(MasterItemdataGridView.Columns[i].DataPropertyName);
                        row.Cells[i].Value = 1;
                    }
                    r[i] = row.Cells[i].Value;
                }
                datatable.Rows.Add(r);
            }

            if (this.CALLED.Equals("ASSIGNITEMPURCHASERECEIPT")){
                assignItemQuantityPassingData(datatable);
            }
            else  masterItemPassingData(datatable);
            this.Close();
        }

        #region Method FindTextBox
        private void FindtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter) && !this.flagQuery)
            {
                
                String strFind = FindtextBox.Text.Contains('*') ? FindtextBox.Text.Replace('*', '%') : (FindtextBox.Text + '%');

                String addictional = columnMasterItemFind.Equals("ITEMID_ALIAS") ? String.Format("OR {0} IN (SELECT ITEMID_ALIAS FROM M_ITEM WHERE ITEMID LIKE '{1}') ", columnMasterItemFind, strFind) : "";

                String FIND = String.Format("( A.{0} LIKE '{1}' {2} ) AND ", columnMasterItemFind.Equals("ITEMID_ALIAS") ? "ITEMID" : columnMasterItemFind, strFind, addictional);
                //String query = Query.SELECT_ITEM(Status.ACTIVE, FIND);
                String query = "";
                if (CALLED.Equals("PURCHASEORDER"))
                {
                    query = Query.SELECT_ITEM_FOR_PO_WITHOUT_SUPPLIER(Status.ACTIVE, FIND);
                }
                else if (CALLED.Equals("AdjustmentSpoil") || CALLED.Equals("MASTERITEM") || CALLED.Equals("POVIEW") || CALLED.Equals("ITEMSUPPLIER") || CALLED.Equals("RecordCardStockItem") || CALLED.Equals("ListReceiptRevisionReport") || CALLED.Equals("ListReceiptRevisionReport") || CALLED.Equals("ListPurchaseReturnReport"))
                {
                    query = Query.SELECT_ITEM(Status.ACTIVE, FIND);
                }
                else if (CALLED.Equals("MASTERITEMALIAS"))
                {
                    FIND = String.Format("{0} LIKE '{1}' AND ", columnMasterItemFind, strFind);
                    query = Query.SELECT_ITEM_WITH_ALIAS(Status.ACTIVE, this._ITEMID_ALIAS, FIND);
                }
                else if (CALLED.Equals("PURCHASERECEIPT"))
                {
                    //query = Query.SELECT_ITEM_FOR_PR_WITHOUT_SUPPLIER(Status.ACTIVE, FIND);
                    query = Query.SELECT_ITEM_FOR_PO_WITHOUT_SUPPLIER(Status.ACTIVE, FIND);
                }
                else if (CALLED.Equals("PURCHASERETURN")) {
                    query = Query.SELECT_ITEM_FOR_PRETURN(Status.ACTIVE, FIND);
                }
                else if (CALLED.Equals("ASSIGNITEMPURCHASERECEIPT"))
                {
                    FIND = String.Format("{0} LIKE '{1}' AND ", columnMasterItemFind, strFind);
                    query = Query.SELECT_ITEM_WITH_ALIAS(Status.ACTIVE, this._ITEMID_ALIAS, FIND);
                }
                else if (CALLED.Equals("ITEMBUILD"))
                {
                    query = Query.SELECT_ITEM_FOR_BUILD_ITEM(Status.ACTIVE, FIND);
                }
                else if (CALLED.Equals("SALESORDER"))
                {
                    query = Query.SELECT_ITEM_FOR_SO(Status.ACTIVE, FIND);
                }
                else if (CALLED.Equals("SALESRETURN")) {
                    query = Query.SELECT_ITEM_FOR_INVOICE(Status.READY, INVOICEID, FIND); 
                }
                else if (this.CALLED.Equals("REVISIONRECEIPT")) {
                    query = Query.SELECT_ITEM_FOR_REVISION(INVOICEID,Status.ACTIVE, FIND);
                }
                createBackgroundWorkerFillDatagridView(query);
                this.flagQuery = true;
            }
        }
        #endregion

        private void Itemdatagridview_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }
        
        //protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        //{
        //    foreach (Keys arrows in arrowsList)
        //    {
        //        if (keyData == arrows) { 
                    
        //        }
        //    }

        //    if (keyData == (Keys.Control | Keys.F))
        //    {
        //        MessageBox.Show("What the Ctrl+F?");
        //        return true;
        //    }
        //    return base.ProcessCmdKey(ref msg, keyData);
        //}

        private void addColumnsForMasterItem() {
            MasterItemdataGridView.Columns.Clear();
            var NO = new DataGridViewTextBoxColumn();
            NO.Name = "NO";
            NO.HeaderText = "NO";
            NO.DataPropertyName = "NO";
            NO.Width = 40;

            var ITEMID = new DataGridViewTextBoxColumn();
            ITEMID.Name = "ITEMID";
            ITEMID.HeaderText = "PART NO";
            ITEMID.DataPropertyName = "ITEMID";
            ITEMID.Width = 140;

            var ITEMNAME = new DataGridViewTextBoxColumn();
            ITEMNAME.Name = "ITEMNAME";
            ITEMNAME.HeaderText = "PART NAME";
            ITEMNAME.DataPropertyName = "ITEMNAME";
            ITEMNAME.Width = 120;

            var ITEMID_ALIAS = new DataGridViewTextBoxColumn();
            ITEMID_ALIAS.Name = "ITEMID_ALIAS";
            ITEMID_ALIAS.HeaderText = "ALIAS";
            ITEMID_ALIAS.DataPropertyName = "ITEMID_ALIAS";
            ITEMID_ALIAS.Width = 140;

            var BRANDID = new DataGridViewTextBoxColumn();
            BRANDID.Name = "BRANDID";
            BRANDID.HeaderText = "BRAND";
            BRANDID.DataPropertyName = "BRANDID";
            BRANDID.Width = 100;

            var SPEC = new DataGridViewTextBoxColumn();
            SPEC.Name = "SPEC";
            SPEC.HeaderText = "SPEC";
            SPEC.DataPropertyName = "SPEC";
            SPEC.Width = 100;

            var QUANTITY = new DataGridViewTextBoxColumn();
            QUANTITY.Name = "QUANTITY";
            QUANTITY.HeaderText = "QUANTITY";
            QUANTITY.DataPropertyName = "QUANTITY";
            QUANTITY.Width = 70;

            var UOMID = new DataGridViewTextBoxColumn();
            UOMID.Name = "UOMID";
            UOMID.HeaderText = "UOM";
            UOMID.DataPropertyName = "UOMID";
            UOMID.Width = 70;

            var RACKID = new DataGridViewTextBoxColumn();
            RACKID.Name = "RACKID";
            RACKID.HeaderText = "RACK";
            RACKID.DataPropertyName = "RACKID";
            RACKID.Width = 100;

            var ITEMIMAGE = new DataGridViewTextBoxColumn();
            ITEMIMAGE.Name = "ITEMIMAGE";
            ITEMIMAGE.HeaderText = "ITEMIMAGE";
            ITEMIMAGE.DataPropertyName = "ITEMIMAGE";
            ITEMIMAGE.Width = 70;
            ITEMIMAGE.Visible = false;

            var BESTPRICE = new DataGridViewTextBoxColumn();
            BESTPRICE.Name = "BESTPRICE";
            BESTPRICE.HeaderText = "SALE PRICE";
            BESTPRICE.DataPropertyName = "BESTPRICE";
            BESTPRICE.DefaultCellStyle.Format = "N2";
            BESTPRICE.DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
            BESTPRICE.Width = 120;
            BESTPRICE.Visible = true;

            var CREATEDBY = new DataGridViewTextBoxColumn();
            CREATEDBY.Name = "CREATEDBY";
            CREATEDBY.HeaderText = "CREATEDBY";
            CREATEDBY.DataPropertyName = "CREATEDBY";
            CREATEDBY.Width = 70;
            CREATEDBY.Visible = false;

            var UPDATEDATE = new DataGridViewTextBoxColumn();
            UPDATEDATE.Name = "UPDATEDDATE";
            UPDATEDATE.HeaderText = "UPDATEDDATE";
            UPDATEDATE.DataPropertyName = "UPDATEDDATE";
            UPDATEDATE.Width = 70;
            UPDATEDATE.Visible = false;

            MasterItemdataGridView.Columns.AddRange(
                                                        new DataGridViewColumn[]{ 
                                                                                    NO, ITEMID, ITEMNAME, ITEMID_ALIAS, 
                                                                                    BRANDID, SPEC, QUANTITY, UOMID, 
                                                                                    RACKID, BESTPRICE, ITEMIMAGE, CREATEDBY, UPDATEDATE
                                                                                }
                                                   );
        }
        
        private void addColumnsForPurchaseOrder() {
            MasterItemdataGridView.Columns.Clear();
            var NO = new DataGridViewTextBoxColumn();
            NO.Name = "NO";
            NO.HeaderText = "NO";
            NO.DataPropertyName = "NO";
            NO.Width = 40;

            var ITEMID = new DataGridViewTextBoxColumn();
            ITEMID.Name = "ITEMID";
            ITEMID.HeaderText = "PART NO";
            ITEMID.DataPropertyName = "ITEMID";
            ITEMID.Width = 140;

            var ITEMNAME = new DataGridViewTextBoxColumn();
            ITEMNAME.Name = "ITEMNAME";
            ITEMNAME.HeaderText = "PART NAME";
            ITEMNAME.DataPropertyName = "ITEMNAME";
            ITEMNAME.Width = 120;

            var ITEMID_ALIAS = new DataGridViewTextBoxColumn();
            ITEMID_ALIAS.Name = "ITEMID_ALIAS";
            ITEMID_ALIAS.HeaderText = "ALIAS";
            ITEMID_ALIAS.DataPropertyName = "ITEMID_ALIAS";
            ITEMID_ALIAS.Width = 140;

            var ITEMNAME_ALIAS = new DataGridViewTextBoxColumn();
            ITEMNAME_ALIAS.Name = "ITEMNAME_ALIAS";
            ITEMNAME_ALIAS.HeaderText = "ITEMNAME_ALIAS";
            ITEMNAME_ALIAS.DataPropertyName = "ITEMNAME_ALIAS";
            ITEMNAME_ALIAS.Width = 140;
            ITEMNAME_ALIAS.Visible = false;

            var BRANDID = new DataGridViewTextBoxColumn();
            BRANDID.Name = "BRANDID";
            BRANDID.HeaderText = "BRAND";
            BRANDID.DataPropertyName = "BRANDID";
            BRANDID.Width = 100;

            var SPEC = new DataGridViewTextBoxColumn();
            SPEC.Name = "SPEC";
            SPEC.HeaderText = "SPEC";
            SPEC.DataPropertyName = "SPEC";
            SPEC.Width = 100;

            var QUANTITY = new DataGridViewTextBoxColumn();
            QUANTITY.Name = "QUANTITY";
            QUANTITY.HeaderText = "QUANTITY";
            QUANTITY.DataPropertyName = "QUANTITY";
            QUANTITY.Width = 70;

            var UOMID = new DataGridViewTextBoxColumn();
            UOMID.Name = "UOMID";
            UOMID.HeaderText = "UOM";
            UOMID.DataPropertyName = "UOMID";
            UOMID.Width = 70;

            var RACKID = new DataGridViewTextBoxColumn();
            RACKID.Name = "RACKID";
            RACKID.HeaderText = "RACK";
            RACKID.DataPropertyName = "RACKID";
            RACKID.Width = 100;

            var UNITPRICE = new DataGridViewTextBoxColumn();
            UNITPRICE.Name = "UNITPRICE";
            UNITPRICE.HeaderText = "UNIT PRICE";
            UNITPRICE.DataPropertyName = "UNITPRICE";
            UNITPRICE.Width = 70;
            UNITPRICE.Visible = false;

            var AMOUNT = new DataGridViewTextBoxColumn();
            AMOUNT.Name = "AMOUNT";
            AMOUNT.HeaderText = "AMOUNT";
            AMOUNT.DataPropertyName = "AMOUNT";
            AMOUNT.Width = 70;
            AMOUNT.Visible = false;

            MasterItemdataGridView.Columns.AddRange(
                                                        new DataGridViewColumn[]{ 
                                                                                    NO, ITEMID, ITEMNAME, ITEMID_ALIAS, 
                                                                                    BRANDID, SPEC, QUANTITY, UOMID, 
                                                                                    RACKID, UNITPRICE, AMOUNT,ITEMNAME_ALIAS
                                                                                }
                                                   );
            //this.MasterItemdataGridView.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.MasterItemdataGridView_RowPrePaint);
        }

        private void addColumnsForPurchase() {
            MasterItemdataGridView.Columns.Clear();
            var NO = new DataGridViewTextBoxColumn();
            NO.Name = "NO";
            NO.HeaderText = "NO";
            NO.DataPropertyName = "NO";
            NO.Width = 40;

            var ITEMID = new DataGridViewTextBoxColumn();
            ITEMID.Name = "ITEMID";
            ITEMID.HeaderText = "PART NO";
            ITEMID.DataPropertyName = "ITEMID";
            ITEMID.Width = 140;

            var ITEMNAME = new DataGridViewTextBoxColumn();
            ITEMNAME.Name = "ITEMNAME";
            ITEMNAME.HeaderText = "PART NAME";
            ITEMNAME.DataPropertyName = "ITEMNAME";
            ITEMNAME.Width = 120;

            var ITEMID_ALIAS = new DataGridViewTextBoxColumn();
            ITEMID_ALIAS.Name = "ITEMID_ALIAS";
            ITEMID_ALIAS.HeaderText = "ALIAS";
            ITEMID_ALIAS.DataPropertyName = "ITEMID_ALIAS";
            ITEMID_ALIAS.Width = 140;

            var ITEMNAME_ALIAS = new DataGridViewTextBoxColumn();
            ITEMNAME_ALIAS.Name = "ITEMNAME_ALIAS";
            ITEMNAME_ALIAS.HeaderText = "ALIAS NAME";
            ITEMNAME_ALIAS.DataPropertyName = "ITEMNAME_ALIAS";
            ITEMNAME_ALIAS.Width = 100;
            ITEMNAME_ALIAS.Visible = false;

            var BRANDID = new DataGridViewTextBoxColumn();
            BRANDID.Name = "BRANDID";
            BRANDID.HeaderText = "BRAND";
            BRANDID.DataPropertyName = "BRANDID";
            BRANDID.Width = 100;

            var SPEC = new DataGridViewTextBoxColumn();
            SPEC.Name = "SPEC";
            SPEC.HeaderText = "SPEC";
            SPEC.DataPropertyName = "SPEC";
            SPEC.Width = 100;

            var QUANTITY = new DataGridViewTextBoxColumn();
            QUANTITY.Name = "QUANTITY";
            QUANTITY.HeaderText = "QUANTITY";
            QUANTITY.DataPropertyName = "QUANTITY";
            QUANTITY.Width = 70;

            var UOMID = new DataGridViewTextBoxColumn();
            UOMID.Name = "UOMID";
            UOMID.HeaderText = "UOM";
            UOMID.DataPropertyName = "UOMID";
            UOMID.Width = 70;

            var RACKID = new DataGridViewTextBoxColumn();
            RACKID.Name = "RACKID";
            RACKID.HeaderText = "RACK";
            RACKID.DataPropertyName = "RACKID";
            RACKID.Width = 100;

            var UNITPRICE = new DataGridViewTextBoxColumn();
            UNITPRICE.Name = "UNITPRICE";
            UNITPRICE.HeaderText = "UNIT PRICE";
            UNITPRICE.DataPropertyName = "UNITPRICE";
            UNITPRICE.Width = 70;
            UNITPRICE.Visible = false;

            var AMOUNT = new DataGridViewTextBoxColumn();
            AMOUNT.Name = "AMOUNT";
            AMOUNT.HeaderText = "AMOUNT";
            AMOUNT.DataPropertyName = "AMOUNT";
            AMOUNT.Width = 70;
            AMOUNT.Visible = false;

            MasterItemdataGridView.Columns.AddRange(
                                                        new DataGridViewColumn[]{ 
                                                                                    NO, ITEMID, ITEMNAME, ITEMID_ALIAS, ITEMNAME_ALIAS,
                                                                                    BRANDID, SPEC, QUANTITY, UOMID, 
                                                                                    RACKID, UNITPRICE, AMOUNT 
                                                                                }
                                                   );
        }

        private void addColumnsForSalesOrder()
        {
            MasterItemdataGridView.Columns.Clear();
            var NO = new DataGridViewTextBoxColumn();
            NO.Name = "NO";
            NO.HeaderText = "NO";
            NO.DataPropertyName = "NO";
            NO.Width = 40;

            var ITEMID = new DataGridViewTextBoxColumn();
            ITEMID.Name = "ITEMID";
            ITEMID.HeaderText = "PART NO";
            ITEMID.DataPropertyName = "ITEMID";
            ITEMID.Width = 140;

            var ITEMNAME = new DataGridViewTextBoxColumn();
            ITEMNAME.Name = "ITEMNAME";
            ITEMNAME.HeaderText = "PART NAME";
            ITEMNAME.DataPropertyName = "ITEMNAME";
            ITEMNAME.Width = 120;

            var ITEMID_ALIAS = new DataGridViewTextBoxColumn();
            ITEMID_ALIAS.Name = "ITEMID_ALIAS";
            ITEMID_ALIAS.HeaderText = "ALIAS";
            ITEMID_ALIAS.DataPropertyName = "ITEMID_ALIAS";
            ITEMID_ALIAS.Width = 140;

            var ITEMNAME_ALIAS = new DataGridViewTextBoxColumn();
            ITEMNAME_ALIAS.Name = "ITEMNAME_ALIAS";
            ITEMNAME_ALIAS.HeaderText = "ALIAS NAME";
            ITEMNAME_ALIAS.DataPropertyName = "ITEMNAME_ALIAS";
            ITEMNAME_ALIAS.Width = 100;
            ITEMNAME_ALIAS.Visible = false;

            var BRANDID = new DataGridViewTextBoxColumn();
            BRANDID.Name = "BRANDID";
            BRANDID.HeaderText = "BRAND";
            BRANDID.DataPropertyName = "BRANDID";
            BRANDID.Width = 100;

            var SPEC = new DataGridViewTextBoxColumn();
            SPEC.Name = "SPEC";
            SPEC.HeaderText = "SPEC";
            SPEC.DataPropertyName = "SPEC";
            SPEC.Width = 100;

            var STOCK = new DataGridViewTextBoxColumn();
            STOCK.Name = "STOCK";
            STOCK.HeaderText = "STOCK";
            STOCK.DataPropertyName = "STOCK";
            STOCK.Width = 70;
            STOCK.Visible = false;

            var QUANTITY = new DataGridViewTextBoxColumn();
            QUANTITY.Name = "QUANTITY";
            QUANTITY.HeaderText = "QUANTITY";
            QUANTITY.DataPropertyName = "QUANTITY";
            QUANTITY.Width = 70;

            var UOMID = new DataGridViewTextBoxColumn();
            UOMID.Name = "UOMID";
            UOMID.HeaderText = "UOM";
            UOMID.DataPropertyName = "UOMID";
            UOMID.Width = 70;

            var RACKID = new DataGridViewTextBoxColumn();
            RACKID.Name = "RACKID";
            RACKID.HeaderText = "RACK";
            RACKID.DataPropertyName = "RACKID";
            RACKID.Width = 100;

            var UNITPRICE = new DataGridViewTextBoxColumn();
            UNITPRICE.Name = "UNITPRICE";
            UNITPRICE.HeaderText = "UNIT PRICE";
            UNITPRICE.DataPropertyName = "UNITPRICE";
            UNITPRICE.Width = 70;
            UNITPRICE.Visible = false;

            var AMOUNT = new DataGridViewTextBoxColumn();
            AMOUNT.Name = "AMOUNT";
            AMOUNT.HeaderText = "AMOUNT";
            AMOUNT.DataPropertyName = "AMOUNT";
            AMOUNT.Width = 70;
            AMOUNT.Visible = false;

            MasterItemdataGridView.Columns.AddRange(
                                                        new DataGridViewColumn[]{ 
                                                                                    NO, ITEMID, ITEMNAME, ITEMID_ALIAS, ITEMNAME_ALIAS,
                                                                                    BRANDID, SPEC, QUANTITY, UOMID, 
                                                                                    RACKID, UNITPRICE, AMOUNT, STOCK
                                                                                }
                                                   );
        }

        private void addColumnsForSalesReturn()
        {
            MasterItemdataGridView.Columns.Clear();
            var NO = new DataGridViewTextBoxColumn();
            NO.Name = "NO";
            NO.HeaderText = "NO";
            NO.DataPropertyName = "NO";
            NO.Width = 40;

            var ITEMID = new DataGridViewTextBoxColumn();
            ITEMID.Name = "ITEMID";
            ITEMID.HeaderText = "PART NO";
            ITEMID.DataPropertyName = "ITEMID";
            ITEMID.Width = 140;

            var ITEMNAME = new DataGridViewTextBoxColumn();
            ITEMNAME.Name = "ITEMNAME";
            ITEMNAME.HeaderText = "PART NAME";
            ITEMNAME.DataPropertyName = "ITEMNAME";
            ITEMNAME.Width = 120;

            var ITEMIDQTY = new DataGridViewTextBoxColumn();
            ITEMIDQTY.Name = "ITEMIDQTY";
            ITEMIDQTY.HeaderText = "PART NO QTY";
            ITEMIDQTY.DataPropertyName = "ITEMID_ALIAS";
            ITEMIDQTY.Width = 140;

            var ITEMNAMEQTY = new DataGridViewTextBoxColumn();
            ITEMNAMEQTY.Name = "ITEMNAMEQTY";
            ITEMNAMEQTY.HeaderText = "PART NAME QTY";
            ITEMNAMEQTY.DataPropertyName = "ITEMNAMEQTY";
            ITEMNAMEQTY.Width = 100;

            var BRANDID = new DataGridViewTextBoxColumn();
            BRANDID.Name = "BRANDID";
            BRANDID.HeaderText = "BRAND";
            BRANDID.DataPropertyName = "BRANDID";
            BRANDID.Width = 100;

            var SPEC = new DataGridViewTextBoxColumn();
            SPEC.Name = "SPEC";
            SPEC.HeaderText = "SPEC";
            SPEC.DataPropertyName = "SPEC";
            SPEC.Width = 100;

            var QUANTITY = new DataGridViewTextBoxColumn();
            QUANTITY.Name = "QUANTITY";
            QUANTITY.HeaderText = "QUANTITY";
            QUANTITY.DataPropertyName = "QUANTITY";
            QUANTITY.Width = 70;

            var RETURNQUANTITY = new DataGridViewTextBoxColumn();
            RETURNQUANTITY.Name = "RETURNQUANTITY";
            RETURNQUANTITY.HeaderText = "RETURNQUANTITY";
            RETURNQUANTITY.DataPropertyName = "RETURNQUANTITY";
            RETURNQUANTITY.Width = 70;
            RETURNQUANTITY.Visible = false;

            var UOMID = new DataGridViewTextBoxColumn();
            UOMID.Name = "UOMID";
            UOMID.HeaderText = "UOM";
            UOMID.DataPropertyName = "UOMID";
            UOMID.Width = 70;

            var RACKID = new DataGridViewTextBoxColumn();
            RACKID.Name = "RACKID";
            RACKID.HeaderText = "RACK";
            RACKID.DataPropertyName = "RACKID";
            RACKID.Width = 100;

            var UNITPRICE = new DataGridViewTextBoxColumn();
            UNITPRICE.Name = "UNITPRICE";
            UNITPRICE.HeaderText = "UNIT PRICE";
            UNITPRICE.DataPropertyName = "UNITPRICE";
            UNITPRICE.Width = 70;
            UNITPRICE.Visible = false;

            var AMOUNT = new DataGridViewTextBoxColumn();
            AMOUNT.Name = "AMOUNT";
            AMOUNT.HeaderText = "AMOUNT";
            AMOUNT.DataPropertyName = "AMOUNT";
            AMOUNT.Width = 70;
            AMOUNT.Visible = false;

            MasterItemdataGridView.Columns.AddRange(
                                                        new DataGridViewColumn[]{ 
                                                                                    NO, ITEMID, ITEMNAME, ITEMIDQTY, ITEMNAMEQTY,
                                                                                    BRANDID, SPEC, QUANTITY, UOMID, 
                                                                                    RACKID, UNITPRICE, AMOUNT,RETURNQUANTITY
                                                                                }
                                                   );
        }

        private void addColumnsForItemRevision()
        {
            MasterItemdataGridView.Columns.Clear();
            var NO = new DataGridViewTextBoxColumn();
            NO.Name = "NO";
            NO.HeaderText = "NO";
            NO.DataPropertyName = "NO";
            NO.Width = 40;

            var ITEMID = new DataGridViewTextBoxColumn();
            ITEMID.Name = "ITEMID";
            ITEMID.HeaderText = "PART NO";
            ITEMID.DataPropertyName = "ITEMID";
            ITEMID.Width = 140;

            var ITEMNAME = new DataGridViewTextBoxColumn();
            ITEMNAME.Name = "ITEMNAME";
            ITEMNAME.HeaderText = "PART NAME";
            ITEMNAME.DataPropertyName = "ITEMNAME";
            ITEMNAME.Width = 120;

            var ITEMID_ALIAS = new DataGridViewTextBoxColumn();
            ITEMID_ALIAS.Name = "ITEMIDQTY";
            ITEMID_ALIAS.HeaderText = "ALIAS";
            ITEMID_ALIAS.DataPropertyName = "ITEMIDQTY";
            ITEMID_ALIAS.Width = 140;

            var ITEMNAMEQTY = new DataGridViewTextBoxColumn();
            ITEMNAMEQTY.Name = "ITEMNAMEQTY";
            ITEMNAMEQTY.HeaderText = "ITEMNAMEQTY";
            ITEMNAMEQTY.DataPropertyName = "ITEMNAMEQTY";
            ITEMNAMEQTY.Width = 140;
            ITEMNAMEQTY.Visible = false;

            var BRANDID = new DataGridViewTextBoxColumn();
            BRANDID.Name = "BRANDID";
            BRANDID.HeaderText = "BRAND";
            BRANDID.DataPropertyName = "BRANDID";
            BRANDID.Width = 100;

            var SPEC = new DataGridViewTextBoxColumn();
            SPEC.Name = "SPEC";
            SPEC.HeaderText = "SPEC";
            SPEC.DataPropertyName = "SPEC";
            SPEC.Width = 100;

            var QTY = new DataGridViewTextBoxColumn();
            QTY.Name = "QUANTITY";
            QTY.HeaderText = "QTY";
            QTY.DataPropertyName = "QUANTITY";
            QTY.Width = 100;


            var QTYORDER = new DataGridViewTextBoxColumn();
            QTYORDER.Name = "QTYORDER";
            QTYORDER.HeaderText = "QTY ORDER";
            QTYORDER.DataPropertyName = "QTYORDER";
            QTYORDER.Width = 100;

            var PRICE = new DataGridViewTextBoxColumn();
            PRICE.Name = "UNITPRICE";
            PRICE.HeaderText = "UNITPRICE";
            PRICE.DataPropertyName = "UNITPRICE";
            PRICE.Width = 100;
            PRICE.Visible = false;

            var QTYREV = new DataGridViewTextBoxColumn();
            QTYREV.Name = "QTYREVISI";
            QTYREV.HeaderText = "QTYREVISI";
            QTYREV.DataPropertyName = "QTYREVISI";
            QTYREV.Width = 100;
            QTYREV.Visible = false;


            var UOMID = new DataGridViewTextBoxColumn();
            UOMID.Name = "UOMID";
            UOMID.HeaderText = "UOM";
            UOMID.DataPropertyName = "UOMID";
            UOMID.Width = 70;

            var RACKID = new DataGridViewTextBoxColumn();
            RACKID.Name = "RACKID";
            RACKID.HeaderText = "RACK";
            RACKID.DataPropertyName = "RACKID";
            RACKID.Width = 70;

            var AMOUNT = new DataGridViewTextBoxColumn();
            AMOUNT.Name = "AMOUNT";
            AMOUNT.HeaderText = "AMOUNT";
            AMOUNT.DataPropertyName = "AMOUNT";
            AMOUNT.Width = 70;
            AMOUNT.Visible = false;

            MasterItemdataGridView.Columns.AddRange(
                                                        new DataGridViewColumn[]{ 
                                                                                    NO, ITEMID, ITEMNAME, ITEMID_ALIAS, ITEMNAMEQTY,
                                                                                    QTY, QTYORDER,QTYREV, UOMID, PRICE, AMOUNT
                                                                                }
                                                   );
        }

        private void addColumnsForItemBuild()
        {
            MasterItemdataGridView.Columns.Clear();
            var NO = new DataGridViewTextBoxColumn();
            NO.Name = "NO";
            NO.HeaderText = "NO";
            NO.DataPropertyName = "NO";
            NO.Width = 40;

            var ITEMID = new DataGridViewTextBoxColumn();
            ITEMID.Name = "ITEMID";
            ITEMID.HeaderText = "PART NO";
            ITEMID.DataPropertyName = "ITEMID";
            ITEMID.Width = 140;

            var ITEMNAME = new DataGridViewTextBoxColumn();
            ITEMNAME.Name = "ITEMNAME";
            ITEMNAME.HeaderText = "PART NAME";
            ITEMNAME.DataPropertyName = "ITEMNAME";
            ITEMNAME.Width = 120;

            var ITEMID_ALIAS = new DataGridViewTextBoxColumn();
            ITEMID_ALIAS.Name = "ITEMIDQTY";
            ITEMID_ALIAS.HeaderText = "ALIAS";
            ITEMID_ALIAS.DataPropertyName = "ITEMIDQTY";
            ITEMID_ALIAS.Width = 140;

            var ITEMNAME_ALIAS = new DataGridViewTextBoxColumn();
            ITEMNAME_ALIAS.Name = "ITEMNAMEQTY";
            ITEMNAME_ALIAS.HeaderText = "ALIAS NAME";
            ITEMNAME_ALIAS.DataPropertyName = "ITEMNAMEQTY";
            ITEMNAME_ALIAS.Width = 100;
            //ITEMNAME_ALIAS.Visible = false;

            var BRANDID = new DataGridViewTextBoxColumn();
            BRANDID.Name = "BRANDID";
            BRANDID.HeaderText = "BRAND";
            BRANDID.DataPropertyName = "BRANDID";
            BRANDID.Width = 100;

            var SPEC = new DataGridViewTextBoxColumn();
            SPEC.Name = "SPEC";
            SPEC.HeaderText = "SPEC";
            SPEC.DataPropertyName = "SPEC";
            SPEC.Width = 100;

            var QUANTITY = new DataGridViewTextBoxColumn();
            QUANTITY.Name = "QUANTITY";
            QUANTITY.HeaderText = "QUANTITY";
            QUANTITY.DataPropertyName = "QUANTITY";
            QUANTITY.Width = 70;

            var ITEMALIASQUANTITY = new DataGridViewTextBoxColumn();
            ITEMALIASQUANTITY.Name = "ITEMALIASQUANTITY";
            ITEMALIASQUANTITY.HeaderText = "ITEMALIASQUANTITY";
            ITEMALIASQUANTITY.DataPropertyName = "ITEMALIASQUANTITY";
            ITEMALIASQUANTITY.Width = 70;
            ITEMALIASQUANTITY.Visible = false;

            var COMBINEQTY = new DataGridViewTextBoxColumn();
            COMBINEQTY.Name = "COMBINEQTY";
            COMBINEQTY.HeaderText = "COMBINEQTY";
            COMBINEQTY.DataPropertyName = "COMBINEQTY";
            COMBINEQTY.Width = 70;
            COMBINEQTY.Visible = false;

            var UOMID = new DataGridViewTextBoxColumn();
            UOMID.Name = "UOMID";
            UOMID.HeaderText = "UOM";
            UOMID.DataPropertyName = "UOMID";
            UOMID.Width = 70;

            MasterItemdataGridView.Columns.AddRange(
                                                        new DataGridViewColumn[]{ 
                                                                                    NO, ITEMID, ITEMNAME, ITEMID_ALIAS, ITEMNAME_ALIAS,
                                                                                    BRANDID, SPEC, QUANTITY, ITEMALIASQUANTITY, COMBINEQTY, UOMID
                                                                                }
                                                   );
        }

        private void MasterItemdataGridView_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in MasterItemdataGridView.Rows)
                {

                    if (row.Cells["QUANTITY"].Value.ToString().Equals("0"))
                    {
                        DataGridViewCellStyle style = row.DefaultCellStyle;
                        style.ForeColor = Color.Red;
                        row.DefaultCellStyle = style;
                    }
                    else {
                        DataGridViewCellStyle style = row.DefaultCellStyle;
                        style.Font = new Font(MasterItemdataGridView.Font, FontStyle.Bold);
                        row.DefaultCellStyle = style;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            
        }
    }
}
