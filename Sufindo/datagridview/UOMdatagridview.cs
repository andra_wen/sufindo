﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sufindo.Master;

namespace Sufindo
{
    public partial class UOMdatagridview : Form
    {
        public delegate void MasterUOMPassingData(DataTable datatable);

        private delegate void fillDatatableCallBack(DataTable datatable);

        public MasterUOMPassingData masterUOMPassingData;

        private readonly string[] columnMasterBrand = { "UOMID", "UOMNAME"};
        private Connection con;
        private String columnMasterUOMFind = "UOMID";

        private String calledForm;

        public UOMdatagridview(){
            InitializeComponent();
        }

        public UOMdatagridview(String calledForm)
        {
            InitializeComponent();
            
            if (con == null) {
                con = new Connection();
            }
            this.calledForm = calledForm;
            String query =  String.Format("SELECT ROW_NUMBER() OVER (ORDER BY UOMID) AS 'No', UOMID, UOMNAME, INFORMATION, CREATEDBY, UPDATEDDATE " +
                                          "FROM M_UOM " +
                                          "WHERE [STATUS] = '{0}' " +
                                          "ORDER BY UOMID ASC", Status.ACTIVE);
            createBackgroundWorkerFillDatagridView(query);
            
        }

        void createBackgroundWorkerFillDatagridView(String query) {

            for (int i = 0; i < MasterUOMdataGridView.Columns.Count; i++)
            {
                if (MasterUOMdataGridView.Columns[i].DataPropertyName.Equals(columnMasterUOMFind))
                {
                    DataGridViewColumn dataGridViewColumn = MasterUOMdataGridView.Columns[i];
                    dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                    break;
                }
            }
            
            BackgroundWorker bgworker = new BackgroundWorker();
            bgworker.DoWork += new DoWorkEventHandler(bgworker_DoWork);
            bgworker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgworker_RunWorkerCompleted);
            bgworker.RunWorkerAsync(query);
        }

        void bgworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //MessageBox.Show("completed");
            
        }

        void bgworker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                String query = e.Argument as String;

                MasterUOMdataGridView.Invoke(new fillDatatableCallBack(this.fillDatatable), con.openDataTableQuery(query));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void fillDatatable(DataTable datatable){
            MasterUOMdataGridView.DataSource = datatable;
        }

        private void MasterUOMdataGridViewCallPassingData(int currentRow)
        {
            DataTable datatable = new DataTable();
            for (int i = 0; i < MasterUOMdataGridView.Columns.Count; i++)
            {
                datatable.Columns.Add(MasterUOMdataGridView.Columns[i].DataPropertyName);
            }

            DataRow r = datatable.NewRow();
            DataGridViewRow row = MasterUOMdataGridView.Rows[currentRow];
            for (int i = 0; i < MasterUOMdataGridView.Columns.Count; i++)
            {
                r[i] = row.Cells[i].Value;
            }
            datatable.Rows.Add(r);
            masterUOMPassingData(datatable);
            this.Close();
        }

        #region Method MasterUOMdataGridview

        private void MasterUOMdataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (masterUOMPassingData != null)
                {
                    MasterUOMdataGridViewCallPassingData(e.RowIndex);
                }
            }
        }

        private void MasterUOMdataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                foreach (String item in columnMasterBrand)
                {
                    if (MasterUOMdataGridView.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        for (int i = 0; i < MasterUOMdataGridView.Columns.Count; i++)
                        {
                            if (MasterUOMdataGridView.Columns[i].DataPropertyName.Equals(columnMasterUOMFind))
                            {
                                DataGridViewColumn dataGridViewColumn = MasterUOMdataGridView.Columns[i];
                                dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Control;
                            }
                        }
                        break;
                    }
                }


                foreach (String item in columnMasterBrand)
                {
                    if (MasterUOMdataGridView.Columns[e.ColumnIndex].DataPropertyName.Equals(item))
                    {
                        columnMasterUOMFind = MasterUOMdataGridView.Columns[e.ColumnIndex].DataPropertyName;
                        DataGridViewColumn dataGridViewColumn = MasterUOMdataGridView.Columns[e.ColumnIndex];
                        dataGridViewColumn.HeaderCell.Style.BackColor = SystemColors.Info;
                        break;
                    }
                }
            }
        }

        #endregion

        

        #region Method FindTextBox
        private void FindtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter)) {
                
                String strFind = FindtextBox.Text.Contains('*') ? FindtextBox.Text.Replace('*', '%') : (FindtextBox.Text + '%');

                String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY UOMID) AS 'No', UOMID, UOMNAME, " +
                                             "INFORMATION, CREATEDBY, UPDATEDDATE " +
                                             "FROM M_UOM " +
                                             "WHERE {0} LIKE '{1}' AND [STATUS] = '{2}' " +
                                             "ORDER BY UOMID ASC", columnMasterUOMFind, strFind, Status.ACTIVE);
                createBackgroundWorkerFillDatagridView(query);
            }
        }
        #endregion

        private void UOMdatagridview_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
        }

        
        

    }
}
