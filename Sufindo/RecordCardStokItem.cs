﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Sufindo.Purchase;

namespace Sufindo
{
    public partial class RecordCardStokItem : Form
    {
        private delegate void fillAutoCompleteTextBox();

        private delegate void checkTransactionItemCallBack(DataTable datatable);
        
        private delegate void totalQuantityOrder(DataTable datatable);

        private List<Control> controls;
        private List<String> PartNo;
        private Connection connection;

        String ITEMID_ALIAS = "";
        String ITEMID = "";
        Boolean firstActiveForm = true;
        MenuStrip menustripAction;

        private Itemdatagridview itemdatagridview;
        Bitmap defaultImage;

        DataTable datatable = null;

        PurchaseOrderView purchaseorderView;

        Boolean isClosed = false;

        public RecordCardStokItem(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            isClosed = false;
            this.menustripAction = menustripAction;
            controls = new List<Control>();
            connection = new Connection();
            datatable = new DataTable();
            purchaseorderView = null;
            foreach (Control control in this.DatatableLayoutPanel.Controls)
            {
                if (control is TextBox){
                    controls.Add(control);
                }
                else if (control is Panel){
                    foreach (Control controlPanel in control.Controls){
                        if (controlPanel is RichTextBox)
                        {
                            controls.Add(controlPanel);
                        }
                    }
                }
            }

            foreach (Control item in controls){
                if (item is TextBox || item is RichTextBox) {
                    item.BackColor = SystemColors.Info;
                    RichTextBox richtextBox = (item is RichTextBox) ? (RichTextBox)item : null;
                    if (richtextBox != null) richtextBox.ReadOnly = true;
                    item.Enabled = ((item is TextBox) && !item.Name.Equals("PartNotextBox")) ? false : true;
                }
            }

            textBoxIn.Text = "0";
            textBoxOut.Text = "0";
            textBoxSaldo.Text = "0";
            defaultImage = (Bitmap)PartpictureBox.Image;

            FromdateTimePicker.Value = DateTime.Now.AddMonths(-60);
            TodateTimePicker.Value = DateTime.Now;

            //addColumnforDatagridview();
            //AuthorizeUser.sharedInstance.adjustAuthenticationPrice(ref TransaksidataGridView);
        }

        private void getAllPartNoItem() {
            if (PartNo == null) PartNo = new List<string>();
            else PartNo.Clear();

            String query = String.Format("SELECT ITEMID FROM M_ITEM WHERE STATUS = '{0}'", Status.ACTIVE);
            SqlDataReader sqldataReader = connection.sqlDataReaders(query);

            while (sqldataReader.Read())
            {
                PartNo.Add(sqldataReader.GetString(0));
            }
        }

        private void searchPartNobutton_Click(object sender, EventArgs e)
        {
            if (itemdatagridview == null)
            {
                itemdatagridview = new Itemdatagridview("RecordCardStockItem");
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
            else if (itemdatagridview.IsDisposed)
            {
                itemdatagridview = new Itemdatagridview("RecordCardStockItem");
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
        }

        private void MasterItemPassingData(DataTable sender) {
            try
            {
                textBoxUpdate.Text = sender.Rows[0]["UPDATEDDATE"].ToString();
                PartNotextBox.Text = sender.Rows[0]["ITEMID"].ToString();
                ITEMID = PartNotextBox.Text;
                PartNametextBox.Text = sender.Rows[0]["ITEMNAME"].ToString();
                BrandtextBox.Text = sender.Rows[0]["BRANDID"].ToString();
                SpesifikasirichTextBox.Text = sender.Rows[0]["SPEC"].ToString();
                ITEMID_ALIAS = sender.Rows[0]["ITEMID_ALIAS"].ToString();
                SatuantextBox.Text = sender.Rows[0]["UOMID"].ToString();
                RaktextBox.Text = sender.Rows[0]["RACKID"].ToString();
                //Console.WriteLine(sender.Rows[0]["BESTPRICE"].ToString());
                //Double saleprice = Convert.ToDouble(sender.Rows[0]["BESTPRICE"].ToString());
                SellPricetextBox.Text = sender.Rows[0]["BESTPRICE"].ToString();
                
                byte[] byteImage = sender.Rows[0]["ITEMIMAGE"].ToString().Equals("") ? null : (byte[])sender.Rows[0]["ITEMIMAGE"];
                Image img = byteImage == null ? null : Utilities.convertByteToImage(byteImage);
                PartpictureBox.Image = img == null ? defaultImage : img;
                searchAliasPartNobutton.Enabled = true;
                if (this.datatable == null) this.datatable = new DataTable();
                this.datatable.Clear();
                textBoxIn.Text = "0";
                textBoxOut.Text = "0";
                textBoxSaldo.Text = sender.Rows[0]["QUANTITY"].ToString();
                
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("MasterItem reloadAllData : " + ex.Message);
            }
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //MessageBox.Show("Completed");
        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                Console.WriteLine(PartNotextBox.Text);
                Console.WriteLine(FromdateTimePicker.Value.ToString("yyyy-MM-dd"));
                Console.WriteLine(TodateTimePicker.Value.ToString("yyyy-MM-dd"));
                List<SqlParameter> sqlParam = new List<SqlParameter>();
                sqlParam.Add(new SqlParameter("@ITEMID", PartNotextBox.Text));
                sqlParam.Add(new SqlParameter("@FROMDATE", FromdateTimePicker.Value.ToString("yyyy-MM-dd")));
                sqlParam.Add(new SqlParameter("@TODATE", TodateTimePicker.Value.ToString("yyyy-MM-dd")));
                //Console.WriteLine("EXEC SELECT_TRANSACTION_ITEM {0} {1} {2}", PartNotextBox.Text, FromdateTimePicker.Value.ToString("yyyy-MM-dd"), TodateTimePicker.Value.ToString("yyyy-MM-dd"));
                TransaksidataGridView.Invoke(new checkTransactionItemCallBack(this.doCheckTransactionItem), connection.callProcedureDatatable("SELECT_TRANSACTION_ITEM", sqlParam));

                if (!ITEMID.Equals(""))
                {
                    String query = String.Format("SELECT SUM(DP.QUANTITY) AS QUANTITY FROM M_ITEM MI " +
                                                 "JOIN D_PURCHASE_ORDER DP ON MI.ITEMID = DP.ITEMIDQTY " +
                                                 "JOIN H_PURCHASE_ORDER HP ON DP.PURCHASEORDERID = HP.PURCHASEORDERID " +
                                                 "WHERE DP.ITEMIDQTY = '{0}' " +
                                                 "AND HP.STATUS = '{1}'", ITEMID, Status.OPEN);

                    TotalOnOrderlabel.Invoke(new totalQuantityOrder(this.totalQuantityOnOrder), connection.openDataTableQuery(query));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("bgWorker_DoWork RECORDCARDSTOCKITEM ERROR " + ex);
            }
        }

        void totalQuantityOnOrder(DataTable sender) {
            try
            {
                TotalOnOrderlabel.Text = sender.Rows[0]["QUANTITY"].ToString().Equals("") ? "0" : sender.Rows[0]["QUANTITY"].ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine("totalQuantityOnOrder " + ex.Message);
            }
        }

        void doCheckTransactionItem(DataTable sender) {
            this.datatable.Clear();

            //clone datatable     
            DataTable dtCloned = sender.Clone();
            //change data type of column
            dtCloned.Columns["BUYPRICE"].DataType = typeof(String);
            dtCloned.Columns["SELLPRICE"].DataType = typeof(String);
            dtCloned.Columns["RATE"].DataType = typeof(String);
            //import row to cloned datatable
            foreach (DataRow row in sender.Rows)
            {
                dtCloned.ImportRow(row);
            }

            foreach (DataRow row in dtCloned.Rows)
            {
                if (!row["BUYPRICE"].ToString().Equals(String.Empty))
                {
                    row.SetField("BUYPRICE", Double.Parse(row["BUYPRICE"].ToString()).ToString("N2"));
                }
                if (!row["SELLPRICE"].ToString().Equals(String.Empty))
                {
                    row.SetField("SELLPRICE", Double.Parse(row["SELLPRICE"].ToString()).ToString("N2"));
                }
                if (!row["RATE"].ToString().Equals(String.Empty))
                {
                    row.SetField("RATE", Double.Parse(row["RATE"].ToString()).ToString("N2"));
                }
            }

            this.datatable = dtCloned;
            //MessageBox.Show(sender.Rows[0]["IN"].ToString());
            addColumnforDatagridview();
            TransaksidataGridView.DataSource = this.datatable;
            if (TransaksidataGridView.Rows.Count != 0)
            {
                TransaksidataGridView.Rows.Cast<DataGridViewRow>().Sum(t => Convert.ToInt32(t.Cells["IN"].Value));
                textBoxIn.Text = TransaksidataGridView.Rows.Cast<DataGridViewRow>().Sum(t => Convert.ToInt32(t.Cells["IN"].Value)).ToString();
                textBoxOut.Text = TransaksidataGridView.Rows.Cast<DataGridViewRow>().Sum(t => Convert.ToInt32(t.Cells["OUT"].Value)).ToString();
            }

            if(TransaksidataGridView.Rows.Count > 0){
                TransaksidataGridView.FirstDisplayedScrollingRowIndex = TransaksidataGridView.Rows.Count - 1;
            }
        }

        private void searchAliasPartNobutton_Click(object sender, EventArgs e)
        {
            if (itemdatagridview == null)
            {
                itemdatagridview = new Itemdatagridview("MASTERITEMALIAS", ITEMID_ALIAS);
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                //itemdatagridview.MdiParent = this.MdiParent;
                itemdatagridview.ShowDialog();
            }
            else if (itemdatagridview.IsDisposed)
            {
                itemdatagridview = new Itemdatagridview("MASTERITEMALIAS", ITEMID_ALIAS);
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                //itemdatagridview.MdiParent = this.MdiParent;
                itemdatagridview.ShowDialog();
            }
        }

        private void PartNotextBox_Leave(object sender, EventArgs e)
        {
            if (!isClosed)
            {
                Boolean isExistingPartNo = false;
                TotalOnOrderlabel.Text = "0";
                foreach (String item in PartNo)
                {
                    if (item.ToUpper().Equals(PartNotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        PartNotextBox.Text = PartNotextBox.Text.ToUpper();
                        String query = String.Format("SELECT ITEMID, ITEMNAME, " +
                                                     "ITEMID_ALIAS, BRANDID, SPEC, QUANTITY, UOMID, BESTPRICE, RACKID, ITEMIMAGE, CONVERT(VARCHAR(10), UPDATEDDATE, 103) AS UPDATEDDATE " +
                                                     "FROM M_ITEM " +
                                                     "WHERE ITEMID = '{0}'", PartNotextBox.Text);
                        ITEMID = PartNotextBox.Text;
                        DataTable datatable = connection.openDataTableQuery(query);
                        TransaksidataGridView.DataSource = null;
                        MasterItemPassingData(datatable);
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    addColumnforDatagridview();
                    PartNotextBox.Text = "";
                    Utilities.clearAllField(ref controls);
                    PartpictureBox.Image = defaultImage;
                    searchAliasPartNobutton.Enabled = false;
                    ITEMID_ALIAS = "";
                    ITEMID = "";
                    this.datatable.Clear();
                    textBoxIn.Text = "0";
                    textBoxOut.Text = "0";
                    textBoxSaldo.Text = "0";
                    TransaksidataGridView.DataSource = null;
                }
            }
        }

        private void PartNotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                TotalOnOrderlabel.Text = "0";
                foreach (String item in PartNo)
                {
                    if (item.ToUpper().Equals(PartNotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        PartNotextBox.Text = PartNotextBox.Text.ToUpper();
                        String query = String.Format("SELECT ITEMID, ITEMNAME, " +
                                                     "ITEMID_ALIAS, BRANDID, SPEC, QUANTITY, UOMID, BESTPRICE, RACKID, ITEMIMAGE, CONVERT(VARCHAR(10), UPDATEDDATE, 103) AS UPDATEDDATE " +
                                                     "FROM M_ITEM " +
                                                     "WHERE ITEMID = '{0}'", PartNotextBox.Text);
                        ITEMID = PartNotextBox.Text;
                        DataTable datatable = connection.openDataTableQuery(query);
                        TransaksidataGridView.DataSource = null;
                        MasterItemPassingData(datatable);
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    addColumnforDatagridview();
                    PartNotextBox.Text = "";
                    ITEMID = "";
                    Utilities.clearAllField(ref controls);
                    PartpictureBox.Image = defaultImage;
                    searchAliasPartNobutton.Enabled = false;
                    ITEMID_ALIAS = "";
                    this.datatable.Clear();
                    TransaksidataGridView.DataSource = null;
                    textBoxIn.Text = "0";
                    textBoxOut.Text = "0";
                    textBoxSaldo.Text = "0";
                }
            }
        }

        private void OpenOnOrderbutton_Click(object sender, EventArgs e)
        {
            if (!PartNotextBox.Text.Equals(""))
            {
                purchaseorderView = null;
                if (purchaseorderView == null)
                {
                    purchaseorderView = new PurchaseOrderView(PartNotextBox.Text);
                    purchaseorderView.ShowDialog();
                }
                else if (purchaseorderView.IsDisposed)
                {
                    purchaseorderView = new PurchaseOrderView(PartNotextBox.Text);
                    purchaseorderView.ShowDialog();
                }
            }
        }

        private void RecordCardStokItem_Activated(object sender, EventArgs e)
        {
            try
            {
                if (menustripAction != null)
                {
                    AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
                }
                
                Main.ACTIVEFORM = this;

                if (firstActiveForm)
                {
                    firstActiveForm = false;
                    try
                    {
                        BackgroundWorker bgWorker = new BackgroundWorker();
                        bgWorker.DoWork += new DoWorkEventHandler(bgWorkergetAllPartNo_DoWork);
                        bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorkergetAllPartNo_RunWorkerCompleted);
                        bgWorker.RunWorkerAsync();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("RecordCardStokItem_Activated : " + ex.Message);
                    }
                    
                    TransaksidataGridView.DataSource = null;
                    addColumnforDatagridview();
                }
            }
            catch (Exception ex)
            {
                TransaksidataGridView.Rows.Clear();
                PartNotextBox.Text = "";
                Utilities.clearAllField(ref controls);
                PartpictureBox.Image = defaultImage;
                searchAliasPartNobutton.Enabled = false;
                ITEMID_ALIAS = "";
                ITEMID = "";
                this.datatable.Clear();
                textBoxIn.Text = "0";
                textBoxOut.Text = "0";
                textBoxSaldo.Text = "0";
            }
        }

        void bgWorkergetAllPartNo_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (PartNo == null) PartNo = new List<string>();
                else PartNo.Clear();

                String query = String.Format("SELECT ITEMID FROM M_ITEM WHERE STATUS = '{0}'", Status.ACTIVE);
                SqlDataReader sqldataReader = connection.sqlDataReaders(query);

                while (sqldataReader.Read())
                {
                    PartNo.Add(sqldataReader.GetString(0));
                }

                if (IsHandleCreated) BeginInvoke(new fillAutoCompleteTextBox(this.fillAutoComplete));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void fillAutoComplete() {
            PartNotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            PartNotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            PartNotextBox.AutoCompleteCustomSource.Clear();
            PartNotextBox.AutoCompleteCustomSource.AddRange(PartNo.ToArray());
        }

        void bgWorkergetAllPartNo_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //Console.WriteLine("bgWorkergetAllPartNo_RunWorkerCompleted");
        }

        private void ImportExcelbutton_Click(object sender, EventArgs e)
        {

        }

        private void FromdateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            if (FromdateTimePicker.Value.CompareTo(TodateTimePicker.Value) >= 0) {
                TodateTimePicker.Value = FromdateTimePicker.Value.AddDays(1);
            }
        }

        private void TodateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            if (TodateTimePicker.Value.CompareTo(FromdateTimePicker.Value) <= 0) {
                FromdateTimePicker.Value = TodateTimePicker.Value.AddDays(-1);
            }
        }

        private void addColumnforDatagridview() {
            //TransaksidataGridView.DataSource = null;
            TransaksidataGridView.Columns.Clear();
            System.Windows.Forms.DataGridViewTextBoxColumn TRANSACTIONDATE = new DataGridViewTextBoxColumn();
            System.Windows.Forms.DataGridViewTextBoxColumn ITEMIDQTY = new DataGridViewTextBoxColumn();
            System.Windows.Forms.DataGridViewTextBoxColumn TRANSACTIONPERSON = new DataGridViewTextBoxColumn();
            System.Windows.Forms.DataGridViewTextBoxColumn KURS = new DataGridViewTextBoxColumn();
            System.Windows.Forms.DataGridViewTextBoxColumn RATE = new DataGridViewTextBoxColumn();
            System.Windows.Forms.DataGridViewTextBoxColumn PRICE = new DataGridViewTextBoxColumn();
            System.Windows.Forms.DataGridViewTextBoxColumn BUYPRICE = new DataGridViewTextBoxColumn();
            System.Windows.Forms.DataGridViewTextBoxColumn SELLPRICE = new DataGridViewTextBoxColumn();
            System.Windows.Forms.DataGridViewTextBoxColumn IN = new DataGridViewTextBoxColumn();
            System.Windows.Forms.DataGridViewTextBoxColumn OUT = new DataGridViewTextBoxColumn();
            System.Windows.Forms.DataGridViewTextBoxColumn SALDO = new DataGridViewTextBoxColumn();
            System.Windows.Forms.DataGridViewTextBoxColumn INSERTITEMDATE = new DataGridViewTextBoxColumn();

            // 
            // TRANSACTIONDATE
            // 
            TRANSACTIONDATE.DataPropertyName = "TRANSACTIONDATE";
            TRANSACTIONDATE.HeaderText = "DATE";
            TRANSACTIONDATE.MinimumWidth = 70;
            TRANSACTIONDATE.Name = "TRANSACTIONDATE";
            TRANSACTIONDATE.ReadOnly = true;
            TRANSACTIONDATE.SortMode = DataGridViewColumnSortMode.NotSortable;
            // 
            // ITEMIDQTY
            // 
            ITEMIDQTY.DataPropertyName = "ITEMID";
            ITEMIDQTY.FillWeight = 130F;
            ITEMIDQTY.HeaderText = "PART NO";
            ITEMIDQTY.MinimumWidth = 100;
            ITEMIDQTY.Name = "ITEMIDQTY";
            ITEMIDQTY.ReadOnly = true;
            ITEMIDQTY.Width = 130;
            ITEMIDQTY.SortMode = DataGridViewColumnSortMode.NotSortable;
            // 
            // TRANSACTIONPERSON
            // 
           TRANSACTIONPERSON.DataPropertyName = "SUPPLIER/CUSTOMER";
           TRANSACTIONPERSON.FillWeight = 160F;
           TRANSACTIONPERSON.HeaderText = "SUPPLIER/CUSTOMER";
           TRANSACTIONPERSON.MinimumWidth = 125;
           TRANSACTIONPERSON.Name = "TRANSACTIONPERSON";
           TRANSACTIONPERSON.ReadOnly = true;
           TRANSACTIONPERSON.Width = 160;
           TRANSACTIONPERSON.SortMode = DataGridViewColumnSortMode.Automatic;
            // 
            // KURS
            // 
           KURS.DataPropertyName = "KURSID";
           KURS.HeaderText = "KURS";
           KURS.MinimumWidth = 50;
           KURS.Name = "KURS";
           KURS.ReadOnly = true;
           KURS.Width = 50;
           KURS.SortMode = DataGridViewColumnSortMode.NotSortable;
            // 
            // RATE
            // 
           RATE.DataPropertyName = "RATE";
           RATE.HeaderText = "RATE";
           RATE.MinimumWidth = 50;
           RATE.Name = "RATE";
           RATE.ReadOnly = true;
           RATE.SortMode = DataGridViewColumnSortMode.NotSortable;
            // 
            // PRICE
            // 
            PRICE.DataPropertyName = "PRICE";
            PRICE.FillWeight = 120F;
            PRICE.HeaderText = "PRICE";
            PRICE.MinimumWidth = 120;
            PRICE.Name = "PRICE";
            PRICE.ReadOnly = true;
            PRICE.Visible = false;
            PRICE.Width = 100;
            PRICE.SortMode = DataGridViewColumnSortMode.NotSortable;
            PRICE.DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
            PRICE.DefaultCellStyle.Format = "N2";
            // AL
            // SELLPRICE
            // 
            SELLPRICE.DataPropertyName = "SELLPRICE";
            SELLPRICE.HeaderText = "SELL PRICE";
            SELLPRICE.Name = "SELLPRICE";
            SELLPRICE.ReadOnly = true;
            SELLPRICE.Visible = false;
            SELLPRICE.SortMode = DataGridViewColumnSortMode.NotSortable;
            SELLPRICE.DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
            SELLPRICE.DefaultCellStyle.Format = "N2";
            // 
            // BUYPRICE
            // 
            BUYPRICE.DataPropertyName = "BUYPRICE";
            BUYPRICE.HeaderText = "BUY PRICE";
            BUYPRICE.Name = "BUYPRICE";
            BUYPRICE.ReadOnly = true;
            BUYPRICE.Visible = false;
            BUYPRICE.SortMode = DataGridViewColumnSortMode.NotSortable;
            BUYPRICE.DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
            BUYPRICE.DefaultCellStyle.Format = "N2";
            // 
            // IN
            // 
            IN.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            IN.DataPropertyName = "IN";
            IN.FillWeight = 50F;
            IN.HeaderText = "IN";
            IN.MinimumWidth = 50;
            IN.Name = "IN";
            IN.ReadOnly = true;
            IN.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            IN.Width = 50;
            IN.SortMode = DataGridViewColumnSortMode.NotSortable;
            // 
            // OUT
            // 
            OUT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            OUT.DataPropertyName = "OUT";
            OUT.FillWeight = 50F;
            OUT.HeaderText = "OUT";
            OUT.MinimumWidth = 50;
            OUT.Name = "OUT";
            OUT.ReadOnly = true;
            OUT.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            OUT.Width = 50;
            OUT.SortMode = DataGridViewColumnSortMode.NotSortable;
            // 
            // SALDO
            // 
            SALDO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            SALDO.DataPropertyName = "SALDO";
            SALDO.FillWeight = 60F;
            SALDO.HeaderText = "SALDO";
            SALDO.MinimumWidth = 60;
            SALDO.Name = "SALDO";
            SALDO.ReadOnly = true;
            SALDO.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            SALDO.Width = 60;
            SALDO.SortMode = DataGridViewColumnSortMode.NotSortable;
            // 
            // INSERTITEMDATE
            // 
            INSERTITEMDATE.DataPropertyName = "INSERTITEMDATE";
            INSERTITEMDATE.HeaderText = "INSERTITEMDATE";
            INSERTITEMDATE.Name = "INSERTITEMDATE";
            INSERTITEMDATE.ReadOnly = true;
            INSERTITEMDATE.Visible = false;
            INSERTITEMDATE.SortMode = DataGridViewColumnSortMode.NotSortable;

            this.TransaksidataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
                                                            TRANSACTIONDATE,
                                                            ITEMIDQTY,
                                                            TRANSACTIONPERSON,
                                                            KURS,
                                                            RATE,
                                                            PRICE,
                                                            BUYPRICE,
                                                            SELLPRICE,
                                                            IN,
                                                            OUT,
                                                            SALDO,
                                                            INSERTITEMDATE}
                                                        );

            AuthorizeUser.sharedInstance.adjustAuthenticationPrice(ref TransaksidataGridView);
        }

        private void RecordCardStokItem_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                isClosed = true;
                if (connection != null) connection.CloseConnection();
                if (Main.LISTFORM.Count != 0)
                {
                    Form f = Main.LISTFORM.First(s => s.GetType().Equals(this.GetType()));
                    if (f != null)
                    {
                        Main.LISTFORM.Remove(this);
                        if (Main.LISTFORM.Count == 0)
                        {
                            Utilities.showMenuStripAction(menustripAction.Items, false);
                        }
                        Main.ACTIVEFORM = null;
                    }
                }
                this.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

    }
}
