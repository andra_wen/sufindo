﻿namespace Sufindo
{
    partial class ItemBuild
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.AddItembutton = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ItemDowndataGridView = new System.Windows.Forms.DataGridView();
            this.NO2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMID2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMNAME2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMIDQTY2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMNAMEQTY2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SPEC2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COMBINEQTY2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUANTITY2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMID2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BRANDID2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Del2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ActionBuildItembutton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ItemUpdataGridView = new System.Windows.Forms.DataGridView();
            this.NO1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMID1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PARTNAME1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMIDQTY1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMNAMEQTY1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SPEC1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COMBINEQTY1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUANTITY1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMID1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Del1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.BRANDID1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Resultbutton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ItemDowndataGridView)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ItemUpdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.AddItembutton, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.Resultbutton, 0, 4);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1100, 586);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // AddItembutton
            // 
            this.AddItembutton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.AddItembutton.BackColor = System.Drawing.Color.Transparent;
            this.AddItembutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AddItembutton.FlatAppearance.BorderSize = 0;
            this.AddItembutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddItembutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.AddItembutton.ForeColor = System.Drawing.Color.Transparent;
            this.AddItembutton.Image = global::Sufindo.Properties.Resources.plus;
            this.AddItembutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.AddItembutton.Location = new System.Drawing.Point(0, 0);
            this.AddItembutton.Margin = new System.Windows.Forms.Padding(0);
            this.AddItembutton.Name = "AddItembutton";
            this.AddItembutton.Size = new System.Drawing.Size(83, 32);
            this.AddItembutton.TabIndex = 19;
            this.AddItembutton.Text = "Add Item";
            this.AddItembutton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.AddItembutton.UseVisualStyleBackColor = false;
            this.AddItembutton.Click += new System.EventHandler(this.AddItembutton_Click);
            // 
            // panel3
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel3, 2);
            this.panel3.Controls.Add(this.ItemDowndataGridView);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 324);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1094, 259);
            this.panel3.TabIndex = 2;
            // 
            // ItemDowndataGridView
            // 
            this.ItemDowndataGridView.AllowUserToAddRows = false;
            this.ItemDowndataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.ItemDowndataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NO2,
            this.ITEMID2,
            this.ITEMNAME2,
            this.ITEMIDQTY2,
            this.ITEMNAMEQTY2,
            this.SPEC2,
            this.COMBINEQTY2,
            this.QUANTITY2,
            this.UOMID2,
            this.BRANDID2,
            this.Del2});
            this.ItemDowndataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ItemDowndataGridView.Location = new System.Drawing.Point(0, 0);
            this.ItemDowndataGridView.Name = "ItemDowndataGridView";
            this.ItemDowndataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ItemDowndataGridView.Size = new System.Drawing.Size(1094, 259);
            this.ItemDowndataGridView.TabIndex = 0;
            this.ItemDowndataGridView.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.ItemDowndataGridView_CellValidated);
            this.ItemDowndataGridView.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.ItemDowndataGridView_UserDeletedRow);
            this.ItemDowndataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ItemDowndataGridView_CellClick);
            this.ItemDowndataGridView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.ItemDowndataGridView_EditingControlShowing);
            // 
            // NO2
            // 
            this.NO2.DataPropertyName = "NO";
            this.NO2.FillWeight = 30F;
            this.NO2.HeaderText = "NO";
            this.NO2.Name = "NO2";
            this.NO2.ReadOnly = true;
            this.NO2.Width = 30;
            // 
            // ITEMID2
            // 
            this.ITEMID2.DataPropertyName = "ITEMID";
            this.ITEMID2.FillWeight = 120F;
            this.ITEMID2.HeaderText = "PART NO";
            this.ITEMID2.Name = "ITEMID2";
            this.ITEMID2.ReadOnly = true;
            this.ITEMID2.Width = 120;
            // 
            // ITEMNAME2
            // 
            this.ITEMNAME2.DataPropertyName = "ITEMNAME";
            this.ITEMNAME2.FillWeight = 150F;
            this.ITEMNAME2.HeaderText = "PART NAME";
            this.ITEMNAME2.Name = "ITEMNAME2";
            this.ITEMNAME2.ReadOnly = true;
            this.ITEMNAME2.Width = 150;
            // 
            // ITEMIDQTY2
            // 
            this.ITEMIDQTY2.DataPropertyName = "ITEMIDQTY";
            this.ITEMIDQTY2.FillWeight = 130F;
            this.ITEMIDQTY2.HeaderText = "PART NO QTY";
            this.ITEMIDQTY2.Name = "ITEMIDQTY2";
            this.ITEMIDQTY2.ReadOnly = true;
            this.ITEMIDQTY2.Width = 130;
            // 
            // ITEMNAMEQTY2
            // 
            this.ITEMNAMEQTY2.DataPropertyName = "ITEMNAMEQTY";
            this.ITEMNAMEQTY2.FillWeight = 150F;
            this.ITEMNAMEQTY2.HeaderText = "PART NAME QTY";
            this.ITEMNAMEQTY2.Name = "ITEMNAMEQTY2";
            this.ITEMNAMEQTY2.ReadOnly = true;
            this.ITEMNAMEQTY2.Width = 150;
            // 
            // SPEC2
            // 
            this.SPEC2.DataPropertyName = "SPEC";
            this.SPEC2.FillWeight = 150F;
            this.SPEC2.HeaderText = "SPEC";
            this.SPEC2.Name = "SPEC2";
            this.SPEC2.ReadOnly = true;
            this.SPEC2.Width = 150;
            // 
            // COMBINEQTY2
            // 
            this.COMBINEQTY2.DataPropertyName = "COMBINEQTY";
            this.COMBINEQTY2.FillWeight = 120F;
            this.COMBINEQTY2.HeaderText = "COMBINE QTY";
            this.COMBINEQTY2.Name = "COMBINEQTY2";
            this.COMBINEQTY2.Width = 120;
            // 
            // QUANTITY2
            // 
            this.QUANTITY2.DataPropertyName = "ITEMALIASQUANTITY";
            this.QUANTITY2.FillWeight = 80F;
            this.QUANTITY2.HeaderText = "QUANTITY";
            this.QUANTITY2.Name = "QUANTITY2";
            this.QUANTITY2.ReadOnly = true;
            this.QUANTITY2.Width = 80;
            // 
            // UOMID2
            // 
            this.UOMID2.DataPropertyName = "UOMID";
            this.UOMID2.FillWeight = 80F;
            this.UOMID2.HeaderText = "UOM";
            this.UOMID2.Name = "UOMID2";
            this.UOMID2.ReadOnly = true;
            this.UOMID2.Width = 80;
            // 
            // BRANDID2
            // 
            this.BRANDID2.DataPropertyName = "BRANDID";
            this.BRANDID2.HeaderText = "BRAND";
            this.BRANDID2.Name = "BRANDID2";
            this.BRANDID2.ReadOnly = true;
            this.BRANDID2.Visible = false;
            // 
            // Del2
            // 
            this.Del2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Del2.DataPropertyName = "Del";
            this.Del2.FillWeight = 24F;
            this.Del2.HeaderText = "";
            this.Del2.Image = global::Sufindo.Properties.Resources.minus;
            this.Del2.MinimumWidth = 24;
            this.Del2.Name = "Del2";
            this.Del2.ReadOnly = true;
            this.Del2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Del2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Del2.Width = 24;
            // 
            // panel2
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel2, 2);
            this.panel2.Controls.Add(this.ActionBuildItembutton);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 241);
            this.panel2.Name = "panel2";
            this.tableLayoutPanel1.SetRowSpan(this.panel2, 2);
            this.panel2.Size = new System.Drawing.Size(1094, 45);
            this.panel2.TabIndex = 1;
            // 
            // ActionBuildItembutton
            // 
            this.ActionBuildItembutton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ActionBuildItembutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.ActionBuildItembutton.Location = new System.Drawing.Point(501, 8);
            this.ActionBuildItembutton.Name = "ActionBuildItembutton";
            this.ActionBuildItembutton.Size = new System.Drawing.Size(92, 28);
            this.ActionBuildItembutton.TabIndex = 0;
            this.ActionBuildItembutton.UseVisualStyleBackColor = true;
            this.ActionBuildItembutton.Click += new System.EventHandler(this.ActionBuildItembutton_Click);
            // 
            // panel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel1, 2);
            this.panel1.Controls.Add(this.ItemUpdataGridView);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 35);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1094, 200);
            this.panel1.TabIndex = 0;
            // 
            // ItemUpdataGridView
            // 
            this.ItemUpdataGridView.AllowUserToAddRows = false;
            this.ItemUpdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.ItemUpdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NO1,
            this.ITEMID1,
            this.PARTNAME1,
            this.ITEMIDQTY1,
            this.ITEMNAMEQTY1,
            this.SPEC1,
            this.COMBINEQTY1,
            this.QUANTITY1,
            this.UOMID1,
            this.Del1,
            this.BRANDID1});
            this.ItemUpdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ItemUpdataGridView.Location = new System.Drawing.Point(0, 0);
            this.ItemUpdataGridView.Name = "ItemUpdataGridView";
            this.ItemUpdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ItemUpdataGridView.Size = new System.Drawing.Size(1094, 200);
            this.ItemUpdataGridView.TabIndex = 0;
            this.ItemUpdataGridView.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.ItemUpdataGridView_CellValidated);
            this.ItemUpdataGridView.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.ItemUpdataGridView_UserDeletedRow);
            this.ItemUpdataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ItemUpdataGridView_CellClick);
            this.ItemUpdataGridView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.ItemUpdataGridView_EditingControlShowing);
            // 
            // NO1
            // 
            this.NO1.DataPropertyName = "NO";
            this.NO1.FillWeight = 30F;
            this.NO1.HeaderText = "NO";
            this.NO1.Name = "NO1";
            this.NO1.ReadOnly = true;
            this.NO1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.NO1.Width = 30;
            // 
            // ITEMID1
            // 
            this.ITEMID1.DataPropertyName = "ITEMID";
            this.ITEMID1.FillWeight = 120F;
            this.ITEMID1.HeaderText = "PART NO";
            this.ITEMID1.Name = "ITEMID1";
            this.ITEMID1.ReadOnly = true;
            this.ITEMID1.Width = 120;
            // 
            // PARTNAME1
            // 
            this.PARTNAME1.DataPropertyName = "ITEMNAME";
            this.PARTNAME1.FillWeight = 150F;
            this.PARTNAME1.HeaderText = "PART NAME";
            this.PARTNAME1.Name = "PARTNAME1";
            this.PARTNAME1.ReadOnly = true;
            this.PARTNAME1.Width = 150;
            // 
            // ITEMIDQTY1
            // 
            this.ITEMIDQTY1.DataPropertyName = "ITEMIDQTY";
            this.ITEMIDQTY1.FillWeight = 130F;
            this.ITEMIDQTY1.HeaderText = "PART NO QTY";
            this.ITEMIDQTY1.Name = "ITEMIDQTY1";
            this.ITEMIDQTY1.ReadOnly = true;
            this.ITEMIDQTY1.Width = 130;
            // 
            // ITEMNAMEQTY1
            // 
            this.ITEMNAMEQTY1.DataPropertyName = "ITEMNAMEQTY";
            this.ITEMNAMEQTY1.FillWeight = 150F;
            this.ITEMNAMEQTY1.HeaderText = "PART NAME QTY";
            this.ITEMNAMEQTY1.Name = "ITEMNAMEQTY1";
            this.ITEMNAMEQTY1.ReadOnly = true;
            this.ITEMNAMEQTY1.Width = 150;
            // 
            // SPEC1
            // 
            this.SPEC1.DataPropertyName = "SPEC";
            this.SPEC1.FillWeight = 150F;
            this.SPEC1.HeaderText = "SPEC";
            this.SPEC1.Name = "SPEC1";
            this.SPEC1.ReadOnly = true;
            this.SPEC1.Width = 150;
            // 
            // COMBINEQTY1
            // 
            this.COMBINEQTY1.DataPropertyName = "COMBINEQTY";
            this.COMBINEQTY1.FillWeight = 120F;
            this.COMBINEQTY1.HeaderText = "RESULT";
            this.COMBINEQTY1.Name = "COMBINEQTY1";
            this.COMBINEQTY1.Width = 120;
            // 
            // QUANTITY1
            // 
            this.QUANTITY1.DataPropertyName = "ITEMALIASQUANTITY";
            this.QUANTITY1.FillWeight = 80F;
            this.QUANTITY1.HeaderText = "QUANTITY";
            this.QUANTITY1.Name = "QUANTITY1";
            this.QUANTITY1.ReadOnly = true;
            this.QUANTITY1.Width = 80;
            // 
            // UOMID1
            // 
            this.UOMID1.DataPropertyName = "UOMID";
            this.UOMID1.FillWeight = 80F;
            this.UOMID1.HeaderText = "UOM";
            this.UOMID1.MinimumWidth = 80;
            this.UOMID1.Name = "UOMID1";
            this.UOMID1.ReadOnly = true;
            this.UOMID1.Width = 80;
            // 
            // Del1
            // 
            this.Del1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Del1.DataPropertyName = "Del";
            this.Del1.FillWeight = 24F;
            this.Del1.HeaderText = " ";
            this.Del1.Image = global::Sufindo.Properties.Resources.minus;
            this.Del1.MinimumWidth = 24;
            this.Del1.Name = "Del1";
            this.Del1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Del1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Del1.Width = 24;
            // 
            // BRANDID1
            // 
            this.BRANDID1.DataPropertyName = "BRANDID";
            this.BRANDID1.HeaderText = "BRAND";
            this.BRANDID1.Name = "BRANDID1";
            this.BRANDID1.ReadOnly = true;
            this.BRANDID1.Visible = false;
            // 
            // Resultbutton
            // 
            this.Resultbutton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Resultbutton.BackColor = System.Drawing.Color.Transparent;
            this.Resultbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Resultbutton.FlatAppearance.BorderSize = 0;
            this.Resultbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Resultbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Resultbutton.ForeColor = System.Drawing.Color.Transparent;
            this.Resultbutton.Image = global::Sufindo.Properties.Resources.plus;
            this.Resultbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Resultbutton.Location = new System.Drawing.Point(0, 289);
            this.Resultbutton.Margin = new System.Windows.Forms.Padding(0);
            this.Resultbutton.Name = "Resultbutton";
            this.Resultbutton.Size = new System.Drawing.Size(83, 32);
            this.Resultbutton.TabIndex = 20;
            this.Resultbutton.Text = "Add Item";
            this.Resultbutton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Resultbutton.UseVisualStyleBackColor = false;
            this.Resultbutton.Click += new System.EventHandler(this.Resultbutton_Click);
            // 
            // ItemBuild
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1103, 588);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ItemBuild";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Build In";
            this.Activated += new System.EventHandler(this.ItemBuild_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ItemBuild_FormClosed);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ItemDowndataGridView)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ItemUpdataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView ItemUpdataGridView;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button AddItembutton;
        private System.Windows.Forms.Button Resultbutton;
        private System.Windows.Forms.Button ActionBuildItembutton;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView ItemDowndataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMID2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMNAME2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMIDQTY2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMNAMEQTY2;
        private System.Windows.Forms.DataGridViewTextBoxColumn SPEC2;
        private System.Windows.Forms.DataGridViewTextBoxColumn COMBINEQTY2;
        private System.Windows.Forms.DataGridViewTextBoxColumn QUANTITY2;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMID2;
        private System.Windows.Forms.DataGridViewTextBoxColumn BRANDID2;
        private System.Windows.Forms.DataGridViewImageColumn Del2;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMID1;
        private System.Windows.Forms.DataGridViewTextBoxColumn PARTNAME1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMIDQTY1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMNAMEQTY1;
        private System.Windows.Forms.DataGridViewTextBoxColumn SPEC1;
        private System.Windows.Forms.DataGridViewTextBoxColumn COMBINEQTY1;
        private System.Windows.Forms.DataGridViewTextBoxColumn QUANTITY1;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMID1;
        private System.Windows.Forms.DataGridViewImageColumn Del1;
        private System.Windows.Forms.DataGridViewTextBoxColumn BRANDID1;
    }
}