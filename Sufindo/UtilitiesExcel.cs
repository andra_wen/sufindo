﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data;
using Microsoft.Office.Interop.Excel;
using System.Windows.Forms;
using System.IO;

namespace Sufindo
{
    class UtilitiesExcel
    {
        Configurations config = null;
        private String PATH = "";
        private String connectionString = "";
        OleDbConnection dbconnection = null;
        OleDbCommand dbcmd = null;
        OleDbDataReader dbDataReader = null;
        OleDbDataAdapter dbDataAdapter = null;
        SqlBulkCopy bulkCopy = null;
        public UtilitiesExcel(String PATH) {
            try
            {
                this.PATH = PATH;
                connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + this.PATH + ";Extended Properties=Excel 12.0;";  
                //connectionString = @"provider=Microsoft.JET.OLEDB.4.0;Data Source=" + this.PATH + ";Extended Properties=" + "\"Excel 8.0;Hdr=Yes;\"";
                dbconnection = new OleDbConnection(connectionString);
                dbconnection.Close();

                config = new Configurations();
            }
            catch (Exception ex)
            {
                
            }
        }

        public void ImportExcelToSQL(String sheet,String TABLENAME) {
            try{
                String []column = config.getColumnExcel(sheet);

                String query = String.Format("SELECT {0} FROM [{1}$]", String.Join(",",column), sheet);
                //MessageBox.Show(query);
                //System.Windows.Forms.MessageBox.Show(query);
                dbcmd = new OleDbCommand(query, dbconnection);
                if (dbconnection.State == System.Data.ConnectionState.Closed){
                    dbconnection.Open();
                }
                System.Data.DataTable dt = new System.Data.DataTable();
                dbDataAdapter = new OleDbDataAdapter(dbcmd);
                dbDataAdapter.Fill(dt);
                Console.WriteLine(dt.Columns[3].ColumnName);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Console.WriteLine(dt.Rows[i][3].ToString());
                }
                //dbDataReader = dbcmd.ExecuteReader();
                bulkCopy = new SqlBulkCopy(config.connectionStrings);
                //for (int i = 0; i < column.Length; i++)
                //{
                //    bulkCopy.ColumnMappings.Add(column[i], column[i]);
                //}
                bulkCopy.DestinationTableName = TABLENAME;
                bulkCopy.NotifyAfter = 1;
                bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(bulkCopy_SqlRowsCopied);
                //foreach (DataRow row in dt.Rows)
                //{
                //   bulkCopy.WriteToServer(
                //}
                bulkCopy.WriteToServer(dt);
                System.Windows.Forms.MessageBox.Show(String.Format("SUCCESS TO IMPORT EXCEL IN SHEET {0} TO SQL SERVER ON TABLE {0}",sheet));
                dbconnection.Close();
            }
            catch (Exception ex){
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        void bulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            //Console.WriteLine(e.RowsCopied);
        }

        public static void ImportDataTableToExcel(System.Data.DataTable dt) {
            _Application appExcel = new Microsoft.Office.Interop.Excel.Application();

            _Workbook wbExcel = appExcel.Workbooks.Add(Type.Missing);

            _Worksheet wsExcel = null;

            appExcel.Visible = false;

            wsExcel = (_Worksheet)wbExcel.Sheets["sheet1"];
            wsExcel = (_Worksheet)wbExcel.ActiveSheet;

            wsExcel.Name = "Export From PO";

            // storing header part in Excel
            int columnSheet = 1;
            
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                wsExcel.Cells[1, columnSheet] = dt.Columns[i].ColumnName;
                columnSheet++;
            }

            // storing Each row and column value to excel sheet
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    wsExcel.Cells[i + 2, j + 1] = dt.Rows[i][j].ToString();
                }
            }
            //for (int i = 0; i < dt.Rows.Count - 1; i++)
            //{
            //    for (int j = 0; j < dt.Columns.Count; j++)
            //    {
            //        wsExcel.Cells[i + 2, j + 1] = dt.Rows[i].Cells[j].Value.ToString();
            //    }
            //}

            // save the application
            String formatSave = DateTime.Now.ToString("YYYYMMDD-HHmmss");
            wbExcel.SaveAs(@"D:\" + formatSave + ".xlsx", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            //wbExcel.SaveAs("c:\\output.xls", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            appExcel.Quit();
        }
        
        public static void ImportDataToExcel(DataGridView dt) {
            _Application appExcel = new Microsoft.Office.Interop.Excel.Application();

            _Workbook wbExcel = appExcel.Workbooks.Add(Type.Missing);

            _Worksheet wsExcel = null;

            appExcel.Visible = false;

            wsExcel = (_Worksheet)wbExcel.Sheets["sheet1"];
            wsExcel = (_Worksheet)wbExcel.ActiveSheet;

            wsExcel.Name = "Export From PO";

            // storing header part in Excel
            int columnSheet = 1;
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                if (!dt.Columns[i].HeaderText.Equals(""))
                {
                    wsExcel.Cells[1, columnSheet] = dt.Columns[i].HeaderText;
                    columnSheet++;
                }
            }

            // storing Each row and column value to excel sheet

            //for (int i = 0; i < dt.Rows.Count - 1; i++)
            //{
            //    for (int j = 0; j < dt.Columns.Count; j++)
            //    {
            //        wsExcel.Cells[i + 2, j + 1] = dt.Rows[i].Cells[j].Value.ToString();
            //    }
            //}

            // save the application
            wbExcel.SaveAs(@"D:\output.xlsx", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            //wbExcel.SaveAs("c:\\output.xls", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            appExcel.Quit();
        }

        public static void ImportHeaderDetailToExcel(System.Data.DataTable header, System.Data.DataTable detail, String filename) {
            _Application appExcel = new Microsoft.Office.Interop.Excel.Application();

            _Workbook wbExcel = appExcel.Workbooks.Add(Type.Missing);

            _Worksheet wsExcel = null;

            appExcel.Visible = false;

            wsExcel = (_Worksheet)wbExcel.Sheets["sheet1"];
            wsExcel = (_Worksheet)wbExcel.ActiveSheet;

            wsExcel.Name = "Export From PO";

            //for header
            for (int i = 0; i < header.Columns.Count; i++)
            {
                wsExcel.Cells[i + 1, 1] = header.Columns[i].ColumnName;
            }

            // storing Each row and column value to excel sheet
            for (int i = 0; i < header.Columns.Count; i++)
            {
                for (int j = 0; j < header.Rows.Count; j++)
                {
                    wsExcel.Cells[i + 1, j + 2] = header.Rows[j][i].ToString();
                }
            }

            for (int i = 0; i < detail.Columns.Count; i++)
            {
                wsExcel.Cells[header.Columns.Count, i + 1] = detail.Columns[i].ColumnName;
            }

            for (int i = 0; i < detail.Rows.Count; i++)
            {
                for (int j = 0; j < detail.Columns.Count; j++)
                {
                    wsExcel.Cells[i + ( header.Columns.Count + 1 ), j + 1] = detail.Rows[i][j].ToString();
                }
            }

            // save the application
            String formatSave = DateTime.Now.ToString("yyyyMMdd-HHmmss");
            //String path = String.Format(@"{0}\{1}.xlsx", System.Windows.Forms.Application.StartupPath, formatSave);
            String path = String.Format(@"{0}", filename);
           
            wbExcel.SaveAs(path, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            //wbExcel.SaveAs("c:\\output.xls", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            appExcel.Quit();
        }

        public static String browseExcel()
        {
            String fileNamePath = "";
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Excel Files(*.xls; *.xlsx)|*.xls; *.xlsx";

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    fileNamePath = openFileDialog.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return fileNamePath;
        }
    }
}
