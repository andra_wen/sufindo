﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Sufindo.Common;

namespace Sufindo
{
    public partial class ImportExportForm : Form
    {
        MenuStrip menustripAction;
        public delegate void runningprogressbardelegate(int val);
        public ImportExportForm()
        {
            InitializeComponent();
            XMLHelper xmlhelper = new XMLHelper();
            DataTable dt = xmlhelper.DisplayValueEntityExcel();
            EntityImportcomboBox.AutoCompleteSource = AutoCompleteSource.ListItems;
            EntityImportcomboBox.AutoCompleteMode = AutoCompleteMode.Suggest;
            EntityImportcomboBox.DataSource = dt;
            EntityImportcomboBox.DisplayMember = "Displaymember";
            EntityImportcomboBox.ValueMember = "Valuemember";

            EntityExportcomboBox.AutoCompleteSource = AutoCompleteSource.ListItems;
            EntityExportcomboBox.AutoCompleteMode = AutoCompleteMode.Suggest;
            EntityExportcomboBox.DataSource = dt;
            EntityExportcomboBox.DisplayMember = "Displaymember";
            EntityExportcomboBox.ValueMember = "Valuemember";
        }

        public ImportExportForm(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            this.menustripAction = menustripAction;
            XMLHelper xmlhelper = new XMLHelper();
            DataTable dt = xmlhelper.DisplayValueEntityExcel();
            EntityImportcomboBox.AutoCompleteSource = AutoCompleteSource.ListItems;
            EntityImportcomboBox.AutoCompleteMode = AutoCompleteMode.Suggest;
            EntityImportcomboBox.DataSource = dt;
            EntityImportcomboBox.DisplayMember = "Displaymember";
            EntityImportcomboBox.ValueMember = "Valuemember";

            EntityExportcomboBox.AutoCompleteSource = AutoCompleteSource.ListItems;
            EntityExportcomboBox.AutoCompleteMode = AutoCompleteMode.Suggest;
            EntityExportcomboBox.DataSource = dt;
            EntityExportcomboBox.DisplayMember = "Displaymember";
            EntityExportcomboBox.ValueMember = "Valuemember";
        }

        private void BrowseImport_Click(object sender, EventArgs e)
        {
            locationImptextBox.Text = openfileExcel();
        }

        private String openfileExcel() {
            String filepath = "";
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Excel Files(*.xls; *.xlsx)|*.xls; *.xlsx";

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    filepath = openFileDialog.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return filepath;
        }

        public void import() {
            try
            {
                if (File.Exists(locationImptextBox.Text)) {
                    Importinglabel.Text = "Please Wait ... ";
                    importprogressBar.Value = 0;
                    String argument = EntityImportcomboBox.SelectedValue.ToString();
                    BackgroundWorker bgWorker = new BackgroundWorker();
                    bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWorkImport);
                    bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompletedImport);
                    bgWorker.RunWorkerAsync(argument);
                }
                else {
                    MessageBox.Show("File Not Found on this path " + locationImptextBox.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("failure import : " + ex.Message);
            }
        }

        public void export() {
            try
            {
                if (!locationExporttextBox.Text.Equals(""))
                {
                    exportingprogressBar.Value = 0;
                    String argument = EntityExportcomboBox.SelectedValue.ToString();
                    BackgroundWorker bgWorker = new BackgroundWorker();
                    bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWorkExport);
                    bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompletedExport);
                    bgWorker.RunWorkerAsync(argument);
                }
                else
                {
                    MessageBox.Show("File Not Found on this path " + locationImptextBox.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("failure import : " + ex.Message);
            }
        }

        void bgWorker_RunWorkerCompletedExport(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show("Exporting Success");
            Exportinglabel.Text = "";
        }

        void bgWorker_DoWorkExport(object sender, DoWorkEventArgs e)
        {
            ExcelHelper excelHelper = new ExcelHelper(locationExporttextBox.Text);
            excelHelper.excelHelperRunningProgressBar = new ExcelHelper.ExcelHelperRunningProgressBar(ExcelHelperRunningProgressBar);
            excelHelper.exportToExcel(e.Argument.ToString());
        }

        void bgWorker_RunWorkerCompletedImport(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show("Importing Success");
            Importinglabel.Text = "";
        }

        void bgWorker_DoWorkImport(object sender, DoWorkEventArgs e)
        {
            ExcelHelper excelhelper = new ExcelHelper(locationImptextBox.Text);
            excelhelper.excelHelperRunningProgressBar = new ExcelHelper.ExcelHelperRunningProgressBar(ExcelHelperRunningProgressBar);
            excelhelper.importToSql(e.Argument.ToString());
        }

        private void ExcelHelperRunningProgressBar(long val, long total, String type) {
            if (type.Equals("Exporting")) {
                double progress = Math.Floor(((double)val / (double)total) * 100);
                exportingprogressBar.Invoke(new runningprogressbardelegate(runningprogressbarexport), (int)progress);
            }
            else if (type.Equals("Importing")) {
                double progress = Math.Floor(((double)val / (double)total) * 100);
                importprogressBar.Invoke(new runningprogressbardelegate(runningprogressbarimport), (int)progress);
            }
        }

        private void runningprogressbarimport(int progress){
            importprogressBar.Value = progress;
            Importinglabel.Text = String.Format("Importing {0}%", progress);
        }

        private void runningprogressbarexport(int progress)
        {
            exportingprogressBar.Value = progress;
            Exportinglabel.Text = String.Format("Exporting {0}%", progress);
        }

        private void browseExportbutton_Click(object sender, EventArgs e)
        {
            locationExporttextBox.Text = savefileExcel();
        }

        private String savefileExcel()
        {
            String filepath = "";
            try
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Excel Files(*.xls; *.xlsx)|*.xls; *.xlsx";

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    filepath = saveFileDialog.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return filepath;
        }

        private void ImportExportForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }

        private void ImportExportForm_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            Main.ACTIVEFORM = this;
        }

    }
}