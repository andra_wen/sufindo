﻿namespace Sufindo.Setting
{
    partial class AuthenticationPrice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.UserdataGridView = new System.Windows.Forms.DataGridView();
            this.EMPLOYEEID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMPLOYEENAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POSITION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PricedataGridView = new System.Windows.Forms.DataGridView();
            this.AuthPriceID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AuthPriceName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VISIBLE = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.UserdataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PricedataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // UserdataGridView
            // 
            this.UserdataGridView.AllowUserToAddRows = false;
            this.UserdataGridView.AllowUserToDeleteRows = false;
            this.UserdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.UserdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EMPLOYEEID,
            this.EMPLOYEENAME,
            this.POSITION});
            this.UserdataGridView.Location = new System.Drawing.Point(3, 3);
            this.UserdataGridView.Name = "UserdataGridView";
            this.UserdataGridView.ReadOnly = true;
            this.UserdataGridView.RowHeadersVisible = false;
            this.UserdataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.UserdataGridView.Size = new System.Drawing.Size(436, 179);
            this.UserdataGridView.TabIndex = 0;
            this.UserdataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.UserdataGridView_CellClick);
            // 
            // EMPLOYEEID
            // 
            this.EMPLOYEEID.DataPropertyName = "EMPLOYEEID";
            this.EMPLOYEEID.FillWeight = 120F;
            this.EMPLOYEEID.HeaderText = "EMPLOYEE ID";
            this.EMPLOYEEID.Name = "EMPLOYEEID";
            this.EMPLOYEEID.ReadOnly = true;
            this.EMPLOYEEID.Width = 104;
            // 
            // EMPLOYEENAME
            // 
            this.EMPLOYEENAME.DataPropertyName = "EMPLOYEENAME";
            this.EMPLOYEENAME.FillWeight = 140F;
            this.EMPLOYEENAME.HeaderText = "EMPLOYEE NAME";
            this.EMPLOYEENAME.Name = "EMPLOYEENAME";
            this.EMPLOYEENAME.ReadOnly = true;
            this.EMPLOYEENAME.Width = 124;
            // 
            // POSITION
            // 
            this.POSITION.DataPropertyName = "POSITION";
            this.POSITION.HeaderText = "POSITION";
            this.POSITION.Name = "POSITION";
            this.POSITION.ReadOnly = true;
            this.POSITION.Width = 83;
            // 
            // PricedataGridView
            // 
            this.PricedataGridView.AllowUserToAddRows = false;
            this.PricedataGridView.AllowUserToDeleteRows = false;
            this.PricedataGridView.AllowUserToResizeColumns = false;
            this.PricedataGridView.AllowUserToResizeRows = false;
            this.PricedataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PricedataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AuthPriceID,
            this.AuthPriceName,
            this.VISIBLE});
            this.PricedataGridView.Location = new System.Drawing.Point(445, 3);
            this.PricedataGridView.MultiSelect = false;
            this.PricedataGridView.Name = "PricedataGridView";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PricedataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.PricedataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.PricedataGridView.Size = new System.Drawing.Size(278, 179);
            this.PricedataGridView.TabIndex = 1;
            this.PricedataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.PricedataGridView_CellContentClick);
            // 
            // AuthPriceID
            // 
            this.AuthPriceID.DataPropertyName = "AuthPriceID";
            this.AuthPriceID.HeaderText = "AuthPriceID";
            this.AuthPriceID.Name = "AuthPriceID";
            this.AuthPriceID.Visible = false;
            this.AuthPriceID.Width = 89;
            // 
            // AuthPriceName
            // 
            this.AuthPriceName.DataPropertyName = "AuthPriceName";
            this.AuthPriceName.HeaderText = "PRICE TYPE";
            this.AuthPriceName.Name = "AuthPriceName";
            this.AuthPriceName.Width = 95;
            // 
            // VISIBLE
            // 
            this.VISIBLE.DataPropertyName = "VISIBLE";
            this.VISIBLE.HeaderText = "VISIBLE";
            this.VISIBLE.Name = "VISIBLE";
            this.VISIBLE.Width = 53;
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(3, 188);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(720, 23);
            this.progressBar.Step = 1;
            this.progressBar.TabIndex = 2;
            // 
            // AuthenticationPrice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(728, 218);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.PricedataGridView);
            this.Controls.Add(this.UserdataGridView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AuthenticationPrice";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Authentication Price";
            this.Activated += new System.EventHandler(this.AuthenticationPrice_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AuthenticationPrice_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.UserdataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PricedataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView UserdataGridView;
        private System.Windows.Forms.DataGridView PricedataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMPLOYEEID;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMPLOYEENAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn POSITION;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.DataGridViewTextBoxColumn AuthPriceID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AuthPriceName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn VISIBLE;
    }
}