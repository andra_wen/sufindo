﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Sufindo.Setting
{
    public partial class AuthenticationUser : Form
    {
        Connection conn = null;

        MenuStrip menustripAction;

        private delegate void fillDatatableCallBack(DataTable datatable);

        public AuthenticationUser()
        {
            InitializeComponent();
            conn = new Connection();
            BackgroundWorker bgWorker = new BackgroundWorker();
            bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
            bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
            bgWorker.RunWorkerAsync();
        }

        public AuthenticationUser(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            this.menustripAction = menustripAction;
            conn = new Connection();
            BackgroundWorker bgWorker = new BackgroundWorker();
            bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
            bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
            bgWorker.RunWorkerAsync();
        }

        private void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            
        }

        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                /*
                    0 -> tidak ada  ( textbox - )
                    1 -> ada        ( checkbox  )
                 */
                String query = String.Format("SELECT MENUID, SUBMENUID, SubMenuName AS MENUNAME, NEW, [SAVE], EDIT, FIND, [DELETE], [PRINT], [CLOSE]," +
                                             "[IMPORT], [EXPORT], [CONFIRM], [CANCEL], [VISIBLE] " +
                                             "FROM D_MENU " +
                                             "ORDER BY MENUID, SUBMENUID");
                
                MenudataGridView.Invoke(new fillDatatableCallBack(this.fillDatatable), conn.openDataTableQuery(query));

                query = String.Format("SELECT EMPLOYEEID, EMPLOYEENAME, POSITION FROM M_EMPLOYEE WHERE STATUS = '{0}'", Status.ACTIVE);

                UserdataGridView.Invoke(new fillDatatableCallBack(this.fillDatatableUser), conn.openDataTableQuery(query));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void fillDatatable(DataTable datatable)
        {
            int no = 1;
            for (int i = 0; i < datatable.Rows.Count; i++)
            {
                MenudataGridView.Rows.Add();
                if (datatable.Rows[i]["SUBMENUID"].ToString().Equals("001"))
                {
                    MenudataGridView.Rows[i].HeaderCell.Value = no.ToString();
                    //MenudataGridView.Rows[i].HeaderCell.Style.BackColor = SystemColors.Info;
                    //MenudataGridView.CurrentRow.DefaultCellStyle.BackColor = SystemColors.Info;
                    no++;
                }

                MenudataGridView.Rows[i].Cells[0].Value = datatable.Rows[i]["MENUID"].ToString();
                MenudataGridView.Rows[i].Cells[0].ReadOnly = true;
                MenudataGridView.Rows[i].Cells[1].Value = datatable.Rows[i]["SUBMENUID"].ToString();
                MenudataGridView.Rows[i].Cells[1].ReadOnly = true;
                MenudataGridView.Rows[i].Cells[2].Value = datatable.Rows[i]["MENUNAME"].ToString();
                MenudataGridView.Rows[i].Cells[2].ReadOnly = true;

                MenudataGridView.Rows[i].Cells[3].ReadOnly = datatable.Rows[i]["NEW"].ToString().Equals("1") ? false : true;
                MenudataGridView.Rows[i].Cells[4].ReadOnly = datatable.Rows[i]["SAVE"].ToString().Equals("1") ? false : true;
                MenudataGridView.Rows[i].Cells[5].ReadOnly = datatable.Rows[i]["EDIT"].ToString().Equals("1") ? false : true;
                MenudataGridView.Rows[i].Cells[6].ReadOnly = datatable.Rows[i]["FIND"].ToString().Equals("1") ? false : true;
                MenudataGridView.Rows[i].Cells[7].ReadOnly = datatable.Rows[i]["DELETE"].ToString().Equals("1") ? false : true;
                MenudataGridView.Rows[i].Cells[8].ReadOnly = datatable.Rows[i]["PRINT"].ToString().Equals("1") ? false : true;
                MenudataGridView.Rows[i].Cells[9].ReadOnly = datatable.Rows[i]["CLOSE"].ToString().Equals("1") ? false : true;
                MenudataGridView.Rows[i].Cells[10].ReadOnly = datatable.Rows[i]["IMPORT"].ToString().Equals("1") ? false : true;
                MenudataGridView.Rows[i].Cells[11].ReadOnly = datatable.Rows[i]["EXPORT"].ToString().Equals("1") ? false : true;
                MenudataGridView.Rows[i].Cells[12].ReadOnly = datatable.Rows[i]["CONFIRM"].ToString().Equals("1") ? false : true;
                MenudataGridView.Rows[i].Cells[13].ReadOnly = datatable.Rows[i]["CANCEL"].ToString().Equals("1") ? false : true;
                MenudataGridView.Rows[i].Cells[14].ReadOnly = datatable.Rows[i]["VISIBLE"].ToString().Equals("1") ? false : true;

                MenudataGridView.Rows[i].Cells[3].ReadOnly = datatable.Rows[i]["NEW"].ToString().Equals("1") ? false : true;
                MenudataGridView.Rows[i].Cells[4].ReadOnly = datatable.Rows[i]["SAVE"].ToString().Equals("1") ? false : true;
                MenudataGridView.Rows[i].Cells[5].ReadOnly = datatable.Rows[i]["EDIT"].ToString().Equals("1") ? false : true;
                MenudataGridView.Rows[i].Cells[6].ReadOnly = datatable.Rows[i]["FIND"].ToString().Equals("1") ? false : true;
                MenudataGridView.Rows[i].Cells[7].ReadOnly = datatable.Rows[i]["DELETE"].ToString().Equals("1") ? false : true;
                MenudataGridView.Rows[i].Cells[8].ReadOnly = datatable.Rows[i]["PRINT"].ToString().Equals("1") ? false : true;
                MenudataGridView.Rows[i].Cells[9].ReadOnly = datatable.Rows[i]["CLOSE"].ToString().Equals("1") ? false : true;
                MenudataGridView.Rows[i].Cells[10].ReadOnly = datatable.Rows[i]["IMPORT"].ToString().Equals("1") ? false : true;
                MenudataGridView.Rows[i].Cells[11].ReadOnly = datatable.Rows[i]["EXPORT"].ToString().Equals("1") ? false : true;
                MenudataGridView.Rows[i].Cells[12].ReadOnly = datatable.Rows[i]["CONFIRM"].ToString().Equals("1") ? false : true;
                MenudataGridView.Rows[i].Cells[13].ReadOnly = datatable.Rows[i]["CANCEL"].ToString().Equals("1") ? false : true;
                MenudataGridView.Rows[i].Cells[14].ReadOnly = datatable.Rows[i]["VISIBLE"].ToString().Equals("1") ? false : true;

                MenudataGridView.Rows[i].Cells[3].Style.BackColor = !datatable.Rows[i]["NEW"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : MenudataGridView.Rows[i].Cells[3].Style.BackColor;
                MenudataGridView.Rows[i].Cells[4].Style.BackColor = !datatable.Rows[i]["SAVE"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : MenudataGridView.Rows[i].Cells[4].Style.BackColor;
                MenudataGridView.Rows[i].Cells[5].Style.BackColor = !datatable.Rows[i]["EDIT"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : MenudataGridView.Rows[i].Cells[5].Style.BackColor;
                MenudataGridView.Rows[i].Cells[6].Style.BackColor = !datatable.Rows[i]["FIND"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : MenudataGridView.Rows[i].Cells[6].Style.BackColor;
                MenudataGridView.Rows[i].Cells[7].Style.BackColor = !datatable.Rows[i]["DELETE"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : MenudataGridView.Rows[i].Cells[7].Style.BackColor;
                MenudataGridView.Rows[i].Cells[8].Style.BackColor = !datatable.Rows[i]["PRINT"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : MenudataGridView.Rows[i].Cells[8].Style.BackColor;
                MenudataGridView.Rows[i].Cells[9].Style.BackColor = !datatable.Rows[i]["CLOSE"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : MenudataGridView.Rows[i].Cells[9].Style.BackColor;
                MenudataGridView.Rows[i].Cells[10].Style.BackColor = !datatable.Rows[i]["IMPORT"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : MenudataGridView.Rows[i].Cells[10].Style.BackColor;
                MenudataGridView.Rows[i].Cells[11].Style.BackColor = !datatable.Rows[i]["EXPORT"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : MenudataGridView.Rows[i].Cells[11].Style.BackColor;
                MenudataGridView.Rows[i].Cells[12].Style.BackColor = !datatable.Rows[i]["CONFIRM"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : MenudataGridView.Rows[i].Cells[12].Style.BackColor;
                MenudataGridView.Rows[i].Cells[13].Style.BackColor = !datatable.Rows[i]["CANCEL"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : MenudataGridView.Rows[i].Cells[13].Style.BackColor;
                MenudataGridView.Rows[i].Cells[14].Style.BackColor = !datatable.Rows[i]["VISIBLE"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : MenudataGridView.Rows[i].Cells[14].Style.BackColor;

                MenudataGridView.Rows[i].Cells[3].Style.SelectionBackColor = !datatable.Rows[i]["NEW"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : MenudataGridView.Rows[i].Cells[3].Style.BackColor;
                MenudataGridView.Rows[i].Cells[4].Style.SelectionBackColor = !datatable.Rows[i]["SAVE"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : MenudataGridView.Rows[i].Cells[4].Style.BackColor;
                MenudataGridView.Rows[i].Cells[5].Style.SelectionBackColor = !datatable.Rows[i]["EDIT"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : MenudataGridView.Rows[i].Cells[5].Style.BackColor;
                MenudataGridView.Rows[i].Cells[6].Style.SelectionBackColor = !datatable.Rows[i]["FIND"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : MenudataGridView.Rows[i].Cells[6].Style.BackColor;
                MenudataGridView.Rows[i].Cells[7].Style.SelectionBackColor = !datatable.Rows[i]["DELETE"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : MenudataGridView.Rows[i].Cells[7].Style.BackColor;
                MenudataGridView.Rows[i].Cells[8].Style.SelectionBackColor = !datatable.Rows[i]["PRINT"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : MenudataGridView.Rows[i].Cells[8].Style.BackColor;
                MenudataGridView.Rows[i].Cells[9].Style.SelectionBackColor = !datatable.Rows[i]["CLOSE"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : MenudataGridView.Rows[i].Cells[9].Style.BackColor;
                MenudataGridView.Rows[i].Cells[10].Style.SelectionBackColor = !datatable.Rows[i]["IMPORT"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : MenudataGridView.Rows[i].Cells[10].Style.BackColor;
                MenudataGridView.Rows[i].Cells[11].Style.SelectionBackColor = !datatable.Rows[i]["EXPORT"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : MenudataGridView.Rows[i].Cells[11].Style.BackColor;
                MenudataGridView.Rows[i].Cells[12].Style.SelectionBackColor = !datatable.Rows[i]["CONFIRM"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : MenudataGridView.Rows[i].Cells[12].Style.BackColor;
                MenudataGridView.Rows[i].Cells[13].Style.SelectionBackColor = !datatable.Rows[i]["CANCEL"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : MenudataGridView.Rows[i].Cells[13].Style.BackColor;
                MenudataGridView.Rows[i].Cells[14].Style.SelectionBackColor = !datatable.Rows[i]["VISIBLE"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : MenudataGridView.Rows[i].Cells[14].Style.BackColor;
            }
            for (int i = 0; i < MenudataGridView.Columns.Count; i++)
            {
                MenudataGridView.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            }

            MenudataGridView.Columns[2].Frozen = true;
            
        }

        private void fillDatatableUser(DataTable datatable)
        {
            UserdataGridView.DataSource = datatable;
            mappingauthenticationuser(UserdataGridView.CurrentCell.Value.ToString());
        }

        private void mappingauthenticationuser(String UserID) {
            BackgroundWorker bgWorker = new BackgroundWorker();
            bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWorkMappingAuthenticationUser);
            bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompletedMappingAuthenticationUser);
            bgWorker.RunWorkerAsync(UserID);
        }

        private void bgWorker_RunWorkerCompletedMappingAuthenticationUser(object sender, RunWorkerCompletedEventArgs e)
        {
           
        }

        private void bgWorker_DoWorkMappingAuthenticationUser(object sender, DoWorkEventArgs e)
        {
            try
            {
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@EMPLOYEEID", e.Argument.ToString()));
                MenudataGridView.Invoke(new fillDatatableCallBack(this.fillDatatableMappingAuthenticationUser), conn.callProcedureDatatable("SELECT_AUTHENTICATION_USER", param));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void fillDatatableMappingAuthenticationUser(DataTable datatable) {
            progressBar.Value = 0;
            for (int i = 0; i < datatable.Rows.Count; i++)
            {
                String menuid = datatable.Rows[i]["MENUID"].ToString();
                String submenuid = datatable.Rows[i]["SUBMENUID"].ToString();
                DataGridViewRow rows = MenudataGridView.Rows
                                                       .Cast<DataGridViewRow>()
                                                       .Where(r => r.Cells[0].Value.ToString().Equals(menuid))
                                                       .Where(r => r.Cells[1].Value.ToString().Equals(submenuid))
                                                       .FirstOrDefault();
                if (rows != null)
                {
                    MenudataGridView.Rows[rows.Index].Cells[0].Value = datatable.Rows[i]["MENUID"].ToString();
                    MenudataGridView.Rows[rows.Index].Cells[1].Value = datatable.Rows[i]["SUBMENUID"].ToString();
                    MenudataGridView.Rows[rows.Index].Cells[2].Value = datatable.Rows[i]["MENUNAME"].ToString();
                    MenudataGridView.Rows[rows.Index].Cells[3].Value = ischeckeddatatable(i, datatable, "NEW") ? true : false;
                    MenudataGridView.Rows[rows.Index].Cells[4].Value = ischeckeddatatable(i, datatable, "SAVE") ? true : false;
                    MenudataGridView.Rows[rows.Index].Cells[5].Value = ischeckeddatatable(i, datatable, "EDIT") ? true : false;
                    MenudataGridView.Rows[rows.Index].Cells[6].Value = ischeckeddatatable(i, datatable, "FIND") ? true : false;
                    MenudataGridView.Rows[rows.Index].Cells[7].Value = ischeckeddatatable(i, datatable, "DELETE") ? true : false;
                    MenudataGridView.Rows[rows.Index].Cells[8].Value = ischeckeddatatable(i, datatable, "PRINT") ? true : false;
                    MenudataGridView.Rows[rows.Index].Cells[9].Value = ischeckeddatatable(i, datatable, "CLOSE") ? true : false;
                    MenudataGridView.Rows[rows.Index].Cells[10].Value = ischeckeddatatable(i, datatable, "IMPORT") ? true : false;
                    MenudataGridView.Rows[rows.Index].Cells[11].Value = ischeckeddatatable(i, datatable, "EXPORT") ? true : false;
                    MenudataGridView.Rows[rows.Index].Cells[12].Value = ischeckeddatatable(i, datatable, "CONFIRM") ? true : false;
                    MenudataGridView.Rows[rows.Index].Cells[13].Value = ischeckeddatatable(i, datatable, "CANCEL") ? true : false;
                    MenudataGridView.Rows[rows.Index].Cells[14].Value = ischeckeddatatable(i, datatable, "VISIBLE") ? true : false;
                }
                double progress = Math.Floor(((double)(i + 1) / (double)datatable.Rows.Count) * 100);
                progressBar.Value = (int)progress;
            }
            for (int i = datatable.Rows.Count; i < MenudataGridView.RowCount; i++)
            {
                MenudataGridView.Rows[i].Cells[0].Value = MenudataGridView.Rows[i].Cells[0].Value;
                MenudataGridView.Rows[i].Cells[1].Value = MenudataGridView.Rows[i].Cells[1].Value;
                MenudataGridView.Rows[i].Cells[2].Value = MenudataGridView.Rows[i].Cells[2].Value;
                MenudataGridView.Rows[i].Cells[3].Value = false;
                MenudataGridView.Rows[i].Cells[4].Value = false;
                MenudataGridView.Rows[i].Cells[5].Value = false;
                MenudataGridView.Rows[i].Cells[6].Value = false;
                MenudataGridView.Rows[i].Cells[7].Value = false;
                MenudataGridView.Rows[i].Cells[8].Value = false;
                MenudataGridView.Rows[i].Cells[9].Value = false;
                MenudataGridView.Rows[i].Cells[10].Value = false;
                MenudataGridView.Rows[i].Cells[11].Value = false;
                MenudataGridView.Rows[i].Cells[12].Value = false;
                MenudataGridView.Rows[i].Cells[13].Value = false;
                MenudataGridView.Rows[i].Cells[14].Value = false;
            }
           
        }

        private Boolean ischeckeddatatable(int rows, DataTable datatable, String ColumnName) {
            return !datatable.Rows[rows][ColumnName].ToString().Equals("0") && !datatable.Rows[rows][ColumnName].ToString().Equals(String.Empty);
        }

        private void MenudataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void UserdataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                String UserID = UserdataGridView["EMPLOYEEID", e.RowIndex].Value.ToString();
                mappingauthenticationuser(UserID);
            }
        }

        public void save() {
            String UserID = UserdataGridView["EMPLOYEEID", UserdataGridView.CurrentRow.Index].Value.ToString();
            List<String> listquery = new List<String>();
            String TextBox = "System.Windows.Forms.DataGridViewTextBoxCell";
            String CheckBox = "System.Windows.Forms.DataGridViewCheckBoxCell";
            listquery.Clear();
            progressBar.Value = 0;
            for (int i = 0; i < MenudataGridView.RowCount; i++)
            {
                StringBuilder querybuilder = new StringBuilder();
                querybuilder.Append("INSERT INTO AUTHENTICATION_USER(MenuID, SubMenuID, UserID, new, [save], edit, " +
                                    "find, [delete], [print], [close], import, export, confirm, cancel, visible) " +
                                    "VALUES(");
                List<String> columns = new List<String>();
                for (int j = 0; j < MenudataGridView.ColumnCount; j++)
                {
                    if (TextBox.Equals(MenudataGridView.Rows[i].Cells[j].GetType().ToString()))
                    {
                        String str = (j == 2) ? String.Format("'{0}'",UserID) : String.Format("'{0}'", MenudataGridView.Rows[i].Cells[j].Value.ToString());
                        columns.Add(str);
                    }
                    if (CheckBox.Equals(MenudataGridView.Rows[i].Cells[j].GetType().ToString()))
                    {
                        DataGridViewCheckBoxCell checkbox = (DataGridViewCheckBoxCell)MenudataGridView.Rows[i].Cells[j];
                        
                        Object isreadonly = checkbox.ReadOnly ? "YES" : "NO";
                        if (isreadonly.Equals("YES"))
                        {
                            columns.Add("NULL");
                        }
                        else
                        {
                            int ischeck = 0;
                            if (checkbox.Value != null)
                            {
                                ischeck = checkbox.Value.ToString().Equals("True") ? 1 : 0;
                            }
                            columns.Add(ischeck.ToString());
                        }
                    }
                }
                String imp = String.Join(",", columns.ToArray());
                querybuilder.Append(imp).Append(")");
                listquery.Add(querybuilder.ToString());
            }

            String del = String.Format("DELETE FROM AUTHENTICATION_USER WHERE UserID = '{0}'", UserID);
            if (conn.openReaderQuery(del))
            {
                for (int i = 0; i < listquery.Count; i++)
                {
                    if (conn.openReaderQuery(listquery[i]))
                    {
                        double progress = Math.Floor(((double)(i + 1) / (double)listquery.Count) * 100);
                        progressBar.Value = (int)progress;
                    }
                }
            }
            MessageBox.Show("SAVE");
        }

        private void AuthenticationUser_Activated(object sender, EventArgs e)
        {
            Main.ACTIVEFORM = this;
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
        }

        private void MenudataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;

            if (e.ColumnIndex >= 3)
            {
                var dataGridView = (DataGridView)sender;
                //Console.WriteLine(dataGridView[0,e.RowIndex].Value);
                if (dataGridView[1, e.RowIndex].Value.ToString().Equals("001")) {
                    Console.WriteLine(dataGridView[2, e.RowIndex].Value);
                }
                //Console.WriteLine(dataGridView[1, e.RowIndex].Value);
                //Console.WriteLine(dataGridView[2, e.RowIndex].Value);
                var cell = dataGridView[e.ColumnIndex, e.RowIndex];
                if (!cell.ReadOnly)
                {
                    if (cell.Value == null) cell.Value = false;
                    cell.Value = !(bool)cell.Value;
                }
            }
        }

        private void AuthenticationUser_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (conn != null) conn.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }

        private void buttonSelectAll_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in MenudataGridView.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
	            {//DataGridViewCheckBoxCell
                    if (cell.GetType() == typeof(DataGridViewCheckBoxCell)) {
                        DataGridViewCheckBoxCell checkboxcell = (DataGridViewCheckBoxCell)cell;
                        if (!checkboxcell.ReadOnly) {
                            checkboxcell.Value = true;
                        }  
                    }
                    
	            }
            }
            
        }

        private void buttonunselectall_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in MenudataGridView.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {//DataGridViewCheckBoxCell
                    if (cell.GetType() == typeof(DataGridViewCheckBoxCell))
                    {
                        DataGridViewCheckBoxCell checkboxcell = (DataGridViewCheckBoxCell)cell;
                        if (!checkboxcell.ReadOnly)
                        {
                            checkboxcell.Value = false;
                        }
                    }

                }
            }
        }
    }
}
