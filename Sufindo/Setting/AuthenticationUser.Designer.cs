﻿namespace Sufindo.Setting
{
    partial class AuthenticationUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.UserdataGridView = new System.Windows.Forms.DataGridView();
            this.EMPLOYEEID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMPLOYEENAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POSITION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MenudataGridView = new System.Windows.Forms.DataGridView();
            this.MENUID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SUBMENUID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MENUNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NEW = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.SAVE = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.EDIT = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.FIND = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DELETE = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PRINT = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.CLOSE = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.IMPORT = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.EXPORT = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.CONFIRM = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.CANCEL = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.VISIBLE = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.buttonSelectAll = new System.Windows.Forms.Button();
            this.buttonunselectall = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.UserdataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MenudataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // UserdataGridView
            // 
            this.UserdataGridView.AllowUserToAddRows = false;
            this.UserdataGridView.AllowUserToDeleteRows = false;
            this.UserdataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.UserdataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.UserdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.UserdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EMPLOYEEID,
            this.EMPLOYEENAME,
            this.POSITION});
            this.UserdataGridView.Location = new System.Drawing.Point(3, 3);
            this.UserdataGridView.Name = "UserdataGridView";
            this.UserdataGridView.ReadOnly = true;
            this.UserdataGridView.RowHeadersVisible = false;
            this.UserdataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.UserdataGridView.Size = new System.Drawing.Size(319, 523);
            this.UserdataGridView.TabIndex = 0;
            this.UserdataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.UserdataGridView_CellClick);
            // 
            // EMPLOYEEID
            // 
            this.EMPLOYEEID.DataPropertyName = "EMPLOYEEID";
            this.EMPLOYEEID.FillWeight = 120F;
            this.EMPLOYEEID.HeaderText = "EMPLOYEE ID";
            this.EMPLOYEEID.Name = "EMPLOYEEID";
            this.EMPLOYEEID.ReadOnly = true;
            this.EMPLOYEEID.Width = 104;
            // 
            // EMPLOYEENAME
            // 
            this.EMPLOYEENAME.DataPropertyName = "EMPLOYEENAME";
            this.EMPLOYEENAME.FillWeight = 140F;
            this.EMPLOYEENAME.HeaderText = "EMPLOYEE NAME";
            this.EMPLOYEENAME.Name = "EMPLOYEENAME";
            this.EMPLOYEENAME.ReadOnly = true;
            this.EMPLOYEENAME.Width = 124;
            // 
            // POSITION
            // 
            this.POSITION.DataPropertyName = "POSITION";
            this.POSITION.HeaderText = "POSITION";
            this.POSITION.Name = "POSITION";
            this.POSITION.ReadOnly = true;
            this.POSITION.Width = 83;
            // 
            // MenudataGridView
            // 
            this.MenudataGridView.AllowUserToAddRows = false;
            this.MenudataGridView.AllowUserToDeleteRows = false;
            this.MenudataGridView.AllowUserToResizeColumns = false;
            this.MenudataGridView.AllowUserToResizeRows = false;
            this.MenudataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.MenudataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.MenudataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.MenudataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MenudataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MENUID,
            this.SUBMENUID,
            this.MENUNAME,
            this.NEW,
            this.SAVE,
            this.EDIT,
            this.FIND,
            this.DELETE,
            this.PRINT,
            this.CLOSE,
            this.IMPORT,
            this.EXPORT,
            this.CONFIRM,
            this.CANCEL,
            this.VISIBLE});
            this.MenudataGridView.Location = new System.Drawing.Point(328, 36);
            this.MenudataGridView.MultiSelect = false;
            this.MenudataGridView.Name = "MenudataGridView";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.MenudataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.MenudataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.MenudataGridView.Size = new System.Drawing.Size(811, 490);
            this.MenudataGridView.TabIndex = 1;
            this.MenudataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MenudataGridView_CellDoubleClick);
            this.MenudataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MenudataGridView_CellContentClick);
            // 
            // MENUID
            // 
            this.MENUID.DataPropertyName = "MENUID";
            this.MENUID.HeaderText = "MENUID";
            this.MENUID.Name = "MENUID";
            this.MENUID.Visible = false;
            this.MENUID.Width = 75;
            // 
            // SUBMENUID
            // 
            this.SUBMENUID.DataPropertyName = "SUBMENUID";
            this.SUBMENUID.HeaderText = "SUBMENUID";
            this.SUBMENUID.Name = "SUBMENUID";
            this.SUBMENUID.Visible = false;
            this.SUBMENUID.Width = 97;
            // 
            // MENUNAME
            // 
            this.MENUNAME.DataPropertyName = "MENUNAME";
            this.MENUNAME.HeaderText = "MENU NAME";
            this.MENUNAME.Name = "MENUNAME";
            this.MENUNAME.Width = 98;
            // 
            // NEW
            // 
            this.NEW.DataPropertyName = "NEW";
            this.NEW.HeaderText = "NEW";
            this.NEW.Name = "NEW";
            this.NEW.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.NEW.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.NEW.Width = 58;
            // 
            // SAVE
            // 
            this.SAVE.DataPropertyName = "SAVE";
            this.SAVE.HeaderText = "SAVE";
            this.SAVE.Name = "SAVE";
            this.SAVE.Width = 41;
            // 
            // EDIT
            // 
            this.EDIT.DataPropertyName = "EDIT";
            this.EDIT.HeaderText = "EDIT";
            this.EDIT.Name = "EDIT";
            this.EDIT.Width = 38;
            // 
            // FIND
            // 
            this.FIND.DataPropertyName = "FIND";
            this.FIND.HeaderText = "FIND";
            this.FIND.Name = "FIND";
            this.FIND.Width = 38;
            // 
            // DELETE
            // 
            this.DELETE.HeaderText = "DELETE";
            this.DELETE.Name = "DELETE";
            this.DELETE.Width = 55;
            // 
            // PRINT
            // 
            this.PRINT.DataPropertyName = "PRINT";
            this.PRINT.HeaderText = "PRINT";
            this.PRINT.Name = "PRINT";
            this.PRINT.Width = 46;
            // 
            // CLOSE
            // 
            this.CLOSE.DataPropertyName = "CLOSE";
            this.CLOSE.HeaderText = "CLOSE";
            this.CLOSE.Name = "CLOSE";
            this.CLOSE.Width = 48;
            // 
            // IMPORT
            // 
            this.IMPORT.DataPropertyName = "IMPORT";
            this.IMPORT.HeaderText = "IMPORT";
            this.IMPORT.Name = "IMPORT";
            this.IMPORT.Width = 55;
            // 
            // EXPORT
            // 
            this.EXPORT.DataPropertyName = "EXPORT";
            this.EXPORT.HeaderText = "EXPORT";
            this.EXPORT.Name = "EXPORT";
            this.EXPORT.Width = 57;
            // 
            // CONFIRM
            // 
            this.CONFIRM.DataPropertyName = "CONFIRM";
            this.CONFIRM.HeaderText = "CONFIRM";
            this.CONFIRM.Name = "CONFIRM";
            this.CONFIRM.Width = 62;
            // 
            // CANCEL
            // 
            this.CANCEL.DataPropertyName = "CANCEL";
            this.CANCEL.HeaderText = "CANCEL";
            this.CANCEL.Name = "CANCEL";
            this.CANCEL.Width = 55;
            // 
            // VISIBLE
            // 
            this.VISIBLE.DataPropertyName = "VISIBLE";
            this.VISIBLE.HeaderText = "VISIBLE";
            this.VISIBLE.Name = "VISIBLE";
            this.VISIBLE.Width = 53;
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(3, 532);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(1136, 23);
            this.progressBar.Step = 1;
            this.progressBar.TabIndex = 2;
            // 
            // buttonSelectAll
            // 
            this.buttonSelectAll.Location = new System.Drawing.Point(329, 3);
            this.buttonSelectAll.Name = "buttonSelectAll";
            this.buttonSelectAll.Size = new System.Drawing.Size(75, 27);
            this.buttonSelectAll.TabIndex = 3;
            this.buttonSelectAll.Text = "Select All";
            this.buttonSelectAll.UseVisualStyleBackColor = true;
            this.buttonSelectAll.Click += new System.EventHandler(this.buttonSelectAll_Click);
            // 
            // buttonunselectall
            // 
            this.buttonunselectall.Location = new System.Drawing.Point(410, 3);
            this.buttonunselectall.Name = "buttonunselectall";
            this.buttonunselectall.Size = new System.Drawing.Size(75, 27);
            this.buttonunselectall.TabIndex = 4;
            this.buttonunselectall.Text = "Unselect All";
            this.buttonunselectall.UseVisualStyleBackColor = true;
            this.buttonunselectall.Click += new System.EventHandler(this.buttonunselectall_Click);
            // 
            // AuthenticationUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1142, 559);
            this.Controls.Add(this.buttonunselectall);
            this.Controls.Add(this.buttonSelectAll);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.MenudataGridView);
            this.Controls.Add(this.UserdataGridView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AuthenticationUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Authentication User";
            this.Activated += new System.EventHandler(this.AuthenticationUser_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AuthenticationUser_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.UserdataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MenudataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView UserdataGridView;
        private System.Windows.Forms.DataGridView MenudataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMPLOYEEID;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMPLOYEENAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn POSITION;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.DataGridViewTextBoxColumn MENUID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SUBMENUID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MENUNAME;
        private System.Windows.Forms.DataGridViewCheckBoxColumn NEW;
        private System.Windows.Forms.DataGridViewCheckBoxColumn SAVE;
        private System.Windows.Forms.DataGridViewCheckBoxColumn EDIT;
        private System.Windows.Forms.DataGridViewCheckBoxColumn FIND;
        private System.Windows.Forms.DataGridViewCheckBoxColumn DELETE;
        private System.Windows.Forms.DataGridViewCheckBoxColumn PRINT;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CLOSE;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IMPORT;
        private System.Windows.Forms.DataGridViewCheckBoxColumn EXPORT;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CONFIRM;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CANCEL;
        private System.Windows.Forms.DataGridViewCheckBoxColumn VISIBLE;
        private System.Windows.Forms.Button buttonSelectAll;
        private System.Windows.Forms.Button buttonunselectall;
    }
}