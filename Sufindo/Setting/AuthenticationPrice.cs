﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Sufindo.Setting
{
    public partial class AuthenticationPrice : Form
    {
        Connection conn = null;

        MenuStrip menustripAction;

        private delegate void fillDatatableCallBack(DataTable datatable);

        public AuthenticationPrice()
        {
            InitializeComponent();
            conn = new Connection();
            BackgroundWorker bgWorker = new BackgroundWorker();
            bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
            bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
            bgWorker.RunWorkerAsync();
        }

        public AuthenticationPrice(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            this.menustripAction = menustripAction;
            conn = new Connection();
            BackgroundWorker bgWorker = new BackgroundWorker();
            bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
            bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
            bgWorker.RunWorkerAsync();
        }

        private void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            
        }

        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                /*
                    0 -> tidak ada  ( textbox - )
                    1 -> ada        ( checkbox  )
                 */
                String query = String.Format("SELECT AuthPriceID, AuthPriceName, [VISIBLE] " +
                                             "FROM M_AUTHENTICATION_PRICE " +
                                             "ORDER BY AuthPriceID ");
                
                PricedataGridView.Invoke(new fillDatatableCallBack(this.fillDatatable), conn.openDataTableQuery(query));

                query = String.Format("SELECT EMPLOYEEID, EMPLOYEENAME, POSITION FROM M_EMPLOYEE WHERE STATUS = '{0}'", Status.ACTIVE);

                UserdataGridView.Invoke(new fillDatatableCallBack(this.fillDatatableUser), conn.openDataTableQuery(query));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void fillDatatable(DataTable datatable)
        {
            for (int i = 0; i < datatable.Rows.Count; i++)
            {
                PricedataGridView.Rows.Add();
                
                PricedataGridView.Rows[i].HeaderCell.Value = (i + 1).ToString();

                PricedataGridView.Rows[i].Cells[0].Value = datatable.Rows[i]["AuthPriceID"].ToString();
                PricedataGridView.Rows[i].Cells[0].ReadOnly = true;
                PricedataGridView.Rows[i].Cells[1].Value = datatable.Rows[i]["AuthPriceName"].ToString();
                PricedataGridView.Rows[i].Cells[1].ReadOnly = true;

                PricedataGridView.Rows[i].Cells[2].ReadOnly = datatable.Rows[i]["VISIBLE"].ToString().Equals("1") ? false : true;

                PricedataGridView.Rows[i].Cells[2].ReadOnly = datatable.Rows[i]["VISIBLE"].ToString().Equals("1") ? false : true;

                PricedataGridView.Rows[i].Cells[2].Style.BackColor = !datatable.Rows[i]["VISIBLE"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : PricedataGridView.Rows[i].Cells[2].Style.BackColor;

                PricedataGridView.Rows[i].Cells[2].Style.SelectionBackColor = !datatable.Rows[i]["VISIBLE"].ToString().Equals("1") ? Color.FromArgb(216, 216, 216) : PricedataGridView.Rows[i].Cells[2].Style.BackColor;
            }
            for (int i = 0; i < PricedataGridView.Columns.Count; i++)
            {
                PricedataGridView.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            }

            PricedataGridView.Columns[2].Frozen = true;
            
        }

        private void fillDatatableUser(DataTable datatable)
        {
            UserdataGridView.DataSource = datatable;
            mappingauthenticationuser(UserdataGridView.CurrentCell.Value.ToString());
        }

        private void mappingauthenticationuser(String UserID) {
            BackgroundWorker bgWorker = new BackgroundWorker();
            bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWorkMappingAuthenticationUser);
            bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompletedMappingAuthenticationUser);
            bgWorker.RunWorkerAsync(UserID);
        }

        private void bgWorker_RunWorkerCompletedMappingAuthenticationUser(object sender, RunWorkerCompletedEventArgs e)
        {
           
        }

        private void bgWorker_DoWorkMappingAuthenticationUser(object sender, DoWorkEventArgs e)
        {
            try
            {
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@EMPLOYEEID", e.Argument.ToString()));
                PricedataGridView.Invoke(new fillDatatableCallBack(this.fillDatatableMappingAuthenticationUser), conn.callProcedureDatatable("SELECT_AUTHENTICATION_PRICE", param));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void fillDatatableMappingAuthenticationUser(DataTable datatable) {
            progressBar.Value = 0;
            for (int i = 0; i < datatable.Rows.Count; i++)
            {
                String authpriceid = datatable.Rows[i]["AuthPriceID"].ToString();
                DataGridViewRow rows = PricedataGridView.Rows
                                                       .Cast<DataGridViewRow>()
                                                       .Where(r => r.Cells[0].Value.ToString().Equals(authpriceid))
                                                       .FirstOrDefault();
                if (rows != null)
                {
                    PricedataGridView.Rows[rows.Index].Cells[0].Value = datatable.Rows[i]["AuthPriceID"].ToString();
                    PricedataGridView.Rows[rows.Index].Cells[1].Value = datatable.Rows[i]["AuthPriceName"].ToString();
                    PricedataGridView.Rows[rows.Index].Cells[2].Value = ischeckeddatatable(i, datatable, "VISIBLE") ? true : false;
                }
                double progress = Math.Floor(((double)(i + 1) / (double)datatable.Rows.Count) * 100);
                progressBar.Value = (int)progress;
            }
            for (int i = datatable.Rows.Count; i < PricedataGridView.RowCount; i++)
            {
                PricedataGridView.Rows[i].Cells[0].Value = PricedataGridView.Rows[i].Cells[0].Value;
                PricedataGridView.Rows[i].Cells[1].Value = PricedataGridView.Rows[i].Cells[1].Value;
                PricedataGridView.Rows[i].Cells[2].Value = false;
            }
           
        }

        private Boolean ischeckeddatatable(int rows, DataTable datatable, String ColumnName) {
            return !datatable.Rows[rows][ColumnName].ToString().Equals("0") && !datatable.Rows[rows][ColumnName].ToString().Equals(String.Empty);
        }

        private void UserdataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                String UserID = UserdataGridView["EMPLOYEEID", e.RowIndex].Value.ToString();
                mappingauthenticationuser(UserID);
            }
        }

        public void save() {
            String UserID = UserdataGridView["EMPLOYEEID", UserdataGridView.CurrentRow.Index].Value.ToString();
            List<String> listquery = new List<String>();
            String TextBox = "System.Windows.Forms.DataGridViewTextBoxCell";
            String CheckBox = "System.Windows.Forms.DataGridViewCheckBoxCell";
            listquery.Clear();
            progressBar.Value = 0;
            for (int i = 0; i < PricedataGridView.RowCount; i++)
            {
                StringBuilder querybuilder = new StringBuilder();
                querybuilder.Append("INSERT INTO AUTHENTICATION_PRICE(AuthPriceID, UserID, visible) " +
                                    "VALUES(");
                List<String> columns = new List<String>();
                for (int j = 0; j < PricedataGridView.ColumnCount; j++)
                {
                    if (TextBox.Equals(PricedataGridView.Rows[i].Cells[j].GetType().ToString()))
                    {
                        String str = (j == 1) ? String.Format("'{0}'",UserID) : String.Format("'{0}'", PricedataGridView.Rows[i].Cells[j].Value.ToString());
                        columns.Add(str);
                    }
                    else if (CheckBox.Equals(PricedataGridView.Rows[i].Cells[j].GetType().ToString()))
                    {
                        DataGridViewCheckBoxCell checkbox = (DataGridViewCheckBoxCell)PricedataGridView.Rows[i].Cells[j];
                        
                        Object isreadonly = checkbox.ReadOnly ? "YES" : "NO";
                       
                        if (isreadonly.Equals("YES"))
                        {
                            columns.Add("NULL");
                        }
                        else
                        {
                            int ischeck = 0;
                            if (checkbox.Value != null)
                            {
                                ischeck = checkbox.Value.ToString().Equals("True") ? 1 : 0;
                            }
                            columns.Add(ischeck.ToString());
                        }
                    }
                }
                String imp = String.Join(",", columns.ToArray());
                querybuilder.Append(imp).Append(")");
                listquery.Add(querybuilder.ToString());
            }

            String del = String.Format("DELETE FROM AUTHENTICATION_PRICE WHERE UserID = '{0}'", UserID);
            if (conn.openReaderQuery(del))
            {
                for (int i = 0; i < listquery.Count; i++)
                {
                    
                    if (conn.openReaderQuery(listquery[i]))
                    {
                        double progress = Math.Floor(((double)(i + 1) / (double)listquery.Count) * 100);
                        progressBar.Value = (int)progress;
                    }
                }
            }
            MessageBox.Show("SAVE");
        }

        private void AuthenticationPrice_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (conn != null) conn.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }

        private void AuthenticationPrice_Activated(object sender, EventArgs e)
        {
            Main.ACTIVEFORM = this;
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
        }

        private void PricedataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;

            if (e.ColumnIndex >= 2)
            {
                var dataGridView = (DataGridView)sender;
                var cell = dataGridView[e.ColumnIndex, e.RowIndex];
                if (!cell.ReadOnly)
                {
                    if (cell.Value == null) cell.Value = false;
                    cell.Value = !(bool)cell.Value;
                }
            }
        }
    }
}
