﻿namespace Sufindo
{
    partial class ItemSupplier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemSupplier));
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.datagridviewpanel = new System.Windows.Forms.Panel();
            this.MasterItemdataGridView = new System.Windows.Forms.DataGridView();
            this.NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMID_ALIAS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BRANDID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SPEC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUANTITY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RACKID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Delete = new System.Windows.Forms.DataGridViewImageColumn();
            this.searchSupplierNobutton = new System.Windows.Forms.Button();
            this.SupplierNametextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SupplierNotextBox = new System.Windows.Forms.TextBox();
            this.AddItembutton = new System.Windows.Forms.Button();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.tableLayoutPanel.SuspendLayout();
            this.datagridviewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MasterItemdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 4;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Controls.Add(this.datagridviewpanel, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.searchSupplierNobutton, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.SupplierNametextBox, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.SupplierNotextBox, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.AddItembutton, 0, 2);
            this.tableLayoutPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.tableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 4;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.Size = new System.Drawing.Size(915, 366);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // datagridviewpanel
            // 
            this.datagridviewpanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.SetColumnSpan(this.datagridviewpanel, 4);
            this.datagridviewpanel.Controls.Add(this.MasterItemdataGridView);
            this.datagridviewpanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.datagridviewpanel.Location = new System.Drawing.Point(3, 89);
            this.datagridviewpanel.Name = "datagridviewpanel";
            this.datagridviewpanel.Size = new System.Drawing.Size(909, 274);
            this.datagridviewpanel.TabIndex = 19;
            // 
            // MasterItemdataGridView
            // 
            this.MasterItemdataGridView.AllowUserToAddRows = false;
            this.MasterItemdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.MasterItemdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NO,
            this.ITEMID,
            this.ITEMNAME,
            this.ITEMID_ALIAS,
            this.BRANDID,
            this.SPEC,
            this.QUANTITY,
            this.UOMID,
            this.RACKID,
            this.Delete});
            this.MasterItemdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MasterItemdataGridView.Location = new System.Drawing.Point(0, 0);
            this.MasterItemdataGridView.Name = "MasterItemdataGridView";
            this.MasterItemdataGridView.ReadOnly = true;
            this.MasterItemdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.MasterItemdataGridView.Size = new System.Drawing.Size(909, 274);
            this.MasterItemdataGridView.TabIndex = 0;
            this.MasterItemdataGridView.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.MasterItemdataGridView_CellMouseUp);
            this.MasterItemdataGridView.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.MasterItemdataGridView_UserDeletedRow);
            this.MasterItemdataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MasterItemdataGridView_CellClick);
            // 
            // NO
            // 
            this.NO.DataPropertyName = "NO";
            this.NO.HeaderText = "NO";
            this.NO.Name = "NO";
            this.NO.ReadOnly = true;
            this.NO.Width = 40;
            // 
            // ITEMID
            // 
            this.ITEMID.DataPropertyName = "ITEMID";
            this.ITEMID.HeaderText = "PART NO";
            this.ITEMID.Name = "ITEMID";
            this.ITEMID.ReadOnly = true;
            // 
            // ITEMNAME
            // 
            this.ITEMNAME.DataPropertyName = "ITEMNAME";
            this.ITEMNAME.HeaderText = "PART NAME";
            this.ITEMNAME.Name = "ITEMNAME";
            this.ITEMNAME.ReadOnly = true;
            // 
            // ITEMID_ALIAS
            // 
            this.ITEMID_ALIAS.DataPropertyName = "ITEMID_ALIAS";
            this.ITEMID_ALIAS.HeaderText = "ALIAS";
            this.ITEMID_ALIAS.Name = "ITEMID_ALIAS";
            this.ITEMID_ALIAS.ReadOnly = true;
            // 
            // BRANDID
            // 
            this.BRANDID.DataPropertyName = "BRANDID";
            this.BRANDID.HeaderText = "BRAND";
            this.BRANDID.Name = "BRANDID";
            this.BRANDID.ReadOnly = true;
            // 
            // SPEC
            // 
            this.SPEC.DataPropertyName = "SPEC";
            this.SPEC.HeaderText = "SPEC";
            this.SPEC.Name = "SPEC";
            this.SPEC.ReadOnly = true;
            // 
            // QUANTITY
            // 
            this.QUANTITY.DataPropertyName = "QUANTITY";
            this.QUANTITY.HeaderText = "QUANTITY";
            this.QUANTITY.Name = "QUANTITY";
            this.QUANTITY.ReadOnly = true;
            // 
            // UOMID
            // 
            this.UOMID.DataPropertyName = "UOMID";
            this.UOMID.HeaderText = "UOM";
            this.UOMID.Name = "UOMID";
            this.UOMID.ReadOnly = true;
            // 
            // RACKID
            // 
            this.RACKID.DataPropertyName = "RACKID";
            this.RACKID.HeaderText = "RACK";
            this.RACKID.Name = "RACKID";
            this.RACKID.ReadOnly = true;
            // 
            // Delete
            // 
            this.Delete.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.NullValue = ((object)(resources.GetObject("dataGridViewCellStyle2.NullValue")));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            this.Delete.DefaultCellStyle = dataGridViewCellStyle2;
            this.Delete.FillWeight = 24F;
            this.Delete.HeaderText = "";
            this.Delete.Image = global::Sufindo.Properties.Resources.minus;
            this.Delete.MinimumWidth = 24;
            this.Delete.Name = "Delete";
            this.Delete.ReadOnly = true;
            this.Delete.ToolTipText = "Delete";
            this.Delete.Width = 24;
            // 
            // searchSupplierNobutton
            // 
            this.searchSupplierNobutton.BackColor = System.Drawing.Color.Transparent;
            this.searchSupplierNobutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchSupplierNobutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchSupplierNobutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.searchSupplierNobutton.FlatAppearance.BorderSize = 0;
            this.searchSupplierNobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchSupplierNobutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchSupplierNobutton.Location = new System.Drawing.Point(232, 1);
            this.searchSupplierNobutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.searchSupplierNobutton.Name = "searchSupplierNobutton";
            this.searchSupplierNobutton.Size = new System.Drawing.Size(24, 24);
            this.searchSupplierNobutton.TabIndex = 16;
            this.searchSupplierNobutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.searchSupplierNobutton.UseVisualStyleBackColor = false;
            this.searchSupplierNobutton.Click += new System.EventHandler(this.searchSupplierNobutton_Click);
            // 
            // SupplierNametextBox
            // 
            this.SupplierNametextBox.BackColor = System.Drawing.SystemColors.Info;
            this.SupplierNametextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SupplierNametextBox.Location = new System.Drawing.Point(92, 31);
            this.SupplierNametextBox.Name = "SupplierNametextBox";
            this.SupplierNametextBox.Size = new System.Drawing.Size(134, 20);
            this.SupplierNametextBox.TabIndex = 3;
            this.SupplierNametextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SupplierNametextBox_KeyDown);
            this.SupplierNametextBox.Leave += new System.EventHandler(this.SupplierNametextBox_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Supplier ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Supplier Name";
            // 
            // SupplierNotextBox
            // 
            this.SupplierNotextBox.BackColor = System.Drawing.SystemColors.Info;
            this.SupplierNotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SupplierNotextBox.Location = new System.Drawing.Point(92, 3);
            this.SupplierNotextBox.Name = "SupplierNotextBox";
            this.SupplierNotextBox.Size = new System.Drawing.Size(134, 20);
            this.SupplierNotextBox.TabIndex = 2;
            this.SupplierNotextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SupplierNotextBox_KeyDown);
            this.SupplierNotextBox.Leave += new System.EventHandler(this.SupplierNotextBox_Leave);
            // 
            // AddItembutton
            // 
            this.AddItembutton.BackColor = System.Drawing.Color.Transparent;
            this.AddItembutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AddItembutton.FlatAppearance.BorderSize = 0;
            this.AddItembutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddItembutton.ForeColor = System.Drawing.Color.Transparent;
            this.AddItembutton.Image = global::Sufindo.Properties.Resources.plus;
            this.AddItembutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.AddItembutton.Location = new System.Drawing.Point(0, 54);
            this.AddItembutton.Margin = new System.Windows.Forms.Padding(0);
            this.AddItembutton.Name = "AddItembutton";
            this.AddItembutton.Size = new System.Drawing.Size(89, 32);
            this.AddItembutton.TabIndex = 18;
            this.AddItembutton.Text = "Add Item";
            this.AddItembutton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.AddItembutton.UseVisualStyleBackColor = false;
            this.AddItembutton.Click += new System.EventHandler(this.AddItembutton_Click);
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = global::Sufindo.Properties.Resources.minus;
            this.dataGridViewImageColumn1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // ItemSupplier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(919, 370);
            this.Controls.Add(this.tableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ItemSupplier";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Supplier Item";
            this.Activated += new System.EventHandler(this.ItemSupplier_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ItemSupplier_FormClosed);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.datagridviewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MasterItemdataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox SupplierNametextBox;
        private System.Windows.Forms.TextBox SupplierNotextBox;
        private System.Windows.Forms.Button searchSupplierNobutton;
        private System.Windows.Forms.Button AddItembutton;
        private System.Windows.Forms.Panel datagridviewpanel;
        private System.Windows.Forms.DataGridView MasterItemdataGridView;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMID_ALIAS;
        private System.Windows.Forms.DataGridViewTextBoxColumn BRANDID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SPEC;
        private System.Windows.Forms.DataGridViewTextBoxColumn QUANTITY;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn RACKID;
        private System.Windows.Forms.DataGridViewImageColumn Delete;
    }
}