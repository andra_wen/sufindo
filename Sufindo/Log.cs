﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Sufindo
{
    class Log
    {
        public static Boolean updateQuantityItem(List<Dictionary<Object, Object>> item){
            try
            {
                Connection con = new Connection();
                foreach (Dictionary<Object, Object> dic in item)
                {
                    List<SqlParameter> sqlParam = new List<SqlParameter>();
                    sqlParam.Add(new SqlParameter("@ITEMID", dic["ITEMID"]));
                    sqlParam.Add(new SqlParameter("@QUANTITY", dic["QUANTITY"]));
                    sqlParam.Add(new SqlParameter("@CREATEDBY", ""));
                    if (con.callProcedure("UPDATE_DATA_QUANTITY_ITEM", sqlParam))
                    {
                    }
                    else
                    {
                        MessageBox.Show("INVALID UPDATE QUANTITY ITEM");
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            
            return true;
        }

        public static Boolean insertLogTransaction(Dictionary<String, String> header, List<Dictionary<Object, Object>> item)
        {

            Connection con = new Connection();
            
            List<SqlParameter> sqlParam = new List<SqlParameter>();

            sqlParam.Add(new SqlParameter("@LOGTRANSACTIONID", header["LOGTRANSACTIONID"]));
            sqlParam.Add(new SqlParameter("@TRANSACTIONID", header["TRANSACTIONID"]));
            sqlParam.Add(new SqlParameter("@TRANSACTIONTYPE", header["TRANSACTIONTYPE"]));
            sqlParam.Add(new SqlParameter("@STATUS", header["STATUS"]));
            sqlParam.Add(new SqlParameter("@CREATEDBY", header["CREATEDBY"]));

            if (con.callProcedure("INSERT_DATA_LOG_TRANSACTION", sqlParam)) {
                foreach (Dictionary<Object, Object> dic in item)
                {
                    sqlParam = new List<SqlParameter>();
                    sqlParam.Clear();
                    sqlParam.Add(new SqlParameter("@LOGTRANSACTIONID", header["LOGTRANSACTIONID"]));
                    sqlParam.Add(new SqlParameter("@ITEMIDQTY", dic["ITEMIDQTY"]));
                    sqlParam.Add(new SqlParameter("@PRICE", dic["PRICE"]));
                    if (con.callProcedure("INSERT_DATA_D_LOG_TRANSACTION", sqlParam))
                    {

                    }
                    else
                    {
                        MessageBox.Show("INVALID INSERT LOG QUANTITY ITEM");
                        return false;
                    }
                }
            }
            

            return true;
        }
    }
}
