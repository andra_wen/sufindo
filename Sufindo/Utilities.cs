﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;
using System.Security.Cryptography;
using System.Collections;

namespace Sufindo
{
    class Utilities
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="isVisible"></param>
        public static void showMenuStripAction(ToolStripItemCollection item,bool isVisible) {
            if (item != null)
            {
                for (int i = 0; i < item.Count; i++)
                {
                    item[i].Visible = isVisible;
                }
            }
        }

        public static void changeBackgroundColorTextBox(ref Control control) { 
        
        }

        public static Boolean isNumber(String numbers) {
            return false;
        }

        
        /// <summary>
        /// Method for KeyPress Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public static Boolean onlyNumberTextBox(object sender, KeyPressEventArgs e) {
            return (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar)) ? true : false;
        }

        public static Boolean onlyDecimalTextBox(object sender, KeyPressEventArgs e) {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                return true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                return true;
            }

            if (!char.IsControl(e.KeyChar))
            {

                TextBox textBox = (TextBox)sender;

                if (textBox.Text.IndexOf('.') > -1 &&
                         textBox.Text.Substring(textBox.Text.IndexOf('.')).Length >= 3)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Method for Check String is Empty or Not
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static Boolean isEmptyString(String str) {
            return (str.Equals("")) ? true : false;
        }

        /// <summary>
        /// Method for remove Multiple space
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static String removeMultipleSpace(String str) {
            RegexOptions options = RegexOptions.None;
            Regex regex = new Regex(@"[ ]{2,}", options);
            return regex.Replace(str, @" ").Trim();
        }


        /// <summary>
        /// Method for remove Space
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static String removeSpace(String str) {
            return str.Replace(" ","");
        }

        /// <summary>
        /// Method for Generate ID (DDMMYYHHMMSS)
        /// </summary>
        /// <returns></returns>
        public static String generateID() {
            return DateTime.Now.ToString("ddMMyyHHmmss");
        }

        /// <summary>
        /// Method for Clear All Field Like ComboxBox, TextBox
        /// </summary>
        /// <param name="allField"></param>
        public static void clearAllField(ref List<Control> allField) {
            foreach (var item in allField)
            {
                if (item is TextBox) {
                    item.Text = "";
                }
                else if (item is ComboBox) {
                    ComboBox combobox = (ComboBox)item;
                    combobox.SelectedIndex = -1;
                }
                else if (item is RichTextBox) {
                    item.Text = "";
                }
            }
        }

        public static String browsePictureBox(ref PictureBox pictureBox) {
            String fileNamePath = "";
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp; *.png)|*.jpg; *.jpeg; *.gif; *.bmp; *.png";

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    String ext = Path.GetExtension(openFileDialog.FileName);
                    String[] splitFilename = openFileDialog.FileName.Split('\\');
                    Image img = Image.FromFile(openFileDialog.FileName);
                    pictureBox.Image = img;
                    pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
                    fileNamePath = openFileDialog.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return fileNamePath;
        }
        
        /// <summary>
        /// Method for Convert Image to Byte
        /// </summary>
        /// <param name="FILENAMEPATH"></param>
        /// <returns></returns>
        public static byte[] convertImageToByte(String FILENAMEPATH)
        {
            byte[] imageData = null;

            FileInfo fileInfo = new FileInfo(FILENAMEPATH);

            long imageFileLength = fileInfo.Length;

            FileStream fileStream = new FileStream(FILENAMEPATH, FileMode.Open, FileAccess.Read);

            BinaryReader binaryReader = new BinaryReader(fileStream);

            imageData = binaryReader.ReadBytes((int)imageFileLength);

            return imageData;
        }

        public static byte[] convertImageToByte(Image img)
        {
            byte[] imageData = null;

            MemoryStream ms = new MemoryStream();
            img.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            imageData = ms.ToArray();

            return imageData;
        }

        /// <summary>
        /// Method for Convert byte Image to Image
        /// </summary>
        /// <param name="bytesImage"></param>
        /// <returns></returns>
        public static Image convertByteToImage(Byte[] bytesImage) {
            MemoryStream ms = new MemoryStream(bytesImage);
            Image image = Image.FromStream(ms);
            return image;
        }

        public static void changeDisableColorComboBox(object sender, DrawItemEventArgs e)
        {
            ComboBox combobox = (ComboBox)sender;
           combobox.DropDownStyle = (combobox.Enabled) ? ComboBoxStyle.DropDown : ComboBoxStyle.DropDownList;

            System.Drawing.Graphics g = e.Graphics;
            Rectangle r = e.Bounds;
            if (e.Index >= 0)
            {
                string label = combobox.Items[e.Index].ToString();
                // This is how we draw a disabled control        
                if (e.State == (DrawItemState.Disabled | DrawItemState.NoAccelerator
                        | DrawItemState.NoFocusRect | DrawItemState.ComboBoxEdit))
                {
                    e.Graphics.FillRectangle(new SolidBrush(Color.Red), r);
                    g.DrawString(label, e.Font, Brushes.Black, r);
                    e.DrawFocusRectangle();
                }

                // This is how we draw the items in an enabled control that aren't in focus        
                else if (e.State == (DrawItemState.NoAccelerator | DrawItemState.NoFocusRect))
                {
                    e.Graphics.FillRectangle(new SolidBrush(Color.White), r);
                    g.DrawString(label, e.Font, Brushes.Black, r);
                    e.DrawFocusRectangle();
                }
                // This is how we draw the focused items        
                else
                {
                    e.Graphics.FillRectangle(new SolidBrush(Color.Blue), r);
                    g.DrawString(label, e.Font, Brushes.White, r);
                    e.DrawFocusRectangle();
                }
            }
            g.Dispose();
        }

        public static void generateNoDataTable(ref DataTable datatable){
            int i = 1;
            foreach (DataRow row in datatable.Rows)
            {
                row[0] = i.ToString();
                i++;
            }
        }
        public static String queryMultipleInsert(String TableName, String[] columns, List<String> values) {

            String column = String.Join(", ",columns);

            String val = String.Join(" UNION ALL ", values.ToArray());

            string query = String.Format("INSERT INTO {0} ({1}) {2}", TableName, column, val);
            
            return query;
        }

        public static DataTable convertDatagridViewtoDataTable(DataGridView datagridView) {
            DataTable datatable = new DataTable();

            foreach (DataGridViewColumn columns in datagridView.Columns)
            {
                if (!columns.GetType().ToString().Contains("DataGridViewImageColumn"))
                {
                    DataColumn column = new DataColumn(columns.Name.ToString());
                    datatable.Columns.Add(column);
                }
            }

            return datatable;
        }

        public static Boolean isAvailableID (List<String> idList, String id){
            id = Utilities.removeSpace(id);
            Boolean result = (idList.Find(s => s == Utilities.removeSpace(id)) == null) ? false : true;

            return result;
        }

        private static StringBuilder words;

        private static readonly string[] m_Units = new string[10] {
            string.Empty,
            "satu", "dua", "tiga", "empat", "lima",
            "enam", "tujuh", "delapan", "sembilan"
        };

        private static readonly string[] m_Thousands = new string[5] {
            string.Empty, " ribu", " juta", " miliar", " triliun"
        };

        private static void AddWords(short number, string suffix)
        {
            // Three digits.
            int[] digits = new int[3];
            for (int index = 2; index >= 0; index--)
            {
                digits[index] = number % 10;
                number /= 10;
            }

            bool isLeadingZero = true;

            /* digits[0] == ratusan
             * digits[1] == puluhan
             * digits[2] == satuan
             */
            if (digits[0] > 0)
            {
                if (digits[0] == 1)
                {
                    words.Append("seratus");
                }
                else
                {
                    words.Append(m_Units[digits[0]]).Append(" ratus ");
                }

                isLeadingZero = false;
            }

            if (digits[1] > 0)
            {
                if (digits[0] > 0)
                {
                    words.Append(" ");
                }

                if (digits[1] == 1)
                {
                    switch (digits[2])
                    {
                        case 0:
                            words.Append("sepuluh");
                            break;
                        case 1:
                            words.Append("sebelas");
                            break;
                        default:
                            words.Append(m_Units[digits[2]]).Append(" belas");
                            break;
                    }

                    words.Append(suffix);
                    return;
                }

                words.Append(m_Units[digits[1]]).Append(" puluh");
                isLeadingZero = false;

                if (digits[2] == 0)
                {
                    words.Append(suffix);
                    return;
                }

                words.Append(" ");
            }

            if (isLeadingZero && (digits[2] == 1) && (suffix == " ribu"))
            {
                words.Append("seribu");
                return;
            }

            words.Append(m_Units[digits[2]]).Append(suffix);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="money"></param>
        /// <returns>String</returns>
        public static String ConvertMoneyToWords(decimal money, string currency) {
            
            words = new StringBuilder(200);

            long number = (long)money;

            if (number == 0L)
            {
                words.Append("Nol ");
            }
            else
            {
                // Hitung jumlah per tiga digit.
                int digits = 0;
                long step = 1L;
                while (step <= number)
                {
                    digits++;
                    step *= 1000L;
                }

                for (int index = (digits - 1); index >= 0; index--)
                {
                    long counter = (long)Math.Pow(1000, index);
                    long temp = number / counter;
                    short remainder = (short)(temp % 1000L);

                    if (remainder > 0)
                    {
                        AddWords(remainder, m_Thousands[index % m_Thousands.Length]);
                        words.Append(" ");
                    }
                }
            }

            words.Append(currency);

            // Apakah ada sen ?
            decimal fraction = money - decimal.Truncate(money);
            if (fraction > 0m)
            {
                short cent = (short)(fraction * 100m);

                words.Append(" ");
                AddWords(cent, string.Empty);
                words.Append(" sen");
            }

            //words.Append(".");

            // Kapital huruf pertama.
            words.Replace(words[0], char.ToUpper(words[0]), 0, 1);

            return words.ToString();
        }

        static readonly string PasswordHash = "P@@Sw0rd";
        static readonly string SaltKey = "S@LT&KEY";
        static readonly string VIKey = "@1B2c3D4e5F6g7H8";
        public static String EncryptPassword(String plainText)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
            var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

            byte[] cipherTextBytes;

            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    cipherTextBytes = memoryStream.ToArray();
                    cryptoStream.Close();
                }
                memoryStream.Close();
            }
            return Convert.ToBase64String(cipherTextBytes);
        }

        public static String DecryptPassword(String cipherText)
        {
            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
            byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

            var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
            var memoryStream = new MemoryStream(cipherTextBytes);
            var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
        }

        public static DataTable RemoveDuplicateRows(DataTable dTable, string colName)
        {
            Hashtable hTable = new Hashtable();
            ArrayList duplicateList = new ArrayList();

            //Add list of all the unique item value to hashtable, which stores combination of key, value pair.
            //And add duplicate item value in arraylist.
            foreach (DataRow drow in dTable.Rows)
            {
                if (hTable.Contains(drow[colName]))
                    duplicateList.Add(drow);
                else
                    hTable.Add(drow[colName], string.Empty);
            }

            //Removing a list of duplicate items from datatable.
            foreach (DataRow dRow in duplicateList)
                dTable.Rows.Remove(dRow);

            //Datatable which contains unique records will be return as output.
            return dTable;
        }
    }  
}
