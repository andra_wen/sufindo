﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Sufindo
{
    class AuthorizeUser
    {
        private static AuthorizeUser instance = null;
        private static readonly object padlock = new object();
        
        private Connection conn = new Connection();

        private DataTable _datatableAuthenticationmenu;
        private DataTable _datatableAuthenticationprice;
        private Dictionary<Object, Object> _userdata;

        public AuthorizeUser() { 
            
        }

        public static AuthorizeUser sharedInstance{
            get {
                lock (padlock) {
                    if (instance == null) {
                        instance = new AuthorizeUser();
                    }
                    return instance;
                }
            }
        }
        
        public Dictionary<Object, Object> userdata {
            get{
                return _userdata;
            }
            set{
                _userdata = value;
            }
        }

        public DataTable authenticationmenu {
            get {
                return _datatableAuthenticationmenu;
            }
            set {
                _datatableAuthenticationmenu = value;
            }
        }

        public DataTable authenticationprice
        {
            get
            {
                return _datatableAuthenticationprice;
            }
            set
            {
                _datatableAuthenticationprice = value;
            }
        }

        public void setAuthenticationMenu()
        {
            if (_userdata.Count != 0)
            {
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@EMPLOYEEID", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));
                _datatableAuthenticationmenu = conn.callProcedureDatatable("SELECT_AUTHENTICATION_USER", param);
            }
        }

        public void setAuthenticationPrice() {
            if (_userdata.Count != 0)
            {
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@EMPLOYEEID", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));
                _datatableAuthenticationprice = conn.callProcedureDatatable("SELECT_AUTHENTICATION_PRICE", param);
            }
        }

        public void adjustAuthenticationPrice(ref DataGridView datagridview) {
            DataTable datatableprice = _datatableAuthenticationprice;
            if (datatableprice != null)
            {
                for (int i = 0; i < datatableprice.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        datagridview.Columns["SELLPRICE"].Visible = datatableprice.Rows[0]["VISIBLE"].ToString().Equals("1") ? true : false;
                    }
                    else if (i == 1)
                    {
                        datagridview.Columns["BUYPRICE"].Visible = datatableprice.Rows[1]["VISIBLE"].ToString().Equals("1") ? true : false;
                    }
                }
            }
        }

        public void adjustAuthenticationMenu(ref MenuStrip menustrip) {
            try
            {
                if (_datatableAuthenticationmenu != null)
                {
                    List<ToolStripMenuItem> parentmenuvisible = new List<ToolStripMenuItem>();
                    List<ToolStripMenuItem> childmenuvisible = new List<ToolStripMenuItem>();

                    //String query = String.Format("SELECT A.MenuID, A.MenuName, C.visible FROM M_MENU A " +
                    //                             "JOIN D_MENU B " +
                    //                             "ON A.MenuID = B.MenuID AND MenuName = SubMenuName " +
                    //                             "JOIN AUTHENTICATION_USER C " +
                    //                             "ON A.MenuID = C.MenuID AND B.SubMenuID = C.SubMenuID " +
                    //                             "WHERE UserID = '{0}'", userdata["EMPLOYEEID"].ToString());
                    //DataTable datatablemenu = conn.openDataTableQuery(query);

                    DataTable datatablemenu = _datatableAuthenticationmenu.Copy();
                    for (int j = 0; j < menustrip.Items.Count; j++)
                    {
                        String MenuName = menustrip.Items[j].Text;
                        DataRow row = datatablemenu.Rows
                                      .Cast<DataRow>()
                                      .Where(r => r["MENUNAME"].ToString().Equals(MenuName))
                                      .FirstOrDefault();
                        if (row != null)
                        {
                            Boolean visible = row["VISIBLE"].ToString().Equals("1") ? true : false;
                            menustrip.Items[j].Visible = visible;
                            if (visible)
                            {
                                parentmenuvisible.Add((ToolStripMenuItem)menustrip.Items[j]);
                            }
                        }
                    }

                    //query = String.Format("SELECT A.SubMenuID, B.SubMenuName, A.visible  FROM AUTHENTICATION_USER A " +
                    //                      "JOIN D_MENU B " +
                    //                      "ON A.SubMenuID = B.SubMenuID AND A.MenuID = B.MenuID " +
                    //                      "WHERE UserID = '{0}' AND A.visible = 1 ORDER BY A.MenuID, A.SubMenuID", userdata["EMPLOYEEID"].ToString());

                    //DataTable datatablemenuchild = conn.openDataTableQuery(query);
                    DataTable datatablemenuchild = _datatableAuthenticationmenu.Copy();

                    foreach (ToolStripMenuItem parent in parentmenuvisible)
                    {
                        foreach (ToolStripMenuItem child in parent.DropDownItems)
                        {
                            String MENUNAME = child.Text;
                            String VISIBLE = "1";
                            DataRow row = datatablemenuchild.Rows
                                          .Cast<DataRow>()
                                          .Where(r => r["MENUNAME"].ToString().Equals(MENUNAME))
                                          .Where(r => r["VISIBLE"].ToString().Equals(VISIBLE))
                                          .FirstOrDefault();
                            child.Visible = row != null ? true : false;
                            if (row != null)
                            {
                                childmenuvisible.Add(child);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private Keys[] keys = { Keys.F2, Keys.F3, Keys.F4, Keys.F5, Keys.F6, Keys.F7, Keys.F8, Keys.F9, Keys.F10, Keys.F11, Keys.F12 };
        private ToolTip toolTip;
        public void adjustAuthenticationMenuAction(ref MenuStrip menustripAction, String MENUNAME) {
            int indexKey = 0;
            toolTip = new ToolTip();
            if (_datatableAuthenticationmenu != null)
            {
                DataRow row = _datatableAuthenticationmenu.Rows
                                                          .Cast<DataRow>()
                                                          .Where(r => r["MENUNAME"].ToString().Equals(MENUNAME))
                                                          .FirstOrDefault();
                if (row != null)
                {

                    foreach (ToolStripMenuItem itemAction in menustripAction.Items)
                    {
                        String columnName = itemAction.Name.Substring(0, itemAction.Name.Length - "toolStripMenuItem".Length).ToUpper();
                        if (_datatableAuthenticationmenu.Columns.Contains(columnName))
                        {
                            bool visible = row[columnName].ToString().Equals("1") ? true : false;
                            itemAction.Visible = visible;
                            if(visible){
                                String tooltiptext = String.Format("{0}({1})", columnName, keys[indexKey].ToString());
                                itemAction.MouseHover += (sender, e) => itemAction_MouseHover(sender, e, tooltiptext);
                                itemAction.MouseLeave += new EventHandler(itemAction_MouseLeave);
                                itemAction.ShortcutKeys = keys[indexKey];
                                indexKey++;
                            }
                        }
                    }
                }
            }
        }

        void itemAction_MouseLeave(object sender, EventArgs e)
        {
            if (toolTip != null)
            {
                toolTip.RemoveAll();
            }
        }

        void itemAction_MouseHover(object sender, EventArgs e, String tooltiptext)
        {
            ToolStripMenuItem itemAction = (ToolStripMenuItem)sender;
            if (itemAction.Visible)
            {
                if (toolTip != null)
                {
                    toolTip.ShowAlways = true;
                    toolTip.Show(tooltiptext, itemAction.Owner, itemAction.Bounds.Location.X, 30);
                }
            }
        }
    }
}
