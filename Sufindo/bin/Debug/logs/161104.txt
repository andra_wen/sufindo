- tambahkan remark di sales return dan sales name di ganti sebagai pembuat invoice (update proc SELECT_DATA_SALES_RETURN)
- fix untuk jumlah karakter itemid dan itemidqty sebanyak 32
- fix untuk menampilkan item di invoice
- fix untuk tidak update tanggal PO ketika melakukan perubahan PO (update proc UPDATE_DATA_H_PURCHASE_ORDER)
- fix untuk report inv audit (update [SELECT_TRANSACTION_ITEM], [SELECT_DATA_STOCKTAG], [SELECT_DATA_STOCKCARD_PERSUPPLIER], [SELECT_DATA_STOCKCARD], [SELECT_TRANSACTION_ITEM])

ALTER PROC [dbo].[SELECT_DATA_SALES_RETURN]
	@SALESRETURNID VARCHAR(20)
AS
BEGIN
	SELECT A.SALESRETURNID, CONVERT(VARCHAR(15), A.SALESRETURNDATE, 103) AS SALESRETURNDATE,
	A.INVOICEID, CONVERT(VARCHAR(15), B.INVOICEDATE, 103) AS INVOICEDATE, C.CUSTOMERNAME,C.ZONE, 
	A.REMARK, REBATE, A.TOTAL, D.ITEMID, E.ITEMNAME, ITEMIDQTY,
	(	
		SELECT ITEMNAME 
		FROM M_ITEM
		WHERE ITEMID = ITEMIDQTY
	) AS ITEMNAMEQTY, 
	D.QUANTITY, E.UOMID, D.PRICE, B.CURRENCYID, B.RATE, F.EMPLOYEENAME
	FROM H_SALES_RETURN A JOIN H_INVOICE B ON A.INVOICEID = B.INVOICEID
	JOIN M_CUSTOMER C ON B.CUSTOMERID = C.CUSTOMERID
	JOIN D_SALES_RETURN D ON A.SALESRETURNID = D.SALESRETURNID
	JOIN M_ITEM E ON D.ITEMID = E.ITEMID
	JOIN M_EMPLOYEE F ON B.CREATEDBY = F.EMPLOYEEID
	WHERE A.SALESRETURNID = @SALESRETURNID
END

ALTER PROC [dbo].[UPDATE_DATA_H_PURCHASE_ORDER]
	@PURCHASEORDERID VARCHAR(20),
	@SUPPLIERID VARCHAR(32),
	@CONTACTPERSONSUPPLIER VARCHAR(100),
	@KURSID VARCHAR(100),
	@RATE MONEY,
	@STATUS VARCHAR(10),
	@REMARK VARCHAR(500),
	@CREATEDBY VARCHAR(100)
AS
BEGIN
	UPDATE H_PURCHASE_ORDER SET
		SUPPLIERID = @SUPPLIERID,
		CONTACTPERSONSUPPLIER = @CONTACTPERSONSUPPLIER,
		KURSID = @KURSID,
		RATE = @RATE,
		STATUS = @STATUS,
		REMARK = @REMARK,
		CREATEDBY = @CREATEDBY,
		UPDATEDATE = CURRENT_TIMESTAMP
	WHERE
		PURCHASEORDERID = @PURCHASEORDERID
	
	SELECT @PURCHASEORDERID AS PURCHASEORDERID
END