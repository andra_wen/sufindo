﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Sufindo
{
    public partial class ItemBuild : Form
    {
        MenuStrip menustripAction;
        DataTable masterItem = null;
        DataTable detailItem = null;
        DataTable dt = null;
        String activity = "";
        int ASSIGNITEMROWUP = -1;
        int ASSIGNITEMROWDOWN = -1;
        Connection conn = null;

        public ItemBuild()
        {
            InitializeComponent();
        }

        public ItemBuild(String called, Form parent, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            if (called.Equals("Extract Out Item"))
            {
                //ItemUpdataGridView
                this.COMBINEQTY2.HeaderText = "RESULT";
                this.COMBINEQTY1.HeaderText = "COMBINE QTY";
            }
            else
            {

            }
            this.menustripAction = menustripAction;
            this.Text = called;
            ActionBuildItembutton.Text = called;
            masterItem = new DataTable() ;
            detailItem = new DataTable();
            dt = new DataTable();
            conn = new Connection();
        }

        private void AddItembutton_Click(object sender, EventArgs e)
        {
            if (ItemUpdataGridView.Rows.Count == 0)
            {
                activity = "Add";
                Itemdatagridview itemdatagridview = null;

                if (itemdatagridview == null)
                {
                    itemdatagridview = new Itemdatagridview("ITEMBUILD", dt, false);
                    itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                    itemdatagridview.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Hanya Bisa Satu Item Saja.","Warning",MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }
        }

        private void Resultbutton_Click(object sender, EventArgs e)
        {
            activity = "Result";
            Itemdatagridview itemdatagridview = null;

            if (itemdatagridview == null)
            {
                itemdatagridview = new Itemdatagridview("ITEMBUILD", dt, true);
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
        }

        private void MasterItemPassingData(DataTable sender) {
            
            /*remove column not use*/
            List<string> columnsRemove = new List<string>();

            foreach (DataColumn column in sender.Columns)
            {
                Boolean isExistingColumn = false;
                foreach (DataGridViewColumn columngrid in (activity.Equals("Add")) ? ItemUpdataGridView.Columns : ItemDowndataGridView.Columns)
                {
                    if (column.ColumnName.Equals(columngrid.DataPropertyName))
                    {
                        isExistingColumn = true;
                        break;
                    }
                }
                if (!isExistingColumn)
                {
                    columnsRemove.Add(column.ColumnName);
                }
            }
            foreach (string columnName in columnsRemove)
            {
                
                sender.Columns.Remove(columnName);
            }
           
            if (dt.Rows.Count == 0) dt = sender.Copy();
            else
            {
                foreach (DataRow row in sender.Rows)
                {
                    dt.Rows.Add(row.ItemArray);
                }
            }

            if (activity.Equals("Add")) {
                if (this.detailItem.Rows.Count == 0) this.detailItem = sender.Copy();
                else
                {
                    foreach (DataRow row in sender.Rows)
                    {
                        this.detailItem.Rows.Add(row.ItemArray);
                        break;
                    }
                }
                Utilities.generateNoDataTable(ref detailItem);
                ItemUpdataGridView.DataSource = this.detailItem;
                string TRANSACTIONITEMTYPE = ActionBuildItembutton.Text.Equals("Build In Item") ? "BuildIn" : "ExtractOut";
                activity = "Result";
                String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY C.ITEMNAME) AS 'NO', A.ITEMID, C.ITEMNAME, A.ITEMIDQTY, " +
	                                         "     ( " +
		                                     "       SELECT ITEMNAME FROM M_ITEM " +
		                                     "       WHERE ITEMID = A.ITEMIDQTY " +
	                                         "     )AS ITEMNAMEQTY, " +
	                                         "     C.BRANDID, C.SPEC, A.QUANTITY AS COMBINEQTY, " +
	                                         "     ( " +
		                                     "       SELECT QUANTITY FROM M_ITEM " +
		                                     "       WHERE ITEMID = A.ITEMIDQTY " +
	                                         "     ) AS ITEMALIASQUANTITY, C.UOMID  FROM L_ITEM A " +
                                             "JOIN D_ITEM B " +
                                             "ON A.BUILDITEMID = B.BUILDITEMID " +
                                             "JOIN M_ITEM C " +
                                             "ON A.ITEMID = C.ITEMID " +
                                             "WHERE B.M_ITEMID = '{0}' AND B.TRANSACTIONITEMTYPE = '{1}' AND " +
                                             "A.BUILDITEMID = ( " +
                                                              "SELECT TOP 1 BUILDITEMID " +
                                                              "FROM D_ITEM ORDER " +
                                                              "BY UPDATEDATE DESC )", this.detailItem.Rows[0]["ITEMID"].ToString(), TRANSACTIONITEMTYPE);
                DataTable r = conn.openDataTableQuery(query);
                if (r.Rows.Count > 0) {
                    masterItem.Clear();
                }
                MasterItemPassingData(r);
            }
            else if (activity.Equals("Result")) {
                if (this.masterItem.Rows.Count == 0) this.masterItem = sender.Copy();
                else
                {
                    foreach (DataRow row in sender.Rows)
                    {
                        this.masterItem.Rows.Add(row.ItemArray);
                    }
                }
                Utilities.generateNoDataTable(ref masterItem);
                ItemDowndataGridView.DataSource = this.masterItem;
            }
        }

        private void ActionBuildItembutton_Click(object sender, EventArgs e)
        {
            if (masterItem.Rows.Count != 0 && detailItem.Rows.Count != 0)
            {
                string TRANSACTIONITEMTYPE = ActionBuildItembutton.Text.Equals("Build In Item") ? "BuildIn" : "ExtractOut";
                int flag = 0;
                foreach (DataRow row in this.detailItem.Rows)
                {
                    flag = 0;
                    if (row["COMBINEQTY"].ToString().Equals("0"))
                    {
                        flag = 1;
                        MessageBox.Show("Please Input Combine Quantity more than 0 on this " + row["ITEMID"].ToString());
                        break;
                    }

                    if (TRANSACTIONITEMTYPE.Equals("ExtractOut"))
                    {
                        Int32 ITEMALIASQUANTITY = Int32.Parse(row["ITEMALIASQUANTITY"].ToString().Equals("") ? "0" : row["ITEMALIASQUANTITY"].ToString());
                        Int32 COMBINEQTY = Int32.Parse(row["COMBINEQTY"].ToString().Equals("") ? "0" : row["COMBINEQTY"].ToString());

                        if (COMBINEQTY > ITEMALIASQUANTITY) {
                            MessageBox.Show(String.Format("Please Input this {0} with Combine Quantity less than Quantity ", row["ITEMID"].ToString()));
                            flag = 1;
                            break;
                        }
                    }
                }

                if (flag == 0)
                {
                    foreach (DataRow row in this.masterItem.Rows)
                    {
                        flag = 0;
                        if (row["COMBINEQTY"].ToString().Equals("0"))
                        {
                            MessageBox.Show("Please Input Combine Quantity more than 0 on this " + row["ITEMID"].ToString());
                            flag = 1;
                            break;
                        }

                       
                        if(TRANSACTIONITEMTYPE.Equals("BuildIn")){
                            Int32 ITEMALIASQUANTITY = Int32.Parse(row["ITEMALIASQUANTITY"].ToString().Equals("") ? "0" : row["ITEMALIASQUANTITY"].ToString());
                            Int32 COMBINEQTY = Int32.Parse(row["COMBINEQTY"].ToString().Equals("") ? "0" : row["COMBINEQTY"].ToString());

                            if(COMBINEQTY > ITEMALIASQUANTITY){
                                MessageBox.Show(String.Format("Please Input this {0} with Combine Quantity less than Quantity ", row["ITEMID"].ToString()));
                                flag = 1;
                                break;
                            }
                        }
                    }
                }

                if (flag == 0)
                {
                    List<SqlParameter> sqlParam = new List<SqlParameter>();
                    String ITEMIDQTY = detailItem.Rows[0]["ITEMIDQTY"].ToString().Equals("") ?
                        detailItem.Rows[0]["ITEMID"].ToString() : detailItem.Rows[0]["ITEMIDQTY"].ToString();

                    sqlParam.Add(new SqlParameter("@M_ITEMID", detailItem.Rows[0]["ITEMID"].ToString()));
                    sqlParam.Add(new SqlParameter("@M_ITEMIDQTY", detailItem.Rows[0]["ITEMIDQTY"].ToString()));
                    sqlParam.Add(new SqlParameter("@M_QUANTITY", detailItem.Rows[0]["COMBINEQTY"].ToString()));
                    sqlParam.Add(new SqlParameter("@TRANSACTIONITEMTYPE", TRANSACTIONITEMTYPE));
                    sqlParam.Add(new SqlParameter("@STATUS", Status.ACTIVE));
                    sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));

                    DataTable datatable = conn.callProcedureDatatable("INSERT_DATA_D_ITEM", sqlParam);

                    if (datatable.Rows.Count != 0)
                    {
                        List<string> values = new List<string>();
                        foreach (DataRow row in this.masterItem.Rows)
                        {
                            ITEMIDQTY = row["ITEMIDQTY"].ToString().Equals("") ?
                                   row["ITEMID"].ToString() : row["ITEMIDQTY"].ToString();
                            String value = String.Format("SELECT '{0}', '{1}', '{2}', {3}", datatable.Rows[0]["BUILDITEMID"].ToString(),
                                                                                            row["ITEMID"].ToString(),
                                                                                            ITEMIDQTY, row["COMBINEQTY"].ToString());
                            values.Add(value);
                        }
                        String[] columns = { "BUILDITEMID", "ITEMID", "ITEMIDQTY", "QUANTITY" };
                        if (conn.openReaderQuery(Utilities.queryMultipleInsert("L_ITEM", columns, values)))
                        {
                            MessageBox.Show(String.Format("{0} Succcess", ActionBuildItembutton.Text));
                            dt.Clear();
                            masterItem.Clear();
                            detailItem.Clear();
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Master Item and Detail Item Must have item minimal 1 item");
            }
        }

        private void ItemUpdataGridView_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            for (int i = 0; i < detailItem.Rows.Count; i++)
            {
                if (detailItem.Rows[i].RowState == DataRowState.Deleted)
                {
                    detailItem.Rows.RemoveAt(i);
                }
            }
            Utilities.generateNoDataTable(ref detailItem);
        }

        private void ItemDowndataGridView_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            for (int i = 0; i < masterItem.Rows.Count; i++)
            {
                if (masterItem.Rows[i].RowState == DataRowState.Deleted)
                {
                    masterItem.Rows.RemoveAt(i);
                }
            }
            Utilities.generateNoDataTable(ref masterItem);
        }

        private void ItemUpdataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
        }

        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (ItemUpdataGridView.Rows.Count > 0)
            {
                int currColumn1 = ItemUpdataGridView.CurrentCell.ColumnIndex;
                int currRow1 = ItemUpdataGridView.CurrentCell.RowIndex;
                if (ItemUpdataGridView.Columns[currColumn1].Name.Equals("COMBINEQTY1"))
                {
                    e.Handled = Utilities.onlyNumberTextBox(sender, (KeyPressEventArgs)e);
                }
            }
            if (ItemDowndataGridView.Rows.Count > 0)
            {
                int currColumn2 = ItemDowndataGridView.CurrentCell.ColumnIndex;
                int currRow2 = ItemDowndataGridView.CurrentCell.RowIndex;
                if (ItemDowndataGridView.Columns[currColumn2].Name.Equals("QUANTITY2"))
                {
                    e.Handled = Utilities.onlyNumberTextBox(sender, (KeyPressEventArgs)e);
                }
            }
        }

        private void ItemDowndataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
        }

        private void ItemUpdataGridView_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (ItemUpdataGridView.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in ItemUpdataGridView.Rows)
                {
                    if (this.Text.Equals("Build In"))
                    { 
                    
                    }
                    else if (this.Text.Equals("Extract Out"))
                    {
                        if (!(long.Parse(row.Cells["COMBINEQTY1"].Value.ToString()) <= long.Parse(row.Cells["QUANTITY1"].Value.ToString())))
                        {
                            row.Cells["COMBINEQTY1"].Value = row.Cells["QUANTITY1"].Value;
                        }
                    }
                }
            }
        }

        private void ItemDowndataGridView_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (ItemUpdataGridView.Rows.Count > 0)
                {
                    foreach (DataGridViewRow row in ItemDowndataGridView.Rows)
                    {
                        if (this.Text.Equals("Build In"))
                        {
                            if (!(long.Parse(row.Cells["COMBINEQTY2"].Value.ToString()) <= long.Parse(row.Cells["QUANTITY2"].Value.ToString())))
                            {
                                row.Cells["COMBINEQTY2"].Value = row.Cells["QUANTITY2"].Value;
                            }
                            //if (long.Parse(row.Cells["QUANTITY2"].Value.ToString()) == 0)
                            //{
                            //    row.Cells["QUANTITY2"].Value = "1";
                            //}
                        }
                        else if (this.Text.Equals("Extract Out"))
                        {

                        }
                    }
                }
            }
            catch (Exception )
            {
            }
           
        }

        private void ItemUpdataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (ItemUpdataGridView.Columns[e.ColumnIndex].Name.Equals("Del1"))
                {
                    //MessageBox.Show(ItemUpdataGridView.Rows[e.RowIndex].Cells["ITEMID1"].Value.ToString());
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["ITEMID"].ToString().Equals(ItemUpdataGridView.Rows[e.RowIndex].Cells["ITEMID1"].Value.ToString()))
                        {
                            dt.Rows.RemoveAt(i);
                            break;
                        }
                    }
                    this.detailItem.Rows.RemoveAt(e.RowIndex);
                    Utilities.generateNoDataTable(ref detailItem);
                    ItemUpdataGridView.DataSource = detailItem;
                    
                }
                else if ((ItemUpdataGridView.Columns[e.ColumnIndex].DataPropertyName.Equals("ITEMIDQTY") || ItemUpdataGridView.Columns[e.ColumnIndex].DataPropertyName.Equals("ITEMNAMEQTY")))
                {
                    ASSIGNITEMROWUP = e.RowIndex;
                    String ITEMID = ItemUpdataGridView.Rows[e.RowIndex].Cells["ITEMID1"].Value.ToString();
                    Itemdatagridview itemdatagridview = null;
                    if (itemdatagridview == null)
                    {
                        itemdatagridview = new Itemdatagridview("ASSIGNITEMPURCHASERECEIPT", ITEMID);
                        itemdatagridview.assignItemQuantityPassingData = new Itemdatagridview.AssignItemQuantityPassingData(AssignItemQuantityPassingData);
                        itemdatagridview.ShowDialog();
                    }
                    else if (itemdatagridview.IsDisposed)
                    {
                        itemdatagridview = new Itemdatagridview("ASSIGNITEMPURCHASERECEIPT", ITEMID);
                        itemdatagridview.assignItemQuantityPassingData = new Itemdatagridview.AssignItemQuantityPassingData(AssignItemQuantityPassingData);
                        itemdatagridview.ShowDialog();
                    }


                }
            }
        }

        private void ItemDowndataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (ItemDowndataGridView.Columns[e.ColumnIndex].Name.Equals("Del2"))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["ITEMID"].ToString().Equals(ItemDowndataGridView.Rows[e.RowIndex].Cells["ITEMID2"].Value.ToString()))
                        {
                            dt.Rows.RemoveAt(i);
                            break;
                        }
                    }
                    this.masterItem.Rows.RemoveAt(e.RowIndex);
                    Utilities.generateNoDataTable(ref masterItem);
                    ItemDowndataGridView.DataSource = masterItem;
                }
                else if ((ItemDowndataGridView.Columns[e.ColumnIndex].DataPropertyName.Equals("ITEMIDQTY") || ItemDowndataGridView.Columns[e.ColumnIndex].DataPropertyName.Equals("ITEMNAMEQTY")))
                {
                    ASSIGNITEMROWDOWN = e.RowIndex;
                    String ITEMID = ItemDowndataGridView.Rows[e.RowIndex].Cells["ITEMID2"].Value.ToString();
                    Itemdatagridview itemdatagridview = null;
                    if (itemdatagridview == null)
                    {
                        itemdatagridview = new Itemdatagridview("ASSIGNITEMPURCHASERECEIPT", ITEMID);
                        itemdatagridview.assignItemQuantityPassingData = new Itemdatagridview.AssignItemQuantityPassingData(AssignItemQuantityPassingData);
                        itemdatagridview.ShowDialog();
                    }
                    else if (itemdatagridview.IsDisposed)
                    {
                        itemdatagridview = new Itemdatagridview("ASSIGNITEMPURCHASERECEIPT", ITEMID);
                        itemdatagridview.assignItemQuantityPassingData = new Itemdatagridview.AssignItemQuantityPassingData(AssignItemQuantityPassingData);
                        itemdatagridview.ShowDialog();
                    }


                }
            }
        }

        private void AssignItemQuantityPassingData(DataTable sender)
        {
            if (ASSIGNITEMROWUP != -1) {
                ItemUpdataGridView.Rows[ASSIGNITEMROWUP].Cells["ITEMIDQTY1"].Value = sender.Rows[0]["ITEMID"].ToString();
                ItemUpdataGridView.Rows[ASSIGNITEMROWUP].Cells["ITEMNAMEQTY1"].Value = sender.Rows[0]["ITEMNAME"].ToString();
                ItemUpdataGridView.Rows[ASSIGNITEMROWUP].Cells["QUANTITY1"].Value = sender.Rows[0]["QUANTITY"].ToString();
                ASSIGNITEMROWUP = -1;
            }
            else if (ASSIGNITEMROWDOWN != -1) {
                ItemDowndataGridView.Rows[ASSIGNITEMROWDOWN].Cells["ITEMIDQTY2"].Value = sender.Rows[0]["ITEMID"].ToString();
                ItemDowndataGridView.Rows[ASSIGNITEMROWDOWN].Cells["ITEMNAMEQTY2"].Value = sender.Rows[0]["ITEMNAME"].ToString();
                ItemDowndataGridView.Rows[ASSIGNITEMROWDOWN].Cells["QUANTITY2"].Value = sender.Rows[0]["QUANTITY"].ToString();
                ASSIGNITEMROWDOWN = -1;
            }
            
        }

        private void ItemBuild_Activated(object sender, EventArgs e)
        {
            Main.ACTIVEFORM = this;
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
        }

        private void ItemBuild_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (conn != null) conn.CloseConnection();

            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }

            Main.ACTIVEFORM = null;
        }
    }
}
