﻿namespace Sufindo
{
    partial class Adjustment_Spoil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.Typelabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this._typelabel = new System.Windows.Forms.Label();
            this.qtytextBox = new System.Windows.Forms.TextBox();
            this.NotextBox = new System.Windows.Forms.TextBox();
            this.TypecomboBox = new System.Windows.Forms.ComboBox();
            this.totextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.resulttextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.PartNotextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.StatustextBox = new System.Windows.Forms.TextBox();
            this.SearchPartNObutton = new System.Windows.Forms.Button();
            this.PartnametextBox = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.label12 = new System.Windows.Forms.Label();
            this.Spesifikasipanel = new System.Windows.Forms.Panel();
            this.RemarkrichTextBox = new System.Windows.Forms.RichTextBox();
            this.AddItembutton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.CreateBytextBox = new System.Windows.Forms.TextBox();
            this.SearchNobutton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.DatetextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.Spesifikasipanel.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 12;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.Typelabel, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.label6, 0, 3);
            this.tableLayoutPanel.Controls.Add(this._typelabel, 2, 3);
            this.tableLayoutPanel.Controls.Add(this.qtytextBox, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.NotextBox, 2, 1);
            this.tableLayoutPanel.Controls.Add(this.TypecomboBox, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.totextBox, 3, 3);
            this.tableLayoutPanel.Controls.Add(this.label11, 4, 3);
            this.tableLayoutPanel.Controls.Add(this.resulttextBox, 5, 3);
            this.tableLayoutPanel.Controls.Add(this.label7, 7, 2);
            this.tableLayoutPanel.Controls.Add(this.PartNotextBox, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.label3, 10, 1);
            this.tableLayoutPanel.Controls.Add(this.StatustextBox, 11, 1);
            this.tableLayoutPanel.Controls.Add(this.SearchPartNObutton, 6, 2);
            this.tableLayoutPanel.Controls.Add(this.PartnametextBox, 9, 2);
            this.tableLayoutPanel.Controls.Add(this.panel1, 0, 5);
            this.tableLayoutPanel.Controls.Add(this.label12, 0, 6);
            this.tableLayoutPanel.Controls.Add(this.Spesifikasipanel, 1, 6);
            this.tableLayoutPanel.Controls.Add(this.AddItembutton, 0, 4);
            this.tableLayoutPanel.Controls.Add(this.label4, 7, 0);
            this.tableLayoutPanel.Controls.Add(this.CreateBytextBox, 9, 0);
            this.tableLayoutPanel.Controls.Add(this.SearchNobutton, 6, 1);
            this.tableLayoutPanel.Controls.Add(this.label2, 7, 1);
            this.tableLayoutPanel.Controls.Add(this.DatetextBox, 8, 1);
            this.tableLayoutPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.tableLayoutPanel.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 7;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.Size = new System.Drawing.Size(655, 486);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Type";
            // 
            // Typelabel
            // 
            this.Typelabel.AutoSize = true;
            this.tableLayoutPanel.SetColumnSpan(this.Typelabel, 2);
            this.Typelabel.Location = new System.Drawing.Point(3, 27);
            this.Typelabel.Name = "Typelabel";
            this.Typelabel.Size = new System.Drawing.Size(76, 13);
            this.Typelabel.TabIndex = 7;
            this.Typelabel.Text = "Adjustment No";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Part No";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 89);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Qty";
            // 
            // _typelabel
            // 
            this._typelabel.AutoSize = true;
            this._typelabel.Location = new System.Drawing.Point(98, 89);
            this._typelabel.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this._typelabel.Name = "_typelabel";
            this._typelabel.Size = new System.Drawing.Size(20, 13);
            this._typelabel.TabIndex = 12;
            this._typelabel.Text = "To";
            // 
            // qtytextBox
            // 
            this.qtytextBox.BackColor = System.Drawing.Color.White;
            this.qtytextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.qtytextBox.Enabled = false;
            this.qtytextBox.Location = new System.Drawing.Point(53, 86);
            this.qtytextBox.Name = "qtytextBox";
            this.qtytextBox.Size = new System.Drawing.Size(39, 20);
            this.qtytextBox.TabIndex = 9;
            this.qtytextBox.TextChanged += new System.EventHandler(this.qtytextBox_TextChanged);
            // 
            // NotextBox
            // 
            this.NotextBox.BackColor = System.Drawing.Color.White;
            this.NotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.NotextBox, 4);
            this.NotextBox.Enabled = false;
            this.NotextBox.Location = new System.Drawing.Point(98, 30);
            this.NotextBox.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.NotextBox.Name = "NotextBox";
            this.NotextBox.Size = new System.Drawing.Size(145, 20);
            this.NotextBox.TabIndex = 2;
            this.NotextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NotextBox_KeyDown);
            this.NotextBox.Leave += new System.EventHandler(this.NotextBox_Leave);
            // 
            // TypecomboBox
            // 
            this.tableLayoutPanel.SetColumnSpan(this.TypecomboBox, 5);
            this.TypecomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TypecomboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.TypecomboBox.FormattingEnabled = true;
            this.TypecomboBox.Items.AddRange(new object[] {
            "Adjustment",
            "Spoil"});
            this.TypecomboBox.Location = new System.Drawing.Point(53, 3);
            this.TypecomboBox.Name = "TypecomboBox";
            this.TypecomboBox.Size = new System.Drawing.Size(190, 21);
            this.TypecomboBox.TabIndex = 1;
            this.TypecomboBox.SelectedIndexChanged += new System.EventHandler(this.TypecomboBox_SelectedIndexChanged);
            // 
            // totextBox
            // 
            this.totextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.totextBox.Location = new System.Drawing.Point(124, 86);
            this.totextBox.Name = "totextBox";
            this.totextBox.Size = new System.Drawing.Size(39, 20);
            this.totextBox.TabIndex = 10;
            this.totextBox.TextChanged += new System.EventHandler(this.totextBox_TextChanged);
            this.totextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.totextBox_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(169, 89);
            this.label11.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "=";
            // 
            // resulttextBox
            // 
            this.resulttextBox.BackColor = System.Drawing.Color.White;
            this.resulttextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.resulttextBox.Enabled = false;
            this.resulttextBox.Location = new System.Drawing.Point(188, 86);
            this.resulttextBox.Name = "resulttextBox";
            this.resulttextBox.Size = new System.Drawing.Size(39, 20);
            this.resulttextBox.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.tableLayoutPanel.SetColumnSpan(this.label7, 2);
            this.label7.Location = new System.Drawing.Point(280, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Part Name";
            // 
            // PartNotextBox
            // 
            this.PartNotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.PartNotextBox, 5);
            this.PartNotextBox.Location = new System.Drawing.Point(53, 58);
            this.PartNotextBox.Name = "PartNotextBox";
            this.PartNotextBox.Size = new System.Drawing.Size(182, 20);
            this.PartNotextBox.TabIndex = 6;
            this.PartNotextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PartNotextBox_KeyDown);
            this.PartNotextBox.Leave += new System.EventHandler(this.PartNotextBox_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(477, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Status";
            // 
            // StatustextBox
            // 
            this.StatustextBox.BackColor = System.Drawing.Color.White;
            this.StatustextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.StatustextBox.Enabled = false;
            this.StatustextBox.Location = new System.Drawing.Point(520, 30);
            this.StatustextBox.Name = "StatustextBox";
            this.StatustextBox.Size = new System.Drawing.Size(108, 20);
            this.StatustextBox.TabIndex = 5;
            // 
            // SearchPartNObutton
            // 
            this.SearchPartNObutton.BackColor = System.Drawing.Color.Transparent;
            this.SearchPartNObutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.SearchPartNObutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SearchPartNObutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.SearchPartNObutton.FlatAppearance.BorderSize = 0;
            this.SearchPartNObutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchPartNObutton.ForeColor = System.Drawing.Color.Transparent;
            this.SearchPartNObutton.Location = new System.Drawing.Point(249, 56);
            this.SearchPartNObutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.SearchPartNObutton.Name = "SearchPartNObutton";
            this.SearchPartNObutton.Size = new System.Drawing.Size(25, 24);
            this.SearchPartNObutton.TabIndex = 7;
            this.SearchPartNObutton.Tag = "";
            this.SearchPartNObutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.SearchPartNObutton.UseVisualStyleBackColor = false;
            this.SearchPartNObutton.Click += new System.EventHandler(this.SearchPartNObutton_Click);
            // 
            // PartnametextBox
            // 
            this.PartnametextBox.BackColor = System.Drawing.Color.White;
            this.PartnametextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.PartnametextBox, 2);
            this.PartnametextBox.Enabled = false;
            this.PartnametextBox.Location = new System.Drawing.Point(346, 58);
            this.PartnametextBox.Name = "PartnametextBox";
            this.PartnametextBox.Size = new System.Drawing.Size(168, 20);
            this.PartnametextBox.TabIndex = 8;
            // 
            // panel1
            // 
            this.tableLayoutPanel.SetColumnSpan(this.panel1, 15);
            this.panel1.Controls.Add(this.dataGridView);
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(3, 144);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(625, 204);
            this.panel1.TabIndex = 42;
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(0, 0);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.Size = new System.Drawing.Size(625, 204);
            this.dataGridView.TabIndex = 13;
            this.dataGridView.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dataGridView_UserDeletedRow);
            this.dataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellClick);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 351);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 13);
            this.label12.TabIndex = 43;
            this.label12.Text = "Remark";
            // 
            // Spesifikasipanel
            // 
            this.Spesifikasipanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.Spesifikasipanel, 8);
            this.Spesifikasipanel.Controls.Add(this.RemarkrichTextBox);
            this.Spesifikasipanel.Location = new System.Drawing.Point(53, 354);
            this.Spesifikasipanel.Name = "Spesifikasipanel";
            this.Spesifikasipanel.Size = new System.Drawing.Size(287, 126);
            this.Spesifikasipanel.TabIndex = 44;
            // 
            // RemarkrichTextBox
            // 
            this.RemarkrichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RemarkrichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RemarkrichTextBox.Location = new System.Drawing.Point(0, 0);
            this.RemarkrichTextBox.Name = "RemarkrichTextBox";
            this.RemarkrichTextBox.Size = new System.Drawing.Size(285, 124);
            this.RemarkrichTextBox.TabIndex = 14;
            this.RemarkrichTextBox.TabStop = false;
            this.RemarkrichTextBox.Text = "";
            // 
            // AddItembutton
            // 
            this.AddItembutton.BackColor = System.Drawing.Color.Transparent;
            this.AddItembutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tableLayoutPanel.SetColumnSpan(this.AddItembutton, 2);
            this.AddItembutton.FlatAppearance.BorderSize = 0;
            this.AddItembutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddItembutton.ForeColor = System.Drawing.Color.Transparent;
            this.AddItembutton.Image = global::Sufindo.Properties.Resources.plus;
            this.AddItembutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.AddItembutton.Location = new System.Drawing.Point(0, 109);
            this.AddItembutton.Margin = new System.Windows.Forms.Padding(0);
            this.AddItembutton.Name = "AddItembutton";
            this.AddItembutton.Size = new System.Drawing.Size(92, 32);
            this.AddItembutton.TabIndex = 12;
            this.AddItembutton.Text = "Add Part";
            this.AddItembutton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.AddItembutton.UseVisualStyleBackColor = false;
            this.AddItembutton.Click += new System.EventHandler(this.AddItembutton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.tableLayoutPanel.SetColumnSpan(this.label4, 2);
            this.label4.Location = new System.Drawing.Point(280, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 45;
            this.label4.Text = "Created By";
            // 
            // CreateBytextBox
            // 
            this.CreateBytextBox.BackColor = System.Drawing.Color.White;
            this.CreateBytextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.CreateBytextBox, 2);
            this.CreateBytextBox.Enabled = false;
            this.CreateBytextBox.Location = new System.Drawing.Point(346, 3);
            this.CreateBytextBox.Name = "CreateBytextBox";
            this.CreateBytextBox.Size = new System.Drawing.Size(168, 20);
            this.CreateBytextBox.TabIndex = 46;
            // 
            // SearchNobutton
            // 
            this.SearchNobutton.BackColor = System.Drawing.Color.Transparent;
            this.SearchNobutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.SearchNobutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SearchNobutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.SearchNobutton.FlatAppearance.BorderSize = 0;
            this.SearchNobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchNobutton.ForeColor = System.Drawing.Color.Transparent;
            this.SearchNobutton.Location = new System.Drawing.Point(249, 28);
            this.SearchNobutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.SearchNobutton.Name = "SearchNobutton";
            this.SearchNobutton.Size = new System.Drawing.Size(24, 24);
            this.SearchNobutton.TabIndex = 3;
            this.SearchNobutton.Tag = "";
            this.SearchNobutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.SearchNobutton.UseVisualStyleBackColor = false;
            this.SearchNobutton.Visible = false;
            this.SearchNobutton.Click += new System.EventHandler(this.SearchNobutton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(280, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Date";
            // 
            // DatetextBox
            // 
            this.DatetextBox.BackColor = System.Drawing.Color.White;
            this.DatetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.DatetextBox, 2);
            this.DatetextBox.Enabled = false;
            this.DatetextBox.Location = new System.Drawing.Point(316, 30);
            this.DatetextBox.Name = "DatetextBox";
            this.DatetextBox.Size = new System.Drawing.Size(128, 20);
            this.DatetextBox.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 83);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(1, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Qty";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 7;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 41.92308F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 58.07692F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 47F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 135F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 213F));
            this.tableLayoutPanel2.Controls.Add(this.label9, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(200, 100);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(1, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Type";
            // 
            // textBox4
            // 
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox4.Location = new System.Drawing.Point(-196, 30);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(1, 20);
            this.textBox4.TabIndex = 3;
            // 
            // Adjustment_Spoil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(658, 492);
            this.Controls.Add(this.tableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Adjustment_Spoil";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Adjustment/Spoil";
            this.Activated += new System.EventHandler(this.Adjustment_Spoil_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Adjustment_Spoil_FormClosed);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.Spesifikasipanel.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox NotextBox;
        private System.Windows.Forms.TextBox PartNotextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Typelabel;
        private System.Windows.Forms.ComboBox TypecomboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label _typelabel;
        private System.Windows.Forms.TextBox qtytextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox totextBox;
        private System.Windows.Forms.TextBox DatetextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox resulttextBox;
        private System.Windows.Forms.TextBox StatustextBox;
        private System.Windows.Forms.Button SearchPartNObutton;
        private System.Windows.Forms.Button SearchNobutton;
        private System.Windows.Forms.TextBox PartnametextBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel Spesifikasipanel;
        private System.Windows.Forms.RichTextBox RemarkrichTextBox;
        private System.Windows.Forms.Button AddItembutton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox CreateBytextBox;
    }
}