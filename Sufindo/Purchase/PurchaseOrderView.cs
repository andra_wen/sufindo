﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Sufindo.Purchase
{
    public partial class PurchaseOrderView : Form
    {
        private delegate void fillAutoCompleteTextBox();

        MenuStrip menustripAction;
        PurchaseOrderdatagridview purchaseorderdatagridview;
        Connection connection;
        List<String> PurchaseOrderNo;
        List<String> PartNo;
        List<Control> controlTextBox;
        DataTable datatable;

        String ITEMID = "";

        Itemdatagridview itemdatagridview;

        public PurchaseOrderView(String ITEMID) {
            InitializeComponent();
            itemdatagridview = null;
            purchaseorderdatagridview = null;
            if (connection == null)
            {
                connection = new Connection();
            }
            controlTextBox = new List<Control>();
            foreach (Control control in this.tableLayoutPanel.Controls)
            {
                if (control is TextBox)
                {
                    controlTextBox.Add(control);
                }
                else if (control is Panel)
                {
                    foreach (Control controlPanel in control.Controls)
                    {
                        if (controlPanel is RichTextBox)
                        {
                            controlTextBox.Add(controlPanel);
                        }
                    }
                }
                else if (control is ComboBox)
                {
                    controlTextBox.Add(control);
                }
            }
            datatable = new DataTable();
            this.ITEMID = ITEMID;
            
        }

        public PurchaseOrderView(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            itemdatagridview = null;
            purchaseorderdatagridview = null;
            this.menustripAction = menustripAction;
            if (connection == null)
            {
                connection = new Connection();
            }
            controlTextBox = new List<Control>();
            foreach (Control control in this.tableLayoutPanel.Controls)
            {
                if (control is TextBox)
                {
                    controlTextBox.Add(control);
                }
                else if (control is Panel)
                {
                    foreach (Control controlPanel in control.Controls)
                    {
                        if (controlPanel is RichTextBox)
                        {
                            controlTextBox.Add(controlPanel);
                        }
                    }
                }
                else if (control is ComboBox)
                {
                    controlTextBox.Add(control);
                }
            }
            datatable = new DataTable();
            
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e){

        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e){
           
            if (PurchaseOrderNo == null) PurchaseOrderNo = new List<string>();
            if (PartNo == null) PartNo = new List<string>();
            SqlDataReader sqldataReader = connection.sqlDataReaders(String.Format("SELECT PURCHASEORDERID FROM H_PURCHASE_ORDER WHERE [STATUS] = '{0}'", Status.OPEN));

            while (sqldataReader.Read()) PurchaseOrderNo.Add(sqldataReader.GetString(0));

            sqldataReader = connection.sqlDataReaders(String.Format("SELECT ITEMID FROM M_ITEM WHERE [STATUS] = '{0}'", Status.ACTIVE));

            while (sqldataReader.Read()) PartNo.Add(sqldataReader.GetString(0));

            if (IsHandleCreated) BeginInvoke(new fillAutoCompleteTextBox(this.fillAutoComplete));


        }

        private void fillAutoComplete(){

            PONotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            PONotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            PONotextBox.AutoCompleteCustomSource.AddRange(PurchaseOrderNo.ToArray());
            PONotextBox.Text = "";
            
            PartNOtextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            PartNOtextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            PartNOtextBox.AutoCompleteCustomSource.AddRange(PartNo.ToArray());
            PartNOtextBox.Text = this.ITEMID.Equals("") ? "" : this.ITEMID;
            PONotextBox.Focus();
            PartNOtextBox.Focus();
        }

        private void PartIDtextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                PartNOtextBox.Text = PartNOtextBox.Text.ToUpper();
                Boolean isExistingPartNo = false;
                foreach (String item in PartNo)
                {
                    if (item.ToUpper().Equals(PartNOtextBox.Text.ToUpper()))
                    {
                        Utilities.clearAllField(ref controlTextBox);
                        PartNOtextBox.Text = item.ToUpper();
                        isExistingPartNo = true;
                        addColumnForPurchaseOrder();
                        String query = String.Format("SELECT ITEMID,ITEMNAME " +
                                                     "FROM M_ITEM " +
                                                     "WHERE ITEMID = '{0}' ", PartNOtextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        MasterItemPassingData(datatable);

                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    if (PONotextBox.Text.Equals(""))
                    {
                        Utilities.clearAllField(ref controlTextBox);
                        HistorydataGridView.Columns.Clear();
                    }
                    else PartNOtextBox.Text = "";
                }
            }
       }

        private void PartIDtextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                PartNOtextBox.Text = PartNOtextBox.Text.ToUpper();
                Boolean isExistingPartNo = false;
                foreach (String item in PartNo)
                {
                    if (item.ToUpper().Equals(PartNOtextBox.Text.ToUpper()))
                    {
                        Utilities.clearAllField(ref controlTextBox);
                        PartNOtextBox.Text = item.ToUpper();
                        isExistingPartNo = true;
                        if (!this.ITEMID.Equals("")) addColumnForPurchaseOrder();
                        String query = String.Format("SELECT ITEMID,ITEMNAME " +
                                                     "FROM M_ITEM " +
                                                     "WHERE ITEMID = '{0}' ", PartNOtextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        MasterItemPassingData(datatable);
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    if (PONotextBox.Text.Equals(""))
                    {
                        Utilities.clearAllField(ref controlTextBox);
                        HistorydataGridView.Columns.Clear();
                        this.datatable.Clear();
                    }
                    else PartNOtextBox.Text = "";
                }
            }
            catch (Exception ex)
            {
                
            }
           
        }

        private void MasterItemPassingData(DataTable datatable){
            PartNOtextBox.Text = datatable.Rows[0]["ITEMID"].ToString();
            PartNametextBox.Text = datatable.Rows[0]["ITEMNAME"].ToString();
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.PURCHASEORDERID) AS 'NO', B.ITEMIDQTY, B.ITEMID, A.PURCHASEORDERID, " +
                                         "CONVERT(VARCHAR(10), PURCHASEORDERDATE, 103) AS [PURCHASEORDERDATE], " +
                                         "SUPPLIERNAME, CONTACTPERSONSUPPLIER, B.QUANTITY, B.PRICE AS PRICE, (B.QUANTITY * B.PRICE) AS AMOUNT, REMARK " +
                                         "FROM H_PURCHASE_ORDER A " +
                                         "JOIN D_PURCHASE_ORDER B " +
                                         "ON A.PURCHASEORDERID = B.PURCHASEORDERID " +
                                         "JOIN M_SUPPLIER C " +
                                         "ON A.SUPPLIERID = C.SUPPLIERID " +
                                         "JOIN M_ITEM D " +
                                         "ON B.ITEMID = D.ITEMID " +
                                         "WHERE B.ITEMIDQTY = '{0}' " +
                                         "AND A.STATUS = '{1}'", PartNOtextBox.Text, Status.OPEN);
            DataTable dt = connection.openDataTableQuery(query);
            HistorydataGridView.DataSource = dt;
        }

        //ERROR
        private void PurchaseOrder_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                if (connection != null) connection.CloseConnection();
                if (Main.LISTFORM.Count != 0)
                {
                    Form f = Main.LISTFORM.First(s => s.GetType().Equals(this.GetType()));
                    if (f != null)
                    {
                        Main.LISTFORM.Remove(this);
                        if (Main.LISTFORM.Count == 0)
                        {
                            Utilities.showMenuStripAction(menustripAction.Items, false);
                        }
                        Main.ACTIVEFORM = null;
                    }
                }
                this.Dispose();
            }
            catch (Exception )
            {
                
            }
        }

        private void PONotextBox_Leave(object sender, EventArgs e)
        {
            Boolean isExistingPartNo = false;
            foreach (String item in PurchaseOrderNo)
            {
                if (item.ToUpper().Equals(PONotextBox.Text.ToUpper()))
                {
                    Utilities.clearAllField(ref controlTextBox);
                    PONotextBox.Text = item.ToUpper();
                    addColumnForItem();
                    isExistingPartNo = true;
                    PONotextBox.Text = PONotextBox.Text.ToUpper();
                    String query = String.Format("SELECT PURCHASEORDERID, CONVERT(VARCHAR(10), PURCHASEORDERDATE, 103) AS [PURCHASEORDERDATE], SUPPLIERNAME, " +
                                                 "CONTACTPERSONSUPPLIER, REMARK " +
                                                 "FROM H_PURCHASE_ORDER A " +
                                                 "JOIN M_SUPPLIER B " +
                                                 "ON A.SUPPLIERID = B.SUPPLIERID " +
                                                 "WHERE PURCHASEORDERID = '{0}'", PONotextBox.Text);

                    DataTable datatable = connection.openDataTableQuery(query);
                    PurchaseOrderPassingData(datatable);

                    break;
                }
            }
            if (!isExistingPartNo)
            {
                if (PartNOtextBox.Text.Equals(""))
                {
                    Utilities.clearAllField(ref controlTextBox);
                    HistorydataGridView.Columns.Clear();
                    this.datatable.Clear();
                }
                else PONotextBox.Text = "";
                
               
            }
        }

        private void PONotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                this.datatable.Clear();
                foreach (String item in PurchaseOrderNo)
                {
                    if (item.ToUpper().Equals(PONotextBox.Text.ToUpper()))
                    {
                        Utilities.clearAllField(ref controlTextBox);
                        PONotextBox.Text = item.ToUpper();
                        addColumnForItem();
                        isExistingPartNo = true;
                        PONotextBox.Text = PONotextBox.Text.ToUpper();
                        String query = String.Format("SELECT PURCHASEORDERID, CONVERT(VARCHAR(10), PURCHASEORDERDATE, 103) AS [PURCHASEORDERDATE], SUPPLIERNAME, " +
                                                     "CONTACTPERSONSUPPLIER, REMARK " +
                                                     "FROM H_PURCHASE_ORDER A " +
                                                     "JOIN M_SUPPLIER B " +
                                                     "ON A.SUPPLIERID = B.SUPPLIERID " +
                                                     "WHERE PURCHASEORDERID = '{0}'",PONotextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        PurchaseOrderPassingData(datatable);
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    if (PartNOtextBox.Text.Equals(""))
                    {
                        Utilities.clearAllField(ref controlTextBox);
                        HistorydataGridView.Columns.Clear();
                        this.datatable.Clear();
                    }
                    else PONotextBox.Text = "";
                }
            }
        }

        private void PurchaseOrderPassingData(DataTable sender){
            PONotextBox.Text = sender.Rows[0]["PURCHASEORDERID"].ToString();
            PODatetextBox.Text = sender.Rows[0]["PURCHASEORDERDATE"].ToString();
            SupplierNametextBox.Text = sender.Rows[0]["SUPPLIERNAME"].ToString(); 
            ContactPersontextBox.Text = sender.Rows[0]["CONTACTPERSONSUPPLIER"].ToString();
            RemarkrichTextBox.Text = sender.Rows[0]["REMARK"].ToString();
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMIDQTY, A.ITEMID, B.ITEMNAME, A.QUANTITY, B.UOMID, A.PRICE, (A.QUANTITY * A.PRICE) AS AMOUNT " +
                                         "FROM D_PURCHASE_ORDER A " +
                                         "JOIN M_ITEM B " +
                                         "ON A.ITEMID = B.ITEMID " +
                                         "WHERE PURCHASEORDERID = '{0}'",PONotextBox.Text);

            DataTable datatable = connection.openDataTableQuery(query);
            HistorydataGridView.DataSource = datatable;

            textBoxSLTotalAmount.Text = HistorydataGridView.Rows.Cast<DataGridViewRow>()
                                                                .AsEnumerable()
                                                                .Sum(x => Double.Parse(x.Cells[7].Value.ToString()))
                                                                .ToString();
            //HistorydataGridView.Columns["AMOUNT"].DefaultCellStyle.Format = "N2";
            //HistorydataGridView.Columns["PRICE"].DefaultCellStyle.Format = "N2";
        }

        private void SearchPONObutton_Click(object sender, EventArgs e)
        {
            addColumnForItem();
            if (purchaseorderdatagridview == null)
            {
                purchaseorderdatagridview = new PurchaseOrderdatagridview("PurchaseOrder");
                purchaseorderdatagridview.purchaseOrderPassingData = new PurchaseOrderdatagridview.PurchaseOrderPassingData(PurchaseOrderPassingData);
                //purchaseorderdatagridview.MdiParent = this.MdiParent;
                purchaseorderdatagridview.ShowDialog();
            }
            else if (purchaseorderdatagridview.IsDisposed)
            {
                purchaseorderdatagridview = new PurchaseOrderdatagridview("PurchaseOrder");
                purchaseorderdatagridview.purchaseOrderPassingData = new PurchaseOrderdatagridview.PurchaseOrderPassingData(PurchaseOrderPassingData);
                //purchaseorderdatagridview.MdiParent = this.MdiParent;
                purchaseorderdatagridview.ShowDialog();
            }
        }

        private void RatetextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = Utilities.onlyNumberTextBox(sender, e);
        }

        private void SearchPartNobutton_Click(object sender, EventArgs e)
        {
            if (itemdatagridview == null)
            {
                itemdatagridview = new Itemdatagridview("POVIEW");
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                //itemdatagridview.MdiParent = this.MdiParent;
                itemdatagridview.ShowDialog();
            }
            else if (itemdatagridview.IsDisposed)
            {
                itemdatagridview = new Itemdatagridview("POVIEW");
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                //itemdatagridview.MdiParent = this.MdiParent;
                itemdatagridview.ShowDialog();
            }
        }

        private void addColumnForItem() {
            HistorydataGridView.Columns.Clear();
            var NO = new DataGridViewTextBoxColumn();
            NO.HeaderText = "NO";
            NO.DataPropertyName = "NO";
            NO.Width = 40;

            var ITEMID_ALIAS = new DataGridViewTextBoxColumn();
            ITEMID_ALIAS.HeaderText = "PART NO QTY";
            ITEMID_ALIAS.DataPropertyName = "ITEMIDQTY";
            ITEMID_ALIAS.Width = 130;

            var ITEMID = new DataGridViewTextBoxColumn();
            ITEMID.HeaderText = "PART NO";
            ITEMID.DataPropertyName = "ITEMID";
            ITEMID.Width = 130;

            var ITEMNAME = new DataGridViewTextBoxColumn();
            ITEMNAME.HeaderText = "PART NAME";
            ITEMNAME.DataPropertyName = "ITEMNAME";
            ITEMNAME.Width = 130;

            var QUANTITY = new DataGridViewTextBoxColumn();
            QUANTITY.HeaderText = "QTY";
            QUANTITY.DataPropertyName = "QUANTITY";
            QUANTITY.Width = 50;

            var UOMID = new DataGridViewTextBoxColumn();
            UOMID.HeaderText = "UOM";
            UOMID.DataPropertyName = "UOMID";
            UOMID.Width = 50;

            int authenticationprice = AuthorizeUser.sharedInstance.authenticationprice.Rows.Count;
            Boolean visiblePrice = authenticationprice == 0 ? false :
                AuthorizeUser.sharedInstance.authenticationprice.Rows[1]["VISIBLE"].ToString().Equals("1") ? true : false;

            var PRICE = new DataGridViewTextBoxColumn();
            PRICE.HeaderText = "PRICE";
            PRICE.DataPropertyName = "PRICE";
            PRICE.Width = 100;
            PRICE.DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
            PRICE.DefaultCellStyle.Format = "N2";
            PRICE.Visible = visiblePrice;

            var AMOUNT = new DataGridViewTextBoxColumn();
            AMOUNT.HeaderText = "AMOUNT";
            AMOUNT.DataPropertyName = "AMOUNT";
            AMOUNT.Width = 100;
            AMOUNT.DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
            AMOUNT.DefaultCellStyle.Format = "N2";
            AMOUNT.Visible = visiblePrice;

            HistorydataGridView.Columns.AddRange(new DataGridViewColumn[] { NO, ITEMID_ALIAS, ITEMID, ITEMNAME, QUANTITY, UOMID, PRICE, AMOUNT });
        }

        private void addColumnForPurchaseOrder() {
            HistorydataGridView.Columns.Clear();
            var NO = new DataGridViewTextBoxColumn();
            NO.HeaderText = "NO";
            NO.DataPropertyName = "NO";
            NO.Width = 40;

            var ITEMID_ALIAS = new DataGridViewTextBoxColumn();
            ITEMID_ALIAS.HeaderText = "PART NO QTY";
            ITEMID_ALIAS.DataPropertyName = "ITEMIDQTY";
            ITEMID_ALIAS.Width = 120;

            var ITEMID = new DataGridViewTextBoxColumn();
            ITEMID.HeaderText = "PART NO";
            ITEMID.DataPropertyName = "ITEMID";
            ITEMID.Width = 100;

            var PONO = new DataGridViewTextBoxColumn();
            PONO.HeaderText = "PO NO";
            PONO.DataPropertyName = "PURCHASEORDERID";
            PONO.Width = 70;

            var PODATE = new DataGridViewTextBoxColumn();
            PODATE.HeaderText = "PO DATE";
            PODATE.DataPropertyName = "PURCHASEORDERDATE";
            PODATE.Width = 70;

            var SUPPLIERNAME = new DataGridViewTextBoxColumn();
            SUPPLIERNAME.HeaderText = "SUPPLIER NAME";
            SUPPLIERNAME.DataPropertyName = "SUPPLIERNAME";
            SUPPLIERNAME.Width = 100;

            var CONTACTPERSON = new DataGridViewTextBoxColumn();
            CONTACTPERSON.HeaderText = "CP SUPPLIER";
            CONTACTPERSON.DataPropertyName = "CONTACTPERSONSUPPLIER";
            CONTACTPERSON.Width = 100;

            var QUANTITY = new DataGridViewTextBoxColumn();
            QUANTITY.HeaderText = "QTY";
            QUANTITY.DataPropertyName = "QUANTITY";
            QUANTITY.Width = 50;

            int authenticationprice = AuthorizeUser.sharedInstance.authenticationprice.Rows.Count;
            Boolean visiblePrice = authenticationprice == 0 ? false :
                AuthorizeUser.sharedInstance.authenticationprice.Rows[1]["VISIBLE"].ToString().Equals("1") ? true : false;

            var PRICE = new DataGridViewTextBoxColumn();
            PRICE.HeaderText = "PRICE";
            PRICE.DataPropertyName = "PRICE";
            PRICE.Width = 100;
            PRICE.DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
            PRICE.DefaultCellStyle.Format = "N2";
            PRICE.Visible = visiblePrice;

            var AMOUNT = new DataGridViewTextBoxColumn();
            AMOUNT.HeaderText = "AMOUNT";
            AMOUNT.DataPropertyName = "AMOUNT";
            AMOUNT.Width = 100;
            AMOUNT.DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
            AMOUNT.DefaultCellStyle.Format = "N2";
            AMOUNT.Visible = visiblePrice;

            var REMARK = new DataGridViewTextBoxColumn();
            REMARK.HeaderText = "REMARK";
            REMARK.DataPropertyName = "REMARK";
            REMARK.Width = 150;

            HistorydataGridView.Columns.AddRange(new DataGridViewColumn[] { NO, ITEMID_ALIAS, ITEMID, PONO, PODATE, SUPPLIERNAME, CONTACTPERSON, QUANTITY, PRICE, AMOUNT, REMARK });
        }

        private void PurchaseOrderView_Activated(object sender, EventArgs e)
        {
            if (menustripAction != null)
            {
                AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            }
            reloadAllData();
            Main.ACTIVEFORM = this;
        }
    }
}
