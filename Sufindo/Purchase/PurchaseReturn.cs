﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Sufindo.Purchase
{
    public partial class PurchaseReturn : Form
    {
        private delegate void fillAutoCompleteTextBox();

        ReturnPurchasedatagridview returnpurchasedatagridview;

        MenuStrip menustripAction;
        Connection connection = null;
        List<Control> controlTextBox;
        List<String> partNo;
        List<String> supplierID;
        List<String> purchaseID;
        List<String> returnNo;
        DataTable datatable;
        String activity = "";
        Boolean isFind = false;
        Boolean isPrint = false;
        public PurchaseReturn()
        {
            InitializeComponent();
        }

        public PurchaseReturn(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            returnpurchasedatagridview = null;
            this.menustripAction = menustripAction;
            if (connection == null) {
                connection = new Connection();
            }

            controlTextBox = new List<Control>();
            foreach (Control control in this.tableLayoutPanel.Controls)
            {
                if (control is TextBox)
                {
                    controlTextBox.Add(control);
                }
                else if (control is Panel)
                {
                    foreach (Control controlPanel in control.Controls)
                    {
                        if (controlPanel is RichTextBox)
                        {
                            controlTextBox.Add(controlPanel);
                        }
                    }
                }
                else if (control is ComboBox)
                {
                    controlTextBox.Add(control);
                }
            }
            SupplierIDtextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            SupplierIDtextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            PurchaseNotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            PurchaseNotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            
            newReturn();
        }

        public void save() {
            try
            {
                if (activity.Equals("INSERT") || activity.Equals("UPDATE") && dataGridView.Rows.Count > 0)
                {
                    List<SqlParameter> sqlParam = new List<SqlParameter>();

                    String SupplierID = dataGridView.Rows[0].Cells["SUPPLIERIDS"].Value.ToString();

                    if (activity.Equals("UPDATE")) sqlParam.Add(new SqlParameter("@PURCHASERETURNID", PurchaseReturnNotextBox.Text));
                    sqlParam.Add(new SqlParameter("@SUPPLIERID", SupplierID));
                    sqlParam.Add(new SqlParameter("@REMARK", RemarkrichTextBox.Text));
                    sqlParam.Add(new SqlParameter("@STATUS", Status.ACTIVE));
                    sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));

                    String proc = activity.Equals("UPDATE") ? "UPDATE_DATA_H_PURCHASE_RETURN" : "INSERT_DATA_H_PURCHASE_RETURN";

                    if (activity.Equals("UPDATE")) {
                        DataTable temp = connection.openDataTableQuery("SELECT ITEMIDQTY, QUANTITYRTR FROM D_PURCHASE_RETURN WHERE PURCHASERETURNID = '" + PurchaseReturnNotextBox.Text + "'");

                        for (int i = 0; i < temp.Rows.Count; i++){
                            String qry = String.Format("UPDATE M_ITEM SET QUANTITY = QUANTITY + {0} " +
                                                       "WHERE ITEMID = '{1}'", temp.Rows[i]["QUANTITYRTR"].ToString(), 
                                                       temp.Rows[i]["ITEMIDQTY"].ToString());
                            connection.sqlDataReaders(qry);
                        }

                        /*check ulang untuk delete log_transaction*/
                        //connection.sqlDataReaders("DELETE FROM LOG_TRANSACTION WHERE TRANSACTIONID = 'PR" + PurchaseReturnNotextBox.Text + "'");

                    }

                    DataTable dt = connection.callProcedureDatatable(proc, sqlParam);
                    if (dt.Rows.Count == 1)
                    {
                        //MessageBox.Show("PURCHASE RETURN ID " + dt.Rows[0]["PURCHASERETURNID"].ToString());
                        
                        List<string> values = new List<string>();
                        foreach (DataRow row in this.datatable.Rows)
                        {
                            String value = String.Format("SELECT '{0}', '{1}', '{2}', '{3}', '{4}', '{5}' ",
                                                          dt.Rows[0]["PURCHASERETURNID"].ToString(), row["PURCHASEID"].ToString(),
                                                          row["ITEMID"].ToString(), row["ITEMIDQTY"].ToString(), 
                                                          row["QUANTITYRCV"].ToString(), row["QUANTITYRTR"].ToString());
                            //MessageBox.Show(value);
                            values.Add(value);
                        }
                        String[] columns = { "PURCHASERETURNID", "PURCHASEID", "ITEMID", "ITEMIDQTY", "QUANTITYRCV", "QUANTITYRTR" };
                        String query = String.Format("DELETE FROM D_PURCHASE_RETURN WHERE PURCHASERETURNID = '{0}'", dt.Rows[0]["PURCHASERETURNID"].ToString());
                        if (connection.openReaderQuery(query))
                        {
                            if (connection.openReaderQuery(Utilities.queryMultipleInsert("D_PURCHASE_RETURN", columns, values)))
                            {
                                MessageBox.Show(String.Format("PURCHASE RETURN NO {0}",dt.Rows[0]["PURCHASERETURNID"].ToString()));
                                if (isPrint)
                                {
                                    isPrint = false;
                                    PurchaseReturnReportForm purchasereturnReportForm = new PurchaseReturnReportForm(dt.Rows[0]["PURCHASERETURNID"].ToString(), true);
                                    purchasereturnReportForm.ShowDialog();
                                }
                                newReturn();
                                reloadAllData();
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("INSERT OR UPDATE FAILED");
            }
            
        }

        public void newReturn() {
            Utilities.clearAllField(ref controlTextBox);
            if (datatable == null)
            {
                addRowonDataTable();
            }
            datatable.Clear();
            SearchRPbutton.Visible = false;
            //SearchItembutton.Visible = true;
            //SearchSupplierIDbutton.Visible = true;
            //SearchPurchaseNobutton.Visible = true;
            SearchSupplierIDbutton.Enabled = false;
            SearchPurchaseNobutton.Enabled = false;
            activity = "INSERT";
            StatustextBox.Text = Status.ACTIVE;
            QtyReturntextBox.Text = "0";
            foreach (Control item in controlTextBox)
            {
                if (!item.Name.Equals("PurchaseReturnNotextBox"))
                {
                    if (item is RichTextBox)
                    {
                        RichTextBox richtextBox = (RichTextBox)item;
                        richtextBox.ReadOnly = false;
                        richtextBox.BackColor = Color.White;
                        richtextBox.Text = "";
                    }
                    else
                    {
                        item.Enabled = true;
                        item.Text = "";
                    }
                }
                else
                {
                    TextBox currentTextbox = (TextBox)item;
                    currentTextbox.Enabled = true;
                }

                item.BackColor = Color.White;
            }
            QtyReceivetextBox.Enabled = false;

            PartNametextBox.Enabled = false;
            PurchaseDatetextBox.Enabled = false;
            PurchaseReturnNotextBox.Text = "";
            PurchaseReturnNotextBox.Enabled = false;
            PurchaseReturnNotextBox.Text = DateTime.Now.ToString("yyyy") + "XXXXXX";
            
            DatetextBox.Text = DateTime.Now.ToString("dd/MM/yyyy");
            DatetextBox.Enabled = false;
            StatustextBox.Text = Status.ACTIVE;
            StatustextBox.Enabled = false;

            dataGridView.AllowUserToDeleteRows = true;
            AllowUserToEditRows(true);
            foreach (DataGridViewColumn column in dataGridView.Columns)
            {
                if (column.Name.Equals("DEL"))
                {
                    column.Visible = true;
                    break;
                }
            }
        }

        public void find(){
            if (!SearchRPbutton.Visible || isFind)
            {
                datatable.Clear();
                Utilities.clearAllField(ref controlTextBox);
                activity = "";
                isFind = true;
                SearchRPbutton.Visible = true;
                SearchItembutton.Visible = false;
                SearchSupplierIDbutton.Visible = false;
                SearchPurchaseNobutton.Visible = false;
                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("PurchaseReturnNotextBox"))
                    {
                        if (item is RichTextBox)
                        {
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = true;
                            richtextBox.BackColor = SystemColors.Info;
                            richtextBox.Text = "";
                        }
                        else
                        {
                            item.Enabled = false;
                            item.Text = "";
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.AutoCompleteCustomSource.Clear();
                        currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        currentTextbox.AutoCompleteCustomSource.AddRange(returnNo.ToArray());
                        currentTextbox.Text = "";
                        currentTextbox.Enabled = true;
                        currentTextbox.Focus();
                    }

                    item.BackColor = SystemColors.Info;
                }
                dataGridView.AllowUserToDeleteRows = false;
                AllowUserToEditRows(false);
                foreach (DataGridViewColumn column in dataGridView.Columns)
                {
                    if (column.Name.Equals("DEL"))
                    {
                        column.Visible = false;
                        break;
                    }
                }
            }
            
        }

        public void edit(){
            if ((!SearchRPbutton.Visible || isFind))
            {
                datatable.Clear();
                SearchRPbutton.Visible = true;
                isFind = true;
                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("PurchaseReturnNotextBox"))
                    {
                        if (item is RichTextBox)
                        {
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = false;
                            richtextBox.BackColor = SystemColors.Info;
                            richtextBox.Text = "";
                        }
                        else
                        {
                            item.Enabled = false;
                            item.Text = "";
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.AutoCompleteCustomSource.Clear();
                        currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        currentTextbox.AutoCompleteCustomSource.AddRange(returnNo.ToArray());
                        currentTextbox.Text = "";
                        currentTextbox.Enabled = true;
                        currentTextbox.Focus();
                    }

                    item.BackColor = SystemColors.Info;
                }
                PartNotextBox.Enabled = true;
                dataGridView.AllowUserToDeleteRows = true;
                AllowUserToEditRows(true);
                foreach (DataGridViewColumn column in dataGridView.Columns)
                {
                    if (column.Name.Equals("DEL"))
                    {
                        column.Visible = true;
                        break;
                    }
                }
                activity = "UPDATE";
            }
        }

        private void AllowUserToEditRows(Boolean allow)
        {
            foreach (DataGridViewColumn column in dataGridView.Columns)
            {
                column.ReadOnly = (column.Name.Equals("QUANTITYRTR")) ? !allow : true;
            }
        }

        public void delete(){
            if (isFind && Status.ACTIVE.Equals(StatustextBox.Text))
            {
                List<SqlParameter> sqlParam = new List<SqlParameter>();
                sqlParam.Add(new SqlParameter("@PURCHASERETURNID", PurchaseReturnNotextBox.Text));
                sqlParam.Add(new SqlParameter("@STATUS", Status.DELETE));
                sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));

                if (connection.callProcedure("DELETE_DATA_H_PURCHASE_RETURN", sqlParam))
                {
                    newReturn();
                    reloadAllData();
                }
            }
        }

        public void print() {
            isPrint = true;
            if (activity.Equals("UPDATE") || activity.Equals("INSERT")){
                save();
            }
            else
            {
                PurchaseReturnReportForm purchasereturnReportForm = new PurchaseReturnReportForm(PurchaseReturnNotextBox.Text, true);
                purchasereturnReportForm.ShowDialog();
            }
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (partNo == null)
            {
                partNo = new List<String>();
            }
            else {
                partNo.Clear();
            }
            if (returnNo == null)
            {
                returnNo = new List<String>();
            }
            else {
                returnNo.Clear();
            }
            Console.WriteLine("bgWorker_DoWork");
            String query = String.Format("SELECT DISTINCT ITEMID FROM D_PURCHASE WHERE " +
                                         "PURCHASEID NOT IN " +
                                         "( " +
                                         "   SELECT PURCHASEID FROM H_PURCHASE_RETURN A " +
                                         "   JOIN D_PURCHASE_RETURN B " +
                                         "   ON A.PURCHASERETURNID = B.PURCHASERETURNID " +
                                         "   WHERE STATUS = '{0}' " +
                                         ") " +
                                         "OR ITEMID NOT IN " +
                                         "( " +
                                         "   SELECT ITEMID FROM H_PURCHASE_RETURN A " +
                                         "   JOIN D_PURCHASE_RETURN B " +
                                         "   ON A.PURCHASERETURNID = B.PURCHASERETURNID " +
                                         ")",Status.ACTIVE);

            SqlDataReader sqldataReader = connection.sqlDataReaders(query);

            while (sqldataReader.Read()) partNo.Add(sqldataReader.GetString(0));

            sqldataReader = connection.sqlDataReaders(String.Format("SELECT PURCHASERETURNID FROM H_PURCHASE_RETURN " +
                                                                    "WHERE STATUS != '{0}'", Status.DELETE));
            
            while (sqldataReader.Read()) returnNo.Add(sqldataReader.GetString(0));

            //if (supplierID == null) supplierID = new List<string>();
            //if (contactPerson == null) contactPerson = new List<string>();
            //if (KursID == null) KursID = new List<string>();
            //if (PurchaseOrderNo == null) PurchaseOrderNo = new List<string>();

            //SqlDataReader sqldataReader = connection.sqlDataReaders(String.Format("SELECT SUPPLIERID FROM M_SUPPLIER WHERE [STATUS] = '{0}'", Status.ACTIVE));

            //while (sqldataReader.Read()) supplierID.Add(sqldataReader.GetString(0));

            //sqldataReader = connection.sqlDataReaders(String.Format("SELECT KURSID FROM M_KURS WHERE [STATUS] = '{0}'", Status.ACTIVE));

            //while (sqldataReader.Read()) KursID.Add(sqldataReader.GetString(0));

            //sqldataReader = connection.sqlDataReaders(String.Format("SELECT PURCHASEORDERID FROM H_PURCHASE_ORDER WHERE [STATUS] = '{0}'", Status.OPEN));

            //while (sqldataReader.Read()) PurchaseOrderNo.Add(sqldataReader.GetString(0));

            if (IsHandleCreated) BeginInvoke(new fillAutoCompleteTextBox(this.fillAutoComplete));
        }

        private void fillAutoComplete()
        {
            String tempPartNo = PartNotextBox.Text;
            PartNotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            PartNotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            PartNotextBox.AutoCompleteCustomSource.AddRange(partNo.ToArray());
            PartNotextBox.Text = tempPartNo;
        }
        
        private void PartNotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    Boolean isExistingPartNo = false;
                    if (!isExistingPartNo)
                    {
                        foreach (String item in partNo)
                        {
                            if (item.ToUpper().Equals(PartNotextBox.Text.ToUpper()))
                            {
                                isExistingPartNo = true;
                                PartNotextBox.Text = PartNotextBox.Text.ToUpper();
                                String query = String.Format("SELECT ITEMNAME " +
                                                             "FROM M_ITEM " +
                                                             "WHERE ITEMID = '{0}'", PartNotextBox.Text);

                                DataTable dt = connection.openDataTableQuery(query);
                                PartNametextBox.Text = dt.Rows[0]["ITEMNAME"].ToString();
                                query = String.Format("SELECT DISTINCT SUPPLIERID FROM H_PURCHASE A " +
                                                      "JOIN D_PURCHASE B " +
                                                      "ON A.PURCHASEID = B.PURCHASEID " +
                                                      "WHERE B.ITEMID = '{0}' " +
                                                      "AND " +
                                                      "( " +
                                                      "  A.PURCHASEID NOT IN " +
                                                      "  ( " +
                                                      "      SELECT PURCHASEID FROM H_PURCHASE_RETURN A " +
                                                      "      JOIN D_PURCHASE_RETURN B " +
                                                      "      ON A.PURCHASERETURNID = B.PURCHASERETURNID " +
                                                      "      WHERE STATUS = '{1}' " +
                                                      "  ) " +
                                                      "  OR B.ITEMID NOT IN " +
                                                      "  ( " +
                                                      "      SELECT ITEMID FROM H_PURCHASE_RETURN A " +
                                                      "      JOIN D_PURCHASE_RETURN B " +
                                                      "      ON A.PURCHASERETURNID = B.PURCHASERETURNID " +
                                                      "      WHERE STATUS = '{0}' " +
                                                      "  ) " +
                                                      ")", PartNotextBox.Text, Status.ACTIVE);
                                SupplierIDtextBox.AutoCompleteCustomSource.Clear();
                                SupplierIDtextBox.Text = "";
                                supplierID = new List<String>();
                                SqlDataReader sqldataReader = connection.sqlDataReaders(query);

                                while (sqldataReader.Read()) supplierID.Add(sqldataReader.GetString(0));

                                SupplierIDtextBox.AutoCompleteCustomSource.AddRange(supplierID.ToArray());
                                SupplierIDtextBox.Enabled = true;
                                SearchSupplierIDbutton.Enabled = true;

                                PurchaseNotextBox.Text = "";
                                PurchaseNotextBox.Enabled = false;
                                PurchaseDatetextBox.Text = "";
                                QtyReturntextBox.Text = "0";
                                QtyReceivetextBox.Text = "";

                                QtyReturntextBox.Enabled = false;
                                AddItembutton.Enabled = false;

                                break;
                            }
                        }
                    }



                    for (int i = 0; i < dataGridView.Rows.Count; i++)
                    {
                        Console.WriteLine(dataGridView.Rows[i].Cells["ITEMID"].Value.ToString().ToUpper());
                        if (dataGridView.Rows[i].Cells["ITEMID"].Value.ToString().ToUpper().Equals(PartNotextBox.Text.ToUpper()))
                        {
                            isExistingPartNo = false;
                            break;
                        }
                    }

                    if (!isExistingPartNo)
                    {
                        PartNotextBox.Text = "";
                        PartNametextBox.Text = "";
                        SupplierIDtextBox.Text = "";
                        SupplierIDtextBox.Enabled = false;

                        PurchaseNotextBox.Text = "";
                        PurchaseNotextBox.Enabled = false;
                        PurchaseDatetextBox.Text = "";
                        QtyReturntextBox.Text = "0";
                        QtyReceivetextBox.Text = "";

                        QtyReturntextBox.Enabled = false;
                        AddItembutton.Enabled = false;
                    }
                }
            }
            catch (Exception)
            {
                
            }
            
        }

        private void PartNotextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                Boolean isExistingPartNo = false;
                foreach (String item in partNo)
                {
                    if (item.ToUpper().Equals(PartNotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        PartNotextBox.Text = PartNotextBox.Text.ToUpper();
                        String query = String.Format("SELECT ITEMNAME " +
                                                     "FROM M_ITEM " +
                                                     "WHERE ITEMID = '{0}'", PartNotextBox.Text);

                        DataTable dt = connection.openDataTableQuery(query);
                        PartNametextBox.Text = dt.Rows[0]["ITEMNAME"].ToString();
                        query = String.Format("SELECT DISTINCT SUPPLIERID FROM H_PURCHASE A " +
                                              "JOIN D_PURCHASE B " +
                                              "ON A.PURCHASEID = B.PURCHASEID " +
                                              "WHERE B.ITEMID = '{0}' " +
                                              "AND " +
                                              "( " +
                                              "  A.PURCHASEID NOT IN " +
                                              "  ( " +
                                              "      SELECT PURCHASEID FROM H_PURCHASE_RETURN A " +
                                              "      JOIN D_PURCHASE_RETURN B " +
                                              "      ON A.PURCHASERETURNID = B.PURCHASERETURNID " +
                                              "      WHERE STATUS = '{1}' " +
                                              "  ) " +
                                              "  OR B.ITEMID NOT IN " +
                                              "  ( " +
                                              "      SELECT ITEMID FROM H_PURCHASE_RETURN A " +
                                              "      JOIN D_PURCHASE_RETURN B " +
                                              "      ON A.PURCHASERETURNID = B.PURCHASERETURNID " +
                                              "      WHERE STATUS = '{0}' " +
                                              "  ) " +
                                              ")", PartNotextBox.Text, Status.ACTIVE);
                        SupplierIDtextBox.AutoCompleteCustomSource.Clear();
                        SupplierIDtextBox.Text = "";
                        supplierID = new List<String>();
                        SqlDataReader sqldataReader = connection.sqlDataReaders(query);

                        while (sqldataReader.Read()) supplierID.Add(sqldataReader.GetString(0));
                       
                        SupplierIDtextBox.AutoCompleteCustomSource.AddRange(supplierID.ToArray());
                        SupplierIDtextBox.Enabled = true;
                        SearchSupplierIDbutton.Enabled = true;

                        PurchaseNotextBox.Text = "";
                        PurchaseNotextBox.Enabled = false;
                        PurchaseDatetextBox.Text = "";
                        QtyReturntextBox.Text = "0";
                        QtyReceivetextBox.Text = "";

                        QtyReturntextBox.Enabled = false;
                        AddItembutton.Enabled = false;
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    PartNotextBox.Text = "";
                    PartNametextBox.Text = "";
                    SupplierIDtextBox.Text = "";
                    SupplierIDtextBox.Enabled = false;

                    PurchaseNotextBox.Text = "";
                    PurchaseNotextBox.Enabled = false;
                    PurchaseDatetextBox.Text = "";
                    QtyReturntextBox.Text = "";
                    QtyReceivetextBox.Text = "";

                    QtyReturntextBox.Enabled = false;
                    AddItembutton.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        private void SupplierIDtextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                Boolean isExistingPartNo = false;
                foreach (String item in supplierID)
                {
                    if (item.ToUpper().Equals(SupplierIDtextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        SupplierIDtextBox.Text = SupplierIDtextBox.Text.ToUpper();

                        String query = String.Format("SELECT A.PURCHASEID FROM H_PURCHASE A " +
                                                     "JOIN D_PURCHASE B " +
                                                     "ON A.PURCHASEID = B.PURCHASEID " +
                                                     "WHERE SUPPLIERID = '{0}' AND B.ITEMID = '{1}' " +
                                                     "AND " +
                                                     "( " +
	                                                 "   A.PURCHASEID NOT IN " + 
	                                                 "   ( " +
		                                             "       SELECT PURCHASEID FROM H_PURCHASE_RETURN A " +
		                                             "       JOIN D_PURCHASE_RETURN B " +
		                                             "       ON A.PURCHASERETURNID = B.PURCHASERETURNID " +
		                                             "       WHERE STATUS = '{2}' " +
	                                                 "   ) " +
	                                                 "   OR B.ITEMID NOT IN " +
	                                                 "   ( " +
                                                     "      SELECT ITEMID FROM H_PURCHASE_RETURN A " +
                                                     "      JOIN D_PURCHASE_RETURN B " +
                                                     "      ON A.PURCHASERETURNID = B.PURCHASERETURNID " +
                                                     "      WHERE STATUS = '{2}' " +
                                                     "   ) " +
                                                     ")", SupplierIDtextBox.Text, PartNotextBox.Text, Status.ACTIVE);

                        PurchaseNotextBox.AutoCompleteCustomSource.Clear();
                        purchaseID = new List<String>();
                        SqlDataReader sqldataReader = connection.sqlDataReaders(query);
                        while (sqldataReader.Read()) purchaseID.Add(sqldataReader.GetString(0));
                        PurchaseNotextBox.AutoCompleteCustomSource.AddRange(purchaseID.ToArray());
                        PurchaseNotextBox.Enabled = true;
                        SearchPurchaseNobutton.Enabled = true;

                        QtyReturntextBox.Enabled = false;
                        AddItembutton.Enabled = false;
                        PurchaseNotextBox.Text = "";
                        PurchaseDatetextBox.Text = "";
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    SupplierIDtextBox.Text = "";
                    PurchaseNotextBox.Enabled = false;
                    PurchaseNotextBox.Text = "";
                    PurchaseDatetextBox.Text = "";
                    QtyReturntextBox.Text = "0";
                    QtyReceivetextBox.Text = "";
                    QtyReturntextBox.Enabled = false;
                    AddItembutton.Enabled = false;

                }
            }
            catch (Exception)
            {
                
            }
            
        }

        private void SupplierIDtextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in supplierID)
                {
                    if (item.ToUpper().Equals(SupplierIDtextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        SupplierIDtextBox.Text = SupplierIDtextBox.Text.ToUpper();
                        String query = String.Format("SELECT A.PURCHASEID FROM H_PURCHASE A " +
                                                     "JOIN D_PURCHASE B " +
                                                     "ON A.PURCHASEID = B.PURCHASEID " +
                                                     "WHERE SUPPLIERID = '{0}' AND B.ITEMID = '{1}'", SupplierIDtextBox.Text, PartNotextBox.Text);
                        
                        purchaseID = new List<String>();
                        SqlDataReader sqldataReader = connection.sqlDataReaders(query);
                        PurchaseNotextBox.AutoCompleteCustomSource.Clear();
                        while (sqldataReader.Read()) purchaseID.Add(sqldataReader.GetString(0));
                        PurchaseNotextBox.AutoCompleteCustomSource.AddRange(purchaseID.ToArray());
                        PurchaseNotextBox.Enabled = true;
                        SearchPurchaseNobutton.Enabled = true;

                        QtyReturntextBox.Enabled = false;
                        AddItembutton.Enabled = false;
                        PurchaseNotextBox.Text = "";
                        PurchaseDatetextBox.Text = "";
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    SupplierIDtextBox.Text = "";

                    PurchaseNotextBox.Text = "";
                    PurchaseNotextBox.Enabled = false;
                    PurchaseDatetextBox.Text = "";
                    QtyReturntextBox.Text = "0";
                    QtyReceivetextBox.Text = "";

                    QtyReturntextBox.Enabled = false;
                    AddItembutton.Enabled = false;
                }
            }
        }

        private void PurchaseNotextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                Boolean isExistingPartNo = false;
                foreach (String item in purchaseID)
                {
                    if (item.ToUpper().Equals(PurchaseNotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        PurchaseNotextBox.Text = PurchaseNotextBox.Text.ToUpper();
                        String query = String.Format("SELECT CONVERT(VARCHAR(19), PURCHASEDATE, 103) AS PURCHASEDATE, B.QUANTITY " +
                                                     "FROM H_PURCHASE A " +
                                                     "JOIN D_PURCHASE B " +
                                                     "ON A.PURCHASEID = B.PURCHASEID " +
                                                     "JOIN M_ITEM C " +
                                                     "ON B.ITEMIDQTY =  C.ITEMID " +
                                                     "WHERE A.PURCHASEID = '{0}' " +
                                                     "AND B.ITEMID = '{1}' ", PurchaseNotextBox.Text, PartNotextBox.Text);
                        DataTable dt = connection.openDataTableQuery(query);
                        PurchaseDatetextBox.Text = dt.Rows[0]["PURCHASEDATE"].ToString();
                        QtyReceivetextBox.Text = dt.Rows[0]["QUANTITY"].ToString();
                        QtyReceivetextBox.Enabled = false;
                        QtyReturntextBox.Enabled = true;
                        AddItembutton.Enabled = true;
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    PurchaseNotextBox.Text = "";
                    PurchaseDatetextBox.Text = "";
                    QtyReceivetextBox.Text = "";
                    QtyReturntextBox.Text = "0";
                    QtyReturntextBox.Enabled = false;
                    QtyReceivetextBox.Enabled = false;
                    AddItembutton.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                
            }
            
        }

        private void PurchaseNotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    Boolean isExistingPartNo = false;
                    foreach (String item in purchaseID)
                    {
                        if (item.ToUpper().Equals(PurchaseNotextBox.Text.ToUpper()))
                        {
                            isExistingPartNo = true;
                            PurchaseNotextBox.Text = PurchaseNotextBox.Text.ToUpper();
                            String query = String.Format("SELECT CONVERT(VARCHAR(19), PURCHASEDATE, 103) AS PURCHASEDATE, B.QUANTITY " +
                                                         "FROM H_PURCHASE A " +
                                                         "JOIN D_PURCHASE B " +
                                                         "ON A.PURCHASEID = B.PURCHASEID " +
                                                         "JOIN M_ITEM C " +
                                                         "ON B.ITEMIDQTY =  C.ITEMID " +
                                                         "WHERE A.PURCHASEID = '{0}' " +
                                                         "AND B.ITEMID = '{1}' ", PurchaseNotextBox.Text, PartNotextBox.Text);
                            DataTable dt = connection.openDataTableQuery(query);
                            PurchaseDatetextBox.Text = dt.Rows[0]["PURCHASEDATE"].ToString();
                            QtyReceivetextBox.Text = dt.Rows[0]["QUANTITY"].ToString();
                            QtyReceivetextBox.Enabled = false;
                            QtyReturntextBox.Enabled = true;
                            AddItembutton.Enabled = true;
                            break;
                        }
                    }

                    if (!isExistingPartNo)
                    {
                        PurchaseNotextBox.Text = "";
                        PurchaseDatetextBox.Text = "";
                        QtyReceivetextBox.Text = "";
                        QtyReturntextBox.Text = "0";
                        QtyReturntextBox.Enabled = false;
                        AddItembutton.Enabled = false;
                        QtyReceivetextBox.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                
            }
            
        }

        private void QtyReturntextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                e.Handled = Utilities.onlyNumberTextBox(sender, e);
                if (!e.Handled)
                {
                    if (long.Parse(QtyReturntextBox.Text) < 0 || long.Parse(QtyReturntextBox.Text) > long.Parse(QtyReceivetextBox.Text))
                    {
                        QtyReturntextBox.Text = QtyReceivetextBox.Text;
                    }
                }
            }
            catch (Exception)
            {
                
            }
        }

        private void AddItembutton_Click(object sender, EventArgs e)
        {
            if (QtyReturntextBox.Text.Equals(""))
            {
                MessageBox.Show("Return Quantity must Fill");
            }
            else
            {
                if (dataGridView.Rows.Count > 0) {
                    for (int i = 0; i < dataGridView.Rows.Count; i++)
                    {;
                        if (!dataGridView.Rows[i].Cells["SUPPLIERIDS"].Value.ToString().Equals(SupplierIDtextBox.Text))
                        {
                            return;
                        }
                    }
                }

                string query = String.Format("SELECT B.ITEMIDQTY FROM H_PURCHASE A " +
                                             "JOIN D_PURCHASE B " +
                                             "ON A.PURCHASEID = B.PURCHASEID " +
                                             "WHERE B.ITEMID = '{0}' AND B.PURCHASEID = '{1}' ",
                                             PartNotextBox.Text, PurchaseNotextBox.Text);

                DataTable dt = connection.openDataTableQuery(query);

                int no = dataGridView.Rows.Count + 1;
                this.datatable.Rows.Add(no,PartNotextBox.Text, PartNametextBox.Text, dt.Rows[0]["ITEMIDQTY"].ToString(), SupplierIDtextBox.Text,
                                      PurchaseNotextBox.Text, PurchaseDatetextBox.Text, QtyReceivetextBox.Text, QtyReturntextBox.Text);
                dataGridView.DataSource = datatable;

                String tempPurchaseReturnID = PurchaseReturnNotextBox.Text;
                String tempPurchaseReturnDate = DatetextBox.Text;
                String tempRemark = RemarkrichTextBox.Text;
                Utilities.clearAllField(ref controlTextBox);

                AddItembutton.Enabled = false;
                if (activity.Equals("INSERT"))
                {
                    PurchaseReturnNotextBox.Text = DateTime.Now.ToString("yyyy") + "XXXXXX";
                    DatetextBox.Text = DateTime.Now.ToString("dd/MM/yyyy");
                }
                else {
                    PurchaseReturnNotextBox.Text = tempPurchaseReturnID;
                    DatetextBox.Text = tempPurchaseReturnDate;
                }
                RemarkrichTextBox.Text = tempRemark;
            }
        }

        private void QtyReturntextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                if (long.Parse(QtyReturntextBox.Text) < 0 || long.Parse(QtyReturntextBox.Text) > long.Parse(QtyReceivetextBox.Text))
                {
                    QtyReturntextBox.Text = QtyReceivetextBox.Text;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void dataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                if (e.ColumnIndex == 2)
                {
                    this.datatable.Rows.RemoveAt(e.RowIndex);
                    Utilities.generateNoDataTable(ref datatable);
                    dataGridView.DataSource = datatable;
                }
            }
        }

        private void addRowonDataTable(){
            datatable = new DataTable();
            datatable.Columns.Add("NO", typeof(int));
            datatable.Columns.Add("ITEMID", typeof(String));
            datatable.Columns.Add("ITEMNAME", typeof(String));
            datatable.Columns.Add("ITEMIDQTY", typeof(String));
            datatable.Columns.Add("SUPPLIERID", typeof(String));
            datatable.Columns.Add("PURCHASEID", typeof(String));
            datatable.Columns.Add("PURCHASEDATE", typeof(String));
            datatable.Columns.Add("QUANTITYRCV", typeof(int));
            datatable.Columns.Add("QUANTITYRTR", typeof(int));
        }

        private void dataGridView_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            for (int i = 0; i < datatable.Rows.Count; i++)
            {
                if (datatable.Rows[i].RowState == DataRowState.Deleted)
                {
                    datatable.Rows.RemoveAt(i);
                }
            }
            Utilities.generateNoDataTable(ref datatable);
        }

        private void PurchaseReturn_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref  menustripAction, this.Text);
            reloadAllData();
            Main.ACTIVEFORM = this;
        }

        private void PurchaseReturn_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }

        private void SearchRPbutton_Click(object sender, EventArgs e)
        {
            
            if (returnpurchasedatagridview == null)
            {
                returnpurchasedatagridview = new ReturnPurchasedatagridview("PurchaseReturn");
                returnpurchasedatagridview.returnpurchasePassingData = new ReturnPurchasedatagridview.ReturnPurchasePassingData(ReturnPurchasePassingData);
                //purchaseorderdatagridview.MdiParent = this.MdiParent;
                returnpurchasedatagridview.ShowDialog();
            }
            else if (returnpurchasedatagridview.IsDisposed)
            {
                returnpurchasedatagridview = new ReturnPurchasedatagridview("PurchaseReturn");
                returnpurchasedatagridview.returnpurchasePassingData = new ReturnPurchasedatagridview.ReturnPurchasePassingData(ReturnPurchasePassingData);
                //purchaseorderdatagridview.MdiParent = this.MdiParent;
                returnpurchasedatagridview.ShowDialog();
            }
        }

        private void ReturnPurchasePassingData(DataTable sender){
            Console.WriteLine(sender.Rows.Count);
            PurchaseReturnNotextBox.Text = sender.Rows[0]["PURCHASERETURNID"].ToString();
            DatetextBox.Text = sender.Rows[0]["PURCHASERETURNDATE"].ToString();
            RemarkrichTextBox.Text = sender.Rows[0]["REMARK"].ToString();
            StatustextBox.Text = sender.Rows[0]["STATUS"].ToString();

            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMID, " +
                                         "ITEMNAME, ITEMIDQTY, D.SUPPLIERID, " +
                                         "B.PURCHASEID,CONVERT(VARCHAR(10), B.PURCHASEDATE, 103) AS PURCHASEDATE, " +
                                         "QUANTITYRCV, QUANTITYRTR " +
                                         "FROM D_PURCHASE_RETURN A " +
                                         "JOIN H_PURCHASE B " +
                                         "ON A.PURCHASEID = B.PURCHASEID " +
                                         "JOIN M_ITEM C " +
                                         "ON A.ITEMID = C.ITEMID " +
                                         "JOIN H_PURCHASE_RETURN D " +
                                         "ON A.PURCHASERETURNID = D.PURCHASERETURNID " +
                                         "WHERE A.PURCHASERETURNID = '{0}'", PurchaseReturnNotextBox.Text);

            datatable = connection.openDataTableQuery(query);
            dataGridView.DataSource = datatable;
        }

        private void PurchaseReturnNotextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                Boolean isExistingPartNo = false;
                foreach (String item in returnNo)
                {
                    if (item.ToUpper().Equals(PurchaseReturnNotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        PurchaseReturnNotextBox.Text = PurchaseReturnNotextBox.Text.ToUpper();

                        String query = String.Format("SELECT PURCHASERETURNID, CONVERT(VARCHAR(10), PURCHASERETURNDATE, 103) AS PURCHASERETURNDATE, REMARK, STATUS FROM H_PURCHASE_RETURN " +
                                                     "WHERE PURCHASERETURNID = '{0}'", PurchaseReturnNotextBox.Text);
                        DataTable dt = connection.openDataTableQuery(query);

                        ReturnPurchasePassingData(dt);
                        
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    PurchaseReturnNotextBox.Text = "";
                    DatetextBox.Text = "";
                    RemarkrichTextBox.Text = "";
                    datatable.Clear();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void PurchaseReturnNotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    Boolean isExistingPartNo = false;
                    foreach (String item in returnNo)
                    {
                        if (item.ToUpper().Equals(PurchaseReturnNotextBox.Text.ToUpper()))
                        {
                            isExistingPartNo = true;
                            String query = String.Format("SELECT PURCHASERETURNID,CONVERT(VARCHAR(10), PURCHASERETURNDATE, 103) AS PURCHASERETURNDATE, REMARK, STATUS FROM H_PURCHASE_RETURN " +
                                                         "WHERE PURCHASERETURNID = '{0}'", PurchaseReturnNotextBox.Text);
                            DataTable dt = connection.openDataTableQuery(query);
                            ReturnPurchasePassingData(dt);
                            break;
                        }
                    }

                    if (!isExistingPartNo)
                    {
                        PurchaseReturnNotextBox.Text = "";
                        DatetextBox.Text = "";
                        RemarkrichTextBox.Text = "";
                        datatable.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void dataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
        }

        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            int currColumn = dataGridView.CurrentCell.ColumnIndex;
            int currRow = dataGridView.CurrentCell.RowIndex;
            if (dataGridView.Columns[currColumn].Name.Equals("QUANTITYRTR"))
            {
                e.Handled = Utilities.onlyNumberTextBox(sender, (KeyPressEventArgs)e);
            }
        }

        private void dataGridView_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Int64 QUANTITYRTR = (dataGridView.Rows[e.RowIndex].Cells["QUANTITYRTR"].Value != null) ? Int64.Parse(dataGridView.Rows[e.RowIndex].Cells["QUANTITYRTR"].Value.ToString()) : 0;
                Int64 QUANTITYRCV = (dataGridView.Rows[e.RowIndex].Cells["QUANTITYRCV"].Value != null) ? Int64.Parse(dataGridView.Rows[e.RowIndex].Cells["QUANTITYRCV"].Value.ToString()) : 0;

                if (QUANTITYRTR > QUANTITYRCV) dataGridView.Rows[e.RowIndex].Cells["QUANTITYRTR"].Value = QUANTITYRCV;
                else if (QUANTITYRTR == 0) dataGridView.Rows[e.RowIndex].Cells["QUANTITYRTR"].Value = "1";
            }
            catch (Exception)
            {
                
            }
        }

        Itemdatagridview itemdatagridview = null;
        private void SearchItembutton_Click(object sender, EventArgs e)
        {
            if (itemdatagridview == null)
            {
                itemdatagridview = new Itemdatagridview("PURCHASERETURN", SupplierIDtextBox.Text, datatable);
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
            else if (itemdatagridview.IsDisposed)
            {
                itemdatagridview = new Itemdatagridview("PURCHASERETURN", SupplierIDtextBox.Text, datatable);
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
        }

        private void MasterItemPassingData(DataTable datatable) {
            PartNotextBox.Text = datatable.Rows[0]["ITEMID"].ToString();
            PartNametextBox.Text = datatable.Rows[0]["ITEMNAME"].ToString();
            String query = String.Format("SELECT DISTINCT SUPPLIERID FROM H_PURCHASE A " +
                                         "JOIN D_PURCHASE B " +
                                         "ON A.PURCHASEID = B.PURCHASEID " +
                                         "WHERE B.ITEMID = '{0}' " +
                                         "AND " +
                                         "( " +
                                         "  A.PURCHASEID NOT IN " +
                                         "  ( " +
                                         "      SELECT PURCHASEID FROM H_RETURN_PURCHASE A " +
                                         "      JOIN D_RETURN_PURCHASE B " +
                                         "      ON A.RETURNPURCHASEID = B.RETURNPURCHASEID " +
                                         "      WHERE STATUS = '{1}' " +
                                         "  ) " +
                                         "  OR B.ITEMID NOT IN " +
                                         "  ( " +
                                         "      SELECT ITEMID FROM H_RETURN_PURCHASE A " +
                                         "      JOIN D_RETURN_PURCHASE B " +
                                         "      ON A.RETURNPURCHASEID = B.RETURNPURCHASEID " +
                                         "      WHERE STATUS = '{0}' " +
                                         "  ) " +
                                         ")", PartNotextBox.Text, Status.ACTIVE);
            SupplierIDtextBox.AutoCompleteCustomSource.Clear();
            SupplierIDtextBox.Text = "";
            supplierID = new List<String>();
            SqlDataReader sqldataReader = connection.sqlDataReaders(query);

            while (sqldataReader.Read()) supplierID.Add(sqldataReader.GetString(0));

            SupplierIDtextBox.AutoCompleteCustomSource.AddRange(supplierID.ToArray());
            SupplierIDtextBox.Enabled = true;
            SearchSupplierIDbutton.Enabled = true;

            PurchaseNotextBox.Text = "";
            PurchaseNotextBox.Enabled = false;
            PurchaseDatetextBox.Text = "";
            QtyReturntextBox.Text = "0";
            QtyReceivetextBox.Text = "";

            QtyReturntextBox.Enabled = false;
            AddItembutton.Enabled = false;
        }

        Supplierdatagridview supplierdatagridview = null;
        private void SearchSupplierIDbutton_Click(object sender, EventArgs e)
        {
            if (supplierdatagridview == null)
            {
                supplierdatagridview = new Supplierdatagridview("PurchaseReturn",PartNotextBox.Text);
                supplierdatagridview.masterItemPassingData = new Supplierdatagridview.MasterItemPassingData(MasterSupplierPassingData);
                //supplierdatagridview.MdiParent = this.MdiParent;
                supplierdatagridview.ShowDialog();
            }
            else if (supplierdatagridview.IsDisposed)
            {
                supplierdatagridview = new Supplierdatagridview("PurchaseReturn",PartNotextBox.Text);
                supplierdatagridview.masterItemPassingData = new Supplierdatagridview.MasterItemPassingData(MasterSupplierPassingData);
                //supplierdatagridview.MdiParent = this.MdiParent;
                supplierdatagridview.ShowDialog();
            }
        }

        private void MasterSupplierPassingData(DataTable datatable) {
            SupplierIDtextBox.Text = datatable.Rows[0]["SUPPLIERID"].ToString();
            String query = String.Format("SELECT A.PURCHASEID FROM H_PURCHASE A " +
                                        "JOIN D_PURCHASE B " +
                                        "ON A.PURCHASEID = B.PURCHASEID " +
                                        "WHERE SUPPLIERID = '{0}' AND B.ITEMID = '{1}' " +
                                        "AND " +
                                        "( " +
                                        "   A.PURCHASEID NOT IN " +
                                        "   ( " +
                                        "       SELECT PURCHASEID FROM H_RETURN_PURCHASE A " +
                                        "       JOIN D_RETURN_PURCHASE B " +
                                        "       ON A.RETURNPURCHASEID = B.RETURNPURCHASEID " +
                                        "       WHERE STATUS = '{2}' " +
                                        "   ) " +
                                        "   OR B.ITEMID NOT IN " +
                                        "   ( " +
                                        "      SELECT ITEMID FROM H_RETURN_PURCHASE A " +
                                        "      JOIN D_RETURN_PURCHASE B " +
                                        "      ON A.RETURNPURCHASEID = B.RETURNPURCHASEID " +
                                        "      WHERE STATUS = '{2}' " +
                                        "   ) " +
                                        ")", SupplierIDtextBox.Text, PartNotextBox.Text, Status.ACTIVE);

            PurchaseNotextBox.AutoCompleteCustomSource.Clear();
            purchaseID = new List<String>();
            SqlDataReader sqldataReader = connection.sqlDataReaders(query);
            while (sqldataReader.Read()) purchaseID.Add(sqldataReader.GetString(0));
            PurchaseNotextBox.AutoCompleteCustomSource.AddRange(purchaseID.ToArray());
            PurchaseNotextBox.Enabled = true;
            SearchPurchaseNobutton.Enabled = true;

            QtyReturntextBox.Enabled = false;
            AddItembutton.Enabled = false;
            PurchaseNotextBox.Text = "";
            PurchaseDatetextBox.Text = "";
        }

        Purchasedatagridview purchasedatagridview = null;
        private void SearchPurchaseNobutton_Click(object sender, EventArgs e){
            if (purchasedatagridview == null)
            {
                purchasedatagridview = new Purchasedatagridview("PurchaseOrder");
                purchasedatagridview.purchasePassingData = new Purchasedatagridview.PurchasePassingData(ReceiptPassingData);
                //purchasedatagridview.MdiParent = this.MdiParent;
                purchasedatagridview.ShowDialog();
            }
            else if (purchasedatagridview.IsDisposed)
            {
                purchasedatagridview = new Purchasedatagridview("PurchaseOrder");
                purchasedatagridview.purchasePassingData = new Purchasedatagridview.PurchasePassingData(ReceiptPassingData);
                //purchasedatagridview.MdiParent = this.MdiParent;
                purchasedatagridview.ShowDialog();
            }
        }

        private void ReceiptPassingData(DataTable datatable) {
            PurchaseNotextBox.Text = datatable.Rows[0]["PURCHASEID"].ToString();
            PurchaseDatetextBox.Text = datatable.Rows[0]["PURCHASEDATE"].ToString();
            QtyReceivetextBox.Text = datatable.Rows[0]["QUANTITY"].ToString();
            QtyReceivetextBox.Enabled = false;
            QtyReturntextBox.Enabled = true;
            AddItembutton.Enabled = true;
        }

        public void confirm(){
            if (StatustextBox.Text.Equals(Status.CONFIRM)) {
                MessageBox.Show(String.Format("Purchase Return No {0} Sudah Di Confirm", PurchaseReturnNotextBox.Text));
                return;
            }

            if (PurchaseReturnNotextBox.Text.Contains("XXXXXX") || PurchaseReturnNotextBox.Text.Equals(""))
            {
                MessageBox.Show("Silakan Pilih Purchase Return No terlebih dahulu.");
            }
            else {
                DialogResult conf = MessageBox.Show(this, String.Format("Apakah Anda ingin Confirm Purchase Return No {0}?", PurchaseReturnNotextBox.Text), "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                if (conf == DialogResult.Yes)
                {
                    connection = new Connection();
                    DataTable dt = null;
                    dt = connection.openDataTableQuery(string.Format("SELECT * FROM H_PURCHASE_RETURN WHERE PURCHASERETURNID = '{0}'", PurchaseReturnNotextBox.Text));
                    if (dt.Rows.Count == 0)
                    {
                        MessageBox.Show(String.Format("Purchase Return No {0} yang ingin di Confirm tidak di temukan", PurchaseReturnNotextBox.Text));
                        PurchaseReturnNotextBox.Focus();
                        if (!PurchaseReturnNotextBox.Text.Equals(""))
                        {
                            PurchaseReturnNotextBox.SelectAll();
                        }
                    }
                    else
                    {
                        List<SqlParameter> sqlParam = new List<SqlParameter>();

                        sqlParam.Add(new SqlParameter("@PURCHASERETURNID", PurchaseReturnNotextBox.Text));
                        sqlParam.Add(new SqlParameter("@STATUS", Status.CONFIRM));
                        sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));

                        String proc = "UPDATE_STATUS_H_PURCHASE_RETURN";
                        //langsung execute
                        connection.callProcedureDatatable(proc, sqlParam);
                        MessageBox.Show(String.Format("Purchase Return No {0} Telah Di Confirm", PurchaseReturnNotextBox.Text));
                        reloadAllData();
                        newReturn();
                    }
                }
            }
        }
    }
}