﻿namespace Sufindo.Purchase
{
    partial class PurchaseOrderView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.PartNOtextBox = new System.Windows.Forms.TextBox();
            this.datagridviewpanel = new System.Windows.Forms.Panel();
            this.HistorydataGridView = new System.Windows.Forms.DataGridView();
            this.label6 = new System.Windows.Forms.Label();
            this.PartNametextBox = new System.Windows.Forms.TextBox();
            this.SearchPartNobutton = new System.Windows.Forms.Button();
            this.PONotextBox = new System.Windows.Forms.TextBox();
            this.PODatetextBox = new System.Windows.Forms.TextBox();
            this.SupplierNametextBox = new System.Windows.Forms.TextBox();
            this.ContactPersontextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.RemarkrichTextBox = new System.Windows.Forms.RichTextBox();
            this.Spesifikasipanel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SearchPONObutton = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxSLTotalAmount = new CustomControls.TextBoxSL();
            this.tableLayoutPanel.SuspendLayout();
            this.datagridviewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HistorydataGridView)).BeginInit();
            this.Spesifikasipanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 9;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.PartNOtextBox, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.datagridviewpanel, 0, 7);
            this.tableLayoutPanel.Controls.Add(this.label6, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.PartNametextBox, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.SearchPartNobutton, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.label1, 0, 9);
            this.tableLayoutPanel.Controls.Add(this.Spesifikasipanel, 1, 9);
            this.tableLayoutPanel.Controls.Add(this.SearchPONObutton, 8, 0);
            this.tableLayoutPanel.Controls.Add(this.PONotextBox, 7, 0);
            this.tableLayoutPanel.Controls.Add(this.PODatetextBox, 7, 1);
            this.tableLayoutPanel.Controls.Add(this.SupplierNametextBox, 7, 2);
            this.tableLayoutPanel.Controls.Add(this.ContactPersontextBox, 7, 5);
            this.tableLayoutPanel.Controls.Add(this.label9, 6, 5);
            this.tableLayoutPanel.Controls.Add(this.label5, 6, 2);
            this.tableLayoutPanel.Controls.Add(this.label3, 6, 1);
            this.tableLayoutPanel.Controls.Add(this.label2, 6, 0);
            this.tableLayoutPanel.Controls.Add(this.label7, 0, 8);
            this.tableLayoutPanel.Controls.Add(this.textBoxSLTotalAmount, 1, 8);
            this.tableLayoutPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.tableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 10;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(973, 513);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "PART NO";
            // 
            // PartNOtextBox
            // 
            this.PartNOtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PartNOtextBox.Location = new System.Drawing.Point(79, 3);
            this.PartNOtextBox.Name = "PartNOtextBox";
            this.PartNOtextBox.Size = new System.Drawing.Size(141, 20);
            this.PartNOtextBox.TabIndex = 1;
            this.PartNOtextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PartIDtextBox_KeyDown);
            this.PartNOtextBox.Leave += new System.EventHandler(this.PartIDtextBox_Leave);
            // 
            // datagridviewpanel
            // 
            this.tableLayoutPanel.SetColumnSpan(this.datagridviewpanel, 9);
            this.datagridviewpanel.Controls.Add(this.HistorydataGridView);
            this.datagridviewpanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.datagridviewpanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.datagridviewpanel.Location = new System.Drawing.Point(3, 109);
            this.datagridviewpanel.Name = "datagridviewpanel";
            this.datagridviewpanel.Size = new System.Drawing.Size(967, 240);
            this.datagridviewpanel.TabIndex = 21;
            // 
            // HistorydataGridView
            // 
            this.HistorydataGridView.AllowUserToAddRows = false;
            this.HistorydataGridView.AllowUserToDeleteRows = false;
            this.HistorydataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.HistorydataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HistorydataGridView.Location = new System.Drawing.Point(0, 0);
            this.HistorydataGridView.Name = "HistorydataGridView";
            this.HistorydataGridView.ReadOnly = true;
            this.HistorydataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.HistorydataGridView.Size = new System.Drawing.Size(967, 240);
            this.HistorydataGridView.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 28;
            this.label6.Text = "PART NAME";
            // 
            // PartNametextBox
            // 
            this.PartNametextBox.BackColor = System.Drawing.Color.White;
            this.PartNametextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PartNametextBox.Enabled = false;
            this.PartNametextBox.Location = new System.Drawing.Point(79, 31);
            this.PartNametextBox.Name = "PartNametextBox";
            this.PartNametextBox.Size = new System.Drawing.Size(141, 20);
            this.PartNametextBox.TabIndex = 29;
            // 
            // SearchPartNobutton
            // 
            this.SearchPartNobutton.BackColor = System.Drawing.Color.Transparent;
            this.SearchPartNobutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.SearchPartNobutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SearchPartNobutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.SearchPartNobutton.FlatAppearance.BorderSize = 0;
            this.SearchPartNobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchPartNobutton.ForeColor = System.Drawing.Color.Transparent;
            this.SearchPartNobutton.Location = new System.Drawing.Point(226, 1);
            this.SearchPartNobutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.SearchPartNobutton.Name = "SearchPartNobutton";
            this.SearchPartNobutton.Size = new System.Drawing.Size(24, 24);
            this.SearchPartNobutton.TabIndex = 27;
            this.SearchPartNobutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.SearchPartNobutton.UseVisualStyleBackColor = false;
            this.SearchPartNobutton.Click += new System.EventHandler(this.SearchPartNobutton_Click);
            // 
            // PONotextBox
            // 
            this.PONotextBox.BackColor = System.Drawing.Color.White;
            this.PONotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PONotextBox.Location = new System.Drawing.Point(824, 3);
            this.PONotextBox.Name = "PONotextBox";
            this.PONotextBox.Size = new System.Drawing.Size(116, 20);
            this.PONotextBox.TabIndex = 12;
            this.PONotextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PONotextBox_KeyDown);
            this.PONotextBox.Leave += new System.EventHandler(this.PONotextBox_Leave);
            // 
            // PODatetextBox
            // 
            this.PODatetextBox.BackColor = System.Drawing.Color.White;
            this.PODatetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PODatetextBox.Enabled = false;
            this.PODatetextBox.Location = new System.Drawing.Point(824, 31);
            this.PODatetextBox.Name = "PODatetextBox";
            this.PODatetextBox.Size = new System.Drawing.Size(116, 20);
            this.PODatetextBox.TabIndex = 13;
            // 
            // SupplierNametextBox
            // 
            this.SupplierNametextBox.BackColor = System.Drawing.Color.White;
            this.SupplierNametextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SupplierNametextBox.Enabled = false;
            this.SupplierNametextBox.Location = new System.Drawing.Point(824, 57);
            this.SupplierNametextBox.Name = "SupplierNametextBox";
            this.SupplierNametextBox.Size = new System.Drawing.Size(116, 20);
            this.SupplierNametextBox.TabIndex = 3;
            // 
            // ContactPersontextBox
            // 
            this.ContactPersontextBox.BackColor = System.Drawing.Color.White;
            this.ContactPersontextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ContactPersontextBox.Enabled = false;
            this.ContactPersontextBox.Location = new System.Drawing.Point(824, 83);
            this.ContactPersontextBox.Name = "ContactPersontextBox";
            this.ContactPersontextBox.Size = new System.Drawing.Size(116, 20);
            this.ContactPersontextBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(738, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "P.O No";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(738, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "P.O Date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(738, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Supplier Name";
            // 
            // RemarkrichTextBox
            // 
            this.RemarkrichTextBox.BackColor = System.Drawing.Color.White;
            this.RemarkrichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RemarkrichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RemarkrichTextBox.Location = new System.Drawing.Point(0, 0);
            this.RemarkrichTextBox.Name = "RemarkrichTextBox";
            this.RemarkrichTextBox.ReadOnly = true;
            this.RemarkrichTextBox.Size = new System.Drawing.Size(459, 126);
            this.RemarkrichTextBox.TabIndex = 9;
            this.RemarkrichTextBox.TabStop = false;
            this.RemarkrichTextBox.Text = "";
            // 
            // Spesifikasipanel
            // 
            this.Spesifikasipanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.Spesifikasipanel, 7);
            this.Spesifikasipanel.Controls.Add(this.RemarkrichTextBox);
            this.Spesifikasipanel.Location = new System.Drawing.Point(79, 381);
            this.Spesifikasipanel.Name = "Spesifikasipanel";
            this.Spesifikasipanel.Size = new System.Drawing.Size(461, 128);
            this.Spesifikasipanel.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 378);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Remark";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 352);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 30;
            this.label7.Text = "Total";
            this.label7.Visible = false;
            // 
            // SearchPONObutton
            // 
            this.SearchPONObutton.BackColor = System.Drawing.Color.Transparent;
            this.SearchPONObutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.SearchPONObutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SearchPONObutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.SearchPONObutton.FlatAppearance.BorderSize = 0;
            this.SearchPONObutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchPONObutton.ForeColor = System.Drawing.Color.Transparent;
            this.SearchPONObutton.Location = new System.Drawing.Point(946, 1);
            this.SearchPONObutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.SearchPONObutton.Name = "SearchPONObutton";
            this.SearchPONObutton.Size = new System.Drawing.Size(24, 24);
            this.SearchPONObutton.TabIndex = 26;
            this.SearchPONObutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.SearchPONObutton.UseVisualStyleBackColor = false;
            this.SearchPONObutton.Click += new System.EventHandler(this.SearchPONObutton_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(738, 80);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 13);
            this.label9.TabIndex = 32;
            this.label9.Text = "Contact Person";
            // 
            // textBoxSLTotalAmount
            // 
            this.textBoxSLTotalAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.textBoxSLTotalAmount, 2);
            this.textBoxSLTotalAmount.Decimal = false;
            this.textBoxSLTotalAmount.Location = new System.Drawing.Point(79, 355);
            this.textBoxSLTotalAmount.Money = true;
            this.textBoxSLTotalAmount.Name = "textBoxSLTotalAmount";
            this.textBoxSLTotalAmount.Numeric = false;
            this.textBoxSLTotalAmount.Prefix = "";
            this.textBoxSLTotalAmount.ReadOnly = true;
            this.textBoxSLTotalAmount.Size = new System.Drawing.Size(156, 20);
            this.textBoxSLTotalAmount.TabIndex = 31;
            this.textBoxSLTotalAmount.Visible = false;
            // 
            // PurchaseOrderView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(979, 521);
            this.Controls.Add(this.tableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PurchaseOrderView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PO View";
            this.Activated += new System.EventHandler(this.PurchaseOrderView_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.PurchaseOrder_FormClosed);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.datagridviewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.HistorydataGridView)).EndInit();
            this.Spesifikasipanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox PartNOtextBox;
        private System.Windows.Forms.TextBox SupplierNametextBox;
        private System.Windows.Forms.TextBox ContactPersontextBox;
        private System.Windows.Forms.TextBox PONotextBox;
        private System.Windows.Forms.TextBox PODatetextBox;
        private System.Windows.Forms.Panel datagridviewpanel;
        private System.Windows.Forms.DataGridView HistorydataGridView;
        private System.Windows.Forms.Button SearchPartNobutton;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox PartNametextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel Spesifikasipanel;
        private System.Windows.Forms.RichTextBox RemarkrichTextBox;
        private System.Windows.Forms.Button SearchPONObutton;
        private System.Windows.Forms.Label label9;
        private CustomControls.TextBoxSL textBoxSLTotalAmount;
    }
}