﻿namespace Sufindo.Purchase
{
    partial class PurchaseOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PurchaseOrder));
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.SupplierIDtextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SupplierNametextBox = new System.Windows.Forms.TextBox();
            this.ContactPersontextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.KurstextBox = new System.Windows.Forms.TextBox();
            this.AddItembutton = new System.Windows.Forms.Button();
            this.datagridviewpanel = new System.Windows.Forms.Panel();
            this.MasterItemdataGridView = new System.Windows.Forms.DataGridView();
            this.NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMID_ALIAS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMNAME_ALIAS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUANTITY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UNITPRICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REMOVE = new System.Windows.Forms.DataGridViewImageColumn();
            this.Spesifikasipanel = new System.Windows.Forms.Panel();
            this.RemarkrichTextBox = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.searchKursbutton = new System.Windows.Forms.Button();
            this.ExportExcelbutton = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.searchContactPersonbutton = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.searchSupplierNobutton = new System.Windows.Forms.Button();
            this.StatustextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.PODatetextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.PONotextBox = new System.Windows.Forms.TextBox();
            this.SearchPONObutton = new System.Windows.Forms.Button();
            this.copyPObutton = new System.Windows.Forms.Button();
            this.RatetextBox = new CustomControls.TextBoxSL();
            this.TotaltextBox = new CustomControls.TextBoxSL();
            this.tableLayoutPanel.SuspendLayout();
            this.datagridviewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MasterItemdataGridView)).BeginInit();
            this.Spesifikasipanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 8;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.SupplierIDtextBox, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.label5, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.SupplierNametextBox, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.ContactPersontextBox, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.label6, 0, 4);
            this.tableLayoutPanel.Controls.Add(this.label7, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.label8, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.KurstextBox, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.AddItembutton, 0, 5);
            this.tableLayoutPanel.Controls.Add(this.datagridviewpanel, 0, 6);
            this.tableLayoutPanel.Controls.Add(this.Spesifikasipanel, 1, 8);
            this.tableLayoutPanel.Controls.Add(this.label1, 0, 8);
            this.tableLayoutPanel.Controls.Add(this.searchKursbutton, 2, 3);
            this.tableLayoutPanel.Controls.Add(this.ExportExcelbutton, 7, 8);
            this.tableLayoutPanel.Controls.Add(this.label10, 6, 7);
            this.tableLayoutPanel.Controls.Add(this.searchContactPersonbutton, 2, 2);
            this.tableLayoutPanel.Controls.Add(this.label9, 3, 2);
            this.tableLayoutPanel.Controls.Add(this.searchSupplierNobutton, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.StatustextBox, 4, 2);
            this.tableLayoutPanel.Controls.Add(this.label3, 3, 1);
            this.tableLayoutPanel.Controls.Add(this.PODatetextBox, 4, 1);
            this.tableLayoutPanel.Controls.Add(this.label2, 3, 0);
            this.tableLayoutPanel.Controls.Add(this.PONotextBox, 4, 0);
            this.tableLayoutPanel.Controls.Add(this.SearchPONObutton, 6, 0);
            this.tableLayoutPanel.Controls.Add(this.copyPObutton, 7, 0);
            this.tableLayoutPanel.Controls.Add(this.RatetextBox, 1, 4);
            this.tableLayoutPanel.Controls.Add(this.TotaltextBox, 7, 7);
            this.tableLayoutPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.tableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 9;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.Size = new System.Drawing.Size(978, 577);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Supplier ID";
            // 
            // SupplierIDtextBox
            // 
            this.SupplierIDtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SupplierIDtextBox.Location = new System.Drawing.Point(89, 3);
            this.SupplierIDtextBox.Name = "SupplierIDtextBox";
            this.SupplierIDtextBox.Size = new System.Drawing.Size(176, 20);
            this.SupplierIDtextBox.TabIndex = 1;
            this.SupplierIDtextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SupplierIDtextBox_KeyDown);
            this.SupplierIDtextBox.Leave += new System.EventHandler(this.SupplierIDtextBox_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Supplier Name";
            // 
            // SupplierNametextBox
            // 
            this.SupplierNametextBox.BackColor = System.Drawing.Color.White;
            this.SupplierNametextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SupplierNametextBox.Enabled = false;
            this.SupplierNametextBox.Location = new System.Drawing.Point(89, 31);
            this.SupplierNametextBox.Name = "SupplierNametextBox";
            this.SupplierNametextBox.Size = new System.Drawing.Size(176, 20);
            this.SupplierNametextBox.TabIndex = 3;
            // 
            // ContactPersontextBox
            // 
            this.ContactPersontextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ContactPersontextBox.Enabled = false;
            this.ContactPersontextBox.Location = new System.Drawing.Point(89, 57);
            this.ContactPersontextBox.Name = "ContactPersontextBox";
            this.ContactPersontextBox.Size = new System.Drawing.Size(176, 20);
            this.ContactPersontextBox.TabIndex = 3;
            this.ContactPersontextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ContactPersontextBox_KeyDown);
            this.ContactPersontextBox.Leave += new System.EventHandler(this.ContactPersontextBox_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 110);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Rate";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 82);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Kurs";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 54);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Contact Person";
            // 
            // KurstextBox
            // 
            this.KurstextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.KurstextBox.Location = new System.Drawing.Point(89, 85);
            this.KurstextBox.Name = "KurstextBox";
            this.KurstextBox.Size = new System.Drawing.Size(176, 20);
            this.KurstextBox.TabIndex = 5;
            this.KurstextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KurstextBox_KeyDown);
            this.KurstextBox.Leave += new System.EventHandler(this.KurstextBox_Leave);
            // 
            // AddItembutton
            // 
            this.AddItembutton.BackColor = System.Drawing.Color.Transparent;
            this.AddItembutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AddItembutton.Enabled = false;
            this.AddItembutton.FlatAppearance.BorderSize = 0;
            this.AddItembutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddItembutton.ForeColor = System.Drawing.Color.Transparent;
            this.AddItembutton.Image = global::Sufindo.Properties.Resources.plus;
            this.AddItembutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.AddItembutton.Location = new System.Drawing.Point(0, 136);
            this.AddItembutton.Margin = new System.Windows.Forms.Padding(0);
            this.AddItembutton.Name = "AddItembutton";
            this.AddItembutton.Size = new System.Drawing.Size(83, 32);
            this.AddItembutton.TabIndex = 8;
            this.AddItembutton.Text = "Add Item";
            this.AddItembutton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.AddItembutton.UseVisualStyleBackColor = false;
            this.AddItembutton.Click += new System.EventHandler(this.AddItembutton_Click);
            // 
            // datagridviewpanel
            // 
            this.tableLayoutPanel.SetColumnSpan(this.datagridviewpanel, 8);
            this.datagridviewpanel.Controls.Add(this.MasterItemdataGridView);
            this.datagridviewpanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.datagridviewpanel.Location = new System.Drawing.Point(3, 171);
            this.datagridviewpanel.Name = "datagridviewpanel";
            this.datagridviewpanel.Size = new System.Drawing.Size(966, 240);
            this.datagridviewpanel.TabIndex = 21;
            // 
            // MasterItemdataGridView
            // 
            this.MasterItemdataGridView.AllowUserToAddRows = false;
            this.MasterItemdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.MasterItemdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NO,
            this.ITEMID,
            this.ITEMNAME,
            this.ITEMID_ALIAS,
            this.ITEMNAME_ALIAS,
            this.QUANTITY,
            this.UOMID,
            this.UNITPRICE,
            this.AMOUNT,
            this.REMOVE});
            this.MasterItemdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MasterItemdataGridView.Location = new System.Drawing.Point(0, 0);
            this.MasterItemdataGridView.Name = "MasterItemdataGridView";
            this.MasterItemdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.MasterItemdataGridView.Size = new System.Drawing.Size(966, 240);
            this.MasterItemdataGridView.TabIndex = 0;
            this.MasterItemdataGridView.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.MasterItemdataGridView_CellValidated);
            this.MasterItemdataGridView.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.MasterItemdataGridView_UserDeletedRow);
            this.MasterItemdataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.MasterItemdataGridView_CellFormatting);
            this.MasterItemdataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MasterItemdataGridView_CellClick);
            this.MasterItemdataGridView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.MasterItemdataGridView_EditingControlShowing);
            // 
            // NO
            // 
            this.NO.DataPropertyName = "NO";
            this.NO.FillWeight = 40F;
            this.NO.HeaderText = "NO";
            this.NO.Name = "NO";
            this.NO.ReadOnly = true;
            this.NO.Width = 40;
            // 
            // ITEMID
            // 
            this.ITEMID.DataPropertyName = "ITEMID";
            this.ITEMID.HeaderText = "PART NO";
            this.ITEMID.Name = "ITEMID";
            this.ITEMID.ReadOnly = true;
            this.ITEMID.Width = 120;
            // 
            // ITEMNAME
            // 
            this.ITEMNAME.DataPropertyName = "ITEMNAME";
            this.ITEMNAME.FillWeight = 120F;
            this.ITEMNAME.HeaderText = "PART NAME";
            this.ITEMNAME.Name = "ITEMNAME";
            this.ITEMNAME.ReadOnly = true;
            this.ITEMNAME.Width = 120;
            // 
            // ITEMID_ALIAS
            // 
            this.ITEMID_ALIAS.DataPropertyName = "ITEMID_ALIAS";
            this.ITEMID_ALIAS.HeaderText = "PART NO QTY";
            this.ITEMID_ALIAS.Name = "ITEMID_ALIAS";
            this.ITEMID_ALIAS.ReadOnly = true;
            // 
            // ITEMNAME_ALIAS
            // 
            this.ITEMNAME_ALIAS.DataPropertyName = "ITEMNAME_ALIAS";
            this.ITEMNAME_ALIAS.FillWeight = 120F;
            this.ITEMNAME_ALIAS.HeaderText = "PART NAME QTY";
            this.ITEMNAME_ALIAS.Name = "ITEMNAME_ALIAS";
            this.ITEMNAME_ALIAS.Width = 120;
            // 
            // QUANTITY
            // 
            this.QUANTITY.DataPropertyName = "QUANTITY";
            this.QUANTITY.FillWeight = 70F;
            this.QUANTITY.HeaderText = "QTY";
            this.QUANTITY.Name = "QUANTITY";
            this.QUANTITY.Width = 70;
            // 
            // UOMID
            // 
            this.UOMID.DataPropertyName = "UOMID";
            this.UOMID.HeaderText = "UOM";
            this.UOMID.Name = "UOMID";
            this.UOMID.ReadOnly = true;
            this.UOMID.Width = 60;
            // 
            // UNITPRICE
            // 
            this.UNITPRICE.DataPropertyName = "UNITPRICE";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.UNITPRICE.DefaultCellStyle = dataGridViewCellStyle4;
            this.UNITPRICE.HeaderText = "UNIT PRICE";
            this.UNITPRICE.Name = "UNITPRICE";
            // 
            // AMOUNT
            // 
            this.AMOUNT.DataPropertyName = "AMOUNT";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.NullValue = null;
            this.AMOUNT.DefaultCellStyle = dataGridViewCellStyle5;
            this.AMOUNT.HeaderText = "AMOUNT";
            this.AMOUNT.Name = "AMOUNT";
            this.AMOUNT.ReadOnly = true;
            // 
            // REMOVE
            // 
            this.REMOVE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.NullValue = ((object)(resources.GetObject("dataGridViewCellStyle6.NullValue")));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.White;
            this.REMOVE.DefaultCellStyle = dataGridViewCellStyle6;
            this.REMOVE.FillWeight = 24F;
            this.REMOVE.HeaderText = "";
            this.REMOVE.Image = global::Sufindo.Properties.Resources.minus;
            this.REMOVE.MinimumWidth = 24;
            this.REMOVE.Name = "REMOVE";
            this.REMOVE.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.REMOVE.Width = 24;
            // 
            // Spesifikasipanel
            // 
            this.Spesifikasipanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.Spesifikasipanel, 6);
            this.Spesifikasipanel.Controls.Add(this.RemarkrichTextBox);
            this.Spesifikasipanel.Location = new System.Drawing.Point(89, 454);
            this.Spesifikasipanel.Name = "Spesifikasipanel";
            this.Spesifikasipanel.Size = new System.Drawing.Size(430, 106);
            this.Spesifikasipanel.TabIndex = 9;
            // 
            // RemarkrichTextBox
            // 
            this.RemarkrichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RemarkrichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RemarkrichTextBox.Location = new System.Drawing.Point(0, 0);
            this.RemarkrichTextBox.Name = "RemarkrichTextBox";
            this.RemarkrichTextBox.Size = new System.Drawing.Size(428, 104);
            this.RemarkrichTextBox.TabIndex = 9;
            this.RemarkrichTextBox.TabStop = false;
            this.RemarkrichTextBox.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 451);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Remark";
            // 
            // searchKursbutton
            // 
            this.searchKursbutton.BackColor = System.Drawing.Color.Transparent;
            this.searchKursbutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchKursbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchKursbutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.searchKursbutton.FlatAppearance.BorderSize = 0;
            this.searchKursbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchKursbutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchKursbutton.Location = new System.Drawing.Point(271, 83);
            this.searchKursbutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.searchKursbutton.Name = "searchKursbutton";
            this.searchKursbutton.Size = new System.Drawing.Size(24, 24);
            this.searchKursbutton.TabIndex = 6;
            this.searchKursbutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.searchKursbutton.UseVisualStyleBackColor = false;
            this.searchKursbutton.Click += new System.EventHandler(this.searchKursbutton_Click);
            // 
            // ExportExcelbutton
            // 
            this.ExportExcelbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExportExcelbutton.Location = new System.Drawing.Point(596, 452);
            this.ExportExcelbutton.Margin = new System.Windows.Forms.Padding(1);
            this.ExportExcelbutton.Name = "ExportExcelbutton";
            this.ExportExcelbutton.Size = new System.Drawing.Size(74, 22);
            this.ExportExcelbutton.TabIndex = 27;
            this.ExportExcelbutton.Text = "Export Excel";
            this.ExportExcelbutton.UseVisualStyleBackColor = true;
            this.ExportExcelbutton.Visible = false;
            this.ExportExcelbutton.Click += new System.EventHandler(this.ImportExcelbutton_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(532, 414);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 25);
            this.label10.TabIndex = 24;
            this.label10.Text = "Total";
            // 
            // searchContactPersonbutton
            // 
            this.searchContactPersonbutton.BackColor = System.Drawing.Color.Transparent;
            this.searchContactPersonbutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchContactPersonbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchContactPersonbutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.searchContactPersonbutton.FlatAppearance.BorderSize = 0;
            this.searchContactPersonbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchContactPersonbutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchContactPersonbutton.Location = new System.Drawing.Point(271, 55);
            this.searchContactPersonbutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.searchContactPersonbutton.Name = "searchContactPersonbutton";
            this.searchContactPersonbutton.Size = new System.Drawing.Size(24, 24);
            this.searchContactPersonbutton.TabIndex = 4;
            this.searchContactPersonbutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.searchContactPersonbutton.UseVisualStyleBackColor = false;
            this.searchContactPersonbutton.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(301, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Status";
            // 
            // searchSupplierNobutton
            // 
            this.searchSupplierNobutton.BackColor = System.Drawing.Color.Transparent;
            this.searchSupplierNobutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchSupplierNobutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchSupplierNobutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.searchSupplierNobutton.FlatAppearance.BorderSize = 0;
            this.searchSupplierNobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchSupplierNobutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchSupplierNobutton.Location = new System.Drawing.Point(271, 1);
            this.searchSupplierNobutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.searchSupplierNobutton.Name = "searchSupplierNobutton";
            this.searchSupplierNobutton.Size = new System.Drawing.Size(24, 24);
            this.searchSupplierNobutton.TabIndex = 2;
            this.searchSupplierNobutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.searchSupplierNobutton.UseVisualStyleBackColor = false;
            this.searchSupplierNobutton.Click += new System.EventHandler(this.searchSupplierNobutton_Click);
            // 
            // StatustextBox
            // 
            this.StatustextBox.BackColor = System.Drawing.Color.White;
            this.StatustextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.StatustextBox, 2);
            this.StatustextBox.Enabled = false;
            this.StatustextBox.Location = new System.Drawing.Point(358, 57);
            this.StatustextBox.Name = "StatustextBox";
            this.StatustextBox.Size = new System.Drawing.Size(140, 20);
            this.StatustextBox.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(301, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "P.O Date";
            // 
            // PODatetextBox
            // 
            this.PODatetextBox.BackColor = System.Drawing.Color.White;
            this.PODatetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.PODatetextBox, 2);
            this.PODatetextBox.Enabled = false;
            this.PODatetextBox.Location = new System.Drawing.Point(358, 31);
            this.PODatetextBox.Name = "PODatetextBox";
            this.PODatetextBox.Size = new System.Drawing.Size(140, 20);
            this.PODatetextBox.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(301, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "P.O No";
            // 
            // PONotextBox
            // 
            this.PONotextBox.BackColor = System.Drawing.Color.White;
            this.PONotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.PONotextBox, 2);
            this.PONotextBox.Enabled = false;
            this.PONotextBox.Location = new System.Drawing.Point(358, 3);
            this.PONotextBox.Name = "PONotextBox";
            this.PONotextBox.Size = new System.Drawing.Size(168, 20);
            this.PONotextBox.TabIndex = 12;
            this.PONotextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PONotextBox_KeyDown);
            this.PONotextBox.Leave += new System.EventHandler(this.PONotextBox_Leave);
            // 
            // SearchPONObutton
            // 
            this.SearchPONObutton.BackColor = System.Drawing.Color.Transparent;
            this.SearchPONObutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.SearchPONObutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SearchPONObutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.SearchPONObutton.FlatAppearance.BorderSize = 0;
            this.SearchPONObutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchPONObutton.ForeColor = System.Drawing.Color.Transparent;
            this.SearchPONObutton.Location = new System.Drawing.Point(532, 1);
            this.SearchPONObutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.SearchPONObutton.Name = "SearchPONObutton";
            this.SearchPONObutton.Size = new System.Drawing.Size(24, 24);
            this.SearchPONObutton.TabIndex = 26;
            this.SearchPONObutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.SearchPONObutton.UseVisualStyleBackColor = false;
            this.SearchPONObutton.Visible = false;
            this.SearchPONObutton.Click += new System.EventHandler(this.SearchPONObutton_Click);
            // 
            // copyPObutton
            // 
            this.copyPObutton.Location = new System.Drawing.Point(598, 3);
            this.copyPObutton.Name = "copyPObutton";
            this.copyPObutton.Size = new System.Drawing.Size(72, 22);
            this.copyPObutton.TabIndex = 12;
            this.copyPObutton.Text = "Copy PO";
            this.copyPObutton.UseVisualStyleBackColor = true;
            this.copyPObutton.Visible = false;
            this.copyPObutton.Click += new System.EventHandler(this.copyPObutton_Click);
            // 
            // RatetextBox
            // 
            this.RatetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RatetextBox.Decimal = false;
            this.RatetextBox.Location = new System.Drawing.Point(89, 113);
            this.RatetextBox.Money = true;
            this.RatetextBox.Name = "RatetextBox";
            this.RatetextBox.Numeric = false;
            this.RatetextBox.Prefix = "";
            this.RatetextBox.Size = new System.Drawing.Size(176, 20);
            this.RatetextBox.TabIndex = 7;
            // 
            // TotaltextBox
            // 
            this.TotaltextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TotaltextBox.Decimal = false;
            this.TotaltextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotaltextBox.Location = new System.Drawing.Point(598, 417);
            this.TotaltextBox.Money = true;
            this.TotaltextBox.Name = "TotaltextBox";
            this.TotaltextBox.Numeric = false;
            this.TotaltextBox.Prefix = "";
            this.TotaltextBox.Size = new System.Drawing.Size(185, 31);
            this.TotaltextBox.TabIndex = 25;
            // 
            // PurchaseOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 584);
            this.Controls.Add(this.tableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PurchaseOrder";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Purchase Order";
            this.Activated += new System.EventHandler(this.PurchaseOrder_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.PurchaseOrder_FormClosed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PurchaseOrder_FormClosing);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.datagridviewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MasterItemdataGridView)).EndInit();
            this.Spesifikasipanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox SupplierIDtextBox;
        private System.Windows.Forms.TextBox SupplierNametextBox;
        private System.Windows.Forms.TextBox ContactPersontextBox;
        private System.Windows.Forms.TextBox PONotextBox;
        private System.Windows.Forms.TextBox PODatetextBox;
        private System.Windows.Forms.TextBox StatustextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox KurstextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button AddItembutton;
        private System.Windows.Forms.Panel datagridviewpanel;
        private System.Windows.Forms.DataGridView MasterItemdataGridView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel Spesifikasipanel;
        private System.Windows.Forms.RichTextBox RemarkrichTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button searchSupplierNobutton;
        private System.Windows.Forms.Button searchKursbutton;
        private System.Windows.Forms.Button searchContactPersonbutton;
        private System.Windows.Forms.Button SearchPONObutton;
        private System.Windows.Forms.Button ExportExcelbutton;
        private System.Windows.Forms.Button copyPObutton;
        private CustomControls.TextBoxSL RatetextBox;
        private CustomControls.TextBoxSL TotaltextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMID_ALIAS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMNAME_ALIAS;
        private System.Windows.Forms.DataGridViewTextBoxColumn QUANTITY;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn UNITPRICE;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT;
        private System.Windows.Forms.DataGridViewImageColumn REMOVE;
    }
}