﻿namespace Sufindo.Purchase
{
    partial class PurchaseReceiptRevision
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PurchaseReceiptRevision));
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.RevisiNotextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Spesifikasipanel = new System.Windows.Forms.Panel();
            this.RemarkrichTextBox = new System.Windows.Forms.RichTextBox();
            this.datagridviewpanel = new System.Windows.Forms.Panel();
            this.MasterItemdataGridView = new System.Windows.Forms.DataGridView();
            this.NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMIDQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMNAMEQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QTYORDER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QTYREVISI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UNITPRICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.del = new System.Windows.Forms.DataGridViewImageColumn();
            this.AddItembutton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.KurstextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.ContactPersontextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SupplierNametextBox = new System.Windows.Forms.TextBox();
            this.SearchRevisiNObutton = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.ReceiptDatetextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ReceiptNotextBox = new System.Windows.Forms.TextBox();
            this.SearchRcvNObutton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.RevisionDatetextBox = new System.Windows.Forms.TextBox();
            this.RatetextBox = new CustomControls.TextBoxSL();
            this.TotaltextBox = new CustomControls.TextBoxSL();
            this.tableLayoutPanel.SuspendLayout();
            this.Spesifikasipanel.SuspendLayout();
            this.datagridviewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MasterItemdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 10;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 73F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 79F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 82F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.RevisiNotextBox, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.label1, 0, 9);
            this.tableLayoutPanel.Controls.Add(this.Spesifikasipanel, 1, 9);
            this.tableLayoutPanel.Controls.Add(this.datagridviewpanel, 0, 7);
            this.tableLayoutPanel.Controls.Add(this.AddItembutton, 0, 6);
            this.tableLayoutPanel.Controls.Add(this.label6, 0, 5);
            this.tableLayoutPanel.Controls.Add(this.label7, 0, 4);
            this.tableLayoutPanel.Controls.Add(this.KurstextBox, 1, 4);
            this.tableLayoutPanel.Controls.Add(this.label8, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.ContactPersontextBox, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.SupplierNametextBox, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.SearchRevisiNObutton, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.label9, 4, 1);
            this.tableLayoutPanel.Controls.Add(this.ReceiptDatetextBox, 5, 1);
            this.tableLayoutPanel.Controls.Add(this.label10, 8, 8);
            this.tableLayoutPanel.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.ReceiptNotextBox, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.SearchRcvNObutton, 2, 1);
            this.tableLayoutPanel.Controls.Add(this.label3, 4, 0);
            this.tableLayoutPanel.Controls.Add(this.RevisionDatetextBox, 5, 0);
            this.tableLayoutPanel.Controls.Add(this.RatetextBox, 1, 5);
            this.tableLayoutPanel.Controls.Add(this.TotaltextBox, 9, 8);
            this.tableLayoutPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.tableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 10;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.Size = new System.Drawing.Size(1096, 531);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Receipt Revision No";
            // 
            // RevisiNotextBox
            // 
            this.RevisiNotextBox.BackColor = System.Drawing.Color.White;
            this.RevisiNotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RevisiNotextBox.Location = new System.Drawing.Point(114, 3);
            this.RevisiNotextBox.Name = "RevisiNotextBox";
            this.RevisiNotextBox.Size = new System.Drawing.Size(176, 20);
            this.RevisiNotextBox.TabIndex = 12;
            this.RevisiNotextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RevisiNotextBox_KeyDown);
            this.RevisiNotextBox.Leave += new System.EventHandler(this.RevisiNotextBox_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 408);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Remark";
            // 
            // Spesifikasipanel
            // 
            this.Spesifikasipanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.Spesifikasipanel, 5);
            this.Spesifikasipanel.Controls.Add(this.RemarkrichTextBox);
            this.Spesifikasipanel.Location = new System.Drawing.Point(114, 411);
            this.Spesifikasipanel.Name = "Spesifikasipanel";
            this.Spesifikasipanel.Size = new System.Drawing.Size(428, 109);
            this.Spesifikasipanel.TabIndex = 9;
            // 
            // RemarkrichTextBox
            // 
            this.RemarkrichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RemarkrichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RemarkrichTextBox.Location = new System.Drawing.Point(0, 0);
            this.RemarkrichTextBox.Name = "RemarkrichTextBox";
            this.RemarkrichTextBox.Size = new System.Drawing.Size(426, 107);
            this.RemarkrichTextBox.TabIndex = 9;
            this.RemarkrichTextBox.TabStop = false;
            this.RemarkrichTextBox.Text = "";
            // 
            // datagridviewpanel
            // 
            this.datagridviewpanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.SetColumnSpan(this.datagridviewpanel, 10);
            this.datagridviewpanel.Controls.Add(this.MasterItemdataGridView);
            this.datagridviewpanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.datagridviewpanel.Location = new System.Drawing.Point(3, 199);
            this.datagridviewpanel.Name = "datagridviewpanel";
            this.datagridviewpanel.Size = new System.Drawing.Size(1090, 169);
            this.datagridviewpanel.TabIndex = 21;
            // 
            // MasterItemdataGridView
            // 
            this.MasterItemdataGridView.AllowUserToAddRows = false;
            this.MasterItemdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.MasterItemdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NO,
            this.ITEMID,
            this.ITEMNAME,
            this.ITEMIDQTY,
            this.ITEMNAMEQTY,
            this.QTYORDER,
            this.QTYREVISI,
            this.UOMID,
            this.UNITPRICE,
            this.AMOUNT,
            this.del});
            this.MasterItemdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MasterItemdataGridView.Location = new System.Drawing.Point(0, 0);
            this.MasterItemdataGridView.Name = "MasterItemdataGridView";
            this.MasterItemdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.MasterItemdataGridView.Size = new System.Drawing.Size(1090, 169);
            this.MasterItemdataGridView.TabIndex = 0;
            this.MasterItemdataGridView.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.MasterItemdataGridView_CellValidated);
            this.MasterItemdataGridView.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.MasterItemdataGridView_UserDeletedRow);
            this.MasterItemdataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.MasterItemdataGridView_CellFormatting);
            this.MasterItemdataGridView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.MasterItemdataGridView_EditingControlShowing);
            this.MasterItemdataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MasterItemdataGridView_CellContentClick);
            // 
            // NO
            // 
            this.NO.DataPropertyName = "NO";
            this.NO.FillWeight = 50F;
            this.NO.HeaderText = "NO";
            this.NO.MinimumWidth = 50;
            this.NO.Name = "NO";
            this.NO.Width = 50;
            // 
            // ITEMID
            // 
            this.ITEMID.DataPropertyName = "ITEMID";
            this.ITEMID.FillWeight = 150F;
            this.ITEMID.HeaderText = "PART NO";
            this.ITEMID.Name = "ITEMID";
            this.ITEMID.Width = 150;
            // 
            // ITEMNAME
            // 
            this.ITEMNAME.DataPropertyName = "ITEMNAME";
            this.ITEMNAME.FillWeight = 150F;
            this.ITEMNAME.HeaderText = "PART NAME";
            this.ITEMNAME.Name = "ITEMNAME";
            this.ITEMNAME.Width = 150;
            // 
            // ITEMIDQTY
            // 
            this.ITEMIDQTY.DataPropertyName = "ITEMIDQTY";
            this.ITEMIDQTY.FillWeight = 110F;
            this.ITEMIDQTY.HeaderText = "PART NO QTY";
            this.ITEMIDQTY.Name = "ITEMIDQTY";
            this.ITEMIDQTY.Width = 110;
            // 
            // ITEMNAMEQTY
            // 
            this.ITEMNAMEQTY.DataPropertyName = "ITEMNAMEQTY";
            this.ITEMNAMEQTY.FillWeight = 120F;
            this.ITEMNAMEQTY.HeaderText = "PART NAME QTY";
            this.ITEMNAMEQTY.Name = "ITEMNAMEQTY";
            this.ITEMNAMEQTY.Width = 120;
            // 
            // QTYORDER
            // 
            this.QTYORDER.DataPropertyName = "QTYORDER";
            this.QTYORDER.HeaderText = "QTY ORDER";
            this.QTYORDER.Name = "QTYORDER";
            // 
            // QTYREVISI
            // 
            this.QTYREVISI.DataPropertyName = "QTYREVISI";
            this.QTYREVISI.HeaderText = "QTY REVISION";
            this.QTYREVISI.Name = "QTYREVISI";
            // 
            // UOMID
            // 
            this.UOMID.DataPropertyName = "UOMID";
            this.UOMID.FillWeight = 70F;
            this.UOMID.HeaderText = "UOM";
            this.UOMID.Name = "UOMID";
            this.UOMID.Width = 70;
            // 
            // UNITPRICE
            // 
            this.UNITPRICE.DataPropertyName = "UNITPRICE";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle1.Format = "N2";
            this.UNITPRICE.DefaultCellStyle = dataGridViewCellStyle1;
            this.UNITPRICE.FillWeight = 130F;
            this.UNITPRICE.HeaderText = "UNIT PRICE";
            this.UNITPRICE.Name = "UNITPRICE";
            this.UNITPRICE.Width = 130;
            // 
            // AMOUNT
            // 
            this.AMOUNT.DataPropertyName = "AMOUNT";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle2.Format = "N2";
            this.AMOUNT.DefaultCellStyle = dataGridViewCellStyle2;
            this.AMOUNT.FillWeight = 150F;
            this.AMOUNT.HeaderText = "AMOUNT";
            this.AMOUNT.Name = "AMOUNT";
            this.AMOUNT.Width = 150;
            // 
            // del
            // 
            this.del.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.del.DataPropertyName = "Del";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.NullValue = ((object)(resources.GetObject("dataGridViewCellStyle3.NullValue")));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            this.del.DefaultCellStyle = dataGridViewCellStyle3;
            this.del.FillWeight = 24F;
            this.del.HeaderText = "";
            this.del.Image = global::Sufindo.Properties.Resources.minus;
            this.del.MinimumWidth = 24;
            this.del.Name = "del";
            this.del.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.del.Width = 24;
            // 
            // AddItembutton
            // 
            this.AddItembutton.BackColor = System.Drawing.Color.Transparent;
            this.AddItembutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AddItembutton.Enabled = false;
            this.AddItembutton.FlatAppearance.BorderSize = 0;
            this.AddItembutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddItembutton.ForeColor = System.Drawing.Color.Transparent;
            this.AddItembutton.Image = global::Sufindo.Properties.Resources.plus;
            this.AddItembutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.AddItembutton.Location = new System.Drawing.Point(0, 164);
            this.AddItembutton.Margin = new System.Windows.Forms.Padding(0);
            this.AddItembutton.Name = "AddItembutton";
            this.AddItembutton.Size = new System.Drawing.Size(83, 32);
            this.AddItembutton.TabIndex = 8;
            this.AddItembutton.Text = "Add Item";
            this.AddItembutton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.AddItembutton.UseVisualStyleBackColor = false;
            this.AddItembutton.Click += new System.EventHandler(this.AddItembutton_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 138);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Rate";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 110);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Kurs";
            // 
            // KurstextBox
            // 
            this.KurstextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.KurstextBox.Location = new System.Drawing.Point(114, 113);
            this.KurstextBox.Name = "KurstextBox";
            this.KurstextBox.Size = new System.Drawing.Size(176, 20);
            this.KurstextBox.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 84);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Contact Person";
            // 
            // ContactPersontextBox
            // 
            this.ContactPersontextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ContactPersontextBox.Enabled = false;
            this.ContactPersontextBox.Location = new System.Drawing.Point(114, 87);
            this.ContactPersontextBox.Name = "ContactPersontextBox";
            this.ContactPersontextBox.Size = new System.Drawing.Size(176, 20);
            this.ContactPersontextBox.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Supplier Name";
            // 
            // SupplierNametextBox
            // 
            this.SupplierNametextBox.BackColor = System.Drawing.Color.White;
            this.SupplierNametextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SupplierNametextBox.Enabled = false;
            this.SupplierNametextBox.Location = new System.Drawing.Point(114, 59);
            this.SupplierNametextBox.Name = "SupplierNametextBox";
            this.SupplierNametextBox.Size = new System.Drawing.Size(176, 20);
            this.SupplierNametextBox.TabIndex = 3;
            // 
            // SearchRevisiNObutton
            // 
            this.SearchRevisiNObutton.BackColor = System.Drawing.Color.Transparent;
            this.SearchRevisiNObutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.SearchRevisiNObutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SearchRevisiNObutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.SearchRevisiNObutton.FlatAppearance.BorderSize = 0;
            this.SearchRevisiNObutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchRevisiNObutton.Location = new System.Drawing.Point(296, 1);
            this.SearchRevisiNObutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.SearchRevisiNObutton.Name = "SearchRevisiNObutton";
            this.SearchRevisiNObutton.Size = new System.Drawing.Size(24, 24);
            this.SearchRevisiNObutton.TabIndex = 26;
            this.SearchRevisiNObutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.SearchRevisiNObutton.UseVisualStyleBackColor = false;
            this.SearchRevisiNObutton.Click += new System.EventHandler(this.SearchRevisiNObutton_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(399, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 13);
            this.label9.TabIndex = 27;
            this.label9.Text = "Receipt Date";
            // 
            // ReceiptDatetextBox
            // 
            this.ReceiptDatetextBox.BackColor = System.Drawing.Color.White;
            this.ReceiptDatetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ReceiptDatetextBox.Enabled = false;
            this.ReceiptDatetextBox.Location = new System.Drawing.Point(519, 31);
            this.ReceiptDatetextBox.Name = "ReceiptDatetextBox";
            this.ReceiptDatetextBox.Size = new System.Drawing.Size(111, 20);
            this.ReceiptDatetextBox.TabIndex = 30;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(745, 371);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 25);
            this.label10.TabIndex = 24;
            this.label10.Text = "Total";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Receipt No";
            // 
            // ReceiptNotextBox
            // 
            this.ReceiptNotextBox.BackColor = System.Drawing.Color.White;
            this.ReceiptNotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ReceiptNotextBox.Enabled = false;
            this.ReceiptNotextBox.Location = new System.Drawing.Point(114, 31);
            this.ReceiptNotextBox.Name = "ReceiptNotextBox";
            this.ReceiptNotextBox.Size = new System.Drawing.Size(176, 20);
            this.ReceiptNotextBox.TabIndex = 29;
            this.ReceiptNotextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReceiptNotextBox_KeyDown);
            this.ReceiptNotextBox.Leave += new System.EventHandler(this.ReceiptNotextBox_Leave);
            // 
            // SearchRcvNObutton
            // 
            this.SearchRcvNObutton.BackColor = System.Drawing.Color.Transparent;
            this.SearchRcvNObutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.SearchRcvNObutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SearchRcvNObutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.SearchRcvNObutton.FlatAppearance.BorderSize = 0;
            this.SearchRcvNObutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchRcvNObutton.ForeColor = System.Drawing.Color.Transparent;
            this.SearchRcvNObutton.Location = new System.Drawing.Point(296, 29);
            this.SearchRcvNObutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.SearchRcvNObutton.Name = "SearchRcvNObutton";
            this.SearchRcvNObutton.Size = new System.Drawing.Size(24, 24);
            this.SearchRcvNObutton.TabIndex = 31;
            this.SearchRcvNObutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.SearchRcvNObutton.UseVisualStyleBackColor = false;
            this.SearchRcvNObutton.Visible = false;
            this.SearchRcvNObutton.Click += new System.EventHandler(this.SearchRcvNObutton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(399, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 13);
            this.label3.TabIndex = 32;
            this.label3.Text = "Receipt Revision Date";
            // 
            // RevisionDatetextBox
            // 
            this.RevisionDatetextBox.BackColor = System.Drawing.Color.White;
            this.RevisionDatetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RevisionDatetextBox.Enabled = false;
            this.RevisionDatetextBox.Location = new System.Drawing.Point(519, 3);
            this.RevisionDatetextBox.Name = "RevisionDatetextBox";
            this.RevisionDatetextBox.Size = new System.Drawing.Size(111, 20);
            this.RevisionDatetextBox.TabIndex = 33;
            // 
            // RatetextBox
            // 
            this.RatetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RatetextBox.Decimal = false;
            this.RatetextBox.Location = new System.Drawing.Point(114, 141);
            this.RatetextBox.Money = true;
            this.RatetextBox.Name = "RatetextBox";
            this.RatetextBox.Numeric = false;
            this.RatetextBox.Prefix = "";
            this.RatetextBox.Size = new System.Drawing.Size(176, 20);
            this.RatetextBox.TabIndex = 7;
            // 
            // TotaltextBox
            // 
            this.TotaltextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TotaltextBox.Decimal = false;
            this.TotaltextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotaltextBox.Location = new System.Drawing.Point(827, 374);
            this.TotaltextBox.Money = true;
            this.TotaltextBox.Name = "TotaltextBox";
            this.TotaltextBox.Numeric = false;
            this.TotaltextBox.Prefix = "";
            this.TotaltextBox.Size = new System.Drawing.Size(158, 31);
            this.TotaltextBox.TabIndex = 25;
            // 
            // PurchaseReceiptRevision
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1102, 535);
            this.Controls.Add(this.tableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PurchaseReceiptRevision";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Receipt Revision";
            this.Activated += new System.EventHandler(this.PurchaseReceiptRevision_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.PurchaseReceiptRevision_FormClosed);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.Spesifikasipanel.ResumeLayout(false);
            this.datagridviewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MasterItemdataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox SupplierNametextBox;
        private System.Windows.Forms.TextBox ContactPersontextBox;
        private System.Windows.Forms.TextBox RevisiNotextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox KurstextBox;
        private System.Windows.Forms.Button AddItembutton;
        private System.Windows.Forms.Panel datagridviewpanel;
        private System.Windows.Forms.DataGridView MasterItemdataGridView;
        private System.Windows.Forms.Button SearchRevisiNObutton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel Spesifikasipanel;
        private System.Windows.Forms.RichTextBox RemarkrichTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox ReceiptDatetextBox;
        private System.Windows.Forms.TextBox ReceiptNotextBox;
        private System.Windows.Forms.Button SearchRcvNObutton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox RevisionDatetextBox;
        private CustomControls.TextBoxSL RatetextBox;
        private CustomControls.TextBoxSL TotaltextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMIDQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMNAMEQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn QTYORDER;
        private System.Windows.Forms.DataGridViewTextBoxColumn QTYREVISI;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn UNITPRICE;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT;
        private System.Windows.Forms.DataGridViewImageColumn del;
    }
}