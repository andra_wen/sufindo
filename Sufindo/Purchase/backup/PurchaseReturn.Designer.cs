﻿namespace Sufindo.Purchase
{
    partial class PurchaseReturn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PurchaseReturn));
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.PurchaseDatetextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.PurchaseNotextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.PartNametextBox = new System.Windows.Forms.TextBox();
            this.DatetextBox = new System.Windows.Forms.TextBox();
            this.SupplierIDtextBox = new System.Windows.Forms.TextBox();
            this.PurchaseReturnNotextBox = new System.Windows.Forms.TextBox();
            this.PartNotextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMIDQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SUPPLIERIDS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PURCHASEIDS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PURCHASEDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICEID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICEDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUANTITYRCV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUANTITYRTR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEL = new System.Windows.Forms.DataGridViewImageColumn();
            this.AddItembutton = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.QtyReturntextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.QtyReceivetextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.InvoiceNotextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.InvoiceDatetextBox = new System.Windows.Forms.TextBox();
            this.Spesifikasipanel = new System.Windows.Forms.Panel();
            this.RemarkrichTextBox = new System.Windows.Forms.RichTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.SearchRPbutton = new System.Windows.Forms.Button();
            this.SearchItembutton = new System.Windows.Forms.Button();
            this.SearchSupplierIDbutton = new System.Windows.Forms.Button();
            this.SearchPurchaseNobutton = new System.Windows.Forms.Button();
            this.tableLayoutPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.Spesifikasipanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(292, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Date";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Purchase Return No";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 5;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.Controls.Add(this.PurchaseDatetextBox, 4, 3);
            this.tableLayoutPanel.Controls.Add(this.label11, 3, 3);
            this.tableLayoutPanel.Controls.Add(this.PurchaseNotextBox, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.label10, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.PartNametextBox, 4, 1);
            this.tableLayoutPanel.Controls.Add(this.DatetextBox, 4, 0);
            this.tableLayoutPanel.Controls.Add(this.SupplierIDtextBox, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.PurchaseReturnNotextBox, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.PartNotextBox, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.label2, 3, 0);
            this.tableLayoutPanel.Controls.Add(this.label6, 3, 1);
            this.tableLayoutPanel.Controls.Add(this.panel1, 0, 8);
            this.tableLayoutPanel.Controls.Add(this.AddItembutton, 0, 7);
            this.tableLayoutPanel.Controls.Add(this.label9, 0, 6);
            this.tableLayoutPanel.Controls.Add(this.QtyReturntextBox, 1, 6);
            this.tableLayoutPanel.Controls.Add(this.label8, 0, 5);
            this.tableLayoutPanel.Controls.Add(this.QtyReceivetextBox, 1, 5);
            this.tableLayoutPanel.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel.Controls.Add(this.InvoiceNotextBox, 1, 4);
            this.tableLayoutPanel.Controls.Add(this.label7, 3, 4);
            this.tableLayoutPanel.Controls.Add(this.InvoiceDatetextBox, 4, 4);
            this.tableLayoutPanel.Controls.Add(this.Spesifikasipanel, 1, 9);
            this.tableLayoutPanel.Controls.Add(this.label12, 0, 9);
            this.tableLayoutPanel.Controls.Add(this.SearchRPbutton, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.SearchItembutton, 2, 1);
            this.tableLayoutPanel.Controls.Add(this.SearchSupplierIDbutton, 2, 2);
            this.tableLayoutPanel.Controls.Add(this.SearchPurchaseNobutton, 2, 3);
            this.tableLayoutPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.tableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 10;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(813, 596);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // PurchaseDatetextBox
            // 
            this.PurchaseDatetextBox.BackColor = System.Drawing.Color.White;
            this.PurchaseDatetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PurchaseDatetextBox.Enabled = false;
            this.PurchaseDatetextBox.Location = new System.Drawing.Point(376, 87);
            this.PurchaseDatetextBox.Name = "PurchaseDatetextBox";
            this.PurchaseDatetextBox.Size = new System.Drawing.Size(176, 20);
            this.PurchaseDatetextBox.TabIndex = 11;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(292, 84);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "Purchase Date";
            // 
            // PurchaseNotextBox
            // 
            this.PurchaseNotextBox.BackColor = System.Drawing.Color.White;
            this.PurchaseNotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PurchaseNotextBox.Enabled = false;
            this.PurchaseNotextBox.Location = new System.Drawing.Point(113, 87);
            this.PurchaseNotextBox.Name = "PurchaseNotextBox";
            this.PurchaseNotextBox.Size = new System.Drawing.Size(143, 20);
            this.PurchaseNotextBox.TabIndex = 9;
            this.PurchaseNotextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PurchaseNotextBox_KeyDown);
            this.PurchaseNotextBox.Leave += new System.EventHandler(this.PurchaseNotextBox_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 84);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "Purchase No";
            // 
            // PartNametextBox
            // 
            this.PartNametextBox.BackColor = System.Drawing.Color.White;
            this.PartNametextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PartNametextBox.Enabled = false;
            this.PartNametextBox.Location = new System.Drawing.Point(376, 31);
            this.PartNametextBox.Name = "PartNametextBox";
            this.PartNametextBox.Size = new System.Drawing.Size(176, 20);
            this.PartNametextBox.TabIndex = 6;
            // 
            // DatetextBox
            // 
            this.DatetextBox.BackColor = System.Drawing.Color.White;
            this.DatetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatetextBox.Enabled = false;
            this.DatetextBox.Location = new System.Drawing.Point(376, 3);
            this.DatetextBox.Name = "DatetextBox";
            this.DatetextBox.Size = new System.Drawing.Size(176, 20);
            this.DatetextBox.TabIndex = 3;
            // 
            // SupplierIDtextBox
            // 
            this.SupplierIDtextBox.BackColor = System.Drawing.Color.White;
            this.SupplierIDtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SupplierIDtextBox.Enabled = false;
            this.SupplierIDtextBox.Location = new System.Drawing.Point(113, 59);
            this.SupplierIDtextBox.Name = "SupplierIDtextBox";
            this.SupplierIDtextBox.Size = new System.Drawing.Size(143, 20);
            this.SupplierIDtextBox.TabIndex = 7;
            this.SupplierIDtextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SupplierIDtextBox_KeyDown);
            this.SupplierIDtextBox.Leave += new System.EventHandler(this.SupplierIDtextBox_Leave);
            // 
            // PurchaseReturnNotextBox
            // 
            this.PurchaseReturnNotextBox.BackColor = System.Drawing.Color.White;
            this.PurchaseReturnNotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PurchaseReturnNotextBox.Enabled = false;
            this.PurchaseReturnNotextBox.Location = new System.Drawing.Point(113, 3);
            this.PurchaseReturnNotextBox.Name = "PurchaseReturnNotextBox";
            this.PurchaseReturnNotextBox.Size = new System.Drawing.Size(143, 20);
            this.PurchaseReturnNotextBox.TabIndex = 1;
            this.PurchaseReturnNotextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PurchaseReturnNotextBox_KeyDown);
            this.PurchaseReturnNotextBox.Leave += new System.EventHandler(this.PurchaseReturnNotextBox_Leave);
            // 
            // PartNotextBox
            // 
            this.PartNotextBox.BackColor = System.Drawing.Color.White;
            this.PartNotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PartNotextBox.Location = new System.Drawing.Point(113, 31);
            this.PartNotextBox.Name = "PartNotextBox";
            this.PartNotextBox.Size = new System.Drawing.Size(143, 20);
            this.PartNotextBox.TabIndex = 4;
            this.PartNotextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PartNotextBox_KeyDown);
            this.PartNotextBox.Leave += new System.EventHandler(this.PartNotextBox_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Part No";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Supplier ID";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(292, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Part Name";
            // 
            // panel1
            // 
            this.tableLayoutPanel.SetColumnSpan(this.panel1, 5);
            this.panel1.Controls.Add(this.dataGridView);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(3, 225);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(807, 238);
            this.panel1.TabIndex = 18;
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NO,
            this.ITEMID,
            this.ITEMNAME,
            this.ITEMIDQTY,
            this.SUPPLIERIDS,
            this.PURCHASEIDS,
            this.PURCHASEDATE,
            this.INVOICEID,
            this.INVOICEDATE,
            this.QUANTITYRCV,
            this.QUANTITYRTR,
            this.DEL});
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(0, 0);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.Size = new System.Drawing.Size(807, 238);
            this.dataGridView.TabIndex = 18;
            this.dataGridView.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellValidated);
            this.dataGridView.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dataGridView_UserDeletedRow);
            this.dataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellClick);
            this.dataGridView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dataGridView_EditingControlShowing);
            // 
            // NO
            // 
            this.NO.DataPropertyName = "NO";
            this.NO.FillWeight = 30F;
            this.NO.HeaderText = "NO";
            this.NO.Name = "NO";
            this.NO.ReadOnly = true;
            this.NO.Width = 30;
            // 
            // ITEMID
            // 
            this.ITEMID.DataPropertyName = "ITEMID";
            this.ITEMID.HeaderText = "PART NO";
            this.ITEMID.Name = "ITEMID";
            this.ITEMID.ReadOnly = true;
            // 
            // ITEMNAME
            // 
            this.ITEMNAME.DataPropertyName = "ITEMNAME";
            this.ITEMNAME.HeaderText = "PART NAME";
            this.ITEMNAME.Name = "ITEMNAME";
            this.ITEMNAME.ReadOnly = true;
            // 
            // ITEMIDQTY
            // 
            this.ITEMIDQTY.DataPropertyName = "ITEMIDQTY";
            this.ITEMIDQTY.HeaderText = "PART NO QTY";
            this.ITEMIDQTY.Name = "ITEMIDQTY";
            this.ITEMIDQTY.ReadOnly = true;
            this.ITEMIDQTY.Width = 120;
            // 
            // SUPPLIERIDS
            // 
            this.SUPPLIERIDS.DataPropertyName = "SUPPLIERID";
            this.SUPPLIERIDS.HeaderText = "SUPPLIER ID";
            this.SUPPLIERIDS.Name = "SUPPLIERIDS";
            this.SUPPLIERIDS.ReadOnly = true;
            // 
            // PURCHASEIDS
            // 
            this.PURCHASEIDS.DataPropertyName = "PURCHASEID";
            this.PURCHASEIDS.HeaderText = "PURCHASE NO";
            this.PURCHASEIDS.Name = "PURCHASEIDS";
            this.PURCHASEIDS.ReadOnly = true;
            this.PURCHASEIDS.Width = 110;
            // 
            // PURCHASEDATE
            // 
            this.PURCHASEDATE.DataPropertyName = "PURCHASEDATE";
            this.PURCHASEDATE.FillWeight = 120F;
            this.PURCHASEDATE.HeaderText = "PURCHASE DATE";
            this.PURCHASEDATE.Name = "PURCHASEDATE";
            this.PURCHASEDATE.ReadOnly = true;
            this.PURCHASEDATE.Width = 125;
            // 
            // INVOICEID
            // 
            this.INVOICEID.DataPropertyName = "INVOICEID";
            this.INVOICEID.HeaderText = "Invoice No";
            this.INVOICEID.Name = "INVOICEID";
            this.INVOICEID.ReadOnly = true;
            this.INVOICEID.Visible = false;
            // 
            // INVOICEDATE
            // 
            this.INVOICEDATE.DataPropertyName = "INVOICEDATE";
            this.INVOICEDATE.HeaderText = "Invoice Date";
            this.INVOICEDATE.Name = "INVOICEDATE";
            this.INVOICEDATE.ReadOnly = true;
            this.INVOICEDATE.Visible = false;
            // 
            // QUANTITYRCV
            // 
            this.QUANTITYRCV.DataPropertyName = "QUANTITYRCV";
            this.QUANTITYRCV.HeaderText = "QTY RECEIVE";
            this.QUANTITYRCV.Name = "QUANTITYRCV";
            this.QUANTITYRCV.ReadOnly = true;
            this.QUANTITYRCV.Width = 120;
            // 
            // QUANTITYRTR
            // 
            this.QUANTITYRTR.DataPropertyName = "QUANTITYRTR";
            this.QUANTITYRTR.HeaderText = "QTY RETURN";
            this.QUANTITYRTR.Name = "QUANTITYRTR";
            this.QUANTITYRTR.ReadOnly = true;
            this.QUANTITYRTR.Width = 120;
            // 
            // DEL
            // 
            this.DEL.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.NullValue = ((object)(resources.GetObject("dataGridViewCellStyle1.NullValue")));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.DEL.DefaultCellStyle = dataGridViewCellStyle1;
            this.DEL.FillWeight = 24F;
            this.DEL.HeaderText = "";
            this.DEL.Image = global::Sufindo.Properties.Resources.minus;
            this.DEL.MinimumWidth = 24;
            this.DEL.Name = "DEL";
            this.DEL.Width = 24;
            // 
            // AddItembutton
            // 
            this.AddItembutton.BackColor = System.Drawing.Color.Transparent;
            this.AddItembutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AddItembutton.Enabled = false;
            this.AddItembutton.FlatAppearance.BorderSize = 0;
            this.AddItembutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddItembutton.ForeColor = System.Drawing.Color.Transparent;
            this.AddItembutton.Image = global::Sufindo.Properties.Resources.plus;
            this.AddItembutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.AddItembutton.Location = new System.Drawing.Point(0, 190);
            this.AddItembutton.Margin = new System.Windows.Forms.Padding(0);
            this.AddItembutton.Name = "AddItembutton";
            this.AddItembutton.Size = new System.Drawing.Size(88, 32);
            this.AddItembutton.TabIndex = 16;
            this.AddItembutton.Text = "Add Item";
            this.AddItembutton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.AddItembutton.UseVisualStyleBackColor = false;
            this.AddItembutton.Click += new System.EventHandler(this.AddItembutton_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 164);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "QTY Return";
            // 
            // QtyReturntextBox
            // 
            this.QtyReturntextBox.BackColor = System.Drawing.Color.White;
            this.QtyReturntextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.QtyReturntextBox.Enabled = false;
            this.QtyReturntextBox.Location = new System.Drawing.Point(113, 167);
            this.QtyReturntextBox.Name = "QtyReturntextBox";
            this.QtyReturntextBox.Size = new System.Drawing.Size(143, 20);
            this.QtyReturntextBox.TabIndex = 15;
            this.QtyReturntextBox.Leave += new System.EventHandler(this.QtyReturntextBox_Leave);
            this.QtyReturntextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.QtyReturntextBox_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 138);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "QTY Receive";
            // 
            // QtyReceivetextBox
            // 
            this.QtyReceivetextBox.BackColor = System.Drawing.Color.White;
            this.QtyReceivetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.QtyReceivetextBox.Enabled = false;
            this.QtyReceivetextBox.Location = new System.Drawing.Point(113, 141);
            this.QtyReceivetextBox.Name = "QtyReceivetextBox";
            this.QtyReceivetextBox.Size = new System.Drawing.Size(143, 20);
            this.QtyReceivetextBox.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Invoice No";
            this.label5.Visible = false;
            // 
            // InvoiceNotextBox
            // 
            this.InvoiceNotextBox.BackColor = System.Drawing.Color.White;
            this.InvoiceNotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.InvoiceNotextBox.Enabled = false;
            this.InvoiceNotextBox.Location = new System.Drawing.Point(113, 115);
            this.InvoiceNotextBox.Name = "InvoiceNotextBox";
            this.InvoiceNotextBox.Size = new System.Drawing.Size(143, 20);
            this.InvoiceNotextBox.TabIndex = 12;
            this.InvoiceNotextBox.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(292, 112);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Invoice Date";
            this.label7.Visible = false;
            // 
            // InvoiceDatetextBox
            // 
            this.InvoiceDatetextBox.BackColor = System.Drawing.Color.White;
            this.InvoiceDatetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.InvoiceDatetextBox.Enabled = false;
            this.InvoiceDatetextBox.Location = new System.Drawing.Point(376, 115);
            this.InvoiceDatetextBox.Name = "InvoiceDatetextBox";
            this.InvoiceDatetextBox.Size = new System.Drawing.Size(176, 20);
            this.InvoiceDatetextBox.TabIndex = 13;
            this.InvoiceDatetextBox.Visible = false;
            // 
            // Spesifikasipanel
            // 
            this.Spesifikasipanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.Spesifikasipanel, 4);
            this.Spesifikasipanel.Controls.Add(this.RemarkrichTextBox);
            this.Spesifikasipanel.Location = new System.Drawing.Point(113, 469);
            this.Spesifikasipanel.Name = "Spesifikasipanel";
            this.Spesifikasipanel.Size = new System.Drawing.Size(426, 124);
            this.Spesifikasipanel.TabIndex = 23;
            // 
            // RemarkrichTextBox
            // 
            this.RemarkrichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RemarkrichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RemarkrichTextBox.Location = new System.Drawing.Point(0, 0);
            this.RemarkrichTextBox.Name = "RemarkrichTextBox";
            this.RemarkrichTextBox.Size = new System.Drawing.Size(424, 122);
            this.RemarkrichTextBox.TabIndex = 19;
            this.RemarkrichTextBox.TabStop = false;
            this.RemarkrichTextBox.Text = "";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 466);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "Remark";
            // 
            // SearchRPbutton
            // 
            this.SearchRPbutton.BackColor = System.Drawing.Color.Transparent;
            this.SearchRPbutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.SearchRPbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SearchRPbutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.SearchRPbutton.FlatAppearance.BorderSize = 0;
            this.SearchRPbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchRPbutton.Location = new System.Drawing.Point(262, 1);
            this.SearchRPbutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.SearchRPbutton.Name = "SearchRPbutton";
            this.SearchRPbutton.Size = new System.Drawing.Size(24, 24);
            this.SearchRPbutton.TabIndex = 2;
            this.SearchRPbutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.SearchRPbutton.UseVisualStyleBackColor = false;
            this.SearchRPbutton.Visible = false;
            this.SearchRPbutton.Click += new System.EventHandler(this.SearchRPbutton_Click);
            // 
            // SearchItembutton
            // 
            this.SearchItembutton.BackColor = System.Drawing.Color.Transparent;
            this.SearchItembutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.SearchItembutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SearchItembutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.SearchItembutton.FlatAppearance.BorderSize = 0;
            this.SearchItembutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchItembutton.Location = new System.Drawing.Point(262, 29);
            this.SearchItembutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.SearchItembutton.Name = "SearchItembutton";
            this.SearchItembutton.Size = new System.Drawing.Size(24, 24);
            this.SearchItembutton.TabIndex = 5;
            this.SearchItembutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.SearchItembutton.UseVisualStyleBackColor = false;
            this.SearchItembutton.Visible = false;
            this.SearchItembutton.Click += new System.EventHandler(this.SearchItembutton_Click);
            // 
            // SearchSupplierIDbutton
            // 
            this.SearchSupplierIDbutton.BackColor = System.Drawing.Color.Transparent;
            this.SearchSupplierIDbutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.SearchSupplierIDbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SearchSupplierIDbutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.SearchSupplierIDbutton.FlatAppearance.BorderSize = 0;
            this.SearchSupplierIDbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchSupplierIDbutton.Location = new System.Drawing.Point(262, 57);
            this.SearchSupplierIDbutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.SearchSupplierIDbutton.Name = "SearchSupplierIDbutton";
            this.SearchSupplierIDbutton.Size = new System.Drawing.Size(24, 24);
            this.SearchSupplierIDbutton.TabIndex = 8;
            this.SearchSupplierIDbutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.SearchSupplierIDbutton.UseVisualStyleBackColor = false;
            this.SearchSupplierIDbutton.Visible = false;
            this.SearchSupplierIDbutton.Click += new System.EventHandler(this.SearchSupplierIDbutton_Click);
            // 
            // SearchPurchaseNobutton
            // 
            this.SearchPurchaseNobutton.BackColor = System.Drawing.Color.Transparent;
            this.SearchPurchaseNobutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.SearchPurchaseNobutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SearchPurchaseNobutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.SearchPurchaseNobutton.FlatAppearance.BorderSize = 0;
            this.SearchPurchaseNobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchPurchaseNobutton.Location = new System.Drawing.Point(262, 85);
            this.SearchPurchaseNobutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.SearchPurchaseNobutton.Name = "SearchPurchaseNobutton";
            this.SearchPurchaseNobutton.Size = new System.Drawing.Size(24, 24);
            this.SearchPurchaseNobutton.TabIndex = 10;
            this.SearchPurchaseNobutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.SearchPurchaseNobutton.UseVisualStyleBackColor = false;
            this.SearchPurchaseNobutton.Visible = false;
            this.SearchPurchaseNobutton.Click += new System.EventHandler(this.SearchPurchaseNobutton_Click);
            // 
            // PurchaseReturn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(819, 602);
            this.Controls.Add(this.tableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PurchaseReturn";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Purchase Return";
            this.Activated += new System.EventHandler(this.PurchaseReturn_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.PurchaseReturn_FormClosed);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.Spesifikasipanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button AddItembutton;
        private System.Windows.Forms.TextBox PartNametextBox;
        private System.Windows.Forms.TextBox DatetextBox;
        private System.Windows.Forms.TextBox InvoiceNotextBox;
        private System.Windows.Forms.TextBox SupplierIDtextBox;
        private System.Windows.Forms.TextBox PurchaseReturnNotextBox;
        private System.Windows.Forms.TextBox PartNotextBox;
        private System.Windows.Forms.TextBox QtyReceivetextBox;
        private System.Windows.Forms.TextBox QtyReturntextBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.TextBox PurchaseDatetextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox PurchaseNotextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox InvoiceDatetextBox;
        private System.Windows.Forms.Panel Spesifikasipanel;
        private System.Windows.Forms.RichTextBox RemarkrichTextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button SearchRPbutton;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMIDQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn SUPPLIERIDS;
        private System.Windows.Forms.DataGridViewTextBoxColumn PURCHASEIDS;
        private System.Windows.Forms.DataGridViewTextBoxColumn PURCHASEDATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn INVOICEID;
        private System.Windows.Forms.DataGridViewTextBoxColumn INVOICEDATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn QUANTITYRCV;
        private System.Windows.Forms.DataGridViewTextBoxColumn QUANTITYRTR;
        private System.Windows.Forms.DataGridViewImageColumn DEL;
        private System.Windows.Forms.Button SearchItembutton;
        private System.Windows.Forms.Button SearchSupplierIDbutton;
        private System.Windows.Forms.Button SearchPurchaseNobutton;

    }
}