﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Resources;

namespace Sufindo.Purchase
{
    public partial class PurchaseReceipt : Form
    {
        private delegate void fillAutoCompleteTextBox();

        Supplierdatagridview supplierdatagridview;
        PurchaseOrderdatagridview purchaseorderdatagridview;
        Purchasedatagridview purchasedatagridview;
        MenuStrip menustripAction;
        Connection connection;
        List<String> KursID;
        List<String> PurchaseOrderNo;
        List<String> SupplierName;
        List<String> contactPerson;
        List<String> ReceiptNo;
        List<Control> controlTextBox;
        
        DataTable datatable;

        Kursdatagridview kursdatagridview;
        Itemdatagridview itemdatagridview;

        String activity = "";
        Boolean isFind = false;
        Boolean addNewItem = false;
        String SupplierID = "";

        int ASSIGNITEMROW = -1;

        public PurchaseReceipt()
        {
            InitializeComponent();
        }

        public PurchaseReceipt(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            this.menustripAction = menustripAction;
            kursdatagridview = null;
            itemdatagridview = null;
            purchaseorderdatagridview = null;
            supplierdatagridview = null;
            if (connection == null)
            {
                connection = new Connection();
            }
            controlTextBox = new List<Control>();
            foreach (Control control in this.tableLayoutPanel.Controls)
            {
                if (control is TextBox)
                {
                    controlTextBox.Add(control);
                }
                else if (control is Panel)
                {
                    foreach (Control controlPanel in control.Controls)
                    {
                        if (controlPanel is RichTextBox)
                        {
                            controlTextBox.Add(controlPanel);
                        }
                    }
                }
                else if (control is ComboBox)
                {
                    controlTextBox.Add(control);
                }
            }
            datatable = new DataTable();
            newTransaction();
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                //MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }

        public void save(){
            if (activity.Equals("UPDATE") || activity.Equals("INSERT"))
            {
                if (isValidation())
                {
                    List<SqlParameter> sqlParam = new List<SqlParameter>();

                    if (activity.Equals("UPDATE")) sqlParam.Add(new SqlParameter("@PURCHASEID", ReceiptNotextBox.Text));
                    sqlParam.Add(new SqlParameter("@PURCHASEORDERID", PONotextBox.Text));
                    sqlParam.Add(new SqlParameter("@SUPPLIERID", SupplierID));
                    sqlParam.Add(new SqlParameter("@CONTACTPERSONSUPPLIER", ContactPersontextBox.Text));
                    sqlParam.Add(new SqlParameter("@KURSID", KurstextBox.Text.ToUpper()));
                    sqlParam.Add(new SqlParameter("@RATE", RatetextBox.DecimalValue.ToString()));
                    sqlParam.Add(new SqlParameter("@STATUS", Status.ACTIVE));
                    sqlParam.Add(new SqlParameter("@REMARK", RemarkrichTextBox.Text));
                    sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));

                    String proc = activity.Equals("UPDATE") ? "UPDATE_DATA_H_PURCHASE" : "INSERT_DATA_H_PURCHASE";

                    DataTable dt = connection.callProcedureDatatable(proc, sqlParam);

                    if (dt.Rows.Count == 1)
                    {
                        MessageBox.Show("Receipt NO " + dt.Rows[0]["PURCHASEID"].ToString());
                        List<string> values = new List<string>();
                        foreach (DataGridViewRow row in this.MasterItemdataGridView.Rows)
                        {
                            String unitpricestr = row.Cells["UNITPRICE"].Value.ToString().Contains(",") ? row.Cells["UNITPRICE"].Value.ToString().Replace(",", "") : row.Cells["UNITPRICE"].Value.ToString();
                            String value = String.Format("SELECT '{0}', '{1}', '{2}', {3}, {4}", dt.Rows[0]["PURCHASEID"].ToString(), row.Cells["ITEMID"].Value,
                                                                                               row.Cells["ITEMIDQTY"].Value.ToString().Equals("") ? row.Cells["ITEMID"].Value : row.Cells["ITEMIDQTY"].Value,
                                                                                               row.Cells["QUANTITY"].Value, unitpricestr);
                            values.Add(value);
                        }
                        String[] columns = { "PURCHASEID", "ITEMID", "ITEMIDQTY", "QUANTITY", "PRICE" };
                        String query = String.Format("DELETE FROM D_PURCHASE WHERE PURCHASEID = '{0}'", dt.Rows[0]["PURCHASEID"].ToString());
                        if (connection.openReaderQuery(query))
                        {
                            if (connection.openReaderQuery(Utilities.queryMultipleInsert("D_PURCHASE", columns, values)))
                            {
                                sqlParam.Clear();
                                sqlParam.Add(new SqlParameter("PURCHASEORDERID", PONotextBox.Text));
                                sqlParam.Add(new SqlParameter("STATUS", Status.CLOSE));
                                if (!PODatetextBox.Text.Equals("-"))
                                {
                                    if (connection.callProcedure("UPDATE_STATUS_H_PURCHASE_ORDER", sqlParam))
                                    {
                                        reloadAllData();
                                        newTransaction();
                                    }
                                }
                                else
                                {
                                    reloadAllData();
                                    newTransaction();
                                }
                            }
                            else {
                                MessageBox.Show("Test " + Utilities.queryMultipleInsert("D_PURCHASE", columns, values));
                            }
                        }
                    }
                }
            }
        }

        private Boolean isValidation()
        {
            if (Utilities.isEmptyString(PONotextBox.Text)) {
                MessageBox.Show("Please Select Purchase No ID");
            }
            else if (Utilities.isEmptyString(KurstextBox.Text))
            {
                MessageBox.Show("Please Input Currency");
            }
            else if (!isAvailableKurs())
            {
                MessageBox.Show("Please Input Correctly Currency");
            }
            else if (Utilities.isEmptyString(RatetextBox.Text))
            {
                MessageBox.Show("Please Input Rate");
            }
            else if (MasterItemdataGridView.Rows.Count == 0)
            {
                MessageBox.Show("Minimal One Part Item  For Order");
            }
            else {
                return true;
            }
            return false;
        }

        private Boolean isAvailableKurs()
        {
            KurstextBox.Text = Utilities.removeSpace(KurstextBox.Text.ToUpper());
            Boolean result = Utilities.isAvailableID(KursID, KurstextBox.Text);
            return result;
        }

        public void delete(){
            //if (isFind && isAvailablePurchaseOrderNoForUpdate())
            //{
            //    List<SqlParameter> sqlParam = new List<SqlParameter>();
            //    String USERLOGIN = "";
            //    sqlParam.Add(new SqlParameter("@PURCHASEORDERID", PONotextBox.Text));
            //    sqlParam.Add(new SqlParameter("@STATUS", Status.DELETE));
            //    sqlParam.Add(new SqlParameter("@CREATEDBY", USERLOGIN));

            //    if (connection.callProcedure("DELETE_DATA_H_PURCHASE_ORDER", sqlParam))
            //    {
            //        newTransaction();
            //        reloadAllData();
            //    }
            //}
        }

        public void newTransaction()
        {
            Utilities.clearAllField(ref controlTextBox);
            datatable.Clear();
            
            ReceiptDatetextBox.Text = DateTime.Now.ToString("dd/MM/yyyy");
            ReceiptNotextBox.Text = DateTime.Now.ToString("yyyy") + "XXXXXX";
            AddItembutton.Enabled = false;
            
            activity = "INSERT";
            isFind = false;
            
            TotaltextBox.Enabled = false;
            PONotextBox.Text = directioncheckbox.Checked ? "-" : "";
            PONotextBox.BackColor = SystemColors.Info;
            PONotextBox.Enabled = !directioncheckbox.Checked;

            PODatetextBox.Enabled = false;
            PODatetextBox.Text = directioncheckbox.Checked ? "-" : "";
            PODatetextBox.BackColor = SystemColors.Info;

            SupplierNametextBox.Text = "";
            SupplierNametextBox.Enabled = directioncheckbox.Checked;
            SupplierNametextBox.BackColor = SystemColors.Info;

            ContactPersontextBox.Text = "";
            ContactPersontextBox.Enabled = false;
            ContactPersontextBox.BackColor = SystemColors.Info;

            searchKursbutton.Visible = directioncheckbox.Checked;
            KurstextBox.Text = "";
            KurstextBox.Enabled = directioncheckbox.Checked;
            KurstextBox.BackColor = SystemColors.Window;
            KurstextBox.Text = directioncheckbox.Checked ? "IDR" : "";
            RatetextBox.Text = "";
            RatetextBox.Enabled = directioncheckbox.Checked;
            RatetextBox.Text = directioncheckbox.Checked ? "1" : "";
            RatetextBox.BackColor = SystemColors.Window;

            MasterItemdataGridView.AllowUserToDeleteRows = true;
            AllowUserToEditRows(true);
            foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
            {
                if (column.Name.Equals("del"))
                {
                    column.Visible = true;
                    break;
                }
            }
            directioncheckbox.Visible = true;
            ReceiptNotextBox.Enabled = false;
            SearchPONObutton.Visible = true;
            SearchRENObutton.Visible = false;
            RemarkrichTextBox.ReadOnly = false;
            RemarkrichTextBox.BackColor = Color.White;
        }

        public void find(){
            if (!SearchRENObutton.Visible || isFind)
            {
                SearchPONObutton.Visible = false;
                AddItembutton.Enabled = false;
                SearchRENObutton.Visible = true;
                SearchsupplierButton.Visible = false;
                isFind = true;

                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("ReceiptNotextBox"))
                    {
                        if (item is RichTextBox)
                        {
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = true;
                            richtextBox.BackColor = SystemColors.Info;
                            richtextBox.Text = "";
                        }
                        else
                        {
                            item.Enabled = false;
                            item.Text = "";
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        currentTextbox.AutoCompleteCustomSource.AddRange(ReceiptNo.ToArray());
                        
                        currentTextbox.Enabled = true;
                    }

                    item.BackColor = SystemColors.Info;
                }
                searchKursbutton.Visible = false;
               
                datatable.Clear();
                ReceiptNotextBox.Text = "";
                ReceiptDatetextBox.Text = "";
                MasterItemdataGridView.AllowUserToDeleteRows = false;
                AllowUserToEditRows(false);
                addNewItem = false;
                foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
                {
                    if (column.Name.Equals("del"))
                    {
                        column.Visible = false;
                        break;
                    }
                }
                
                activity = "";
                directioncheckbox.Visible = false;
            }
        }

        public void edit(){
            //if ((!SearchPONObutton.Visible || isFind) && StatustextBox.Text.Equals(Status.OPEN))
            //{
            //    AddItembutton.Enabled = true;
            //    KurstextBox.Enabled = true;
            //    RatetextBox.Enabled = true;
            //    searchKursbutton.Visible = true;
            //    KurstextBox.BackColor = SystemColors.Window;
            //    RatetextBox.BackColor = SystemColors.Window;
            //    MasterItemdataGridView.AllowUserToDeleteRows = true;
            //    AllowUserToEditRows(true);
            //    RemarkrichTextBox.ReadOnly = false;
            //    RemarkrichTextBox.BackColor = SystemColors.Window;
            //    addNewItem = false;
            //    foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
            //    {
            //        if (column.Name.Equals("del"))
            //        {
            //            column.Visible = true;
            //            break;
            //        }
            //    }
            //    activity = "UPDATE";
            //}
        }

        private void AllowUserToEditRows(Boolean allow)
        {
            foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
            {
                column.ReadOnly = (column.Name.Equals("QUANTITY") || column.Name.Equals("UNITPRICE")) ? !allow : true;
            }
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e){

        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e){
            
            if (KursID == null) KursID = new List<string>();
            if (PurchaseOrderNo == null) PurchaseOrderNo = new List<string>();
            if (ReceiptNo == null) ReceiptNo = new List<string>();
            if (SupplierName == null) SupplierName = new List<string>();

            SupplierName.Clear();
            KursID.Clear();
            PurchaseOrderNo.Clear();
            ReceiptNo.Clear();

            SqlDataReader sqldataReader = connection.sqlDataReaders(String.Format("SELECT KURSID FROM M_KURS WHERE [STATUS] = '{0}'", Status.ACTIVE));

            while (sqldataReader.Read()) KursID.Add(sqldataReader.GetString(0));

            sqldataReader = connection.sqlDataReaders(String.Format("SELECT PURCHASEORDERID FROM H_PURCHASE_ORDER WHERE [STATUS] = '{0}'", Status.OPEN));
            
            while (sqldataReader.Read()) PurchaseOrderNo.Add(sqldataReader.GetString(0));

            sqldataReader = connection.sqlDataReaders(String.Format("SELECT PURCHASEID FROM H_PURCHASE WHERE [STATUS] = '{0}'", Status.ACTIVE));

            while (sqldataReader.Read()) ReceiptNo.Add(sqldataReader.GetString(0));

            sqldataReader = connection.sqlDataReaders(String.Format("SELECT SUPPLIERNAME FROM M_SUPPLIER WHERE [STATUS] = '{0}'", Status.ACTIVE));

            while (sqldataReader.Read()) SupplierName.Add(sqldataReader.GetString(0));
            
            if (IsHandleCreated) BeginInvoke(new fillAutoCompleteTextBox(this.fillAutoComplete));
        }

        private void fillAutoComplete(){
            String tempKurs = KurstextBox.Text;
            KurstextBox.AutoCompleteCustomSource.Clear();
            KurstextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            KurstextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            KurstextBox.AutoCompleteCustomSource.AddRange(KursID.ToArray());
            KurstextBox.Text = tempKurs;

            String tempPONO = PONotextBox.Text;
            PONotextBox.AutoCompleteCustomSource.Clear();
            PONotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            PONotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            PONotextBox.AutoCompleteCustomSource.AddRange(PurchaseOrderNo.ToArray());
            PONotextBox.Text = directioncheckbox.Checked ? "-" : tempPONO;

            ReceiptNotextBox.AutoCompleteCustomSource.Clear();
            ReceiptNotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            ReceiptNotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            ReceiptNotextBox.AutoCompleteCustomSource.AddRange(ReceiptNo.ToArray());

            SupplierNametextBox.AutoCompleteCustomSource.Clear();
            SupplierNametextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            SupplierNametextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            SupplierNametextBox.AutoCompleteCustomSource.AddRange(SupplierName.ToArray());
        }

        private void KurstextBox_Leave(object sender, EventArgs e)
        {
            KurstextBox.Text = KurstextBox.Text.ToUpper();

            Boolean isExistingPartNo = false;
            foreach (String item in KursID)
            {
                if (item.ToUpper().Equals(KurstextBox.Text.ToUpper()))
                {
                    isExistingPartNo = true;
                    break;
                }
            }

            if (!isExistingPartNo)
            {
                KurstextBox.Text = "IDR";
                RatetextBox.Text = "1";
            }
        }

        private void KurstextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in KursID)
                {
                    if (item.ToUpper().Equals(KurstextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        KurstextBox.Text = KurstextBox.Text.ToUpper();
                        String query = String.Format("SELECT RATE " +
                                                     "FROM M_KURS " +
                                                     "WHERE KURSID = '{0}'", KurstextBox.Text);

                        DataTable dt = connection.openDataTableQuery(query);

                        RatetextBox.Text = dt.Rows[0]["RATE"].ToString();
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    KurstextBox.Text = "IDR";
                    RatetextBox.Text = "1";
                }
            }
        }

        private void searchKursbutton_Click(object sender, EventArgs e)
        {
            if (kursdatagridview == null)
            {
                kursdatagridview = new Kursdatagridview("MasterKurs");
                kursdatagridview.masterKursPassingData = new Kursdatagridview.MasterKursPassingData(MasterKursPassingData);
                //kursdatagridview.MdiParent = this.MdiParent;
                kursdatagridview.ShowDialog();
            }
            else if (kursdatagridview.IsDisposed)
            {
                kursdatagridview = new Kursdatagridview("MasterKurs");
                kursdatagridview.masterKursPassingData = new Kursdatagridview.MasterKursPassingData(MasterKursPassingData);
                //kursdatagridview.MdiParent = this.MdiParent;
                kursdatagridview.ShowDialog();
            }
        }

        private void MasterKursPassingData(DataTable sender){
            sender.Rows[0]["KURSID"].ToString();
            KurstextBox.Text = sender.Rows[0]["KURSID"].ToString();
            RatetextBox.Text = sender.Rows[0]["RATE"].ToString();
        }

        private void AddItembutton_Click(object sender, EventArgs e)
        {
            addNewItem = true;
            if (itemdatagridview == null)
            {
                itemdatagridview = new Itemdatagridview("PURCHASERECEIPT", SupplierID, datatable);
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
            else if (itemdatagridview.IsDisposed)
            {
                itemdatagridview = new Itemdatagridview("PURCHASERECEIPT", SupplierID, datatable);
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
            
        }
        private void MasterItemPurhcaseOrderPassingData(DataTable datatable)
        {
            if(this.datatable == null) this.datatable = new DataTable();

            /*remove column not use*/
            List<string> columnsRemove = new List<string>();

            foreach (DataColumn columns in datatable.Columns)
            {
                if (columns.ColumnName.Equals("ITEMNAME_ALIAS")) {
                    columns.ColumnName = "ITEMNAMEQTY";
                }
                else if (columns.ColumnName.Equals("ITEMID_ALIAS"))
                {
                    columns.ColumnName = "ITEMIDQTY";
                }
            }
            foreach (DataColumn column in datatable.Columns)
            {
                Boolean isExistingColumn = false;
                foreach (DataGridViewColumn columngrid in MasterItemdataGridView.Columns)
                {
                    if (column.ColumnName.Equals(columngrid.DataPropertyName))
                    {
                        isExistingColumn = true;
                        break;
                    }
                }
                if (!isExistingColumn)
                {
                    columnsRemove.Add(column.ColumnName);
                }
            }
            foreach (string columnName in columnsRemove)
            {
                datatable.Columns.Remove(columnName);
            }
            
            if (this.datatable.Rows.Count == 0) this.datatable = datatable;
            else{
                foreach (DataRow row in datatable.Rows)
                {
                    this.datatable.Rows.Add(row.ItemArray);
                }
            }

            foreach (DataRow row in this.datatable.Rows)
            {
                //Object obj = "1";
                //row["QUANTITY"] = (addNewItem) ? obj : row["QUANTITY"];
                Object obj = "0";
                row["UNITPRICE"] = (row["UNITPRICE"].ToString().Equals("")) ? obj : row["UNITPRICE"];
                Int64 quantity = Int64.Parse(row["QUANTITY"].ToString());
                Double unitprice = Double.Parse(row["UNITPRICE"].ToString());
                Double amount = (quantity * unitprice);
                row["AMOUNT"] = (Object)amount.ToString();
            }

            Utilities.generateNoDataTable(ref this.datatable);

            MasterItemdataGridView.DataSource = this.datatable;

            Double total = 0.0;
            foreach (DataGridViewRow row in MasterItemdataGridView.Rows)
            {
                total += Double.Parse(row.Cells["AMOUNT"].Value.ToString());
            }
            TotaltextBox.Text = total.ToString();

//            setDecimalN2();
        }
        private void MasterItemPurhcasePassingData(DataTable datatable)
        {
            if (this.datatable == null) this.datatable = new DataTable();

            /*remove column not use*/
            List<string> columnsRemove = new List<string>();

            foreach (DataColumn columns in datatable.Columns)
            {
                if (columns.ColumnName.Equals("ITEMNAME_ALIAS"))
                {
                    columns.ColumnName = "ITEMNAMEQTY";
                }
                else if (columns.ColumnName.Equals("ITEMID_ALIAS"))
                {
                    columns.ColumnName = "ITEMIDQTY";
                }
            }
            foreach (DataColumn column in datatable.Columns)
            {
                Boolean isExistingColumn = false;
                foreach (DataGridViewColumn columngrid in MasterItemdataGridView.Columns)
                {
                    if (column.ColumnName.Equals(columngrid.DataPropertyName))
                    {
                        isExistingColumn = true;
                        break;
                    }
                }
                if (!isExistingColumn)
                {
                    columnsRemove.Add(column.ColumnName);
                }
            }
            foreach (string columnName in columnsRemove)
            {
                datatable.Columns.Remove(columnName);
            }

            if (this.datatable.Rows.Count == 0) this.datatable = datatable;
            else
            {
                foreach (DataRow row in datatable.Rows)
                {
                    this.datatable.Rows.Add(row.ItemArray);
                }
            }

            foreach (DataRow row in this.datatable.Rows)
            {
                //Object obj = "1";
                //row["QUANTITY"] = (addNewItem) ? obj : row["QUANTITY"];
                Object obj = "0";
                row["UNITPRICE"] = (row["UNITPRICE"].ToString().Equals("")) ? obj : row["UNITPRICE"];
                Int64 quantity = Int64.Parse(row["QUANTITY"].ToString());
                Double unitprice = Double.Parse(row["UNITPRICE"].ToString());
                Double amount = (quantity * unitprice);
                row["AMOUNT"] = (Object)amount.ToString();
            }

            Utilities.generateNoDataTable(ref this.datatable);

            MasterItemdataGridView.DataSource = this.datatable;

            Double total = 0.0;
            foreach (DataGridViewRow row in MasterItemdataGridView.Rows)
            {
                total += Double.Parse(row.Cells["AMOUNT"].Value.ToString());
            }
            TotaltextBox.Text = total.ToString();

            //setDecimalN2();
        }
        private void MasterItemPassingData(DataTable datatable){
            if(this.datatable == null) this.datatable = new DataTable();

            /*remove column not use*/
            List<string> columnsRemove = new List<string>();

            foreach (DataColumn columns in datatable.Columns)
            {
                if (columns.ColumnName.Equals("ITEMNAME_ALIAS")) {
                    columns.ColumnName = "ITEMNAMEQTY";
                }
                else if (columns.ColumnName.Equals("ITEMID_ALIAS"))
                {
                    columns.ColumnName = "ITEMIDQTY";
                }
            }
            foreach (DataColumn column in datatable.Columns)
            {
                Boolean isExistingColumn = false;
                foreach (DataGridViewColumn columngrid in MasterItemdataGridView.Columns)
                {
                    if (column.ColumnName.Equals(columngrid.DataPropertyName))
                    {
                        isExistingColumn = true;
                        break;
                    }
                }
                if (!isExistingColumn)
                {
                    columnsRemove.Add(column.ColumnName);
                }
            }
            foreach (string columnName in columnsRemove)
            {
                datatable.Columns.Remove(columnName);
            }

            foreach (DataRow row in datatable.Rows)
            {
                Object obj = "1";
                row["QUANTITY"] = obj;
            }
            
            if (this.datatable.Rows.Count == 0) this.datatable = datatable;
            else{
                foreach (DataRow row in datatable.Rows)
                {
                    this.datatable.Rows.Add(row.ItemArray);
                }
            }

            foreach (DataRow row in this.datatable.Rows)
            {
                Object obj = "0";
                row["UNITPRICE"] = (row["UNITPRICE"].ToString().Equals("")) ? obj : row["UNITPRICE"];
                Int64 quantity = Int64.Parse(row["QUANTITY"].ToString());
                Double unitprice = Double.Parse(row["UNITPRICE"].ToString());
                Double amount = (quantity * unitprice);
                row["AMOUNT"] = (Object)amount.ToString();
            }

            Utilities.generateNoDataTable(ref this.datatable);

            MasterItemdataGridView.DataSource = this.datatable;

            Double total = 0.0;
            foreach (DataGridViewRow row in MasterItemdataGridView.Rows)
            {
                total += Double.Parse(row.Cells["AMOUNT"].Value.ToString());
            }
            TotaltextBox.Text = total.ToString();

//            setDecimalN2();
        }

        private void AssignItemQuantityPassingData(DataTable sender) {
            MasterItemdataGridView.Rows[ASSIGNITEMROW].Cells["ITEMIDQTY"].Value = sender.Rows[0]["ITEMID"].ToString();
            MasterItemdataGridView.Rows[ASSIGNITEMROW].Cells["ITEMNAMEQTY"].Value = sender.Rows[0]["ITEMNAME"].ToString();
        }

        private void MasterItemdataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1){
                if (MasterItemdataGridView.Columns[e.ColumnIndex].Name.Equals("del") )
                {
                    if (datatable.Rows.Count == 1) {
                        TotaltextBox.Text = "0";
                    }
                    this.datatable.Rows.RemoveAt(e.RowIndex);
                    Utilities.generateNoDataTable(ref datatable);
                    MasterItemdataGridView.DataSource = datatable;
                }
                else if (!activity.Equals("") && (MasterItemdataGridView.Columns[e.ColumnIndex].Name.Equals("ITEMIDQTY") || MasterItemdataGridView.Columns[e.ColumnIndex].Name.Equals("ITEMNAMEQTY")) && activity.Equals("INSERT"))
                {
                    ASSIGNITEMROW = e.RowIndex;
                    String ITEMID = MasterItemdataGridView.Rows[e.RowIndex].Cells["ITEMID"].Value.ToString();
                    if (itemdatagridview == null)
                    {
                        itemdatagridview = new Itemdatagridview("ASSIGNITEMPURCHASERECEIPT", ITEMID);
                        itemdatagridview.assignItemQuantityPassingData = new Itemdatagridview.AssignItemQuantityPassingData(AssignItemQuantityPassingData);
                        itemdatagridview.ShowDialog();
                    }
                    else if (itemdatagridview.IsDisposed)
                    {
                        itemdatagridview = new Itemdatagridview("ASSIGNITEMPURCHASERECEIPT", ITEMID);
                        itemdatagridview.assignItemQuantityPassingData = new Itemdatagridview.AssignItemQuantityPassingData(AssignItemQuantityPassingData);
                        itemdatagridview.ShowDialog();
                    }
                }
            }
        }

        private void MasterItemdataGridView_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            for (int i = 0; i < datatable.Rows.Count; i++)
            {
                if (datatable.Rows[i].RowState == DataRowState.Deleted)
                {
                    datatable.Rows.RemoveAt(i);
                }
            }
            Utilities.generateNoDataTable(ref datatable);
        }

        private void PurchaseOrder_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }

        private void MasterItemdataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
        }

        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            int currColumn = MasterItemdataGridView.CurrentCell.ColumnIndex;
            int currRow = MasterItemdataGridView.CurrentCell.RowIndex;
            if (MasterItemdataGridView.Columns[currColumn].Name.Equals("QUANTITY"))
            {
                e.Handled = Utilities.onlyNumberTextBox(sender, (KeyPressEventArgs)e);
            }
            else if (MasterItemdataGridView.Columns[currColumn].Name.Equals("UNITPRICE"))
            {
                //e.Handled = Utilities.onlyNumberTextBox(sender, (KeyPressEventArgs)e);
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
                {
                    e.Handled = true;
                }
            }
            
        }

        private void MasterItemdataGridView_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (MasterItemdataGridView.Rows.Count > 0)
            {
                Int64 quantity = (MasterItemdataGridView.Rows[e.RowIndex].Cells["QUANTITY"].Value == null || MasterItemdataGridView.Rows[e.RowIndex].Cells["QUANTITY"].Value.ToString().Equals("")) ? 0 : Int64.Parse(MasterItemdataGridView.Rows[e.RowIndex].Cells["QUANTITY"].Value.ToString());
                Double unitprice = (MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value == null || MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value.ToString().Equals("")) ? 0 : Double.Parse(MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value.ToString());
                MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value = unitprice.ToString();
                ////unitprice = 1000;
                //if (activity.Equals("UPDATE"))
                //{
                //    MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value = unitprice.ToString();
                //}
                //else if (activity.Equals("INSERT"))
                //{
                //    MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value = unitprice.ToString();
                //}

                Double amount = (quantity * unitprice);

                MasterItemdataGridView.Rows[e.RowIndex].Cells["AMOUNT"].Value = amount.ToString();

                Double total = 0.0;
                foreach (DataGridViewRow row in MasterItemdataGridView.Rows)
                {
                    total += Double.Parse(row.Cells["AMOUNT"].Value.ToString());
                }
                TotaltextBox.Text = total.ToString();

//                setDecimalN2();
            }
        }

        private void PONotextBox_Leave(object sender, EventArgs e)
        {
            if (!isFind)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in PurchaseOrderNo)
                {
                    if (item.ToUpper().Equals(PONotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        PONotextBox.Text = PONotextBox.Text.ToUpper();
                        String query = String.Format("SELECT PURCHASEORDERID, CONVERT(VARCHAR(10), PURCHASEORDERDATE, 103) AS [PURCHASEORDERDATE], A.SUPPLIERID,SUPPLIERNAME, " +
                                                     "CONTACTPERSONSUPPLIER, KURSID, RATE, REMARK, A.STATUS, A.CREATEDBY " +
                                                     "FROM H_PURCHASE_ORDER A " +
                                                     "JOIN M_SUPPLIER B " +
                                                     "ON A.SUPPLIERID = B.SUPPLIERID " +
                                                     "WHERE PURCHASEORDERID = '{0}'", PONotextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        PurchaseOrderPassingData(datatable);
//                        setDecimalN2();
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    newTransaction();
                }
            }
        }

        private void PONotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                this.datatable.Clear();
                foreach (String item in PurchaseOrderNo)
                {
                    if (item.ToUpper().Equals(PONotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        PONotextBox.Text = PONotextBox.Text.ToUpper();
                        String query = String.Format("SELECT PURCHASEORDERID, CONVERT(VARCHAR(10), PURCHASEORDERDATE, 103) AS [PURCHASEORDERDATE], A.SUPPLIERID,SUPPLIERNAME, " +
                                                     "CONTACTPERSONSUPPLIER, KURSID, RATE, REMARK, A.STATUS, A.CREATEDBY " +
                                                     "FROM H_PURCHASE_ORDER A " +
                                                     "JOIN M_SUPPLIER B " +
                                                     "ON A.SUPPLIERID = B.SUPPLIERID " +
                                                     "WHERE PURCHASEORDERID = '{0}'",PONotextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        PurchaseOrderPassingData(datatable);
//                        setDecimalN2();
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    newTransaction();
                    
                }
            }
        }

        private void PurchaseOrderPassingData(DataTable sender){
            if (sender.Rows[0]["STATUS"].ToString().Equals(Status.CLOSE)) {
                return;
            }

            PONotextBox.Text = sender.Rows[0]["PURCHASEORDERID"].ToString();
            PODatetextBox.Text = sender.Rows[0]["PURCHASEORDERDATE"].ToString();
            SupplierID = sender.Rows[0]["SUPPLIERID"].ToString(); 
            SupplierNametextBox.Text = sender.Rows[0]["SUPPLIERNAME"].ToString(); 
            ContactPersontextBox.Text = sender.Rows[0]["CONTACTPERSONSUPPLIER"].ToString();
            KurstextBox.Text = sender.Rows[0]["KURSID"].ToString();
            RatetextBox.Text = sender.Rows[0]["RATE"].ToString();
            //StatustextBox.Text = sender.Rows[0]["STATUS"].ToString(); 
            //RemarkrichTextBox.Text = sender.Rows[0]["REMARK"].ToString(); 
            sender.Rows[0]["CREATEDBY"].ToString();
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMID, ITEMNAME, " +
                                         "A.ITEMIDQTY, (SELECT ITEMNAME FROM M_ITEM WHERE ITEMID = A.ITEMIDQTY) AS ITEMNAMEQTY, " +
                                         "A.QUANTITY, UOMID, PRICE AS UNITPRICE, '0' AS AMOUNT " +
                                         "FROM D_PURCHASE_ORDER A " +
                                         "JOIN M_ITEM B " +
                                         "ON A.ITEMID = B.ITEMID " +
                                         "WHERE PURCHASEORDERID = '{0}' ",PONotextBox.Text);
            
            DataTable datatable = connection.openDataTableQuery(query);

            searchKursbutton.Visible = true;
            KurstextBox.Enabled = true;
            RatetextBox.Enabled = true;
            AddItembutton.Enabled = true;
            this.datatable.Clear();
            MasterItemPurhcaseOrderPassingData(datatable);
            //MasterItemPassingData(datatable);
        }

        private void SearchPONObutton_Click(object sender, EventArgs e)
        {
            if (purchaseorderdatagridview == null)
            {
                purchaseorderdatagridview = new PurchaseOrderdatagridview("PurchaseReceipt");
                purchaseorderdatagridview.purchaseOrderPassingData = new PurchaseOrderdatagridview.PurchaseOrderPassingData(PurchaseOrderPassingData);
                //purchaseorderdatagridview.MdiParent = this.MdiParent;
                purchaseorderdatagridview.ShowDialog();
            }
            else if (purchaseorderdatagridview.IsDisposed)
            {
                purchaseorderdatagridview = new PurchaseOrderdatagridview("PurchaseReceipt");
                purchaseorderdatagridview.purchaseOrderPassingData = new PurchaseOrderdatagridview.PurchaseOrderPassingData(PurchaseOrderPassingData);
                //purchaseorderdatagridview.MdiParent = this.MdiParent;
                purchaseorderdatagridview.ShowDialog();
            }
//            setDecimalN2();
        }
        //insert ke detail d_return_purchase bermasalah
        private void SearchRENObutton_Click(object sender, EventArgs e)
        {
            if (purchasedatagridview == null)
            {
                purchasedatagridview = new Purchasedatagridview("PurchaseOrder");
                purchasedatagridview.purchasePassingData = new Purchasedatagridview.PurchasePassingData(ReceiptPassingData);
                //purchasedatagridview.MdiParent = this.MdiParent;
                purchasedatagridview.ShowDialog();
            }
            else if (purchasedatagridview.IsDisposed)
            {
                purchasedatagridview = new Purchasedatagridview("PurchaseOrder");
                purchasedatagridview.purchasePassingData = new Purchasedatagridview.PurchasePassingData(ReceiptPassingData);
                //purchasedatagridview.MdiParent = this.MdiParent;
                purchasedatagridview.ShowDialog();
            }
            setDecimalN2();
        }

        private void ReceiptPassingData(DataTable sender) {

            ReceiptNotextBox.Text = sender.Rows[0]["PURCHASEID"].ToString();
            ReceiptDatetextBox.Text = sender.Rows[0]["PURCHASEDATE"].ToString();
            PONotextBox.Text = sender.Rows[0]["PURCHASEORDERID"].ToString();
            PODatetextBox.Text = sender.Rows[0]["PURCHASEORDERDATE"].ToString().Equals("") ? "-" : sender.Rows[0]["PURCHASEORDERDATE"].ToString();
            //SupplierID = sender.Rows[0]["SUPPLIERID"].ToString();
            SupplierNametextBox.Text = sender.Rows[0]["SUPPLIERNAME"].ToString();
            ContactPersontextBox.Text = sender.Rows[0]["CONTACTPERSONSUPPLIER"].ToString();
            KurstextBox.Text = sender.Rows[0]["KURSID"].ToString();
            RatetextBox.Text = sender.Rows[0]["RATE"].ToString();
            RemarkrichTextBox.Text = sender.Rows[0]["REMARK"].ToString();
            sender.Rows[0]["CREATEDBY"].ToString();
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMID, B.ITEMNAME, A.ITEMIDQTY, " +
                                         "(SELECT ITEMNAME FROM M_ITEM WHERE ITEMID =  A.ITEMIDQTY ) AS ITEMNAMEQTY, " +
                                         "A.QUANTITY, UOMID, PRICE AS UNITPRICE, '0' AS AMOUNT  " +
                                         "FROM D_PURCHASE A " +
                                         "JOIN M_ITEM B " +
                                         "ON A.ITEMID = B.ITEMID " +
                                         "WHERE PURCHASEID = '{0}'", ReceiptNotextBox.Text);

            DataTable datatable = connection.openDataTableQuery(query);

            this.datatable.Clear();
            MasterItemPurhcasePassingData(datatable);
        }

        private void ReceiptNotextBox_Leave(object sender, EventArgs e)
        {
            Boolean isExistingPartNo = false;
            foreach (String item in ReceiptNo)
            {
                if (item.ToUpper().Equals(ReceiptNotextBox.Text.ToUpper()))
                {
                    isExistingPartNo = true;
                    ReceiptNotextBox.Text = ReceiptNotextBox.Text.ToUpper();
                    String query = String.Format("SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY PURCHASEID) AS 'No', " +
                                                 "PURCHASEID, CONVERT(VARCHAR(10), PURCHASEDATE, 103) AS [PURCHASEDATE], " +
                                                 "A.PURCHASEORDERID, " +
                                                 "(SELECT CONVERT(VARCHAR(10), PURCHASEORDERDATE, 103) AS [PURCHASEORDERDATE] FROM H_PURCHASE_ORDER C WHERE C.PURCHASEORDERID = A.PURCHASEORDERID) AS [PURCHASEORDERDATE], " +
                                                 "B.SUPPLIERNAME, A.CONTACTPERSONSUPPLIER, A.KURSID, A.RATE, A.STATUS, A.REMARK, A.CREATEDBY " +
                                                 "FROM H_PURCHASE A, M_SUPPLIER B " +
                                                 "WHERE A.SUPPLIERID = B.SUPPLIERID " +
                                                 "AND A.STATUS = '{0}' AND PURCHASEID = '{1}' " +
                                                 "ORDER BY PURCHASEORDERID ASC", Status.ACTIVE, ReceiptNotextBox.Text);

                    DataTable datatable = connection.openDataTableQuery(query);
                    ReceiptPassingData(datatable);
//                    setDecimalN2();
                    break;
                }
            }
            if (!isExistingPartNo)
            {
                //newTransaction();
                Utilities.clearAllField(ref controlTextBox);
                this.datatable.Clear();
                //ReceiptNotextBox.Text = "";
                //ReceiptDatetextBox.Text = "";
            }
        }

        private void ReceiptNotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                this.datatable.Clear();
                foreach (String item in ReceiptNo)
                {
                    if (item.ToUpper().Equals(ReceiptNotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        ReceiptNotextBox.Text = ReceiptNotextBox.Text.ToUpper();
                        String query = String.Format("SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY PURCHASEID) AS 'No', " +
                                                     "PURCHASEID, CONVERT(VARCHAR(10), PURCHASEDATE, 103) AS [PURCHASEDATE], " +
                                                     "A.PURCHASEORDERID, " +
                                                     "(SELECT CONVERT(VARCHAR(10), PURCHASEORDERDATE, 103) AS [PURCHASEORDERDATE] FROM H_PURCHASE_ORDER C WHERE C.PURCHASEORDERID = A.PURCHASEORDERID) AS [PURCHASEORDERDATE], " +
                                                     "B.SUPPLIERNAME, A.CONTACTPERSONSUPPLIER, A.KURSID, A.RATE, A.STATUS, A.REMARK, A.CREATEDBY " +
                                                     "FROM H_PURCHASE A, M_SUPPLIER B " +
                                                     "WHERE A.SUPPLIERID = B.SUPPLIERID " +
                                                     "AND A.STATUS = '{0}' AND PURCHASEID = '{1}' " +
                                                     "ORDER BY PURCHASEORDERID ASC", Status.ACTIVE, ReceiptNotextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        ReceiptPassingData(datatable);
//                        setDecimalN2();
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    //newTransaction();
                    Utilities.clearAllField(ref controlTextBox);
                    this.datatable.Clear();
                    //ReceiptNotextBox.Text = "";
                    //ReceiptDatetextBox.Text = "";
                }
            }
        }

        private void directioncheckbox_CheckedChanged(object sender, EventArgs e)
        {
            newTransaction();
            PONotextBox.Text = directioncheckbox.Checked ? "-" : "";
            PODatetextBox.Text = directioncheckbox.Checked ? "-" : "";
            PONotextBox.Enabled = !directioncheckbox.Checked;
            PODatetextBox.Enabled = !directioncheckbox.Checked;
            SearchPONObutton.Enabled = !directioncheckbox.Checked;
            SupplierNametextBox.Enabled = directioncheckbox.Checked;
            SearchsupplierButton.Visible = directioncheckbox.Checked;
            KurstextBox.Enabled = directioncheckbox.Checked;
            KurstextBox.Text = "IDR";
            RatetextBox.Enabled = directioncheckbox.Checked;
            RatetextBox.Text = "1";

            searchKursbutton.Visible = directioncheckbox.Checked;
        }

        private void SearchsupplierButton_Click(object sender, EventArgs e)
        {
            if (supplierdatagridview == null)
            {
                supplierdatagridview = new Supplierdatagridview("MasterSupplier");
                supplierdatagridview.masterItemPassingData = new Supplierdatagridview.MasterItemPassingData(MasterSupplierPassingData);
                //supplierdatagridview.MdiParent = this.MdiParent;
                supplierdatagridview.ShowDialog();
            }
            else if (supplierdatagridview.IsDisposed)
            {
                supplierdatagridview = new Supplierdatagridview("MasterSupplier");
                supplierdatagridview.masterItemPassingData = new Supplierdatagridview.MasterItemPassingData(MasterSupplierPassingData);
                //supplierdatagridview.MdiParent = this.MdiParent;
                supplierdatagridview.ShowDialog();
            }
        }

        private void MasterSupplierPassingData(DataTable sender) {
            if (sender.Rows.Count == 0) {
                ContactPersontextBox.Text = "";
                return;
            }
            SupplierID = sender.Rows[0]["SUPPLIERID"].ToString();
            SupplierNametextBox.Text = sender.Rows[0]["SUPPLIERNAME"].ToString();

            String query = String.Format("SELECT CONTACTPERSON " +
                                         "FROM D_PERSON_SUPPLIER " +
                                         "WHERE SUPPLIERID = '{0}'", SupplierID);

            
            if (contactPerson == null) contactPerson = new List<string>();

            contactPerson.Clear();

            SqlDataReader sqldataReader = connection.sqlDataReaders(query);

            while (sqldataReader.Read()) contactPerson.Add(sqldataReader.GetString(0));

            ContactPersontextBox.AutoCompleteCustomSource.Clear();

            ContactPersontextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            ContactPersontextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            ContactPersontextBox.AutoCompleteCustomSource.AddRange(contactPerson.ToArray());
            ContactPersontextBox.Text = sender.Rows[0]["CONTACTPERSON"].ToString();
            ContactPersontextBox.Enabled = true;
            
            AddItembutton.Enabled = true;
            this.datatable.Clear();
        }

        private void SupplierNametextBox_Leave(object sender, EventArgs e)
        {
            Boolean isExistingPartNo = false;
            foreach (String item in SupplierName)
            {
                if (item.ToUpper().Equals(SupplierNametextBox.Text.ToUpper()))
                {
                    isExistingPartNo = true;

                    String query = String.Format("SELECT TOP 1 A.SUPPLIERID, SUPPLIERNAME, CONTACTPERSON " +
                                                 "FROM M_SUPPLIER A " +
                                                 "JOIN D_PERSON_SUPPLIER B " +
                                                 "ON A.SUPPLIERID = B.SUPPLIERID " +
                                                 "WHERE A.SUPPLIERNAME = '{0}' " +
                                                 "ORDER BY CONTACTPERSON", SupplierNametextBox.Text);

                    DataTable datatable = connection.openDataTableQuery(query);
                    MasterSupplierPassingData(datatable);

                    break;
                }
            }

            if (!isExistingPartNo)
            {
                SupplierNametextBox.Text = "";
                ContactPersontextBox.Text = "";
                ContactPersontextBox.AutoCompleteCustomSource.Clear();
                ContactPersontextBox.Enabled = false;
                this.datatable.Clear();
                AddItembutton.Enabled = false;
            }
        }

        private void SupplierNametextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                this.datatable.Clear();
                foreach (String item in SupplierName)
                {
                    if (item.ToUpper().Equals(SupplierNametextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        SupplierNametextBox.Text = item;

                        String query = String.Format("SELECT TOP 1 A.SUPPLIERID, SUPPLIERNAME, CONTACTPERSON " +
                                                     "FROM M_SUPPLIER A  " +
                                                     "JOIN D_PERSON_SUPPLIER B " +
                                                     "ON A.SUPPLIERID = B.SUPPLIERID " +
                                                     "WHERE A.SUPPLIERNAME = '{0}' " +
                                                     "ORDER BY CONTACTPERSON", SupplierNametextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        MasterSupplierPassingData(datatable);
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    SupplierNametextBox.Text = "";
                    ContactPersontextBox.Text = "";
                    ContactPersontextBox.Enabled = false;
                    ContactPersontextBox.AutoCompleteCustomSource.Clear();
                    this.datatable.Clear();
                    AddItembutton.Enabled = false;
                }
            }
        }

        private void ContactPersontextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in contactPerson)
                {
                    if (item.ToUpper().Equals(ContactPersontextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        ContactPersontextBox.Text = item;
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    ContactPersontextBox.Text = "";
                }
            }
        }

        private void ContactPersontextBox_Leave(object sender, EventArgs e)
        {
            if (contactPerson != null)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in contactPerson)
                {
                    if (item.ToUpper().Equals(ContactPersontextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    ContactPersontextBox.Text = "";
                }
            }
        }

        private void PurchaseReceipt_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            reloadAllData();
            Main.ACTIVEFORM = this;
        }

        private void setDecimalN2()
        {
            foreach (DataGridViewRow row in MasterItemdataGridView.Rows)
            {
                decimal nilai = Decimal.Parse(row.Cells["AMOUNT"].Value.ToString());
                row.Cells["AMOUNT"].Value = nilai.ToString("N2");
            }
        }

        private void MasterItemdataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 8 || e.ColumnIndex == 9)
            {
                try
                {
                    Decimal d = Decimal.Parse(e.Value.ToString());
                    e.Value = d.ToString("N2");
                }
                catch (Exception ex)
                {
                    Decimal d = 0;
                    e.Value = d.ToString("N2");
                }
            }
        }

        private void PurchaseReceipt_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show("Anda yakin mau keluar?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (result == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
    }
}