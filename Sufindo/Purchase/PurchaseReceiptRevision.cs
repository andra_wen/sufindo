﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Resources;

namespace Sufindo.Purchase
{
    public partial class PurchaseReceiptRevision : Form
    {
        private delegate void fillAutoCompleteTextBox();

        Purchasedatagridview purchasedatagridview;
        MenuStrip menustripAction;
        Connection connection;
        List<String> RevisiNo;
        List<String> ReceiptNo;
        List<Control> controlTextBox;
        
        DataTable datatable;

        ReceiptRevisiondatagridview receiverevisionview;
        Itemdatagridview itemdatagridview;

        String activity = "";
        Boolean isFind = false;
        Boolean addNewItem = false;
        Boolean isSaved = false;
        String PurchaseID = "";

        public PurchaseReceiptRevision()
        {
            InitializeComponent();
        }

        private void SearchRevisiNObutton_Click(object sender, EventArgs e)
        {
            if (receiverevisionview == null)
            {
                receiverevisionview = new ReceiptRevisiondatagridview("PurchaseReceiptRevision");
                receiverevisionview.receiverevisionPassingData = new ReceiptRevisiondatagridview.ReceiveRevisionPassingData(MasterReceiveRevisionPassingData);
                receiverevisionview.ShowDialog();
            }
            else if (receiverevisionview.IsDisposed)
            {
                receiverevisionview = new ReceiptRevisiondatagridview("PurchaseReceiptRevision");
                receiverevisionview.receiverevisionPassingData = new ReceiptRevisiondatagridview.ReceiveRevisionPassingData(MasterReceiveRevisionPassingData);
                receiverevisionview.ShowDialog();
            }
//            setDecimalN2();
        }

        private void MasterReceiveRevisionPassingData(DataTable sender)
        {
            sender.Rows[0]["REVISIONID"].ToString();
            RevisiNotextBox.Text = sender.Rows[0]["REVISIONID"].ToString();
            RevisionDatetextBox.Text = sender.Rows[0]["REVISIONDATE"].ToString();
            ReceiptNotextBox.Text = sender.Rows[0]["PURCHASEID"].ToString();
            ReceiptDatetextBox.Text = sender.Rows[0]["PURCHASEDATE"].ToString();

            PurchaseID = sender.Rows[0]["PURCHASEID"].ToString();

            SupplierNametextBox.Text = sender.Rows[0]["SUPPLIERNAME"].ToString();
            ContactPersontextBox.Text = sender.Rows[0]["CONTACTPERSONSUPPLIER"].ToString();
            KurstextBox.Text = sender.Rows[0]["KURSNAME"].ToString();
            RatetextBox.Text = sender.Rows[0]["RATE"].ToString();

            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMID, B.ITEMNAME, A.ITEMIDQTY, " +
                                        "C.ITEMNAME AS ITEMNAMEQTY, ISNULL(E.QUANTITY,0) AS QTYORDER, A.QUANTITY AS QTYREVISI, B.UOMID, A.PRICE AS UNITPRICE, '0' AS AMOUNT " +
                                        "FROM D_REVISI_PURCHASE A " +
                                        "JOIN M_ITEM B ON A.ITEMID = B.ITEMID " +
                                        "JOIN M_ITEM C ON A.ITEMIDQTY = C.ITEMID " +
                                        "JOIN H_REVISI_PURCHASE D ON D.REVISIONID = A.REVISIONID " +
                                        "LEFT JOIN D_PURCHASE E ON D.PURCHASEID = E.PURCHASEID AND A.ITEMID = E.ITEMID " +
                                        "WHERE A.REVISIONID = '{0}'", RevisiNotextBox.Text);

            DataTable datatable = connection.openDataTableQuery(query);

            this.datatable.Clear();
            MasterItemPassingData(datatable);
        }

        public PurchaseReceiptRevision(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            controlTextBox = new List<Control>();
            this.menustripAction = menustripAction;

            if (connection == null)
            {
                connection = new Connection();
            }

            foreach (Control control in this.tableLayoutPanel.Controls)
            {
                if (control is TextBox)
                {
                    controlTextBox.Add(control);
                }
                else if (control is Panel)
                {
                    foreach (Control controlPanel in control.Controls)
                    {
                        if (controlPanel is RichTextBox)
                        {
                            controlTextBox.Add(controlPanel);
                        }
                    }
                }
                else if (control is ComboBox)
                {
                    controlTextBox.Add(control);
                }
            }
            datatable = new DataTable();
            newTransaction();
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }

        public void save(){
            if (activity.Equals("UPDATE") || activity.Equals("INSERT"))
            {
                if (isValidation())
                {
                    List<SqlParameter> sqlParam = new List<SqlParameter>();

                    if (activity.Equals("UPDATE")) sqlParam.Add(new SqlParameter("@REVISIONID", RevisiNotextBox.Text));
                    sqlParam.Add(new SqlParameter("@PURCHASEID", ReceiptNotextBox.Text));
                    sqlParam.Add(new SqlParameter("@STATUS", Status.ACTIVE));
                    sqlParam.Add(new SqlParameter("@REMARK", RemarkrichTextBox.Text));
                    sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));

                    String proc = activity.Equals("UPDATE") ? "UPDATE_DATA_H_REVISI" : "INSERT_DATA_H_REVISI";

                    DataTable dt = connection.callProcedureDatatable(proc, sqlParam);

                    if (dt.Rows.Count == 1)
                    {
                        
                        List<string> values = new List<string>();
                        foreach (DataGridViewRow row in this.MasterItemdataGridView.Rows)
                        {
                            String unitpricestr = row.Cells["UNITPRICE"].Value.ToString().Contains(",") ? row.Cells["UNITPRICE"].Value.ToString().Replace(",", "") : row.Cells["UNITPRICE"].Value.ToString();
                            String value = String.Format("SELECT '{0}', '{1}', '{2}', {3}, {4}", dt.Rows[0]["REVISIONID"].ToString(), row.Cells["ITEMID"].Value,
                                                                                               row.Cells["ITEMIDQTY"].Value.ToString().Equals("") ? row.Cells["ITEMID"].Value : row.Cells["ITEMIDQTY"].Value,
                                                                                               row.Cells["QTYREVISI"].Value, unitpricestr);
                            values.Add(value);
                        }
                        String[] columns = { "REVISIONID", "ITEMID", "ITEMIDQTY", "QUANTITY", "PRICE" };
                        String query = String.Format("DELETE FROM D_REVISI_PURCHASE WHERE REVISIONID = '{0}'", dt.Rows[0]["REVISIONID"].ToString());
                        if (connection.openReaderQuery(query))
                        {
                            if (connection.openReaderQuery(Utilities.queryMultipleInsert("D_REVISI_PURCHASE", columns, values)))
                            {
                                MessageBox.Show(String.Format("Receipt Revision No {0}, Confirm dulu stock baru berubah", dt.Rows[0]["REVISIONID"].ToString()));
                                //DialogResult conf = MessageBox.Show(this, "Ingin membuat Revisi baru?", "Revisi", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                                //MessageBox.Show(String.Format("Receipt Revision No {0}, Stock sudah revisi", dt.Rows[0]["REVISIONID"].ToString()));
                                //if (conf == DialogResult.Yes)
                                //{
                                    reloadAllData();
                                    newTransaction();
                                    isSaved = false;
                                //}
                                //else
                                //{
                                //    RevisiNotextBox.Text = dt.Rows[0]["REVISIONID"].ToString();
                                //    reloadAllData();
                                //    isSaved = true;
                                //}
                            }
                        }
                    }
                }
            }
        }

        private Boolean isValidation()
        {
            if (Utilities.isEmptyString(ReceiptNotextBox.Text))
            {
                MessageBox.Show("Please Select Receipt No ID");
            }
            else if (MasterItemdataGridView.Rows.Count == 0)
            {
                MessageBox.Show("Minimal One Part Item  For Order");
            }
            else
            {
                return true;
            }
            return false;
        }

        public void newTransaction()
        {
            Utilities.clearAllField(ref controlTextBox);
            datatable.Clear();

            RevisiNotextBox.Text = DateTime.Now.ToString("yyyy") + "XXXXXX";
            RevisiNotextBox.Enabled = false;
            RevisiNotextBox.BackColor = SystemColors.Info;

            RevisionDatetextBox.Enabled = false;
            RevisionDatetextBox.BackColor = SystemColors.Info;
            RevisionDatetextBox.Text = DateTime.Now.ToString("dd/MM/yyyy");

            ReceiptNotextBox.Text = "";
            ReceiptNotextBox.BackColor = Color.White;
            ReceiptNotextBox.Enabled = true;

            AddItembutton.Enabled = true;

            SearchRevisiNObutton.Visible = false;
            SearchRcvNObutton.Visible = true;
            
            RemarkrichTextBox.ReadOnly = false;
            RemarkrichTextBox.BackColor = Color.White;

            activity = "INSERT";
            isFind = false;
            isSaved = false;

            TotaltextBox.Enabled = false;
            TotaltextBox.BackColor = SystemColors.Info;

            SupplierNametextBox.Enabled = false;
            TotaltextBox.BackColor = SystemColors.Info;

            ContactPersontextBox.Enabled = false;
            TotaltextBox.BackColor = SystemColors.Info;

            KurstextBox.Enabled = false;
            KurstextBox.BackColor = SystemColors.Info;

            RatetextBox.Enabled = false;
            RatetextBox.BackColor = SystemColors.Info;

            MasterItemdataGridView.AllowUserToDeleteRows = true;
            foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
            {
                if (column.Name.Equals("del"))
                {
                    column.Visible = true;
                    break;
                }
            }
            AllowUserToEditRows(true);
            //foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
            //{
            //    if (column.Name.Equals("del"))
            //    {
            //        column.Visible = true;
            //        break;
            //    }
            //}
            //directioncheckbox.Visible = true;

        }

        public void find(){
            if (!SearchRevisiNObutton.Visible || isFind)
            {
                SearchRevisiNObutton.Visible = true;
                AddItembutton.Enabled = false;
                SearchRcvNObutton.Visible = false;
                isFind = true;

                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("RevisiNotextBox"))
                    {
                        if (item is RichTextBox)
                        {
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = true;
                            richtextBox.BackColor = SystemColors.Info;
                            richtextBox.Text = "";
                        }
                        else
                        {
                            item.Enabled = false;
                            item.BackColor = SystemColors.Info;
                            item.Text = "";
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        currentTextbox.AutoCompleteCustomSource.AddRange(RevisiNo.ToArray());
                        currentTextbox.Text = "";
                        currentTextbox.Enabled = true;
                        currentTextbox.BackColor = Color.White;
                    }
                }
        //        searchKursbutton.Visible = false;
               
                datatable.Clear();
                ReceiptNotextBox.Text = "";
                ReceiptDatetextBox.Text = "";
                MasterItemdataGridView.AllowUserToDeleteRows = false;
                AllowUserToEditRows(false);
                addNewItem = false;
                foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
                {
                    if (column.Name.Equals("del"))
                    {
                        column.Visible = false;
                        break;
                    }
                }
                
                activity = "";
                SupplierNametextBox.Text = "";
                ContactPersontextBox.Text = "";
                KurstextBox.Text = "";
                //directioncheckbox.Visible = false;
            }
        }

        public void edit(){
            if ((!SearchRevisiNObutton.Visible || isFind))
            {
                find();
                AddItembutton.Enabled = true;
                SearchRevisiNObutton.Visible = true;
                RevisiNotextBox.Enabled = true;
                RevisiNotextBox.BackColor = SystemColors.Window;
                MasterItemdataGridView.AllowUserToDeleteRows = true;

                RemarkrichTextBox.Enabled = true;
                RemarkrichTextBox.BackColor = SystemColors.Window;
                RemarkrichTextBox.ReadOnly = false;

                addNewItem = false;
                foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
                {
                    if (column.Name.Equals("del"))
                    {
                        column.Visible = true;
                        break;
                    }
                }
                activity = "UPDATE";
                isSaved = false;
                AllowUserToEditRows(true);
            }
        }

        public void confirm()
        {
            if (RevisiNotextBox.Text.Contains("XXXXXX") || RevisiNotextBox.Text.Equals(""))
            {
                MessageBox.Show("Silakan Pilih Receipt Revision No terlebih dahulu.");
            }
            else {
                DialogResult conf = MessageBox.Show(this, String.Format("Apakah Anda ingin Confirm Receipt Revision No {0}?", RevisiNotextBox.Text), "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                if (conf == DialogResult.Yes)
                {
                    connection = new Connection();
                    DataTable dt = null;
                    dt = connection.openDataTableQuery(string.Format("SELECT * FROM H_REVISI_PURCHASE WHERE REVISIONID = '{0}'", RevisiNotextBox.Text));
                    if (dt.Rows.Count == 0)
                    {
                        MessageBox.Show(String.Format("Receipt Revision No {0} yang ingin di Confirm tidak di temukan", RevisiNotextBox.Text));
                        RevisiNotextBox.Focus();
                        if (!RevisiNotextBox.Text.Equals(""))
                        {
                            RevisiNotextBox.SelectAll();
                        }
                    }
                    else
                    {
                        if (isSaved || isFind)
                        {
                            List<SqlParameter> sqlParam = new List<SqlParameter>();

                            sqlParam.Add(new SqlParameter("@REVISIONID", RevisiNotextBox.Text));
                            sqlParam.Add(new SqlParameter("@STATUS", Status.CONFIRM));
                            sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));

                            String proc = "UPDATE_STATUS_REVISI";
                            //langsung execute
                            connection.callProcedureDatatable(proc, sqlParam);
                            MessageBox.Show(String.Format("Receipt Revision No {0} Telah Di Confirm", RevisiNotextBox.Text));
                            reloadAllData();
                            newTransaction();
                        }
                    }
                }
            }
        }

        private void AllowUserToEditRows(Boolean allow)
        {
            foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
            {
                column.ReadOnly = (column.Name.Equals("QTYREVISI") || column.Name.Equals("UNITPRICE")) ? !allow : true;
            }
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e){
            if (RevisiNo == null) RevisiNo = new List<string>();
            if (ReceiptNo == null) ReceiptNo = new List<string>();
            RevisiNo.Clear();
            ReceiptNo.Clear();

            SqlDataReader sqldataReader = connection.sqlDataReaders(String.Format("SELECT REVISIONID FROM H_REVISI_PURCHASE WHERE [STATUS] = '{0}'", Status.ACTIVE));

            while (sqldataReader.Read()) RevisiNo.Add(sqldataReader.GetString(0));

            sqldataReader = connection.sqlDataReaders(String.Format("SELECT PURCHASEID FROM H_PURCHASE WHERE [STATUS] = '{0}'", Status.ACTIVE));
            
            while (sqldataReader.Read()) ReceiptNo.Add(sqldataReader.GetString(0));

            if (IsHandleCreated) BeginInvoke(new fillAutoCompleteTextBox(this.fillAutoComplete));
        }

        private void fillAutoComplete()
        {

            RevisiNotextBox.AutoCompleteCustomSource.Clear();
            RevisiNotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            RevisiNotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            RevisiNotextBox.AutoCompleteCustomSource.AddRange(RevisiNo.ToArray());

            ReceiptNotextBox.AutoCompleteCustomSource.Clear();
            ReceiptNotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            ReceiptNotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            ReceiptNotextBox.AutoCompleteCustomSource.AddRange(ReceiptNo.ToArray());

        }

        private void MasterItemPassingData(DataTable datatable)
        {
            if(this.datatable == null) this.datatable = new DataTable();

        //    /*remove column not use*/
            List<string> columnsRemove = new List<string>();

            //pengecekan apakah ada itemname_alias / itemid_alias
            foreach (DataColumn columns in datatable.Columns)
            {
                if (columns.ColumnName.Equals("ITEMNAME_ALIAS"))
                {
                    columns.ColumnName = "ITEMNAMEQTY";
                }
                else if (columns.ColumnName.Equals("ITEMID_ALIAS"))
                {
                    columns.ColumnName = "ITEMIDQTY";
                }
            }

            foreach (DataColumn column in datatable.Columns)
            {
                Boolean isExistingColumn = false;
                foreach (DataGridViewColumn columngrid in MasterItemdataGridView.Columns)
                {
                    if (column.ColumnName.Equals(columngrid.DataPropertyName))
                    {
                        isExistingColumn = true;
                        break;
                    }
                }
                if (!isExistingColumn)
                {
                    columnsRemove.Add(column.ColumnName);
                }
            }

            foreach (String item in columnsRemove)
            {
                datatable.Columns.Remove(item); 
            }

            if (this.datatable.Rows.Count == 0) this.datatable = datatable;
            else
            {
                foreach (DataRow row in datatable.Rows)
                {
                    this.datatable.Rows.Add(row.ItemArray);
                }
            }

            foreach (DataRow row in this.datatable.Rows)
            {
                Object obj = "0";
                //row["QUANTITY"] = (addNewItem) ? obj : row["QUANTITY"];
                row["UNITPRICE"] = (row["UNITPRICE"].ToString().Equals("")) ? obj : row["UNITPRICE"];
                Int64 quantity = Int64.Parse(row["QTYORDER"].ToString());
                Int64 revsiqty = Int64.Parse(row["QTYREVISI"].ToString());
                Double unitprice = Double.Parse(row["UNITPRICE"].ToString());
                Int64 qty = revsiqty > 0 ? revsiqty : quantity;
                Double amount = qty * unitprice;
                row["AMOUNT"] = (Object)amount.ToString();
            }

            Utilities.generateNoDataTable(ref this.datatable);

            MasterItemdataGridView.DataSource = this.datatable;

            Double total = 0.0;
            foreach (DataGridViewRow row in MasterItemdataGridView.Rows)
            {
                total += Double.Parse(row.Cells["AMOUNT"].Value.ToString());
            }
            TotaltextBox.Text = total.ToString();
        }

        //private void AssignItemQuantityPassingData(DataTable sender) {
        //    MasterItemdataGridView.Rows[ASSIGNITEMROW].Cells["ITEMIDQTY"].Value = sender.Rows[0]["ITEMID"].ToString();
        //    MasterItemdataGridView.Rows[ASSIGNITEMROW].Cells["ITEMNAMEQTY"].Value = sender.Rows[0]["ITEMNAME"].ToString();
        //}

        //private void MasterItemdataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    if (e.RowIndex != -1 && e.ColumnIndex != -1){
        //        if (MasterItemdataGridView.Columns[e.ColumnIndex].Name.Equals("del") )
        //        {
        //            if (datatable.Rows.Count == 1) {
        //                TotaltextBox.Text = "0";
        //            }
        //            this.datatable.Rows.RemoveAt(e.RowIndex);
        //            Utilities.generateNoDataTable(ref datatable);
        //            MasterItemdataGridView.DataSource = datatable;
        //        }
        //        else if ((MasterItemdataGridView.Columns[e.ColumnIndex].Name.Equals("ITEMIDQTY") || MasterItemdataGridView.Columns[e.ColumnIndex].Name.Equals("ITEMNAMEQTY")) && activity.Equals("INSERT"))
        //        {
        //            ASSIGNITEMROW = e.RowIndex;
        //            String ITEMID = MasterItemdataGridView.Rows[e.RowIndex].Cells["ITEMID"].Value.ToString();
        //            if (itemdatagridview == null)
        //            {
        //                itemdatagridview = new Itemdatagridview("ASSIGNITEMPURCHASERECEIPT", ITEMID);
        //                itemdatagridview.assignItemQuantityPassingData = new Itemdatagridview.AssignItemQuantityPassingData(AssignItemQuantityPassingData);
        //                itemdatagridview.ShowDialog();
        //            }
        //            else if (itemdatagridview.IsDisposed)
        //            {
        //                itemdatagridview = new Itemdatagridview("ASSIGNITEMPURCHASERECEIPT", ITEMID);
        //                itemdatagridview.assignItemQuantityPassingData = new Itemdatagridview.AssignItemQuantityPassingData(AssignItemQuantityPassingData);
        //                itemdatagridview.ShowDialog();
        //            }

                    
        //        }
        //    }
        //}

        //private void MasterItemdataGridView_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        //{
        //    for (int i = 0; i < datatable.Rows.Count; i++)
        //    {
        //        if (datatable.Rows[i].RowState == DataRowState.Deleted)
        //        {
        //            datatable.Rows.RemoveAt(i);
        //        }
        //    }
        //    Utilities.generateNoDataTable(ref datatable);
        //}

        //private void PurchaseOrder_FormClosed(object sender, FormClosedEventArgs e)
        //{
        //    if (connection != null) connection.CloseConnection();
        //    Main.LISTFORM.Remove(this);
        //    if (Main.LISTFORM.Count == 0)
        //    {
        //        Utilities.showMenuStripAction(menustripAction.Items, false);
        //    }
        //    Main.ACTIVEFORM = null;
        //}

        //private void MasterItemdataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        //{
        //    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
        //}

        //private void Control_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    int currColumn = MasterItemdataGridView.CurrentCell.ColumnIndex;
        //    int currRow = MasterItemdataGridView.CurrentCell.RowIndex;
        //    if (MasterItemdataGridView.Columns[currColumn].Name.Equals("QUANTITY"))
        //    {
        //        e.Handled = Utilities.onlyNumberTextBox(sender, (KeyPressEventArgs)e);
        //    }
        //    else if (MasterItemdataGridView.Columns[currColumn].Name.Equals("UNITPRICE"))
        //    {
        //        e.Handled = Utilities.onlyNumberTextBox(sender, (KeyPressEventArgs)e);
        //    }
            
        //}

        //private void PONotextBox_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode == Keys.Enter)
        //    {
        //        Boolean isExistingPartNo = false;
        //        this.datatable.Clear();
        //        foreach (String item in PurchaseOrderNo)
        //        {
        //            if (item.ToUpper().Equals(RevisiNotextBox.Text.ToUpper()))
        //            {
        //                isExistingPartNo = true;
        //                RevisiNotextBox.Text = RevisiNotextBox.Text.ToUpper();
        //                String query = String.Format("SELECT PURCHASEORDERID, CONVERT(VARCHAR(10), PURCHASEORDERDATE, 111) AS [PURCHASEORDERDATE], A.SUPPLIERID,SUPPLIERNAME, " +
        //                                             "CONTACTPERSONSUPPLIER, KURSID, RATE, REMARK, A.STATUS, A.CREATEDBY " +
        //                                             "FROM H_PURCHASE_ORDER A " +
        //                                             "JOIN M_SUPPLIER B " +
        //                                             "ON A.SUPPLIERID = B.SUPPLIERID " +
        //                                             "WHERE PURCHASEORDERID = '{0}'",RevisiNotextBox.Text);

        //                DataTable datatable = connection.openDataTableQuery(query);
        //                PurchaseOrderPassingData(datatable);
        //                break;
        //            }
        //        }

        //        if (!isExistingPartNo)
        //        {
        //            newTransaction();
                    
        //        }
        //    }
        //}

        //private void PurchaseOrderPassingData(DataTable sender){
            
        //    RevisiNotextBox.Text = sender.Rows[0]["PURCHASEORDERID"].ToString();
        //    PODatetextBox.Text = DateTime.Parse(sender.Rows[0]["PURCHASEORDERDATE"].ToString()).ToString("dd/MM/yyyy");
        //    SupplierID = sender.Rows[0]["SUPPLIERID"].ToString(); 
        //    SupplierNametextBox.Text = sender.Rows[0]["SUPPLIERNAME"].ToString(); 
        //    ContactPersontextBox.Text = sender.Rows[0]["CONTACTPERSONSUPPLIER"].ToString();
        //    KurstextBox.Text = sender.Rows[0]["KURSID"].ToString();
        //    RatetextBox.Text = sender.Rows[0]["RATE"].ToString();
        //    //StatustextBox.Text = sender.Rows[0]["STATUS"].ToString(); 
        //    //RemarkrichTextBox.Text = sender.Rows[0]["REMARK"].ToString(); 
        //    sender.Rows[0]["CREATEDBY"].ToString();
        //    String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMID, ITEMNAME, " +
        //                                 "ITEMID_ALIAS AS ITEMIDQTY, (SELECT ITEMNAME FROM M_ITEM WHERE ITEMID = B.ITEMID_ALIAS) AS ITEMNAMEQTY, " +
        //                                 "A.QUANTITY, UOMID, PRICE AS UNITPRICE, '0' AS AMOUNT " +
        //                                 "FROM D_PURCHASE_ORDER A " +
        //                                 "JOIN M_ITEM B " +
        //                                 "ON A.ITEMID = B.ITEMID " +
        //                                 "WHERE PURCHASEORDERID = '{0}' ",RevisiNotextBox.Text);
            
        //    DataTable datatable = connection.openDataTableQuery(query);

        //    searchKursbutton.Visible = true;
        //    KurstextBox.Enabled = true;
        //    RatetextBox.Enabled = true;
        //    AddItembutton.Enabled = true;
        //    this.datatable.Clear();
        //    MasterItemPurhcaseOrderPassingData(datatable);
        //    //MasterItemPassingData(datatable);
        //}

        //private void SearchPONObutton_Click(object sender, EventArgs e)
        //{
        //    if (purchaseorderdatagridview == null)
        //    {
        //        purchaseorderdatagridview = new PurchaseOrderdatagridview("PurchaseReceipt");
        //        purchaseorderdatagridview.purchaseOrderPassingData = new PurchaseOrderdatagridview.PurchaseOrderPassingData(PurchaseOrderPassingData);
        //        //purchaseorderdatagridview.MdiParent = this.MdiParent;
        //        purchaseorderdatagridview.ShowDialog();
        //    }
        //    else if (purchaseorderdatagridview.IsDisposed)
        //    {
        //        purchaseorderdatagridview = new PurchaseOrderdatagridview("PurchaseReceipt");
        //        purchaseorderdatagridview.purchaseOrderPassingData = new PurchaseOrderdatagridview.PurchaseOrderPassingData(PurchaseOrderPassingData);
        //        //purchaseorderdatagridview.MdiParent = this.MdiParent;
        //        purchaseorderdatagridview.ShowDialog();
        //    }
        //}
        ////insert ke detail d_return_purchase bermasalah
        
        private void ReceiptPassingData(DataTable sender) 
        {
            connection = new Connection();
            DataTable dt = null;
            dt = connection.openDataTableQuery(string.Format("SELECT REVISIONID FROM H_REVISI_PURCHASE WHERE PURCHASEID = '{0}'", sender.Rows[0]["PURCHASEID"].ToString()));
            if (dt.Rows.Count == 0)
            {
                PurchaseID = sender.Rows[0]["PURCHASEID"].ToString();
                ReceiptNotextBox.Text = sender.Rows[0]["PURCHASEID"].ToString();
                ReceiptDatetextBox.Text = sender.Rows[0]["PURCHASEDATE"].ToString();
                SupplierNametextBox.Text = sender.Rows[0]["SUPPLIERNAME"].ToString();
                ContactPersontextBox.Text = sender.Rows[0]["CONTACTPERSONSUPPLIER"].ToString();
                KurstextBox.Text = sender.Rows[0]["KURSID"].ToString();
                RatetextBox.Text = sender.Rows[0]["RATE"].ToString();
                RemarkrichTextBox.Text = sender.Rows[0]["REMARK"].ToString();
                sender.Rows[0]["CREATEDBY"].ToString();
                String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMID, B.ITEMNAME, A.ITEMIDQTY, " +
                                            "C.ITEMNAME AS ITEMNAMEQTY, A.QUANTITY AS QTYORDER, '0' AS QTYREVISI, B.UOMID, PRICE AS UNITPRICE, '0' AS AMOUNT " +
                                            "FROM D_PURCHASE A " +
                                            "JOIN M_ITEM B ON A.ITEMID = B.ITEMID " +
                                            "JOIN M_ITEM C ON A.ITEMIDQTY = C.ITEMID " +
                                            "WHERE PURCHASEID = '{0}'", ReceiptNotextBox.Text);

                DataTable datatable = connection.openDataTableQuery(query);

                this.datatable.Clear();
                MasterItemPassingData(datatable);
            }
            else {
                MessageBox.Show("No Receipt ini sudah pernah di revisi dengan No Revisi : " + dt.Rows[0]["REVISIONID"].ToString());
                newTransaction();
            }
        }

        private void ReceiptNotextBox_Leave(object sender, EventArgs e)
        {
            Boolean isExistingPartNo = false;
            foreach (String item in ReceiptNo)
            {
                if (item.ToUpper().Equals(ReceiptNotextBox.Text.ToUpper()))
                {
                    isExistingPartNo = true;
                    connection = new Connection();
                    DataTable dt = null;
                    dt = connection.openDataTableQuery(string.Format("SELECT * FROM H_REVISI_PURCHASE WHERE PURCHASEID = '{0}'",ReceiptNotextBox.Text));
                    if(dt.Rows.Count ==0)
                    {
                        PurchaseID = ReceiptNotextBox.Text;
                        String query = String.Format("SELECT PURCHASEID, CONVERT(VARCHAR(10), PURCHASEDATE, 103) AS [PURCHASEDATE], " +
                                                    "A.PURCHASEORDERID, "+
                                                    "(SELECT CONVERT(VARCHAR(10), PURCHASEORDERDATE, 103) AS [PURCHASEORDERDATE] "+
                                                    "FROM H_PURCHASE_ORDER C WHERE C.PURCHASEORDERID = A.PURCHASEORDERID) AS [PURCHASEORDERDATE], "+
                                                    "B.SUPPLIERNAME, A.CONTACTPERSONSUPPLIER, A.KURSID, A.RATE, A.STATUS, A.REMARK, A.CREATEDBY "+
                                                    "FROM H_PURCHASE A, M_SUPPLIER B "+
                                                    "WHERE A.SUPPLIERID = B.SUPPLIERID "+
                                                    "AND PURCHASEID = '{0}'", ReceiptNotextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        ReceiptPassingData(datatable);
                    }
                    else
                    {
                        MessageBox.Show("No Receipt ini sudah pernah di revisi dengan No Revisi : " + dt.Rows[0]["REVISIONID"].ToString());
                        newTransaction();
                    }
                    break;
                }
            }
            if (!isExistingPartNo)
            {
                ReceiptNotextBox.Focus();
                ReceiptNotextBox.SelectAll();
            }
//            setDecimalN2();
        }

        private void PurchaseReceiptRevision_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            reloadAllData();
            Main.ACTIVEFORM = this;
        }

        private void SearchRcvNObutton_Click(object sender, EventArgs e)
        {
            if (purchasedatagridview == null)
            {
                purchasedatagridview = new Purchasedatagridview("PurchaseOrder");
                purchasedatagridview.purchasePassingData = new Purchasedatagridview.PurchasePassingData(ReceiptPassingData);
                purchasedatagridview.ShowDialog();
            }
            else if (purchasedatagridview.IsDisposed)
            {
                purchasedatagridview = new Purchasedatagridview("PurchaseOrder");
                purchasedatagridview.purchasePassingData = new Purchasedatagridview.PurchasePassingData(ReceiptPassingData);
                purchasedatagridview.ShowDialog();
            }
            ReceiptNotextBox.Focus();
        }

        private void AddItembutton_Click(object sender, EventArgs e)
        {
            if (ReceiptNotextBox.Text.Equals(""))
            {
                MessageBox.Show("No Revisi belum terisi");
                ReceiptNotextBox.Focus();
            }
            else
            {
                addNewItem = true;
                if (itemdatagridview == null)
                {
                    itemdatagridview = new Itemdatagridview("REVISIONRECEIPT", PurchaseID, datatable);
                    itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                    itemdatagridview.ShowDialog();
                }
                else if (itemdatagridview.IsDisposed)
                {
                    itemdatagridview = new Itemdatagridview("REVISIONRECEIPT", PurchaseID, datatable);
                    itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                    itemdatagridview.ShowDialog();
                }
            }
        }

        private void RevisiNotextBox_Leave(object sender, EventArgs e)
        {
            Boolean isExistingPartNo = false;
            foreach (String item in RevisiNo)
            {
                if (item.ToUpper().Equals(RevisiNotextBox.Text.ToUpper()))
                {
                    PurchaseID = RevisiNotextBox.Text.ToUpper();
                    isExistingPartNo = true;
                    String query = String.Format("SELECT B.REVISIONID, CONVERT(VARCHAR(10), B.REVISSIONDATE, 111) AS REVISIONDATE, "+
                                                "A.PURCHASEID, CONVERT(VARCHAR(10), A.PURCHASEDATE, 111) AS PURCHASEDATE, A.SUPPLIERID, "+
                                                "C.SUPPLIERNAME, A.CREATEDBY AS RECEIVE_BY, A.CONTACTPERSONSUPPLIER, D.KURSNAME, A.RATE "+
                                                "FROM H_PURCHASE A "+
                                                "JOIN H_REVISI_PURCHASE B ON A.PURCHASEID = B.PURCHASEID "+
                                                "JOIN M_SUPPLIER C ON A.SUPPLIERID = C.SUPPLIERID "+
                                                "JOIN M_KURS D ON A.KURSID = D.KURSID "+
                                                "WHERE B.REVISIONID = '{0}'", RevisiNotextBox.Text);

                    DataTable datatable = connection.openDataTableQuery(query);
                    MasterReceiveRevisionPassingData(datatable);
                    break;
                }
            }
            if (!isExistingPartNo)
            {
                ReceiptNotextBox.Focus();
                ReceiptNotextBox.SelectAll();
            }
//            setDecimalN2();
        }

        private void MasterItemdataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                if (MasterItemdataGridView.Columns[e.ColumnIndex].Name.Equals("del"))
                {
                    if (datatable.Rows.Count == 1)
                    {
                        TotaltextBox.Text = "0";
                    }
                    this.datatable.Rows.RemoveAt(e.RowIndex);
                    Utilities.generateNoDataTable(ref datatable);
                    MasterItemdataGridView.DataSource = datatable;
                }
                else if ((MasterItemdataGridView.Columns[e.ColumnIndex].Name.Equals("ITEMIDQTY") || MasterItemdataGridView.Columns[e.ColumnIndex].Name.Equals("ITEMNAMEQTY")) && activity.Equals("INSERT"))
                {
                    //ASSIGNITEMROW = e.RowIndex;
                    //String ITEMID = MasterItemdataGridView.Rows[e.RowIndex].Cells["ITEMID"].Value.ToString();
                    //if (itemdatagridview == null)
                    //{
                    //    itemdatagridview = new Itemdatagridview("ASSIGNITEMPURCHASERECEIPT", ITEMID);
                    //    itemdatagridview.assignItemQuantityPassingData = new Itemdatagridview.AssignItemQuantityPassingData(AssignItemQuantityPassingData);
                    //    itemdatagridview.ShowDialog();
                    //}
                    //else if (itemdatagridview.IsDisposed)
                    //{
                    //    itemdatagridview = new Itemdatagridview("ASSIGNITEMPURCHASERECEIPT", ITEMID);
                    //    itemdatagridview.assignItemQuantityPassingData = new Itemdatagridview.AssignItemQuantityPassingData(AssignItemQuantityPassingData);
                    //    itemdatagridview.ShowDialog();
                    //}
                }
            }
        }

        private void MasterItemdataGridView_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
                if (MasterItemdataGridView.Rows.Count > 0)
                {
                    Int64 quantity = (MasterItemdataGridView.Rows[e.RowIndex].Cells["QTYORDER"].Value == null || MasterItemdataGridView.Rows[e.RowIndex].Cells["QTYORDER"].Value.ToString().Equals("")) ? 0 : Int64.Parse(MasterItemdataGridView.Rows[e.RowIndex].Cells["QTYORDER"].Value.ToString());
                    Int64 revsiqty = (MasterItemdataGridView.Rows[e.RowIndex].Cells["QTYREVISI"].Value == null || MasterItemdataGridView.Rows[e.RowIndex].Cells["QTYREVISI"].Value.ToString().Equals("")) ? 0 : Int64.Parse(MasterItemdataGridView.Rows[e.RowIndex].Cells["QTYREVISI"].Value.ToString());

                    //if (revsiqty > quantity) {
                    //    revsiqty = quantity;
                    //    MasterItemdataGridView.Rows[e.RowIndex].Cells["QTYREVISI"].Value = revsiqty;
                    //}

                    Double unitprice = (MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value == null || MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value.ToString().Equals("")) ? 0 : Double.Parse(MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value.ToString());
                    MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value = unitprice.ToString();

                    Int64 qty = revsiqty > 0 ? revsiqty : quantity;
                    //Double amount = (quantity * unitprice) - (revsiqty * unitprice);
                    Double amount = qty * unitprice;
                    
                    MasterItemdataGridView.Rows[e.RowIndex].Cells["AMOUNT"].Value = amount;
                    
                    Double total = 0.0;
                    foreach (DataGridViewRow row in MasterItemdataGridView.Rows)
                    {
                        total += Double.Parse(row.Cells["AMOUNT"].Value.ToString());
                    }
                    TotaltextBox.Text = total.ToString();
                }
        }

        private void PurchaseReceiptRevision_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                if (connection != null) connection.CloseConnection();
                if (Main.LISTFORM.Count != 0)
                {
                    Form f = Main.LISTFORM.First(s => s.GetType().Equals(this.GetType()));
                    if (f != null)
                    {
                        Main.LISTFORM.Remove(this);
                        if (Main.LISTFORM.Count == 0)
                        {
                            Utilities.showMenuStripAction(menustripAction.Items, false);
                        }
                        Main.ACTIVEFORM = null;
                    }
                }
                this.Dispose();
            }
            catch (Exception ex)
            {

            }
        }

        private void ReceiptNotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in ReceiptNo)
                {
                    if (item.ToUpper().Equals(ReceiptNotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        connection = new Connection();
                        DataTable dt = null;
                        dt = connection.openDataTableQuery(string.Format("SELECT REVISIONID FROM H_REVISI_PURCHASE WHERE PURCHASEID = '{0}'", ReceiptNotextBox.Text));
                        if (dt.Rows.Count == 0)
                        {
                            PurchaseID = ReceiptNotextBox.Text;
                            String query = String.Format("SELECT PURCHASEID, CONVERT(VARCHAR(10), PURCHASEDATE, 103) AS [PURCHASEDATE], " +
                                                        "A.PURCHASEORDERID, " +
                                                        "(SELECT CONVERT(VARCHAR(10), PURCHASEORDERDATE, 103) AS [PURCHASEORDERDATE] " +
                                                        "FROM H_PURCHASE_ORDER C WHERE C.PURCHASEORDERID = A.PURCHASEORDERID) AS [PURCHASEORDERDATE], " +
                                                        "B.SUPPLIERNAME, A.CONTACTPERSONSUPPLIER, A.KURSID, A.RATE, A.STATUS, A.REMARK, A.CREATEDBY " +
                                                        "FROM H_PURCHASE A, M_SUPPLIER B " +
                                                        "WHERE A.SUPPLIERID = B.SUPPLIERID " +
                                                        "AND PURCHASEID = '{0}'", ReceiptNotextBox.Text);

                            DataTable datatable = connection.openDataTableQuery(query);
                            ReceiptPassingData(datatable);
                        }
                        else
                        {
                            MessageBox.Show("No Receipt ini sudah pernah di revisi dengan No Revisi : " + dt.Rows[0]["REVISIONID"].ToString());
                            newTransaction();
                        }
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    ReceiptNotextBox.Focus();
                    ReceiptNotextBox.SelectAll();
                }
//                setDecimalN2();
            }
        }

        private void RevisiNotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in RevisiNo)
                {
                    if (item.ToUpper().Equals(RevisiNotextBox.Text.ToUpper()))
                    {
                        PurchaseID = RevisiNotextBox.Text.ToUpper();
                        isExistingPartNo = true;
                        String query = String.Format("SELECT B.REVISIONID, CONVERT(VARCHAR(10), B.REVISSIONDATE, 103) AS REVISIONDATE, " +
                                                    "A.PURCHASEID, CONVERT(VARCHAR(10), A.PURCHASEDATE, 103) AS PURCHASEDATE, A.SUPPLIERID, " +
                                                    "C.SUPPLIERNAME, A.CREATEDBY AS RECEIVE_BY, A.CONTACTPERSONSUPPLIER, D.KURSNAME, A.RATE " +
                                                    "FROM H_PURCHASE A " +
                                                    "JOIN H_REVISI_PURCHASE B ON A.PURCHASEID = B.PURCHASEID " +
                                                    "JOIN M_SUPPLIER C ON A.SUPPLIERID = C.SUPPLIERID " +
                                                    "JOIN M_KURS D ON A.KURSID = D.KURSID " +
                                                    "WHERE B.REVISIONID = '{0}'", RevisiNotextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        MasterReceiveRevisionPassingData(datatable);
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    ReceiptNotextBox.Focus();
                    ReceiptNotextBox.SelectAll();
                }
//                setDecimalN2();
            }
        }

        private void MasterItemdataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
        }

        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            int currColumn = MasterItemdataGridView.CurrentCell.ColumnIndex;
            int currRow = MasterItemdataGridView.CurrentCell.RowIndex;
            if (MasterItemdataGridView.Columns[currColumn].Name.Equals("QTYREVISI"))
            {
                e.Handled = Utilities.onlyNumberTextBox(sender, (KeyPressEventArgs)e);
            }
            else if (MasterItemdataGridView.Columns[currColumn].Name.Equals("UNITPRICE"))
            {
                //e.Handled = Utilities.onlyNumberTextBox(sender, (KeyPressEventArgs)e);
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
                {
                    e.Handled = true;
                }
            }
        }

        private void MasterItemdataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 9 || e.ColumnIndex == 10)
            {
                try
                {
                    Decimal d = Decimal.Parse(e.Value.ToString());
                    e.Value = d.ToString("N2");
                }
                catch (Exception ex)
                {
                    Decimal d = 0;
                    e.Value = d.ToString("N2");
                }
            }
        }

        private void MasterItemdataGridView_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            for (int i = 0; i < datatable.Rows.Count; i++)
            {
                if (datatable.Rows[i].RowState == DataRowState.Deleted)
                {
                    datatable.Rows.RemoveAt(i);
                }
            }
            Utilities.generateNoDataTable(ref datatable);
        }
    }
}