﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Sufindo.Report.Purchase;

namespace Sufindo.Purchase
{
    public partial class PurchaseOrder : Form
    {
        private delegate void fillAutoCompleteTextBox();

        PurchaseOrderdatagridview purchaseorderdatagridview;
        MenuStrip menustripAction;
        Connection connection;
        List<String> contactPerson;
        List<String> supplierID;
        List<String> KursID;
        List<String> PurchaseOrderNo;
        List<Control> controlTextBox;
        DataTable datatable;

        Supplierdatagridview supplierdatagridview;
        Kursdatagridview kursdatagridview;
        Itemdatagridview itemdatagridview;

        String activity = "";
        Boolean isFind = false;
        Boolean addNewItem = false;
        int ASSIGNITEMROW;

        public PurchaseOrder()
        {
            InitializeComponent();
        }

        public PurchaseOrder(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            this.menustripAction = menustripAction;
            supplierdatagridview = null;
            kursdatagridview = null;
            itemdatagridview = null;
            purchaseorderdatagridview = null;
            if (connection == null)
            {
                connection = new Connection();
            }
            controlTextBox = new List<Control>();
            foreach (Control control in this.tableLayoutPanel.Controls)
            {
                if (control is TextBox)
                {
                    controlTextBox.Add(control);
                }
                else if (control is Panel)
                {
                    foreach (Control controlPanel in control.Controls)
                    {
                        if (controlPanel is RichTextBox)
                        {
                            controlTextBox.Add(controlPanel);
                        }
                    }
                }
                else if (control is ComboBox)
                {
                    controlTextBox.Add(control);
                }
            }
            datatable = new DataTable();
            newTransaction();
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception)
            {
                //MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }

        public void save(){

            String tempkurs = KurstextBox.Text.ToUpper();
            String temprate = RatetextBox.DecimalValue.ToString();
            try
            {
                if (activity.Equals("UPDATE") || activity.Equals("INSERT"))
                {
                    if (isValidation())
                    {

                        List<SqlParameter> sqlParam = new List<SqlParameter>();
                        if (activity.Equals("UPDATE")) sqlParam.Add(new SqlParameter("@PURCHASEORDERID", PONotextBox.Text));
                        sqlParam.Add(new SqlParameter("@SUPPLIERID", SupplierIDtextBox.Text));
                        sqlParam.Add(new SqlParameter("@CONTACTPERSONSUPPLIER", ContactPersontextBox.Text));
                        sqlParam.Add(new SqlParameter("@KURSID", KurstextBox.Text.ToUpper()));
                        sqlParam.Add(new SqlParameter("@RATE", RatetextBox.DecimalValue.ToString()));
                        sqlParam.Add(new SqlParameter("@STATUS", StatustextBox.Text));
                        sqlParam.Add(new SqlParameter("@REMARK", RemarkrichTextBox.Text));
                        sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));

                        String proc = activity.Equals("UPDATE") ? "UPDATE_DATA_H_PURCHASE_ORDER" : "INSERT_DATA_H_PURCHASE_ORDER";

                        DataTable dt = connection.callProcedureDatatable(proc, sqlParam);

                        if (dt.Rows.Count == 1)
                        {
                            List<string> values = new List<string>();
                            foreach (DataRow row in this.datatable.Rows)
                            {
                                String unitpricestr = row["UNITPRICE"].ToString().Contains(",") ? row["UNITPRICE"].ToString().Replace(",", "") : row["UNITPRICE"].ToString();
                                String value = String.Format("SELECT '{0}', '{1}','{2}', {3}, {4}", dt.Rows[0]["PURCHASEORDERID"].ToString(), row["ITEMID"].ToString(),row["ITEMID_ALIAS"].ToString(),
                                                                                                    row["QUANTITY"].ToString(), unitpricestr);
                                values.Add(value);
                            }
                            String[] columns = { "PURCHASEORDERID", "ITEMID", "ITEMIDQTY", "QUANTITY", "PRICE" };
                            String query = String.Format("DELETE FROM D_PURCHASE_ORDER WHERE PURCHASEORDERID = '{0}'", dt.Rows[0]["PURCHASEORDERID"].ToString());
                            if (connection.openReaderQuery(query))
                            {
                                if (connection.openReaderQuery(Utilities.queryMultipleInsert("D_PURCHASE_ORDER", columns, values)))
                                {
                                    MessageBox.Show("PURCHASE ORDER NO " + dt.Rows[0]["PURCHASEORDERID"].ToString());
                                    //MessageBox.Show("SUCCESS INSERT");
                                    newTransaction();
                                    reloadAllData();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                
            }

            KurstextBox.Text = tempkurs;
            RatetextBox.Text = temprate;
        }

        private Boolean isValidation()
        {
            if (Utilities.isEmptyString(SupplierIDtextBox.Text)){
                MessageBox.Show("Please Input Supplier No");
            }
            else if (activity.Equals("UPDATE") && !isAvailablePurchaseOrderNoForUpdate()){
                MessageBox.Show("Part No Item has not been Available for Update");
            }
            else if (Utilities.isEmptyString(ContactPersontextBox.Text)){
                MessageBox.Show("Please Input Contact Person");
            }
            else if (Utilities.isEmptyString(KurstextBox.Text)){
                MessageBox.Show("Please Input Currency");
            }
            else if (!isAvailableKurs())
            {
                MessageBox.Show("Please Input Correctly Currency");
            }
            else if (Utilities.isEmptyString(RatetextBox.Text)){
                MessageBox.Show("Please Input Rate");
            }
            else if (MasterItemdataGridView.Rows.Count == 0){
                MessageBox.Show("Minimal One Part Item  For Order");
            }
            else{
                return true;
            }
            return false;
        }

        private Boolean isAvailableKurs()
        {
            KurstextBox.Text = Utilities.removeSpace(KurstextBox.Text.ToUpper());
            Boolean result = Utilities.isAvailableID(KursID, KurstextBox.Text);
            return result;
        }

        private Boolean isAvailablePurchaseOrderNoForUpdate()
        {
            PONotextBox.Text = Utilities.removeSpace(PONotextBox.Text);
            Boolean result = Utilities.isAvailableID(PurchaseOrderNo, PONotextBox.Text);
            return result;
        }

        public void delete(){
            if (isFind && isAvailablePurchaseOrderNoForUpdate())
            {
                List<SqlParameter> sqlParam = new List<SqlParameter>();
                sqlParam.Add(new SqlParameter("@PURCHASEORDERID", PONotextBox.Text));
                sqlParam.Add(new SqlParameter("@STATUS", Status.DELETE));
                sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));

                if (connection.callProcedure("DELETE_DATA_H_PURCHASE_ORDER", sqlParam))
                {
                    newTransaction();
                    reloadAllData();
                }
            }
        }

        public void newTransaction()
        {
            Utilities.clearAllField(ref controlTextBox);
            datatable.Clear();
            ContactPersontextBox.Enabled = false;
            searchContactPersonbutton.Visible = false;
            AddItembutton.Enabled = false;
            copyPObutton.Visible = false;
            activity = "INSERT";
            isFind = false;
            SearchPONObutton.Visible = false;
            foreach (Control item in controlTextBox)
            {
                if (item is RichTextBox)
                {
                    RichTextBox richtextBox = (RichTextBox)item;
                    richtextBox.ReadOnly = false;
                    richtextBox.BackColor = SystemColors.Window;
                }
                else
                {
                    item.Enabled = true;
                }
                item.BackColor = SystemColors.Window;
            }
            SupplierNametextBox.Enabled = false;
            SupplierNametextBox.BackColor = SystemColors.Window;
            TotaltextBox.Enabled = false;
            PONotextBox.Text = DateTime.Now.ToString("yyyy") + "XXXXXX";
            PONotextBox.Enabled = false;
            PODatetextBox.Text = DateTime.Now.ToString("dd/MM/yyyy");
            PODatetextBox.Enabled = false;
            StatustextBox.Text = Status.OPEN;
            StatustextBox.Enabled = false;
            searchKursbutton.Visible = true;
            searchSupplierNobutton.Visible = true;
            KurstextBox.Text = "IDR";
            RatetextBox.Text = "1";
            MasterItemdataGridView.AllowUserToDeleteRows = true;
            AllowUserToEditRows(true);
            foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
            {
                if (column.Name.Equals("REMOVE"))
                {
                    column.Visible = true;
                    break;
                }
            }
        }

        public void find(){
            for (int i = 0; i < 2; i++)
            {
                if (!SearchPONObutton.Visible || isFind)
                {
                    copyPObutton.Visible = false;
                    foreach (Control item in controlTextBox)
                    {
                        if (!item.Name.Equals("PONotextBox"))
                        {
                            if (item is RichTextBox)
                            {
                                RichTextBox richtextBox = (RichTextBox)item;
                                richtextBox.ReadOnly = true;
                                richtextBox.BackColor = SystemColors.Info;
                                richtextBox.Text = "";
                            }
                            else
                            {
                                item.Enabled = false;
                                item.Text = "";
                            }
                        }
                        else
                        {
                            TextBox currentTextbox = (TextBox)item;
                            currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                            currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                            currentTextbox.AutoCompleteCustomSource.AddRange(PurchaseOrderNo.ToArray());
                            currentTextbox.Text = "";
                            currentTextbox.Enabled = true;
                        }

                        item.BackColor = SystemColors.Info;
                    }

                    SearchPONObutton.Visible = true;
                    AddItembutton.Enabled = false;

                    searchKursbutton.Visible = false;
                    searchSupplierNobutton.Visible = false;
                    isFind = true;
                    datatable.Clear();
                    MasterItemdataGridView.AllowUserToDeleteRows = false;
                    AllowUserToEditRows(false);
                    addNewItem = false;
                    foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
                    {
                        if (column.Name.Equals("REMOVE"))
                        {
                            column.Visible = false;
                            break;
                        }
                    }
                    activity = "";
                }
            }
        }

        public void edit(){
            for (int i = 0; i < 2; i++)
            {
                if ((!SearchPONObutton.Visible || isFind))
                {
                    foreach (Control item in controlTextBox)
                    {
                        if (!item.Name.Equals("PONotextBox"))
                        {
                            if (item is RichTextBox)
                            {
                                RichTextBox richtextBox = (RichTextBox)item;
                                richtextBox.ReadOnly = true;
                                richtextBox.BackColor = SystemColors.Info;
                            }
                            else
                            {
                                item.Enabled = false;
                            }
                        }
                        else
                        {
                            TextBox currentTextbox = (TextBox)item;
                            currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                            currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                            currentTextbox.AutoCompleteCustomSource.AddRange(PurchaseOrderNo.ToArray());
                            currentTextbox.Enabled = true;

                        }

                        item.BackColor = SystemColors.Info;
                        item.Text = "";
                    }

                    SearchPONObutton.Visible = true;
                    this.datatable.Clear();
                    searchSupplierNobutton.Visible = false;
                    isFind = true;
                    MasterItemdataGridView.AllowUserToDeleteRows = true;
                    AllowUserToEditRows(true);
                    addNewItem = false;

                    foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
                    {
                        if (column.Name.Equals("REMOVE"))
                        {
                            column.Visible = true;
                            break;
                        }
                    }

                    KurstextBox.Enabled = true;
                    RatetextBox.Enabled = true;
                    searchKursbutton.Visible = true;
                    KurstextBox.BackColor = SystemColors.Window;
                    RatetextBox.BackColor = SystemColors.Window;

                    RemarkrichTextBox.ReadOnly = false;
                    RemarkrichTextBox.BackColor = SystemColors.Window;
                    copyPObutton.Visible = false;

                    activity = "UPDATE";
                }
            }
        }

        public void print() {
            if (isFind/* && activity.Equals("")*/)
            {
                if (!PONotextBox.Text.Equals("") && !PONotextBox.Text.Contains('X'))
                {
                    DialogResult r = MessageBox.Show("Apakah Anda mau export purchase order dengan menggunakan part alias ?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (r == DialogResult.Yes)
                    {
                        PurchaseOrderReportForm purchaseorderreportform = new PurchaseOrderReportForm(PONotextBox.Text, true);
                        purchaseorderreportform.ShowDialog();
                    }
                    else if (r == DialogResult.No)
                    {
                        PurchaseOrderReportForm purchaseorderreportform = new PurchaseOrderReportForm(PONotextBox.Text, false);
                        purchaseorderreportform.ShowDialog();
                    }
                }
                else {
                    MessageBox.Show("Silakan Masukan Purchase Order No Yang ingin di cetak");
                }
            }
            else {
                MessageBox.Show("Silakan Find Purhcase Order Terlebih Dahulu");
            }
        }

        private void AllowUserToEditRows(Boolean allow)
        {
            foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
            {
                column.ReadOnly = (column.Name.Equals("QUANTITY") || column.Name.Equals("UNITPRICE")) ? !allow : true;
            }
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e){

        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e){
            try
            {
                if (supplierID == null) supplierID = new List<string>();
                //if (contactPerson == null) contactPerson = new List<string>();
                if (KursID == null) KursID = new List<string>();
                if (PurchaseOrderNo == null) PurchaseOrderNo = new List<string>();

                SqlDataReader sqldataReader = connection.sqlDataReaders(String.Format("SELECT SUPPLIERID FROM M_SUPPLIER WHERE [STATUS] = '{0}'", Status.ACTIVE));

                while (sqldataReader.Read()) supplierID.Add(sqldataReader.GetString(0));

                sqldataReader = connection.sqlDataReaders(String.Format("SELECT KURSID FROM M_KURS WHERE [STATUS] = '{0}'", Status.ACTIVE));

                while (sqldataReader.Read()) KursID.Add(sqldataReader.GetString(0));

                sqldataReader = connection.sqlDataReaders(String.Format("SELECT PURCHASEORDERID FROM H_PURCHASE_ORDER WHERE [STATUS] = '{0}'", Status.OPEN));

                while (sqldataReader.Read()) PurchaseOrderNo.Add(sqldataReader.GetString(0));

                if (IsHandleCreated) BeginInvoke(new fillAutoCompleteTextBox(this.fillAutoComplete));
            }
            catch (Exception ex)
            {
                Console.WriteLine("bgWorker_DoWork" + ex.Message);
            }
        }

        private void fillAutoComplete(){
            String tempSupplierID = SupplierIDtextBox.Text;
            SupplierIDtextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            SupplierIDtextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            SupplierIDtextBox.AutoCompleteCustomSource.AddRange(supplierID.ToArray());
            SupplierIDtextBox.Text = tempSupplierID;

            KurstextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            KurstextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            KurstextBox.AutoCompleteCustomSource.AddRange(KursID.ToArray());
            //KurstextBox.Text = "IDR";
            //RatetextBox.Text = "1";
            //MessageBox.Show("pass autofile");
        }

        private void SupplierIDtextBox_KeyDown(object sender, KeyEventArgs e)
        {
           
            if (e.KeyCode == Keys.Enter)
            {
                SupplierIDtextBox.Text = SupplierIDtextBox.Text.ToUpper();
                Boolean isExistingPartNo = false;
                foreach (String item in supplierID)
                {
                    if (item.ToUpper().Equals(SupplierIDtextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;

                        String query = String.Format("SELECT TOP 1 A.SUPPLIERID, SUPPLIERNAME, CONTACTPERSON " +
                                                     "FROM M_SUPPLIER A  " +
                                                     "LEFT JOIN D_PERSON_SUPPLIER B " +
                                                     "ON A.SUPPLIERID = B.SUPPLIERID " +
                                                     "WHERE A.SUPPLIERID = '{0}' " + 
                                                     "ORDER BY CONTACTPERSON", SupplierIDtextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        MasterSupplierPassingData(datatable);

                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    SupplierIDtextBox.Text = "";
                    SupplierNametextBox.Text = "";
                    ContactPersontextBox.Text = "";
                    ContactPersontextBox.Enabled = false;
                    searchContactPersonbutton.Visible = false;
                    AddItembutton.Enabled = false;
                    PONotextBox.Text = DateTime.Now.ToString("yyyy") + "XXXXXX";
                }
            }
       }

        private void SupplierIDtextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                SupplierIDtextBox.Text = SupplierIDtextBox.Text.ToUpper();
                Boolean isExistingPartNo = false;
                foreach (String item in supplierID)
                {
                    if (item.ToUpper().Equals(SupplierIDtextBox.Text.ToUpper()))
                    {
                        String query = String.Format("SELECT TOP 1 A.SUPPLIERID, SUPPLIERNAME, CONTACTPERSON " +
                                                     "FROM M_SUPPLIER A  " +
                                                     "LEFT JOIN D_PERSON_SUPPLIER B " +
                                                     "ON A.SUPPLIERID = B.SUPPLIERID " +
                                                     "WHERE A.SUPPLIERID = '{0}' " +
                                                     "ORDER BY CONTACTPERSON", SupplierIDtextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        MasterSupplierPassingData(datatable);

                        isExistingPartNo = true;
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    SupplierIDtextBox.Text = "";
                    SupplierNametextBox.Text = "";
                    ContactPersontextBox.Text = "";
                    ContactPersontextBox.Enabled = false;
                    searchContactPersonbutton.Visible = false;
                    AddItembutton.Enabled = false;
                    PONotextBox.Text = DateTime.Now.ToString("yyyy") + "XXXXXX";
                }
            }
            catch (Exception)
            {
                
                throw;
            }
          
        }

        private void ContactPersontextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in contactPerson)
                {
                    if (item.ToUpper().Equals(ContactPersontextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        ContactPersontextBox.Text = item;
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    ContactPersontextBox.Text = "";
                }
            }
        }

        private void ContactPersontextBox_Leave(object sender, EventArgs e)
        {
            if (contactPerson != null)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in contactPerson)
                {
                    if (item.ToUpper().Equals(ContactPersontextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    ContactPersontextBox.Text = "";
                }
            }
        }

        private void KurstextBox_Leave(object sender, EventArgs e)
        {
            Boolean isExistingPartNo = false;
            foreach (String item in KursID)
            {
                if (item.ToUpper().Equals(KurstextBox.Text.ToUpper()))
                {
                    isExistingPartNo = true;
                    KurstextBox.Text = KurstextBox.Text.ToUpper();
                    String query = String.Format("SELECT RATE " +
                                                 "FROM M_KURS " +
                                                 "WHERE KURSID = '{0}'", KurstextBox.Text);

                    DataTable dt = connection.openDataTableQuery(query);
                    RatetextBox.Text = dt.Rows[0]["RATE"].ToString();
                    break;
                }
            }

            if (!isExistingPartNo)
            {
                KurstextBox.Text = "IDR";
                RatetextBox.Text = "1";
                //MessageBox.Show("pass leave");
            }
        }

        private void KurstextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in KursID)
                {
                    if (item.ToUpper().Equals(KurstextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        KurstextBox.Text = KurstextBox.Text.ToUpper();
                        String query = String.Format("SELECT RATE " +
                                                     "FROM M_KURS " +
                                                     "WHERE KURSID = '{0}'", KurstextBox.Text);

                        DataTable dt = connection.openDataTableQuery(query);
                        RatetextBox.Text = dt.Rows[0]["RATE"].ToString();
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    KurstextBox.Text = "IDR";
                    RatetextBox.Text = "1";
                    //MessageBox.Show("pass message down");
                }
            }
        }

        private void searchSupplierNobutton_Click(object sender, EventArgs e)
        {
            if (supplierdatagridview == null)
            {
                supplierdatagridview = new Supplierdatagridview("MasterSupplier");
                supplierdatagridview.masterItemPassingData = new Supplierdatagridview.MasterItemPassingData(MasterSupplierPassingData);
                //supplierdatagridview.MdiParent = this.MdiParent;
                supplierdatagridview.ShowDialog();
            }
            else if (supplierdatagridview.IsDisposed)
            {
                supplierdatagridview = new Supplierdatagridview("MasterSupplier");
                supplierdatagridview.masterItemPassingData = new Supplierdatagridview.MasterItemPassingData(MasterSupplierPassingData);
                //supplierdatagridview.MdiParent = this.MdiParent;
                supplierdatagridview.ShowDialog();
            }
        }

        private void MasterSupplierPassingData(DataTable sender){
            SupplierIDtextBox.Text = sender.Rows[0]["SUPPLIERID"].ToString();
            SupplierNametextBox.Text = sender.Rows[0]["SUPPLIERNAME"].ToString();

            PONotextBox.Text = DateTime.Now.ToString("yyyy") + "XXXXXX";

            String query = String.Format("SELECT CONTACTPERSON " +
                                         "FROM D_PERSON_SUPPLIER " +
                                         "WHERE SUPPLIERID = '{0}'", SupplierIDtextBox.Text);

            if (contactPerson == null) contactPerson = new List<string>();
            
            contactPerson.Clear();

            SqlDataReader sqldataReader = connection.sqlDataReaders(query);

            while (sqldataReader.Read()) contactPerson.Add(sqldataReader.GetString(0));
            
            ContactPersontextBox.AutoCompleteCustomSource.Clear();
            
            ContactPersontextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            ContactPersontextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            ContactPersontextBox.AutoCompleteCustomSource.AddRange(contactPerson.ToArray());
            ContactPersontextBox.Text = sender.Rows[0]["CONTACTPERSON"].ToString(); ;
            ContactPersontextBox.Enabled = true;
            //searchContactPersonbutton.Visible = true;
            AddItembutton.Enabled = true;
        }

        private void searchKursbutton_Click(object sender, EventArgs e)
        {
            if (kursdatagridview == null)
            {
                kursdatagridview = new Kursdatagridview("MasterKurs");
                kursdatagridview.masterKursPassingData = new Kursdatagridview.MasterKursPassingData(MasterKursPassingData);
                //kursdatagridview.MdiParent = this.MdiParent;
                kursdatagridview.ShowDialog();
            }
            else if (kursdatagridview.IsDisposed)
            {
                kursdatagridview = new Kursdatagridview("MasterKurs");
                kursdatagridview.masterKursPassingData = new Kursdatagridview.MasterKursPassingData(MasterKursPassingData);
                //kursdatagridview.MdiParent = this.MdiParent;
                kursdatagridview.ShowDialog();
            }
        }

        private void MasterKursPassingData(DataTable sender){
            sender.Rows[0]["KURSID"].ToString();
            KurstextBox.Text = sender.Rows[0]["KURSID"].ToString();
            RatetextBox.Text = sender.Rows[0]["RATE"].ToString();
        }

        private void AddItembutton_Click(object sender, EventArgs e)
        {
            addNewItem = true;
            if (itemdatagridview == null)
            {
                itemdatagridview = new Itemdatagridview("PURCHASEORDER", SupplierIDtextBox.Text, datatable);
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
            else if (itemdatagridview.IsDisposed)
            {
                itemdatagridview = new Itemdatagridview("PURCHASEORDER", SupplierIDtextBox.Text, datatable);
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
        }

        private void MasterItemPassingData(DataTable datatable){
            /*remove column not use*/
            List<string> columnsRemove = new List<string>();
            foreach (DataColumn column in datatable.Columns)
            {
                Boolean isExistingColumn = false;
                foreach (DataGridViewColumn columngrid in MasterItemdataGridView.Columns)
                {
                    if (column.ColumnName.Equals(columngrid.DataPropertyName))
                    {
                        isExistingColumn = true;
                        break;
                    }
                }
                if (!isExistingColumn)
                {
                    columnsRemove.Add(column.ColumnName);
                }
            }
            foreach (string columnName in columnsRemove)
            {
                datatable.Columns.Remove(columnName);
            }
            //datatable.Columns["ITEMID_ALIAS"].SetOrdinal(1);

            foreach (DataRow row in datatable.Rows)
            {
                Object obj = "1";
                row["QUANTITY"] = obj;
            }


            if (this.datatable.Rows.Count == 0) this.datatable = datatable;
            else {
                foreach (DataRow row in datatable.Rows)
                {
                    this.datatable.Rows.Add(row.ItemArray);
                }
            }
            
            foreach (DataRow row in this.datatable.Rows)
            {
                Object obj = "0";
                row["UNITPRICE"] = (row["UNITPRICE"].ToString().Equals("")) ? obj.ToString() : row["UNITPRICE"].ToString();
                Int64 quantity = Int64.Parse(row["QUANTITY"].ToString());
                Decimal unitprice = Decimal.Parse(row["UNITPRICE"].ToString());
                Decimal amount = (quantity * unitprice);
                row["AMOUNT"] = (Object)amount.ToString();
            }
            
            Utilities.generateNoDataTable(ref this.datatable);

            MasterItemdataGridView.DataSource = this.datatable;

            Decimal total = 0;
            foreach (DataGridViewRow row in MasterItemdataGridView.Rows)
            {
                total += Decimal.Parse(row.Cells["AMOUNT"].Value.ToString());
            }
            TotaltextBox.Text = total.ToString();
        }

        private void MasterItemPurhcaseOrderPassingData(DataTable datatable)
        {
            /*remove column not use*/
            List<string> columnsRemove = new List<string>();
            foreach (DataColumn column in datatable.Columns)
            {
                Boolean isExistingColumn = false;
                foreach (DataGridViewColumn columngrid in MasterItemdataGridView.Columns)
                {
                    if (column.ColumnName.Equals(columngrid.DataPropertyName))
                    {
                        isExistingColumn = true;
                        break;
                    }
                }
                if (!isExistingColumn)
                {
                    columnsRemove.Add(column.ColumnName);
                }
            }
            foreach (string columnName in columnsRemove)
            {
                datatable.Columns.Remove(columnName);
            }
            //datatable.Columns["ITEMID_ALIAS"].SetOrdinal(1);

            if (this.datatable.Rows.Count == 0) this.datatable = datatable;
            else
            {
                foreach (DataRow row in datatable.Rows)
                {
                    this.datatable.Rows.Add(row.ItemArray);
                }
            }

            foreach (DataRow row in this.datatable.Rows)
            {
                Object obj = "0";
                row["UNITPRICE"] = (row["UNITPRICE"].ToString().Equals("")) ? obj : row["UNITPRICE"];
                Int64 quantity = Int64.Parse(row["QUANTITY"].ToString());
                Double unitprice = Double.Parse(row["UNITPRICE"].ToString());
                Double amount = (quantity * unitprice);
                row["AMOUNT"] = (Object)amount.ToString();
            }

            Utilities.generateNoDataTable(ref this.datatable);

            MasterItemdataGridView.DataSource = this.datatable;

            Double total = 0.0;
            foreach (DataGridViewRow row in MasterItemdataGridView.Rows)
            {
                total += Double.Parse(row.Cells["AMOUNT"].Value.ToString());
            }
            TotaltextBox.Text = total.ToString();
        }
        
        private void MasterItemdataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                if (MasterItemdataGridView.Columns[e.ColumnIndex].Name.Equals("REMOVE"))
                {
                    this.datatable.Rows.RemoveAt(e.RowIndex);
                    Utilities.generateNoDataTable(ref datatable);
                    MasterItemdataGridView.DataSource = datatable;
                }
                else if (!activity.Equals("") && MasterItemdataGridView.Columns[e.ColumnIndex].Name.Equals("ITEMID_ALIAS"))
                {
                    ASSIGNITEMROW = e.RowIndex;
                    String ITEMID = MasterItemdataGridView.Rows[e.RowIndex].Cells["ITEMID"].Value.ToString();
                    if (itemdatagridview == null)
                    {
                        itemdatagridview = new Itemdatagridview("ASSIGNITEMPURCHASERECEIPT", ITEMID);
                        itemdatagridview.assignItemQuantityPassingData = new Itemdatagridview.AssignItemQuantityPassingData(AssignItemQuantityPassingData);
                        itemdatagridview.ShowDialog();
                    }
                    else if (itemdatagridview.IsDisposed)
                    {
                        itemdatagridview = new Itemdatagridview("ASSIGNITEMPURCHASERECEIPT", ITEMID);
                        itemdatagridview.assignItemQuantityPassingData = new Itemdatagridview.AssignItemQuantityPassingData(AssignItemQuantityPassingData);
                        itemdatagridview.ShowDialog();
                    }
                }
            }
            //MessageBox.Show(sender.ToString() + " and " + e.ToString());
        }

        private void AssignItemQuantityPassingData(DataTable sender)
        {
            MasterItemdataGridView.Rows[ASSIGNITEMROW].Cells["ITEMID_ALIAS"].Value = sender.Rows[0]["ITEMID"].ToString();
            MasterItemdataGridView.Rows[ASSIGNITEMROW].Cells["ITEMNAME_ALIAS"].Value = sender.Rows[0]["ITEMNAME"].ToString();
        }

        private void MasterItemdataGridView_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            for (int i = 0; i < datatable.Rows.Count; i++)
            {
                if (datatable.Rows[i].RowState == DataRowState.Deleted)
                {
                    datatable.Rows.RemoveAt(i);
                }
            }
            Utilities.generateNoDataTable(ref datatable);
        }
        private void MasterItemdataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
        }

        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            int currColumn = MasterItemdataGridView.CurrentCell.ColumnIndex;
            int currRow = MasterItemdataGridView.CurrentCell.RowIndex;
            if (MasterItemdataGridView.Columns[currColumn].Name.Equals("QUANTITY"))
            {
                e.Handled = Utilities.onlyNumberTextBox(sender, (KeyPressEventArgs)e);
            }
            else if (MasterItemdataGridView.Columns[currColumn].Name.Equals("UNITPRICE"))
            {
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
                {
                    e.Handled = true;
                }
            }
        }

        private void MasterItemdataGridView_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (MasterItemdataGridView.Rows.Count > 0)
            {
                Int64 quantity = (MasterItemdataGridView.Rows[e.RowIndex].Cells["QUANTITY"].Value == null || MasterItemdataGridView.Rows[e.RowIndex].Cells["QUANTITY"].Value.ToString().Equals("")) ? 1 : Int64.Parse(MasterItemdataGridView.Rows[e.RowIndex].Cells["QUANTITY"].Value.ToString());
                quantity = quantity.ToString().Equals("0") ? 1 : quantity;
                MasterItemdataGridView.Rows[e.RowIndex].Cells["QUANTITY"].Value = quantity.ToString();
                String unitpricestr = (MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value == null || MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value.ToString().Equals("")) ? "0" : MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value.ToString();
                Double unitprice = (MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value == null || MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value.ToString().Equals("")) ? 0 : Double.Parse(MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value.ToString());
                //if (activity.Equals("UPDATE"))
                //{
                //    MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value = unitprice.ToString();
                //}
                //else if (activity.Equals("INSERT"))
                //{
                //    //MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value = unitprice.ToString("N2");
                //}
                Double amount = (quantity * unitprice);
                MasterItemdataGridView.Rows[e.RowIndex].Cells["AMOUNT"].Value = amount;

                Double total = 0.0;
                foreach (DataGridViewRow row in MasterItemdataGridView.Rows)
                {
                    total += Double.Parse(row.Cells["AMOUNT"].Value.ToString());
                }
                TotaltextBox.Text = total.ToString();
            }
        }

        private void PONotextBox_Leave(object sender, EventArgs e)
        {
            if (isFind)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in PurchaseOrderNo)
                {
                    if (item.ToUpper().Equals(PONotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        PONotextBox.Text = PONotextBox.Text.ToUpper();
                        String query = String.Format("SELECT PURCHASEORDERID, CONVERT(VARCHAR(10), PURCHASEORDERDATE, 103) AS [PURCHASEORDERDATE], A.SUPPLIERID,SUPPLIERNAME, " +
                                                     "CONTACTPERSONSUPPLIER, KURSID, RATE, REMARK, A.STATUS, A.CREATEDBY " +
                                                     "FROM H_PURCHASE_ORDER A " +
                                                     "JOIN M_SUPPLIER B " +
                                                     "ON A.SUPPLIERID = B.SUPPLIERID " +
                                                     "WHERE PURCHASEORDERID = '{0}'", PONotextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        PurchaseOrderPassingData(datatable);
//                        setDecimalN2();
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    PONotextBox.Text = "";
                    Utilities.clearAllField(ref controlTextBox);
                    this.datatable.Clear();
                    AddItembutton.Enabled = false;
                }
            }
        }

        private void PONotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                this.datatable.Clear();
                foreach (String item in PurchaseOrderNo)
                {
                    if (item.ToUpper().Equals(PONotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        PONotextBox.Text = PONotextBox.Text.ToUpper();
                        String query = String.Format("SELECT PURCHASEORDERID, CONVERT(VARCHAR(10), PURCHASEORDERDATE, 103) AS [PURCHASEORDERDATE], A.SUPPLIERID,SUPPLIERNAME, " +
                                                     "CONTACTPERSONSUPPLIER, KURSID, RATE, REMARK, A.STATUS, A.CREATEDBY " +
                                                     "FROM H_PURCHASE_ORDER A " +
                                                     "JOIN M_SUPPLIER B " +
                                                     "ON A.SUPPLIERID = B.SUPPLIERID " +
                                                     "WHERE PURCHASEORDERID = '{0}'",PONotextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        PurchaseOrderPassingData(datatable);
                        
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    PONotextBox.Text = "";
                    Utilities.clearAllField(ref controlTextBox);
                    AddItembutton.Enabled = false;   
                }
            }
        }

        private void PurchaseOrderPassingData(DataTable sender){
           
            PONotextBox.Text = sender.Rows[0]["PURCHASEORDERID"].ToString();
            PODatetextBox.Text = sender.Rows[0]["PURCHASEORDERDATE"].ToString();
            SupplierIDtextBox.Text = sender.Rows[0]["SUPPLIERID"].ToString();
            SupplierNametextBox.Text = sender.Rows[0]["SUPPLIERNAME"].ToString();
            ContactPersontextBox.Text = sender.Rows[0]["CONTACTPERSONSUPPLIER"].ToString();
            KurstextBox.Text = sender.Rows[0]["KURSID"].ToString();
            RatetextBox.Text = sender.Rows[0]["RATE"].ToString();
            StatustextBox.Text = sender.Rows[0]["STATUS"].ToString();
            RemarkrichTextBox.Text = sender.Rows[0]["REMARK"].ToString();
            sender.Rows[0]["CREATEDBY"].ToString();
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO',A.ITEMID, ITEMNAME, A.ITEMIDQTY AS ITEMID_ALIAS, " +
                                         "( " +
                                         "   SELECT ITEMNAME " +
                                         "   FROM M_ITEM " +
                                         "   WHERE ITEMID = A.ITEMIDQTY " +
                                         ") AS ITEMNAME_ALIAS, " +
                                         "A.QUANTITY, UOMID, PRICE AS UNITPRICE, '0' AS AMOUNT " +
                                         "FROM D_PURCHASE_ORDER A " +
                                         "JOIN M_ITEM B " +
                                         "ON A.ITEMID = B.ITEMID " +
                                         "WHERE PURCHASEORDERID = '{0}' ", PONotextBox.Text);

            DataTable datatable = connection.openDataTableQuery(query);
            this.datatable.Clear();
            MasterItemPurhcaseOrderPassingData(datatable);

            if (StatustextBox.Text.Equals(Status.CLOSE))
            {
                
                SearchPONObutton.Visible = true;
                AddItembutton.Enabled = false;

                searchKursbutton.Visible = false;
                searchSupplierNobutton.Visible = false;
                isFind = true;
                MasterItemdataGridView.AllowUserToDeleteRows = false;
                AllowUserToEditRows(false);
                addNewItem = false;
                foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
                {
                    if (column.Name.Equals("REMOVE"))
                    {
                        column.Visible = false;
                        break;
                    }
                }
                activity = "";

                KurstextBox.Enabled = false;
                RatetextBox.Enabled = false;
                searchKursbutton.Visible = false;
                KurstextBox.BackColor = SystemColors.Info;
                RatetextBox.BackColor = SystemColors.Info;

                RemarkrichTextBox.ReadOnly = true;
                RemarkrichTextBox.BackColor = SystemColors.Info;
                copyPObutton.Visible = true;
            }
            else if (StatustextBox.Text.Equals(Status.OPEN))
            {
                Boolean isupdate = activity.Equals("UPDATE");
                SearchPONObutton.Visible = true;
                searchSupplierNobutton.Visible = false;
                AddItembutton.Enabled = isupdate;
                isFind = true;

                MasterItemdataGridView.AllowUserToDeleteRows = isupdate;
                AllowUserToEditRows(isupdate);
                addNewItem = isupdate;

                foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
                {
                    if (column.Name.Equals("REMOVE"))
                    {
                        column.Visible = isupdate;
                        break;
                    }
                }

                KurstextBox.Enabled = isupdate;
                RatetextBox.Enabled = isupdate;
                searchKursbutton.Visible = isupdate;
                KurstextBox.BackColor = SystemColors.Window;
                RatetextBox.BackColor = SystemColors.Window;

                RemarkrichTextBox.ReadOnly = !isupdate;
                RemarkrichTextBox.BackColor = SystemColors.Window;
                copyPObutton.Visible = true;
                
                //activity = "UPDATE";
            }
        }

        private void SearchPONObutton_Click(object sender, EventArgs e)
        {
            if (purchaseorderdatagridview == null)
            {
                purchaseorderdatagridview = new PurchaseOrderdatagridview("PurchaseOrder");
                purchaseorderdatagridview.purchaseOrderPassingData = new PurchaseOrderdatagridview.PurchaseOrderPassingData(PurchaseOrderPassingData);
                //purchaseorderdatagridview.MdiParent = this.MdiParent;
                purchaseorderdatagridview.ShowDialog();
            }
            else if (purchaseorderdatagridview.IsDisposed)
            {
                purchaseorderdatagridview = new PurchaseOrderdatagridview("PurchaseOrder");
                purchaseorderdatagridview.purchaseOrderPassingData = new PurchaseOrderdatagridview.PurchaseOrderPassingData(PurchaseOrderPassingData);
                //purchaseorderdatagridview.MdiParent = this.MdiParent;
                purchaseorderdatagridview.ShowDialog();
            }
//            setDecimalN2();
        }

        private void PurchaseOrder_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            reloadAllData();
            Main.ACTIVEFORM = this;
        }

        private void ImportExcelbutton_Click(object sender, EventArgs e)
        {
            if (activity.Equals(""))
            {
                String queryHeader = String.Format("SELECT PURCHASEORDERID AS [P.O NO], CONVERT(VARCHAR(10), PURCHASEORDERDATE, 103) AS [P.O DATE], A.SUPPLIERID, SUPPLIERNAME, " +
                                                   "CONTACTPERSONSUPPLIER AS [CONTACT PERSON], KURSID, RATE, A.STATUS, REMARK " +
                                                   "FROM H_PURCHASE_ORDER A " +
                                                   "JOIN M_SUPPLIER B " +
                                                   "ON A.SUPPLIERID = B.SUPPLIERID " +
                                                   "WHERE A.PURCHASEORDERID = '{0}' ", PONotextBox.Text);
                DataTable header = connection.openDataTableQuery(queryHeader);

                String queryDetail = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', ITEMID_ALIAS AS ALIAS, " +
                                                   "A.ITEMID AS [PART NO], ITEMNAME AS [PART NAME], " +
                                                   "A.QUANTITY, UOMID AS UOM, PRICE FROM D_PURCHASE_ORDER A " +
                                                   "JOIN M_ITEM B " +
                                                   "ON A.ITEMID = B.ITEMID " +
                                                   "WHERE PURCHASEORDERID = '{0}'", PONotextBox.Text);
                DataTable detail = connection.openDataTableQuery(queryDetail);

                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.InitialDirectory = Convert.ToString(Environment.SpecialFolder.MyDocuments);
                saveFileDialog.Filter = "Excel Files(*.xlsx; *.xls)|*.xlsx; *.xls";
                saveFileDialog.FilterIndex = 1;

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    UtilitiesExcel.ImportHeaderDetailToExcel(header, detail, saveFileDialog.FileName);
                    
                    MessageBox.Show("Data has been export to excel");
                } 
            }
        }

        private void copyPObutton_Click(object sender, EventArgs e)
        {
            if (isFind || activity.Equals("UPDATE")) {
                String tempPONotextBox = PONotextBox.Text;
                List<SqlParameter> sqlParam = new List<SqlParameter>();
                sqlParam.Add(new SqlParameter("@PURCHASEORDERIDCOPY", PONotextBox.Text));
                sqlParam.Add(new SqlParameter("@STATUS", Status.OPEN));
                sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));

                String proc = "COPY_PURCHASE_ORDER";
                DataTable dt = connection.callProcedureDatatable(proc, sqlParam);

                if (dt.Rows.Count == 1)
                {
                    MessageBox.Show(String.Format("Purchase No {0}",dt.Rows[0]["PURCHASEORDERID"].ToString()));
                    PurchaseOrderNo.Add(dt.Rows[0]["PURCHASEORDERID"].ToString());
                    PONotextBox.AutoCompleteCustomSource.Clear();
                    PONotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                    PONotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    PONotextBox.AutoCompleteCustomSource.AddRange(PurchaseOrderNo.ToArray());
                    PONotextBox.Text = tempPONotextBox;
                }
            }
        }

        private void setDecimalN2()
        {
            foreach (DataGridViewRow row in MasterItemdataGridView.Rows)
            {
                decimal nilai = Decimal.Parse(row.Cells["AMOUNT"].Value.ToString());
                row.Cells["AMOUNT"].Value = nilai.ToString("N2");
            }
        }

        private void MasterItemdataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 8 || e.ColumnIndex == 9)
            {
                try
                {
                    Decimal d = Decimal.Parse(e.Value.ToString());
                    e.Value = d.ToString("N2");
                }
                catch (Exception ex)
                {
                    Decimal d = 0;
                    e.Value = d.ToString("N2");
                }
            }
        }

        private void PurchaseOrder_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show("Anda yakin mau keluar?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (result == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void PurchaseOrder_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }
    }
}