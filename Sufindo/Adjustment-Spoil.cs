﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Data.SqlClient;

namespace Sufindo
{
    public partial class Adjustment_Spoil : Form
    {
        MenuStrip menustripAction;
        AdjustSpoildatagridview adjustspoildatagridview;
        private delegate void fillAutoCompleteTextBox();
        private Itemdatagridview itemdatagridview;
        Connection connection;
        List<Control> controlTextBox;
        List<String> partno;
        List<String> adjustment_spoil_no;
        DataTable datatable;
        int number = 0;

        String activity = "";
        bool isFind = false;
        public Adjustment_Spoil()
        {
            InitializeComponent();
        }

        public Adjustment_Spoil(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            adjustspoildatagridview = null;
            this.menustripAction = menustripAction;
            connection = new Connection();
            controlTextBox = new List<Control>();
            foreach (Control control in this.tableLayoutPanel.Controls)
            {
                if (control is TextBox)
                {
                    controlTextBox.Add(control);
                }
                else if (control is Panel)
                {
                    foreach (Control controlPanel in control.Controls)
                    {
                        if (controlPanel is RichTextBox)
                        {
                            controlTextBox.Add(controlPanel);
                        }
                    }
                }
                else if (control is ComboBox)
                {
                    controlTextBox.Add(control);
                }
            }

            //foreach (Control txtBox in controlTextBox)
            //{
            //    if (txtBox is RichTextBox)
            //    {
            //        RichTextBox richtextBox = (RichTextBox)txtBox;
            //        richtextBox.ReadOnly = true;
            //        richtextBox.BackColor = SystemColors.Info;
            //        richtextBox.Text = "";
            //    }
            //    else
            //    {
            //        txtBox.Enabled = false;
            //        txtBox.Text = "";
            //    }
            //    txtBox.BackColor = SystemColors.Info;
            //}
            datatable = new DataTable();
            newas();
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                Thread.Sleep(100);

                if (partno == null) partno = new List<String>();
                else partno.Clear();

                //if (adjustment_spoil_no == null) adjustment_spoil_no = new List<String>();
                //else adjustment_spoil_no.Clear();

                SqlDataReader sqldataReader = connection.sqlDataReaders(String.Format("SELECT ITEMID FROM M_ITEM WHERE [STATUS] = '{0}'", Status.ACTIVE));

                while (sqldataReader.Read()) partno.Add(sqldataReader.GetString(0));
                
                //if (adjustment_spoil_no == null) adjustment_spoil_no = new List<String>();
                //else adjustment_spoil_no.Clear();

                //sqldataReader = TypecomboBox.Text.Equals("Adjustment") ? connection.sqlDataReaders(String.Format("SELECT ADJUSTMENTID FROM H_ADJUSTMENT_ITEM WHERE [STATUS] = '{0}'", Status.READY)) :
                //                                                         connection.sqlDataReaders(String.Format("SELECT SPOILID FROM H_SPOIL_ITEM WHERE [STATUS] = '{0}'", Status.READY));

                //while (sqldataReader.Read()) adjustment_spoil_no.Add(sqldataReader.GetString(0));

                if (IsHandleCreated) BeginInvoke(new fillAutoCompleteTextBox(this.fillAutoComplete));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void fillAutoComplete()
        {
            PartNotextBox.AutoCompleteCustomSource.Clear();
            PartNotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            PartNotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            PartNotextBox.AutoCompleteCustomSource.AddRange(partno.ToArray());
            PartNotextBox.Text = "";
            
        }

        public void newas() {
            try
            {
                isFind = false;
                Utilities.clearAllField(ref controlTextBox);
                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("NotextBox"))
                    {
                        if (item is RichTextBox)
                        {
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = false;
                            richtextBox.BackColor = Color.White;
                            richtextBox.Text = "";
                        }
                        else
                        {
                            item.Enabled = true;
                            item.Text = "";
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.Enabled = false;
                    }

                    item.BackColor = Color.White;
                }
                DatetextBox.Enabled = false;
                StatustextBox.Enabled = false;
                CreateBytextBox.Enabled = false;
                PartnametextBox.Enabled = false;
                qtytextBox.Enabled = false;
                resulttextBox.Enabled = false;

                CreateBytextBox.Text = AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString();
                NotextBox.Text = DateTime.Now.ToString("yyyyMM") + "XXXX";
                NotextBox.Enabled = false;
                DatetextBox.Text = DateTime.Now.ToString("dd/MM/yyyy");
                StatustextBox.Text = Status.READY;
                TypecomboBox.SelectedIndex = 0;
                SearchNobutton.Visible = false;
                SearchPartNObutton.Visible = true;
                AddItembutton.Enabled = true;
                clearPartField();
                datatable.Clear();
                activity = "INSERT";
                dataGridView.AllowUserToDeleteRows = true;
                foreach (DataGridViewColumn column in dataGridView.Columns)
                {
                    if (column.Name.Equals("del"))
                    {
                        column.Visible = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public void save() {
            if (isvalidate())
            {

                if (activity.Equals("INSERT") || activity.Equals("UPDATE"))
                {
                    List<SqlParameter> sqlParam = new List<SqlParameter>();

                    if (TypecomboBox.SelectedIndex == 0)
                    {
                        if (activity.Equals("UPDATE")) sqlParam.Add(new SqlParameter("@ADJUSTMENTID", NotextBox.Text));
                    }
                    else if (activity.Equals("UPDATE")) sqlParam.Add(new SqlParameter("@SPOILID", NotextBox.Text));

                    sqlParam.Add(new SqlParameter("@REMARK", RemarkrichTextBox.Text));
                    sqlParam.Add(new SqlParameter("@STATUS", Status.READY));
                    sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"]));

                    String proc = "";
                    if (TypecomboBox.SelectedIndex == 0)
                    {
                        proc = activity.Equals("UPDATE") ? "UPDATE_DATA_H_ADJUSTMENT_ITEM" : "INSERT_DATA_H_ADJUSTMENT_ITEM";
                    }
                    else
                    {
                        proc = activity.Equals("UPDATE") ? "UPDATE_DATA_H_SPOIL_ITEM" : "INSERT_DATA_H_SPOIL_ITEM";
                    }

                    DataTable dt = connection.callProcedureDatatable(proc, sqlParam);
                    if (dt.Rows.Count == 1)
                    {
                        List<string> values = new List<string>();
                        foreach (DataRow row in this.datatable.Rows)
                        {
                            String value = String.Format("SELECT '{0}', '{1}', {2}, {3}", (TypecomboBox.SelectedIndex == 0) ? dt.Rows[0]["ADJUSTMENTID"].ToString() : dt.Rows[0]["SPOILID"].ToString(),
                                                                                                 row["ITEMID"].ToString(),
                                                                                                 row["QUANTITY"].ToString(),
                                                                                                 row["ADJUSTSPOIL"].ToString());
                            values.Add(value);
                        }

                        if (TypecomboBox.SelectedIndex == 0)
                        {
                            String[] columns = { "ADJUSTMENTID", "ITEMID", "QUANTITY", "ADJUSTMENT" };
                            String query = String.Format("DELETE FROM D_ADJUSTMENT_ITEM WHERE ADJUSTMENTID = '{0}'", dt.Rows[0]["ADJUSTMENTID"].ToString());
                            if (connection.openReaderQuery(query))
                            {
                                if (connection.openReaderQuery(Utilities.queryMultipleInsert("D_ADJUSTMENT_ITEM", columns, values)))
                                {
                                    MessageBox.Show(String.Format("Success Insert Adjustment No {0}", dt.Rows[0]["ADJUSTMENTID"].ToString()));
                                    reloadAllData();
                                    newas();
                                }
                            }
                        }
                        else
                        {
                            String[] columns = { "SPOILID", "ITEMID", "QUANTITY", "SPOIL" };
                            String query = String.Format("DELETE FROM D_SPOIL_ITEM WHERE SPOILID = '{0}'", dt.Rows[0]["SPOILID"].ToString());
                            if (connection.openReaderQuery(query))
                            {
                                if (connection.openReaderQuery(Utilities.queryMultipleInsert("D_SPOIL_ITEM", columns, values)))
                                {
                                    MessageBox.Show(String.Format("Success Insert Spoil No {0}", dt.Rows[0]["SPOILID"].ToString()));
                                    reloadAllData();
                                    newas();
                                }
                            }
                        }
                    }
                }
            }
            else {
                MessageBox.Show("Minimal 1 Item.");
            }
        }

        public Boolean isvalidate() {
            if (dataGridView.Rows.Count >= 1) {
                return true;
            }
            return false;
        }

        public void find() {
            try
            {
                if (!SearchNobutton.Visible || isFind)
                {
                    reloadAllData();
                    Utilities.clearAllField(ref controlTextBox);
                    SearchNobutton.Visible = true;
                    AddItembutton.Enabled = false;
                    isFind = true;
                    TypecomboBox.SelectedIndex = 0;

                    foreach (Control item in controlTextBox)
                    {
                        if (!item.Name.Equals("NotextBox"))
                        {
                            if (item is RichTextBox)
                            {
                                RichTextBox richtextBox = (RichTextBox)item;
                                richtextBox.ReadOnly = true;
                                richtextBox.BackColor = SystemColors.Info;
                                richtextBox.Text = "";
                            }
                            else
                            {
                                item.Enabled = false;
                                item.Text = "";
                            }
                        }
                        else
                        {
                            TextBox currentTextbox = (TextBox)item;
                            currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                            currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                            currentTextbox.AutoCompleteCustomSource.AddRange(adjustment_spoil_no.ToArray());
                            currentTextbox.Text = "";
                            currentTextbox.Enabled = true;
                        }

                        item.BackColor = SystemColors.Info;
                    }

                    datatable.Clear();
                    activity = "";
                    AddItembutton.Enabled = false;
                    clearPartField();
                    TypecomboBox.Enabled = true;
                    SearchPartNObutton.Visible = false;
                    dataGridView.AllowUserToDeleteRows = false;
                    foreach (DataGridViewColumn column in dataGridView.Columns)
                    {
                        if (column.Name.Equals("del"))
                        {
                            column.Visible = false;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public void edit() {
            if (!SearchNobutton.Visible || isFind)
            {
                reloadAllData();
                Utilities.clearAllField(ref controlTextBox);
                SearchNobutton.Visible = true;
                AddItembutton.Enabled = true;
                isFind = true;
                TypecomboBox.SelectedIndex = 0;

                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("NotextBox"))
                    {
                        if (item is RichTextBox)
                        {
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = true;
                            richtextBox.BackColor = SystemColors.Info;
                            richtextBox.Text = "";
                        }
                        else
                        {
                            item.Enabled = false;
                            item.Text = "";
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        currentTextbox.AutoCompleteCustomSource.AddRange(adjustment_spoil_no.ToArray());
                        currentTextbox.Text = "";
                        currentTextbox.Enabled = true;
                    }

                    item.BackColor = SystemColors.Info;
                }
                RemarkrichTextBox.ReadOnly = false;
                totextBox.Enabled = true;
                PartNotextBox.Enabled = true;
                datatable.Clear();
                activity = "UPDATE";
                AddItembutton.Enabled = true;
                clearPartField();
                TypecomboBox.Enabled = true;
                SearchPartNObutton.Visible = true;
                dataGridView.AllowUserToDeleteRows = true;
                foreach (DataGridViewColumn column in dataGridView.Columns)
                {
                    if (column.Name.Equals("del"))
                    {
                        column.Visible = true;
                        break;
                    }
                }
            }
        }

        public void delete() {
            if (isFind && StatustextBox.Text.Equals(Status.READY))
            {
                List<SqlParameter> sqlParam = new List<SqlParameter>();
                String USERLOGIN = "";
                if (TypecomboBox.SelectedIndex == 0)
                {
                    sqlParam.Add(new SqlParameter("@ADJUSTMENTID", NotextBox.Text));
                }
                else
                {
                    sqlParam.Add(new SqlParameter("@SPOILID", NotextBox.Text));
                }
                sqlParam.Add(new SqlParameter("@STATUS", Status.DELETE));
                sqlParam.Add(new SqlParameter("@CREATEDBY", USERLOGIN));

                String proc = (TypecomboBox.SelectedIndex == 0) ? "DELETE_DATA_H_ADJUSTMENT_ITEM" : "DELETE_DATA_H_SPOIL_ITEM";

                if (connection.callProcedure(proc, sqlParam))
                {
                    MessageBox.Show("Success Deleted");
                    reloadAllData();
                }
            }
            else {
                MessageBox.Show("Sudah di konfirmasi, tidak bisa di delete.");
            }
        }

        private void PartNotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in partno)
                {
                    if (item.ToUpper().Equals(PartNotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        PartNotextBox.Text = PartNotextBox.Text.ToUpper();
                        String query = String.Format("SELECT ITEMID, ITEMNAME, QUANTITY " +
                                                     "FROM M_ITEM " +
                                                     "WHERE ITEMID = '{0}' AND [STATUS] = '{1}'", PartNotextBox.Text, Status.ACTIVE);

                        DataTable datatable = connection.openDataTableQuery(query);
                        MasterItemPassingData(datatable);
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    clearPartField();
                    newas();
                }
            }
        }

        private void PartNotextBox_Leave(object sender, EventArgs e)
        {
            PartNotextBox.Text = PartNotextBox.Text.ToUpper();
            Boolean isExistingPartNo = false;
            foreach (String item in partno)
            {
                if (item.ToUpper().Equals(PartNotextBox.Text.ToUpper()))
                {
                    isExistingPartNo = true;
                    PartNotextBox.Text = PartNotextBox.Text.ToUpper();
                    String query = String.Format("SELECT ITEMID, ITEMNAME, QUANTITY " +
                                                 "FROM M_ITEM " +
                                                 "WHERE ITEMID = '{0}' AND [STATUS] = '{1}'", PartNotextBox.Text, Status.ACTIVE);

                    DataTable datatable = connection.openDataTableQuery(query);
                    MasterItemPassingData(datatable);
                    break;
                }
            }
            if (!isExistingPartNo)
            {
                clearPartField();
            }

        }

        private void clearPartField() {
            PartNotextBox.Text = "";
            PartnametextBox.Text = "";
            qtytextBox.Text = "0";
            totextBox.Text = "0";
            resulttextBox.Text = "0";
        }

        private void SearchPartNObutton_Click(object sender, EventArgs e)
        {
            if (itemdatagridview == null)
            {
                itemdatagridview = new Itemdatagridview("AdjustmentSpoil");
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
            else if (itemdatagridview.IsDisposed)
            {
                itemdatagridview = new Itemdatagridview("AdjustmentSpoil");
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
        }

        private void MasterItemPassingData(DataTable sender) {
            PartnametextBox.Text = sender.Rows[0]["ITEMNAME"].ToString();
            PartNotextBox.Text = sender.Rows[0]["ITEMID"].ToString();
            qtytextBox.Text = sender.Rows[0]["QUANTITY"].ToString().Equals("") ? "0" : sender.Rows[0]["QUANTITY"].ToString();
        }
        private void resetColumnDataGridView()
        {
            dataGridView.Columns.Clear();
            DataGridViewTextBoxColumn NO = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn ITEMID = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn ITEMNAME = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn QUANTITY = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn ADJUSTSPOIL = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn LASTQTY = new DataGridViewTextBoxColumn();
            DataGridViewImageColumn DEL = new DataGridViewImageColumn();

            NO.DataPropertyName = "NO";
            NO.HeaderText = "NO";
            NO.Name = "NO";
            NO.ReadOnly = true;
            NO.Width = 40;

            ITEMID.DataPropertyName = "ITEMID";
            ITEMID.HeaderText = "PART NO";
            ITEMID.Name = "ITEMID";
            ITEMID.ReadOnly = true;
            ITEMID.Width = 120;

            ITEMNAME.DataPropertyName = "ITEMNAME";
            ITEMNAME.HeaderText = "PART NAME";
            ITEMNAME.Name = "ITEMNAME";
            ITEMNAME.ReadOnly = true;
            ITEMNAME.Width = 120;

            QUANTITY.DataPropertyName = "QUANTITY";
            QUANTITY.HeaderText = "QTY";
            QUANTITY.Name = "Qty";
            QUANTITY.ReadOnly = true;

            ADJUSTSPOIL.DataPropertyName = "ADJUSTSPOIL";
            ADJUSTSPOIL.HeaderText = TypecomboBox.Text.ToUpper();
            ADJUSTSPOIL.Name = "ADJUSTSPOIL";
            ADJUSTSPOIL.ReadOnly = true;

            LASTQTY.DataPropertyName = "LASTQTY";
            LASTQTY.FillWeight = 120F;
            LASTQTY.HeaderText = "LAST QTY";
            LASTQTY.Name = "LASTQTY";
            LASTQTY.ReadOnly = true;

            DEL.Name = "del";
            DEL.HeaderText = "";
            DEL.FillWeight = 24F;
            DEL.Width = 24;
            DEL.Image = Properties.Resources.minus;
            DEL.Visible = activity.Equals("") ? false : true;

            dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
                NO,
                ITEMID,
                ITEMNAME,
                QUANTITY,
                ADJUSTSPOIL,
                LASTQTY,DEL});
        }

        private void Adjustment_Spoil_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }

        private void Adjustment_Spoil_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            Main.ACTIVEFORM = this;
            reloadAllData();
        }

        private void totextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = Utilities.onlyNumberTextBox(sender, e);
        }

        private void TypecomboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.resetColumnDataGridView();
            Typelabel.Text = String.Format("{0} No", TypecomboBox.Text);
            _typelabel.Text = TypecomboBox.Text;
            clearPartField();
            datatable.Clear();
            //AdjustSpoil.HeaderText =
            dataGridView.Columns[4].HeaderText = TypecomboBox.Text.ToUpper();
            if (isFind)
            {
                if (adjustment_spoil_no == null) adjustment_spoil_no = new List<String>();
                else adjustment_spoil_no.Clear();

                SqlDataReader sqldataReader = TypecomboBox.Text.Equals("Adjustment") ? connection.sqlDataReaders(String.Format("SELECT ADJUSTMENTID FROM H_ADJUSTMENT_ITEM WHERE [STATUS] = '{0}' OR [STATUS] = '{1}'", Status.READY, Status.CONFIRM)) :
                                                                         connection.sqlDataReaders(String.Format("SELECT SPOILID FROM H_SPOIL_ITEM WHERE [STATUS] = '{0}' OR [STATUS] = '{1}'", Status.READY, Status.CONFIRM));
                
                while (sqldataReader.Read()) adjustment_spoil_no.Add(sqldataReader.GetString(0));

                NotextBox.AutoCompleteCustomSource.Clear();
                NotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                NotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                NotextBox.AutoCompleteCustomSource.AddRange(adjustment_spoil_no.ToArray());
                NotextBox.Text = "";
            }
        }

        private void AddItembutton_Click(object sender, EventArgs e)
        {
            int result = Int32.Parse(resulttextBox.Text);
            if (!PartNotextBox.Text.Equals("") && (!qtytextBox.Text.Equals("0") || !totextBox.Text.Equals("0")) && result >= 0)
            {
                bool isExisting = false;
                foreach (DataRow row in this.datatable.Rows)
                {
                    if (row["ITEMID"].ToString().Equals(PartNotextBox.Text))
                    {
                        Int64 lastqty = (TypecomboBox.SelectedIndex == 0) ? Int64.Parse(totextBox.Text) :
                                                                            Int64.Parse(qtytextBox.Text) - Int64.Parse(totextBox.Text);
                        row["QUANTITY"] = qtytextBox.Text;
                        row["ADJUSTSPOIL"] = totextBox.Text;
                        row["LASTQTY"] = lastqty.ToString();
                        isExisting = true;
                        break;
                    }
                }
                if (!isExisting)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("NO", typeof(String));
                    dt.Columns.Add("ITEMID", typeof(String));
                    dt.Columns.Add("ITEMNAME", typeof(String));
                    dt.Columns.Add("QUANTITY", typeof(String));
                    dt.Columns.Add("ADJUSTSPOIL", typeof(String));
                    dt.Columns.Add("LASTQTY", typeof(String));
                    
                    int qty = qtytextBox.Text.Equals("") ? 0 : Int32.Parse(qtytextBox.Text);
                    int to = totextBox.Text.Equals("") ? 0 : Int32.Parse(totextBox.Text);
                    int r = Int32.Parse(resulttextBox.Text);
                    number++;
                    dt.Rows.Add(number.ToString(), PartNotextBox.Text, PartnametextBox.Text, qtytextBox.Text, totextBox.Text, r.ToString());
                    AdjustmentSpoilItemPassingData(dt);
                }
                clearPartField();
            }
        }

        private void AdjustmentSpoilItemPassingData(DataTable sender) {
            if (this.datatable == null) this.datatable = new DataTable();
            else
            {
                if (datatable.Rows.Count == 0) this.datatable.Clear();
            }

            if (this.datatable.Rows.Count == 0) this.datatable = sender;
            else
            {
                foreach (DataRow row in sender.Rows)
                {
                    this.datatable.Rows.Add(row.ItemArray);
                }
            }

            dataGridView.DataSource = datatable;

            Utilities.generateNoDataTable(ref this.datatable);

            number = this.datatable.Rows.Count;
        }

        private void dataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (dataGridView.Columns[e.ColumnIndex].Name.Equals("del"))
                {
                    this.datatable.Rows.RemoveAt(e.RowIndex);
                    Utilities.generateNoDataTable(ref datatable);
                    dataGridView.DataSource = datatable;
                    clearPartField();
                }
                else
                {
                    MasterItemPassingDataField(e.RowIndex);
                }
            }
            else {
                clearPartField();
            }
        }

        private void dataGridView_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            for (int i = 0; i < datatable.Rows.Count; i++)
            {
                if (datatable.Rows[i].RowState == DataRowState.Deleted)
                {
                    datatable.Rows.RemoveAt(i);
                }
            }
            clearPartField();
            Utilities.generateNoDataTable(ref datatable);
        }

        private void MasterItemPassingDataField(int indexRow) {
            PartNotextBox.Text = dataGridView.Rows[indexRow].Cells[2].Value.ToString();
            PartnametextBox.Text = dataGridView.Rows[indexRow].Cells[3].Value.ToString();
            qtytextBox.Text = dataGridView.Rows[indexRow].Cells[4].Value.ToString();
            totextBox.Text = dataGridView.Rows[indexRow].Cells[5].Value.ToString();
        }

        private void totextBox_TextChanged(object sender, EventArgs e)
        {
            int qty = qtytextBox.Text.Equals("") ? 0 : Int32.Parse(qtytextBox.Text);
            int to = totextBox.Text.Equals("") ? 0 : Int32.Parse(totextBox.Text);
            int r = 0;
            
            if (TypecomboBox.SelectedIndex == 0)
            {
                r = to;
            }
            else if(TypecomboBox.SelectedIndex == 1)
            {
                r = qty - to;
            }
            resulttextBox.Text = r.ToString();
        }

        private void qtytextBox_TextChanged(object sender, EventArgs e)
        {
            int qty = qtytextBox.Text.Equals("") ? 0 : Int32.Parse(qtytextBox.Text);
            int to = totextBox.Text.Equals("") ? 0 : Int32.Parse(totextBox.Text);
            int r = qty - to;
            resulttextBox.Text = r.ToString();
        }

        private void SearchNobutton_Click(object sender, EventArgs e)
        {
            if (adjustspoildatagridview == null)
            {
                adjustspoildatagridview = new AdjustSpoildatagridview(TypecomboBox.Text);
                adjustspoildatagridview.Text = TypecomboBox.Text;
                adjustspoildatagridview.adjustmentSpoilPassingData = new AdjustSpoildatagridview.AdjustmentSpoilPassingData(AdjustSpoilPassingData);
                adjustspoildatagridview.ShowDialog();
            }
            else if (adjustspoildatagridview.IsDisposed)
            {
                adjustspoildatagridview = new AdjustSpoildatagridview(TypecomboBox.Text);
                adjustspoildatagridview.Text = TypecomboBox.Text;
                adjustspoildatagridview.adjustmentSpoilPassingData = new AdjustSpoildatagridview.AdjustmentSpoilPassingData(AdjustSpoilPassingData);
                adjustspoildatagridview.ShowDialog();
            }
        }

        private void AdjustSpoilPassingData(DataTable sender) {
            if (sender.Rows.Count == 1)
            {
                NotextBox.Text = TypecomboBox.Text.Equals("Adjustment") ? sender.Rows[0]["ADJUSTMENTID"].ToString() : sender.Rows[0]["SPOILID"].ToString();
                DatetextBox.Text = TypecomboBox.Text.Equals("Adjustment") ? sender.Rows[0]["ADJUSTMENTDATE"].ToString() : sender.Rows[0]["SPOILDATE"].ToString();
                StatustextBox.Text = sender.Rows[0]["STATUS"].ToString();
                RemarkrichTextBox.Text = sender.Rows[0]["REMARK"].ToString();
                CreateBytextBox.Text = sender.Rows[0]["CREATEDBY"].ToString();

                String query = "";
                if (TypecomboBox.Text.Equals("Adjustment"))
                {
                    query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMID, B.ITEMNAME, A.QUANTITY, A.ADJUSTMENT AS ADJUSTSPOIL, A.ADJUSTMENT AS LASTQTY " +
                                          "FROM D_ADJUSTMENT_ITEM A  " +
                                          "JOIN M_ITEM B ON A.ITEMID = B.ITEMID " +
                                          "WHERE ADJUSTMENTID = '{0}'", NotextBox.Text);
                }
                else
                {
                    query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMID, B.ITEMNAME, A.QUANTITY, A.SPOIL AS ADJUSTSPOIL, (A.QUANTITY - A.SPOIL) AS LASTQTY " +
                                          "FROM D_SPOIL_ITEM A " +
                                          "JOIN M_ITEM B ON A.ITEMID = B.ITEMID " +
                                          "WHERE SPOILID = '{0}'", NotextBox.Text);
                }
                datatable.Clear();
                DataTable dt = connection.openDataTableQuery(query);
                AdjustmentSpoilItemPassingData(dt);
                if (StatustextBox.Text.Equals(Status.CONFIRM)) {
                    isFind = true;
                    activity = "";
                    AddItembutton.Enabled = false;
                    clearPartField();
                    TypecomboBox.Enabled = true;
                    SearchPartNObutton.Visible = false;
                    dataGridView.AllowUserToDeleteRows = false;
                    foreach (DataGridViewColumn column in dataGridView.Columns)
                    {
                        if (column.Name.Equals("del"))
                        {
                            column.Visible = false;
                            break;
                        }
                    }
                    PartNotextBox.Enabled = false;
                    totextBox.Enabled = false;
                    RemarkrichTextBox.ReadOnly = true;
                }
                else if (StatustextBox.Text.Equals(Status.READY))
                {
                    Boolean isupdate = activity.Equals("UPDATE");
                    totextBox.Enabled = isupdate;
                    PartNotextBox.Enabled = isupdate;
                    isFind = true;
                    //activity = "UPDATE";
                    AddItembutton.Enabled = isupdate;
                    clearPartField();
                    TypecomboBox.Enabled = true;
                    SearchPartNObutton.Visible = isupdate;
                    dataGridView.AllowUserToDeleteRows = isupdate;
                    foreach (DataGridViewColumn column in dataGridView.Columns)
                    {
                        if (column.Name.Equals("del"))
                        {
                            column.Visible = isupdate;
                            break;
                        }
                    }
                    PartNotextBox.Enabled = isupdate;
                    totextBox.Enabled = isupdate;
                    RemarkrichTextBox.ReadOnly = !isupdate;
                }
            }
        }

        private void NotextBox_Leave(object sender, EventArgs e)
        {
            NotextBox.Text = NotextBox.Text.ToUpper();
            Boolean isExistingPartNo = false;
            foreach (String item in adjustment_spoil_no)
            {
                if (item.ToUpper().Equals(NotextBox.Text.ToUpper()))
                {
                    isExistingPartNo = true;
                    NotextBox.Text = NotextBox.Text.ToUpper();
                    String query = "";
                    if (TypecomboBox.Text.Equals("Adjustment"))
                    {
                        query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY ADJUSTMENTID) AS 'NO', ADJUSTMENTID, " +
                                              "CONVERT(VARCHAR(10), ADJUSTMENTDATE, 103) AS ADJUSTMENTDATE, REMARK, STATUS, CREATEDBY " +
                                              "FROM H_ADJUSTMENT_ITEM " +
                                              "WHERE ADJUSTMENTID = '{0}'", TypecomboBox.Text);
                    }
                    else
                    {
                        query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SPOILID) AS 'NO', SPOILID, " +
                                              "CONVERT(VARCHAR(10), SPOILDATE, 103) AS SPOILDATE, REMARK, STATUS, CREATEDBY " +
                                              "FROM H_SPOIL_ITEM " +
                                              "WHERE SPOILID = '{0}'", TypecomboBox.Text);
                    }

                    DataTable datatable = connection.openDataTableQuery(query);
                    AdjustSpoilPassingData(datatable);
                    break;
                }
            }
            if (!isExistingPartNo)
            {
                NotextBox.Text = "";
                DatetextBox.Text = "";
                StatustextBox.Text = "";
                RemarkrichTextBox.Text = "";
                datatable.Clear();
            }
        }

        private void NotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in adjustment_spoil_no)
                {
                    if (item.ToUpper().Equals(NotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        NotextBox.Text = NotextBox.Text.ToUpper();
                        String query = "";
                        if (TypecomboBox.Text.Equals("Adjustment"))
                        {
                            query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY ADJUSTMENTID) AS 'NO', ADJUSTMENTID, " +
                                                  "CONVERT(VARCHAR(10), ADJUSTMENTDATE, 103) AS ADJUSTMENTDATE, REMARK, STATUS, CREATEDBY " +
                                                  "FROM H_ADJUSTMENT_ITEM " +
                                                  "WHERE ADJUSTMENTID = '{0}'", NotextBox.Text);
                        }
                        else {
                            query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SPOILID) AS 'NO', SPOILID, " +
                                                  "CONVERT(VARCHAR(10), SPOILDATE, 103) AS SPOILDATE, REMARK, STATUS, CREATEDBY " +
                                                  "FROM H_SPOIL_ITEM " +
                                                  "WHERE SPOILID = '{0}'", NotextBox.Text);
                        }

                        DataTable datatable = connection.openDataTableQuery(query);
                        AdjustSpoilPassingData(datatable);
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    NotextBox.Text = "";
                    DatetextBox.Text = "";
                    StatustextBox.Text = "";
                    RemarkrichTextBox.Text = "";
                    datatable.Clear();
                }
            }
        }

        public void confirm() {
            if (isFind && activity.Equals("") && StatustextBox.Text.Equals(Status.READY))
            {
                List<SqlParameter> sqlParam = new List<SqlParameter>();

                if (TypecomboBox.SelectedIndex == 0)
                {
                    sqlParam.Add(new SqlParameter("@ADJUSTMENTID", NotextBox.Text));
                }
                else
                {
                    sqlParam.Add(new SqlParameter("@SPOILID", NotextBox.Text));
                }

                sqlParam.Add(new SqlParameter("@STATUS", Status.CONFIRM));
                sqlParam.Add(new SqlParameter("@CONFIRMBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"]));

                String proc = "";
                if (TypecomboBox.SelectedIndex == 0)
                {
                    proc = "UPDATE_STATUS_H_ADJUSTMENT_ITEM";
                }
                else
                {
                    proc = "UPDATE_STATUS_H_SPOIL_ITEM";
                }

                if (connection.callProcedure(proc, sqlParam))
                {
                    MessageBox.Show("Confirm Success");
                    reloadAllData();
                    newas();
                }
            }
            else {
                if (StatustextBox.Text.Equals(Status.CONFIRM))
                {
                    MessageBox.Show("Sudah dikonfirmasi, Tidak bisa 2 kali.");
                }
                else {
                    MessageBox.Show("Harus Simpan terlebih dahulu, sebelum dikonfirmasi.");
                }
            }
        }
    }
}
