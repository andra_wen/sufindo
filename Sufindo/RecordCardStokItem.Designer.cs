﻿namespace Sufindo
{
    partial class RecordCardStokItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.RaktextBox = new System.Windows.Forms.TextBox();
            this.SatuantextBox = new System.Windows.Forms.TextBox();
            this.searchPartNobutton = new System.Windows.Forms.Button();
            this.searchAliasPartNobutton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.PartNotextBox = new System.Windows.Forms.TextBox();
            this.PartNametextBox = new System.Windows.Forms.TextBox();
            this.BrandtextBox = new System.Windows.Forms.TextBox();
            this.Spesifikasipanel = new System.Windows.Forms.Panel();
            this.SpesifikasirichTextBox = new System.Windows.Forms.RichTextBox();
            this.TodateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.FromdateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureboxpanel = new System.Windows.Forms.Panel();
            this.PartpictureBox = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.paneldatagridview = new System.Windows.Forms.Panel();
            this.TransaksidataGridView = new System.Windows.Forms.DataGridView();
            this.DatatableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxTotal = new System.Windows.Forms.TextBox();
            this.textBoxOut = new System.Windows.Forms.TextBox();
            this.textBoxIn = new System.Windows.Forms.TextBox();
            this.textBoxSaldo = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.SellPricetextBox = new CustomControls.TextBoxSL();
            this.textBoxUpdate = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.OpenOnOrderbutton = new System.Windows.Forms.Button();
            this.TotalOnOrderlabel = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.Spesifikasipanel.SuspendLayout();
            this.pictureboxpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PartpictureBox)).BeginInit();
            this.paneldatagridview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TransaksidataGridView)).BeginInit();
            this.DatatableLayoutPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(706, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 20);
            this.label1.TabIndex = 14;
            this.label1.Text = "Rak";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(706, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 20);
            this.label8.TabIndex = 13;
            this.label8.Text = "Satuan";
            // 
            // RaktextBox
            // 
            this.RaktextBox.BackColor = System.Drawing.Color.White;
            this.RaktextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RaktextBox.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RaktextBox.ForeColor = System.Drawing.Color.Black;
            this.RaktextBox.Location = new System.Drawing.Point(792, 36);
            this.RaktextBox.Name = "RaktextBox";
            this.RaktextBox.ReadOnly = true;
            this.RaktextBox.Size = new System.Drawing.Size(146, 26);
            this.RaktextBox.TabIndex = 16;
            // 
            // SatuantextBox
            // 
            this.SatuantextBox.BackColor = System.Drawing.Color.White;
            this.SatuantextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SatuantextBox.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SatuantextBox.ForeColor = System.Drawing.Color.Black;
            this.SatuantextBox.Location = new System.Drawing.Point(792, 3);
            this.SatuantextBox.Name = "SatuantextBox";
            this.SatuantextBox.ReadOnly = true;
            this.SatuantextBox.Size = new System.Drawing.Size(147, 26);
            this.SatuantextBox.TabIndex = 14;
            // 
            // searchPartNobutton
            // 
            this.searchPartNobutton.BackColor = System.Drawing.Color.Transparent;
            this.searchPartNobutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchPartNobutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchPartNobutton.FlatAppearance.BorderSize = 0;
            this.searchPartNobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchPartNobutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchPartNobutton.Location = new System.Drawing.Point(672, 6);
            this.searchPartNobutton.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.searchPartNobutton.Name = "searchPartNobutton";
            this.searchPartNobutton.Size = new System.Drawing.Size(26, 24);
            this.searchPartNobutton.TabIndex = 2;
            this.searchPartNobutton.Tag = "2";
            this.searchPartNobutton.UseVisualStyleBackColor = false;
            this.searchPartNobutton.Click += new System.EventHandler(this.searchPartNobutton_Click);
            // 
            // searchAliasPartNobutton
            // 
            this.searchAliasPartNobutton.BackColor = System.Drawing.Color.Transparent;
            this.searchAliasPartNobutton.BackgroundImage = global::Sufindo.Properties.Resources.alias;
            this.searchAliasPartNobutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchAliasPartNobutton.Enabled = false;
            this.searchAliasPartNobutton.FlatAppearance.BorderSize = 0;
            this.searchAliasPartNobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchAliasPartNobutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchAliasPartNobutton.Location = new System.Drawing.Point(642, 6);
            this.searchAliasPartNobutton.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.searchAliasPartNobutton.Name = "searchAliasPartNobutton";
            this.searchAliasPartNobutton.Size = new System.Drawing.Size(24, 24);
            this.searchAliasPartNobutton.TabIndex = 3;
            this.searchAliasPartNobutton.Tag = "3";
            this.searchAliasPartNobutton.UseVisualStyleBackColor = false;
            this.searchAliasPartNobutton.Click += new System.EventHandler(this.searchAliasPartNobutton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.DatatableLayoutPanel.SetColumnSpan(this.label5, 2);
            this.label5.Location = new System.Drawing.Point(210, 97);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "Spesifikasi";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.DatatableLayoutPanel.SetColumnSpan(this.label4, 2);
            this.label4.Location = new System.Drawing.Point(210, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "Brand";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.DatatableLayoutPanel.SetColumnSpan(this.label3, 2);
            this.label3.Location = new System.Drawing.Point(210, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Part Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.DatatableLayoutPanel.SetColumnSpan(this.label2, 2);
            this.label2.Location = new System.Drawing.Point(210, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Part No";
            // 
            // PartNotextBox
            // 
            this.PartNotextBox.BackColor = System.Drawing.SystemColors.Window;
            this.PartNotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PartNotextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PartNotextBox.Location = new System.Drawing.Point(301, 3);
            this.PartNotextBox.Name = "PartNotextBox";
            this.PartNotextBox.Size = new System.Drawing.Size(335, 26);
            this.PartNotextBox.TabIndex = 1;
            this.PartNotextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PartNotextBox_KeyDown);
            this.PartNotextBox.Leave += new System.EventHandler(this.PartNotextBox_Leave);
            // 
            // PartNametextBox
            // 
            this.PartNametextBox.BackColor = System.Drawing.Color.White;
            this.PartNametextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.PartNametextBox, 4);
            this.PartNametextBox.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PartNametextBox.ForeColor = System.Drawing.Color.Black;
            this.PartNametextBox.Location = new System.Drawing.Point(301, 36);
            this.PartNametextBox.Name = "PartNametextBox";
            this.PartNametextBox.ReadOnly = true;
            this.PartNametextBox.Size = new System.Drawing.Size(399, 26);
            this.PartNametextBox.TabIndex = 2;
            // 
            // BrandtextBox
            // 
            this.BrandtextBox.BackColor = System.Drawing.Color.White;
            this.BrandtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BrandtextBox.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BrandtextBox.ForeColor = System.Drawing.Color.Black;
            this.BrandtextBox.Location = new System.Drawing.Point(301, 68);
            this.BrandtextBox.Name = "BrandtextBox";
            this.BrandtextBox.ReadOnly = true;
            this.BrandtextBox.Size = new System.Drawing.Size(255, 26);
            this.BrandtextBox.TabIndex = 3;
            // 
            // Spesifikasipanel
            // 
            this.Spesifikasipanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.Spesifikasipanel, 4);
            this.Spesifikasipanel.Controls.Add(this.SpesifikasirichTextBox);
            this.Spesifikasipanel.Location = new System.Drawing.Point(301, 100);
            this.Spesifikasipanel.Name = "Spesifikasipanel";
            this.DatatableLayoutPanel.SetRowSpan(this.Spesifikasipanel, 2);
            this.Spesifikasipanel.Size = new System.Drawing.Size(399, 107);
            this.Spesifikasipanel.TabIndex = 6;
            // 
            // SpesifikasirichTextBox
            // 
            this.SpesifikasirichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SpesifikasirichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SpesifikasirichTextBox.Enabled = false;
            this.SpesifikasirichTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SpesifikasirichTextBox.ForeColor = System.Drawing.Color.Black;
            this.SpesifikasirichTextBox.Location = new System.Drawing.Point(0, 0);
            this.SpesifikasirichTextBox.Name = "SpesifikasirichTextBox";
            this.SpesifikasirichTextBox.ReadOnly = true;
            this.SpesifikasirichTextBox.Size = new System.Drawing.Size(397, 105);
            this.SpesifikasirichTextBox.TabIndex = 7;
            this.SpesifikasirichTextBox.TabStop = false;
            this.SpesifikasirichTextBox.Text = "";
            // 
            // TodateTimePicker
            // 
            this.TodateTimePicker.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatatableLayoutPanel.SetColumnSpan(this.TodateTimePicker, 2);
            this.TodateTimePicker.CustomFormat = "dd/MM/yyyy";
            this.TodateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TodateTimePicker.Location = new System.Drawing.Point(243, 213);
            this.TodateTimePicker.Name = "TodateTimePicker";
            this.TodateTimePicker.Size = new System.Drawing.Size(137, 26);
            this.TodateTimePicker.TabIndex = 21;
            this.TodateTimePicker.ValueChanged += new System.EventHandler(this.TodateTimePicker_ValueChanged);
            // 
            // FromdateTimePicker
            // 
            this.FromdateTimePicker.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FromdateTimePicker.CustomFormat = "dd/MM/yyyy";
            this.FromdateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FromdateTimePicker.Location = new System.Drawing.Point(55, 213);
            this.FromdateTimePicker.Name = "FromdateTimePicker";
            this.FromdateTimePicker.Size = new System.Drawing.Size(134, 26);
            this.FromdateTimePicker.TabIndex = 20;
            this.FromdateTimePicker.Value = new System.DateTime(2013, 11, 9, 0, 0, 0, 0);
            this.FromdateTimePicker.ValueChanged += new System.EventHandler(this.FromdateTimePicker_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(210, 213);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 20);
            this.label7.TabIndex = 19;
            this.label7.Text = "To";
            // 
            // pictureboxpanel
            // 
            this.DatatableLayoutPanel.SetColumnSpan(this.pictureboxpanel, 2);
            this.pictureboxpanel.Controls.Add(this.PartpictureBox);
            this.pictureboxpanel.Location = new System.Drawing.Point(3, 3);
            this.pictureboxpanel.Name = "pictureboxpanel";
            this.DatatableLayoutPanel.SetRowSpan(this.pictureboxpanel, 4);
            this.pictureboxpanel.Size = new System.Drawing.Size(201, 154);
            this.pictureboxpanel.TabIndex = 15;
            // 
            // PartpictureBox
            // 
            this.PartpictureBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.PartpictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PartpictureBox.Image = global::Sufindo.Properties.Resources.noimage;
            this.PartpictureBox.Location = new System.Drawing.Point(0, 0);
            this.PartpictureBox.Name = "PartpictureBox";
            this.PartpictureBox.Size = new System.Drawing.Size(201, 154);
            this.PartpictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PartpictureBox.TabIndex = 15;
            this.PartpictureBox.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 213);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 20);
            this.label6.TabIndex = 18;
            this.label6.Text = "From";
            // 
            // paneldatagridview
            // 
            this.DatatableLayoutPanel.SetColumnSpan(this.paneldatagridview, 11);
            this.paneldatagridview.Controls.Add(this.TransaksidataGridView);
            this.paneldatagridview.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paneldatagridview.Location = new System.Drawing.Point(3, 245);
            this.paneldatagridview.Name = "paneldatagridview";
            this.paneldatagridview.Size = new System.Drawing.Size(997, 279);
            this.paneldatagridview.TabIndex = 17;
            // 
            // TransaksidataGridView
            // 
            this.TransaksidataGridView.AllowUserToAddRows = false;
            this.TransaksidataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.TransaksidataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.TransaksidataGridView.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TransaksidataGridView.BackgroundColor = System.Drawing.SystemColors.ActiveCaptionText;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TransaksidataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.TransaksidataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.TransaksidataGridView.DefaultCellStyle = dataGridViewCellStyle11;
            this.TransaksidataGridView.Location = new System.Drawing.Point(0, 0);
            this.TransaksidataGridView.Name = "TransaksidataGridView";
            this.TransaksidataGridView.ReadOnly = true;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TransaksidataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.TransaksidataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TransaksidataGridView.Size = new System.Drawing.Size(997, 276);
            this.TransaksidataGridView.TabIndex = 3;
            // 
            // DatatableLayoutPanel
            // 
            this.DatatableLayoutPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.DatatableLayoutPanel.ColumnCount = 10;
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.Controls.Add(this.paneldatagridview, 0, 6);
            this.DatatableLayoutPanel.Controls.Add(this.label6, 0, 5);
            this.DatatableLayoutPanel.Controls.Add(this.pictureboxpanel, 0, 0);
            this.DatatableLayoutPanel.Controls.Add(this.label7, 2, 5);
            this.DatatableLayoutPanel.Controls.Add(this.FromdateTimePicker, 1, 5);
            this.DatatableLayoutPanel.Controls.Add(this.TodateTimePicker, 3, 5);
            this.DatatableLayoutPanel.Controls.Add(this.BrandtextBox, 4, 2);
            this.DatatableLayoutPanel.Controls.Add(this.PartNametextBox, 4, 1);
            this.DatatableLayoutPanel.Controls.Add(this.PartNotextBox, 4, 0);
            this.DatatableLayoutPanel.Controls.Add(this.label2, 2, 0);
            this.DatatableLayoutPanel.Controls.Add(this.label3, 2, 1);
            this.DatatableLayoutPanel.Controls.Add(this.label4, 2, 2);
            this.DatatableLayoutPanel.Controls.Add(this.label5, 2, 3);
            this.DatatableLayoutPanel.Controls.Add(this.searchAliasPartNobutton, 5, 0);
            this.DatatableLayoutPanel.Controls.Add(this.searchPartNobutton, 6, 0);
            this.DatatableLayoutPanel.Controls.Add(this.SatuantextBox, 9, 0);
            this.DatatableLayoutPanel.Controls.Add(this.RaktextBox, 9, 1);
            this.DatatableLayoutPanel.Controls.Add(this.label8, 8, 0);
            this.DatatableLayoutPanel.Controls.Add(this.label1, 8, 1);
            this.DatatableLayoutPanel.Controls.Add(this.panel1, 0, 7);
            this.DatatableLayoutPanel.Controls.Add(this.label10, 8, 2);
            this.DatatableLayoutPanel.Controls.Add(this.SellPricetextBox, 9, 2);
            this.DatatableLayoutPanel.Controls.Add(this.textBoxUpdate, 9, 3);
            this.DatatableLayoutPanel.Controls.Add(this.label11, 8, 3);
            this.DatatableLayoutPanel.Controls.Add(this.panel2, 0, 4);
            this.DatatableLayoutPanel.Controls.Add(this.Spesifikasipanel, 4, 3);
            this.DatatableLayoutPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatatableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.DatatableLayoutPanel.Name = "DatatableLayoutPanel";
            this.DatatableLayoutPanel.RowCount = 8;
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.Size = new System.Drawing.Size(1003, 568);
            this.DatatableLayoutPanel.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.DatatableLayoutPanel.SetColumnSpan(this.panel1, 10);
            this.panel1.Controls.Add(this.textBoxTotal);
            this.panel1.Controls.Add(this.textBoxOut);
            this.panel1.Controls.Add(this.textBoxIn);
            this.panel1.Controls.Add(this.textBoxSaldo);
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(3, 530);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(997, 34);
            this.panel1.TabIndex = 22;
            // 
            // textBoxTotal
            // 
            this.textBoxTotal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxTotal.Location = new System.Drawing.Point(657, 6);
            this.textBoxTotal.Name = "textBoxTotal";
            this.textBoxTotal.Size = new System.Drawing.Size(49, 22);
            this.textBoxTotal.TabIndex = 3;
            this.textBoxTotal.Text = "Total";
            // 
            // textBoxOut
            // 
            this.textBoxOut.BackColor = System.Drawing.Color.White;
            this.textBoxOut.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxOut.ForeColor = System.Drawing.Color.Black;
            this.textBoxOut.Location = new System.Drawing.Point(808, 6);
            this.textBoxOut.Name = "textBoxOut";
            this.textBoxOut.ReadOnly = true;
            this.textBoxOut.Size = new System.Drawing.Size(60, 22);
            this.textBoxOut.TabIndex = 2;
            this.textBoxOut.Text = "1000";
            // 
            // textBoxIn
            // 
            this.textBoxIn.BackColor = System.Drawing.Color.White;
            this.textBoxIn.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxIn.ForeColor = System.Drawing.Color.Black;
            this.textBoxIn.Location = new System.Drawing.Point(743, 6);
            this.textBoxIn.Name = "textBoxIn";
            this.textBoxIn.ReadOnly = true;
            this.textBoxIn.Size = new System.Drawing.Size(60, 22);
            this.textBoxIn.TabIndex = 1;
            this.textBoxIn.Text = "1000";
            // 
            // textBoxSaldo
            // 
            this.textBoxSaldo.BackColor = System.Drawing.Color.White;
            this.textBoxSaldo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxSaldo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSaldo.ForeColor = System.Drawing.Color.Black;
            this.textBoxSaldo.Location = new System.Drawing.Point(874, 6);
            this.textBoxSaldo.Name = "textBoxSaldo";
            this.textBoxSaldo.ReadOnly = true;
            this.textBoxSaldo.Size = new System.Drawing.Size(60, 22);
            this.textBoxSaldo.TabIndex = 0;
            this.textBoxSaldo.Text = "1000";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(706, 65);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 20);
            this.label10.TabIndex = 24;
            this.label10.Text = "Sale Price";
            // 
            // SellPricetextBox
            // 
            this.SellPricetextBox.BackColor = System.Drawing.Color.White;
            this.SellPricetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SellPricetextBox.Decimal = false;
            this.SellPricetextBox.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SellPricetextBox.ForeColor = System.Drawing.Color.Black;
            this.SellPricetextBox.Location = new System.Drawing.Point(792, 68);
            this.SellPricetextBox.Money = true;
            this.SellPricetextBox.Name = "SellPricetextBox";
            this.SellPricetextBox.Numeric = false;
            this.SellPricetextBox.Prefix = "";
            this.SellPricetextBox.ReadOnly = true;
            this.SellPricetextBox.Size = new System.Drawing.Size(147, 26);
            this.SellPricetextBox.TabIndex = 26;
            // 
            // textBoxUpdate
            // 
            this.textBoxUpdate.BackColor = System.Drawing.Color.White;
            this.textBoxUpdate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxUpdate.Font = new System.Drawing.Font("Eras Demi ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxUpdate.ForeColor = System.Drawing.Color.Black;
            this.textBoxUpdate.Location = new System.Drawing.Point(792, 100);
            this.textBoxUpdate.Name = "textBoxUpdate";
            this.textBoxUpdate.ReadOnly = true;
            this.textBoxUpdate.Size = new System.Drawing.Size(146, 26);
            this.textBoxUpdate.TabIndex = 27;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(706, 97);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 20);
            this.label11.TabIndex = 28;
            this.label11.Text = "Date";
            // 
            // panel2
            // 
            this.DatatableLayoutPanel.SetColumnSpan(this.panel2, 4);
            this.panel2.Controls.Add(this.OpenOnOrderbutton);
            this.panel2.Controls.Add(this.TotalOnOrderlabel);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Location = new System.Drawing.Point(3, 163);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(201, 35);
            this.panel2.TabIndex = 23;
            // 
            // OpenOnOrderbutton
            // 
            this.OpenOnOrderbutton.BackColor = System.Drawing.Color.Transparent;
            this.OpenOnOrderbutton.BackgroundImage = global::Sufindo.Properties.Resources.history;
            this.OpenOnOrderbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.OpenOnOrderbutton.FlatAppearance.BorderSize = 0;
            this.OpenOnOrderbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OpenOnOrderbutton.ForeColor = System.Drawing.Color.Transparent;
            this.OpenOnOrderbutton.Location = new System.Drawing.Point(160, 3);
            this.OpenOnOrderbutton.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.OpenOnOrderbutton.Name = "OpenOnOrderbutton";
            this.OpenOnOrderbutton.Size = new System.Drawing.Size(26, 24);
            this.OpenOnOrderbutton.TabIndex = 3;
            this.OpenOnOrderbutton.Tag = "2";
            this.OpenOnOrderbutton.UseVisualStyleBackColor = false;
            this.OpenOnOrderbutton.Click += new System.EventHandler(this.OpenOnOrderbutton_Click);
            // 
            // TotalOnOrderlabel
            // 
            this.TotalOnOrderlabel.AutoSize = true;
            this.TotalOnOrderlabel.Location = new System.Drawing.Point(117, 7);
            this.TotalOnOrderlabel.Name = "TotalOnOrderlabel";
            this.TotalOnOrderlabel.Size = new System.Drawing.Size(18, 20);
            this.TotalOnOrderlabel.TabIndex = 1;
            this.TotalOnOrderlabel.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(107, 20);
            this.label9.TabIndex = 0;
            this.label9.Text = "ON ORDER : ";
            // 
            // RecordCardStokItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 577);
            this.Controls.Add(this.DatatableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "RecordCardStokItem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Inventory Audit";
            this.Activated += new System.EventHandler(this.RecordCardStokItem_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.RecordCardStokItem_FormClosed);
            this.Spesifikasipanel.ResumeLayout(false);
            this.pictureboxpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PartpictureBox)).EndInit();
            this.paneldatagridview.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TransaksidataGridView)).EndInit();
            this.DatatableLayoutPanel.ResumeLayout(false);
            this.DatatableLayoutPanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel DatatableLayoutPanel;
        private System.Windows.Forms.Panel paneldatagridview;
        private System.Windows.Forms.DataGridView TransaksidataGridView;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel pictureboxpanel;
        private System.Windows.Forms.PictureBox PartpictureBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker FromdateTimePicker;
        private System.Windows.Forms.DateTimePicker TodateTimePicker;
        private System.Windows.Forms.Panel Spesifikasipanel;
        private System.Windows.Forms.RichTextBox SpesifikasirichTextBox;
        private System.Windows.Forms.TextBox BrandtextBox;
        private System.Windows.Forms.TextBox PartNametextBox;
        private System.Windows.Forms.TextBox PartNotextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button searchAliasPartNobutton;
        private System.Windows.Forms.Button searchPartNobutton;
        private System.Windows.Forms.TextBox SatuantextBox;
        private System.Windows.Forms.TextBox RaktextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button OpenOnOrderbutton;
        private System.Windows.Forms.Label TotalOnOrderlabel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private CustomControls.TextBoxSL SellPricetextBox;
        private System.Windows.Forms.TextBox textBoxUpdate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxTotal;
        private System.Windows.Forms.TextBox textBoxOut;
        private System.Windows.Forms.TextBox textBoxIn;
        private System.Windows.Forms.TextBox textBoxSaldo;

    }
}