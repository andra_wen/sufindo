﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;

namespace Sufindo
{
    public partial class ItemSupplier : Form
    {
        private delegate void fillTextFieldAutoCompleteCallBack();

        Itemdatagridview itemdatagridview = null;
        ToolStripItemCollection iconItemCollectionMain;
        MenuStrip menustripAction;
        Supplierdatagridview supplierdatagridview = null;

        Connection connection = null;
        DataTable datatable;

        String activity = "";

        List<String> SupplierNo;
        List<String> SupplierName;

        public ItemSupplier(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();

            if (connection == null) connection = new Connection();
            this.menustripAction = menustripAction;
            datatable = new DataTable();
            activity = "INSERT";
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();

            }
            catch (Exception ex)
            {
                MessageBox.Show("MasterItem reloadAllData : " + ex.Message);
            }
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //MessageBox.Show("Completed");
        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (SupplierNo == null) SupplierNo = new List<string>();
            if (SupplierName == null) SupplierName = new List<string>();

            SqlDataReader sqldataReader = connection.sqlDataReaders("SELECT SUPPLIERID, SUPPLIERNAME FROM M_SUPPLIER");

            while (sqldataReader.Read())
            {
                SupplierNo.Add(sqldataReader.GetString(0));
                SupplierName.Add(sqldataReader.GetString(1));
            }

            if (IsHandleCreated) BeginInvoke(new fillTextFieldAutoCompleteCallBack(this.fillTextFieldAutoComplete));

        }

        void fillTextFieldAutoComplete() {
            SupplierNotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            SupplierNotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            SupplierNotextBox.AutoCompleteCustomSource.AddRange(SupplierNo.ToArray());
            SupplierNotextBox.Text = "";

            SupplierNametextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            SupplierNametextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            SupplierNametextBox.AutoCompleteCustomSource.AddRange(SupplierName.ToArray());
            SupplierNametextBox.Text = "";
        }

        private void AddItembutton_Click(object sender, EventArgs e)
        {
            if (itemdatagridview == null)
            {
                itemdatagridview = new Itemdatagridview("ITEMSUPPLIER",datatable);
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                //itemdatagridview.MdiParent = this.MdiParent;
                itemdatagridview.ShowDialog();
            }
            else if (itemdatagridview.IsDisposed)
            {
                itemdatagridview = new Itemdatagridview("ITEMSUPPLIER", datatable);
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                //itemdatagridview.MdiParent = this.MdiParent;
                itemdatagridview.ShowDialog();
            }
        }

        private void MasterItemPassingData (DataTable datatable){
            /*remove column not use*/
            List<string> columnsRemove = new List<string>();

            foreach (DataColumn column in datatable.Columns)
            {
                Boolean isExistingColumn = false;
                foreach (DataGridViewColumn columngrid in MasterItemdataGridView.Columns)
                {
                    if (column.ColumnName.Equals(columngrid.DataPropertyName))
                    {
                        isExistingColumn = true;
                        break;
                    }
                }
                if (!isExistingColumn)
                {
                    columnsRemove.Add(column.ColumnName);
                }
            }
            foreach (string columnName in columnsRemove)
            {
                datatable.Columns.Remove(columnName);
            }

            if (this.datatable.Rows.Count == 0){
                this.datatable = datatable;
            }
            else{
                foreach (DataRow row in datatable.Rows){
                    this.datatable.Rows.Add(row.ItemArray);
                }
            }

            Utilities.generateNoDataTable(ref this.datatable);
            this.datatable = Utilities.RemoveDuplicateRows(this.datatable, "ITEMID");
            MasterItemdataGridView.DataSource = this.datatable;
            //MasterItemdataGridView.Columns.Add("Delete", "");
        }

        private void MasterItemdataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1) {
                if (e.ColumnIndex == 0) {
                    datatable.Rows.RemoveAt(e.RowIndex);
                    Utilities.generateNoDataTable(ref datatable);

                    MasterItemdataGridView.DataSource = datatable;
                }
            }
        }

        private void MasterItemdataGridView_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e){
            if (e.Button == MouseButtons.Right) {
                if (e.RowIndex == -1 && e.ColumnIndex != -1){
                    ContextMenuStrip menu = new ContextMenuStrip();
                    menu.BackColor = Color.White;
                    
                    var frozen = menu.Items.Add(String.Format("Frozen {0}", MasterItemdataGridView.Columns[e.ColumnIndex].HeaderText));
                    frozen.Name = "Frozen";
                    int ColumnIndex = e.ColumnIndex;
                    menu.Items["Frozen"].Click += new EventHandler((senderx, ex) => Frozen_Click(senderx, ex, ColumnIndex));
                    
                    if (MasterItemdataGridView.Columns[e.ColumnIndex].Frozen){
                        var unfrozen = menu.Items.Add(String.Format("UnFrozen {0}", MasterItemdataGridView.Columns[e.ColumnIndex].HeaderText));
                        unfrozen.Name = "UnFrozen";
                        menu.Items["UnFrozen"].Click += new EventHandler((senderx, ex) => UnFrozen_Click(senderx, ex, ColumnIndex));
                    }
                    var unfrozenAll = menu.Items.Add("UnFrozen All Column");
                    unfrozenAll.Name = "UnFrozenAll";
                    menu.Items["UnFrozenAll"].Click +=new EventHandler(UnFrozenAll_Click);
                    Point point = MasterItemdataGridView.PointToClient(Control.MousePosition);

                    menu.Show(MasterItemdataGridView, point);
                }
            }
        }

        void UnFrozen_Click(object sender, EventArgs e, int ColumnIndex)
        {
            MasterItemdataGridView.Columns[ColumnIndex].Frozen = false;
        }

        void Frozen_Click(object sender, EventArgs e, int ColumnIndex)
        {
            MasterItemdataGridView.Columns[ColumnIndex].Frozen = true;
        }

        void UnFrozenAll_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
            {
                if (column.Frozen) {
                    column.Frozen = false;
                }
            }
        }

        private void searchSupplierNobutton_Click(object sender, EventArgs e)
        {
            if (supplierdatagridview == null)
            {
                supplierdatagridview = new Supplierdatagridview("MasterSupplier");
                supplierdatagridview.masterItemPassingData = new Supplierdatagridview.MasterItemPassingData(MasterSupplierPassingData);
                //supplierdatagridview.MdiParent = this.MdiParent;
                supplierdatagridview.ShowDialog();
            }
            else if (supplierdatagridview.IsDisposed)
            {
                supplierdatagridview = new Supplierdatagridview("MasterSupplier");
                supplierdatagridview.masterItemPassingData = new Supplierdatagridview.MasterItemPassingData(MasterSupplierPassingData);
                //supplierdatagridview.MdiParent = this.MdiParent;
                supplierdatagridview.ShowDialog();
            }
        }

        private void MasterSupplierPassingData(DataTable sender)
        {
            sender.Rows[0]["SUPPLIERID"].ToString();
            SupplierNotextBox.Text = sender.Rows[0]["SUPPLIERID"].ToString();
            SupplierNametextBox.Text = sender.Rows[0]["SUPPLIERNAME"].ToString();

            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMID, ITEMNAME,ITEMID_ALIAS, BRANDID, " +
                                         "SPEC, QUANTITY, UOMID, BESTPRICE, RACKID, ITEMIMAGE, CREATEDBY, UPDATEDDATE " +
                                         "FROM D_ITEM_SUPPLIER A " +
                                         "JOIN M_ITEM B " +
                                         "ON A.ITEMID = B.ITEMID " +
                                         "WHERE A.SUPPLIERID = '{0}'", SupplierNotextBox.Text);
            
            DataTable datatable = connection.openDataTableQuery(query);
            MasterItemPassingData(datatable);
            //MasterItemdataGridView.DataSource = this.datatable;

            //AddressrichTextBox.Text = sender.Rows[0]["ADDRESS"].ToString();
            //ZonetextBox.Text = sender.Rows[0]["ZONE"].ToString();
            //NoTelptextBox.Text = sender.Rows[0]["NOTELP"].ToString();
            //NoFaxtextBox.Text = sender.Rows[0]["FAX"].ToString();
            //EmailtextBox.Text = sender.Rows[0]["EMAIL"].ToString();
            //ContactPersontextBox.Text = sender.Rows[0]["CONTACTPERSON"].ToString();
            //JabatantextBox.Text = sender.Rows[0]["JABATAN"].ToString();
        }

        public void save() {
            try
            {
                if (activity.Equals("UPDATE") || activity.Equals("INSERT"))
                {
                    if (isValidation()) {

                        List<string> values = new List<string>();
                        foreach (DataRow row in this.datatable.Rows)
                        {
                            String value = String.Format("SELECT '{0}', '{1}'", row["ITEMID"].ToString(), SupplierNotextBox.Text);
                            values.Add(value);
                        }
                        String[] columns = { "ITEMID", "SUPPLIERID" };
                        String query = String.Format("DELETE FROM D_ITEM_SUPPLIER WHERE SUPPLIERID = '{0}'", SupplierNotextBox.Text);
                        if (connection.openReaderQuery(query))
                        {
                            if (connection.openReaderQuery(Utilities.queryMultipleInsert("D_ITEM_SUPPLIER", columns, values)))
                            {
                                MessageBox.Show("SUCCESS INSERT");
                                datatable.Clear();
                                SupplierNotextBox.Text = "";
                                SupplierNametextBox.Text = "";
                            }
                        }
                    }
                    
                }
            }
            catch (Exception ex){
                MessageBox.Show("INSERT FAILED ON insertMasterItem() : " + ex);
            }
        }

        private Boolean isValidation() {
            if (SupplierNotextBox.Text.Equals(""))
            {
                MessageBox.Show("Please Choose Supplier No");
            }
            else if (MasterItemdataGridView.Rows.Count == 0)
            {
                MessageBox.Show("Minimal One Part Item");
            }
            else return true;
            return false;
        }

        private void SupplierNotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in SupplierNo)
                {
                    if (item.ToUpper().Equals(SupplierNotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        SupplierNotextBox.Text = SupplierNotextBox.Text.ToUpper();
                        String query = String.Format("SELECT SUPPLIERID, SUPPLIERNAME " +
                                                     "FROM M_SUPPLIER " +
                                                     "WHERE SUPPLIERID = '{0}'", SupplierNotextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        MasterSupplierPassingData(datatable);
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    SupplierNotextBox.Text = "";
                    SupplierNametextBox.Text = "";
                    datatable.Clear();
                    MasterItemdataGridView.DataSource = datatable;
                }
            }
        }

        private void SupplierNotextBox_Leave(object sender, EventArgs e)
        {
            Boolean isExistingPartNo = false;
            SupplierNotextBox.Text = SupplierNotextBox.Text.ToUpper();
            foreach (String item in SupplierNo)
            {
                if (item.ToUpper().Equals(SupplierNotextBox.Text.ToUpper()))
                {
                    isExistingPartNo = true;
                    SupplierNotextBox.Text = SupplierNotextBox.Text.ToUpper();
                    String query = String.Format("SELECT SUPPLIERID, SUPPLIERNAME " +
                                                 "FROM M_SUPPLIER " +
                                                 "WHERE SUPPLIERID = '{0}'", SupplierNotextBox.Text);

                    DataTable datatable = connection.openDataTableQuery(query);
                    MasterSupplierPassingData(datatable);
                    break;
                }
            }
            if (!isExistingPartNo)
            {
                SupplierNotextBox.Text = "";
                SupplierNametextBox.Text = "";
                datatable.Clear();
                MasterItemdataGridView.DataSource = datatable;
            }
        }

        private void SupplierNametextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in SupplierName)
                {
                    if (item.ToUpper().Equals(SupplierNametextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        SupplierNametextBox.Text = SupplierNametextBox.Text.ToUpper();
                        String query = String.Format("SELECT SUPPLIERID, SUPPLIERNAME "+
                                                     "FROM M_SUPPLIER " +
                                                     "WHERE SUPPLIERNAME = '{0}'", SupplierNametextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        MasterSupplierPassingData(datatable);
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    SupplierNotextBox.Text = "";
                    SupplierNametextBox.Text = "";
                    datatable.Clear();
                    MasterItemdataGridView.DataSource = datatable;
                }
            }
        }

        private void SupplierNametextBox_Leave(object sender, EventArgs e)
        {
            Boolean isExistingPartNo = false;
            SupplierNametextBox.Text = SupplierNotextBox.Text.ToUpper();
            foreach (String item in SupplierName)
            {
                if (item.ToUpper().Equals(SupplierNametextBox.Text.ToUpper()))
                {
                    isExistingPartNo = true;
                    break;
                }
            }
            if (!isExistingPartNo)
            {
                SupplierNotextBox.Text = "";
                SupplierNametextBox.Text = "";
                datatable.Clear();
                MasterItemdataGridView.DataSource = datatable;
            }
        }

        private void ItemSupplier_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }

        private void MasterItemdataGridView_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            for (int i = 0; i < datatable.Rows.Count; i++)
            {
                if (datatable.Rows[i].RowState == DataRowState.Deleted) {
                    datatable.Rows.RemoveAt(i);
                }
            }
            Utilities.generateNoDataTable(ref datatable);
        }

        private void ItemSupplier_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            reloadAllData();
            Main.ACTIVEFORM = this;
        }
    }
}
