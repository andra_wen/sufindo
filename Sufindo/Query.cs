﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sufindo
{
    class Query
    {
        public static String GENERATE_ID_BUILD_ITEM() {
            
            return "";
        }

        public static String GENERATE_ID_FOR_PO() {
            Connection con = new Connection();
            con.openDataTableQuery("SELECT PURCHASEORDERID FROM H_PURCHASEORDER");
            return "";
        }

        public static String SELECT_ITEM(String STATUS, String FIND)
        {   
            /*
               SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'No', A.ITEMID, ITEMNAME, ITEMID_ALIAS,
               BRANDID, SPEC, QUANTITY, UOMID, BESTPRICE, RACKID, ITEMIMAGE,
               CREATEDBY, UPDATEDDATE 
               FROM M_ITEM A
               WHERE [STATUS] = 'Actived'
               ORDER BY A.ITEMID ASC
             */
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'No', A.ITEMID, ITEMNAME, ITEMID_ALIAS, " +
                                         "BRANDID, SPEC, QUANTITY, UOMID, BESTPRICE, RACKID, ITEMIMAGE, " +
                                         "CREATEDBY, CONVERT(VARCHAR(10), UPDATEDDATE, 103) AS [UPDATEDDATE] " +
                                         "FROM M_ITEM A " +
                                         "WHERE {0} [STATUS] = '{1}' " +
                                         "ORDER BY A.ITEMID ASC", FIND, STATUS);

            return query;
        }

        public static String SELECT_ITEM_WITH_ALIAS(String STATUS, String ITEMID_ALIAS, String FIND) {
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'No', A.ITEMID, ITEMNAME, ITEMID_ALIAS, " +
                                         "BRANDID, SPEC, QUANTITY, UOMID, BESTPRICE, RACKID, ITEMIMAGE, " +
                                         "CREATEDBY, CONVERT(VARCHAR(10), UPDATEDDATE, 103) AS [UPDATEDDATE] " +
                                         "FROM M_ITEM A " +
                                         "WHERE {0} ITEMID_ALIAS = '{1}' AND [STATUS] = '{2}' " +
                                         "ORDER BY A.ITEMID ASC", FIND, ITEMID_ALIAS ,STATUS);
            return query;
        }

        public static String SELECT_ITEM_FOR_PO_WITH_SUPPLIER(String STATUS, String SUPPLIERID) {
            /*
                SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', 
                A.ITEMID, ITEMNAME, ITEMID_ALIAS, 
                BRANDID, SPEC, QUANTITY, UOMID, RACKID, 
                (
	                SELECT TOP 1 PRICE 
	                FROM LOG_TRANSACTION Z
	                JOIN D_LOG_TRANSACTION Y 
	                ON Z.TRANSACTIONID = Y.TRANSACTIONID
	                WHERE ITEMID = A.ITEMID 
	                ORDER BY TRANSACTIONDATE DESC
                ) AS UNITPRICE, '0' AS AMOUNT
                FROM M_ITEM A 
                JOIN D_ITEM_SUPPLIER B
                ON A.ITEMID = B.ITEMID
                WHERE [STATUS] = 'Actived' AND SUPPLIERID = 'YS'
                ORDER BY A.ITEMID ASC
             */
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', " +
                                         "A.ITEMID, ITEMNAME, ITEMID_ALIAS, " +
                                         "( " +
                                         "   SELECT ITEMNAME " +
                                         "   FROM M_ITEM " +
                                         "   WHERE ITEMID = A.ITEMID_ALIAS " +
                                         ") AS ITEMNAME_ALIAS, " +
                                         "BRANDID, SPEC, QUANTITY, UOMID, RACKID, " +
                                         "(" +
                                         "    SELECT TOP 1 BUYPRICE " +
                                         "    FROM LOG_TRANSACTION " +
                                         "    WHERE ITEMID = A.ITEMID " +
                                         "    ORDER BY TRANSACTIONDATE DESC " +
                                         ") AS UNITPRICE, '0' AS AMOUNT " +
                                         "FROM M_ITEM A " +
                                         "JOIN D_ITEM_SUPPLIER B " +
                                         "ON A.ITEMID = B.ITEMID " +
                                         "WHERE [STATUS] = '{0}' AND SUPPLIERID = '{1}' " +
                                         "ORDER BY A.ITEMID ASC", STATUS, SUPPLIERID);
            return query;
        }

        public static String SELECT_ITEM_FOR_PO_WITHOUT_SUPPLIER(String STATUS, String FIND)
        {

            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY ITEMID) AS NO, * FROM " +
                                         "( " +
	                                     "   SELECT A.ITEMID, ITEMNAME, ITEMID_ALIAS, " +
	                                     "   (SELECT ITEMNAME FROM M_ITEM WHERE ITEMID = A.ITEMID_ALIAS ) AS ITEMNAME_ALIAS," + 
	                                     "   BRANDID, SPEC, QUANTITY, UOMID, RACKID, " +
	                                     "   (SELECT TOP 1 BUYPRICE FROM LOG_TRANSACTION WHERE ITEMID = A.ITEMID ORDER BY TRANSACTIONDATE DESC ) AS UNITPRICE, " +
	                                     "   '0' AS AMOUNT " +
	                                     "   FROM M_ITEM A JOIN D_ITEM_SUPPLIER B " +
	                                     "   ON A.ITEMID = B.ITEMID " +
	                                     "   WHERE {0} [STATUS] = '{1}' " +
                                         " 	" +
	                                     "     UNION " +
	                                     "   SELECT A.ITEMID, ITEMNAME, ITEMID_ALIAS, " +
	                                     "   (SELECT ITEMNAME FROM M_ITEM WHERE ITEMID = A.ITEMID_ALIAS ) AS ITEMNAME_ALIAS, BRANDID, SPEC," +
	                                     "   QUANTITY, UOMID, RACKID, '0' AS UNITPRICE, '0' AS AMOUNT " +
	                                     "   FROM M_ITEM A WHERE {0} " +
	                                     "   NOT EXISTS (SELECT ITEMID FROM D_ITEM_SUPPLIER WHERE ITEMID = A.ITEMID) " +
	                                     "   AND [STATUS] = '{1}' " +
                                         ") AS TEMPVIEW ORDER BY ITEMID", FIND, STATUS);
            return query;
        }

        public static String SELECT_ITEM_FOR_PR_WITH_SUPPLIER(String STATUS, String SUPPLIERID) {
            /*
                SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', 
                A.ITEMID, ITEMNAME, ITEMID_ALIAS, 
                (
	                SELECT ITEMNAME 
	                FROM M_ITEM
	                WHERE ITEMID = A.ITEMID_ALIAS
                ) AS ITEMNAME_ALIAS,
                BRANDID, SPEC, QUANTITY, UOMID, RACKID, 
                (
                    SELECT TOP 1 PRICE 
                    FROM LOG_TRANSACTION Z
                    JOIN D_LOG_TRANSACTION Y 
                    ON Z.TRANSACTIONID = Y.TRANSACTIONID
                    WHERE ITEMID = A.ITEMID 
                    ORDER BY TRANSACTIONDATE DESC
                ) AS UNITPRICE, '0' AS AMOUNT
                FROM M_ITEM A 
                JOIN D_ITEM_SUPPLIER B
                ON A.ITEMID = B.ITEMID
                WHERE [STATUS] = 'Actived' AND SUPPLIERID = 'YS'
                ORDER BY A.ITEMID ASC
             */
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', " +
                                         "A.ITEMID, ITEMNAME, ITEMID_ALIAS, " +
                                         "( " +
	                                     "   SELECT ITEMNAME " +
	                                     "   FROM M_ITEM " +
	                                     "   WHERE ITEMID = A.ITEMID_ALIAS " +
                                         ") AS ITEMNAME_ALIAS, " +
                                         "BRANDID, SPEC, QUANTITY, UOMID, RACKID, " +
                                         "( " +
                                         "  SELECT TOP 1 BUYPRICE " +
                                         "   FROM LOG_TRANSACTION " +
                                         "   WHERE ITEMID = A.ITEMID " +
                                         "   ORDER BY TRANSACTIONDATE DESC " +
                                         ") AS UNITPRICE, '0' AS AMOUNT " +
                                         "FROM M_ITEM A " +
                                         "JOIN D_ITEM_SUPPLIER B " +
                                         "ON A.ITEMID = B.ITEMID " +
                                         "WHERE [STATUS] = '{0}' AND SUPPLIERID = '{1}' " +
                                         "ORDER BY A.ITEMID ASC", STATUS, SUPPLIERID);
            return query;
        }

        public static String SELECT_ITEM_FOR_SO(String STATUS)
        {
            /*
                SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', 
                A.ITEMID, ITEMNAME, ITEMID_ALIAS, 
                (
	                SELECT ITEMNAME 
	                FROM M_ITEM
	                WHERE ITEMID = A.ITEMID_ALIAS
                ) AS ITEMNAME_ALIAS,
                BRANDID, SPEC, QUANTITY, UOMID, RACKID, 
                (
                    SELECT TOP 1 PRICE 
                    FROM LOG_TRANSACTION Z
                    JOIN D_LOG_TRANSACTION Y 
                    ON Z.TRANSACTIONID = Y.TRANSACTIONID
                    WHERE ITEMID = A.ITEMID 
                    ORDER BY TRANSACTIONDATE DESC
                ) AS UNITPRICE, '0' AS AMOUNT
                FROM M_ITEM A 
                JOIN D_ITEM_SUPPLIER B
                ON A.ITEMID = B.ITEMID
                WHERE [STATUS] = 'Actived' AND SUPPLIERID = 'YS'
                ORDER BY A.ITEMID ASC
             */
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', " +
                                         "A.ITEMID, ITEMNAME, ITEMID_ALIAS, " +
                                         "( " +
                                         "   SELECT ITEMNAME " +
                                         "   FROM M_ITEM " +
                                         "   WHERE ITEMID = A.ITEMID_ALIAS " +
                                         ") AS ITEMNAME_ALIAS, " +
                                         "( " +
                                         "   SELECT QUANTITY " +
                                         "   FROM M_ITEM " +
                                         "   WHERE ITEMID = A.ITEMID_ALIAS " +
                                         ") AS STOCK, " +
                                         "BRANDID, SPEC, QUANTITY, " +
                                         "UOMID, RACKID, " +
                                         "( "+
	                                     "  SELECT TOP 1 SELLPRICE " +
                                         "  FROM LOG_TRANSACTION " +
                                         "  WHERE ITEMID = A.ITEMID " +
                                         "  ORDER BY TRANSACTIONDATE DESC " +
                                         ") AS UNITPRICE, '0' AS AMOUNT " +
                                         "FROM M_ITEM A " +
                                         "WHERE [STATUS] = '{0}' " +
                                         "ORDER BY A.ITEMID ASC", STATUS);
            return query;
        }

        public static String SELECT_ITEM_FOR_SO(String STATUS, String FIND)
        {
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', " +
                                         "A.ITEMID, ITEMNAME, ITEMID_ALIAS, " +
                                         "( " +
                                         "   SELECT ITEMNAME " +
                                         "   FROM M_ITEM " +
                                         "   WHERE ITEMID = A.ITEMID_ALIAS " +
                                         ") AS ITEMNAME_ALIAS, " +
                                         "( " +
                                         "   SELECT QUANTITY " +
                                         "   FROM M_ITEM " +
                                         "   WHERE ITEMID = A.ITEMID_ALIAS " +
                                         ") AS STOCK, " +
                                         "BRANDID, SPEC, QUANTITY, " +
                                         "UOMID, RACKID, " +
                                         "( " +
                                         "  SELECT TOP 1 SELLPRICE " +
                                         "  FROM LOG_TRANSACTION " +
                                         "  WHERE ITEMID = A.ITEMID " +
                                         "  ORDER BY TRANSACTIONDATE DESC " +
                                         ") AS UNITPRICE, '0' AS AMOUNT " +
                                         "FROM M_ITEM A " +
                                         "WHERE {1} [STATUS] = '{0}'" +
                                         "ORDER BY A.ITEMID ASC", STATUS, FIND);
            return query;
        }

        public static String SELECT_ITEM_FOR_INVOICE(String Status, String INVOICEID) {
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMID, B.ITEMNAME, " +
                                         "A.ITEMIDQTY AS ITEMID_ALIAS,  " +
                                         "( " +
                                         "   SELECT ITEMNAME FROM M_ITEM " +
                                         "   WHERE ITEMID = A.ITEMID " +
                                         ") AS ITEMNAMEQTY, " +
                                         "A.QUANTITY,A.QUANTITY AS RETURNQUANTITY, UOMID, PRICE AS UNITPRICE, '0' AS AMOUNT " +
                                         "FROM D_INVOICE A " +
                                         "JOIN M_ITEM B " +
                                         "ON A.ITEMID = B.ITEMID " +
                                         "WHERE INVOICEID = '{0}'", INVOICEID);
            return query;
        }

        public static String SELECT_ITEM_FOR_INVOICE(String Status, String INVOICEID, String FIND)
        {
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMID, B.ITEMNAME, " +
                                         "A.ITEMIDQTY AS ITEMID_ALIAS, " +
                                         "( " +
                                         "   SELECT ITEMNAME FROM M_ITEM " +
                                         "   WHERE ITEMID = A.ITEMID " +
                                         ") AS ITEMNAMEQTY, " +
                                         "A.QUANTITY,A.QUANTITY AS RETURNQUANTITY, UOMID, PRICE AS UNITPRICE, '0' AS AMOUNT " +
                                         "FROM D_INVOICE A " +
                                         "JOIN M_ITEM B " +
                                         "ON A.ITEMID = B.ITEMID " +
                                         "WHERE {0} INVOICEID = '{1}'", FIND, INVOICEID);
            return query;
        }

        public static String SELECT_ITEM_FOR_PR_WITHOUT_SUPPLIER(String STATUS, String FIND)
        {
            /*
                SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', 
                A.ITEMID, ITEMNAME, ITEMID_ALIAS, 
                (
                    SELECT ITEMNAME 
                    FROM M_ITEM
                    WHERE ITEMID = A.ITEMID_ALIAS
                ) AS ITEMNAME_ALIAS,
                BRANDID, SPEC, QUANTITY, UOMID, RACKID, 
                (
                    SELECT TOP 1 PRICE 
                    FROM LOG_TRANSACTION Z
                    JOIN D_LOG_TRANSACTION Y 
                    ON Z.TRANSACTIONID = Y.TRANSACTIONID
                    WHERE ITEMID = A.ITEMID 
                    ORDER BY TRANSACTIONDATE DESC
                ) AS UNITPRICE, '0' AS AMOUNT
                FROM M_ITEM A 
                JOIN D_ITEM_SUPPLIER B
                ON A.ITEMID = B.ITEMID
                WHERE [STATUS] = 'Actived' AND SUPPLIERID = 'YS'
                ORDER BY A.ITEMID ASC
             */
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', " +
                                         "A.ITEMID, ITEMNAME, ITEMID_ALIAS, " +
                                         "( " +
                                         "   SELECT ITEMNAME " +
                                         "   FROM M_ITEM " +
                                         "   WHERE ITEMID = A.ITEMID_ALIAS " +
                                         ") AS ITEMNAME_ALIAS, " +
                                         "BRANDID, SPEC, QUANTITY, UOMID, RACKID, " +
                                         "(" +
                                         "    SELECT TOP 1 BUYPRICE " +
                                         "    FROM LOG_TRANSACTION " +
                                         "    WHERE ITEMID = A.ITEMID " +
                                         "    ORDER BY TRANSACTIONDATE DESC " +
                                         ") AS UNITPRICE, '0' AS AMOUNT " +
                                         "FROM M_ITEM A " +
                                         "JOIN D_ITEM_SUPPLIER B " +
                                         "ON A.ITEMID = B.ITEMID " +
                                         "WHERE {0} [STATUS] = '{1}' " +
                                         "UNION " +
                                         "SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMID, ITEMNAME, ITEMID_ALIAS, " +
                                         "( " +
                                         "   SELECT ITEMNAME " +
                                         "   FROM M_ITEM " +
                                         "   WHERE ITEMID = A.ITEMID_ALIAS " +
                                         ") AS ITEMNAME_ALIAS, " +
                                         "BRANDID, SPEC, QUANTITY, UOMID, RACKID, '0' AS UNITPRICE, '0' AS AMOUNT " +
                                         "FROM M_ITEM A " +
                                         "WHERE  {0} NOT EXISTS (SELECT ITEMID FROM D_ITEM_SUPPLIER WHERE ITEMID = A.ITEMID) " +
                                         "AND [STATUS] = '{1}' " +
                                         "ORDER BY A.ITEMID ASC", FIND, STATUS);

            return query;
        }

        public static String SELECT_ITEM_FOR_PRETURN(String STATUS, String FIND) {
            String query = String.Format("SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMID, ITEMNAME, ITEMID_ALIAS, " +
                                         "( " +
                                         "  SELECT ITEMNAME " +
                                         "  FROM M_ITEM " +
                                         "  WHERE ITEMID = A.ITEMID_ALIAS " +
                                         ") AS ITEMNAME_ALIAS, " +
                                         "BRANDID, SPEC, A.QUANTITY, UOMID, RACKID," +
                                         "( " +
                                         "  SELECT TOP 1 BUYPRICE " +
                                         "  FROM LOG_TRANSACTION " +
                                         "  WHERE ITEMID = A.ITEMID " +
                                         "  ORDER BY TRANSACTIONDATE DESC " +
                                         ") AS UNITPRICE, '0' AS AMOUNT " +
                                         "FROM D_PURCHASE B JOIN " +
                                         "M_ITEM A " +
                                         "ON A.ITEMID = B.ITEMID " +
                                         "WHERE {1} " +
                                         "(PURCHASEID NOT IN " +
                                         "( " +
                                         "   SELECT PURCHASEID FROM H_RETURN_PURCHASE C " +
                                         "   JOIN D_RETURN_PURCHASE D " +
                                         "   ON C.RETURNPURCHASEID = D.RETURNPURCHASEID " +
                                         "   WHERE STATUS = '{0}' " +
                                         ") " +
                                         "OR A.ITEMID NOT IN " +
                                         "( " +
                                         "   SELECT ITEMID FROM H_RETURN_PURCHASE E " +
                                         "   JOIN D_RETURN_PURCHASE F " +
                                         "   ON E.RETURNPURCHASEID = F.RETURNPURCHASEID " +
                                         "   WHERE STATUS = '{0}' " +
                                         "))", STATUS, FIND);
            Console.WriteLine(query);
            return query;
        }

        public static String SELECT_ITEM_FOR_BUILD_ITEM(String STATUS, String FIND) {
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', " +
                                         "A.ITEMID, ITEMNAME,ITEMID_ALIAS AS ITEMIDQTY, " +
                                         "( " +
                                         "    SELECT ITEMNAME " +
                                         "    FROM M_ITEM " +
                                         "    WHERE ITEMID = A.ITEMID_ALIAS " +
                                         ") AS ITEMNAMEQTY, " +
                                         "BRANDID, SPEC, '1' AS COMBINEQTY, " +
                                         "( " +
                                         "    SELECT QUANTITY " +
                                         "    FROM M_ITEM " +
                                         "    WHERE ITEMID = A.ITEMID_ALIAS " +
                                         ") AS ITEMALIASQUANTITY, QUANTITY, UOMID " +
                                         "FROM M_ITEM A " +
                                         "WHERE {0} [STATUS] = '{1}'", FIND, STATUS);
            return query;
        }

        public static string SELECT_ITEM_FOR_REVISION(String PurchaseID,String STATUS, String FIND)
        {
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMID, A.ITEMNAME, A.ITEMID_ALIAS AS ITEMIDQTY, " +
                                         "(SELECT ITEMNAME FROM M_ITEM WHERE ITEMID = A.ITEMID_ALIAS) AS ITEMNAMEQTY, " +
                                         "A.QUANTITY AS QUANTITY, " +
                                         "ISNULL((SELECT QUANTITY FROM D_PURCHASE WHERE ITEMID = A.ITEMID AND PURCHASEID = '{0}'),0) AS QTYORDER, '0' AS QTYREVISI," +
                                         "A.UOMID, ISNULL((SELECT PRICE FROM D_PURCHASE WHERE ITEMID = A.ITEMID AND PURCHASEID = '{0}'),0) AS UNITPRICE, " +
                                         "'0' AS AMOUNT " +
                                         "FROM M_ITEM A JOIN " +
                                         "D_PURCHASE B ON A.ITEMID = B.ITEMID " +
                                         "WHERE {1} A.STATUS = '{2}' AND PURCHASEID = '{0}'", PurchaseID, FIND, STATUS);
            return query;
        }
        //public static string SELECT_ITEM_FOR_REVISIONFIND(String STATUS, String FIND)
        //{
        //    String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMID, " +
        //                                "C.ITEMNAME, C.ITEMID_ALIAS AS ITEMIDQTY, E.ITEMNAME AS ITEMNAMEQTY, A.QUANTITY AS QUANTITY, 0 AS QTYREVISI, C.UOMID, A.PRICE AS UNITPRICE, 0 AS AMOUNT " +
        //                                "FROM D_PURCHASE A JOIN H_PURCHASE B ON A.PURCHASEID = B.PURCHASEID " +
        //                                "JOIN M_ITEM C ON A.ITEMID = C.ITEMID " +
        //                                "JOIN M_BRAND D ON C.BRANDID = D.BRANDID " +
        //                                "JOIN M_ITEM E ON A.ITEMIDQTY = E.ITEMID " +
        //                                "WHERE {0} B.STATUS = '{1}' ", FIND, STATUS);
        //    return query;
        //}
    }
}
