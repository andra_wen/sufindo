﻿namespace Sufindo
{
    partial class ImportExportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Exportinglabel = new System.Windows.Forms.Label();
            this.exportingprogressBar = new System.Windows.Forms.ProgressBar();
            this.EntityExportcomboBox = new System.Windows.Forms.ComboBox();
            this.browseExportbutton = new System.Windows.Forms.Button();
            this.locationExporttextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Importinglabel = new System.Windows.Forms.Label();
            this.importprogressBar = new System.Windows.Forms.ProgressBar();
            this.EntityImportcomboBox = new System.Windows.Forms.ComboBox();
            this.browseImportButton = new System.Windows.Forms.Button();
            this.locationImptextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.groupBox2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(442, 273);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Exportinglabel);
            this.groupBox2.Controls.Add(this.exportingprogressBar);
            this.groupBox2.Controls.Add(this.EntityExportcomboBox);
            this.groupBox2.Controls.Add(this.browseExportbutton);
            this.groupBox2.Controls.Add(this.locationExporttextBox);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(3, 138);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(435, 131);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Export";
            // 
            // Exportinglabel
            // 
            this.Exportinglabel.AutoSize = true;
            this.Exportinglabel.Location = new System.Drawing.Point(10, 109);
            this.Exportinglabel.Name = "Exportinglabel";
            this.Exportinglabel.Size = new System.Drawing.Size(51, 13);
            this.Exportinglabel.TabIndex = 7;
            this.Exportinglabel.Text = "Exporting";
            // 
            // exportingprogressBar
            // 
            this.exportingprogressBar.Location = new System.Drawing.Point(10, 80);
            this.exportingprogressBar.Name = "exportingprogressBar";
            this.exportingprogressBar.Size = new System.Drawing.Size(413, 23);
            this.exportingprogressBar.Step = 1;
            this.exportingprogressBar.TabIndex = 9;
            // 
            // EntityExportcomboBox
            // 
            this.EntityExportcomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.EntityExportcomboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EntityExportcomboBox.FormattingEnabled = true;
            this.EntityExportcomboBox.Location = new System.Drawing.Point(77, 19);
            this.EntityExportcomboBox.Name = "EntityExportcomboBox";
            this.EntityExportcomboBox.Size = new System.Drawing.Size(146, 21);
            this.EntityExportcomboBox.TabIndex = 6;
            // 
            // browseExportbutton
            // 
            this.browseExportbutton.Location = new System.Drawing.Point(348, 44);
            this.browseExportbutton.Name = "browseExportbutton";
            this.browseExportbutton.Size = new System.Drawing.Size(75, 23);
            this.browseExportbutton.TabIndex = 8;
            this.browseExportbutton.Text = "Browse";
            this.browseExportbutton.UseVisualStyleBackColor = true;
            this.browseExportbutton.Click += new System.EventHandler(this.browseExportbutton_Click);
            // 
            // locationExporttextBox
            // 
            this.locationExporttextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.locationExporttextBox.Location = new System.Drawing.Point(77, 46);
            this.locationExporttextBox.Name = "locationExporttextBox";
            this.locationExporttextBox.Size = new System.Drawing.Size(265, 20);
            this.locationExporttextBox.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Entity";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Location";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Importinglabel);
            this.groupBox1.Controls.Add(this.importprogressBar);
            this.groupBox1.Controls.Add(this.EntityImportcomboBox);
            this.groupBox1.Controls.Add(this.browseImportButton);
            this.groupBox1.Controls.Add(this.locationImptextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(435, 129);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Import";
            // 
            // Importinglabel
            // 
            this.Importinglabel.AutoSize = true;
            this.Importinglabel.Location = new System.Drawing.Point(10, 104);
            this.Importinglabel.Name = "Importinglabel";
            this.Importinglabel.Size = new System.Drawing.Size(50, 13);
            this.Importinglabel.TabIndex = 6;
            this.Importinglabel.Text = "Importing";
            // 
            // importprogressBar
            // 
            this.importprogressBar.Location = new System.Drawing.Point(10, 74);
            this.importprogressBar.Name = "importprogressBar";
            this.importprogressBar.Size = new System.Drawing.Size(413, 23);
            this.importprogressBar.Step = 1;
            this.importprogressBar.TabIndex = 4;
            // 
            // EntityImportcomboBox
            // 
            this.EntityImportcomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.EntityImportcomboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EntityImportcomboBox.FormattingEnabled = true;
            this.EntityImportcomboBox.Location = new System.Drawing.Point(77, 13);
            this.EntityImportcomboBox.Name = "EntityImportcomboBox";
            this.EntityImportcomboBox.Size = new System.Drawing.Size(146, 21);
            this.EntityImportcomboBox.TabIndex = 1;
            // 
            // browseImportButton
            // 
            this.browseImportButton.Location = new System.Drawing.Point(348, 38);
            this.browseImportButton.Name = "browseImportButton";
            this.browseImportButton.Size = new System.Drawing.Size(75, 23);
            this.browseImportButton.TabIndex = 3;
            this.browseImportButton.Text = "Browse";
            this.browseImportButton.UseVisualStyleBackColor = true;
            this.browseImportButton.Click += new System.EventHandler(this.BrowseImport_Click);
            // 
            // locationImptextBox
            // 
            this.locationImptextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.locationImptextBox.Location = new System.Drawing.Point(77, 40);
            this.locationImptextBox.Name = "locationImptextBox";
            this.locationImptextBox.Size = new System.Drawing.Size(265, 20);
            this.locationImptextBox.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Entity";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Location";
            // 
            // ImportExportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(447, 278);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ImportExportForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Import/Export";
            this.Activated += new System.EventHandler(this.ImportExportForm_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ImportExportForm_FormClosed);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ProgressBar importprogressBar;
        private System.Windows.Forms.ComboBox EntityImportcomboBox;
        private System.Windows.Forms.Button browseImportButton;
        private System.Windows.Forms.TextBox locationImptextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar exportingprogressBar;
        private System.Windows.Forms.ComboBox EntityExportcomboBox;
        private System.Windows.Forms.Button browseExportbutton;
        private System.Windows.Forms.TextBox locationExporttextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Importinglabel;
        private System.Windows.Forms.Label Exportinglabel;
    }
}