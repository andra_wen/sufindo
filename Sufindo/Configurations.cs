﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Windows.Forms;
using System.Data;

namespace Sufindo
{
    class Configurations
    {

        public Configurations()
        {
            try
            {
                XmlDocument xmlConfigurations = new XmlDocument();
                
                xmlConfigurations.Load(Application.StartupPath + "/Configurations.xml");
                XmlElement root = xmlConfigurations.DocumentElement;
                
                foreach (XmlNode parentNode in root.ChildNodes)
                {
                    if (parentNode.Attributes["name"].Value.ToString().Equals("connectionStrings"))
                    {
                        connectionStrings = parentNode.SelectSingleNode("connectionStrings").InnerText;
                        connectionStrings = String.Format(connectionStrings, "sufindo", "sufindo");
                        break;
                    }
                }
                
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("[CONFIGURATION01]: Please Check Your Configuration xml "+ex.Message);
            }
        }
        
        public String connectionStrings { set; get; }

        public string[] getColumnExcel(String Sheet) {
            try
            {
                List<String> column = new List<String>();
                XmlDocument xmlConfigurations = new XmlDocument();

                xmlConfigurations.Load(Application.StartupPath + "/UtilitiesExcel.xml");
                XmlElement root = xmlConfigurations.DocumentElement;
                foreach (XmlNode parentNode in root.ChildNodes)
                {
                    if (parentNode.Attributes["name"].Value.ToString().Equals(Sheet))
                    {
                        for (int i = 0; i < parentNode.ChildNodes.Count; i++)
                        {
                            column.Add(parentNode.ChildNodes[i].FirstChild.InnerText);
                        }
                    }
                }
                return column.ToArray();
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("[CONFIGURATION01]: Please Check Your Configuration xml " + ex.Message);
                return null;
            }
           
        }

        public DataTable getDisplayValueExcel() {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Displaymember", typeof(string));
                dt.Columns.Add("Valuemember", typeof(string));

                List<String> column = new List<String>();
                XmlDocument xmlConfigurations = new XmlDocument();

                xmlConfigurations.Load(Application.StartupPath + "/UtilitiesExcel.xml");
                XmlElement root = xmlConfigurations.DocumentElement;
                foreach (XmlNode parentNode in root.ChildNodes)
                {
                    dt.Rows.Add(parentNode.Attributes["display"].Value.ToString(),parentNode.Attributes["name"].Value.ToString());
                    //Console.WriteLine("Display {0}", parentNode.Attributes["name"].Value.ToString());
                    //Console.WriteLine("Value {0}", parentNode.Attributes["value"].Value.ToString());
                    
                }
                return dt;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("[CONFIGURATION01]: Please Check Your Configuration xml " + ex.Message);
                return null;
            }
        }

    }
}
