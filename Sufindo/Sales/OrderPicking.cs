﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Threading;

using Sufindo.Report.Sales;

namespace Sufindo.Sales
{
    public partial class OrderPicking : Form
    {
        private delegate void fillAutoCompleteTextBox();

        SalesOrderdatagridview salesorderdatagridview;
        OrderPickingdatagridview orderpickingdatagridview;
        MenuStrip menustripAction;
        Itemdatagridview itemdatagridview;
        Connection connection;
        DataTable datatable;
        List<String> SalesOrderNo;
        List<String> OrderPickingNo;
        List<Control> controlTextBox;

        Boolean addNewItem = false;
        Boolean isFind = false;

        public OrderPicking()
        {
            InitializeComponent();
        }

        public OrderPicking(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            this.menustripAction = menustripAction;
            itemdatagridview = null;
            salesorderdatagridview = null;
            if (connection == null)
            {
                connection = new Connection();
            }
            controlTextBox = new List<Control>();
            foreach (Control control in this.tableLayoutPanel.Controls)
            {
                if (control is TextBox)
                {
                    controlTextBox.Add(control);
                }
                else if (control is Panel)
                {
                    foreach (Control controlPanel in control.Controls)
                    {
                        if (controlPanel is RichTextBox)
                        {
                            controlTextBox.Add(controlPanel);
                        }
                    }
                }
                else if (control is ComboBox)
                {
                    controlTextBox.Add(control);
                }
            }

            foreach (Control txtBox in controlTextBox)
            {
                if (txtBox is RichTextBox)
                {
                    RichTextBox richtextBox = (RichTextBox)txtBox;
                    richtextBox.ReadOnly = true;
                    richtextBox.BackColor = SystemColors.Info;
                    richtextBox.Text = "";
                }
                else
                {
                    txtBox.Enabled = false;
                    txtBox.Text = "";
                }
                txtBox.BackColor = SystemColors.Info;
            }
            disableDataCustomer();
            clearDataCustomer();
            datatable = new DataTable();
            neworderpicking();
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }

        void bgWorker_DoWorkInsert(object sender, DoWorkEventArgs e)
        {
            try
            {
                Object[] obj = e.Argument as Object[];

                String SALESORDERID = obj[0].ToString();
                String ORDERPICKINGID = obj[1].ToString();
                String query = String.Format("SELECT ITEMID, ITEMIDQTY, QUANTITY, PRICE FROM D_SALES_ORDER WHERE SALESORDERID = {0}", SALESORDERID);
                DataTable dt = connection.openDataTableQuery(query);

                List<string> values = new List<string>();
                foreach (DataRow row in dt.Rows)
                {
                    String value = String.Format("SELECT '{0}', '{1}', '{2}', {3}, {4}", ORDERPICKINGID, row["ITEMID"].ToString(),
                                                                                  row["ITEMIDQTY"].ToString(),
                                                                                  row["QUANTITY"].ToString(), row["PRICE"].ToString());
                    values.Add(value);
                }
                String[] columns = { "ORDERPICKINGID", "ITEMID", "ITEMIDQTY", "QUANTITY", "PRICE" };
                if (connection.openReaderQuery(Utilities.queryMultipleInsert("D_ORDER_PICKING", columns, values)))
                {
                    
                    //MessageBox.Show("SUCCESS");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        void bgWorker_RunWorkerInsertCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        public void neworderpicking() {
            reloadAllData();
            Utilities.clearAllField(ref controlTextBox);
            datatable.Clear();
            SONotextBox.Enabled = true;
            OPNOtextBox.Enabled = false;
            OPNOtextBox.Text = DateTime.Now.ToString("yyyyMM") + "XXXX";
            OPDatetextBox.Text = DateTime.Now.ToString("dd/MM/yyyy");
            OPStatustextBox.Text = Status.OPEN;
            SearchOPNObutton.Visible = false;
            SearchSONObutton.Visible = true;
            isFind = false;
        }

        public void print() {
            DialogResult result = MessageBox.Show("Pastikan Anda print setelah preview agar Order Picking No tidak duplikasi", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            if (result == DialogResult.No) {
                return;
            }

            if (!isFind && !SODatetextBox.Text.Equals(""))
            {
                try
                {
                    if (checkStockQuantity())
                    {
                        List<SqlParameter> sqlParam = new List<SqlParameter>();

                        sqlParam.Add(new SqlParameter("@SALESORDERID", SONotextBox.Text));
                        sqlParam.Add(new SqlParameter("@STATUS", Status.CLOSE));
                        sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));

                        DataTable dt = connection.callProcedureDatatable("INSERT_DATA_H_ORDER_PICKING", sqlParam);
                        if (dt.Rows.Count == 1)
                        {
                            String ORDERPICKINGID = dt.Rows[0]["ORDERPICKINGID"].ToString();
                            BackgroundWorker bgWorkerInsert = new BackgroundWorker();
                            bgWorkerInsert.DoWork += new DoWorkEventHandler(bgWorker_DoWorkInsert);
                            bgWorkerInsert.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerInsertCompleted);
                            Object[] obj = { SONotextBox.Text, ORDERPICKINGID };
                            bgWorkerInsert.RunWorkerAsync(obj);
                            OrderPickingReportForm orderpickingreportForm = new OrderPickingReportForm(ORDERPICKINGID, true);
                            orderpickingreportForm.clearOrderPickingData = new OrderPickingReportForm.ClearOrderPickingData(ClearOrderPickingData);
                            orderpickingreportForm.ShowDialog();
                        }
                    }
                    else {
                        MessageBox.Show("Stock tidak mencukupi untuk Order Quantity");
                    }
                }
                catch (Exception)
                {

                }
            }
        }

        private Boolean checkStockQuantity() {

            for (int i = 0; i < MasterItemdataGridView.Rows.Count; i++)
            {
                Double stock = Double.Parse(MasterItemdataGridView.Rows[i].Cells["STOCK"].Value.ToString());
                Double quantity = Double.Parse(MasterItemdataGridView.Rows[i].Cells["QUANTITY"].Value.ToString());
                if (quantity > stock) {
                    return false;
                }               
            }

            return true;
        }

        void ClearOrderPickingData() {
            neworderpicking();
        }

        public void close() {
            if (isFind) {
                if (OPStatustextBox.Text.Equals(Status.CANCEL) || OPStatustextBox.Text.Equals(Status.OPEN))
                {
                    DialogResult result = MessageBox.Show(String.Format("Apakah Anda Yakin Close Order Picking {0} ?", OPNOtextBox.Text), "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (result == DialogResult.Yes)
                    {
                        List<SqlParameter> sqlParam = new List<SqlParameter>();
                        sqlParam.Add(new SqlParameter("@ORDERPICKINGID", OPNOtextBox.Text));
                        sqlParam.Add(new SqlParameter("@STATUS", Status.CLOSE));
                        if (connection.callProcedure("UPDATE_STATUS_H_ORDER_PICKING", sqlParam))
                        {
                            if (OPStatustextBox.Text.Equals(Status.CANCEL))
                            {
                                sqlParam = new List<SqlParameter>();
                                sqlParam.Add(new SqlParameter("@ORDERPICKINGID", OPNOtextBox.Text));
                                if (connection.callProcedure("INSERT_LOG_TRANSACTION_ORDER_PICKING", sqlParam))
                                {
                                    find();
                                }
                            }
                            else if (OPStatustextBox.Text.Equals(Status.OPEN))
                            {
                                sqlParam = new List<SqlParameter>();
                                sqlParam.Add(new SqlParameter("@SALESORDERID", SONotextBox.Text));
                                sqlParam.Add(new SqlParameter("@STATUS", Status.READY));
                                if (connection.callProcedure("UPDATE_STATUS_H_SALES_ORDER", sqlParam))
                                {
                                    sqlParam = new List<SqlParameter>();
                                    sqlParam.Add(new SqlParameter("@ORDERPICKINGID", OPNOtextBox.Text));
                                    if (connection.callProcedure("INSERT_LOG_TRANSACTION_ORDER_PICKING", sqlParam))
                                    {
                                        find();
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show(String.Format("Status Order Picking No {0} Must Cancel or Open", OPNOtextBox.Text));
                }
            }

        }

        public void find() {
            reloadAllData();
            isFind = true;
            OPNOtextBox.Enabled = true;
            Utilities.clearAllField(ref controlTextBox);
            datatable.Clear();
            clearDataCustomer();
            disableDataCustomer();
            SONotextBox.Enabled = false;
            OPNOtextBox.Text = "";
            OPDatetextBox.Text = "";
            OPStatustextBox.Text = "";
            SearchOPNObutton.Visible = true;
            SearchSONObutton.Visible = false;
        }

        private void disableDataCustomer(){
            NameCustomertextBox.Enabled = false;
        }

        private void clearDataCustomer(){
            CustomerIDtextBox.Text = "";
            NameCustomertextBox.Text = "";
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e){

        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e){
            try
            {
                Thread.Sleep(100);

                if (SalesOrderNo == null) SalesOrderNo = new List<string>();
                else SalesOrderNo.Clear();

                if (OrderPickingNo == null) OrderPickingNo = new List<string>();
                else OrderPickingNo.Clear();

                SqlDataReader sqldataReader = connection.sqlDataReaders(String.Format("SELECT SALESORDERID FROM H_SALES_ORDER WHERE [STATUS] = '{0}'", Status.READY));

                while (sqldataReader.Read()) SalesOrderNo.Add(sqldataReader.GetString(0));

                sqldataReader = sqldataReader = connection.sqlDataReaders(String.Format("SELECT ORDERPICKINGID FROM H_ORDER_PICKING WHERE [STATUS] = '{0}' OR [STATUS] = '{0}'", Status.OPEN, Status.CANCEL));

                while (sqldataReader.Read()) OrderPickingNo.Add(sqldataReader.GetString(0));

                if (IsHandleCreated) BeginInvoke(new fillAutoCompleteTextBox(this.fillAutoComplete));
            }
            catch (Exception ex)
            {
                
            }
        }

        private void fillAutoComplete(){
            SONotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            SONotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            SONotextBox.AutoCompleteCustomSource.Clear();
            SONotextBox.AutoCompleteCustomSource.AddRange(SalesOrderNo.ToArray());

            OPNOtextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            OPNOtextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            OPNOtextBox.AutoCompleteCustomSource.Clear();
            OPNOtextBox.AutoCompleteCustomSource.AddRange(OrderPickingNo.ToArray());
        }

        private void MasterCustomerPassingData(DataTable sender){
            CustomerIDtextBox.Text = sender.Rows[0]["CUSTOMERID"].ToString();
            NameCustomertextBox.Text = sender.Rows[0]["CUSTOMERNAME"].ToString();
        }

        private void MasterItemPassingData(DataTable datatable){

            if (this.datatable == null) this.datatable = new DataTable();
            else
            {
                if(datatable.Rows.Count == 0)
                this.datatable.Clear();
            }

            /*remove column not use*/
            List<string> columnsRemove = new List<string>();

            foreach (DataColumn columns in datatable.Columns)
            {
                if (columns.ColumnName.Equals("ITEMNAME_ALIAS"))
                {
                    columns.ColumnName = "ITEMNAMEQTY";
                }
                else if (columns.ColumnName.Equals("ITEMID_ALIAS"))
                {
                    columns.ColumnName = "ITEMIDQTY";
                }
            }
            foreach (DataColumn column in datatable.Columns)
            {
                Boolean isExistingColumn = false;
                foreach (DataGridViewColumn columngrid in MasterItemdataGridView.Columns)
                {
                    if (column.ColumnName.Equals(columngrid.DataPropertyName))
                    {
                        isExistingColumn = true;
                        break;
                    }
                }
                if (!isExistingColumn)
                {
                    columnsRemove.Add(column.ColumnName);
                }
            }
            foreach (string columnName in columnsRemove)
            {
                datatable.Columns.Remove(columnName);
            }

            if (this.datatable.Rows.Count == 0) this.datatable = datatable;
            else
            {
                foreach (DataRow row in datatable.Rows)
                {
                    this.datatable.Rows.Add(row.ItemArray);
                }
            }

            foreach (DataRow row in this.datatable.Rows)
            {
                Object obj = "1";
                row["QUANTITY"] = (addNewItem) ? obj : row["QUANTITY"];
                obj = "0";
                row["UNITPRICE"] = (row["UNITPRICE"].ToString().Equals("")) ? obj : row["UNITPRICE"];
                Int64 quantity = Int64.Parse(row["QUANTITY"].ToString());
                Double unitprice = Double.Parse(row["UNITPRICE"].ToString());
                Double amount = (quantity * unitprice);
                row["AMOUNT"] = (Object)amount.ToString();
            }

            Utilities.generateNoDataTable(ref this.datatable);

            MasterItemdataGridView.DataSource = this.datatable;

            Double total = 0.0;
            foreach (DataGridViewRow row in MasterItemdataGridView.Rows)
            {
                total += Double.Parse(row.Cells["AMOUNT"].Value.ToString());
            }
        }

        private void SalesOrderPassingData(DataTable sender){
            SONotextBox.Text = sender.Rows[0]["SALESORDERID"].ToString();
            SODatetextBox.Text = sender.Rows[0]["SALESORDERDATE"].ToString();
            CustomerIDtextBox.Text = sender.Rows[0]["CUSTOMERID"].ToString();
            
            SOStatustextBox.Text = sender.Rows[0]["STATUS"].ToString();
            
            RemarkrichTextBox.Text = sender.Rows[0]["REMARK"].ToString();

            String query = String.Format("SELECT CUSTOMERID, CUSTOMERNAME, CONTACTPERSON, NOHP " +
                                         "FROM M_CUSTOMER " +
                                         "WHERE CUSTOMERID = '{0}'", CustomerIDtextBox.Text);

            DataTable datatable = connection.openDataTableQuery(query);
            MasterCustomerPassingData(datatable);

            sender.Rows[0]["CREATEDBY"].ToString();
            if (isFind)
            {
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMID, B.ITEMNAME, " +
                                      "A.ITEMIDQTY,  " +
                                      "( " +
                                      "   SELECT ITEMNAME FROM M_ITEM " +
                                      "   WHERE ITEMID = A.ITEMID " +
                                      ") AS ITEMNAMEQTY, " +
                                      "( " +
                                      "  SELECT QUANTITY FROM M_ITEM " +
                                      "  WHERE ITEMID = A.ITEMIDQTY " +
                                      ") AS STOCK, " +
                                      "A.QUANTITY, UOMID, PRICE AS UNITPRICE, '0' AS AMOUNT " +
                                      "FROM D_ORDER_PICKING A " +
                                      "JOIN M_ITEM B " +
                                      "ON A.ITEMID = B.ITEMID " +
                                      "WHERE ORDERPICKINGID = '{0}' ORDER BY A.ITEMID ASC", OPNOtextBox.Text);
            }
            else
            {
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMID, B.ITEMNAME, " +
                                     "A.ITEMIDQTY,  " +
                                     "( " +
                                     "   SELECT ITEMNAME FROM M_ITEM " +
                                     "   WHERE ITEMID = A.ITEMID " +
                                     ") AS ITEMNAMEQTY, " +
                                     "( " +
                                     "  SELECT QUANTITY FROM M_ITEM " +
                                     "  WHERE ITEMID = A.ITEMIDQTY " +
                                     ") AS STOCK, " +
                                     "A.QUANTITY, UOMID, PRICE AS UNITPRICE, '0' AS AMOUNT " +
                                     "FROM D_SALES_ORDER A " +
                                     "JOIN M_ITEM B " +
                                     "ON A.ITEMID = B.ITEMID " +
                                     "WHERE SALESORDERID = '{0}' ORDER BY A.ITEMID ASC", SONotextBox.Text);
            }
            datatable = connection.openDataTableQuery(query);
            
            this.datatable.Clear();
            MasterItemPassingData(datatable);
            
        }

        private void SearchPONObutton_Click(object sender, EventArgs e)
        {
            if (salesorderdatagridview == null)
            {
                salesorderdatagridview = new SalesOrderdatagridview("OrderPicking");
                salesorderdatagridview.salesOrderPassingData = new SalesOrderdatagridview.SalesOrderPassingData(SalesOrderPassingData);
                salesorderdatagridview.ShowDialog();
            }
            else if (salesorderdatagridview.IsDisposed)
            {
                salesorderdatagridview = new SalesOrderdatagridview("OrderPicking");
                salesorderdatagridview.salesOrderPassingData = new SalesOrderdatagridview.SalesOrderPassingData(SalesOrderPassingData);
                salesorderdatagridview.ShowDialog();
            }
        }

        private void SONotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingSONo = false;
                this.datatable.Clear();
                foreach (String item in SalesOrderNo)
                {
                    if (item.ToUpper().Equals(SONotextBox.Text.ToUpper()))
                    {
                        isExistingSONo = true;
                        SONotextBox.Text = SONotextBox.Text.ToUpper();
                        
                        String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SALESORDERID) AS 'NO', SALESORDERID, " +
                                                     "CONVERT(VARCHAR(10), SALESORDERDATE, 103) AS [SALESORDERDATE], " +
                                                     "CUSTOMERID, CURRENCYID, RATE, PAYMENTMETHOD, DISCOUNT, REMARK, STATUS, CREATEDBY " +
                                                     "FROM H_SALES_ORDER " +
                                                     "WHERE SALESORDERID = '{0}' " +
                                                     "ORDER BY SALESORDERID ASC", SONotextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        SalesOrderPassingData(datatable);
                        break;
                    }
                }

                if (!isExistingSONo)
                {
                    SONotextBox.Text = "";
                    Utilities.clearAllField(ref controlTextBox);
                    this.datatable.Clear();
                    
                    clearDataCustomer();
                    OPNOtextBox.Text = DateTime.Now.ToString("yyyyMM") + "XXXX";
                    OPDatetextBox.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    OPStatustextBox.Text = Status.OPEN;
                }
            }
        }

        private void SONotextBox_Leave(object sender, EventArgs e)
        {
            if (SONotextBox.Text.Equals(""))
            {
                return;
            }

            Boolean isExistingPartNo = false;
            foreach (String item in SalesOrderNo)
            {
                if (item.ToUpper().Equals(SONotextBox.Text.ToUpper()))
                {
                    isExistingPartNo = true;
                    
                    String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SALESORDERID) AS 'NO', SALESORDERID, " +
                                                 "CONVERT(VARCHAR(10), SALESORDERDATE, 103) AS [SALESORDERDATE], " +
                                                 "CUSTOMERID, CURRENCYID, RATE, PAYMENTMETHOD, DISCOUNT, REMARK, STATUS, CREATEDBY " +
                                                 "FROM H_SALES_ORDER " +
                                                 "WHERE SALESORDERID = '{0}' " +
                                                 "ORDER BY SALESORDERID ASC", SONotextBox.Text);

                    DataTable datatable = connection.openDataTableQuery(query);
                    SalesOrderPassingData(datatable);
                    break;
                }
            }
            if (!isExistingPartNo)
            {
                SONotextBox.Text = "";
                Utilities.clearAllField(ref controlTextBox);
                this.datatable.Clear();
                
                clearDataCustomer();
                OPNOtextBox.Text = DateTime.Now.ToString("yyyyMM") + "XXXX";
                OPDatetextBox.Text = DateTime.Now.ToString("dd/MM/yyyy");
                OPStatustextBox.Text = Status.OPEN;
            }
        }

        private void OrderPicking_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            reloadAllData();
            Main.ACTIVEFORM = this;
        }

        private void OrderPicking_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }

        private void OPNOtextBox_Leave(object sender, EventArgs e)
        {
            if (isFind)
            {

                Boolean isExistingOP = false;
                foreach (String item in OrderPickingNo)
                {
                    if (item.ToUpper().Equals(OPNOtextBox.Text.ToUpper()))
                    {
                        isExistingOP = true;

                        String query = String.Format("SELECT ORDERPICKINGID, CONVERT(VARCHAR(10), ORDERPICKINGDATE, 103) AS [ORDERPICKINGDATE], " + 
                                                     "SALESORDERID, [STATUS], CREATEDBY " +
                                                     "FROM H_ORDER_PICKING " +
                                                     "WHERE ORDERPICKINGID = '{0}'", OPNOtextBox.Text);
                        DataTable datatable = connection.openDataTableQuery(query);
                        OrderPickingPassingData(datatable);
                        break;
                    }
                }
                if (!isExistingOP)
                {
                    OPNOtextBox.Text = "";
                    Utilities.clearAllField(ref controlTextBox);
                    this.datatable.Clear();
                    clearDataCustomer();
                }
            }
        }

        private void OPNOtextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingOPNo = false;
                this.datatable.Clear();
                foreach (String item in OrderPickingNo)
                {
                    if (item.ToUpper().Equals(OPNOtextBox.Text.ToUpper()))
                    {
                        isExistingOPNo = true;
                        OPNOtextBox.Text = OPNOtextBox.Text.ToUpper();

                        String query = String.Format("SELECT ORDERPICKINGID, CONVERT(VARCHAR(10), ORDERPICKINGDATE, 103) AS [ORDERPICKINGDATE], " +
                                                     "SALESORDERID, [STATUS], CREATEDBY " +
                                                     "FROM H_ORDER_PICKING " +
                                                     "WHERE ORDERPICKINGID = '{0}'", OPNOtextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        OrderPickingPassingData(datatable);
                        break;
                    }
                }

                if (!isExistingOPNo)
                {
                    OPNOtextBox.Text = "";
                    Utilities.clearAllField(ref controlTextBox);
                    this.datatable.Clear();
                    clearDataCustomer();
                }
            }
        }

        private void SearchOPNObutton_Click(object sender, EventArgs e)
        {
            if (orderpickingdatagridview == null)
            {
                orderpickingdatagridview = new OrderPickingdatagridview("OrderPicking");
                orderpickingdatagridview.salesOrderPassingData = new OrderPickingdatagridview.OrderPickingPassingData(OrderPickingPassingData);
                orderpickingdatagridview.ShowDialog();
            }
            else if (orderpickingdatagridview.IsDisposed)
            {
                orderpickingdatagridview = new OrderPickingdatagridview("OrderPicking");
                orderpickingdatagridview.salesOrderPassingData = new OrderPickingdatagridview.OrderPickingPassingData(OrderPickingPassingData);
                orderpickingdatagridview.ShowDialog();
            }
        }

        private void OrderPickingPassingData(DataTable sender) {
            OPNOtextBox.Text = sender.Rows[0]["ORDERPICKINGID"].ToString();
            OPDatetextBox.Text = sender.Rows[0]["ORDERPICKINGDATE"].ToString();
            OPStatustextBox.Text = sender.Rows[0]["STATUS"].ToString();
            SONotextBox.Text = sender.Rows[0]["SALESORDERID"].ToString();
            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SALESORDERID) AS 'NO', SALESORDERID, " +
                                         "CONVERT(VARCHAR(10), SALESORDERDATE, 103) AS [SALESORDERDATE], " +
                                         "CUSTOMERID, CURRENCYID, RATE, PAYMENTMETHOD, DISCOUNT, REMARK, STATUS, CREATEDBY " +
                                         "FROM H_SALES_ORDER " +
                                         "WHERE SALESORDERID = '{0}' " +
                                         "ORDER BY SALESORDERID ASC", SONotextBox.Text);

            DataTable datatable = connection.openDataTableQuery(query);
            SalesOrderPassingData(datatable);
        }

        private void OutStandingbutton_Click(object sender, EventArgs e)
        {

        }
    }
}
