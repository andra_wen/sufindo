﻿namespace Sufindo.Sales
{
    partial class Cost
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.CustomerNametextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.CustomerIDtextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SalesIDtextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.InvoiceDatetextBox = new System.Windows.Forms.TextBox();
            this.InvoiceNotextBox = new System.Windows.Forms.TextBox();
            this.SearchCustomerIDbutton = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.SalesNametextBox = new System.Windows.Forms.TextBox();
            this.SearchSalesIDbutton = new System.Windows.Forms.Button();
            this.SearchInvoiceNObutton = new System.Windows.Forms.Button();
            this.TotalInvoicetextBox = new CustomControls.TextBoxSL();
            this.CosttextBox = new CustomControls.TextBoxSL();
            this.GrandTotaltextBox = new CustomControls.TextBoxSL();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 6;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Controls.Add(this.label6, 0, 5);
            this.tableLayoutPanel.Controls.Add(this.label3, 0, 4);
            this.tableLayoutPanel.Controls.Add(this.label1, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.CustomerNametextBox, 5, 2);
            this.tableLayoutPanel.Controls.Add(this.label5, 4, 2);
            this.tableLayoutPanel.Controls.Add(this.label8, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.CustomerIDtextBox, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.SalesIDtextBox, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.label7, 4, 0);
            this.tableLayoutPanel.Controls.Add(this.InvoiceDatetextBox, 5, 0);
            this.tableLayoutPanel.Controls.Add(this.InvoiceNotextBox, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.SearchCustomerIDbutton, 2, 2);
            this.tableLayoutPanel.Controls.Add(this.label9, 4, 1);
            this.tableLayoutPanel.Controls.Add(this.SalesNametextBox, 5, 1);
            this.tableLayoutPanel.Controls.Add(this.SearchSalesIDbutton, 2, 1);
            this.tableLayoutPanel.Controls.Add(this.SearchInvoiceNObutton, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.TotalInvoicetextBox, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.CosttextBox, 1, 4);
            this.tableLayoutPanel.Controls.Add(this.GrandTotaltextBox, 1, 5);
            this.tableLayoutPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.tableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 6;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(592, 187);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 146);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 24);
            this.label6.TabIndex = 43;
            this.label6.Text = "Grand Total";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 24);
            this.label3.TabIndex = 41;
            this.label3.Text = "Cost";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 24);
            this.label1.TabIndex = 39;
            this.label1.Text = "Total Invoice";
            // 
            // CustomerNametextBox
            // 
            this.CustomerNametextBox.BackColor = System.Drawing.Color.White;
            this.CustomerNametextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CustomerNametextBox.Location = new System.Drawing.Point(434, 59);
            this.CustomerNametextBox.Name = "CustomerNametextBox";
            this.CustomerNametextBox.Size = new System.Drawing.Size(140, 20);
            this.CustomerNametextBox.TabIndex = 8;
            this.CustomerNametextBox.Tag = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(304, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(124, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "Customer Name";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 20);
            this.label8.TabIndex = 36;
            this.label8.Text = "Invoice No";
            // 
            // CustomerIDtextBox
            // 
            this.CustomerIDtextBox.BackColor = System.Drawing.Color.White;
            this.CustomerIDtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CustomerIDtextBox.Enabled = false;
            this.CustomerIDtextBox.Location = new System.Drawing.Point(125, 59);
            this.CustomerIDtextBox.Name = "CustomerIDtextBox";
            this.CustomerIDtextBox.Size = new System.Drawing.Size(143, 20);
            this.CustomerIDtextBox.TabIndex = 5;
            this.CustomerIDtextBox.Tag = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 20);
            this.label4.TabIndex = 28;
            this.label4.Text = "Customer ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Sales ID";
            // 
            // SalesIDtextBox
            // 
            this.SalesIDtextBox.BackColor = System.Drawing.Color.White;
            this.SalesIDtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SalesIDtextBox.Enabled = false;
            this.SalesIDtextBox.Location = new System.Drawing.Point(125, 31);
            this.SalesIDtextBox.Name = "SalesIDtextBox";
            this.SalesIDtextBox.Size = new System.Drawing.Size(143, 20);
            this.SalesIDtextBox.TabIndex = 3;
            this.SalesIDtextBox.Tag = "";
            this.SalesIDtextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SONotextBox_KeyDown);
            this.SalesIDtextBox.Leave += new System.EventHandler(this.SONotextBox_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(304, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 20);
            this.label7.TabIndex = 33;
            this.label7.Text = "Invoice Date";
            // 
            // InvoiceDatetextBox
            // 
            this.InvoiceDatetextBox.BackColor = System.Drawing.Color.White;
            this.InvoiceDatetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.InvoiceDatetextBox.Enabled = false;
            this.InvoiceDatetextBox.Location = new System.Drawing.Point(434, 3);
            this.InvoiceDatetextBox.Name = "InvoiceDatetextBox";
            this.InvoiceDatetextBox.Size = new System.Drawing.Size(140, 20);
            this.InvoiceDatetextBox.TabIndex = 34;
            this.InvoiceDatetextBox.Tag = "0";
            // 
            // InvoiceNotextBox
            // 
            this.InvoiceNotextBox.BackColor = System.Drawing.Color.White;
            this.InvoiceNotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.InvoiceNotextBox.Enabled = false;
            this.InvoiceNotextBox.Location = new System.Drawing.Point(125, 3);
            this.InvoiceNotextBox.Name = "InvoiceNotextBox";
            this.InvoiceNotextBox.Size = new System.Drawing.Size(143, 20);
            this.InvoiceNotextBox.TabIndex = 1;
            this.InvoiceNotextBox.Tag = "";
            this.InvoiceNotextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.InvoiceNOtextBox_KeyDown);
            this.InvoiceNotextBox.Leave += new System.EventHandler(this.InvoiceNOtextBox_Leave);
            // 
            // SearchCustomerIDbutton
            // 
            this.SearchCustomerIDbutton.BackColor = System.Drawing.Color.Transparent;
            this.SearchCustomerIDbutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.SearchCustomerIDbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SearchCustomerIDbutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.SearchCustomerIDbutton.FlatAppearance.BorderSize = 0;
            this.SearchCustomerIDbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchCustomerIDbutton.ForeColor = System.Drawing.Color.Transparent;
            this.SearchCustomerIDbutton.Location = new System.Drawing.Point(274, 57);
            this.SearchCustomerIDbutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.SearchCustomerIDbutton.Name = "SearchCustomerIDbutton";
            this.SearchCustomerIDbutton.Size = new System.Drawing.Size(24, 24);
            this.SearchCustomerIDbutton.TabIndex = 6;
            this.SearchCustomerIDbutton.Tag = "";
            this.SearchCustomerIDbutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.SearchCustomerIDbutton.UseVisualStyleBackColor = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(304, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 20);
            this.label9.TabIndex = 45;
            this.label9.Text = "Sales Name";
            // 
            // SalesNametextBox
            // 
            this.SalesNametextBox.BackColor = System.Drawing.Color.White;
            this.SalesNametextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SalesNametextBox.Enabled = false;
            this.SalesNametextBox.Location = new System.Drawing.Point(434, 31);
            this.SalesNametextBox.Name = "SalesNametextBox";
            this.SalesNametextBox.Size = new System.Drawing.Size(140, 20);
            this.SalesNametextBox.TabIndex = 46;
            this.SalesNametextBox.Tag = "";
            // 
            // SearchSalesIDbutton
            // 
            this.SearchSalesIDbutton.BackColor = System.Drawing.Color.Transparent;
            this.SearchSalesIDbutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.SearchSalesIDbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SearchSalesIDbutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.SearchSalesIDbutton.FlatAppearance.BorderSize = 0;
            this.SearchSalesIDbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchSalesIDbutton.ForeColor = System.Drawing.Color.Transparent;
            this.SearchSalesIDbutton.Location = new System.Drawing.Point(274, 29);
            this.SearchSalesIDbutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.SearchSalesIDbutton.Name = "SearchSalesIDbutton";
            this.SearchSalesIDbutton.Size = new System.Drawing.Size(24, 24);
            this.SearchSalesIDbutton.TabIndex = 4;
            this.SearchSalesIDbutton.Tag = "";
            this.SearchSalesIDbutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.SearchSalesIDbutton.UseVisualStyleBackColor = false;
            this.SearchSalesIDbutton.Visible = false;
            this.SearchSalesIDbutton.Click += new System.EventHandler(this.SearchSalesIDbutton_Click);
            // 
            // SearchInvoiceNObutton
            // 
            this.SearchInvoiceNObutton.BackColor = System.Drawing.Color.Transparent;
            this.SearchInvoiceNObutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.SearchInvoiceNObutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SearchInvoiceNObutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.SearchInvoiceNObutton.FlatAppearance.BorderSize = 0;
            this.SearchInvoiceNObutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchInvoiceNObutton.ForeColor = System.Drawing.Color.Transparent;
            this.SearchInvoiceNObutton.Location = new System.Drawing.Point(274, 1);
            this.SearchInvoiceNObutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.SearchInvoiceNObutton.Name = "SearchInvoiceNObutton";
            this.SearchInvoiceNObutton.Size = new System.Drawing.Size(24, 24);
            this.SearchInvoiceNObutton.TabIndex = 2;
            this.SearchInvoiceNObutton.Tag = "";
            this.SearchInvoiceNObutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.SearchInvoiceNObutton.UseVisualStyleBackColor = false;
            this.SearchInvoiceNObutton.Click += new System.EventHandler(this.SearchInvoiceNObutton_Click);
            // 
            // TotalInvoicetextBox
            // 
            this.TotalInvoicetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TotalInvoicetextBox.Decimal = false;
            this.TotalInvoicetextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotalInvoicetextBox.Location = new System.Drawing.Point(125, 87);
            this.TotalInvoicetextBox.Money = true;
            this.TotalInvoicetextBox.Name = "TotalInvoicetextBox";
            this.TotalInvoicetextBox.Numeric = false;
            this.TotalInvoicetextBox.Prefix = "";
            this.TotalInvoicetextBox.Size = new System.Drawing.Size(143, 29);
            this.TotalInvoicetextBox.TabIndex = 7;
            // 
            // CosttextBox
            // 
            this.CosttextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CosttextBox.Decimal = false;
            this.CosttextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CosttextBox.Location = new System.Drawing.Point(125, 118);
            this.CosttextBox.Money = true;
            this.CosttextBox.Name = "CosttextBox";
            this.CosttextBox.Numeric = false;
            this.CosttextBox.Prefix = "";
            this.CosttextBox.Size = new System.Drawing.Size(143, 29);
            this.CosttextBox.TabIndex = 8;
            this.CosttextBox.TextChanged += new System.EventHandler(this.CosttextBox_TextChanged);
            // 
            // GrandTotaltextBox
            // 
            this.GrandTotaltextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GrandTotaltextBox.Decimal = false;
            this.GrandTotaltextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrandTotaltextBox.Location = new System.Drawing.Point(125, 149);
            this.GrandTotaltextBox.Money = true;
            this.GrandTotaltextBox.Name = "GrandTotaltextBox";
            this.GrandTotaltextBox.Numeric = false;
            this.GrandTotaltextBox.Prefix = "";
            this.GrandTotaltextBox.Size = new System.Drawing.Size(143, 29);
            this.GrandTotaltextBox.TabIndex = 9;
            // 
            // Cost
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(598, 194);
            this.Controls.Add(this.tableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Cost";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cost";
            this.Activated += new System.EventHandler(this.Cost_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Cost_FormClosed);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.TextBox SalesIDtextBox;
        private System.Windows.Forms.Button SearchSalesIDbutton;
        private System.Windows.Forms.Button SearchInvoiceNObutton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox InvoiceDatetextBox;
        private System.Windows.Forms.TextBox InvoiceNotextBox;
        private System.Windows.Forms.TextBox CustomerNametextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox CustomerIDtextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button SearchCustomerIDbutton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox SalesNametextBox;
        private CustomControls.TextBoxSL TotalInvoicetextBox;
        private CustomControls.TextBoxSL CosttextBox;
        private CustomControls.TextBoxSL GrandTotaltextBox;
    }
}