﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Threading;
using Sufindo.Report.Sales.Invoice;

namespace Sufindo.Sales
{
    public partial class SalesOrder : Form
    {
        private delegate void fillComboboxCallback(DataTable datatable);
        private delegate void fillAutoCompleteTextBox();

        SalesOrderdatagridview salesorderdatagridview;
        MenuStrip menustripAction;
        Connection connection;
        List<String> customerID;
        List<String> KursID;
        List<String> SalesOrderNo;
        List<Control> controlTextBox;
        DataTable datatable;

        Customerdatagridview customerdatagridview;
        Kursdatagridview kursdatagridview;
        Itemdatagridview itemdatagridview;

        String activity = "";
        Boolean isFind = false;
        Boolean addNewItem = false;

        int ASSIGNITEMROW = -1;

        public SalesOrder()
        {
            InitializeComponent();
        }

        public SalesOrder(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            this.menustripAction = menustripAction;
            customerdatagridview = null;
            kursdatagridview = null;
            itemdatagridview = null;
            salesorderdatagridview = null;
            if (connection == null)
            {
                connection = new Connection();
            }
            controlTextBox = new List<Control>();
            foreach (Control control in this.tableLayoutPanel.Controls)
            {
                if (control is TextBox)
                {
                    controlTextBox.Add(control);
                }
                else if (control is Panel)
                {
                    foreach (Control controlPanel in control.Controls)
                    {
                        if (controlPanel is RichTextBox)
                        {
                            controlTextBox.Add(controlPanel);
                        }
                    }
                }
                else if (control is ComboBox)
                {
                    controlTextBox.Add(control);
                }
            }
            datatable = new DataTable();
            newTransaction();
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (customerID == null) customerID = new List<string>();
                else customerID.Clear();

                if (KursID == null) KursID = new List<string>();
                else KursID.Clear();

                if (SalesOrderNo == null) SalesOrderNo = new List<string>();
                else SalesOrderNo.Clear();

                //SqlDataReader sqldataReader = connection.sqlDataReaders(String.Format("SELECT CUSTOMERID FROM M_CUSTOMER WHERE [STATUS] = '{0}'", Status.ACTIVE));
                //while (sqldataReader.Read()) customerID.Add(sqldataReader.GetString(0));

                SqlDataReader sqldataReader = connection.sqlDataReaders(String.Format("SELECT KURSID FROM M_KURS WHERE [STATUS] = '{0}'", Status.ACTIVE));
                while (sqldataReader.Read()) KursID.Add(sqldataReader.GetString(0));

                String query = String.Format("SELECT SALESORDERID FROM H_SALES_ORDER WHERE ([STATUS] = '{0}' OR [STATUS] = '{1}') AND CREATEDBY = '{2}' ", Status.READY, Status.RELEASE, AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString());
                sqldataReader = connection.sqlDataReaders(query);
                while (sqldataReader.Read()) SalesOrderNo.Add(sqldataReader.GetString(0));

                DataTable datatable = connection.openDataTableQuery(String.Format("SELECT CUSTOMERID, CUSTOMERNAME, ADDRESS, NOTELP, FAX, CONTACTPERSON, " +
                                                                                  "EMAIL, NOHP FROM M_CUSTOMER WHERE [STATUS] = '{0}'", Status.ACTIVE));
                object[] myArray = new object[1];
                myArray[0] = datatable;
                if (IsHandleCreated) BeginInvoke(new fillComboboxCallback(this.fillCombobox), myArray);

                if (IsHandleCreated) BeginInvoke(new fillAutoCompleteTextBox(this.fillAutoComplete));
            }
            catch (Exception ex)
            {
                Console.WriteLine("bgWorker_DoWork " + ex.Message);
            }
        }

        private void fillCombobox(DataTable datatable) {
            int selectedIndex = customerCombobox.SelectedIndex;
            customerCombobox.AutoCompleteSource = AutoCompleteSource.ListItems;
            customerCombobox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            customerCombobox.DataSource = datatable;
            customerCombobox.DisplayMember = "CUSTOMERNAME";
            customerCombobox.ValueMember = "CUSTOMERID";
            customerCombobox.SelectedIndex = selectedIndex;
        }

        private void fillAutoComplete()
        {
            SONotextBox.AutoCompleteCustomSource.Clear();
            SONotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            SONotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            SONotextBox.AutoCompleteCustomSource.AddRange(SalesOrderNo.ToArray());

            KurstextBox.AutoCompleteCustomSource.Clear();
            KurstextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            KurstextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            KurstextBox.AutoCompleteCustomSource.AddRange(KursID.ToArray());
            KurstextBox.Text = !KurstextBox.Text.Equals("") ? KurstextBox.Text : "IDR";
            RatetextBox.Text = !RatetextBox.Text.Equals("") ? RatetextBox.Text : "1";
        }

        public void save(){
            try
            {
                if (activity.Equals("UPDATE") || activity.Equals("INSERT"))
                {
                    if (isValidation())
                    {
                        List<SqlParameter> sqlParam = new List<SqlParameter>();

                        if (activity.Equals("UPDATE")) sqlParam.Add(new SqlParameter("@SALESORDERID", SONotextBox.Text));

                        sqlParam.Add(new SqlParameter("@CUSTOMERID", CustomerIDtextBox.Text));
                        sqlParam.Add(new SqlParameter("@CURRENCYID", KurstextBox.Text.ToUpper()));
                        sqlParam.Add(new SqlParameter("@PAYMENTMETHOD", paymentMethodscomboBox.Text));
                        sqlParam.Add(new SqlParameter("@RATE", RatetextBox.DecimalValue));
                        sqlParam.Add(new SqlParameter("@REMARK", RemarkrichTextBox.Text));
                        sqlParam.Add(new SqlParameter("@DISCOUNT", DiscounttextBox.DecimalValue));
                        sqlParam.Add(new SqlParameter("@STATUS", Status.READY));
                        sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));

                        String proc = activity.Equals("UPDATE") ? "UPDATE_DATA_H_SALES_ORDER" : "INSERT_DATA_H_SALES_ORDER";

                        DataTable dt = connection.callProcedureDatatable(proc, sqlParam);

                        if (dt.Rows.Count == 1)
                        {
                            List<string> values = new List<string>();
                            foreach (DataRow row in this.datatable.Rows)
                            {
                                String unitpricestr = row["UNITPRICE"].ToString().Contains(",") ? row["UNITPRICE"].ToString().Replace(",", "") : row["UNITPRICE"].ToString();
                                String value = String.Format("SELECT '{0}', '{1}', '{2}', {3}, {4}", dt.Rows[0]["SALESORDERID"].ToString(), row["ITEMID"].ToString(),
                                                                                              row["ITEMIDQTY"].ToString(),
                                                                                              row["QUANTITY"].ToString(), unitpricestr);
                                values.Add(value);
                            }
                            String[] columns = { "SALESORDERID", "ITEMID", "ITEMIDQTY", "QUANTITY", "PRICE" };
                            String query = String.Format("DELETE FROM D_SALES_ORDER WHERE SALESORDERID = '{0}'", dt.Rows[0]["SALESORDERID"].ToString());
                            if (connection.openReaderQuery(query))
                            {
                                if (connection.openReaderQuery(Utilities.queryMultipleInsert("D_SALES_ORDER", columns, values)))
                                {
                                    if (activity.Equals("UPDATE"))
                                    {
                                        query = String.Format("SELECT ORDERPICKINGID, SALESORDERID FROM H_ORDER_PICKING WHERE " +
                                                              "SALESORDERID = '{0}' AND STATUS = '{1}' OR STATUS = '{2}'", SONotextBox.Text, Status.OPEN, Status.RELEASE);
                                        dt = connection.openDataTableQuery(query);
                                        if (dt.Rows.Count != 0)
                                        {
                                            sqlParam = new List<SqlParameter>();
                                            sqlParam.Add(new SqlParameter("@ORDERPICKINGID", dt.Rows[0]["ORDERPICKINGID"].ToString()));
                                            sqlParam.Add(new SqlParameter("@STATUS", Status.CANCEL));
                                            if (connection.callProcedure("UPDATE_STATUS_H_ORDER_PICKING", sqlParam))
                                            {
                                                MessageBox.Show(String.Format("SO No {0} READY AND OP No {1} CANCEL", SONotextBox.Text, dt.Rows[0]["ORDERPICKINGID"].ToString()));
                                            }
                                        }
                                    }

                                    String SOID = (dt.Rows.Count == 0) ? SONotextBox.Text : dt.Rows[0]["SALESORDERID"].ToString();
                                    MessageBox.Show(String.Format("Sales Order No {0}", SOID));
                                    newTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private Boolean isValidation()
        {
            
            if (Utilities.isEmptyString(CustomerIDtextBox.Text))
            {
                MessageBox.Show("Please Input Customer Name");
            }
            else if (activity.Equals("UPDATE") && !isAvailableSalesOrderNoForUpdate())
            {
                MessageBox.Show("Sales Order has not been Available for Update");
            }
            else if (Utilities.isEmptyString(KurstextBox.Text))
            {
                MessageBox.Show("Please Input Currency");
            }
            else if (!isAvailableKurs())
            {
                MessageBox.Show("Please Input Correctly Currency");
            }
            else if (Utilities.isEmptyString(RatetextBox.Text))
            {
                MessageBox.Show("Please Input Rate");
            }
            else if (paymentMethodscomboBox.SelectedIndex == 0) {
                MessageBox.Show("Please Select Method Payment");
            }
            else if (MasterItemdataGridView.Rows.Count == 0)
            {
                MessageBox.Show("Minimal One Part Item  For Order");
            }
            else if (!checkQuantityOrder()) {
                MessageBox.Show("Order QTY tidak boleh melebihi stok atau nol");
            }
            else
            {
                return true;
            }
            return false;
        }

        private Boolean isAvailableKurs()
        {
            KurstextBox.Text = Utilities.removeSpace(KurstextBox.Text.ToUpper());
            Boolean result = Utilities.isAvailableID(KursID, KurstextBox.Text);
            return result;
        }

        private Boolean checkQuantityOrder() {
            foreach (DataGridViewRow row in MasterItemdataGridView.Rows)
            {
                if (row.Cells["STOCK"].Value.ToString().Equals("0")  || row.Cells["QUANTITY"].Value.ToString().Equals("0")) {
                    return false;
                }
                else if (Int64.Parse(row.Cells["QUANTITY"].Value.ToString()) > Int64.Parse(row.Cells["STOCK"].Value.ToString()))
                {
                    return false;
                }
            }
            return true;
        }

        private Boolean isAvailableSalesOrderNoForUpdate()
        {
            SONotextBox.Text = Utilities.removeSpace(SONotextBox.Text);
            Boolean result = Utilities.isAvailableID(SalesOrderNo, SONotextBox.Text);
            return result;
        }

        private void print()
        {

        }

        public void delete(){
            if (isFind && isAvailableSalesOrderNoForUpdate() && (StatustextBox.Text.Equals(Status.READY) || StatustextBox.Text.Equals(Status.RELEASE)))
            {
                List<SqlParameter> sqlParam = new List<SqlParameter>();
                sqlParam.Add(new SqlParameter("@SALESORDERID", SONotextBox.Text));
                sqlParam.Add(new SqlParameter("@STATUS", Status.DELETE));

                if (connection.callProcedure("UPDATE_STATUS_H_SALES_ORDER", sqlParam))
                {
                    String query = String.Format("SELECT ORDERPICKINGID FROM H_ORDER_PICKING WHERE " +
                                                 "SALESORDERID = '{0}' AND STATUS = '{1}'", SONotextBox.Text, Status.OPEN);
                    DataTable dt = connection.openDataTableQuery(query);
                    if (dt.Rows.Count != 0)
                    {
                        sqlParam = new List<SqlParameter>();
                        sqlParam.Add(new SqlParameter("@ORDERPICKINGID", dt.Rows[0]["ORDERPICKINGID"].ToString()));
                        sqlParam.Add(new SqlParameter("@STATUS", Status.CANCEL));
                        if (connection.callProcedure("UPDATE_STATUS_H_ORDER_PICKING", sqlParam))
                        {
                            MessageBox.Show("SO DELETED AND OP CANCEL");
                            newTransaction();
                        }
                    }
                    else {
                        MessageBox.Show("SO DELETED");
                        newTransaction();
                    }
                }
            }
            else
            {
                MessageBox.Show("Status Sales Order Must Release OR Ready");
            }
        }

        public void newTransaction()
        {
            Utilities.clearAllField(ref controlTextBox);
            datatable.Clear();
            AddItembutton.Enabled = true;
            paymentMethodscomboBox.SelectedIndex = 0;
            activity = "INSERT";
            isFind = false;
            SearchSONObutton.Visible = false;
            foreach (Control item in controlTextBox)
            {
                if (item is RichTextBox)
                {
                    RichTextBox richtextBox = (RichTextBox)item;
                    richtextBox.ReadOnly = false;
                    richtextBox.BackColor = SystemColors.Window;
                }
                else
                {
                    item.Enabled = true;
                }
                item.BackColor = SystemColors.Window;
            }

            TotaltextBox.Enabled = false;
            GrandTotaltextBox.Enabled = false;

            searchCustomerIDbutton.Visible = true;
            disableDataCustomer();
            clearDataCustomer();
            SONotextBox.Text = DateTime.Now.ToString("yyyyMM") + "XXXX";
            SONotextBox.Enabled = false;
            SODatetextBox.Text = DateTime.Now.ToString("dd/MM/yyyy");
            SODatetextBox.Enabled = false;
            StatustextBox.Text = Status.READY;
            StatustextBox.Enabled = false;
            searchKursbutton.Visible = true;
            
            DiscounttextBox.Text = "0";
            TotaltextBox.Text = "0";
            GrandTotaltextBox.Text = "0";

            KurstextBox.Text = "IDR";
            RatetextBox.Text = "1";

            MasterItemdataGridView.AllowUserToDeleteRows = true;
            AllowUserToEditRows(true);
            foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
            {
                if (column.Name.Equals("del"))
                {
                    column.Visible = true;
                    break;
                }
            }
            visibleConfirmationAction(false);
        }

        private void disableDataCustomer(){
            CustomerIDtextBox.Enabled = false;
            AddressrichTextBox.ReadOnly = true;
            NoTelptextBox.Enabled = false;
            NoFaxtextBox.Enabled = false;
            EmailtextBox.Enabled = false;
            ContactPersontextBox.Enabled = false;
            HandphonetextBox.Enabled = false;
        }

        private void clearDataCustomer(){
            customerCombobox.SelectedIndex = -1;
            CustomerIDtextBox.Text = "";
            AddressrichTextBox.Text = "";
            NoTelptextBox.Text = "";
            NoFaxtextBox.Text = "";
            EmailtextBox.Text = "";
            ContactPersontextBox.Text = "";
            HandphonetextBox.Text = "";
        }

        public void find(){
            if (!SearchSONObutton.Visible || isFind)
            {
                reloadAllData();
                Utilities.clearAllField(ref controlTextBox);
                SearchSONObutton.Visible = true;
                AddItembutton.Enabled = false;
                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("SONotextBox"))
                    {
                        if (item is RichTextBox)
                        {
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = true;
                            richtextBox.BackColor = SystemColors.Info;
                            richtextBox.Text = "";
                        }
                        else
                        {
                            item.Enabled = false;
                            item.Text = "";
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        currentTextbox.AutoCompleteCustomSource.AddRange(SalesOrderNo.ToArray());
                        currentTextbox.Text = "";
                        currentTextbox.Enabled = true;
                    }

                    item.BackColor = SystemColors.Info;
                }
                searchKursbutton.Visible = false;
                searchCustomerIDbutton.Visible = false;
                clearDataCustomer();
                disableDataCustomer();

                paymentMethodscomboBox.SelectedIndex = 0;

                isFind = true;
                datatable.Clear();
                MasterItemdataGridView.AllowUserToDeleteRows = false;
                AllowUserToEditRows(false);
                addNewItem = false;
                foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
                {
                    if (column.Name.Equals("del"))
                    {
                        column.Visible = false;
                        break;
                    }
                }
                activity = "";
                AddItembutton.Enabled = false;
                visibleConfirmationAction(true);
            }
        }

        public void edit(){
            if ((!SearchSONObutton.Visible || isFind))
            {
                reloadAllData();
                SearchSONObutton.Visible = true;
                Utilities.clearAllField(ref controlTextBox);
                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("SONotextBox"))
                    {
                        if (item is RichTextBox)
                        {
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = true;
                            richtextBox.BackColor = SystemColors.Info;
                        }
                        else
                        {
                            item.Enabled = false;
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.AutoCompleteCustomSource.Clear();
                        currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        currentTextbox.AutoCompleteCustomSource.AddRange(SalesOrderNo.ToArray());
                        currentTextbox.Enabled = true;
                    }

                    item.BackColor = SystemColors.Info;
                    item.Text = "";
                }
                this.datatable.Clear();
                searchCustomerIDbutton.Visible = false;
                disableDataCustomer();
                clearDataCustomer();

                paymentMethodscomboBox.SelectedIndex = 0;
                paymentMethodscomboBox.Enabled = true;

                DiscounttextBox.Enabled = true;

                isFind = true;
                MasterItemdataGridView.AllowUserToDeleteRows = true;
                AllowUserToEditRows(true);
                addNewItem = false;
                foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
                {
                    if (column.Name.Equals("del"))
                    {
                        column.Visible = true;
                        break;
                    }
                }

                KurstextBox.Enabled = true;
                RatetextBox.Enabled = true;
                searchKursbutton.Visible = true;

                RemarkrichTextBox.ReadOnly = false;
                RemarkrichTextBox.BackColor = SystemColors.Info;

                activity = "UPDATE";

                visibleConfirmationAction(false);
            }
        }

        private void visibleConfirmationAction(bool visible) {
            //InvoicecheckBox.Checked = InvoicecheckBox.Checked ? visible : InvoicecheckBox.Checked;
            //DOcheckBox.Checked = DOcheckBox.Checked ? visible : DOcheckBox.Checked;
            printPanel.Visible = visible;
        }

        private void AllowUserToEditRows(Boolean allow)
        {
            foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
            {
                column.ReadOnly = (column.Name.Equals("QUANTITY") || column.Name.Equals("UNITPRICE")) ? !allow : true;
            }
        }
       
        private void KurstextBox_Leave(object sender, EventArgs e)
        {
            Boolean isExistingPartNo = false;
            foreach (String item in KursID)
            {
                if (item.ToUpper().Equals(KurstextBox.Text.ToUpper()))
                {
                    isExistingPartNo = true;
                    break;
                }
            }

            if (!isExistingPartNo)
            {
                KurstextBox.Text = "IDR";
                RatetextBox.Text = "1";
            }
        }

        private void KurstextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in KursID)
                {
                    if (item.ToUpper().Equals(KurstextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        KurstextBox.Text = KurstextBox.Text.ToUpper();
                        String query = String.Format("SELECT RATE " +
                                                     "FROM M_KURS " +
                                                     "WHERE KURSID = '{0}'", KurstextBox.Text);

                        DataTable dt = connection.openDataTableQuery(query);
                        RatetextBox.Text = dt.Rows[0]["RATE"].ToString();
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    KurstextBox.Text = "IDR";
                    RatetextBox.Text = "1";
                }
            }
        }

        private void searchCustomerIDbutton_Click(object sender, EventArgs e)
        {
            if (customerdatagridview == null)
            {
                customerdatagridview = new Customerdatagridview("MasterCustomer");
                customerdatagridview.masterCustomerPassingData = new Customerdatagridview.MasterCustomerPassingData(MasterCustomerPassingData);
                //supplierdatagridview.MdiParent = this.MdiParent;
                customerdatagridview.ShowDialog();
            }
            else if (customerdatagridview.IsDisposed)
            {
                customerdatagridview = new Customerdatagridview("MasterCustomer");
                customerdatagridview.masterCustomerPassingData = new Customerdatagridview.MasterCustomerPassingData(MasterCustomerPassingData);
                //supplierdatagridview.MdiParent = this.MdiParent;
                customerdatagridview.ShowDialog();
            }
        }

        private void MasterCustomerPassingData(DataTable sender){
            customerCombobox.Text = sender.Rows[0]["CUSTOMERNAME"].ToString();
            CustomerIDtextBox.Text = sender.Rows[0]["CUSTOMERID"].ToString();
            AddressrichTextBox.Text = sender.Rows[0]["ADDRESS"].ToString();
            NoTelptextBox.Text = sender.Rows[0]["NOTELP"].ToString();
            NoFaxtextBox.Text = sender.Rows[0]["FAX"].ToString();
            EmailtextBox.Text = sender.Rows[0]["EMAIL"].ToString();
            ContactPersontextBox.Text = sender.Rows[0]["CONTACTPERSON"].ToString();
            HandphonetextBox.Text = sender.Rows[0]["NOHP"].ToString();
        }

        private void searchKursbutton_Click(object sender, EventArgs e)
        {
            if (kursdatagridview == null)
            {
                kursdatagridview = new Kursdatagridview("MasterKurs");
                kursdatagridview.masterKursPassingData = new Kursdatagridview.MasterKursPassingData(MasterKursPassingData);
                //kursdatagridview.MdiParent = this.MdiParent;
                kursdatagridview.ShowDialog();
            }
            else if (kursdatagridview.IsDisposed)
            {
                kursdatagridview = new Kursdatagridview("MasterKurs");
                kursdatagridview.masterKursPassingData = new Kursdatagridview.MasterKursPassingData(MasterKursPassingData);
                //kursdatagridview.MdiParent = this.MdiParent;
                kursdatagridview.ShowDialog();
            }
        }

        private void MasterKursPassingData(DataTable sender){
            sender.Rows[0]["KURSID"].ToString();
            KurstextBox.Text = sender.Rows[0]["KURSID"].ToString();
            RatetextBox.Text = sender.Rows[0]["RATE"].ToString();
        }

        private void AddItembutton_Click(object sender, EventArgs e)
        {
            addNewItem = true;
            if (itemdatagridview == null)
            {
                itemdatagridview = new Itemdatagridview("SALESORDER", "", datatable);
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
            else if (itemdatagridview.IsDisposed)
            {
                itemdatagridview = new Itemdatagridview("SALESORDER", "", datatable);
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
        }

        private void MasterItemPassingData(DataTable datatable){

            if (this.datatable == null) this.datatable = new DataTable();
            else
            {
                if(datatable.Rows.Count == 0)this.datatable.Clear();
            }

            /*remove column not use*/
            List<string> columnsRemove = new List<string>();

            foreach (DataColumn columns in datatable.Columns)
            {
                if (columns.ColumnName.Equals("ITEMNAME_ALIAS"))
                {
                    columns.ColumnName = "ITEMNAMEQTY";
                }
                else if (columns.ColumnName.Equals("ITEMID_ALIAS"))
                {
                    columns.ColumnName = "ITEMIDQTY";
                }
            }
            foreach (DataColumn column in datatable.Columns)
            {
                Boolean isExistingColumn = false;
                foreach (DataGridViewColumn columngrid in MasterItemdataGridView.Columns)
                {
                    if (column.ColumnName.Equals(columngrid.DataPropertyName))
                    {
                        isExistingColumn = true;
                        break;
                    }
                }
                if (!isExistingColumn)
                {
                    columnsRemove.Add(column.ColumnName);
                }
            }
            foreach (string columnName in columnsRemove)
            {
                datatable.Columns.Remove(columnName);
            }

            if (this.datatable.Rows.Count == 0)
            {
                //foreach (DataRow row in datatable.Rows)
                //{
                //    row["QUANTITY"] = row["STOCK"];
                //}
                this.datatable = datatable;
            }
            else
            {
                foreach (DataRow row in datatable.Rows)
                {
                    //row["QUANTITY"] = row["STOCK"];
                    this.datatable.Rows.Add(row.ItemArray);
                }
            }

            foreach (DataRow row in this.datatable.Rows)
            {
                //row["QUANTITY"] = StatustextBox.Text.Equals(Status.RELEASE) ? row["QUANTITY"] : row["STOCK"];
                Int64 quantity = Int64.Parse(row["QUANTITY"].ToString());
                Double unitprice = row["UNITPRICE"].ToString().Equals("") ? 0.0 : Double.Parse(row["UNITPRICE"].ToString());
                row["UNITPRICE"] = unitprice.ToString();
                Double amount = (quantity * unitprice);
                row["AMOUNT"] = (Object)amount.ToString();
            }

            Utilities.generateNoDataTable(ref this.datatable);

            MasterItemdataGridView.DataSource = this.datatable;

            Double total = 0.0;
            foreach (DataGridViewRow row in MasterItemdataGridView.Rows)
            {
                total += Double.Parse(row.Cells["AMOUNT"].Value.ToString());
            }
            TotaltextBox.Text = total.ToString();

            Double discount = DiscounttextBox.DecimalValue;

            GrandTotaltextBox.Text = (total - discount).ToString();
        }

        private void AssignItemQuantityPassingData(DataTable sender)
        {
            MasterItemdataGridView.Rows[ASSIGNITEMROW].Cells["ITEMIDQTY"].Value = sender.Rows[0]["ITEMID"].ToString();
            MasterItemdataGridView.Rows[ASSIGNITEMROW].Cells["ITEMNAMEQTY"].Value = sender.Rows[0]["ITEMNAME"].ToString();
            MasterItemdataGridView.Rows[ASSIGNITEMROW].Cells["STOCK"].Value = sender.Rows[0]["QUANTITY"].ToString();
            //MasterItemdataGridView.Rows[ASSIGNITEMROW].Cells["QUANTITY"].Value = 1;
        }

        private void MasterItemdataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                if (MasterItemdataGridView.Columns[e.ColumnIndex].Name.Equals("del"))
                {
                    if (datatable.Rows.Count == 1)
                    {
                        TotaltextBox.Text = "0";
                        GrandTotaltextBox.Text = "0";
                    }
                    this.datatable.Rows.RemoveAt(e.RowIndex);
                    Utilities.generateNoDataTable(ref datatable);
                    MasterItemdataGridView.DataSource = datatable;
                }
                else if ((MasterItemdataGridView.Columns[e.ColumnIndex].Name.Equals("ITEMIDQTY") || MasterItemdataGridView.Columns[e.ColumnIndex].Name.Equals("ITEMNAMEQTY")) && (activity.Equals("INSERT") || activity.Equals("UPDATE")))
                {
                    ASSIGNITEMROW = e.RowIndex;
                    String ITEMID = MasterItemdataGridView.Rows[e.RowIndex].Cells["ITEMID"].Value.ToString();
                    if (itemdatagridview == null)
                    {
                        itemdatagridview = new Itemdatagridview("ASSIGNITEMPURCHASERECEIPT", ITEMID);
                        itemdatagridview.assignItemQuantityPassingData = new Itemdatagridview.AssignItemQuantityPassingData(AssignItemQuantityPassingData);
                        itemdatagridview.ShowDialog();
                    }
                    else if (itemdatagridview.IsDisposed)
                    {
                        itemdatagridview = new Itemdatagridview("ASSIGNITEMPURCHASERECEIPT", ITEMID);
                        itemdatagridview.assignItemQuantityPassingData = new Itemdatagridview.AssignItemQuantityPassingData(AssignItemQuantityPassingData);
                        itemdatagridview.ShowDialog();
                    }
                }
            }
        }

        private void MasterItemdataGridView_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            for (int i = 0; i < datatable.Rows.Count; i++)
            {
                if (datatable.Rows[i].RowState == DataRowState.Deleted)
                {
                    datatable.Rows.RemoveAt(i);
                }
            }
            Utilities.generateNoDataTable(ref datatable);
            Double total = 0.0;
            foreach (DataGridViewRow row in MasterItemdataGridView.Rows)
            {
                total += Double.Parse(row.Cells["AMOUNT"].Value.ToString());
            }
            TotaltextBox.Text = total.ToString();

            Double discount = DiscounttextBox.DecimalValue;

            GrandTotaltextBox.Text = (total - discount).ToString();
        }

        private void MasterItemdataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
        }

        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            int currColumn = MasterItemdataGridView.CurrentCell.ColumnIndex;
            int currRow = MasterItemdataGridView.CurrentCell.RowIndex;
            if (MasterItemdataGridView.Columns[currColumn].Name.Equals("QUANTITY"))
            {
                e.Handled = Utilities.onlyNumberTextBox(sender, (KeyPressEventArgs)e);
            }
            else if (MasterItemdataGridView.Columns[currColumn].Name.Equals("UNITPRICE"))
            {
                //e.Handled = Utilities.onlyNumberTextBox(sender, (KeyPressEventArgs)e);
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
                {
                    e.Handled = true;
                }
            }
        }

        private void MasterItemdataGridView_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                    Int64 stock = (MasterItemdataGridView.Rows[e.RowIndex].Cells["STOCK"].Value == null || MasterItemdataGridView.Rows[e.RowIndex].Cells["STOCK"].Value.Equals("")) ? 0 : Int64.Parse(MasterItemdataGridView.Rows[e.RowIndex].Cells["STOCK"].Value.ToString());
                    Int64 quantity = (MasterItemdataGridView.Rows[e.RowIndex].Cells["QUANTITY"].Value == null || MasterItemdataGridView.Rows[e.RowIndex].Cells["QUANTITY"].Value.Equals("")) ? 0 : Int64.Parse(MasterItemdataGridView.Rows[e.RowIndex].Cells["QUANTITY"].Value.ToString());
                    Double unitprice = (MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value == null || MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value.ToString().Equals("")) ? 0 : Double.Parse(MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value.ToString());
                    //if (activity.Equals("UPDATE"))
                    //{
                    //    MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value = unitprice.ToString();
                    //}
                    //else if (activity.Equals("INSERT"))
                    //{
                    //    MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value = unitprice.ToString("N2");
                    //} 
                    if (quantity >= stock)
                    {
                        if (activity.Equals("INSERT")) {
                            MasterItemdataGridView.Rows[e.RowIndex].Cells["QUANTITY"].Value = stock;
                            quantity = Int64.Parse(MasterItemdataGridView.Rows[e.RowIndex].Cells["QUANTITY"].Value.ToString());
                        }
                    }
                    else if (quantity == 0)
                    {
                        MasterItemdataGridView.Rows[e.RowIndex].Cells["QUANTITY"].Value = 1;
                        quantity = Int64.Parse(MasterItemdataGridView.Rows[e.RowIndex].Cells["QUANTITY"].Value.ToString());
                    }

                    Double amount = (quantity * unitprice);
                    MasterItemdataGridView.Rows[e.RowIndex].Cells["AMOUNT"].Value = amount;//.ToString("N2");
                    

                    Double total = 0.0;
                    foreach (DataGridViewRow row in MasterItemdataGridView.Rows)
                    {
                        total += Double.Parse(row.Cells["AMOUNT"].Value.ToString());
                    }
                    TotaltextBox.Text = total.ToString();

                    Double discount = DiscounttextBox.DecimalValue;

                    GrandTotaltextBox.Text = (total - discount).ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ex" + ex.Message);
            }
        }

        private void SalesOrderPassingData(DataTable sender){
            SONotextBox.Text = sender.Rows[0]["SALESORDERID"].ToString();
            SODatetextBox.Text = sender.Rows[0]["SALESORDERDATE"].ToString();
            customerCombobox.SelectedValue = sender.Rows[0]["CUSTOMERID"].ToString();
            KurstextBox.Text = sender.Rows[0]["CURRENCYID"].ToString();
            RatetextBox.Text = sender.Rows[0]["RATE"].ToString();
            StatustextBox.Text = sender.Rows[0]["STATUS"].ToString();
            DiscounttextBox.Text = sender.Rows[0]["DISCOUNT"].ToString();
            RemarkrichTextBox.Text = sender.Rows[0]["REMARK"].ToString();
            paymentMethodscomboBox.Text = sender.Rows[0]["PAYMENTMETHOD"].ToString();
            InvoicecheckBox.Checked = true;
            DOcheckBox.Checked = false;

            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMID, B.ITEMNAME, " +
                                  "A.ITEMIDQTY,  " +
                                  "( " +
                                  "   SELECT ITEMNAME FROM M_ITEM " +
                                  "   WHERE ITEMID = A.ITEMID " +
                                  ") AS ITEMNAMEQTY, " +
                                  "( " +
                                  "  SELECT QUANTITY FROM M_ITEM " +
                                  "  WHERE ITEMID = A.ITEMIDQTY " +
                                  ") AS STOCK, " +
                                  "A.QUANTITY, UOMID, PRICE AS UNITPRICE, '0' AS AMOUNT " +
                                  "FROM D_SALES_ORDER A " +
                                  "JOIN M_ITEM B " +
                                  "ON A.ITEMID = B.ITEMID " +
                                  "WHERE SALESORDERID = '{0}' ORDER BY A.ITEMID ASC", SONotextBox.Text);
            DataTable datatable = connection.openDataTableQuery(query);
            this.datatable.Clear();
            MasterItemPassingData(datatable);
            AddItembutton.Enabled = activity.Equals("") ? false : true;
        }

        private void SalesOrder_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            reloadAllData();
            Main.ACTIVEFORM = this;
        }

        private void SONotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingSONo = false;
                this.datatable.Clear();
                foreach (String item in SalesOrderNo)
                {
                    if (item.ToUpper().Equals(SONotextBox.Text.ToUpper()))
                    {
                        isExistingSONo = true;
                        SONotextBox.Text = SONotextBox.Text.ToUpper();
                        
                        String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SALESORDERID) AS 'NO', SALESORDERID, " +
                                                     "CONVERT(VARCHAR(10), SALESORDERDATE, 103) AS [SALESORDERDATE], " +
                                                     "CUSTOMERID, CURRENCYID, RATE, PAYMENTMETHOD, DISCOUNT, REMARK, STATUS, CREATEDBY " +
                                                     "FROM H_SALES_ORDER " +
                                                     "WHERE SALESORDERID = '{0}' " +
                                                     "ORDER BY SALESORDERID ASC", SONotextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        SalesOrderPassingData(datatable);
//                        setDecimalN2();
                        break;
                    }
                }

                if (!isExistingSONo)
                {
                    SONotextBox.Text = "";
                    Utilities.clearAllField(ref controlTextBox);
                    this.datatable.Clear();
                    AddItembutton.Enabled = false;
                    clearDataCustomer();
                }
                
            }
        }

        private void SONotextBox_Leave(object sender, EventArgs e)
        {
            if (isFind)
            {

                Boolean isExistingPartNo = false;
                foreach (String item in SalesOrderNo)
                {
                    if (item.ToUpper().Equals(SONotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        
                        String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SALESORDERID) AS 'NO', SALESORDERID, " +
                                                     "CONVERT(VARCHAR(10), SALESORDERDATE, 103) AS [SALESORDERDATE], " +
                                                     "CUSTOMERID, CURRENCYID, RATE, PAYMENTMETHOD, DISCOUNT, REMARK, STATUS, CREATEDBY " +
                                                     "FROM H_SALES_ORDER " +
                                                     "WHERE SALESORDERID = '{0}' " +
                                                     "ORDER BY SALESORDERID ASC", SONotextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        SalesOrderPassingData(datatable);
//                        setDecimalN2();
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    SONotextBox.Text = "";
                    Utilities.clearAllField(ref controlTextBox);
                    this.datatable.Clear();
                    AddItembutton.Enabled = false;
                    clearDataCustomer();
                }
            }
        }

        private void SalesOrder_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }

        private void Confirmbutton_Click(object sender, EventArgs e)
        {
            
        }

        private void ClearSalesOrderData()
        {
            find();
        }

        public void confirm() {
            if (isFind && StatustextBox.Text.Equals(Status.RELEASE) && (activity.Equals("") || activity.Equals("UPDATE")))
            {
                List<SqlParameter> sqlParam = new List<SqlParameter>();
                sqlParam.Add(new SqlParameter("@SALESORDERID", SONotextBox.Text));
                sqlParam.Add(new SqlParameter("@STATUS", Status.CONFIRM));

                if (connection.callProcedure("UPDATE_STATUS_H_SALES_ORDER", sqlParam))
                {
                    String query = String.Format("SELECT ORDERPICKINGID FROM H_ORDER_PICKING " +
                                                 "WHERE SALESORDERID = '{0}' AND STATUS = '{1}'", SONotextBox.Text, Status.OPEN);
                    DataTable dt = connection.openDataTableQuery(query);
                    if (dt.Rows.Count != 0)
                    {
                        sqlParam = new List<SqlParameter>();
                        sqlParam.Add(new SqlParameter("@ORDERPICKINGID", dt.Rows[0]["ORDERPICKINGID"].ToString()));
                        sqlParam.Add(new SqlParameter("@STATUS", Status.CLOSE));
                        if (connection.callProcedure("UPDATE_STATUS_H_ORDER_PICKING", sqlParam))
                        {
                            sqlParam = new List<SqlParameter>();
                            sqlParam.Add(new SqlParameter("@SALESORDERID", SONotextBox.Text));
                            sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));
                            sqlParam.Add(new SqlParameter("@TOTAL", GrandTotaltextBox.DecimalValue));
                            sqlParam.Add(new SqlParameter("@STATUS", Status.ACTIVE));
                            DataTable dtinvoice = connection.callProcedureDatatable("INSERT_DATA_INVOICE_AND_DELIVERY_ORDER", sqlParam);
                            if (dtinvoice.Rows.Count != 0)
                            {
                                if (InvoicecheckBox.Checked)
                                {
                                    InvoiceReportForm invoice = new InvoiceReportForm(dtinvoice.Rows[0]["INVOICEID"].ToString());
                                    invoice.clearSalesOrderData = new InvoiceReportForm.ClearSalesOrderData(ClearSalesOrderData);
                                    invoice.ShowDialog();
                                }

                                if (DOcheckBox.Checked)
                                {
                                    DeliveryOrderReportForm deliver = new DeliveryOrderReportForm(dtinvoice.Rows[0]["DELIVERYORDERID"].ToString());
                                    deliver.clearSalesOrderData = new DeliveryOrderReportForm.ClearSalesOrderData(ClearSalesOrderData);
                                    deliver.ShowDialog();
                                }
                                else
                                {
                                    find();
                                }

                                //sqlParam = new List<SqlParameter>();
                                //sqlParam.Add(new SqlParameter("@INVOICEID", dtinvoice.Rows[0]["INVOICEID"].ToString()));
                                //sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));
                                //sqlParam.Add(new SqlParameter("@STATUS", Status.ACTIVE));
                                //DataTable dtdeliveryorder = connection.callProcedureDatatable("INSERT_DATA_DELIVERY_ORDER", sqlParam);

                                //if (dtdeliveryorder.Rows.Count != 0)
                                //{
                                //    if (InvoicecheckBox.Checked)
                                //    {
                                //        InvoiceReportForm invoice = new InvoiceReportForm(dtinvoice.Rows[0]["INVOICEID"].ToString());
                                //        invoice.clearSalesOrderData = new InvoiceReportForm.ClearSalesOrderData(ClearSalesOrderData);
                                //        invoice.ShowDialog();
                                //    }

                                //    if (DOcheckBox.Checked)
                                //    {
                                //        DeliveryOrderReportForm deliver = new DeliveryOrderReportForm(dtdeliveryorder.Rows[0]["DELIVERYORDERID"].ToString());
                                //        deliver.clearSalesOrderData = new DeliveryOrderReportForm.ClearSalesOrderData(ClearSalesOrderData);
                                //        deliver.ShowDialog();
                                //    }
                                //    else {
                                //        find();
                                //    }
                                //    //}
                                //}
                                //else
                                //{

                                //}
                            }
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Transaksi ini belum bisa di confirm, karena barang belum di siapkan oleh bagian gudang.\n NB: Confirm kalau barang sudah dibawah.");
            }
        }

        public void cancel() {
            if (isFind && StatustextBox.Text.Equals(Status.RELEASE))
            {
                List<SqlParameter> sqlParam = new List<SqlParameter>();
                sqlParam.Add(new SqlParameter("@SALESORDERID", SONotextBox.Text));
                sqlParam.Add(new SqlParameter("@STATUS", Status.READY));

                if (connection.callProcedure("UPDATE_STATUS_H_SALES_ORDER", sqlParam))
                {
                    String query = String.Format("SELECT ORDERPICKINGID FROM H_ORDER_PICKING WHERE " +
                                                 "SALESORDERID = '{0}' AND STATUS = '{1}'", SONotextBox.Text, Status.OPEN);
                    DataTable dt = connection.openDataTableQuery(query);
                    if (dt.Rows.Count != 0)
                    {
                        sqlParam = new List<SqlParameter>();
                        sqlParam.Add(new SqlParameter("@ORDERPICKINGID", dt.Rows[0]["ORDERPICKINGID"].ToString()));
                        sqlParam.Add(new SqlParameter("@STATUS", Status.CANCEL));
                        if (connection.callProcedure("UPDATE_STATUS_H_ORDER_PICKING", sqlParam))
                        {
                            MessageBox.Show("SUCCESS UPDATE SO READY AND OP CANCEL");
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Status Sales Order Must Release");
            }
        }

        private void SearchSONObutton_Click(object sender, EventArgs e)
        {
            //Boolean isReadyOnly = activity.Equals("UPDATE") ? true : false;
            Boolean isReadyOnly = activity.Equals("UPDATE") ? false : false;
            salesorderdatagridview = null;
            if (salesorderdatagridview == null)
            {
                salesorderdatagridview = new SalesOrderdatagridview("SalesOrder", isReadyOnly);
                salesorderdatagridview.salesOrderPassingData = new SalesOrderdatagridview.SalesOrderPassingData(SalesOrderPassingData);
                salesorderdatagridview.ShowDialog();
            }
            else if (salesorderdatagridview.IsDisposed)
            {
                salesorderdatagridview = new SalesOrderdatagridview("SalesOrder", isReadyOnly);
                salesorderdatagridview.salesOrderPassingData = new SalesOrderdatagridview.SalesOrderPassingData(SalesOrderPassingData);
                salesorderdatagridview.ShowDialog();
            }

//            setDecimalN2();
        }

        private void DiscounttextBox_TextChanged(object sender, EventArgs e)
        {
            Double discount = DiscounttextBox.DecimalValue;
            Double total = TotaltextBox.DecimalValue;
            GrandTotaltextBox.Text = (total - discount).ToString();
        }

        private void MasterItemdataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 10 || e.ColumnIndex == 11)
            {
                try
                {
                    Decimal d = Decimal.Parse(e.Value.ToString());
                    e.Value = d.ToString("N2");
                }
                catch (Exception ex)
                {
                    Decimal d = 0;
                    e.Value = d.ToString("N2");
                }
            }
            //if (e.ColumnIndex == 10 || e.ColumnIndex == 11)
            //{
            //    Decimal d = Decimal.Parse(e.Value.ToString());
            //    e.Value = d.ToString("N2");
            //}
        }

        private void SalesOrder_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show("Anda yakin mau keluar?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (result == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void customerCombobox_SelectedValueChanged(object sender, EventArgs e)
        {
            DataTable datatable = (DataTable)customerCombobox.DataSource;
            if (customerCombobox.SelectedIndex >= 0 && customerCombobox.SelectedIndex < datatable.Rows.Count)
            {
                Object[] item = datatable.Rows[customerCombobox.SelectedIndex].ItemArray;
                DataColumnCollection columns = datatable.Columns;
                DataTable dt = new DataTable();
                dt = datatable.Copy();
                dt.Clear();
                dt.Rows.Add(item);
                dt.AcceptChanges();
                this.MasterCustomerPassingData(dt);
            }
            else
            {
                this.clearDataCustomer();
            }
        }

        private void customerCombobox_Validated(object sender, EventArgs e)
        {
            if (customerCombobox.SelectedIndex == -1) {
                this.customerCombobox.Text = "";
                this.clearDataCustomer();
            }
        }
    }
}