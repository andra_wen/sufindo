﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Threading;
using Sufindo.Report.Sales.Invoice;

namespace Sufindo.Sales
{
    public partial class SalesReturn : Form
    {
        private delegate void fillAutoCompleteTextBox();

        SalesReturndatagridview salesreturndatagridview;
        Invoicedatagridview invoicedatagridview;
        MenuStrip menustripAction;
        Connection connection;
        List<String> SalesReturnNo;
        List<String> InvoiceNo;
        List<Control> controlTextBox;
        DataTable datatable;

        Itemdatagridview itemdatagridview;

        String activity = "";
        Boolean isFind = false;
        Boolean isPrint = false;

        public SalesReturn()
        {
            InitializeComponent();
        }

        public SalesReturn(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            this.menustripAction = menustripAction;
            itemdatagridview = null;
            invoicedatagridview = null;
            if (connection == null)
            {
                connection = new Connection();
            }
            controlTextBox = new List<Control>();
            foreach (Control control in this.tableLayoutPanel.Controls)
            {
                if (control is TextBox)
                {
                    controlTextBox.Add(control);
                }
                else if (control is Panel)
                {
                    foreach (Control controlPanel in control.Controls)
                    {
                        if (controlPanel is RichTextBox)
                        {
                            controlTextBox.Add(controlPanel);
                        }
                    }
                }
                else if (control is ComboBox)
                {
                    controlTextBox.Add(control);
                }
            }
            datatable = new DataTable();
            newSalesReturn();
            RebatetextBox.Text = "0";
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }

        public void save(){
            if (activity.Equals("UPDATE") || activity.Equals("INSERT"))
            {
                if (isValidation())
                {
                    
                    List<SqlParameter> sqlParam = new List<SqlParameter>();

                    RebatetextBox.Text = RebatetextBox.Text.Equals("") ? "0" : RebatetextBox.Text;
                    if (activity.Equals("UPDATE")) sqlParam.Add(new SqlParameter("@SALESRETURNID", SalesReturnNotextBox.Text));
                    sqlParam.Add(new SqlParameter("@INVOICEID",InvoiceNotextBox.Text));
	                sqlParam.Add(new SqlParameter("@REBATE",RebatetextBox.Text));
	                sqlParam.Add(new SqlParameter("@REMARK", RemarkrichTextBox.Text));
                    sqlParam.Add(new SqlParameter("@TOTAL", GrandTotaltextBox.Text));
                    sqlParam.Add(new SqlParameter("@STATUS", Status.READY));
                    sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));

                    String proc = activity.Equals("UPDATE") ? "UPDATE_DATA_H_SALES_RETURN" : "INSERT_DATA_H_SALES_RETURN";

                    DataTable dt = connection.callProcedureDatatable(proc, sqlParam);

                    if (dt.Rows.Count == 1)
                    {
                        List<string> values = new List<string>();
                        foreach (DataRow row in this.datatable.Rows)
                        {
                            String unitpricestr = row["UNITPRICE"].ToString().Contains(",") ? row["UNITPRICE"].ToString().Replace(",", "") : row["UNITPRICE"].ToString();
                            String value = String.Format("SELECT '{0}', '{1}', '{2}', {3}, {4}", dt.Rows[0]["SALESRETURNID"].ToString(), row["ITEMID"].ToString(),
                                                                                          row["ITEMIDQTY"].ToString(),
                                                                                          row["RETURNQUANTITY"].ToString(), unitpricestr);
                            values.Add(value);
                        }
                        String[] columns = { "SALESRETURNID", "ITEMID", "ITEMIDQTY", "QUANTITY", "PRICE" };
                        String query = String.Format("DELETE FROM D_SALES_RETURN WHERE SALESRETURNID = '{0}'", dt.Rows[0]["SALESRETURNID"].ToString());
                        if (connection.openReaderQuery(query))
                        {
                            if (connection.openReaderQuery(Utilities.queryMultipleInsert("D_SALES_RETURN", columns, values)))
                            {
                                MessageBox.Show(String.Format("Sales Return No. {0}", dt.Rows[0]["SALESRETURNID"].ToString()));
                                if (isPrint)
                                {
                                    isPrint = false;
                                    SalesReturnReportForm salesreturnReportForm = new SalesReturnReportForm(dt.Rows[0]["SALESRETURNID"].ToString(), checkBoxAlias.Checked);
                                    salesreturnReportForm.ShowDialog();
                                    newSalesReturn();
                                    reloadAllData();
                                }
                                else
                                {
                                    newSalesReturn();
                                    reloadAllData();
                                }
                            }
                        }
                    }
                }
            }
        }

        private Boolean isValidation()
        {
            if (Utilities.isEmptyString(CustomerIDtextBox.Text))
            {
                MessageBox.Show("Please Input Supplier No");
            }
            else if (activity.Equals("UPDATE") && !isAvailableSalesReturnNoForUpdate())
            {
                MessageBox.Show("Sales Return has not been Available for Update");
            }
            else if (Utilities.isEmptyString(KurstextBox.Text))
            {
                MessageBox.Show("Please Input Kurse");
            }
            else if (Utilities.isEmptyString(RatetextBox.Text))
            {
                MessageBox.Show("Please Input Rate");
            }
            else if (MasterItemdataGridView.Rows.Count == 0)
            {
                MessageBox.Show("Minimal One Part Item  For Order");
            }
            else
            {
                return true;
            }
            return false;
        }

        private Boolean isAvailableSalesReturnNoForUpdate()
        {
            SalesReturnNotextBox.Text = Utilities.removeSpace(SalesReturnNotextBox.Text);
            Boolean result = Utilities.isAvailableID(SalesReturnNo, SalesReturnNotextBox.Text);
            return result;
        }

        public void print()
        {
            isPrint = true;
            if (activity.Equals("UPDATE") || activity.Equals("INSERT"))
            {
                save();
            }
            else {
                SalesReturnReportForm salesreturnReportForm = new SalesReturnReportForm(SalesReturnNotextBox.Text, checkBoxAlias.Checked);
                salesreturnReportForm.ShowDialog();
            }
        }

        public void delete(){
            if (isFind && isAvailableSalesReturnNoForUpdate())
            {
                List<SqlParameter> sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@SALESRETURNID", SalesReturnNotextBox.Text));
                sqlParam.Add(new SqlParameter("@STATUS", Status.DELETE));
                sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));

                if (connection.callProcedure("DELETE_DATA_H_SALES_RETURN", sqlParam))
                {
                    newSalesReturn();
                    reloadAllData();
                }
            }
            else {
                MessageBox.Show("Tidak Bisa Hapus Data ini");
            }
        }

        public void newSalesReturn()
        {
            Utilities.clearAllField(ref controlTextBox);
            datatable.Clear();
            activity = "INSERT";
            isFind = false;
            SearchSalesReturnNobutton.Visible = false;
            SearchInvoiceNObutton.Visible = true;
            AddItembutton.Enabled = false;
            foreach (Control item in controlTextBox)
            {
                if (item is RichTextBox)
                {
                    RichTextBox richtextBox = (RichTextBox)item;
                    richtextBox.ReadOnly = false;
                    richtextBox.BackColor = SystemColors.Window;
                }
                else
                {
                    item.Enabled = true;
                }
                item.BackColor = SystemColors.Window;
            }

            TotalInvoicetextBox.Enabled = false;
            
            disableDataCustomer();
            clearDataCustomer();
            SalesReturnNotextBox.Enabled = false;
            SalesReturnNotextBox.Text = DateTime.Now.ToString("yyyyMM") + "XXXX";
            InvoiceNotextBox.Enabled = true;
            SalesReturnDatetextBox.Text = DateTime.Now.ToString("dd/MM/yyyy");
            SalesReturnDatetextBox.Enabled = false;
            InvoiceDatetextBox.Enabled = false;
            SalesReturnStatustextBox.Text = Status.READY;
            SalesReturnStatustextBox.Enabled = false;
            StatustextBox.Enabled = false;

            KurstextBox.Enabled = false;
            RatetextBox.Enabled = false;
            
            RebatetextBox.Text = "0";
            TotaltextBox.Text = "0";
            GrandTotaltextBox.Text = "0";
            TotalInvoicetextBox.Text = "0";

            TotaltextBox.Enabled = false;
            GrandTotaltextBox.Enabled = false;

            MasterItemdataGridView.AllowUserToDeleteRows = true;
            AllowUserToEditRows(true);
            foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
            {
                if (column.Name.Equals("del"))
                {
                    column.Visible = true;
                    break;
                }
            }
        }

        private void disableDataCustomer(){
            CustomerIDtextBox.Enabled = false;
            NameCustomertextBox.Enabled = false;
            AddressrichTextBox.ReadOnly = true;
            NoTelptextBox.Enabled = false;
            NoFaxtextBox.Enabled = false;
            EmailtextBox.Enabled = false;
            ContactPersontextBox.Enabled = false;
            HandphonetextBox.Enabled = false;
        }

        private void clearDataCustomer(){
            CustomerIDtextBox.Text = "";
            NameCustomertextBox.Text = "";
            AddressrichTextBox.Text = "";
            NoTelptextBox.Text = "";
            NoFaxtextBox.Text = "";
            EmailtextBox.Text = "";
            ContactPersontextBox.Text = "";
            HandphonetextBox.Text = "";
        }

        public void find(){
            try
            {
                if (!SearchSalesReturnNobutton.Visible || isFind)
                {
                    Utilities.clearAllField(ref controlTextBox);
                    SearchSalesReturnNobutton.Visible = true;
                    SearchInvoiceNObutton.Visible = false;
                    foreach (Control item in controlTextBox)
                    {
                        if (!item.Name.Equals("SalesReturnNotextBox"))
                        {
                            if (item is RichTextBox)
                            {
                                RichTextBox richtextBox = (RichTextBox)item;
                                richtextBox.ReadOnly = true;
                                richtextBox.BackColor = SystemColors.Info;
                                richtextBox.Text = "";
                            }
                            else
                            {
                                item.Enabled = false;
                                item.Text = "";
                            }
                        }
                        else
                        {
                            TextBox currentTextbox = (TextBox)item;
                            currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                            currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                            currentTextbox.AutoCompleteCustomSource.AddRange(InvoiceNo.ToArray());
                            currentTextbox.Text = "";
                            currentTextbox.Enabled = true;
                        }

                        item.BackColor = SystemColors.Info;
                    }
                    reloadAllData();
                    SalesReturnNotextBox.Text = "";
                    isFind = true;
                    datatable.Clear();
                    MasterItemdataGridView.AllowUserToDeleteRows = false;
                    AllowUserToEditRows(false);
                    disableDataCustomer();
                    clearDataCustomer();
                    foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
                    {
                        if (column.Name.Equals("del"))
                        {
                            column.Visible = false;
                            break;
                        }
                    }
                    activity = "";

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public void edit(){
            if ((!SearchSalesReturnNobutton.Visible || isFind))
            {
                reloadAllData();
                Utilities.clearAllField(ref controlTextBox);
                SearchSalesReturnNobutton.Visible = true;
                SearchInvoiceNObutton.Visible = false;
                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("SalesReturnNotextBox"))
                    {
                        if (item is RichTextBox)
                        {
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = true;
                            richtextBox.BackColor = SystemColors.Info;
                        }
                        else
                        {
                            item.Enabled = false;
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.AutoCompleteCustomSource.Clear();
                        currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        currentTextbox.AutoCompleteCustomSource.AddRange(InvoiceNo.ToArray());
                        currentTextbox.Enabled = true;
                    }

                    item.BackColor = SystemColors.Info;
                    item.Text = "";
                }
                this.datatable.Clear();
                disableDataCustomer();
                clearDataCustomer();

                RebatetextBox.Enabled = true;

                isFind = true;
                MasterItemdataGridView.AllowUserToDeleteRows = true;
                AllowUserToEditRows(true);

                foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
                {
                    if (column.Name.Equals("del"))
                    {
                        column.Visible = true;
                        break;
                    }
                }

                RemarkrichTextBox.ReadOnly = false;
                RemarkrichTextBox.BackColor = SystemColors.Info;

                activity = "UPDATE";

            }
        }

        private void AllowUserToEditRows(Boolean allow)
        {
            foreach (DataGridViewColumn column in MasterItemdataGridView.Columns)
            {
                column.ReadOnly = (column.Name.Equals("RETURNQUANTITY")) ? !allow : true;
            }
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e){

        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e){
            try
            {
                Thread.Sleep(100);

                if (SalesReturnNo == null) SalesReturnNo = new List<string>();
                else SalesReturnNo.Clear();

                if (InvoiceNo == null) InvoiceNo = new List<string>();
                else InvoiceNo.Clear();

                SqlDataReader sqldataReader = connection.sqlDataReaders(String.Format("SELECT SALESRETURNID FROM H_SALES_RETURN WHERE [STATUS] = '{0}'", Status.READY));

                while (sqldataReader.Read()) SalesReturnNo.Add(sqldataReader.GetString(0));

                sqldataReader = connection.sqlDataReaders(String.Format("SELECT INVOICEID FROM H_INVOICE WHERE [STATUS] = '{0}'", Status.ACTIVE));

                while (sqldataReader.Read()) InvoiceNo.Add(sqldataReader.GetString(0));

                if (IsHandleCreated) BeginInvoke(new fillAutoCompleteTextBox(this.fillAutoComplete));
            }
            catch (Exception )
            {
                
            }
        }

        private void fillAutoComplete(){
            String tempSalesReturnNo = SalesReturnNotextBox.Text;
            SalesReturnNotextBox.AutoCompleteCustomSource.Clear();
            SalesReturnNotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            SalesReturnNotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            SalesReturnNotextBox.AutoCompleteCustomSource.AddRange(SalesReturnNo.ToArray());
            SalesReturnNotextBox.Text = tempSalesReturnNo;

            String tempInvoiceNo = InvoiceNotextBox.Text;
            InvoiceNotextBox.AutoCompleteCustomSource.Clear();
            InvoiceNotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            InvoiceNotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            InvoiceNotextBox.AutoCompleteCustomSource.AddRange(InvoiceNo.ToArray());
            InvoiceNotextBox.Text = tempInvoiceNo;
        }

        private void MasterCustomerPassingData(DataTable sender){
            CustomerIDtextBox.Text = sender.Rows[0]["CUSTOMERID"].ToString();
            NameCustomertextBox.Text = sender.Rows[0]["CUSTOMERNAME"].ToString();
            AddressrichTextBox.Text = sender.Rows[0]["ADDRESS"].ToString();
            NoTelptextBox.Text = sender.Rows[0]["NOTELP"].ToString();
            NoFaxtextBox.Text = sender.Rows[0]["FAX"].ToString();
            EmailtextBox.Text = sender.Rows[0]["EMAIL"].ToString();
            ContactPersontextBox.Text = sender.Rows[0]["CONTACTPERSON"].ToString();
            HandphonetextBox.Text = sender.Rows[0]["NOHP"].ToString();
        }

        private void AddItembutton_Click(object sender, EventArgs e)
        {
            if (itemdatagridview == null)
            {
                itemdatagridview = new Itemdatagridview("SALESRETURN", InvoiceNotextBox.Text, this.datatable);
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
            else if (itemdatagridview.IsDisposed)
            {
                itemdatagridview = new Itemdatagridview("SALESRETURN", InvoiceNotextBox.Text, this.datatable);
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
        }

        private void MasterItemPassingData(DataTable datatable){

            if (this.datatable == null) this.datatable = new DataTable();
            else
            {
                if(this.datatable.Rows.Count == 0)this.datatable.Clear();
            }

            /*remove column not use*/
            List<string> columnsRemove = new List<string>();

            foreach (DataColumn columns in datatable.Columns)
            {
                if (columns.ColumnName.Equals("ITEMNAME_ALIAS"))
                {
                    columns.ColumnName = "ITEMNAMEQTY";
                }
                else if (columns.ColumnName.Equals("ITEMID_ALIAS"))
                {
                    columns.ColumnName = "ITEMIDQTY";
                }
            }
            foreach (DataColumn column in datatable.Columns)
            {
                Boolean isExistingColumn = false;
                foreach (DataGridViewColumn columngrid in MasterItemdataGridView.Columns)
                {
                    if (column.ColumnName.Equals(columngrid.DataPropertyName))
                    {
                        isExistingColumn = true;
                        break;
                    }
                }
                if (!isExistingColumn)
                {
                    columnsRemove.Add(column.ColumnName);
                }
            }
            foreach (string columnName in columnsRemove)
            {
                datatable.Columns.Remove(columnName);
            }

            if (this.datatable.Rows.Count == 0) this.datatable = datatable;
            else
            {
                foreach (DataRow row in datatable.Rows)
                {
                    this.datatable.Rows.Add(row.ItemArray);
                }
            }

            //foreach (DataRow row in this.datatable.Rows)
            //{
            //    Object obj = (!isFind) ? "1" : row["QUANTITY"];
            //    obj = (addNewItem) ? "1" : obj;
            //    row["QUANTITY"] = (addNewItem) ? obj : row["QUANTITY"];
            //    obj = "0";
            //    row["UNITPRICE"] = (row["UNITPRICE"].ToString().Equals("")) ? obj : row["UNITPRICE"];
            //    Int64 quantity = Int64.Parse(row["QUANTITY"].ToString());
            //    Double unitprice = Double.Parse(row["UNITPRICE"].ToString());
            //    Double amount = (quantity * unitprice);
            //    row["AMOUNT"] = (Object)amount.ToString();
            //}

            foreach (DataRow row in this.datatable.Rows)
            {
                Int64 returnquantity = Int64.Parse(row["RETURNQUANTITY"].ToString());
                Double unitprice = Double.Parse(row["UNITPRICE"].ToString());
                Double amount = (returnquantity * unitprice);
                row["AMOUNT"] = (Object)amount.ToString();
            }

            Utilities.generateNoDataTable(ref this.datatable);

            MasterItemdataGridView.DataSource = this.datatable;

            Double total = 0.0;
            foreach (DataGridViewRow row in MasterItemdataGridView.Rows)
            {
                total += Double.Parse(row.Cells["AMOUNT"].Value.ToString());
            }
            TotaltextBox.Text = total.ToString();

            Double rebate = RebatetextBox.Text.Equals("") ? 0.0 : Double.Parse(RebatetextBox.Text);

            GrandTotaltextBox.Text = (total - rebate).ToString();
        }

        private void AssignItemQuantityPassingData(DataTable sender)
        {
            MasterItemdataGridView.Rows[0].Cells["ITEMIDQTY"].Value = sender.Rows[0]["ITEMID"].ToString();
            MasterItemdataGridView.Rows[0].Cells["ITEMNAMEQTY"].Value = sender.Rows[0]["ITEMNAME"].ToString();
            MasterItemdataGridView.Rows[0].Cells["STOCK"].Value = sender.Rows[0]["QUANTITY"].ToString();
        }

        private void MasterItemdataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (MasterItemdataGridView.Columns[e.ColumnIndex].Name.Equals("del"))
                {
                    if (datatable.Rows.Count == 1)
                    {
                        //TotalInvoicetextBox.Text = "0";
                        //GrandTotaltextBox.Text = "0";
                    }
                    this.datatable.Rows.RemoveAt(e.RowIndex);
                    Utilities.generateNoDataTable(ref datatable);
                    MasterItemdataGridView.DataSource = this.datatable;
                }
                //else if ((MasterItemdataGridView.Columns[e.ColumnIndex].Name.Equals("ITEMIDQTY") || MasterItemdataGridView.Columns[e.ColumnIndex].Name.Equals("ITEMNAMEQTY")) && (activity.Equals("INSERT") || activity.Equals("UPDATE")))
                //{
                //    String ITEMID = MasterItemdataGridView.Rows[e.RowIndex].Cells["ITEMID"].Value.ToString();
                //    if (itemdatagridview == null)
                //    {
                //        itemdatagridview = new Itemdatagridview("ASSIGNITEMPURCHASERECEIPT", ITEMID);
                //        itemdatagridview.assignItemQuantityPassingData = new Itemdatagridview.AssignItemQuantityPassingData(AssignItemQuantityPassingData);
                //        itemdatagridview.ShowDialog();
                //    }
                //    else if (itemdatagridview.IsDisposed)
                //    {
                //        itemdatagridview = new Itemdatagridview("ASSIGNITEMPURCHASERECEIPT", ITEMID);
                //        itemdatagridview.assignItemQuantityPassingData = new Itemdatagridview.AssignItemQuantityPassingData(AssignItemQuantityPassingData);
                //        itemdatagridview.ShowDialog();
                //    }
                //}
            }
        }

        private void MasterItemdataGridView_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            for (int i = 0; i < datatable.Rows.Count; i++)
            {
                if (datatable.Rows[i].RowState == DataRowState.Deleted)
                {
                    datatable.Rows.RemoveAt(i);
                }
            }
            Utilities.generateNoDataTable(ref datatable);
            Double total = 0.0;
            foreach (DataGridViewRow row in MasterItemdataGridView.Rows)
            {
                total += Double.Parse(row.Cells["AMOUNT"].Value.ToString());
            }
            TotaltextBox.Text = total.ToString();

            Double rebate = RebatetextBox.Text.Equals("") ? 0.0 : Double.Parse(RebatetextBox.Text);

            GrandTotaltextBox.Text = (Double.Parse(TotaltextBox.Text) - rebate).ToString();
        }

        private void MasterItemdataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
        }

        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            int currColumn = MasterItemdataGridView.CurrentCell.ColumnIndex;
            int currRow = MasterItemdataGridView.CurrentCell.RowIndex;
            if (MasterItemdataGridView.Columns[currColumn].Name.Equals("QUANTITY"))
            {
                e.Handled = Utilities.onlyNumberTextBox(sender, (KeyPressEventArgs)e);
            }
            else if (MasterItemdataGridView.Columns[currColumn].Name.Equals("UNITPRICE"))
            {
                //e.Handled = Utilities.onlyNumberTextBox(sender, (KeyPressEventArgs)e);
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
                {
                    e.Handled = true;
                }
            }
        }

        private void MasterItemdataGridView_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Int64 returnqty = (MasterItemdataGridView.Rows[e.RowIndex].Cells["RETURNQUANTITY"].Value == null || MasterItemdataGridView.Rows[e.RowIndex].Cells["RETURNQUANTITY"].Value.ToString().Equals("")) ? 0 : Int64.Parse(MasterItemdataGridView.Rows[e.RowIndex].Cells["RETURNQUANTITY"].Value.ToString());
                Int64 quantity = (MasterItemdataGridView.Rows[e.RowIndex].Cells["QUANTITY"].Value == null || MasterItemdataGridView.Rows[e.RowIndex].Cells["QUANTITY"].Value.ToString().Equals("")) ? 0 : Int64.Parse(MasterItemdataGridView.Rows[e.RowIndex].Cells["QUANTITY"].Value.ToString());
                Double unitprice = (MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value == null || MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value.ToString().Equals("")) ? 0 : Double.Parse(MasterItemdataGridView.Rows[e.RowIndex].Cells["UNITPRICE"].Value.ToString());
                if (returnqty >= quantity)
                {
                    MasterItemdataGridView.Rows[e.RowIndex].Cells["RETURNQUANTITY"].Value = quantity;
                    returnqty = Int64.Parse(MasterItemdataGridView.Rows[e.RowIndex].Cells["RETURNQUANTITY"].Value.ToString());
                }
                else if (returnqty == 0)
                {
                    MasterItemdataGridView.Rows[e.RowIndex].Cells["RETURNQUANTITY"].Value = 1;
                    returnqty = Int64.Parse(MasterItemdataGridView.Rows[e.RowIndex].Cells["RETURNQUANTITY"].Value.ToString());
                }
                
                //unitprice = 1000;
                Double amount = (returnqty * unitprice);
                MasterItemdataGridView.Rows[e.RowIndex].Cells["AMOUNT"].Value = amount;//.ToString("N2");

                Double total = 0.0;
                foreach (DataGridViewRow row in MasterItemdataGridView.Rows)
                {
                    total += Double.Parse(row.Cells["AMOUNT"].Value.ToString());
                }
                TotaltextBox.Text = total.ToString();

                Double rebate = RebatetextBox.DecimalValue;

                GrandTotaltextBox.Text = (total - rebate).ToString();

            }
            catch (Exception ex)
            {
            }
            
        }

        private void InvoicepassingData(DataTable sender)
        {
            if (sender.Rows.Count == 1)
            {
                AddItembutton.Enabled = true;
                InvoiceNotextBox.Text = sender.Rows[0]["INVOICEID"].ToString();
                InvoiceDatetextBox.Text = sender.Rows[0]["INVOICEDATE"].ToString();
                CustomerIDtextBox.Text = sender.Rows[0]["CUSTOMERID"].ToString();
                KurstextBox.Text = sender.Rows[0]["CURRENCYID"].ToString();
                RatetextBox.Text = sender.Rows[0]["RATE"].ToString();
                StatustextBox.Text = sender.Rows[0]["STATUS"].ToString();
                TotalInvoicetextBox.Text = sender.Rows[0]["TOTAL"].ToString();
                //RemarkrichTextBox.Text = sender.Rows[0]["REMARK"].ToString();

                String query = String.Format("SELECT CUSTOMERID, CUSTOMERNAME, ADDRESS, NOTELP, FAX, EMAIL, CONTACTPERSON, NOHP " +
                                             "FROM M_CUSTOMER " +
                                             "WHERE CUSTOMERID = '{0}'", CustomerIDtextBox.Text);

                DataTable datatable = connection.openDataTableQuery(query);
                MasterCustomerPassingData(datatable);

                //sender.Rows[0]["CREATEDBY"].ToString();
                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMID, B.ITEMNAME, " +
                                     "A.ITEMIDQTY,  " +
                                     "( " +
                                     "   SELECT ITEMNAME FROM M_ITEM " +
                                     "   WHERE ITEMID = A.ITEMID " +
                                     ") AS ITEMNAMEQTY, " +
                                     "A.QUANTITY,A.QUANTITY AS RETURNQUANTITY, UOMID, PRICE AS UNITPRICE, '0' AS AMOUNT " +
                                     "FROM D_INVOICE A " +
                                     "JOIN M_ITEM B " +
                                     "ON A.ITEMID = B.ITEMID " +
                                     "WHERE INVOICEID = '{0}'", InvoiceNotextBox.Text);
                datatable = connection.openDataTableQuery(query);
                this.datatable.Clear();
                MasterItemPassingData(datatable);
            }

        }

        private void SalesReturn_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }

        public void confirm() {
            if (isFind && SalesReturnStatustextBox.Text.Equals(Status.READY) && activity.Equals(""))
            {
                List<SqlParameter> sqlParam = new List<SqlParameter>();
                sqlParam.Add(new SqlParameter("@SALESRETURNID", SalesReturnNotextBox.Text));
                sqlParam.Add(new SqlParameter("@STATUS", Status.CONFIRM));
                sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));

                if (connection.callProcedure("UPDATE_STATUS_H_SALES_RETURN", sqlParam))
                {
                    sqlParam = new List<SqlParameter>();
                    sqlParam.Add(new SqlParameter("@SALESRETURNID", SalesReturnNotextBox.Text));
                    if (connection.callProcedure("INSERT_LOG_TRANSACTION_SALES_RETURN", sqlParam))
                    {
                        MessageBox.Show("Confirm, stock masuk kembali.");
                        find();
                    }
                }
            }
        }

        private void clearSalesReturnData() {
            newSalesReturn();
        }

        public void cancel() {
            if (isFind && StatustextBox.Text.Equals(Status.RELEASE))
            {
                List<SqlParameter> sqlParam = new List<SqlParameter>();
                sqlParam.Add(new SqlParameter("@SALESORDERID", InvoiceNotextBox.Text));
                sqlParam.Add(new SqlParameter("@STATUS", Status.READY));

                if (connection.callProcedure("UPDATE_STATUS_H_SALES_ORDER", sqlParam))
                {
                    String query = String.Format("SELECT ORDERPICKINGID FROM H_ORDER_PICKING WHERE " +
                                                 "SALESORDERID = '{0}' AND STATUS = '{1}'", InvoiceNotextBox.Text, Status.OPEN);
                    DataTable dt = connection.openDataTableQuery(query);
                    if (dt.Rows.Count != 0)
                    {
                        sqlParam = new List<SqlParameter>();
                        sqlParam.Add(new SqlParameter("@ORDERPICKINGID", dt.Rows[0]["ORDERPICKINGID"].ToString()));
                        sqlParam.Add(new SqlParameter("@STATUS", Status.CANCEL));
                        if (connection.callProcedure("UPDATE_STATUS_H_ORDER_PICKING", sqlParam))
                        {
                            MessageBox.Show("SUCCESS UPDATE SO READY AND OP CANCEL");
                        }
                    }
                }
            }
        }

        private void SalesReturn_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            reloadAllData();
            Main.ACTIVEFORM = this;
        }

        private void InvoiceNotextBox_Leave(object sender, EventArgs e)
        {
                Boolean isExistingPartNo = false;
                foreach (String item in InvoiceNo)
                {
                    if (item.ToUpper().Equals(InvoiceNotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;

                        String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY INVOICEID) AS 'NO', INVOICEID, " +
                                                      "CONVERT(VARCHAR(10), INVOICEDATE, 103) AS [INVOICEDATE], " +
                                                      "CUSTOMERID, CURRENCYID, RATE, PAYMENTMETHOD, DISCOUNT, TOTAL, REMARK, STATUS, CREATEDBY " +
                                                      "FROM H_INVOICE " +
                                                      "WHERE INVOICEID = '{0}' " +
                                                      "ORDER BY INVOICEID ASC", InvoiceNotextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        
                        InvoicepassingData(datatable);
//                        setDecimalN2();
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    AddItembutton.Enabled = false;
                    InvoiceNotextBox.Text = "";
                    Utilities.clearAllField(ref controlTextBox);
                    this.datatable.Clear();
                    clearDataCustomer();
                    SalesReturnNotextBox.Text = DateTime.Now.ToString("yyyyMM") + "XXXX";
                    SalesReturnDatetextBox.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    SalesReturnStatustextBox.Text = Status.READY;
                }
                
        }

        private void InvoiceNotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingSONo = false;
                this.datatable.Clear();
                foreach (String item in InvoiceNo)
                {
                    if (item.ToUpper().Equals(InvoiceNotextBox.Text.ToUpper()))
                    {
                        isExistingSONo = true;
                        InvoiceNotextBox.Text = InvoiceNotextBox.Text.ToUpper();

                        String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY INVOICEID) AS 'NO', INVOICEID, " +
                                                     "CONVERT(VARCHAR(10), INVOICEDATE, 103) AS [INVOICEDATE], " +
                                                     "CUSTOMERID, CURRENCYID, RATE, PAYMENTMETHOD, DISCOUNT, TOTAL, REMARK, STATUS, CREATEDBY " +
                                                     "FROM H_INVOICE " +
                                                     "WHERE INVOICEID = '{0}' " +
                                                     "ORDER BY INVOICEID ASC", InvoiceNotextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        InvoicepassingData(datatable);
//                        setDecimalN2();
                        break;
                    }
                }

                if (!isExistingSONo)
                {
                    AddItembutton.Enabled = false;
                    InvoiceNotextBox.Text = "";
                    Utilities.clearAllField(ref controlTextBox);
                    this.datatable.Clear();
                    clearDataCustomer();
                    SalesReturnNotextBox.Text = DateTime.Now.ToString("yyyyMM") + "XXXX";
                    SalesReturnDatetextBox.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    SalesReturnStatustextBox.Text = Status.READY;
                }
                
            }
        }

        private void SalesReturnNotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Boolean isExistingSONo = false;
                this.datatable.Clear();
                foreach (String item in SalesReturnNo)
                {
                    if (item.ToUpper().Equals(SalesReturnNotextBox.Text.ToUpper()))
                    {
                        isExistingSONo = true;
                        SalesReturnNotextBox.Text = SalesReturnNotextBox.Text.ToUpper();

                        String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SALESRETURNID) AS 'NO',SALESRETURNID,CONVERT(VARCHAR(10), SALESRETURNDATE, 103) AS SALESRETURNDATE, " +
                                                     "A.INVOICEID, CONVERT(VARCHAR(10), INVOICEDATE, 103) AS INVOICEDATE, " +
                                                     "B.CUSTOMERID, C.CUSTOMERNAME, D.EMPLOYEENAME, B.CURRENCYID, B.RATE, A.REBATE, A.TOTAL, B.TOTAL AS TOTALINVOICE, A.REMARK, A.STATUS, D.EMPLOYEEID " +
                                                     "FROM H_SALES_RETURN A " +
                                                     "JOIN H_INVOICE B " +
                                                     "ON A.INVOICEID = B.INVOICEID " +
                                                     "JOIN M_CUSTOMER C " +
                                                     "ON B.CUSTOMERID = C.CUSTOMERID " +
                                                     "JOIN M_EMPLOYEE D " +
                                                     "ON A.CREATEDBY = D.EMPLOYEEID " +
                                                     "WHERE SALESRETURNID = '{0}' " +
                                                     "ORDER BY SALESRETURNID ASC", SalesReturnNotextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        SalesReturnpassingData(datatable);
//                        setDecimalN2();
                        break;
                    }
                }

                if (!isExistingSONo)
                {
                    AddItembutton.Enabled = false;
                    SalesReturnNotextBox.Text = "";
                    Utilities.clearAllField(ref controlTextBox);
                    this.datatable.Clear();
                    clearDataCustomer();
                }
               
            }
        }

        private void SalesReturnNotextBox_Leave(object sender, EventArgs e)
        {
            if (isFind)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in SalesReturnNo)
                {
                    if (item.ToUpper().Equals(SalesReturnNotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;

                        String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SALESRETURNID) AS 'NO',SALESRETURNID,CONVERT(VARCHAR(10), SALESRETURNDATE, 103) AS SALESRETURNDATE, " +
                                                     "A.INVOICEID, CONVERT(VARCHAR(10), INVOICEDATE, 103) AS INVOICEDATE, " +
                                                     "B.CUSTOMERID, C.CUSTOMERNAME, D.EMPLOYEENAME, B.CURRENCYID, B.RATE, A.REBATE, A.TOTAL, B.TOTAL AS TOTALINVOICE, A.REMARK, A.STATUS, D.EMPLOYEEID " +
                                                     "FROM H_SALES_RETURN A " +
                                                     "JOIN H_INVOICE B " +
                                                     "ON A.INVOICEID = B.INVOICEID " +
                                                     "JOIN M_CUSTOMER C " +
                                                     "ON B.CUSTOMERID = C.CUSTOMERID " +
                                                     "JOIN M_EMPLOYEE D " +
                                                     "ON A.CREATEDBY = D.EMPLOYEEID " +
                                                     "WHERE SALESRETURNID = '{0}' " +
                                                     "ORDER BY SALESRETURNID ASC", SalesReturnNotextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        SalesReturnpassingData(datatable);
//                        setDecimalN2();
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    AddItembutton.Enabled = false;
                    SalesReturnNotextBox.Text = "";
                    Utilities.clearAllField(ref controlTextBox);
                    this.datatable.Clear();
                    clearDataCustomer();
                }
                
            }
        }

        private void SearchInvoiceNObutton_Click(object sender, EventArgs e)
        {
            if (invoicedatagridview == null)
            {
                invoicedatagridview = new Invoicedatagridview("Invoice");
                invoicedatagridview.invoicePassingData = new Invoicedatagridview.InvoicePassingData(InvoicepassingData);
                invoicedatagridview.ShowDialog();
            }
            else if (invoicedatagridview.IsDisposed)
            {
                invoicedatagridview = new Invoicedatagridview("Invoice");
                invoicedatagridview.invoicePassingData = new Invoicedatagridview.InvoicePassingData(InvoicepassingData);
                invoicedatagridview.ShowDialog();
            }

//            setDecimalN2();
        }

        private void SearchSalesReturnNobutton_Click(object sender, EventArgs e)
        {
            if (salesreturndatagridview == null)
            {
                salesreturndatagridview = new SalesReturndatagridview("SalesReturn");
                salesreturndatagridview.salesreturnPassingData = new SalesReturndatagridview.SalesReturnPassingData(SalesReturnpassingData);
                salesreturndatagridview.ShowDialog();
            }
            else if (salesreturndatagridview.IsDisposed)
            {
                salesreturndatagridview = new SalesReturndatagridview("SalesReturn");
                salesreturndatagridview.salesreturnPassingData = new SalesReturndatagridview.SalesReturnPassingData(SalesReturnpassingData);
                salesreturndatagridview.ShowDialog();
            }
//            setDecimalN2();
        }

        private void SalesReturnpassingData(DataTable sender) {
            if (sender.Rows.Count == 1)
            {
                AddItembutton.Enabled = activity.Equals("UPDATE");
                SalesReturnNotextBox.Text = sender.Rows[0]["SALESRETURNID"].ToString();
                SalesReturnDatetextBox.Text = sender.Rows[0]["SALESRETURNDATE"].ToString();
                SalesReturnStatustextBox.Text = sender.Rows[0]["STATUS"].ToString();
                InvoiceNotextBox.Text = sender.Rows[0]["INVOICEID"].ToString();
                InvoiceDatetextBox.Text = sender.Rows[0]["INVOICEDATE"].ToString();
                TotalInvoicetextBox.Text = sender.Rows[0]["TOTALINVOICE"].ToString();
                KurstextBox.Text = sender.Rows[0]["CURRENCYID"].ToString();
                RatetextBox.Text = sender.Rows[0]["RATE"].ToString();

                RemarkrichTextBox.Text = sender.Rows[0]["REMARK"].ToString();
                RebatetextBox.Text = sender.Rows[0]["REBATE"].ToString();
                GrandTotaltextBox.Text = sender.Rows[0]["TOTAL"].ToString();
                
                CustomerIDtextBox.Text = sender.Rows[0]["CUSTOMERID"].ToString();
                String query = String.Format("SELECT CUSTOMERID, CUSTOMERNAME, ADDRESS, NOTELP, FAX, EMAIL, CONTACTPERSON, NOHP " +
                                             "FROM M_CUSTOMER " +
                                             "WHERE CUSTOMERID = '{0}'", CustomerIDtextBox.Text);

                DataTable datatable = connection.openDataTableQuery(query);
                MasterCustomerPassingData(datatable);

                query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY A.ITEMID) AS 'NO', A.ITEMID, D.ITEMNAME, " +
                                      "A.ITEMIDQTY, " +
                                      "( " +
                                      "   SELECT ITEMNAME FROM M_ITEM " +
                                      "   WHERE ITEMID = A.ITEMIDQTY " +
                                      ") AS ITEMNAMEQTY, " +
                                      "C.QUANTITY, " +
                                      "A.QUANTITY AS RETURNQUANTITY, UOMID, A.PRICE AS UNITPRICE, '0' AS AMOUNT " +
                                      "FROM D_SALES_RETURN A " +
                                      "JOIN H_SALES_RETURN B " +
                                      "ON A.SALESRETURNID = B.SALESRETURNID " +
                                      "JOIN D_INVOICE C " +
                                      "ON B.INVOICEID = C.INVOICEID AND C.ITEMID = A.ITEMID " +
                                      "JOIN M_ITEM D " +
                                      "ON A.ITEMID = D.ITEMID " +
                                      "WHERE A.SALESRETURNID = '{0}'", SalesReturnNotextBox.Text);
                datatable = connection.openDataTableQuery(query);
                this.datatable.Clear();
                MasterItemPassingData(datatable);
            }
        }

        private void RebatetextBox_TextChanged(object sender, EventArgs e)
        {
            Double rebate = RebatetextBox.DecimalValue;
            Double total = TotaltextBox.DecimalValue;
            GrandTotaltextBox.Text = (total - rebate).ToString();
        }

        private void setDecimalN2()
        {
            foreach (DataGridViewRow row in MasterItemdataGridView.Rows)
            {
                decimal nilai = Decimal.Parse(row.Cells["AMOUNT"].Value.ToString());
                row.Cells["AMOUNT"].Value = nilai.ToString("N2");
            }
        }

        private void MasterItemdataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            //if (e.ColumnIndex == 11 || e.ColumnIndex == 12)
            //{
            //    Decimal d = Decimal.Parse(e.Value.ToString());
            //    e.Value = d.ToString("N2");
            //}

            if (e.ColumnIndex == 11 || e.ColumnIndex == 12)
            {
                try
                {
                    Decimal d = Decimal.Parse(e.Value.ToString());
                    e.Value = d.ToString("N2");
                }
                catch (Exception ex)
                {
                    Decimal d = 0;
                    e.Value = d.ToString("N2");
                }
            }
        }

        private void MasterItemdataGridView_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            for (int i = 0; i < datatable.Rows.Count; i++)
            {
                if (datatable.Rows[i].RowState == DataRowState.Deleted)
                {
                    datatable.Rows.RemoveAt(i);
                }
            }
            Utilities.generateNoDataTable(ref datatable);
            Double total = 0.0;
            foreach (DataGridViewRow row in MasterItemdataGridView.Rows)
            {
                total += Double.Parse(row.Cells["AMOUNT"].Value.ToString());
            }
            TotaltextBox.Text = total.ToString();

            Double rebate = RebatetextBox.Text.Equals("") ? 0.0 : Double.Parse(RebatetextBox.Text);

            GrandTotaltextBox.Text = (Double.Parse(TotaltextBox.Text) - rebate).ToString();
        }
    }
}