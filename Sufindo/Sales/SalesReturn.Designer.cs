﻿namespace Sufindo.Sales
{
    partial class SalesReturn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SalesReturn));
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.KurstextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.NameCustomertextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.AddressrichTextBox = new System.Windows.Forms.RichTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.NoTelptextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.NoFaxtextBox = new System.Windows.Forms.TextBox();
            this.ContactPersontextBox = new System.Windows.Forms.TextBox();
            this.EmailtextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.HandphonetextBox = new System.Windows.Forms.TextBox();
            this.CustomerIDtextBox = new System.Windows.Forms.TextBox();
            this.InvoiceNotextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.SalesReturnNotextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.SearchSalesReturnNobutton = new System.Windows.Forms.Button();
            this.SearchInvoiceNObutton = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SalesReturnDatetextBox = new System.Windows.Forms.TextBox();
            this.InvoiceDatetextBox = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.SalesReturnStatustextBox = new System.Windows.Forms.TextBox();
            this.StatustextBox = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.datagridviewpanel = new System.Windows.Forms.Panel();
            this.MasterItemdataGridView = new System.Windows.Forms.DataGridView();
            this.NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMID_ALIAS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMIDQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMNAMEQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STOCK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUANTITY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RETURNQUANTITY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UNITPRICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.del = new System.Windows.Forms.DataGridViewImageColumn();
            this.AddItembutton = new System.Windows.Forms.Button();
            this.Spesifikasipanel = new System.Windows.Forms.Panel();
            this.RemarkrichTextBox = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBoxAlias = new System.Windows.Forms.CheckBox();
            this.RatetextBox = new CustomControls.TextBoxSL();
            this.TotalInvoicetextBox = new CustomControls.TextBoxSL();
            this.TotaltextBox = new CustomControls.TextBoxSL();
            this.RebatetextBox = new CustomControls.TextBoxSL();
            this.GrandTotaltextBox = new CustomControls.TextBoxSL();
            this.tableLayoutPanel.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.datagridviewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MasterItemdataGridView)).BeginInit();
            this.Spesifikasipanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 10;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.Controls.Add(this.KurstextBox, 1, 4);
            this.tableLayoutPanel.Controls.Add(this.label6, 0, 5);
            this.tableLayoutPanel.Controls.Add(this.label7, 0, 4);
            this.tableLayoutPanel.Controls.Add(this.groupBox1, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.CustomerIDtextBox, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.InvoiceNotextBox, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.label18, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.SalesReturnNotextBox, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.label10, 4, 5);
            this.tableLayoutPanel.Controls.Add(this.SearchSalesReturnNobutton, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.SearchInvoiceNObutton, 2, 1);
            this.tableLayoutPanel.Controls.Add(this.label19, 3, 0);
            this.tableLayoutPanel.Controls.Add(this.label3, 3, 1);
            this.tableLayoutPanel.Controls.Add(this.SalesReturnDatetextBox, 4, 0);
            this.tableLayoutPanel.Controls.Add(this.InvoiceDatetextBox, 4, 1);
            this.tableLayoutPanel.Controls.Add(this.label20, 6, 0);
            this.tableLayoutPanel.Controls.Add(this.label9, 6, 1);
            this.tableLayoutPanel.Controls.Add(this.SalesReturnStatustextBox, 7, 0);
            this.tableLayoutPanel.Controls.Add(this.StatustextBox, 7, 1);
            this.tableLayoutPanel.Controls.Add(this.label21, 5, 10);
            this.tableLayoutPanel.Controls.Add(this.label16, 5, 9);
            this.tableLayoutPanel.Controls.Add(this.label17, 5, 8);
            this.tableLayoutPanel.Controls.Add(this.datagridviewpanel, 0, 7);
            this.tableLayoutPanel.Controls.Add(this.AddItembutton, 0, 6);
            this.tableLayoutPanel.Controls.Add(this.Spesifikasipanel, 1, 8);
            this.tableLayoutPanel.Controls.Add(this.label1, 0, 8);
            this.tableLayoutPanel.Controls.Add(this.checkBoxAlias, 5, 11);
            this.tableLayoutPanel.Controls.Add(this.RatetextBox, 1, 5);
            this.tableLayoutPanel.Controls.Add(this.TotalInvoicetextBox, 5, 5);
            this.tableLayoutPanel.Controls.Add(this.TotaltextBox, 6, 8);
            this.tableLayoutPanel.Controls.Add(this.RebatetextBox, 6, 9);
            this.tableLayoutPanel.Controls.Add(this.GrandTotaltextBox, 6, 10);
            this.tableLayoutPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.tableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 12;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(994, 632);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // KurstextBox
            // 
            this.KurstextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.KurstextBox.Location = new System.Drawing.Point(94, 249);
            this.KurstextBox.Name = "KurstextBox";
            this.KurstextBox.Size = new System.Drawing.Size(137, 20);
            this.KurstextBox.TabIndex = 17;
            this.KurstextBox.Tag = "7";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 272);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Rate";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 246);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Currency";
            // 
            // groupBox1
            // 
            this.tableLayoutPanel.SetColumnSpan(this.groupBox1, 10);
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(3, 85);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(759, 158);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data Customer";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.6726F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.3274F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 157F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 127F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 237F));
            this.tableLayoutPanel1.Controls.Add(this.NameCustomertextBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.NoTelptextBox, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label12, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label13, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.NoFaxtextBox, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.ContactPersontextBox, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.EmailtextBox, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.label14, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.label15, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.HandphonetextBox, 4, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(746, 131);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // NameCustomertextBox
            // 
            this.NameCustomertextBox.BackColor = System.Drawing.Color.White;
            this.NameCustomertextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NameCustomertextBox.Location = new System.Drawing.Point(74, 3);
            this.NameCustomertextBox.Name = "NameCustomertextBox";
            this.NameCustomertextBox.Size = new System.Drawing.Size(147, 22);
            this.NameCustomertextBox.TabIndex = 10;
            this.NameCustomertextBox.Tag = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "Name";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 29);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 29);
            this.label8.TabIndex = 9;
            this.label8.Text = "Address";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel1.SetColumnSpan(this.panel1, 2);
            this.panel1.Controls.Add(this.AddressrichTextBox);
            this.panel1.Location = new System.Drawing.Point(74, 32);
            this.panel1.Name = "panel1";
            this.tableLayoutPanel1.SetRowSpan(this.panel1, 2);
            this.panel1.Size = new System.Drawing.Size(304, 65);
            this.panel1.TabIndex = 9;
            // 
            // AddressrichTextBox
            // 
            this.AddressrichTextBox.BackColor = System.Drawing.Color.White;
            this.AddressrichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.AddressrichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddressrichTextBox.Location = new System.Drawing.Point(0, 0);
            this.AddressrichTextBox.Name = "AddressrichTextBox";
            this.AddressrichTextBox.Size = new System.Drawing.Size(302, 63);
            this.AddressrichTextBox.TabIndex = 11;
            this.AddressrichTextBox.TabStop = false;
            this.AddressrichTextBox.Tag = "";
            this.AddressrichTextBox.Text = "";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(3, 100);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 20);
            this.label11.TabIndex = 10;
            this.label11.Text = "No Telp";
            // 
            // NoTelptextBox
            // 
            this.NoTelptextBox.BackColor = System.Drawing.Color.White;
            this.NoTelptextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NoTelptextBox.Location = new System.Drawing.Point(74, 103);
            this.NoTelptextBox.Name = "NoTelptextBox";
            this.NoTelptextBox.Size = new System.Drawing.Size(147, 22);
            this.NoTelptextBox.TabIndex = 12;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(384, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 20);
            this.label12.TabIndex = 12;
            this.label12.Text = "Fax";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(384, 29);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(119, 20);
            this.label13.TabIndex = 14;
            this.label13.Text = "Contact Person";
            // 
            // NoFaxtextBox
            // 
            this.NoFaxtextBox.BackColor = System.Drawing.Color.White;
            this.NoFaxtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NoFaxtextBox.Location = new System.Drawing.Point(511, 3);
            this.NoFaxtextBox.Name = "NoFaxtextBox";
            this.NoFaxtextBox.Size = new System.Drawing.Size(166, 22);
            this.NoFaxtextBox.TabIndex = 13;
            // 
            // ContactPersontextBox
            // 
            this.ContactPersontextBox.BackColor = System.Drawing.Color.White;
            this.ContactPersontextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ContactPersontextBox.Location = new System.Drawing.Point(511, 32);
            this.ContactPersontextBox.Name = "ContactPersontextBox";
            this.ContactPersontextBox.Size = new System.Drawing.Size(166, 22);
            this.ContactPersontextBox.TabIndex = 14;
            // 
            // EmailtextBox
            // 
            this.EmailtextBox.BackColor = System.Drawing.Color.White;
            this.EmailtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.EmailtextBox.Location = new System.Drawing.Point(511, 61);
            this.EmailtextBox.Name = "EmailtextBox";
            this.EmailtextBox.Size = new System.Drawing.Size(166, 22);
            this.EmailtextBox.TabIndex = 15;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(384, 100);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(93, 20);
            this.label14.TabIndex = 15;
            this.label14.Text = "Handphone";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(384, 58);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(48, 20);
            this.label15.TabIndex = 19;
            this.label15.Text = "Email";
            // 
            // HandphonetextBox
            // 
            this.HandphonetextBox.BackColor = System.Drawing.Color.White;
            this.HandphonetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.HandphonetextBox.Location = new System.Drawing.Point(511, 103);
            this.HandphonetextBox.Name = "HandphonetextBox";
            this.HandphonetextBox.Size = new System.Drawing.Size(166, 22);
            this.HandphonetextBox.TabIndex = 16;
            // 
            // CustomerIDtextBox
            // 
            this.CustomerIDtextBox.BackColor = System.Drawing.Color.White;
            this.CustomerIDtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CustomerIDtextBox.Enabled = false;
            this.CustomerIDtextBox.Location = new System.Drawing.Point(94, 59);
            this.CustomerIDtextBox.Name = "CustomerIDtextBox";
            this.CustomerIDtextBox.Size = new System.Drawing.Size(137, 20);
            this.CustomerIDtextBox.TabIndex = 9;
            this.CustomerIDtextBox.Tag = "";
            // 
            // InvoiceNotextBox
            // 
            this.InvoiceNotextBox.BackColor = System.Drawing.Color.White;
            this.InvoiceNotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.InvoiceNotextBox.Location = new System.Drawing.Point(94, 31);
            this.InvoiceNotextBox.Name = "InvoiceNotextBox";
            this.InvoiceNotextBox.Size = new System.Drawing.Size(137, 20);
            this.InvoiceNotextBox.TabIndex = 5;
            this.InvoiceNotextBox.Tag = "";
            this.InvoiceNotextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.InvoiceNotextBox_KeyDown);
            this.InvoiceNotextBox.Leave += new System.EventHandler(this.InvoiceNotextBox_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 28;
            this.label4.Text = "Customer ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Invoice No";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(3, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(85, 13);
            this.label18.TabIndex = 35;
            this.label18.Text = "Sales Return No";
            // 
            // SalesReturnNotextBox
            // 
            this.SalesReturnNotextBox.BackColor = System.Drawing.Color.White;
            this.SalesReturnNotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SalesReturnNotextBox.Enabled = false;
            this.SalesReturnNotextBox.Location = new System.Drawing.Point(94, 3);
            this.SalesReturnNotextBox.Name = "SalesReturnNotextBox";
            this.SalesReturnNotextBox.Size = new System.Drawing.Size(137, 20);
            this.SalesReturnNotextBox.TabIndex = 1;
            this.SalesReturnNotextBox.Tag = "";
            this.SalesReturnNotextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SalesReturnNotextBox_KeyDown);
            this.SalesReturnNotextBox.Leave += new System.EventHandler(this.SalesReturnNotextBox_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(367, 272);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "Total Invoice";
            // 
            // SearchSalesReturnNobutton
            // 
            this.SearchSalesReturnNobutton.BackColor = System.Drawing.Color.Transparent;
            this.SearchSalesReturnNobutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.SearchSalesReturnNobutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SearchSalesReturnNobutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.SearchSalesReturnNobutton.FlatAppearance.BorderSize = 0;
            this.SearchSalesReturnNobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchSalesReturnNobutton.ForeColor = System.Drawing.Color.Transparent;
            this.SearchSalesReturnNobutton.Location = new System.Drawing.Point(237, 1);
            this.SearchSalesReturnNobutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.SearchSalesReturnNobutton.Name = "SearchSalesReturnNobutton";
            this.SearchSalesReturnNobutton.Size = new System.Drawing.Size(24, 24);
            this.SearchSalesReturnNobutton.TabIndex = 2;
            this.SearchSalesReturnNobutton.Tag = "";
            this.SearchSalesReturnNobutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.SearchSalesReturnNobutton.UseVisualStyleBackColor = false;
            this.SearchSalesReturnNobutton.Visible = false;
            this.SearchSalesReturnNobutton.Click += new System.EventHandler(this.SearchSalesReturnNobutton_Click);
            // 
            // SearchInvoiceNObutton
            // 
            this.SearchInvoiceNObutton.BackColor = System.Drawing.Color.Transparent;
            this.SearchInvoiceNObutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.SearchInvoiceNObutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SearchInvoiceNObutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.SearchInvoiceNObutton.FlatAppearance.BorderSize = 0;
            this.SearchInvoiceNObutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchInvoiceNObutton.ForeColor = System.Drawing.Color.Transparent;
            this.SearchInvoiceNObutton.Location = new System.Drawing.Point(237, 29);
            this.SearchInvoiceNObutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.SearchInvoiceNObutton.Name = "SearchInvoiceNObutton";
            this.SearchInvoiceNObutton.Size = new System.Drawing.Size(24, 24);
            this.SearchInvoiceNObutton.TabIndex = 6;
            this.SearchInvoiceNObutton.Tag = "";
            this.SearchInvoiceNObutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.SearchInvoiceNObutton.UseVisualStyleBackColor = false;
            this.SearchInvoiceNObutton.Click += new System.EventHandler(this.SearchInvoiceNObutton_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(267, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(94, 13);
            this.label19.TabIndex = 38;
            this.label19.Text = "Sales Return Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(267, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Invoice Date";
            // 
            // SalesReturnDatetextBox
            // 
            this.SalesReturnDatetextBox.BackColor = System.Drawing.Color.White;
            this.SalesReturnDatetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.SalesReturnDatetextBox, 2);
            this.SalesReturnDatetextBox.Enabled = false;
            this.SalesReturnDatetextBox.Location = new System.Drawing.Point(367, 3);
            this.SalesReturnDatetextBox.Name = "SalesReturnDatetextBox";
            this.SalesReturnDatetextBox.Size = new System.Drawing.Size(140, 20);
            this.SalesReturnDatetextBox.TabIndex = 3;
            this.SalesReturnDatetextBox.Tag = "0";
            // 
            // InvoiceDatetextBox
            // 
            this.InvoiceDatetextBox.BackColor = System.Drawing.Color.White;
            this.InvoiceDatetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.InvoiceDatetextBox, 2);
            this.InvoiceDatetextBox.Enabled = false;
            this.InvoiceDatetextBox.Location = new System.Drawing.Point(367, 31);
            this.InvoiceDatetextBox.Name = "InvoiceDatetextBox";
            this.InvoiceDatetextBox.Size = new System.Drawing.Size(140, 20);
            this.InvoiceDatetextBox.TabIndex = 7;
            this.InvoiceDatetextBox.Tag = "0";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(556, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(37, 13);
            this.label20.TabIndex = 39;
            this.label20.Text = "Status";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(556, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Status";
            this.label9.Visible = false;
            // 
            // SalesReturnStatustextBox
            // 
            this.SalesReturnStatustextBox.BackColor = System.Drawing.Color.White;
            this.SalesReturnStatustextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.SalesReturnStatustextBox, 2);
            this.SalesReturnStatustextBox.Enabled = false;
            this.SalesReturnStatustextBox.Location = new System.Drawing.Point(599, 3);
            this.SalesReturnStatustextBox.Name = "SalesReturnStatustextBox";
            this.SalesReturnStatustextBox.Size = new System.Drawing.Size(151, 20);
            this.SalesReturnStatustextBox.TabIndex = 4;
            this.SalesReturnStatustextBox.Tag = "";
            // 
            // StatustextBox
            // 
            this.StatustextBox.BackColor = System.Drawing.Color.White;
            this.StatustextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.StatustextBox, 2);
            this.StatustextBox.Enabled = false;
            this.StatustextBox.Location = new System.Drawing.Point(599, 31);
            this.StatustextBox.Name = "StatustextBox";
            this.StatustextBox.Size = new System.Drawing.Size(151, 20);
            this.StatustextBox.TabIndex = 8;
            this.StatustextBox.Tag = "";
            this.StatustextBox.Visible = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(442, 576);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(108, 24);
            this.label21.TabIndex = 42;
            this.label21.Text = "Grand Total";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(442, 541);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(70, 24);
            this.label16.TabIndex = 30;
            this.label16.Text = "Rebate";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(442, 506);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(51, 24);
            this.label17.TabIndex = 40;
            this.label17.Text = "Total";
            // 
            // datagridviewpanel
            // 
            this.tableLayoutPanel.SetColumnSpan(this.datagridviewpanel, 10);
            this.datagridviewpanel.Controls.Add(this.MasterItemdataGridView);
            this.datagridviewpanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.datagridviewpanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.datagridviewpanel.Location = new System.Drawing.Point(3, 333);
            this.datagridviewpanel.Name = "datagridviewpanel";
            this.datagridviewpanel.Size = new System.Drawing.Size(988, 170);
            this.datagridviewpanel.TabIndex = 21;
            // 
            // MasterItemdataGridView
            // 
            this.MasterItemdataGridView.AllowUserToAddRows = false;
            this.MasterItemdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.MasterItemdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NO,
            this.ITEMID_ALIAS,
            this.ITEMID,
            this.ITEMNAME,
            this.ITEMIDQTY,
            this.ITEMNAMEQTY,
            this.STOCK,
            this.QUANTITY,
            this.RETURNQUANTITY,
            this.UOMID,
            this.UNITPRICE,
            this.AMOUNT,
            this.del});
            this.MasterItemdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MasterItemdataGridView.Location = new System.Drawing.Point(0, 0);
            this.MasterItemdataGridView.Name = "MasterItemdataGridView";
            this.MasterItemdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.MasterItemdataGridView.Size = new System.Drawing.Size(988, 170);
            this.MasterItemdataGridView.TabIndex = 20;
            this.MasterItemdataGridView.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.MasterItemdataGridView_CellValidated);
            this.MasterItemdataGridView.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.MasterItemdataGridView_UserDeletedRow);
            this.MasterItemdataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.MasterItemdataGridView_CellFormatting);
            this.MasterItemdataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MasterItemdataGridView_CellClick);
            this.MasterItemdataGridView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.MasterItemdataGridView_EditingControlShowing);
            this.MasterItemdataGridView.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.MasterItemdataGridView_RowsRemoved);
            // 
            // NO
            // 
            this.NO.DataPropertyName = "NO";
            this.NO.FillWeight = 40F;
            this.NO.HeaderText = "NO";
            this.NO.Name = "NO";
            this.NO.ReadOnly = true;
            this.NO.Width = 40;
            // 
            // ITEMID_ALIAS
            // 
            this.ITEMID_ALIAS.DataPropertyName = "ITEMID_ALIAS";
            this.ITEMID_ALIAS.HeaderText = "ALIAS";
            this.ITEMID_ALIAS.Name = "ITEMID_ALIAS";
            this.ITEMID_ALIAS.ReadOnly = true;
            this.ITEMID_ALIAS.Visible = false;
            // 
            // ITEMID
            // 
            this.ITEMID.DataPropertyName = "ITEMID";
            this.ITEMID.HeaderText = "PART NO";
            this.ITEMID.Name = "ITEMID";
            this.ITEMID.ReadOnly = true;
            // 
            // ITEMNAME
            // 
            this.ITEMNAME.DataPropertyName = "ITEMNAME";
            this.ITEMNAME.HeaderText = "PART NAME";
            this.ITEMNAME.Name = "ITEMNAME";
            this.ITEMNAME.ReadOnly = true;
            // 
            // ITEMIDQTY
            // 
            this.ITEMIDQTY.DataPropertyName = "ITEMIDQTY";
            this.ITEMIDQTY.FillWeight = 105F;
            this.ITEMIDQTY.HeaderText = "PART NO QTY";
            this.ITEMIDQTY.Name = "ITEMIDQTY";
            this.ITEMIDQTY.Width = 105;
            // 
            // ITEMNAMEQTY
            // 
            this.ITEMNAMEQTY.DataPropertyName = "ITEMNAMEQTY";
            this.ITEMNAMEQTY.FillWeight = 120F;
            this.ITEMNAMEQTY.HeaderText = "PART NAME QTY";
            this.ITEMNAMEQTY.Name = "ITEMNAMEQTY";
            this.ITEMNAMEQTY.Width = 120;
            // 
            // STOCK
            // 
            this.STOCK.DataPropertyName = "STOCK";
            this.STOCK.FillWeight = 60F;
            this.STOCK.HeaderText = "STOCK";
            this.STOCK.Name = "STOCK";
            this.STOCK.Visible = false;
            this.STOCK.Width = 60;
            // 
            // QUANTITY
            // 
            this.QUANTITY.DataPropertyName = "QUANTITY";
            this.QUANTITY.FillWeight = 80F;
            this.QUANTITY.HeaderText = "ORDER QTY";
            this.QUANTITY.Name = "QUANTITY";
            this.QUANTITY.ReadOnly = true;
            this.QUANTITY.Width = 80;
            // 
            // RETURNQUANTITY
            // 
            this.RETURNQUANTITY.DataPropertyName = "RETURNQUANTITY";
            this.RETURNQUANTITY.FillWeight = 90F;
            this.RETURNQUANTITY.HeaderText = "RETURN QTY";
            this.RETURNQUANTITY.Name = "RETURNQUANTITY";
            this.RETURNQUANTITY.Width = 90;
            // 
            // UOMID
            // 
            this.UOMID.DataPropertyName = "UOMID";
            this.UOMID.FillWeight = 65F;
            this.UOMID.HeaderText = "UOM";
            this.UOMID.Name = "UOMID";
            this.UOMID.ReadOnly = true;
            this.UOMID.Width = 65;
            // 
            // UNITPRICE
            // 
            this.UNITPRICE.DataPropertyName = "UNITPRICE";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle7.Format = "N2";
            this.UNITPRICE.DefaultCellStyle = dataGridViewCellStyle7;
            this.UNITPRICE.HeaderText = "UNIT PRICE";
            this.UNITPRICE.Name = "UNITPRICE";
            this.UNITPRICE.ReadOnly = true;
            // 
            // AMOUNT
            // 
            this.AMOUNT.DataPropertyName = "AMOUNT";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle8.Format = "N2";
            this.AMOUNT.DefaultCellStyle = dataGridViewCellStyle8;
            this.AMOUNT.HeaderText = "AMOUNT";
            this.AMOUNT.Name = "AMOUNT";
            this.AMOUNT.ReadOnly = true;
            // 
            // del
            // 
            this.del.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.NullValue = ((object)(resources.GetObject("dataGridViewCellStyle9.NullValue")));
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.White;
            this.del.DefaultCellStyle = dataGridViewCellStyle9;
            this.del.FillWeight = 24F;
            this.del.HeaderText = "";
            this.del.Image = global::Sufindo.Properties.Resources.minus;
            this.del.MinimumWidth = 24;
            this.del.Name = "del";
            this.del.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.del.Width = 24;
            // 
            // AddItembutton
            // 
            this.AddItembutton.BackColor = System.Drawing.Color.Transparent;
            this.AddItembutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AddItembutton.FlatAppearance.BorderSize = 0;
            this.AddItembutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddItembutton.ForeColor = System.Drawing.Color.Transparent;
            this.AddItembutton.Image = global::Sufindo.Properties.Resources.plus;
            this.AddItembutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.AddItembutton.Location = new System.Drawing.Point(0, 298);
            this.AddItembutton.Margin = new System.Windows.Forms.Padding(0);
            this.AddItembutton.Name = "AddItembutton";
            this.AddItembutton.Size = new System.Drawing.Size(80, 32);
            this.AddItembutton.TabIndex = 44;
            this.AddItembutton.Text = "Add Part";
            this.AddItembutton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.AddItembutton.UseVisualStyleBackColor = false;
            this.AddItembutton.Click += new System.EventHandler(this.AddItembutton_Click);
            // 
            // Spesifikasipanel
            // 
            this.Spesifikasipanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.Spesifikasipanel, 4);
            this.Spesifikasipanel.Controls.Add(this.RemarkrichTextBox);
            this.Spesifikasipanel.Location = new System.Drawing.Point(94, 509);
            this.Spesifikasipanel.Name = "Spesifikasipanel";
            this.tableLayoutPanel.SetRowSpan(this.Spesifikasipanel, 4);
            this.Spesifikasipanel.Size = new System.Drawing.Size(342, 122);
            this.Spesifikasipanel.TabIndex = 20;
            // 
            // RemarkrichTextBox
            // 
            this.RemarkrichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RemarkrichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RemarkrichTextBox.Location = new System.Drawing.Point(0, 0);
            this.RemarkrichTextBox.Name = "RemarkrichTextBox";
            this.RemarkrichTextBox.Size = new System.Drawing.Size(340, 120);
            this.RemarkrichTextBox.TabIndex = 22;
            this.RemarkrichTextBox.TabStop = false;
            this.RemarkrichTextBox.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 506);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Remark";
            // 
            // checkBoxAlias
            // 
            this.checkBoxAlias.AutoSize = true;
            this.checkBoxAlias.Location = new System.Drawing.Point(442, 614);
            this.checkBoxAlias.Name = "checkBoxAlias";
            this.checkBoxAlias.Size = new System.Drawing.Size(48, 17);
            this.checkBoxAlias.TabIndex = 45;
            this.checkBoxAlias.Text = "Alias";
            this.checkBoxAlias.UseVisualStyleBackColor = true;
            // 
            // RatetextBox
            // 
            this.RatetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RatetextBox.Decimal = false;
            this.RatetextBox.Location = new System.Drawing.Point(94, 275);
            this.RatetextBox.Money = true;
            this.RatetextBox.Name = "RatetextBox";
            this.RatetextBox.Numeric = false;
            this.RatetextBox.Prefix = "";
            this.RatetextBox.Size = new System.Drawing.Size(130, 20);
            this.RatetextBox.TabIndex = 18;
            // 
            // TotalInvoicetextBox
            // 
            this.TotalInvoicetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.TotalInvoicetextBox, 2);
            this.TotalInvoicetextBox.Decimal = false;
            this.TotalInvoicetextBox.Enabled = false;
            this.TotalInvoicetextBox.Location = new System.Drawing.Point(442, 275);
            this.TotalInvoicetextBox.Money = true;
            this.TotalInvoicetextBox.Name = "TotalInvoicetextBox";
            this.TotalInvoicetextBox.Numeric = false;
            this.TotalInvoicetextBox.Prefix = "";
            this.TotalInvoicetextBox.Size = new System.Drawing.Size(130, 20);
            this.TotalInvoicetextBox.TabIndex = 23;
            // 
            // TotaltextBox
            // 
            this.TotaltextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.TotaltextBox, 2);
            this.TotaltextBox.Decimal = false;
            this.TotaltextBox.Enabled = false;
            this.TotaltextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotaltextBox.Location = new System.Drawing.Point(556, 509);
            this.TotaltextBox.Money = true;
            this.TotaltextBox.Name = "TotaltextBox";
            this.TotaltextBox.Numeric = false;
            this.TotaltextBox.Prefix = "";
            this.TotaltextBox.Size = new System.Drawing.Size(182, 29);
            this.TotaltextBox.TabIndex = 46;
            // 
            // RebatetextBox
            // 
            this.RebatetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.RebatetextBox, 2);
            this.RebatetextBox.Decimal = false;
            this.RebatetextBox.Enabled = false;
            this.RebatetextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RebatetextBox.Location = new System.Drawing.Point(556, 544);
            this.RebatetextBox.Money = true;
            this.RebatetextBox.Name = "RebatetextBox";
            this.RebatetextBox.Numeric = false;
            this.RebatetextBox.Prefix = "";
            this.RebatetextBox.Size = new System.Drawing.Size(182, 29);
            this.RebatetextBox.TabIndex = 24;
            this.RebatetextBox.TextChanged += new System.EventHandler(this.RebatetextBox_TextChanged);
            // 
            // GrandTotaltextBox
            // 
            this.GrandTotaltextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.GrandTotaltextBox, 2);
            this.GrandTotaltextBox.Decimal = false;
            this.GrandTotaltextBox.Enabled = false;
            this.GrandTotaltextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrandTotaltextBox.Location = new System.Drawing.Point(556, 579);
            this.GrandTotaltextBox.Money = true;
            this.GrandTotaltextBox.Name = "GrandTotaltextBox";
            this.GrandTotaltextBox.Numeric = false;
            this.GrandTotaltextBox.Prefix = "";
            this.GrandTotaltextBox.Size = new System.Drawing.Size(182, 29);
            this.GrandTotaltextBox.TabIndex = 47;
            // 
            // SalesReturn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(999, 647);
            this.Controls.Add(this.tableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SalesReturn";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sales Return";
            this.Activated += new System.EventHandler(this.SalesReturn_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SalesReturn_FormClosed);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.datagridviewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MasterItemdataGridView)).EndInit();
            this.Spesifikasipanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox InvoiceNotextBox;
        private System.Windows.Forms.TextBox InvoiceDatetextBox;
        private System.Windows.Forms.TextBox StatustextBox;
        private System.Windows.Forms.TextBox KurstextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel datagridviewpanel;
        private System.Windows.Forms.DataGridView MasterItemdataGridView;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button SearchInvoiceNObutton;
        private System.Windows.Forms.Panel Spesifikasipanel;
        private System.Windows.Forms.RichTextBox RemarkrichTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox CustomerIDtextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox SalesReturnNotextBox;
        private System.Windows.Forms.Button SearchSalesReturnNobutton;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox SalesReturnDatetextBox;
        private System.Windows.Forms.TextBox SalesReturnStatustextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox NameCustomertextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RichTextBox AddressrichTextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox NoTelptextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox NoFaxtextBox;
        private System.Windows.Forms.TextBox ContactPersontextBox;
        private System.Windows.Forms.TextBox EmailtextBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox HandphonetextBox;
        private System.Windows.Forms.Button AddItembutton;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.CheckBox checkBoxAlias;
        private CustomControls.TextBoxSL RatetextBox;
        private CustomControls.TextBoxSL TotalInvoicetextBox;
        private CustomControls.TextBoxSL TotaltextBox;
        private CustomControls.TextBoxSL RebatetextBox;
        private CustomControls.TextBoxSL GrandTotaltextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMID_ALIAS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMIDQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMNAMEQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn STOCK;
        private System.Windows.Forms.DataGridViewTextBoxColumn QUANTITY;
        private System.Windows.Forms.DataGridViewTextBoxColumn RETURNQUANTITY;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn UNITPRICE;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT;
        private System.Windows.Forms.DataGridViewImageColumn del;
    }
}