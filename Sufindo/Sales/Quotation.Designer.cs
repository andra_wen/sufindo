﻿namespace Sufindo.Sales
{
    partial class Quotation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.RatetextBox = new CustomControls.TextBoxSL();
            this.datagridviewpanel = new System.Windows.Forms.Panel();
            this.MasterItemdataGridView = new System.Windows.Forms.DataGridView();
            this.NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMID_ALIAS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMIDQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMNAMEQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STOCK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUANTITY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UNITPRICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.del = new System.Windows.Forms.DataGridViewImageColumn();
            this.AddItembutton = new System.Windows.Forms.Button();
            this.searchKursbutton = new System.Windows.Forms.Button();
            this.KurstextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.QONotextBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.CustomerIDtextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.AddressrichTextBox = new System.Windows.Forms.RichTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.NoTelptextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.NoFaxtextBox = new System.Windows.Forms.TextBox();
            this.ContactPersontextBox = new System.Windows.Forms.TextBox();
            this.EmailtextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.HandphonetextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.searchCustomerIDbutton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Spesifikasipanel = new System.Windows.Forms.Panel();
            this.RemarkrichTextBox = new System.Windows.Forms.RichTextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.SearchQuotationNObutton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.QODatetextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.StatustextBox = new System.Windows.Forms.TextBox();
            this.paymentMethodscomboBox = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.TotaltextBox = new CustomControls.TextBoxSL();
            this.DiscounttextBox = new CustomControls.TextBoxSL();
            this.GrandTotaltextBox = new CustomControls.TextBoxSL();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerCombobox = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel.SuspendLayout();
            this.datagridviewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MasterItemdataGridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.Spesifikasipanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 9;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.Controls.Add(this.RatetextBox, 1, 4);
            this.tableLayoutPanel.Controls.Add(this.datagridviewpanel, 0, 6);
            this.tableLayoutPanel.Controls.Add(this.AddItembutton, 0, 5);
            this.tableLayoutPanel.Controls.Add(this.searchKursbutton, 2, 3);
            this.tableLayoutPanel.Controls.Add(this.KurstextBox, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.label6, 0, 4);
            this.tableLayoutPanel.Controls.Add(this.label7, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.QONotextBox, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.groupBox1, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.searchCustomerIDbutton, 2, 1);
            this.tableLayoutPanel.Controls.Add(this.label1, 0, 7);
            this.tableLayoutPanel.Controls.Add(this.Spesifikasipanel, 1, 7);
            this.tableLayoutPanel.Controls.Add(this.label18, 4, 3);
            this.tableLayoutPanel.Controls.Add(this.SearchQuotationNObutton, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.label3, 3, 0);
            this.tableLayoutPanel.Controls.Add(this.QODatetextBox, 4, 0);
            this.tableLayoutPanel.Controls.Add(this.label9, 6, 0);
            this.tableLayoutPanel.Controls.Add(this.StatustextBox, 7, 0);
            this.tableLayoutPanel.Controls.Add(this.paymentMethodscomboBox, 5, 3);
            this.tableLayoutPanel.Controls.Add(this.label10, 6, 7);
            this.tableLayoutPanel.Controls.Add(this.label16, 6, 8);
            this.tableLayoutPanel.Controls.Add(this.label17, 6, 9);
            this.tableLayoutPanel.Controls.Add(this.TotaltextBox, 7, 7);
            this.tableLayoutPanel.Controls.Add(this.DiscounttextBox, 7, 8);
            this.tableLayoutPanel.Controls.Add(this.GrandTotaltextBox, 7, 9);
            this.tableLayoutPanel.Controls.Add(this.customerCombobox, 1, 1);
            this.tableLayoutPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.tableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 10;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.Size = new System.Drawing.Size(945, 614);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // RatetextBox
            // 
            this.RatetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RatetextBox.Decimal = false;
            this.RatetextBox.Location = new System.Drawing.Point(94, 251);
            this.RatetextBox.Money = true;
            this.RatetextBox.Name = "RatetextBox";
            this.RatetextBox.Numeric = false;
            this.RatetextBox.Prefix = "";
            this.RatetextBox.Size = new System.Drawing.Size(130, 20);
            this.RatetextBox.TabIndex = 18;
            // 
            // datagridviewpanel
            // 
            this.tableLayoutPanel.SetColumnSpan(this.datagridviewpanel, 9);
            this.datagridviewpanel.Controls.Add(this.MasterItemdataGridView);
            this.datagridviewpanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.datagridviewpanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.datagridviewpanel.Location = new System.Drawing.Point(3, 309);
            this.datagridviewpanel.Name = "datagridviewpanel";
            this.datagridviewpanel.Size = new System.Drawing.Size(939, 170);
            this.datagridviewpanel.TabIndex = 21;
            // 
            // MasterItemdataGridView
            // 
            this.MasterItemdataGridView.AllowUserToAddRows = false;
            this.MasterItemdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.MasterItemdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NO,
            this.ITEMID_ALIAS,
            this.ITEMID,
            this.ITEMNAME,
            this.ITEMIDQTY,
            this.ITEMNAMEQTY,
            this.STOCK,
            this.QUANTITY,
            this.UOMID,
            this.UNITPRICE,
            this.AMOUNT,
            this.del});
            this.MasterItemdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MasterItemdataGridView.Location = new System.Drawing.Point(0, 0);
            this.MasterItemdataGridView.Name = "MasterItemdataGridView";
            this.MasterItemdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.MasterItemdataGridView.Size = new System.Drawing.Size(939, 170);
            this.MasterItemdataGridView.TabIndex = 0;
            this.MasterItemdataGridView.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.MasterItemdataGridView_CellValidated);
            this.MasterItemdataGridView.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.MasterItemdataGridView_UserDeletedRow);
            this.MasterItemdataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.MasterItemdataGridView_CellFormatting);
            this.MasterItemdataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MasterItemdataGridView_CellClick);
            this.MasterItemdataGridView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.MasterItemdataGridView_EditingControlShowing);
            // 
            // NO
            // 
            this.NO.DataPropertyName = "NO";
            this.NO.FillWeight = 40F;
            this.NO.HeaderText = "NO";
            this.NO.Name = "NO";
            this.NO.ReadOnly = true;
            this.NO.Width = 40;
            // 
            // ITEMID_ALIAS
            // 
            this.ITEMID_ALIAS.DataPropertyName = "ITEMID_ALIAS";
            this.ITEMID_ALIAS.HeaderText = "ALIAS";
            this.ITEMID_ALIAS.Name = "ITEMID_ALIAS";
            this.ITEMID_ALIAS.ReadOnly = true;
            this.ITEMID_ALIAS.Visible = false;
            // 
            // ITEMID
            // 
            this.ITEMID.DataPropertyName = "ITEMID";
            this.ITEMID.HeaderText = "PART NO";
            this.ITEMID.Name = "ITEMID";
            this.ITEMID.ReadOnly = true;
            // 
            // ITEMNAME
            // 
            this.ITEMNAME.DataPropertyName = "ITEMNAME";
            this.ITEMNAME.HeaderText = "PART NAME";
            this.ITEMNAME.Name = "ITEMNAME";
            this.ITEMNAME.ReadOnly = true;
            // 
            // ITEMIDQTY
            // 
            this.ITEMIDQTY.DataPropertyName = "ITEMIDQTY";
            this.ITEMIDQTY.FillWeight = 105F;
            this.ITEMIDQTY.HeaderText = "PART NO QTY";
            this.ITEMIDQTY.Name = "ITEMIDQTY";
            this.ITEMIDQTY.Width = 105;
            // 
            // ITEMNAMEQTY
            // 
            this.ITEMNAMEQTY.DataPropertyName = "ITEMNAMEQTY";
            this.ITEMNAMEQTY.FillWeight = 120F;
            this.ITEMNAMEQTY.HeaderText = "PART NAME QTY";
            this.ITEMNAMEQTY.Name = "ITEMNAMEQTY";
            this.ITEMNAMEQTY.Width = 120;
            // 
            // STOCK
            // 
            this.STOCK.DataPropertyName = "STOCK";
            this.STOCK.FillWeight = 60F;
            this.STOCK.HeaderText = "STOCK";
            this.STOCK.Name = "STOCK";
            this.STOCK.Width = 60;
            // 
            // QUANTITY
            // 
            this.QUANTITY.DataPropertyName = "QUANTITY";
            this.QUANTITY.FillWeight = 80F;
            this.QUANTITY.HeaderText = "ORDER QTY";
            this.QUANTITY.Name = "QUANTITY";
            this.QUANTITY.Width = 80;
            // 
            // UOMID
            // 
            this.UOMID.DataPropertyName = "UOMID";
            this.UOMID.FillWeight = 65F;
            this.UOMID.HeaderText = "UOM";
            this.UOMID.Name = "UOMID";
            this.UOMID.ReadOnly = true;
            this.UOMID.Width = 65;
            // 
            // UNITPRICE
            // 
            this.UNITPRICE.DataPropertyName = "UNITPRICE";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle1.Format = "N2";
            this.UNITPRICE.DefaultCellStyle = dataGridViewCellStyle1;
            this.UNITPRICE.HeaderText = "UNIT PRICE";
            this.UNITPRICE.Name = "UNITPRICE";
            // 
            // AMOUNT
            // 
            this.AMOUNT.DataPropertyName = "AMOUNT";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle2.Format = "N2";
            this.AMOUNT.DefaultCellStyle = dataGridViewCellStyle2;
            this.AMOUNT.HeaderText = "AMOUNT";
            this.AMOUNT.Name = "AMOUNT";
            this.AMOUNT.ReadOnly = true;
            // 
            // del
            // 
            this.del.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.NullValue = null;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            this.del.DefaultCellStyle = dataGridViewCellStyle3;
            this.del.FillWeight = 24F;
            this.del.HeaderText = "";
            this.del.Image = global::Sufindo.Properties.Resources.minus;
            this.del.MinimumWidth = 24;
            this.del.Name = "del";
            this.del.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.del.Width = 24;
            // 
            // AddItembutton
            // 
            this.AddItembutton.BackColor = System.Drawing.Color.Transparent;
            this.AddItembutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AddItembutton.FlatAppearance.BorderSize = 0;
            this.AddItembutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddItembutton.ForeColor = System.Drawing.Color.Transparent;
            this.AddItembutton.Image = global::Sufindo.Properties.Resources.plus;
            this.AddItembutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.AddItembutton.Location = new System.Drawing.Point(0, 274);
            this.AddItembutton.Margin = new System.Windows.Forms.Padding(0);
            this.AddItembutton.Name = "AddItembutton";
            this.AddItembutton.Size = new System.Drawing.Size(91, 32);
            this.AddItembutton.TabIndex = 19;
            this.AddItembutton.Text = "Add Item";
            this.AddItembutton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.AddItembutton.UseVisualStyleBackColor = false;
            this.AddItembutton.Click += new System.EventHandler(this.AddItembutton_Click);
            // 
            // searchKursbutton
            // 
            this.searchKursbutton.BackColor = System.Drawing.Color.Transparent;
            this.searchKursbutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchKursbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchKursbutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.searchKursbutton.FlatAppearance.BorderSize = 0;
            this.searchKursbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchKursbutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchKursbutton.Location = new System.Drawing.Point(272, 221);
            this.searchKursbutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.searchKursbutton.Name = "searchKursbutton";
            this.searchKursbutton.Size = new System.Drawing.Size(24, 24);
            this.searchKursbutton.TabIndex = 17;
            this.searchKursbutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.searchKursbutton.UseVisualStyleBackColor = false;
            this.searchKursbutton.Click += new System.EventHandler(this.searchKursbutton_Click);
            // 
            // KurstextBox
            // 
            this.KurstextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.KurstextBox.Location = new System.Drawing.Point(94, 223);
            this.KurstextBox.Name = "KurstextBox";
            this.KurstextBox.Size = new System.Drawing.Size(130, 20);
            this.KurstextBox.TabIndex = 16;
            this.KurstextBox.Tag = "7";
            this.KurstextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KurstextBox_KeyDown);
            this.KurstextBox.Leave += new System.EventHandler(this.KurstextBox_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 248);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Rate";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 220);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Currency";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Q.O No";
            // 
            // QONotextBox
            // 
            this.QONotextBox.BackColor = System.Drawing.Color.White;
            this.QONotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.QONotextBox.Enabled = false;
            this.QONotextBox.Location = new System.Drawing.Point(94, 3);
            this.QONotextBox.Name = "QONotextBox";
            this.QONotextBox.Size = new System.Drawing.Size(172, 20);
            this.QONotextBox.TabIndex = 1;
            this.QONotextBox.Tag = "";
            this.QONotextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.QONotextBox_KeyDown);
            this.QONotextBox.Leave += new System.EventHandler(this.QONotextBox_Leave);
            // 
            // groupBox1
            // 
            this.tableLayoutPanel.SetColumnSpan(this.groupBox1, 9);
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.groupBox1.Location = new System.Drawing.Point(3, 59);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(758, 158);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data Customer";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.6726F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.3274F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 157F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 127F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 214F));
            this.tableLayoutPanel1.Controls.Add(this.CustomerIDtextBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.NoTelptextBox, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label12, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label13, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.NoFaxtextBox, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.ContactPersontextBox, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.EmailtextBox, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.label14, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.label15, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.HandphonetextBox, 4, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(746, 131);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // CustomerIDtextBox
            // 
            this.CustomerIDtextBox.BackColor = System.Drawing.Color.White;
            this.CustomerIDtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CustomerIDtextBox.Location = new System.Drawing.Point(81, 3);
            this.CustomerIDtextBox.Name = "CustomerIDtextBox";
            this.CustomerIDtextBox.Size = new System.Drawing.Size(163, 20);
            this.CustomerIDtextBox.TabIndex = 8;
            this.CustomerIDtextBox.Tag = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "ID";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 29);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 20);
            this.label8.TabIndex = 9;
            this.label8.Text = "Address";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel1.SetColumnSpan(this.panel1, 2);
            this.panel1.Controls.Add(this.AddressrichTextBox);
            this.panel1.Location = new System.Drawing.Point(81, 32);
            this.panel1.Name = "panel1";
            this.tableLayoutPanel1.SetRowSpan(this.panel1, 2);
            this.panel1.Size = new System.Drawing.Size(320, 65);
            this.panel1.TabIndex = 9;
            // 
            // AddressrichTextBox
            // 
            this.AddressrichTextBox.BackColor = System.Drawing.Color.White;
            this.AddressrichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.AddressrichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddressrichTextBox.Location = new System.Drawing.Point(0, 0);
            this.AddressrichTextBox.Name = "AddressrichTextBox";
            this.AddressrichTextBox.Size = new System.Drawing.Size(318, 63);
            this.AddressrichTextBox.TabIndex = 10;
            this.AddressrichTextBox.TabStop = false;
            this.AddressrichTextBox.Tag = "";
            this.AddressrichTextBox.Text = "";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(3, 100);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 20);
            this.label11.TabIndex = 10;
            this.label11.Text = "No Telp";
            // 
            // NoTelptextBox
            // 
            this.NoTelptextBox.BackColor = System.Drawing.Color.White;
            this.NoTelptextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NoTelptextBox.Location = new System.Drawing.Point(81, 103);
            this.NoTelptextBox.Name = "NoTelptextBox";
            this.NoTelptextBox.Size = new System.Drawing.Size(163, 20);
            this.NoTelptextBox.TabIndex = 11;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(407, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 20);
            this.label12.TabIndex = 12;
            this.label12.Text = "Fax";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(407, 29);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(119, 20);
            this.label13.TabIndex = 14;
            this.label13.Text = "Contact Person";
            // 
            // NoFaxtextBox
            // 
            this.NoFaxtextBox.BackColor = System.Drawing.Color.White;
            this.NoFaxtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NoFaxtextBox.Location = new System.Drawing.Point(534, 3);
            this.NoFaxtextBox.Name = "NoFaxtextBox";
            this.NoFaxtextBox.Size = new System.Drawing.Size(166, 20);
            this.NoFaxtextBox.TabIndex = 12;
            // 
            // ContactPersontextBox
            // 
            this.ContactPersontextBox.BackColor = System.Drawing.Color.White;
            this.ContactPersontextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ContactPersontextBox.Location = new System.Drawing.Point(534, 32);
            this.ContactPersontextBox.Name = "ContactPersontextBox";
            this.ContactPersontextBox.Size = new System.Drawing.Size(166, 20);
            this.ContactPersontextBox.TabIndex = 13;
            // 
            // EmailtextBox
            // 
            this.EmailtextBox.BackColor = System.Drawing.Color.White;
            this.EmailtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.EmailtextBox.Location = new System.Drawing.Point(534, 61);
            this.EmailtextBox.Name = "EmailtextBox";
            this.EmailtextBox.Size = new System.Drawing.Size(166, 20);
            this.EmailtextBox.TabIndex = 14;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(407, 100);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(93, 20);
            this.label14.TabIndex = 15;
            this.label14.Text = "Handphone";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(407, 58);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(48, 20);
            this.label15.TabIndex = 19;
            this.label15.Text = "Email";
            // 
            // HandphonetextBox
            // 
            this.HandphonetextBox.BackColor = System.Drawing.Color.White;
            this.HandphonetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.HandphonetextBox.Location = new System.Drawing.Point(534, 103);
            this.HandphonetextBox.Name = "HandphonetextBox";
            this.HandphonetextBox.Size = new System.Drawing.Size(166, 20);
            this.HandphonetextBox.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 28;
            this.label4.Text = "Customer Name";
            // 
            // searchCustomerIDbutton
            // 
            this.searchCustomerIDbutton.BackColor = System.Drawing.Color.Transparent;
            this.searchCustomerIDbutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchCustomerIDbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchCustomerIDbutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.searchCustomerIDbutton.FlatAppearance.BorderSize = 0;
            this.searchCustomerIDbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchCustomerIDbutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchCustomerIDbutton.Location = new System.Drawing.Point(272, 29);
            this.searchCustomerIDbutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.searchCustomerIDbutton.Name = "searchCustomerIDbutton";
            this.searchCustomerIDbutton.Size = new System.Drawing.Size(24, 24);
            this.searchCustomerIDbutton.TabIndex = 6;
            this.searchCustomerIDbutton.Tag = "";
            this.searchCustomerIDbutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.searchCustomerIDbutton.UseVisualStyleBackColor = false;
            this.searchCustomerIDbutton.Click += new System.EventHandler(this.searchCustomerIDbutton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 482);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Remark";
            // 
            // Spesifikasipanel
            // 
            this.Spesifikasipanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.Spesifikasipanel, 5);
            this.Spesifikasipanel.Controls.Add(this.RemarkrichTextBox);
            this.Spesifikasipanel.Location = new System.Drawing.Point(94, 485);
            this.Spesifikasipanel.Name = "Spesifikasipanel";
            this.tableLayoutPanel.SetRowSpan(this.Spesifikasipanel, 3);
            this.Spesifikasipanel.Size = new System.Drawing.Size(428, 126);
            this.Spesifikasipanel.TabIndex = 20;
            // 
            // RemarkrichTextBox
            // 
            this.RemarkrichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RemarkrichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RemarkrichTextBox.Location = new System.Drawing.Point(0, 0);
            this.RemarkrichTextBox.Name = "RemarkrichTextBox";
            this.RemarkrichTextBox.Size = new System.Drawing.Size(426, 124);
            this.RemarkrichTextBox.TabIndex = 20;
            this.RemarkrichTextBox.TabStop = false;
            this.RemarkrichTextBox.Text = "";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(360, 220);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(92, 13);
            this.label18.TabIndex = 32;
            this.label18.Text = "Payment Methods";
            // 
            // SearchQuotationNObutton
            // 
            this.SearchQuotationNObutton.BackColor = System.Drawing.Color.Transparent;
            this.SearchQuotationNObutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.SearchQuotationNObutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SearchQuotationNObutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.SearchQuotationNObutton.FlatAppearance.BorderSize = 0;
            this.SearchQuotationNObutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchQuotationNObutton.ForeColor = System.Drawing.Color.Transparent;
            this.SearchQuotationNObutton.Location = new System.Drawing.Point(272, 1);
            this.SearchQuotationNObutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.SearchQuotationNObutton.Name = "SearchQuotationNObutton";
            this.SearchQuotationNObutton.Size = new System.Drawing.Size(24, 24);
            this.SearchQuotationNObutton.TabIndex = 2;
            this.SearchQuotationNObutton.Tag = "";
            this.SearchQuotationNObutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.SearchQuotationNObutton.UseVisualStyleBackColor = false;
            this.SearchQuotationNObutton.Visible = false;
            this.SearchQuotationNObutton.Click += new System.EventHandler(this.SearchQuotationNObutton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(302, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Q.O Date";
            // 
            // QODatetextBox
            // 
            this.QODatetextBox.BackColor = System.Drawing.Color.White;
            this.QODatetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.QODatetextBox, 2);
            this.QODatetextBox.Enabled = false;
            this.QODatetextBox.Location = new System.Drawing.Point(360, 3);
            this.QODatetextBox.Name = "QODatetextBox";
            this.QODatetextBox.Size = new System.Drawing.Size(149, 20);
            this.QODatetextBox.TabIndex = 3;
            this.QODatetextBox.Tag = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(528, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Status";
            // 
            // StatustextBox
            // 
            this.StatustextBox.BackColor = System.Drawing.Color.White;
            this.StatustextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.StatustextBox, 2);
            this.StatustextBox.Enabled = false;
            this.StatustextBox.Location = new System.Drawing.Point(642, 3);
            this.StatustextBox.Name = "StatustextBox";
            this.StatustextBox.Size = new System.Drawing.Size(151, 20);
            this.StatustextBox.TabIndex = 4;
            this.StatustextBox.Tag = "";
            // 
            // paymentMethodscomboBox
            // 
            this.tableLayoutPanel.SetColumnSpan(this.paymentMethodscomboBox, 3);
            this.paymentMethodscomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.paymentMethodscomboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.paymentMethodscomboBox.FormattingEnabled = true;
            this.paymentMethodscomboBox.Items.AddRange(new object[] {
            "Choose",
            "Credit",
            "Cash"});
            this.paymentMethodscomboBox.Location = new System.Drawing.Point(458, 223);
            this.paymentMethodscomboBox.Name = "paymentMethodscomboBox";
            this.paymentMethodscomboBox.Size = new System.Drawing.Size(168, 21);
            this.paymentMethodscomboBox.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(528, 482);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 24);
            this.label10.TabIndex = 24;
            this.label10.Text = "Total";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(528, 517);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 24);
            this.label16.TabIndex = 30;
            this.label16.Text = "Discount";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(528, 552);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(108, 24);
            this.label17.TabIndex = 31;
            this.label17.Text = "Grand Total";
            // 
            // TotaltextBox
            // 
            this.TotaltextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.TotaltextBox, 2);
            this.TotaltextBox.Decimal = false;
            this.TotaltextBox.Enabled = false;
            this.TotaltextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotaltextBox.Location = new System.Drawing.Point(642, 485);
            this.TotaltextBox.Money = true;
            this.TotaltextBox.Name = "TotaltextBox";
            this.TotaltextBox.Numeric = false;
            this.TotaltextBox.Prefix = "";
            this.TotaltextBox.Size = new System.Drawing.Size(168, 29);
            this.TotaltextBox.TabIndex = 21;
            // 
            // DiscounttextBox
            // 
            this.DiscounttextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.DiscounttextBox, 2);
            this.DiscounttextBox.Decimal = false;
            this.DiscounttextBox.Enabled = false;
            this.DiscounttextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DiscounttextBox.Location = new System.Drawing.Point(642, 520);
            this.DiscounttextBox.Money = true;
            this.DiscounttextBox.Name = "DiscounttextBox";
            this.DiscounttextBox.Numeric = false;
            this.DiscounttextBox.Prefix = "";
            this.DiscounttextBox.Size = new System.Drawing.Size(168, 29);
            this.DiscounttextBox.TabIndex = 22;
            this.DiscounttextBox.TextChanged += new System.EventHandler(this.DiscounttextBox_TextChanged);
            // 
            // GrandTotaltextBox
            // 
            this.GrandTotaltextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.GrandTotaltextBox, 2);
            this.GrandTotaltextBox.Decimal = false;
            this.GrandTotaltextBox.Enabled = false;
            this.GrandTotaltextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrandTotaltextBox.Location = new System.Drawing.Point(642, 555);
            this.GrandTotaltextBox.Money = true;
            this.GrandTotaltextBox.Name = "GrandTotaltextBox";
            this.GrandTotaltextBox.Numeric = false;
            this.GrandTotaltextBox.Prefix = "";
            this.GrandTotaltextBox.Size = new System.Drawing.Size(168, 29);
            this.GrandTotaltextBox.TabIndex = 23;
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.NullValue = null;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewImageColumn1.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewImageColumn1.FillWeight = 24F;
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = global::Sufindo.Properties.Resources.minus;
            this.dataGridViewImageColumn1.MinimumWidth = 24;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewImageColumn1.Width = 24;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "NO";
            this.dataGridViewTextBoxColumn1.FillWeight = 40F;
            this.dataGridViewTextBoxColumn1.HeaderText = "NO";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 40;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "ITEMID_ALIAS";
            this.dataGridViewTextBoxColumn2.HeaderText = "ALIAS";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "ITEMID";
            this.dataGridViewTextBoxColumn3.HeaderText = "PART NO";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "ITEMNAME";
            this.dataGridViewTextBoxColumn4.HeaderText = "PART NAME";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "ITEMIDQTY";
            this.dataGridViewTextBoxColumn5.FillWeight = 105F;
            this.dataGridViewTextBoxColumn5.HeaderText = "PART NO QTY";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 105;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "ITEMNAMEQTY";
            this.dataGridViewTextBoxColumn6.FillWeight = 120F;
            this.dataGridViewTextBoxColumn6.HeaderText = "PART NAME QTY";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 120;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "STOCK";
            this.dataGridViewTextBoxColumn7.FillWeight = 60F;
            this.dataGridViewTextBoxColumn7.HeaderText = "STOCK";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 60;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "QUANTITY";
            this.dataGridViewTextBoxColumn8.FillWeight = 80F;
            this.dataGridViewTextBoxColumn8.HeaderText = "ORDER QTY";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 80;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "UOMID";
            this.dataGridViewTextBoxColumn9.FillWeight = 65F;
            this.dataGridViewTextBoxColumn9.HeaderText = "UOM";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 65;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "UNITPRICE";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle5.Format = "N2";
            this.dataGridViewTextBoxColumn10.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewTextBoxColumn10.HeaderText = "UNIT PRICE";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "AMOUNT";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle6.Format = "N2";
            this.dataGridViewTextBoxColumn11.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewTextBoxColumn11.HeaderText = "AMOUNT";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // customerCombobox
            // 
            this.customerCombobox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.customerCombobox.FormattingEnabled = true;
            this.customerCombobox.Location = new System.Drawing.Point(94, 31);
            this.customerCombobox.Name = "customerCombobox";
            this.customerCombobox.Size = new System.Drawing.Size(172, 21);
            this.customerCombobox.TabIndex = 33;
            this.customerCombobox.SelectedValueChanged += new System.EventHandler(this.customerCombobox_SelectedValueChanged);
            this.customerCombobox.Validated += new System.EventHandler(this.customerCombobox_Validated);
            // 
            // Quotation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(952, 621);
            this.Controls.Add(this.tableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Quotation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quotation";
            this.Activated += new System.EventHandler(this.Quotation_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Quotation_FormClosed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Quotation_FormClosing);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.datagridviewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MasterItemdataGridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.Spesifikasipanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox QONotextBox;
        private System.Windows.Forms.TextBox QODatetextBox;
        private System.Windows.Forms.TextBox StatustextBox;
        private System.Windows.Forms.TextBox KurstextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button AddItembutton;
        private System.Windows.Forms.Panel datagridviewpanel;
        private System.Windows.Forms.DataGridView MasterItemdataGridView;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button searchCustomerIDbutton;
        private System.Windows.Forms.Button searchKursbutton;
        private System.Windows.Forms.Button SearchQuotationNObutton;
        private System.Windows.Forms.Panel Spesifikasipanel;
        private System.Windows.Forms.RichTextBox RemarkrichTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox CustomerIDtextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox NoTelptextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RichTextBox AddressrichTextBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox NoFaxtextBox;
        private System.Windows.Forms.TextBox ContactPersontextBox;
        private System.Windows.Forms.TextBox EmailtextBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox HandphonetextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox paymentMethodscomboBox;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private CustomControls.TextBoxSL RatetextBox;
        private CustomControls.TextBoxSL TotaltextBox;
        private CustomControls.TextBoxSL DiscounttextBox;
        private CustomControls.TextBoxSL GrandTotaltextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMID_ALIAS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMIDQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMNAMEQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn STOCK;
        private System.Windows.Forms.DataGridViewTextBoxColumn QUANTITY;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn UNITPRICE;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT;
        private System.Windows.Forms.DataGridViewImageColumn del;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.ComboBox customerCombobox;
    }
}