﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Threading;

using Sufindo.Report.Sales;

namespace Sufindo.Sales
{
    public partial class Cost : Form
    {
        private delegate void fillAutoCompleteTextBox();

        Invoicedatagridview invoicedatagridview;
        MenuStrip menustripAction;
        Connection connection;
        DataTable datatable;
        List<String> InvoiceNo;
        List<Control> controlTextBox;

        Boolean isFind = false;

        public Cost()
        {
            InitializeComponent();
        }

        public Cost(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            this.menustripAction = menustripAction;
            invoicedatagridview = null;
            if (connection == null)
            {
                connection = new Connection();
            }
            controlTextBox = new List<Control>();
            foreach (Control control in this.tableLayoutPanel.Controls)
            {
                if (control is TextBox)
                {
                    controlTextBox.Add(control);
                }
                else if (control is Panel)
                {
                    foreach (Control controlPanel in control.Controls)
                    {
                        if (controlPanel is RichTextBox)
                        {
                            controlTextBox.Add(controlPanel);
                        }
                    }
                }
                else if (control is ComboBox)
                {
                    controlTextBox.Add(control);
                }
            }

            foreach (Control txtBox in controlTextBox)
            {
                if (txtBox is RichTextBox)
                {
                    RichTextBox richtextBox = (RichTextBox)txtBox;
                    richtextBox.ReadOnly = true;
                    richtextBox.BackColor = SystemColors.Info;
                    richtextBox.Text = "";
                }
                else
                {
                    txtBox.Enabled = false;
                    txtBox.Text = "";
                }
                txtBox.BackColor = SystemColors.Info;
            }
            disableDataCustomer();
            clearDataCustomer();
            newcostinvoice();
            CosttextBox.Enabled = true;
            SearchCustomerIDbutton.Visible = false;
            datatable = new DataTable();
        }

        public void save() {
            List<SqlParameter> sqlParam = new List<SqlParameter>();

            sqlParam.Add(new SqlParameter("@INVOICEID", InvoiceNotextBox.Text));
            sqlParam.Add(new SqlParameter("@COST", CosttextBox.DecimalValue));
            sqlParam.Add(new SqlParameter("@STATUS", Status.READY));
            sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));

            String proc = "INSERT_DATA_COST_INVOICE";

            DataTable dt = connection.callProcedureDatatable(proc, sqlParam);
            if (dt.Rows.Count != 0) {
                MessageBox.Show("SUCCESS INSERT");
                newcostinvoice();
            }
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }

        public void newcostinvoice() {
            Utilities.clearAllField(ref controlTextBox);
            reloadAllData();
            SalesIDtextBox.Enabled = false;
            InvoiceNotextBox.Enabled = true;
            isFind = false;
        }

        void bgWorker_DoWorkInsert(object sender, DoWorkEventArgs e) {
            try
            {
                Object []obj = e.Argument as Object[];

                String SALESORDERID = obj[0].ToString();
                String ORDERPICKINGID = obj[1].ToString();
                String query = String.Format("SELECT ITEMID, ITEMIDQTY, QUANTITY, PRICE FROM D_SALES_ORDER WHERE SALESORDERID = {0}", SALESORDERID);
                DataTable dt = connection.openDataTableQuery(query);

                List<string> values = new List<string>();
                foreach (DataRow row in dt.Rows)
                {
                    String value = String.Format("SELECT '{0}', '{1}', '{2}', {3}, {4}", ORDERPICKINGID, row["ITEMID"].ToString(),
                                                                                  row["ITEMIDQTY"].ToString(),
                                                                                  row["QUANTITY"].ToString(), row["PRICE"].ToString());
                    values.Add(value);
                }
                String[] columns = { "ORDERPICKINGID", "ITEMID", "ITEMIDQTY", "QUANTITY", "PRICE" };
                if (connection.openReaderQuery(Utilities.queryMultipleInsert("D_ORDER_PICKING", columns, values)))
                {
                    MessageBox.Show("SUCCESS");
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        void bgWorker_RunWorkerInsertCompleted(object sender, RunWorkerCompletedEventArgs e) { 
            
        }

        public void close() {
            if (isFind) {
                List<SqlParameter> sqlParam = new List<SqlParameter>();
                sqlParam.Add(new SqlParameter("@ORDERPICKINGID", InvoiceNotextBox.Text));
                sqlParam.Add(new SqlParameter("@STATUS", Status.CLOSE));
                if (connection.callProcedure("UPDATE_STATUS_H_ORDER_PICKING", sqlParam))
                {
                    sqlParam = new List<SqlParameter>();
                    sqlParam.Add(new SqlParameter("@SALESORDERID", SalesIDtextBox.Text));
                    sqlParam.Add(new SqlParameter("@STATUS", Status.READY));
                    if (connection.callProcedure("UPDATE_STATUS_H_SALES_ORDER", sqlParam))
                    {
                        sqlParam = new List<SqlParameter>();
                        sqlParam.Add(new SqlParameter("@ORDERPICKINGID", InvoiceNotextBox.Text));
                        if (connection.callProcedure("INSERT_LOG_TRANSACTION_ORDER_PICKING", sqlParam))
                        {
                            MessageBox.Show("SUCCESS INSERT");
                        }
                    }
                }
            }
        }

        public void find() {
            reloadAllData();
            isFind = true;
            InvoiceNotextBox.Enabled = true;
            Utilities.clearAllField(ref controlTextBox);
            clearDataCustomer();
            disableDataCustomer();
            
            InvoiceNotextBox.Text = "";
            InvoiceDatetextBox.Text = "";
            
            SearchInvoiceNObutton.Visible = true;
            SearchSalesIDbutton.Visible = false;
        }

        private void disableDataCustomer(){
            CustomerNametextBox.Enabled = false;
        }

        private void clearDataCustomer(){
            CustomerIDtextBox.Text = "";
            CustomerNametextBox.Text = "";
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e){

        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e){
            try
            {
                Thread.Sleep(100);

                if (InvoiceNo == null) InvoiceNo = new List<string>();
                else InvoiceNo.Clear();

                SqlDataReader sqldataReader = connection.sqlDataReaders(String.Format("SELECT INVOICEID FROM H_INVOICE WHERE [STATUS] = '{0}'", Status.ACTIVE));

                while (sqldataReader.Read()) InvoiceNo.Add(sqldataReader.GetString(0));

                if (IsHandleCreated) BeginInvoke(new fillAutoCompleteTextBox(this.fillAutoComplete));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void fillAutoComplete(){
            try
            {
                InvoiceNotextBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                InvoiceNotextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                InvoiceNotextBox.AutoCompleteCustomSource.Clear();
                InvoiceNotextBox.AutoCompleteCustomSource.AddRange(InvoiceNo.ToArray());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            
        }

        private void InvoicePassingData(DataTable sender){
            InvoiceNotextBox.Text = sender.Rows[0]["INVOICEID"].ToString();
            InvoiceDatetextBox.Text = sender.Rows[0]["INVOICEDATE"].ToString();
            SalesIDtextBox.Text = sender.Rows[0]["CREATEDBY"].ToString();
            SalesNametextBox.Text = sender.Rows[0]["EMPLOYEENAME"].ToString();
            CustomerIDtextBox.Text = sender.Rows[0]["CUSTOMERID"].ToString();
            CustomerNametextBox.Text = sender.Rows[0]["CUSTOMERNAME"].ToString();
            TotalInvoicetextBox.Text = sender.Rows[0]["TOTAL"].ToString();

            String query = String.Format("SELECT COST " +
                                         "FROM COST_INVOICE " +
                                         "WHERE INVOICEID = '{0}'", InvoiceNotextBox.Text);

            DataTable dt = connection.openDataTableQuery(query);

            if (dt.Rows.Count != 0)
            {
                CosttextBox.Text = dt.Rows[0]["COST"].ToString();
            }
            else {
                CosttextBox.Text = "0";
            }
            double grandtotal = TotalInvoicetextBox.DecimalValue + CosttextBox.DecimalValue;

            GrandTotaltextBox.Text = grandtotal.ToString();
        }

        private void SONotextBox_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void SONotextBox_Leave(object sender, EventArgs e)
        {
        }

        private void InvoiceNOtextBox_Leave(object sender, EventArgs e)
        {
            if(InvoiceNotextBox.Text.Equals("")){
                return;
            }
            Boolean isExistingOP = false;
            foreach (String item in InvoiceNo)
            {
                if (item.ToUpper().Equals(InvoiceNotextBox.Text.ToUpper()))
                {
                    isExistingOP = true;

                    String query = String.Format("SELECT INVOICEID, CONVERT(VARCHAR(10), INVOICEDATE, 103) AS [INVOICEDATE], " + 
                                                 "A.CREATEDBY, EMPLOYEENAME, A.CUSTOMERID, CUSTOMERNAME, TOTAL " +
                                                 "FROM H_INVOICE A JOIN M_EMPLOYEE B " +
                                                 "ON A.CREATEDBY = B.EMPLOYEEID JOIN M_CUSTOMER C " +
                                                 "ON A.CUSTOMERID = C.CUSTOMERID " +
                                                 "WHERE INVOICEID = '{0}'", InvoiceNotextBox.Text);
                    DataTable datatable = connection.openDataTableQuery(query);
                    InvoicePassingData(datatable);
                    break;
                }
            }
            if (!isExistingOP)
            {
                InvoiceNotextBox.Text = "";
                Utilities.clearAllField(ref controlTextBox);
                this.datatable.Clear();
                clearDataCustomer();
            }
            //}
        }

        private void InvoiceNOtextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (InvoiceNotextBox.Text.Equals(""))
            {
                return;
            }
            Boolean isExistingOPNo = false;
            this.datatable.Clear();
            foreach (String item in InvoiceNo)
            {
                if (item.ToUpper().Equals(InvoiceNotextBox.Text.ToUpper()))
                {
                    isExistingOPNo = true;
                    InvoiceNotextBox.Text = InvoiceNotextBox.Text.ToUpper();

                    String query = String.Format("SELECT INVOICEID, CONVERT(VARCHAR(10), INVOICEDATE, 103) AS [INVOICEDATE], " +
                                                 "A.CREATEDBY, EMPLOYEENAME, A.CUSTOMERID, CUSTOMERNAME, TOTAL " +
                                                 "FROM H_INVOICE A JOIN M_EMPLOYEE B " +
                                                 "ON A.CREATEDBY = B.EMPLOYEEID JOIN M_CUSTOMER C " +
                                                 "ON A.CUSTOMERID = C.CUSTOMERID " +
                                                 "WHERE INVOICEID = '{0}'", InvoiceNotextBox.Text);

                    DataTable datatable = connection.openDataTableQuery(query);
                    InvoicePassingData(datatable);
                    break;
                }
            }

            if (!isExistingOPNo)
            {
                InvoiceNotextBox.Text = "";
                Utilities.clearAllField(ref controlTextBox);
                this.datatable.Clear();
                clearDataCustomer();
            }
        }

        private void SearchInvoiceNObutton_Click(object sender, EventArgs e) 
        {
            if (invoicedatagridview == null)
            {
                invoicedatagridview = new Invoicedatagridview("Invoice");
                invoicedatagridview.invoicePassingData = new Invoicedatagridview.InvoicePassingData(InvoicePassingData);
                invoicedatagridview.ShowDialog();
            }
            else if (invoicedatagridview.IsDisposed)
            {
                invoicedatagridview = new Invoicedatagridview("Invoice");
                invoicedatagridview.invoicePassingData = new Invoicedatagridview.InvoicePassingData(InvoicePassingData);
                invoicedatagridview.ShowDialog();
            }
        }
        private void SearchSalesIDbutton_Click(object sender, EventArgs e)
        {
            if (invoicedatagridview == null)
            {
                invoicedatagridview = new Invoicedatagridview("Invoice");
                invoicedatagridview.invoicePassingData = new Invoicedatagridview.InvoicePassingData(InvoicePassingData);
                invoicedatagridview.ShowDialog();
            }
            else if (invoicedatagridview.IsDisposed)
            {
                invoicedatagridview = new Invoicedatagridview("Invoice");
                invoicedatagridview.invoicePassingData = new Invoicedatagridview.InvoicePassingData(InvoicePassingData);
                invoicedatagridview.ShowDialog();
            }
        }

        private void Cost_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            reloadAllData();
            Main.ACTIVEFORM = this;
        }

        private void Cost_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }

        private void CosttextBox_TextChanged(object sender, EventArgs e)
        {
            //Double cost = CosttextBox.Text.Equals("") ? 0.0 : Double.Parse(CosttextBox.Text);
            Double cost = CosttextBox.DecimalValue;
            Double total = TotalInvoicetextBox.DecimalValue;
            //Text.Equals("") ? 0.0 : Double.Parse(TotalInvoicetextBox.Text);
            GrandTotaltextBox.Text = (total + cost).ToString();
        }

    }
}
