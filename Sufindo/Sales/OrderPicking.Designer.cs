﻿namespace Sufindo.Sales
{
    partial class OrderPicking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.NameCustomertextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.datagridviewpanel = new System.Windows.Forms.Panel();
            this.MasterItemdataGridView = new System.Windows.Forms.DataGridView();
            this.NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMID_ALIAS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMIDQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEMNAMEQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STOCK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUANTITY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UNITPRICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CustomerIDtextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Spesifikasipanel = new System.Windows.Forms.Panel();
            this.RemarkrichTextBox = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SONotextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SODatetextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SOStatustextBox = new System.Windows.Forms.TextBox();
            this.OPStatustextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.OPDatetextBox = new System.Windows.Forms.TextBox();
            this.OPNOtextBox = new System.Windows.Forms.TextBox();
            this.OutStandingbutton = new System.Windows.Forms.Button();
            this.SearchOPNObutton = new System.Windows.Forms.Button();
            this.SearchSONObutton = new System.Windows.Forms.Button();
            this.tableLayoutPanel.SuspendLayout();
            this.datagridviewpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MasterItemdataGridView)).BeginInit();
            this.Spesifikasipanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 8;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Controls.Add(this.NameCustomertextBox, 4, 2);
            this.tableLayoutPanel.Controls.Add(this.label5, 3, 2);
            this.tableLayoutPanel.Controls.Add(this.label8, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.datagridviewpanel, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.CustomerIDtextBox, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.label1, 0, 5);
            this.tableLayoutPanel.Controls.Add(this.Spesifikasipanel, 1, 5);
            this.tableLayoutPanel.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.SONotextBox, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.label3, 3, 1);
            this.tableLayoutPanel.Controls.Add(this.SODatetextBox, 4, 1);
            this.tableLayoutPanel.Controls.Add(this.label9, 6, 1);
            this.tableLayoutPanel.Controls.Add(this.label6, 6, 0);
            this.tableLayoutPanel.Controls.Add(this.SOStatustextBox, 7, 1);
            this.tableLayoutPanel.Controls.Add(this.OPStatustextBox, 7, 0);
            this.tableLayoutPanel.Controls.Add(this.label7, 3, 0);
            this.tableLayoutPanel.Controls.Add(this.OPDatetextBox, 4, 0);
            this.tableLayoutPanel.Controls.Add(this.OPNOtextBox, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.OutStandingbutton, 7, 7);
            this.tableLayoutPanel.Controls.Add(this.SearchOPNObutton, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.SearchSONObutton, 2, 1);
            this.tableLayoutPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.tableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 7;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.Size = new System.Drawing.Size(804, 398);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // NameCustomertextBox
            // 
            this.NameCustomertextBox.BackColor = System.Drawing.Color.White;
            this.NameCustomertextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.NameCustomertextBox, 3);
            this.NameCustomertextBox.Location = new System.Drawing.Point(450, 59);
            this.NameCustomertextBox.Name = "NameCustomertextBox";
            this.NameCustomertextBox.Size = new System.Drawing.Size(188, 20);
            this.NameCustomertextBox.TabIndex = 8;
            this.NameCustomertextBox.Tag = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(320, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(124, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "Customer Name";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 20);
            this.label8.TabIndex = 36;
            this.label8.Text = "O.P No";
            // 
            // datagridviewpanel
            // 
            this.tableLayoutPanel.SetColumnSpan(this.datagridviewpanel, 8);
            this.datagridviewpanel.Controls.Add(this.MasterItemdataGridView);
            this.datagridviewpanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.datagridviewpanel.Location = new System.Drawing.Point(3, 85);
            this.datagridviewpanel.Name = "datagridviewpanel";
            this.datagridviewpanel.Size = new System.Drawing.Size(773, 170);
            this.datagridviewpanel.TabIndex = 21;
            // 
            // MasterItemdataGridView
            // 
            this.MasterItemdataGridView.AllowUserToAddRows = false;
            this.MasterItemdataGridView.AllowUserToDeleteRows = false;
            this.MasterItemdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.MasterItemdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NO,
            this.ITEMID_ALIAS,
            this.ITEMID,
            this.ITEMNAME,
            this.ITEMIDQTY,
            this.ITEMNAMEQTY,
            this.STOCK,
            this.QUANTITY,
            this.UOMID,
            this.UNITPRICE,
            this.AMOUNT});
            this.MasterItemdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MasterItemdataGridView.Location = new System.Drawing.Point(0, 0);
            this.MasterItemdataGridView.Name = "MasterItemdataGridView";
            this.MasterItemdataGridView.ReadOnly = true;
            this.MasterItemdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.MasterItemdataGridView.Size = new System.Drawing.Size(773, 170);
            this.MasterItemdataGridView.TabIndex = 0;
            // 
            // NO
            // 
            this.NO.DataPropertyName = "NO";
            this.NO.FillWeight = 40F;
            this.NO.HeaderText = "NO";
            this.NO.Name = "NO";
            this.NO.ReadOnly = true;
            this.NO.Width = 40;
            // 
            // ITEMID_ALIAS
            // 
            this.ITEMID_ALIAS.DataPropertyName = "ITEMID_ALIAS";
            this.ITEMID_ALIAS.HeaderText = "ALIAS";
            this.ITEMID_ALIAS.Name = "ITEMID_ALIAS";
            this.ITEMID_ALIAS.ReadOnly = true;
            this.ITEMID_ALIAS.Visible = false;
            // 
            // ITEMID
            // 
            this.ITEMID.DataPropertyName = "ITEMID";
            this.ITEMID.HeaderText = "PART NO";
            this.ITEMID.Name = "ITEMID";
            this.ITEMID.ReadOnly = true;
            // 
            // ITEMNAME
            // 
            this.ITEMNAME.DataPropertyName = "ITEMNAME";
            this.ITEMNAME.HeaderText = "PART NAME";
            this.ITEMNAME.Name = "ITEMNAME";
            this.ITEMNAME.ReadOnly = true;
            // 
            // ITEMIDQTY
            // 
            this.ITEMIDQTY.DataPropertyName = "ITEMIDQTY";
            this.ITEMIDQTY.FillWeight = 105F;
            this.ITEMIDQTY.HeaderText = "PART NO QTY";
            this.ITEMIDQTY.Name = "ITEMIDQTY";
            this.ITEMIDQTY.ReadOnly = true;
            this.ITEMIDQTY.Width = 105;
            // 
            // ITEMNAMEQTY
            // 
            this.ITEMNAMEQTY.DataPropertyName = "ITEMNAMEQTY";
            this.ITEMNAMEQTY.FillWeight = 120F;
            this.ITEMNAMEQTY.HeaderText = "PART NAME QTY";
            this.ITEMNAMEQTY.Name = "ITEMNAMEQTY";
            this.ITEMNAMEQTY.ReadOnly = true;
            this.ITEMNAMEQTY.Width = 120;
            // 
            // STOCK
            // 
            this.STOCK.DataPropertyName = "STOCK";
            this.STOCK.FillWeight = 60F;
            this.STOCK.HeaderText = "STOCK";
            this.STOCK.Name = "STOCK";
            this.STOCK.ReadOnly = true;
            this.STOCK.Width = 60;
            // 
            // QUANTITY
            // 
            this.QUANTITY.DataPropertyName = "QUANTITY";
            this.QUANTITY.FillWeight = 80F;
            this.QUANTITY.HeaderText = "ORDER QTY";
            this.QUANTITY.Name = "QUANTITY";
            this.QUANTITY.ReadOnly = true;
            this.QUANTITY.Width = 80;
            // 
            // UOMID
            // 
            this.UOMID.DataPropertyName = "UOMID";
            this.UOMID.FillWeight = 65F;
            this.UOMID.HeaderText = "UOM";
            this.UOMID.Name = "UOMID";
            this.UOMID.ReadOnly = true;
            this.UOMID.Width = 65;
            // 
            // UNITPRICE
            // 
            this.UNITPRICE.DataPropertyName = "UNITPRICE";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle1.Format = "N2";
            this.UNITPRICE.DefaultCellStyle = dataGridViewCellStyle1;
            this.UNITPRICE.HeaderText = "UNIT PRICE";
            this.UNITPRICE.Name = "UNITPRICE";
            this.UNITPRICE.ReadOnly = true;
            this.UNITPRICE.Visible = false;
            // 
            // AMOUNT
            // 
            this.AMOUNT.DataPropertyName = "AMOUNT";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle2.Format = "N2";
            this.AMOUNT.DefaultCellStyle = dataGridViewCellStyle2;
            this.AMOUNT.HeaderText = "AMOUNT";
            this.AMOUNT.Name = "AMOUNT";
            this.AMOUNT.ReadOnly = true;
            this.AMOUNT.Visible = false;
            // 
            // CustomerIDtextBox
            // 
            this.CustomerIDtextBox.BackColor = System.Drawing.Color.White;
            this.CustomerIDtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CustomerIDtextBox.Enabled = false;
            this.CustomerIDtextBox.Location = new System.Drawing.Point(108, 59);
            this.CustomerIDtextBox.Name = "CustomerIDtextBox";
            this.CustomerIDtextBox.Size = new System.Drawing.Size(176, 20);
            this.CustomerIDtextBox.TabIndex = 5;
            this.CustomerIDtextBox.Tag = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 258);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 20);
            this.label1.TabIndex = 22;
            this.label1.Text = "Remark";
            // 
            // Spesifikasipanel
            // 
            this.Spesifikasipanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.Spesifikasipanel, 4);
            this.Spesifikasipanel.Controls.Add(this.RemarkrichTextBox);
            this.Spesifikasipanel.Location = new System.Drawing.Point(108, 261);
            this.Spesifikasipanel.Name = "Spesifikasipanel";
            this.tableLayoutPanel.SetRowSpan(this.Spesifikasipanel, 3);
            this.Spesifikasipanel.Size = new System.Drawing.Size(428, 126);
            this.Spesifikasipanel.TabIndex = 30;
            // 
            // RemarkrichTextBox
            // 
            this.RemarkrichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RemarkrichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RemarkrichTextBox.Location = new System.Drawing.Point(0, 0);
            this.RemarkrichTextBox.Name = "RemarkrichTextBox";
            this.RemarkrichTextBox.Size = new System.Drawing.Size(426, 124);
            this.RemarkrichTextBox.TabIndex = 20;
            this.RemarkrichTextBox.TabStop = false;
            this.RemarkrichTextBox.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 20);
            this.label4.TabIndex = 28;
            this.label4.Text = "Customer ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "S.O No";
            // 
            // SONotextBox
            // 
            this.SONotextBox.BackColor = System.Drawing.Color.White;
            this.SONotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SONotextBox.Enabled = false;
            this.SONotextBox.Location = new System.Drawing.Point(108, 31);
            this.SONotextBox.Name = "SONotextBox";
            this.SONotextBox.Size = new System.Drawing.Size(176, 20);
            this.SONotextBox.TabIndex = 1;
            this.SONotextBox.Tag = "";
            this.SONotextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SONotextBox_KeyDown);
            this.SONotextBox.Leave += new System.EventHandler(this.SONotextBox_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(320, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "S.O Date";
            // 
            // SODatetextBox
            // 
            this.SODatetextBox.BackColor = System.Drawing.Color.White;
            this.SODatetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.SODatetextBox, 2);
            this.SODatetextBox.Enabled = false;
            this.SODatetextBox.Location = new System.Drawing.Point(450, 31);
            this.SODatetextBox.Name = "SODatetextBox";
            this.SODatetextBox.Size = new System.Drawing.Size(140, 20);
            this.SODatetextBox.TabIndex = 3;
            this.SODatetextBox.Tag = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(596, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 20);
            this.label9.TabIndex = 19;
            this.label9.Text = "Status";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(596, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 20);
            this.label6.TabIndex = 31;
            this.label6.Text = "Status";
            // 
            // SOStatustextBox
            // 
            this.SOStatustextBox.BackColor = System.Drawing.Color.White;
            this.SOStatustextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SOStatustextBox.Enabled = false;
            this.SOStatustextBox.Location = new System.Drawing.Point(658, 31);
            this.SOStatustextBox.Name = "SOStatustextBox";
            this.SOStatustextBox.Size = new System.Drawing.Size(140, 20);
            this.SOStatustextBox.TabIndex = 4;
            this.SOStatustextBox.Tag = "";
            // 
            // OPStatustextBox
            // 
            this.OPStatustextBox.BackColor = System.Drawing.Color.White;
            this.OPStatustextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.OPStatustextBox.Enabled = false;
            this.OPStatustextBox.Location = new System.Drawing.Point(658, 3);
            this.OPStatustextBox.Name = "OPStatustextBox";
            this.OPStatustextBox.Size = new System.Drawing.Size(140, 20);
            this.OPStatustextBox.TabIndex = 32;
            this.OPStatustextBox.Tag = "";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(320, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 20);
            this.label7.TabIndex = 33;
            this.label7.Text = "O.P Date";
            // 
            // OPDatetextBox
            // 
            this.OPDatetextBox.BackColor = System.Drawing.Color.White;
            this.OPDatetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel.SetColumnSpan(this.OPDatetextBox, 2);
            this.OPDatetextBox.Enabled = false;
            this.OPDatetextBox.Location = new System.Drawing.Point(450, 3);
            this.OPDatetextBox.Name = "OPDatetextBox";
            this.OPDatetextBox.Size = new System.Drawing.Size(140, 20);
            this.OPDatetextBox.TabIndex = 34;
            this.OPDatetextBox.Tag = "0";
            // 
            // OPNOtextBox
            // 
            this.OPNOtextBox.BackColor = System.Drawing.Color.White;
            this.OPNOtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.OPNOtextBox.Enabled = false;
            this.OPNOtextBox.Location = new System.Drawing.Point(108, 3);
            this.OPNOtextBox.Name = "OPNOtextBox";
            this.OPNOtextBox.Size = new System.Drawing.Size(176, 20);
            this.OPNOtextBox.TabIndex = 35;
            this.OPNOtextBox.Tag = "";
            this.OPNOtextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OPNOtextBox_KeyDown);
            this.OPNOtextBox.Leave += new System.EventHandler(this.OPNOtextBox_Leave);
            // 
            // OutStandingbutton
            // 
            this.OutStandingbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OutStandingbutton.Location = new System.Drawing.Point(658, 364);
            this.OutStandingbutton.Name = "OutStandingbutton";
            this.OutStandingbutton.Size = new System.Drawing.Size(96, 23);
            this.OutStandingbutton.TabIndex = 29;
            this.OutStandingbutton.Text = "OUTSTANDING";
            this.OutStandingbutton.UseVisualStyleBackColor = true;
            this.OutStandingbutton.Visible = false;
            this.OutStandingbutton.Click += new System.EventHandler(this.OutStandingbutton_Click);
            // 
            // SearchOPNObutton
            // 
            this.SearchOPNObutton.BackColor = System.Drawing.Color.Transparent;
            this.SearchOPNObutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.SearchOPNObutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SearchOPNObutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.SearchOPNObutton.FlatAppearance.BorderSize = 0;
            this.SearchOPNObutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchOPNObutton.ForeColor = System.Drawing.Color.Transparent;
            this.SearchOPNObutton.Location = new System.Drawing.Point(290, 1);
            this.SearchOPNObutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.SearchOPNObutton.Name = "SearchOPNObutton";
            this.SearchOPNObutton.Size = new System.Drawing.Size(24, 24);
            this.SearchOPNObutton.TabIndex = 37;
            this.SearchOPNObutton.Tag = "";
            this.SearchOPNObutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.SearchOPNObutton.UseVisualStyleBackColor = false;
            this.SearchOPNObutton.Visible = false;
            this.SearchOPNObutton.Click += new System.EventHandler(this.SearchOPNObutton_Click);
            // 
            // SearchSONObutton
            // 
            this.SearchSONObutton.BackColor = System.Drawing.Color.Transparent;
            this.SearchSONObutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.SearchSONObutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SearchSONObutton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.SearchSONObutton.FlatAppearance.BorderSize = 0;
            this.SearchSONObutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchSONObutton.ForeColor = System.Drawing.Color.Transparent;
            this.SearchSONObutton.Location = new System.Drawing.Point(290, 29);
            this.SearchSONObutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.SearchSONObutton.Name = "SearchSONObutton";
            this.SearchSONObutton.Size = new System.Drawing.Size(24, 24);
            this.SearchSONObutton.TabIndex = 2;
            this.SearchSONObutton.Tag = "";
            this.SearchSONObutton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.SearchSONObutton.UseVisualStyleBackColor = false;
            this.SearchSONObutton.Click += new System.EventHandler(this.SearchPONObutton_Click);
            // 
            // OrderPicking
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(811, 402);
            this.Controls.Add(this.tableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OrderPicking";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Order Picking";
            this.Activated += new System.EventHandler(this.OrderPicking_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OrderPicking_FormClosed);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.datagridviewpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MasterItemdataGridView)).EndInit();
            this.Spesifikasipanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox SONotextBox;
        private System.Windows.Forms.TextBox SODatetextBox;
        private System.Windows.Forms.TextBox SOStatustextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button SearchSONObutton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button OutStandingbutton;
        private System.Windows.Forms.Button SearchOPNObutton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel Spesifikasipanel;
        private System.Windows.Forms.RichTextBox RemarkrichTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox OPStatustextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox OPDatetextBox;
        private System.Windows.Forms.TextBox OPNOtextBox;
        private System.Windows.Forms.TextBox NameCustomertextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel datagridviewpanel;
        private System.Windows.Forms.DataGridView MasterItemdataGridView;
        private System.Windows.Forms.TextBox CustomerIDtextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMID_ALIAS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMIDQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEMNAMEQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn STOCK;
        private System.Windows.Forms.DataGridViewTextBoxColumn QUANTITY;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMID;
        private System.Windows.Forms.DataGridViewTextBoxColumn UNITPRICE;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT;
    }
}