﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Sufindo
{
    public partial class Login : Form
    {
        private static Login instance = null;
        private static readonly object padlock = new object();
        private Connection conn = null;
        private DataTable dt = null;

        public Login()
        {
            InitializeComponent();
            InitializeAuthorizeUser();
        }

        public static Login sharedInstance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Login();
                    }
                    return instance;
                }
            }
        }

        public new void Show() {
            this.InitializeAuthorizeUser();
            base.Show();
        }

        private void InitializeAuthorizeUser() {
            conn = new Connection();
            dt = conn.openDataTableQuery(String.Format("SELECT * FROM M_EMPLOYEE WHERE STATUS = '{0}'", Status.ACTIVE));
        }

        private void Loginbutton_Click(object sender, EventArgs e)
        {
            if (EmployeeIDtextBox.Text.Equals(String.Empty)) {
                MessageBox.Show("Silakan Masukin Employee ID");
            }
            else if (EmployeePasswordtextBox.Text.Equals(String.Empty))
            {
                MessageBox.Show("Silakan Masukin Password");
            }
            else {
                if (dt != null)
                {
                    String enkrippassword = Utilities.EncryptPassword(EmployeePasswordtextBox.Text);
                    DataRow rows = dt.Rows
                                     .Cast<DataRow>()
                                     .Where(r => r["EMPLOYEEID"].ToString().Equals(EmployeeIDtextBox.Text))
                                     .Where(r => r["PASSWORD"].ToString().Equals(enkrippassword))
                                     .FirstOrDefault();
                    if (rows != null)
                    {
                        Dictionary<Object, Object> data = new Dictionary<Object, Object>();
                        foreach (DataColumn column in dt.Columns)
                        {
                            data.Add(column.ColumnName, rows[column.ColumnName]);
                        }
                        AuthorizeUser.sharedInstance.userdata = data;
                        AuthorizeUser.sharedInstance.setAuthenticationPrice();
                        AuthorizeUser.sharedInstance.setAuthenticationMenu();
                        EmployeeIDtextBox.Text = "";
                        EmployeePasswordtextBox.Text = "";
                        Main main = new Main();
                        main.adjustAuthenticationMenuUser = new Main.AdjustAuthenticationMenuUser(AdjustAuthenticationMenuUser);
                        main.setAdjustAuthenticationMenuUser();
                        this.Hide();
                        main.Show();


                    }
                    else
                    {
                        MessageBox.Show("Employee ID atau Password Salah");
                    }
                }
            }
        }

        void AdjustAuthenticationMenuUser(ref MenuStrip menu) {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenu(ref menu);
        }
    }
}
