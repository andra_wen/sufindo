﻿namespace Sufindo.Master
{
    partial class MasterUOM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FormItemMasterDatatableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.Spesifikasipanel = new System.Windows.Forms.Panel();
            this.InformationrichTextBox = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.UOMIDtextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.UOMNametextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.searchUOMIDbutton = new System.Windows.Forms.Button();
            this.FormItemMasterDatatableLayoutPanel.SuspendLayout();
            this.Spesifikasipanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // FormItemMasterDatatableLayoutPanel
            // 
            this.FormItemMasterDatatableLayoutPanel.ColumnCount = 8;
            this.FormItemMasterDatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.FormItemMasterDatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.FormItemMasterDatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.FormItemMasterDatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.FormItemMasterDatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.FormItemMasterDatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.FormItemMasterDatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.FormItemMasterDatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.FormItemMasterDatatableLayoutPanel.Controls.Add(this.Spesifikasipanel, 1, 2);
            this.FormItemMasterDatatableLayoutPanel.Controls.Add(this.label5, 0, 2);
            this.FormItemMasterDatatableLayoutPanel.Controls.Add(this.UOMIDtextBox, 1, 0);
            this.FormItemMasterDatatableLayoutPanel.Controls.Add(this.label3, 0, 1);
            this.FormItemMasterDatatableLayoutPanel.Controls.Add(this.UOMNametextBox, 1, 1);
            this.FormItemMasterDatatableLayoutPanel.Controls.Add(this.label2, 0, 0);
            this.FormItemMasterDatatableLayoutPanel.Controls.Add(this.searchUOMIDbutton, 4, 0);
            this.FormItemMasterDatatableLayoutPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.FormItemMasterDatatableLayoutPanel.Location = new System.Drawing.Point(2, 2);
            this.FormItemMasterDatatableLayoutPanel.Name = "FormItemMasterDatatableLayoutPanel";
            this.FormItemMasterDatatableLayoutPanel.RowCount = 3;
            this.FormItemMasterDatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.FormItemMasterDatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.FormItemMasterDatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.FormItemMasterDatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.FormItemMasterDatatableLayoutPanel.Size = new System.Drawing.Size(484, 193);
            this.FormItemMasterDatatableLayoutPanel.TabIndex = 1;
            // 
            // Spesifikasipanel
            // 
            this.Spesifikasipanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FormItemMasterDatatableLayoutPanel.SetColumnSpan(this.Spesifikasipanel, 7);
            this.Spesifikasipanel.Controls.Add(this.InformationrichTextBox);
            this.Spesifikasipanel.Location = new System.Drawing.Point(72, 57);
            this.Spesifikasipanel.Name = "Spesifikasipanel";
            this.Spesifikasipanel.Size = new System.Drawing.Size(404, 126);
            this.Spesifikasipanel.TabIndex = 4;
            // 
            // InformationrichTextBox
            // 
            this.InformationrichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.InformationrichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InformationrichTextBox.Location = new System.Drawing.Point(0, 0);
            this.InformationrichTextBox.Name = "InformationrichTextBox";
            this.InformationrichTextBox.Size = new System.Drawing.Size(402, 124);
            this.InformationrichTextBox.TabIndex = 4;
            this.InformationrichTextBox.Text = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Information";
            // 
            // UOMIDtextBox
            // 
            this.UOMIDtextBox.BackColor = System.Drawing.SystemColors.Window;
            this.UOMIDtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FormItemMasterDatatableLayoutPanel.SetColumnSpan(this.UOMIDtextBox, 3);
            this.UOMIDtextBox.Location = new System.Drawing.Point(72, 3);
            this.UOMIDtextBox.Name = "UOMIDtextBox";
            this.UOMIDtextBox.Size = new System.Drawing.Size(204, 20);
            this.UOMIDtextBox.TabIndex = 1;
            this.UOMIDtextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.UOMIDtextBox_KeyDown);
            this.UOMIDtextBox.Leave += new System.EventHandler(this.UOMIDtextBox_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "UOM Name";
            // 
            // UOMNametextBox
            // 
            this.UOMNametextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FormItemMasterDatatableLayoutPanel.SetColumnSpan(this.UOMNametextBox, 4);
            this.UOMNametextBox.Location = new System.Drawing.Point(72, 31);
            this.UOMNametextBox.Name = "UOMNametextBox";
            this.UOMNametextBox.Size = new System.Drawing.Size(255, 20);
            this.UOMNametextBox.TabIndex = 3;
            this.UOMNametextBox.Leave += new System.EventHandler(this.UOMNametextBox_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "UOM ID";
            // 
            // searchUOMIDbutton
            // 
            this.searchUOMIDbutton.BackColor = System.Drawing.Color.Transparent;
            this.searchUOMIDbutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchUOMIDbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchUOMIDbutton.FlatAppearance.BorderSize = 0;
            this.searchUOMIDbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchUOMIDbutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchUOMIDbutton.Location = new System.Drawing.Point(282, 1);
            this.searchUOMIDbutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.searchUOMIDbutton.Name = "searchUOMIDbutton";
            this.searchUOMIDbutton.Size = new System.Drawing.Size(24, 24);
            this.searchUOMIDbutton.TabIndex = 2;
            this.searchUOMIDbutton.UseVisualStyleBackColor = false;
            this.searchUOMIDbutton.Visible = false;
            this.searchUOMIDbutton.Click += new System.EventHandler(this.searchUOMIDbutton_Click);
            // 
            // MasterUOM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 195);
            this.Controls.Add(this.FormItemMasterDatatableLayoutPanel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MasterUOM";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "UOM";
            this.Activated += new System.EventHandler(this.MasterUOM_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MasterUOM_FormClosed);
            this.FormItemMasterDatatableLayoutPanel.ResumeLayout(false);
            this.FormItemMasterDatatableLayoutPanel.PerformLayout();
            this.Spesifikasipanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel FormItemMasterDatatableLayoutPanel;
        private System.Windows.Forms.TextBox UOMIDtextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox UOMNametextBox;
        private System.Windows.Forms.Button searchUOMIDbutton;
        private System.Windows.Forms.Panel Spesifikasipanel;
        private System.Windows.Forms.RichTextBox InformationrichTextBox;
        private System.Windows.Forms.Label label5;
    }
}