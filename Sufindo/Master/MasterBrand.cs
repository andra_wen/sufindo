﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Sufindo.Master
{
    public partial class MasterBrand : Form
    {
        List<Control> controlTextBox;

        Connection connection;
        MenuStrip menustripAction;
        Branddatagridview branddatagridview;

        List<String> BrandID;

        String activity = "";
        Boolean isFind = false;

        public MasterBrand(){
            InitializeComponent();
        }

        public MasterBrand(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();

            branddatagridview = null;

            controlTextBox = new List<Control>();
            this.menustripAction = menustripAction; 
            //DatatableLayoutPanel.Location = new Point((this.Size.Width - DatatableLayoutPanel.Size.Width) / 2, (this.Size.Height - DatatableLayoutPanel.Size.Height) / 2);

            controlTextBox = new List<Control>();

            connection = new Connection();

            foreach (Control control in this.DatatableLayoutPanel.Controls) {
                if (control is TextBox) {
                    controlTextBox.Add(control);
                }
                else if (control is Panel) {
                    foreach (Control controlPanel in control.Controls) {
                        if (controlPanel is RichTextBox) {
                            controlTextBox.Add(controlPanel);
                        }
                    }
                }
                else if (control is ComboBox) {
                    controlTextBox.Add(control);
                }
            }
            newMaster();
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e){
            //MessageBox.Show("Completed");
        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e){
            if (BrandID == null) BrandID = new List<string>();
            BrandID.Clear();
            String query = String.Format("SELECT BRANDID FROM M_BRAND WHERE [STATUS] = '{0}'",Status.ACTIVE);
            SqlDataReader sqldataReader = connection.sqlDataReaders(query);

            while (sqldataReader.Read()){
                BrandID.Add(sqldataReader.GetString(0));
            }
        }

        public void save()
        {
            try
            {
                if (activity.Equals("UPDATE") || activity.Equals("INSERT"))
                {
                    if (isValidation())
                    {
                        List<SqlParameter> sqlParam = new List<SqlParameter>();

                        sqlParam.Add(new SqlParameter("@BRANDID", BrandIDtextBox.Text.ToUpper()));
                        sqlParam.Add(new SqlParameter("@BRANDNAME", BrandNametextBox.Text.ToUpper()));
                        sqlParam.Add(new SqlParameter("@INFORMATION", InformationrichTextBox.Text));
                        sqlParam.Add(new SqlParameter("@STATUS", Status.ACTIVE));
                        sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));
                        
                        String proc = activity.Equals("UPDATE") ? "UPDATE_DATA_BRAND" : "INSERT_DATA_BRAND";
                        if (connection.callProcedure(proc, sqlParam))
                        {
                            MessageBox.Show("Master Brand Berhasil diSimpan");
                            newMaster();
                            reloadAllData();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("INSERT FAILED ON : " + ex);
            }
        }

        public void newMaster()
        {
            if (!(branddatagridview != null && !branddatagridview.IsDisposed))
            {
                Utilities.clearAllField(ref controlTextBox);
                searchBrandIDbutton.Visible = false;
                foreach (var item in controlTextBox)
                {
                    if (item is RichTextBox)
                    {
                        RichTextBox richtextBox = (RichTextBox)item;
                        richtextBox.ReadOnly = false;
                    }
                    else
                    {
                        item.Enabled = true;
                    }
                    item.BackColor = SystemColors.Window;

                }
                BrandIDtextBox.AutoCompleteCustomSource.Clear();
                BrandIDtextBox.AutoCompleteMode = AutoCompleteMode.None;
                BrandIDtextBox.AutoCompleteSource = AutoCompleteSource.None;
                isFind = false;
                activity = "INSERT";
            }
        }

        public void find()
        {
            if (!searchBrandIDbutton.Visible || isFind)
            {
                searchBrandIDbutton.Visible = true;
                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("BrandIDtextBox"))
                    {
                        if (item is RichTextBox)
                        {
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = true;
                            richtextBox.BackColor = SystemColors.Info;
                            richtextBox.Text = "";
                        }
                        else if (item is ComboBox)
                        {
                            ComboBox combobox = (ComboBox)item;
                            combobox.SelectedIndex = -1;
                            item.Enabled = false;
                        }
                        else
                        {
                            item.Enabled = false;
                            item.Text = "";
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        currentTextbox.AutoCompleteCustomSource.AddRange(BrandID.ToArray());
                        currentTextbox.Text = "";
                    }

                    item.BackColor = SystemColors.Info;
                }
                InformationrichTextBox.BackColor = SystemColors.Info;
                isFind = true;
                activity = "";
            }
        }

        public void edit()
        {
            if (!searchBrandIDbutton.Visible || isFind)
            {
                searchBrandIDbutton.Visible = true;
                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("BrandIDtextBox"))
                    {
                        if (item is RichTextBox)
                        {
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = false;
                            richtextBox.BackColor = Color.White;
                        }
                        else
                        {
                            item.Enabled = true;
                            item.BackColor = Color.White;
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        currentTextbox.AutoCompleteCustomSource.AddRange(BrandID.ToArray());
                        item.BackColor = SystemColors.Info;
                    }
                }
                activity = "UPDATE";
                isFind = true;
            }
        }

        public void delete()
        {
            
            if (isFind && isAvailableBrandIDForUpdate())
            {
            
                List<SqlParameter> sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@BRANDID", BrandIDtextBox.Text));
                sqlParam.Add(new SqlParameter("@STATUS", Status.DEACTIVE));

                if (connection.callProcedure("DELETE_DATA_BRAND", sqlParam)){
                    newMaster();
                    reloadAllData();
                }
            }
        }

        private Boolean isValidation()
        {
            if (Utilities.isEmptyString(BrandIDtextBox.Text))
            {
                MessageBox.Show("Please Input Brand ID");
            }
            else if (isAvailableBrandID()){
                MessageBox.Show("Brand ID has been Available");
            }
            else if (activity.Equals("UPDATE") && !isAvailableBrandIDForUpdate()) {
                MessageBox.Show("Brand ID Has not Availabel for Update");
            }
            else if (Utilities.isEmptyString(BrandNametextBox.Text))
            {
                MessageBox.Show("Please Input Brand Name");
            }
            else
            {
                return true;
            }
            return false;
        }

        private Boolean isAvailableBrandID()
        {
            //BrandIDtextBox.Text = Utilities.removeSpace(BrandIDtextBox.Text);
            Boolean result = activity.Equals("UPDATE") ? false : Utilities.isAvailableID(BrandID, BrandIDtextBox.Text);

            return result;
        }

        private Boolean isAvailableBrandIDForUpdate() {
            //BrandIDtextBox.Text = Utilities.removeSpace(BrandIDtextBox.Text);
            Boolean result = Utilities.isAvailableID(BrandID, BrandIDtextBox.Text);
            return result;
        }

        private void MasterBrand_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }

        private void MasterBrandPassingData(DataTable sender)
        {
            sender.Rows[0]["BRANDID"].ToString();
            BrandIDtextBox.Text = sender.Rows[0]["BRANDID"].ToString();
            BrandNametextBox.Text = sender.Rows[0]["BRANDNAME"].ToString();
            InformationrichTextBox.Text = sender.Rows[0]["INFORMATION"].ToString();
        }

        private void BrandIDtextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (searchBrandIDbutton.Visible)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    Boolean isExistingPartNo = false;
                    foreach (String item in BrandID)
                    {
                        if (item.ToUpper().Equals(BrandIDtextBox.Text.ToUpper()))
                        {
                            isExistingPartNo = true;
                            BrandIDtextBox.Text = BrandIDtextBox.Text.ToUpper();
                            String query = String.Format("SELECT BRANDID, BRANDNAME, INFORMATION " +
                                                         "FROM M_BRAND " +
                                                         "WHERE BRANDID = '{0}'", BrandIDtextBox.Text);

                            DataTable datatable = connection.openDataTableQuery(query);
                            MasterBrandPassingData(datatable);
                            break;
                        }
                    }

                    if (!isExistingPartNo)
                    {
                        BrandIDtextBox.Text = "";
                        Utilities.clearAllField(ref controlTextBox);
                    }
                }
            }
            else {
                if (e.KeyCode == Keys.Enter)
                {
                    Boolean isExistingPartNo = false;
                    foreach (String item in BrandID)
                    {
                        if (item.ToUpper().Equals(BrandIDtextBox.Text.ToUpper()))
                        {
                            isExistingPartNo = true;
                            break;
                        }
                    }
                    if (isExistingPartNo)
                    {
                        MessageBox.Show("Brand Already Exist");
                        BrandIDtextBox.Text = "";
                        //Utilities.clearAllField(ref controlTextBox);
                    }
                }
            }
        }

        private void BrandIDtextBox_Leave(object sender, EventArgs e)
        {
            BrandIDtextBox.Text = BrandIDtextBox.Text.ToUpper();
            if (isFind)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in BrandID)
                {
                    if (item.ToUpper().Equals(BrandIDtextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        BrandIDtextBox.Text = BrandIDtextBox.Text.ToUpper();
                        String query = String.Format("SELECT BRANDID, BRANDNAME, INFORMATION " +
                                                     "FROM M_BRAND " +
                                                     "WHERE BRANDID = '{0}'", BrandIDtextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        MasterBrandPassingData(datatable);
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    BrandIDtextBox.Text = "";
                    Utilities.clearAllField(ref controlTextBox);
                }
            }
            else {
                Boolean isExistingPartNo = false;
                foreach (String item in BrandID)
                {
                    if (item.ToUpper().Equals(BrandIDtextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        break;
                    }
                }
                if (isExistingPartNo)
                {
                    MessageBox.Show("Brand Already Exist");
                    BrandIDtextBox.Text = "";
                    //Utilities.clearAllField(ref controlTextBox);
                }
            }
        }

        private void searchBrandIDbutton_Click(object sender, EventArgs e)
        {
            if (branddatagridview == null)
            {
                branddatagridview = new Branddatagridview("MasterBrand");
                branddatagridview.masterBrandPassingData = new Branddatagridview.MasterBrandPassingData(MasterBrandPassingData);
                //branddatagridview.MdiParent = this.MdiParent;
                branddatagridview.ShowDialog();
            }
            else if (branddatagridview.IsDisposed)
            {
                branddatagridview = new Branddatagridview("MasterBrand");
                branddatagridview.masterBrandPassingData = new Branddatagridview.MasterBrandPassingData(MasterBrandPassingData);
                //branddatagridview.MdiParent = this.MdiParent;
                branddatagridview.ShowDialog();
            }
        }

        private void BrandNametextBox_Leave(object sender, EventArgs e)
        {
            BrandNametextBox.Text = BrandNametextBox.Text.ToUpper();
        }

        private void MasterBrand_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            Main.ACTIVEFORM = this;
            reloadAllData();
        }

    }
}
