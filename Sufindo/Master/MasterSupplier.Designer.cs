﻿namespace Sufindo.Master
{
    partial class MasterSupplier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MasterSupplier));
            this.DatatableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.Spesifikasipanel = new System.Windows.Forms.Panel();
            this.AddressrichTextBox = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SupplierNotextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SupplierNametextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.NoFaxtextBox = new System.Windows.Forms.TextBox();
            this.ZonetextBox = new System.Windows.Forms.TextBox();
            this.NoTelptextBox = new System.Windows.Forms.TextBox();
            this.searchSupplierNobutton = new System.Windows.Forms.Button();
            this.ContactPersonpanel = new System.Windows.Forms.Panel();
            this.PersondataGridView = new System.Windows.Forms.DataGridView();
            this.No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContactPerson = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoPhone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Position = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Delete = new System.Windows.Forms.DataGridViewImageColumn();
            this.AddContactPersonbutton = new System.Windows.Forms.Button();
            this.DatatableLayoutPanel.SuspendLayout();
            this.Spesifikasipanel.SuspendLayout();
            this.ContactPersonpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PersondataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // DatatableLayoutPanel
            // 
            this.DatatableLayoutPanel.ColumnCount = 8;
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.Controls.Add(this.Spesifikasipanel, 1, 3);
            this.DatatableLayoutPanel.Controls.Add(this.label5, 0, 3);
            this.DatatableLayoutPanel.Controls.Add(this.SupplierNotextBox, 1, 0);
            this.DatatableLayoutPanel.Controls.Add(this.label3, 0, 1);
            this.DatatableLayoutPanel.Controls.Add(this.SupplierNametextBox, 1, 1);
            this.DatatableLayoutPanel.Controls.Add(this.label2, 0, 0);
            this.DatatableLayoutPanel.Controls.Add(this.label6, 0, 6);
            this.DatatableLayoutPanel.Controls.Add(this.label7, 0, 5);
            this.DatatableLayoutPanel.Controls.Add(this.label1, 0, 4);
            this.DatatableLayoutPanel.Controls.Add(this.NoFaxtextBox, 1, 6);
            this.DatatableLayoutPanel.Controls.Add(this.ZonetextBox, 1, 4);
            this.DatatableLayoutPanel.Controls.Add(this.NoTelptextBox, 1, 5);
            this.DatatableLayoutPanel.Controls.Add(this.searchSupplierNobutton, 4, 0);
            this.DatatableLayoutPanel.Controls.Add(this.ContactPersonpanel, 0, 8);
            this.DatatableLayoutPanel.Controls.Add(this.AddContactPersonbutton, 0, 7);
            this.DatatableLayoutPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.75F);
            this.DatatableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.DatatableLayoutPanel.Name = "DatatableLayoutPanel";
            this.DatatableLayoutPanel.RowCount = 9;
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.Size = new System.Drawing.Size(598, 527);
            this.DatatableLayoutPanel.TabIndex = 1;
            // 
            // Spesifikasipanel
            // 
            this.Spesifikasipanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.Spesifikasipanel, 7);
            this.Spesifikasipanel.Controls.Add(this.AddressrichTextBox);
            this.Spesifikasipanel.Location = new System.Drawing.Point(99, 58);
            this.Spesifikasipanel.Name = "Spesifikasipanel";
            this.Spesifikasipanel.Size = new System.Drawing.Size(402, 124);
            this.Spesifikasipanel.TabIndex = 4;
            // 
            // AddressrichTextBox
            // 
            this.AddressrichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.AddressrichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddressrichTextBox.Location = new System.Drawing.Point(0, 0);
            this.AddressrichTextBox.Name = "AddressrichTextBox";
            this.AddressrichTextBox.Size = new System.Drawing.Size(400, 122);
            this.AddressrichTextBox.TabIndex = 4;
            this.AddressrichTextBox.Text = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "Address";
            // 
            // SupplierNotextBox
            // 
            this.SupplierNotextBox.BackColor = System.Drawing.SystemColors.Window;
            this.SupplierNotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.SupplierNotextBox, 3);
            this.SupplierNotextBox.Location = new System.Drawing.Point(99, 3);
            this.SupplierNotextBox.Name = "SupplierNotextBox";
            this.SupplierNotextBox.Size = new System.Drawing.Size(204, 21);
            this.SupplierNotextBox.TabIndex = 1;
            this.SupplierNotextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SupplierNotextBox_KeyDown);
            this.SupplierNotextBox.Leave += new System.EventHandler(this.SupplierNotextBox_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Supplier Name";
            // 
            // SupplierNametextBox
            // 
            this.SupplierNametextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.SupplierNametextBox, 4);
            this.SupplierNametextBox.Location = new System.Drawing.Point(99, 31);
            this.SupplierNametextBox.Name = "SupplierNametextBox";
            this.SupplierNametextBox.Size = new System.Drawing.Size(255, 21);
            this.SupplierNametextBox.TabIndex = 3;
            this.SupplierNametextBox.Leave += new System.EventHandler(this.SupplierNametextBox_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Supplier ID";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 239);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 15);
            this.label6.TabIndex = 10;
            this.label6.Text = "No Fax";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 212);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 15);
            this.label7.TabIndex = 11;
            this.label7.Text = "No Telp";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 185);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 15);
            this.label1.TabIndex = 17;
            this.label1.Text = "Zone";
            // 
            // NoFaxtextBox
            // 
            this.NoFaxtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.NoFaxtextBox, 4);
            this.NoFaxtextBox.Location = new System.Drawing.Point(99, 242);
            this.NoFaxtextBox.Name = "NoFaxtextBox";
            this.NoFaxtextBox.Size = new System.Drawing.Size(204, 21);
            this.NoFaxtextBox.TabIndex = 7;
            this.NoFaxtextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NoFaxtextBox_KeyPress);
            // 
            // ZonetextBox
            // 
            this.ZonetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.ZonetextBox, 4);
            this.ZonetextBox.Location = new System.Drawing.Point(99, 188);
            this.ZonetextBox.Name = "ZonetextBox";
            this.ZonetextBox.Size = new System.Drawing.Size(204, 21);
            this.ZonetextBox.TabIndex = 5;
            // 
            // NoTelptextBox
            // 
            this.NoTelptextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.NoTelptextBox, 4);
            this.NoTelptextBox.Location = new System.Drawing.Point(99, 215);
            this.NoTelptextBox.Name = "NoTelptextBox";
            this.NoTelptextBox.Size = new System.Drawing.Size(204, 21);
            this.NoTelptextBox.TabIndex = 6;
            this.NoTelptextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NoTelptextBox_KeyPress);
            // 
            // searchSupplierNobutton
            // 
            this.searchSupplierNobutton.BackColor = System.Drawing.Color.Transparent;
            this.searchSupplierNobutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchSupplierNobutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchSupplierNobutton.FlatAppearance.BorderSize = 0;
            this.searchSupplierNobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchSupplierNobutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchSupplierNobutton.Location = new System.Drawing.Point(309, 1);
            this.searchSupplierNobutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.searchSupplierNobutton.Name = "searchSupplierNobutton";
            this.searchSupplierNobutton.Size = new System.Drawing.Size(24, 24);
            this.searchSupplierNobutton.TabIndex = 2;
            this.searchSupplierNobutton.UseVisualStyleBackColor = false;
            this.searchSupplierNobutton.Visible = false;
            this.searchSupplierNobutton.Click += new System.EventHandler(this.searchSupplierNobutton_Click);
            // 
            // ContactPersonpanel
            // 
            this.ContactPersonpanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.DatatableLayoutPanel.SetColumnSpan(this.ContactPersonpanel, 8);
            this.ContactPersonpanel.Controls.Add(this.PersondataGridView);
            this.ContactPersonpanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ContactPersonpanel.Location = new System.Drawing.Point(3, 301);
            this.ContactPersonpanel.Name = "ContactPersonpanel";
            this.ContactPersonpanel.Size = new System.Drawing.Size(592, 223);
            this.ContactPersonpanel.TabIndex = 23;
            // 
            // PersondataGridView
            // 
            this.PersondataGridView.AllowUserToAddRows = false;
            this.PersondataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PersondataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.No,
            this.ContactPerson,
            this.NoPhone,
            this.Email,
            this.Position,
            this.Delete});
            this.PersondataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PersondataGridView.Location = new System.Drawing.Point(0, 0);
            this.PersondataGridView.Name = "PersondataGridView";
            this.PersondataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PersondataGridView.Size = new System.Drawing.Size(592, 223);
            this.PersondataGridView.TabIndex = 0;
            this.PersondataGridView.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.PersondataGridView_UserDeletedRow);
            this.PersondataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.PersondataGridView_CellClick);
            this.PersondataGridView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.PersondataGridView_EditingControlShowing);
            // 
            // No
            // 
            this.No.DataPropertyName = "No";
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.No.DefaultCellStyle = dataGridViewCellStyle5;
            this.No.FillWeight = 50F;
            this.No.HeaderText = "No";
            this.No.MinimumWidth = 50;
            this.No.Name = "No";
            this.No.ReadOnly = true;
            this.No.Width = 50;
            // 
            // ContactPerson
            // 
            this.ContactPerson.DataPropertyName = "CONTACTPERSON";
            this.ContactPerson.HeaderText = "Name";
            this.ContactPerson.Name = "ContactPerson";
            // 
            // NoPhone
            // 
            this.NoPhone.DataPropertyName = "NOPHONE";
            this.NoPhone.HeaderText = "No Phone";
            this.NoPhone.Name = "NoPhone";
            // 
            // Email
            // 
            this.Email.DataPropertyName = "EMAIL";
            this.Email.HeaderText = "Email";
            this.Email.Name = "Email";
            // 
            // Position
            // 
            this.Position.DataPropertyName = "POSITION";
            this.Position.HeaderText = "Position";
            this.Position.Name = "Position";
            // 
            // Delete
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.NullValue = ((object)(resources.GetObject("dataGridViewCellStyle6.NullValue")));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.White;
            this.Delete.DefaultCellStyle = dataGridViewCellStyle6;
            this.Delete.FillWeight = 24F;
            this.Delete.HeaderText = "";
            this.Delete.Image = global::Sufindo.Properties.Resources.minus;
            this.Delete.MinimumWidth = 24;
            this.Delete.Name = "Delete";
            this.Delete.Width = 24;
            // 
            // AddContactPersonbutton
            // 
            this.AddContactPersonbutton.BackColor = System.Drawing.Color.Transparent;
            this.AddContactPersonbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.DatatableLayoutPanel.SetColumnSpan(this.AddContactPersonbutton, 2);
            this.AddContactPersonbutton.FlatAppearance.BorderSize = 0;
            this.AddContactPersonbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddContactPersonbutton.ForeColor = System.Drawing.Color.Transparent;
            this.AddContactPersonbutton.Image = global::Sufindo.Properties.Resources.plus;
            this.AddContactPersonbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.AddContactPersonbutton.Location = new System.Drawing.Point(0, 266);
            this.AddContactPersonbutton.Margin = new System.Windows.Forms.Padding(0);
            this.AddContactPersonbutton.Name = "AddContactPersonbutton";
            this.AddContactPersonbutton.Size = new System.Drawing.Size(150, 32);
            this.AddContactPersonbutton.TabIndex = 8;
            this.AddContactPersonbutton.Text = "Add Contact Person";
            this.AddContactPersonbutton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.AddContactPersonbutton.UseVisualStyleBackColor = false;
            this.AddContactPersonbutton.Click += new System.EventHandler(this.AddContactPersonbutton_Click);
            // 
            // MasterSupplier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 531);
            this.Controls.Add(this.DatatableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MasterSupplier";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Supplier";
            this.Activated += new System.EventHandler(this.MasterSupplier_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MasterSupplier_FormClosed);
            this.DatatableLayoutPanel.ResumeLayout(false);
            this.DatatableLayoutPanel.PerformLayout();
            this.Spesifikasipanel.ResumeLayout(false);
            this.ContactPersonpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PersondataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel DatatableLayoutPanel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox SupplierNametextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox NoFaxtextBox;
        private System.Windows.Forms.TextBox NoTelptextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel Spesifikasipanel;
        private System.Windows.Forms.RichTextBox AddressrichTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button searchSupplierNobutton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ZonetextBox;
        private System.Windows.Forms.Panel ContactPersonpanel;
        private System.Windows.Forms.Button AddContactPersonbutton;
        private System.Windows.Forms.DataGridView PersondataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn No;
        private System.Windows.Forms.DataGridViewTextBoxColumn ContactPerson;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoPhone;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn Position;
        private System.Windows.Forms.DataGridViewImageColumn Delete;
        private System.Windows.Forms.TextBox SupplierNotextBox;
    }
}