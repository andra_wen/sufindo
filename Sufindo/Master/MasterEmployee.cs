﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Globalization;

namespace Sufindo.Master
{
    public partial class MasterEmployee : Form
    {

        //private const int EM_SETCUEBANNER = 0x1501;
        //[DllImport("user32.dll", CharSet = CharSet.Auto)]
        //private static extern Int32 SendMessage(IntPtr hWnd, int msg, int wParam, [MarshalAs(UnmanagedType.LPWStr)]string lParam);

        List<Control> controlTextBox;

        Connection connection;
        MenuStrip menustripAction;
        Employeedatagridview supplierdatagridview;

        List<String> EmployeeNo;

        DataTable datatable = null;

        String activity = "";
        Boolean isFind = false;

        public MasterEmployee(){
            InitializeComponent();
        }

        public MasterEmployee(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();

           // SendMessage(ConfirmPasswordtextBox.Handle, EM_SETCUEBANNER, 0, "Confirm Password");
            
            supplierdatagridview = null;

            controlTextBox = new List<Control>();
            this.menustripAction = menustripAction; 
            //DatatableLayoutPanel.Location = new Point((this.Size.Width - DatatableLayoutPanel.Size.Width) / 2, (this.Size.Height - DatatableLayoutPanel.Size.Height) / 2);

            controlTextBox = new List<Control>();

            connection = new Connection();

            foreach (Control control in this.DatatableLayoutPanel.Controls) {
                if (control is TextBox) {
                    controlTextBox.Add(control);
                }
                else if (control is Panel) {
                    foreach (Control controlPanel in control.Controls) {
                        if (controlPanel is RichTextBox) {
                            controlTextBox.Add(controlPanel);
                        }
                    }
                }
                else if (control is ComboBox) {
                    controlTextBox.Add(control);
                }
            }
            newMasterEmployee();
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("MasterItem reloadAllData : " + ex.Message);
            }
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e){
            //MessageBox.Show("Completed");
        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e){
            if (EmployeeNo == null) EmployeeNo = new List<string>();
            EmployeeNo.Clear();
            String query = String.Format("SELECT EMPLOYEEID FROM M_EMPLOYEE WHERE [STATUS] = '{0}'",Status.ACTIVE);
            SqlDataReader sqldataReader = connection.sqlDataReaders(query);

            while (sqldataReader.Read())
            {
                EmployeeNo.Add(sqldataReader.GetString(0));
            }
        }

        public void save()
        {
            try
            {
                if (activity.Equals("UPDATE") || activity.Equals("INSERT"))
                {
                    if (isValidation())
                    {
                        List<SqlParameter> sqlParam = new List<SqlParameter>();
                        Console.WriteLine(DateTime.ParseExact(BODdateTimePicker.Text, "dd/MM/yyyy", null).ToString("yyyy-MM-dd"));
                       
                        sqlParam.Add(new SqlParameter("@EMPLOYEEID", EmployeeNotextBox.Text));
                        sqlParam.Add(new SqlParameter("@PASSWORD", Utilities.EncryptPassword(PasswordtextBox.Text)));
                        sqlParam.Add(new SqlParameter("@EMPLOYEENAME", EmployeeNametextBox.Text));
                        sqlParam.Add(new SqlParameter("@POSITION", PositiontextBox.Text));
                        sqlParam.Add(new SqlParameter("@ADDRESS", AddressrichTextBox.Text));
                        sqlParam.Add(new SqlParameter("@BOD", DateTime.ParseExact(BODdateTimePicker.Text, "dd/MM/yyyy", null).ToString("yyyy-MM-dd")));
                        sqlParam.Add(new SqlParameter("@NOHP", HandphonetextBox.Text));
                        sqlParam.Add(new SqlParameter("@EMAIL", EmailtextBox.Text));
                        sqlParam.Add(new SqlParameter("@JOINDATE", DateTime.ParseExact(JoindateTimePicker.Text, "dd/MM/yyyy", null).ToString("yyyy-MM-dd")));
                        sqlParam.Add(new SqlParameter("@STATUS", Status.ACTIVE));
                        sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));
                        
                        String proc = activity.Equals("UPDATE") ? "UPDATE_DATA_EMPLOYEE" : "INSERT_DATA_EMPLOYEE";
                        if (connection.callProcedure(proc, sqlParam))
                        {
                            MessageBox.Show("Employee Berhasil DiSimpan");
                            newMasterEmployee();
                            reloadAllData();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("INSERT FAILED ON insertMasterEmployee() : " + ex);
            }
        }

        public void newMasterEmployee()
        {
            if (!(supplierdatagridview != null && !supplierdatagridview.IsDisposed))
            {
                Utilities.clearAllField(ref controlTextBox);
                searchEmployeeNobutton.Visible = false;
                foreach (var item in controlTextBox)
                {
                    if (item is RichTextBox)
                    {
                        RichTextBox richtextBox = (RichTextBox)item;
                        richtextBox.ReadOnly = false;
                    }
                    else
                    {
                        item.Enabled = true;
                    }
                    item.BackColor = SystemColors.Window;

                }
                EmployeeNotextBox.AutoCompleteCustomSource.Clear();
                EmployeeNotextBox.AutoCompleteMode = AutoCompleteMode.None;
                EmployeeNotextBox.AutoCompleteSource = AutoCompleteSource.None;
                isFind = false;
                activity = "INSERT";
            }
        }

        public void find()
        {
            if (!searchEmployeeNobutton.Visible || isFind)
            {
                searchEmployeeNobutton.Visible = true;
                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("EmployeeNotextBox"))
                    {
                        if (item is RichTextBox)
                        {
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = true;
                            richtextBox.BackColor = SystemColors.Info;
                            richtextBox.Text = "";
                        }
                        else if (item is ComboBox)
                        {
                            ComboBox combobox = (ComboBox)item;
                            combobox.SelectedIndex = -1;
                            item.Enabled = false;
                        }
                        else
                        {
                            item.Enabled = false;
                            item.Text = "";
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        currentTextbox.AutoCompleteCustomSource.AddRange(EmployeeNo.ToArray());
                        currentTextbox.Text = "";
                    }

                    item.BackColor = SystemColors.Info;
                }
                AddressrichTextBox.BackColor = SystemColors.Info;
                isFind = true;
                BODdateTimePicker.Enabled = false;
                JoindateTimePicker.Enabled = false;
                activity = "";
            }
        }

        public void edit()
        {
            if (!searchEmployeeNobutton.Visible || isFind)
            {
                searchEmployeeNobutton.Visible = true;
                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("EmployeeNotextBox"))
                    {
                        if (item is RichTextBox)
                        {
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = false;
                            richtextBox.BackColor = Color.White;
                        }
                        else
                        {
                            item.Enabled = true;
                            item.BackColor = Color.White;
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        currentTextbox.AutoCompleteCustomSource.AddRange(EmployeeNo.ToArray());
                        item.BackColor = SystemColors.Info;
                    }
                }
                activity = "UPDATE";
                isFind = true;
                BODdateTimePicker.Enabled = true;
                JoindateTimePicker.Enabled = true;
            }
        }

        public void delete()
        {
            
            if (isFind && isAvailableEmployeeNoForUpdate())
            {
            
                List<SqlParameter> sqlParam = new List<SqlParameter>();
                sqlParam.Add(new SqlParameter("@EMPLOYEEID", EmployeeNotextBox.Text));
                sqlParam.Add(new SqlParameter("@STATUS", Status.DEACTIVE));
                //DataTable dt = connection.callProcedureDatatable("DELETE_DATA_EMPLOYEE", sqlParam);
                //MessageBox.Show(dt.Rows.Count.ToString());
                if (connection.callProcedure("DELETE_DATA_EMPLOYEE", sqlParam))
                {
                    newMasterEmployee();
                    reloadAllData();
                }
            }
        }

        private Boolean isValidation()
        {
            //MessageBox.Show(PasswordtextBox.Text+ " = "+ ConfirmPasswordtextBox.Text);
            if (Utilities.isEmptyString(EmployeeNotextBox.Text))
            {
                MessageBox.Show("Please Input Employee No");
            }
            else if (isAvailableEmployeeNo()) {
                MessageBox.Show("Employee No has been Available");
            }
            else if (activity.Equals("UPDATE") && !isAvailableEmployeeNoForUpdate()) {
                MessageBox.Show("Employee No Has not Availabel for Update");
            }
            else if (Utilities.isEmptyString(EmployeeNametextBox.Text))
            {
                MessageBox.Show("Please Input Employee Name");
            }
            else if (Utilities.isEmptyString(PasswordtextBox.Text)) {
                MessageBox.Show("Please Input Password");
            }
            else if(Utilities.isEmptyString(ConfirmPasswordtextBox.Text)){
                MessageBox.Show("Please Input Confirm Password");
            }
            else if(!PasswordtextBox.Text.Equals(ConfirmPasswordtextBox.Text)){
                MessageBox.Show("Password and Confirm Password Must Same");
            }
            else if (Utilities.isEmptyString(PositiontextBox.Text)) {
                MessageBox.Show("Please Input Position");
            }
            else if (Utilities.isEmptyString(AddressrichTextBox.Text))
            {
                MessageBox.Show("Please Input Address");
            }
            else if (Utilities.isEmptyString(HandphonetextBox.Text))
            {
                MessageBox.Show("Please Input Handphone");
            }
            else if (Utilities.isEmptyString(EmailtextBox.Text))
            {
                MessageBox.Show("Please Input Email");
            }
            else
            {
                if (!this.isFind)
                {
                    String query = String.Format("SELECT * FROM M_EMPLOYEE WHERE EMPLOYEEID = '{0}'", EmployeeNotextBox.Text);
                    DataTable dt = connection.openDataTableQuery(query);
                    if (dt.Rows.Count == 0)
                    {
                        return true;
                    }
                    MessageBox.Show("Employee ID Already exists");
                }
                else {
                    return true;
                }
            }
            return false;
        }

        private Boolean isAvailableEmployeeNo()
        {
            EmployeeNotextBox.Text = Utilities.removeSpace(EmployeeNotextBox.Text);
            Boolean result = activity.Equals("UPDATE") ? false : Utilities.isAvailableID(EmployeeNo, EmployeeNotextBox.Text);

            return result;
        }

        private Boolean isAvailableEmployeeNoForUpdate() {
            EmployeeNotextBox.Text = Utilities.removeSpace(EmployeeNotextBox.Text);
            Boolean result = Utilities.isAvailableID(EmployeeNo, EmployeeNotextBox.Text);
            return result;
        }

        private void NoTelptextBox_KeyPress(object sender, KeyPressEventArgs e){
            e.Handled = Utilities.onlyNumberTextBox(sender, e);
        }

        private void NoFaxtextBox_KeyPress(object sender, KeyPressEventArgs e){
            e.Handled = Utilities.onlyNumberTextBox(sender, e);
        }

        
        private void searchEmployeeNobutton_Click(object sender, EventArgs e)
        {
            if (supplierdatagridview == null)
            {
                supplierdatagridview = new Employeedatagridview("MasterEmployee");
                supplierdatagridview.masterEmployeePassingData = new Employeedatagridview.MasterEmployeePassingData(MasterEmployeePassingData);
                //supplierdatagridview.MdiParent = this.MdiParent;
                supplierdatagridview.ShowDialog();
            }
            else if (supplierdatagridview.IsDisposed)
            {
                supplierdatagridview = new Employeedatagridview("MasterEmployee");
                supplierdatagridview.masterEmployeePassingData = new Employeedatagridview.MasterEmployeePassingData(MasterEmployeePassingData);
                //supplierdatagridview.MdiParent = this.MdiParent;
                supplierdatagridview.ShowDialog();
            }
        }

        private void MasterEmployeePassingData(DataTable sender) {
            try
            {
                EmployeeNotextBox.Text = sender.Rows[0]["EMPLOYEEID"].ToString();
                PasswordtextBox.Text = sender.Rows[0]["PASSWORD"].ToString();
                EmployeeNametextBox.Text = sender.Rows[0]["EMPLOYEENAME"].ToString();
                PositiontextBox.Text = sender.Rows[0]["POSITION"].ToString();
                AddressrichTextBox.Text = sender.Rows[0]["ADDRESS"].ToString();
                BODdateTimePicker.Value = DateTime.ParseExact(sender.Rows[0]["BOD"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                HandphonetextBox.Text = sender.Rows[0]["NOHP"].ToString();
                EmailtextBox.Text = sender.Rows[0]["EMAIL"].ToString();
                JoindateTimePicker.Value = DateTime.ParseExact(sender.Rows[0]["JOINDATE"].ToString(), "dd/MM/yyyy",CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                Console.WriteLine("MasterEmployeePassingData " + ex);
            }
        }

        private void EmployeeNotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (searchEmployeeNobutton.Visible)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    Boolean isExistingPartNo = false;
                    foreach (String item in EmployeeNo)
                    {
                        if (item.ToUpper().Equals(EmployeeNotextBox.Text.ToUpper()))
                        {
                            isExistingPartNo = true;
                            EmployeeNotextBox.Text = EmployeeNotextBox.Text.ToUpper();
                            String query = String.Format("SELECT EMPLOYEEID, PASSWORD, EMPLOYEENAME, POSITION, " +
                                                         "[ADDRESS], CONVERT(VARCHAR(10), BOD, 103) AS [BOD], NOHP,"  +
                                                         "EMAIL, CONVERT(VARCHAR(10), JOINDATE, 103) AS [JOINDATE], STATUS, CREATEDBY " +
                                                         "FROM M_EMPLOYEE " +
                                                         "WHERE EMPLOYEEID = '{0}'", EmployeeNotextBox.Text);

                            DataTable datatable = connection.openDataTableQuery(query);
                            MasterEmployeePassingData(datatable);
                            break;
                        }
                    }

                    if (!isExistingPartNo)
                    {
                        EmployeeNotextBox.Text = "";
                        Utilities.clearAllField(ref controlTextBox);
                    }
                }
            }
            else {
                if (e.KeyCode == Keys.Enter)
                {
                    Boolean isExistingPartNo = false;
                    foreach (String item in EmployeeNo)
                    {
                        if (item.ToUpper().Equals(EmployeeNotextBox.Text.ToUpper()))
                        {
                            isExistingPartNo = true;
                            break;
                        }
                    }
                    if (isExistingPartNo)
                    {
                        MessageBox.Show("Employee ID Already Exist");
                        EmployeeNotextBox.Text = "";
                        //Utilities.clearAllField(ref controlTextBox);
                    }
                }
            }
        }

        private void EmployeeNotextBox_Leave(object sender, EventArgs e)
        {
            EmployeeNotextBox.Text = EmployeeNotextBox.Text.ToUpper();
            if (isFind)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in EmployeeNo)
                {
                    if (item.ToUpper().Equals(EmployeeNotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        EmployeeNotextBox.Text = EmployeeNotextBox.Text.ToUpper();
                        String query = String.Format("SELECT EMPLOYEEID, PASSWORD, EMPLOYEENAME, POSITION, " +
                                                     "[ADDRESS], CONVERT(VARCHAR(10), BOD, 103) AS [BOD], "+
                                                     "NOHP, EMAIL, CONVERT(VARCHAR(10), JOINDATE, 103) AS [JOINDATE], STATUS, CREATEDBY " +
                                                     "FROM M_EMPLOYEE " +
                                                     "WHERE EMPLOYEEID = '{0}'", EmployeeNotextBox.Text);
                        DataTable datatable = connection.openDataTableQuery(query);
                        MasterEmployeePassingData(datatable);
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    EmployeeNotextBox.Text = "";

                    Utilities.clearAllField(ref controlTextBox);
                }
            }
            else {
                Boolean isExistingPartNo = false;
                foreach (String item in EmployeeNo)
                {
                    if (item.ToUpper().Equals(EmployeeNotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        break;
                    }
                }
                if (isExistingPartNo)
                {
                    MessageBox.Show("Employee ID Already Exist");
                    EmployeeNotextBox.Text = "";
                    //Utilities.clearAllField(ref controlTextBox);
                }
            }
        }

        private void EmployeeNametextBox_Leave(object sender, EventArgs e)
        {
            EmployeeNametextBox.Text = EmployeeNametextBox.Text.ToUpper();
        }

        private void MasterCustomer_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            Main.ACTIVEFORM = this;
            reloadAllData();
        }

        private void MasterCustomer_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }

        private void HandphonetextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar));
        }
    }
}
