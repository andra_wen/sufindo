﻿namespace Sufindo.Master
{
    partial class MasterRack
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DatatableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.Spesifikasipanel = new System.Windows.Forms.Panel();
            this.InformationrichTextBox = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.RackIDtextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.RackNametextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.searchRackIDbutton = new System.Windows.Forms.Button();
            this.DatatableLayoutPanel.SuspendLayout();
            this.Spesifikasipanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // DatatableLayoutPanel
            // 
            this.DatatableLayoutPanel.ColumnCount = 8;
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.Controls.Add(this.Spesifikasipanel, 1, 2);
            this.DatatableLayoutPanel.Controls.Add(this.label5, 0, 2);
            this.DatatableLayoutPanel.Controls.Add(this.RackIDtextBox, 1, 0);
            this.DatatableLayoutPanel.Controls.Add(this.label3, 0, 1);
            this.DatatableLayoutPanel.Controls.Add(this.RackNametextBox, 1, 1);
            this.DatatableLayoutPanel.Controls.Add(this.label2, 0, 0);
            this.DatatableLayoutPanel.Controls.Add(this.searchRackIDbutton, 4, 0);
            this.DatatableLayoutPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.75F);
            this.DatatableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.DatatableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.DatatableLayoutPanel.Name = "DatatableLayoutPanel";
            this.DatatableLayoutPanel.RowCount = 3;
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.DatatableLayoutPanel.Size = new System.Drawing.Size(492, 186);
            this.DatatableLayoutPanel.TabIndex = 1;
            // 
            // Spesifikasipanel
            // 
            this.Spesifikasipanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.Spesifikasipanel, 7);
            this.Spesifikasipanel.Controls.Add(this.InformationrichTextBox);
            this.Spesifikasipanel.Location = new System.Drawing.Point(81, 58);
            this.Spesifikasipanel.Name = "Spesifikasipanel";
            this.Spesifikasipanel.Size = new System.Drawing.Size(402, 124);
            this.Spesifikasipanel.TabIndex = 4;
            // 
            // InformationrichTextBox
            // 
            this.InformationrichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.InformationrichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InformationrichTextBox.Location = new System.Drawing.Point(0, 0);
            this.InformationrichTextBox.Name = "InformationrichTextBox";
            this.InformationrichTextBox.Size = new System.Drawing.Size(400, 122);
            this.InformationrichTextBox.TabIndex = 4;
            this.InformationrichTextBox.Text = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "Information";
            // 
            // RackIDtextBox
            // 
            this.RackIDtextBox.BackColor = System.Drawing.SystemColors.Window;
            this.RackIDtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.RackIDtextBox, 3);
            this.RackIDtextBox.Location = new System.Drawing.Point(81, 3);
            this.RackIDtextBox.Name = "RackIDtextBox";
            this.RackIDtextBox.Size = new System.Drawing.Size(204, 21);
            this.RackIDtextBox.TabIndex = 1;
            this.RackIDtextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RackIDtextBox_KeyDown);
            this.RackIDtextBox.Leave += new System.EventHandler(this.RackIDtextBox_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Rack Name";
            // 
            // RackNametextBox
            // 
            this.RackNametextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.RackNametextBox, 4);
            this.RackNametextBox.Location = new System.Drawing.Point(81, 31);
            this.RackNametextBox.Name = "RackNametextBox";
            this.RackNametextBox.Size = new System.Drawing.Size(255, 21);
            this.RackNametextBox.TabIndex = 3;
            this.RackNametextBox.Leave += new System.EventHandler(this.RackNametextBox_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Rack ID";
            // 
            // searchRackIDbutton
            // 
            this.searchRackIDbutton.BackColor = System.Drawing.Color.Transparent;
            this.searchRackIDbutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchRackIDbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchRackIDbutton.FlatAppearance.BorderSize = 0;
            this.searchRackIDbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchRackIDbutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchRackIDbutton.Location = new System.Drawing.Point(291, 1);
            this.searchRackIDbutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.searchRackIDbutton.Name = "searchRackIDbutton";
            this.searchRackIDbutton.Size = new System.Drawing.Size(24, 24);
            this.searchRackIDbutton.TabIndex = 2;
            this.searchRackIDbutton.UseVisualStyleBackColor = false;
            this.searchRackIDbutton.Visible = false;
            this.searchRackIDbutton.Click += new System.EventHandler(this.searchRackIDbutton_Click);
            // 
            // MasterRack
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(497, 192);
            this.Controls.Add(this.DatatableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MasterRack";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rack";
            this.Activated += new System.EventHandler(this.MasterRack_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MasterRack_FormClosed);
            this.DatatableLayoutPanel.ResumeLayout(false);
            this.DatatableLayoutPanel.PerformLayout();
            this.Spesifikasipanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel DatatableLayoutPanel;
        private System.Windows.Forms.TextBox RackIDtextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox RackNametextBox;
        private System.Windows.Forms.Button searchRackIDbutton;
        private System.Windows.Forms.Panel Spesifikasipanel;
        private System.Windows.Forms.RichTextBox InformationrichTextBox;
        private System.Windows.Forms.Label label5;
    }
}