﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Sufindo.Master
{
    public partial class MasterUOM : Form
    {
        List<Control> controlTextBox;

        Connection connection;
        MenuStrip menustripAction;
        UOMdatagridview uomdatagridview;

        List<String> UOMID;

        String activity = "";
        Boolean isFind = false;

        public MasterUOM(){
            InitializeComponent();
        }

        public MasterUOM(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();

            controlTextBox = new List<Control>();
            this.menustripAction = menustripAction; 
            //FormItemMasterDatatableLayoutPanel.Location = new Point((this.Size.Width - FormItemMasterDatatableLayoutPanel.Size.Width) / 2, (this.Size.Height - FormItemMasterDatatableLayoutPanel.Size.Height) / 2);

            controlTextBox = new List<Control>();

            connection = new Connection();

            

            foreach (Control control in this.FormItemMasterDatatableLayoutPanel.Controls) {
                if (control is TextBox) {
                    controlTextBox.Add(control);
                }
                else if (control is Panel) {
                    foreach (Control controlPanel in control.Controls) {
                        if (controlPanel is RichTextBox) {
                            controlTextBox.Add(controlPanel);
                        }
                    }
                }
                else if (control is ComboBox) {
                    controlTextBox.Add(control);
                }
            }
            newMaster();
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e){
            //MessageBox.Show("Completed");
        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e){
            if (UOMID == null) UOMID = new List<string>();
            UOMID.Clear();
            String query = String.Format("SELECT UOMID FROM M_UOM WHERE [STATUS] = '{0}'",Status.ACTIVE);
            SqlDataReader sqldataReader = connection.sqlDataReaders(query);

            while (sqldataReader.Read())
            {
                UOMID.Add(sqldataReader.GetString(0));
            }
        }

        public void save()
        {
            try
            {
                if (activity.Equals("UPDATE") || activity.Equals("INSERT"))
                {
                    if (isValidation())
                    {
                        List<SqlParameter> sqlParam = new List<SqlParameter>();

                        sqlParam.Add(new SqlParameter("@UOMID", UOMIDtextBox.Text));
                        sqlParam.Add(new SqlParameter("@UOMNAME", UOMNametextBox.Text));
                        sqlParam.Add(new SqlParameter("@INFORMATION", InformationrichTextBox.Text));
                        sqlParam.Add(new SqlParameter("@STATUS", Status.ACTIVE));
                        sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));
                        
                        String proc = activity.Equals("UPDATE") ? "UPDATE_DATA_UOM" : "INSERT_DATA_UOM";
                        if (connection.callProcedure(proc, sqlParam))
                        {
                            MessageBox.Show("Master UOM Berhasil diSimpan");
                            reloadAllData();
                            newMaster();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("INSERT FAILED ON : " + ex);
            }
        }

        public void newMaster()
        {
            if (!(uomdatagridview != null && !uomdatagridview.IsDisposed))
            {
                Utilities.clearAllField(ref controlTextBox);
                searchUOMIDbutton.Visible = false;
                foreach (var item in controlTextBox)
                {
                    if (item is RichTextBox)
                    {
                        RichTextBox richtextBox = (RichTextBox)item;
                        richtextBox.ReadOnly = false;
                    }
                    else
                    {
                        item.Enabled = true;
                    }
                    item.BackColor = SystemColors.Window;

                }
                UOMIDtextBox.AutoCompleteCustomSource.Clear();
                UOMIDtextBox.AutoCompleteMode = AutoCompleteMode.None;
                UOMIDtextBox.AutoCompleteSource = AutoCompleteSource.None;
                isFind = false;
                activity = "INSERT";
            }
        }

        public void find()
        {
            if (!searchUOMIDbutton.Visible || isFind)
            {
                searchUOMIDbutton.Visible = true;
                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("UOMIDtextBox"))
                    {
                        if (item is RichTextBox)
                        {
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = true;
                            richtextBox.BackColor = SystemColors.Info;
                            richtextBox.Text = "";
                        }
                        else if (item is ComboBox)
                        {
                            ComboBox combobox = (ComboBox)item;
                            combobox.SelectedIndex = -1;
                            item.Enabled = false;
                        }
                        else
                        {
                            item.Enabled = false;
                            item.Text = "";
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        currentTextbox.AutoCompleteCustomSource.AddRange(UOMID.ToArray());
                        currentTextbox.Text = "";
                    }

                    item.BackColor = SystemColors.Info;
                }
                InformationrichTextBox.BackColor = SystemColors.Info;
                isFind = true;
                activity = "";
            }
        }

        public void edit()
        {
            if (!searchUOMIDbutton.Visible || isFind)
            {
                searchUOMIDbutton.Visible = true;
                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("UOMIDtextBox"))
                    {
                        if (item is RichTextBox)
                        {
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = false;
                            richtextBox.BackColor = Color.White;
                        }
                        else
                        {
                            item.Enabled = true;
                            item.BackColor = Color.White;
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        currentTextbox.AutoCompleteCustomSource.AddRange(UOMID.ToArray());
                        item.BackColor = SystemColors.Info;
                    }
                }
                activity = "UPDATE";
                isFind = true;
            }
        }

        public void delete()
        {
            
            if (isFind && isAvailableUOMIDForUpdate())
            {
            
                List<SqlParameter> sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@UOMID", UOMIDtextBox.Text));
                sqlParam.Add(new SqlParameter("@STATUS", Status.DEACTIVE));

                if (connection.callProcedure("DELETE_DATA_UOM", sqlParam))
                {
                    newMaster();
                    reloadAllData();
                }
            }
        }

        private Boolean isValidation()
        {
            if (Utilities.isEmptyString(UOMIDtextBox.Text))
            {
                MessageBox.Show("Please Input UOM ID");
            }
            else if (isAvailableUOMID())
            {
                MessageBox.Show("UOM ID has been Available");
            }
            else if (activity.Equals("UPDATE") && !isAvailableUOMIDForUpdate())
            {
                MessageBox.Show("UOM ID Has not Availabel for Update");
            }
            else if (Utilities.isEmptyString(UOMNametextBox.Text))
            {
                MessageBox.Show("Please Input UOM Name");
            }
            else
            {
                return true;
            }
            return false;
        }

        private Boolean isAvailableUOMID()
        {
            UOMIDtextBox.Text = Utilities.removeSpace(UOMIDtextBox.Text);
            Boolean result = activity.Equals("UPDATE") ? false : Utilities.isAvailableID(UOMID, UOMIDtextBox.Text);

            return result;
        }

        private Boolean isAvailableUOMIDForUpdate()
        {
            UOMIDtextBox.Text = Utilities.removeSpace(UOMIDtextBox.Text);
            Boolean result = Utilities.isAvailableID(UOMID, UOMIDtextBox.Text);
            return result;
        }

        private void searchUOMIDbutton_Click(object sender, EventArgs e)
        {
            if (uomdatagridview == null)
            {
                uomdatagridview = new UOMdatagridview("MasterUOM");
                uomdatagridview.masterUOMPassingData = new UOMdatagridview.MasterUOMPassingData(MasterUOMPassingData);
                //uomdatagridview.MdiParent = this.MdiParent;
                uomdatagridview.ShowDialog();
            }
            else if (uomdatagridview.IsDisposed)
            {
                uomdatagridview = new UOMdatagridview("MasterUOM");
                uomdatagridview.masterUOMPassingData = new UOMdatagridview.MasterUOMPassingData(MasterUOMPassingData);
                //uomdatagridview.MdiParent = this.MdiParent;
                uomdatagridview.ShowDialog();
            }
        }

        private void MasterUOMPassingData(DataTable sender){
            sender.Rows[0]["UOMID"].ToString();
            UOMIDtextBox.Text = sender.Rows[0]["UOMID"].ToString();
            UOMNametextBox.Text = sender.Rows[0]["UOMNAME"].ToString();
            InformationrichTextBox.Text = sender.Rows[0]["INFORMATION"].ToString();
        }

        private void UOMIDtextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (searchUOMIDbutton.Visible)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    Boolean isExistingPartNo = false;
                    foreach (String item in UOMID)
                    {
                        if (item.ToUpper().Equals(UOMIDtextBox.Text.ToUpper()))
                        {
                            isExistingPartNo = true;
                            UOMIDtextBox.Text = UOMIDtextBox.Text.ToUpper();
                            String query = String.Format("SELECT UOMID, UOMNAME, INFORMATION " +
                                                         "FROM M_UOM " +
                                                         "WHERE UOMID = '{0}'", UOMIDtextBox.Text);

                            DataTable datatable = connection.openDataTableQuery(query);
                            MasterUOMPassingData(datatable);
                            break;
                        }
                    }

                    if (!isExistingPartNo)
                    {
                        UOMIDtextBox.Text = "";
                        Utilities.clearAllField(ref controlTextBox);
                    }
                }
            }
            else {
                if (e.KeyCode == Keys.Enter)
                {
                    Boolean isExistingPartNo = false;
                    foreach (String item in UOMID)
                    {
                        if (item.ToUpper().Equals(UOMIDtextBox.Text.ToUpper()))
                        {
                            isExistingPartNo = true;
                            break;
                        }
                    }
                    if (isExistingPartNo)
                    {
                        MessageBox.Show("UOM Already Exist");
                        UOMIDtextBox.Text = "";
                        //Utilities.clearAllField(ref controlTextBox);
                    }
                }
            }
        }

        private void UOMIDtextBox_Leave(object sender, EventArgs e)
        {
            UOMIDtextBox.Text = UOMIDtextBox.Text.ToUpper();
            if (isFind)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in UOMID)
                {
                    if (item.ToUpper().Equals(UOMIDtextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        UOMIDtextBox.Text = UOMIDtextBox.Text.ToUpper();
                        String query = String.Format("SELECT UOMID, UOMNAME, INFORMATION " +
                                                     "FROM M_UOM " +
                                                     "WHERE UOMID = '{0}'", UOMIDtextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        MasterUOMPassingData(datatable);
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    UOMIDtextBox.Text = "";
                    Utilities.clearAllField(ref controlTextBox);
                }
            }
            else {
                Boolean isExistingPartNo = false;
                foreach (String item in UOMID)
                {
                    if (item.ToUpper().Equals(UOMIDtextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        break;
                    }
                }
                if (isExistingPartNo)
                {
                    MessageBox.Show("UOM Already Exist");
                    UOMIDtextBox.Text = "";
                    //Utilities.clearAllField(ref controlTextBox);
                }
            }
            
        }

        private void MasterUOM_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }

        private void UOMNametextBox_Leave(object sender, EventArgs e)
        {
            UOMNametextBox.Text = UOMNametextBox.Text.ToUpper();
        }

        private void MasterUOM_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            Main.ACTIVEFORM = this;
            reloadAllData();
        }
    }
}
