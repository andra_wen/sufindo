﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;

namespace Sufindo.Master
{
    public partial class MasterItem : Form
    {
        private delegate void fillComboboxCallback();

        MenuStrip menustripAction;
        Connection connection;
        DataTable mdatatable;

        List<Control> controlTextBox;

        private Itemdatagridview itemdatagridview;

        String ITEMID_ALIAS = "";
        String FILENAMEPATH = "";

        String activity = "";

        List<String> PartNo;

        Boolean isFind = false;
        byte[] byteImage;

        Bitmap defaultImage;

        #region Constructor MasterItem
        public MasterItem(){
            InitializeComponent();

            controlTextBox = new List<Control>();
        }

        public MasterItem(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            this.menustripAction = menustripAction; 
            //DatatableLayoutPanel.Location = new Point((this.Size.Width - DatatableLayoutPanel.Size.Width) / 2, (this.Size.Height - DatatableLayoutPanel.Size.Height) / 2);

            controlTextBox = new List<Control>();

            connection = new Connection();
            mdatatable = new DataTable();

            defaultImage = (Bitmap)PartpictureBox.Image;
            
            //reloadAllData();
                        
            foreach (Control control in this.DatatableLayoutPanel.Controls) {
                if (control is TextBox) {
                    controlTextBox.Add(control);
                }
                else if (control is Panel) {
                    foreach (Control controlPanel in control.Controls) {
                        if (controlPanel is RichTextBox) {
                            controlTextBox.Add(controlPanel);
                        }
                    }
                }
                else if (control is ComboBox) {
                    controlTextBox.Add(control);
                }
            }
            bindEventDrawItemCombobox();

            newMaster();


        }
        #endregion

        private void reloadAllData() {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("MasterItem reloadAllData : " + ex.Message);
            }
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e){
            //MessageBox.Show("Completed");
        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (PartNo == null) PartNo = new List<string>();
            else PartNo.Clear();
            
            SqlDataReader sqldataReader = connection.sqlDataReaders(String.Format("SELECT ITEMID FROM M_ITEM WHERE [STATUS] = '{0}'", Status.ACTIVE));

            while (sqldataReader.Read()){
                PartNo.Add(sqldataReader.GetString(0));
            }

            if(IsHandleCreated) BeginInvoke(new fillComboboxCallback(this.fillCombobox));
        }

        void fillCombobox() {
            connection.bindAutoCompleteComboBox(String.Format("SELECT DISTINCT ITEMID FROM M_ITEM WHERE [STATUS] = '{0}' ORDER BY ITEMID ASC", Status.ACTIVE), ref AliascomboBox, "ITEMID", "ITEMID");

            connection.bindAutoCompleteComboBox("SELECT UOMID, UOMNAME FROM M_UOM WHERE STATUS = 'Actived' ORDER BY UOMID ASC", ref SatuancomboBox, "UOMNAME", "UOMID");

            connection.bindAutoCompleteComboBox("SELECT BRANDID, BRANDNAME FROM M_BRAND WHERE STATUS = 'Actived' ORDER BY BRANDID ASC", ref BrandcomboBox, "BRANDNAME", "BRANDID");

            connection.bindAutoCompleteComboBox("SELECT RACKID, RACKNAME FROM M_RACK WHERE STATUS = 'Actived' ORDER BY RACKID ASC", ref RakcomboBox, "RACKNAME", "RACKID");

            AliascomboBox.SelectedIndex = -1;
            BrandcomboBox.SelectedIndex = -1;
            RakcomboBox.SelectedIndex = -1;
            SatuancomboBox.SelectedIndex = -1;
        }

        private void MasterItem_FormClosed(object sender, FormClosedEventArgs e){
            if (connection != null) connection.CloseConnection();
            
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            
            Main.ACTIVEFORM = null;
        }

        private void MasterItemPassingData(DataTable sender) {
            
            textBoxUser.Text = sender.Rows[0]["CREATEDBY"].ToString();
            String updatedate = sender.Rows[0]["UPDATEDDATE"].ToString();
            Console.WriteLine(updatedate); // 22/02/2016
            DateTime dt = DateTime.ParseExact(updatedate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            //Console.WriteLine(dt);
            upDateTimePicker.Value = dt;
            //Console.WriteLine(upDateTimePicker.Text);
            //textBoxUpdate.Text = sender.Rows[0]["UPDATEDDATE"].ToString();
            //MessageBox.Show("CREATED BY: " + sender.Rows[0]["CREATEDBY"].ToString());
            //MessageBox.Show("UPDATE: " + sender.Rows[0]["UPDATEDDATE"].ToString());
            PartNotextBox.Text = sender.Rows[0]["ITEMID"].ToString();
            PartNametextBox.Text = sender.Rows[0]["ITEMNAME"].ToString();
            AliascomboBox.Text = sender.Rows[0]["ITEMID_ALIAS"].ToString();
            //AliascomboBox.SelectedIndex = AliascomboBox.FindString(sender.Rows[0]["ITEMID_ALIAS"].ToString());
            //Console.WriteLine(sender.Rows[0]["ITEMID_ALIAS"].ToString());
            //BrandcomboBox.Text = sender.Rows[0]["BRANDID"].ToString();
            if (!sender.Rows[0]["BRANDID"].ToString().Equals(String.Empty))
            {
                BrandcomboBox.SelectedIndex = BrandcomboBox.FindString(sender.Rows[0]["BRANDID"].ToString().Trim());
            }
            SpesifikasirichTextBox.Text = sender.Rows[0]["SPEC"].ToString();
            QuantitytextBox.Text = sender.Rows[0]["QUANTITY"].ToString();
            //SatuancomboBox.Text = sender.Rows[0]["UOMID"].ToString();
            if (!sender.Rows[0]["UOMID"].ToString().Equals(String.Empty))
            {
                SatuancomboBox.SelectedIndex = SatuancomboBox.FindString(sender.Rows[0]["UOMID"].ToString().Trim());
            }
            PricetextBox.Text = sender.Rows[0]["BESTPRICE"].ToString();
            //RakcomboBox.Text = sender.Rows[0]["RACKID"].ToString();
            if (!sender.Rows[0]["RACKID"].ToString().Equals(String.Empty))
            {
                RakcomboBox.SelectedIndex = RakcomboBox.FindString(sender.Rows[0]["RACKID"].ToString());
            }
            //textBoxRAK.Text = "";

            byteImage = sender.Rows[0]["ITEMIMAGE"].ToString().Equals("") ? null : (byte[])sender.Rows[0]["ITEMIMAGE"];
            Image img = (byteImage == null) ? null : Utilities.convertByteToImage(byteImage);
            PartpictureBox.Image =  (img == null) ? defaultImage : img;
            
             
        }

        public void save() {
            try
            {
                if (activity.Equals("UPDATE") || activity.Equals("INSERT"))
                {
                    if (isValidation()){
                        ITEMID_ALIAS = (AliascomboBox.SelectedIndex == -1) ? PartNotextBox.Text : AliascomboBox.Text;

                        List<SqlParameter> sqlParam = new List<SqlParameter>();
                        QuantitytextBox.Text = QuantitytextBox.Text.Equals("") ? "0" : QuantitytextBox.Text;

                        Double price = PricetextBox.DecimalValue;

                        byte[] Image = null;
                        if (FILENAMEPATH.Equals("")) {
                            Image = ((activity.Equals("UPDATE")) ? ((byteImage == null) ? Utilities.convertImageToByte(defaultImage) : byteImage)
                                                                    : Utilities.convertImageToByte(defaultImage));
                        }
                        else Image = Utilities.convertImageToByte(FILENAMEPATH);
                        
                        sqlParam.Add(new SqlParameter("@ITEMID", PartNotextBox.Text.ToUpper()));
                        sqlParam.Add(new SqlParameter("@ITEMNAME", PartNametextBox.Text.ToUpper()));
                        sqlParam.Add(new SqlParameter("@ITEMID_ALIAS", ITEMID_ALIAS));
                        sqlParam.Add(new SqlParameter("@BRANDID", BrandcomboBox.Text));
                        sqlParam.Add(new SqlParameter("@SPEC", SpesifikasirichTextBox.Text));
                        sqlParam.Add(new SqlParameter("@QUANTITY", QuantitytextBox.Text));
                        sqlParam.Add(new SqlParameter("@UOMID", SatuancomboBox.Text));
                        sqlParam.Add(new SqlParameter("@BESTPRICE", price.ToString()));
                        sqlParam.Add(new SqlParameter("@RACKID", RakcomboBox.Text));
                        sqlParam.Add(new SqlParameter("@ITEMIMAGE", Image));
                        sqlParam.Add(new SqlParameter("@STATUS", Status.ACTIVE));
                        sqlParam.Add(new SqlParameter("@UPDATEDDATE", upDateTimePicker.Value.ToString("yyyy-MM-dd")));
                        sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));

                        String proc = activity.Equals("UPDATE") ? "UPDATE_DATA_ITEM" : "INSERT_DATA_ITEM";
                        if (connection.callProcedure(proc, sqlParam)){
                            MessageBox.Show("Master Item Berhasil diSimpan");
                            if(activity.Equals("UPDATE")){
                                edit();
                            } 
                            else {
                                newMaster();
                            }
                            reloadAllData();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("INSERT FAILED ON insertMasterItem() : " + ex);
            }
        }

        public void newMaster() {
            if (!(itemdatagridview != null && !itemdatagridview.IsDisposed))
            {
                Utilities.clearAllField(ref controlTextBox);
                searchPartNobutton.Visible = false;
                BrowseImagebutton.Visible = true;
                PartpictureBox.Image = null;
                PartpictureBox.Image = defaultImage;
                foreach (var item in controlTextBox){
                    if (item is RichTextBox)
                    {
                        RichTextBox richtextBox = (RichTextBox)item;
                        richtextBox.ReadOnly = false;
                    }
                    else
                    {
                        item.Enabled = true;
                    }
                    item.BackColor = SystemColors.Window;
                    item.Text = "";
                }
                PartNotextBox.AutoCompleteCustomSource.Clear();
                PartNotextBox.AutoCompleteMode = AutoCompleteMode.None;
                PartNotextBox.AutoCompleteSource = AutoCompleteSource.None;
                isFind = false;
                activity = "INSERT";
                textBoxUser.Enabled = false;
                upDateTimePicker.Enabled = true;
                textBoxUser.Text =  AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString();
                upDateTimePicker.Value = DateTime.Now;
            }
            textBoxBrand.Enabled = false;
            textBoxUOM.Enabled = false;
            textBoxRAK.Enabled = false;
            PartNotextBox.Focus();
        }

        public void find() {
            if (!searchPartNobutton.Visible || isFind)
            {
                searchPartNobutton.Visible = true;
                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("PartNotextBox")){
                        if (item is RichTextBox) {
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = true;
                            richtextBox.BackColor = SystemColors.Info;
                            richtextBox.Text = "";
                        }
                        else if (item is ComboBox)
                        {
                            ComboBox combobox = (ComboBox)item;
                            combobox.SelectedIndex = -1;
                            item.Enabled = false;
                        }
                        else
                        {
                            item.Enabled = false;
                            item.Text = "";
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        currentTextbox.AutoCompleteCustomSource.AddRange(PartNo.ToArray());
                        currentTextbox.Text = "";
                    }
                    item.BackColor = SystemColors.Info;
                }
                SpesifikasirichTextBox.BackColor = SystemColors.Info;
                BrowseImagebutton.Visible = false;
                PartpictureBox.Image = defaultImage;
                isFind = true;
                activity = "";
                textBoxUser.Enabled = false;
                upDateTimePicker.Enabled = false;
                QuantitytextBox.Enabled = false;
            }
            textBoxBrand.Enabled = false;
            textBoxUOM.Enabled = false;
            textBoxRAK.Enabled = false;
            PartNotextBox.Focus();
        }

        public void edit() {
            if (!searchPartNobutton.Visible || isFind)
            {
                searchPartNobutton.Visible = true;
                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("PartNotextBox"))
                    {
                        if (item is RichTextBox){
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = false;
                            richtextBox.BackColor = Color.White;
                        }
                        else{
                            item.Enabled = true;
                            item.BackColor = Color.White;
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        currentTextbox.AutoCompleteCustomSource.AddRange(PartNo.ToArray());
                        item.BackColor = SystemColors.Info;
                    }
                    item.Text = "";
                }
                PartpictureBox.Image = defaultImage;
                QuantitytextBox.Enabled = false;
                textBoxUser.Enabled = false;
                //upDateTimePicker.Enabled = false;
                upDateTimePicker.Enabled = true;
                BrowseImagebutton.Visible = true;
                activity = "UPDATE";
                isFind = true;
            }
            textBoxBrand.Enabled = false;
            textBoxUOM.Enabled = false;
            textBoxRAK.Enabled = false;
            PartNotextBox.Focus();
        }

        public void delete() {
            if (isFind && !isAvailablePartNoForUpdate())
            {
                List<SqlParameter> sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@ITEMID", PartNotextBox.Text));
                sqlParam.Add(new SqlParameter("@STATUS", Status.DEACTIVE));

                if (connection.callProcedure("DELETE_DATA_ITEM", sqlParam))
                {
                    MessageBox.Show("Data Berhasil Di Hapus");
                    newMaster();
                    reloadAllData();
                }
            }
            else if (isAvailablePartNoForUpdate()) {
                //MessageBox.Show("Part No : {0} Tidak Ditemukan.", PartNotextBox.Text);
            }
        }

        private Boolean isValidation() {
            if (Utilities.isEmptyString(PartNotextBox.Text))
            {
                MessageBox.Show("Please Input Part No");
            }
            else if (isAvailablePartNo()) {
                MessageBox.Show("Part No Item has been Available");
            }
            else if (activity.Equals("UPDATE") && isAvailablePartNoForUpdate())
            {
                MessageBox.Show("Part No Item has not been Available for Update");
            }
            else if (Utilities.isEmptyString(PartNametextBox.Text))
            {
                MessageBox.Show("Please Input Part Name");
            }
            else if (Utilities.isEmptyString(BrandcomboBox.Text))
            {
                MessageBox.Show("Please Input Brand Name");
            }
            else if (Utilities.isEmptyString(SatuancomboBox.Text))
            {
                MessageBox.Show("Please Input Satuan");
            }
            else
            {
                return true;
            }
            
            return false;
        }

        private Boolean isAvailablePartNo() {
            //PartNotextBox.Text = Utilities.removeSpace(PartNotextBox.Text);
            Boolean result = activity.Equals("UPDATE") ? false : Utilities.isAvailableID(PartNo, PartNotextBox.Text);

            return result;
        }

        private Boolean isAvailablePartNoForUpdate() {
            //PartNotextBox.Text = Utilities.removeSpace(PartNotextBox.Text);
            Boolean result = (PartNo.Find(s => s == PartNotextBox.Text) == null) ?
                             true : false;
            return result;
        }

        #region Method PriceText
        private void PricetextBox_KeyPress(object sender, KeyPressEventArgs e){
            e.Handled = Utilities.onlyNumberTextBox(sender, e);
        }
        #endregion

        #region Method ComboBox
        private void bindEventDrawItemCombobox()
        {
            foreach (var item in controlTextBox)
            {
                if (item is ComboBox)
                {
                    ComboBox combobox = (ComboBox)item;
                    combobox.DrawItem += new DrawItemEventHandler(comboBox_DrawItem);
                    combobox.EnabledChanged += new EventHandler(comboBox_EnableChanged);
                }
            }
        }

        private void AliascomboBox_KeyDown(object sender, KeyEventArgs e){
            if (e.KeyCode == Keys.Enter)
            {
                bool isExistingDisplayMember = false;
                for (int i = 0; i < AliascomboBox.Items.Count; i++)
                {
                    string name = ((DataRowView)AliascomboBox.Items[i])["ITEMID"].ToString();
                    if (AliascomboBox.Text.Equals(name))
                    {
                        isExistingDisplayMember = true;
                        break;
                    }
                }
                if (!isExistingDisplayMember)
                {
                    AliascomboBox.Text = "";
                }
            }
        }

        private void BrandcomboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                bool isExistingDisplayMember = false;
                for (int i = 0; i < BrandcomboBox.Items.Count; i++)
                {
                    string name = ((DataRowView)BrandcomboBox.Items[i])["BRANDID"].ToString();
                    if (BrandcomboBox.Text.Equals(name))
                    {
                        isExistingDisplayMember = true;
                        textBoxBrand.Text = BrandcomboBox.SelectedValue.ToString();
                        break;
                    }
                }
                if (!isExistingDisplayMember)
                {
                    BrandcomboBox.Text = "";
                    textBoxBrand.Text = "";
                }
            }
        }

        private void SatuancomboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                bool isExistingDisplayMember = false;
                for (int i = 0; i < SatuancomboBox.Items.Count; i++)
                {
                    string name = ((DataRowView)SatuancomboBox.Items[i])["UOMID"].ToString();
                    if (SatuancomboBox.Text.Equals(name))
                    {
                        isExistingDisplayMember = true;
                        textBoxUOM.Text = SatuancomboBox.SelectedValue.ToString();
                        break;
                    }
                }
                if (!isExistingDisplayMember)
                {
                    SatuancomboBox.Text = "";
                    textBoxUOM.Text = "";
                }
            }
        }

        private void RakcomboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                bool isExistingDisplayMember = false;
                for (int i = 0; i < RakcomboBox.Items.Count; i++)
                {
                    string name = ((DataRowView)RakcomboBox.Items[i])["RACKID"].ToString();
                    if (RakcomboBox.Text.Equals(name))
                    {
                        isExistingDisplayMember = true;
                        textBoxRAK.Text = RakcomboBox.SelectedValue.ToString();
                        break;
                    }
                }
                if (!isExistingDisplayMember)
                {
                    RakcomboBox.Text = "";
                    textBoxRAK.Text = "";
                }
            }
        }

        private void BrandcomboBox_Leave(object sender, EventArgs e)
        {
            BrandcomboBox.Text = BrandcomboBox.Text.ToUpper();
            bool isExistingDisplayMember = false;
            for (int i = 0; i < BrandcomboBox.Items.Count; i++)
            {
                string name = ((DataRowView)BrandcomboBox.Items[i])["BRANDID"].ToString();
                if (BrandcomboBox.Text.ToUpper().Equals(name.ToUpper()))
                {
                    isExistingDisplayMember = true;
                    textBoxBrand.Text = BrandcomboBox.SelectedValue.ToString();
                    break;
                }
            }
            if (!isExistingDisplayMember)
            {
                BrandcomboBox.Text = "";
                textBoxBrand.Text = "";
            }
        }



        private void SatuancomboBox_Leave(object sender, EventArgs e)
        {
            SatuancomboBox.Text = SatuancomboBox.Text.ToUpper();
            bool isExistingDisplayMember = false;
            for (int i = 0; i < SatuancomboBox.Items.Count; i++)
            {
                string name = ((DataRowView)SatuancomboBox.Items[i])["UOMID"].ToString();
                if (SatuancomboBox.Text.ToUpper().Equals(name.ToUpper()))
                {
                    isExistingDisplayMember = true;
                    textBoxUOM.Text = SatuancomboBox.SelectedValue.ToString();
                    break;
                }
            }
            if (!isExistingDisplayMember)
            {
                SatuancomboBox.Text = "";
                textBoxUOM.Text = "";
            }
        }

        private void RakcomboBox_Leave(object sender, EventArgs e)
        {
            RakcomboBox.Text = RakcomboBox.Text.ToUpper();
            bool isExistingDisplayMember = false;
            for (int i = 0; i < RakcomboBox.Items.Count; i++)
            {
                string name = ((DataRowView)RakcomboBox.Items[i])["RACKID"].ToString();
                if (RakcomboBox.Text.ToUpper().Equals(name.ToUpper()))
                {
                    isExistingDisplayMember = true;
                    textBoxRAK.Text = RakcomboBox.SelectedValue.ToString();
                    break;
                }
            }
            if (!isExistingDisplayMember)
            {
                RakcomboBox.Text = "";
                textBoxRAK.Text = "";
            }
        }

        private void BrandcomboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            BrandcomboBox.Text = BrandcomboBox.Text.ToUpper();
            bool isExistingDisplayMember = false;
            for (int i = 0; i < BrandcomboBox.Items.Count; i++)
            {
                string name = ((DataRowView)BrandcomboBox.Items[i])["BRANDID"].ToString();
                if (BrandcomboBox.Text.ToUpper().Equals(name.ToUpper()))
                {
                    isExistingDisplayMember = true;
                    textBoxBrand.Text = BrandcomboBox.SelectedValue.ToString();
                    break;
                }
            }
            if (!isExistingDisplayMember)
            {
                BrandcomboBox.Text = "";
                textBoxBrand.Text = "";
            }
        }

        private void SatuancomboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            SatuancomboBox.Text = SatuancomboBox.Text.ToUpper();
            bool isExistingDisplayMember = false;
            for (int i = 0; i < SatuancomboBox.Items.Count; i++)
            {
                string name = ((DataRowView)SatuancomboBox.Items[i])["UOMID"].ToString();
                if (SatuancomboBox.Text.ToUpper().Equals(name.ToUpper()))
                {
                    isExistingDisplayMember = true;
                    textBoxUOM.Text = SatuancomboBox.SelectedValue.ToString();
                    break;
                }
            }
            if (!isExistingDisplayMember)
            {
                SatuancomboBox.Text = "";
                textBoxUOM.Text = "";
            }
        }

        private void RakcomboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            RakcomboBox.Text = RakcomboBox.Text.ToUpper();
            bool isExistingDisplayMember = false;
            for (int i = 0; i < RakcomboBox.Items.Count; i++)
            {
                string name = ((DataRowView)RakcomboBox.Items[i])["RACKID"].ToString();
                if (RakcomboBox.Text.ToUpper().Equals(name.ToUpper()))
                {
                    isExistingDisplayMember = true;
                    textBoxRAK.Text = RakcomboBox.SelectedValue.ToString();
                    break;
                }
            }
            if (!isExistingDisplayMember)
            {
                RakcomboBox.Text = "";
                textBoxRAK.Text = "";
            }
        }

        void comboBox_EnableChanged(object sender, EventArgs e)
        {
            ComboBox combobox = (ComboBox)sender;
            if (combobox.Enabled)
                combobox.DropDownStyle = ComboBoxStyle.DropDown;
            else
                combobox.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        void comboBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            ComboBox combobox = (ComboBox)sender;
            Utilities.changeDisableColorComboBox(sender, e);
        }
        #endregion

        private void BrowseImagebutton_Click(object sender, EventArgs e)
        {
            String fileNamePath = Utilities.browsePictureBox(ref PartpictureBox);
            if (!fileNamePath.Equals(""))
            {
                FILENAMEPATH = fileNamePath;
            }
        }

        //private void searchbutton_Click(object sender, EventArgs e)
        //{
        //    if (itemdatagridview == null)
        //    {
        //        itemdatagridview = new Itemdatagridview("MasterItem");
        //        itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
        //        itemdatagridview.MdiParent = this.MdiParent;
        //        itemdatagridview.Show();
        //    }
        //    else if (itemdatagridview.IsDisposed)
        //    {
        //        itemdatagridview = new Itemdatagridview("MasterItem");
        //        itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
        //        itemdatagridview.MdiParent = this.MdiParent;
        //        itemdatagridview.Show();
        //    }
        //}

        private void searchPartNobutton_Click(object sender, EventArgs e){
            if (itemdatagridview == null)
            {
                itemdatagridview = new Itemdatagridview("MASTERITEM");
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
            else if (itemdatagridview.IsDisposed)
            {
                itemdatagridview = new Itemdatagridview("MASTERITEM");
                itemdatagridview.masterItemPassingData = new Itemdatagridview.MasterItemPassingData(MasterItemPassingData);
                itemdatagridview.ShowDialog();
            }
        }

        private void PartNotextBox_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                PartNotextBox.Text = PartNotextBox.Text.ToUpper();
                if (isFind)
                {
                    Boolean isExistingPartNo = false;
                    foreach (String item in PartNo)
                    {
                        if (item.ToUpper().Equals(PartNotextBox.Text.ToUpper()))
                        {
                            isExistingPartNo = true;
                            PartNotextBox.Text = PartNotextBox.Text.ToUpper();
                            String query = String.Format("SELECT ITEMID, ITEMNAME, " +
                                                         "ITEMID_ALIAS, BRANDID, SPEC, QUANTITY, UOMID, BESTPRICE, RACKID, ITEMIMAGE, CREATEDBY, CONVERT(VARCHAR(10), UPDATEDDATE, 103) AS UPDATEDDATE " +
                                                         "FROM M_ITEM " +
                                                         "WHERE ITEMID = '{0}' AND [STATUS] = '{1}'", PartNotextBox.Text, Status.ACTIVE);

                            DataTable datatable = connection.openDataTableQuery(query);
                            MasterItemPassingData(datatable);
                            break;
                        }
                    }
                    if (!isExistingPartNo)
                    {
                        PartNotextBox.Text = "";
                        PartNotextBox.Focus();
                        Utilities.clearAllField(ref controlTextBox);
                        PartpictureBox.Image = defaultImage;
                    }
                }
            }
        }

        private void PartNotextBox_Leave(object sender, EventArgs e)
        {
            PartNotextBox.Text = PartNotextBox.Text.ToUpper();
            if (isFind)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in PartNo)
                {
                    if (item.ToUpper().Equals(PartNotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        PartNotextBox.Text = PartNotextBox.Text.ToUpper();
                        String query = String.Format("SELECT ITEMID, ITEMNAME, " +
                                                     "ITEMID_ALIAS, BRANDID, SPEC, QUANTITY, UOMID, BESTPRICE, RACKID, ITEMIMAGE, CREATEDBY, CONVERT(VARCHAR(10), UPDATEDDATE, 103) AS UPDATEDDATE " +
                                                     "FROM M_ITEM " +
                                                     "WHERE ITEMID = '{0}' AND [STATUS] = '{1}'", PartNotextBox.Text, Status.ACTIVE);

                        DataTable datatable = connection.openDataTableQuery(query);
                        MasterItemPassingData(datatable);
                        break;
                    }
                }
                if (!isExistingPartNo)
                {
                    PartNotextBox.Text = "";
                    PartNotextBox.Focus();
                    Utilities.clearAllField(ref controlTextBox);
                    PartpictureBox.Image = defaultImage;
                }
            }
            else {
                Boolean isExistingPartNo = false;
                foreach (String item in PartNo)
                {
                    if (item.ToUpper().Equals(PartNotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        break;
                    }
                }
                if (isExistingPartNo)
                {
                    MessageBox.Show("Part No Already Exist");
                    PartNotextBox.Text = "";
                    PartNotextBox.Focus();
                    //Utilities.clearAllField(ref controlTextBox);
                    PartpictureBox.Image = defaultImage;
                }
                else {
                    PartNotextBox.Text = PartNotextBox.Text.ToUpper();
                    String query = String.Format("SELECT ITEMID, STATUS " +
                                                 "FROM M_ITEM " +
                                                 "WHERE ITEMID = '{0}' AND [STATUS] = '{1}'", PartNotextBox.Text, Status.DEACTIVE);

                    DataTable datatable = connection.openDataTableQuery(query);
                    if (datatable.Rows.Count == 1) {
                        DialogResult r = MessageBox.Show("Item Ini Sudah Pernah di Hapus, Apakah Mau di Aktifkan Kembali ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (r == DialogResult.Yes){
                            List<SqlParameter> sqlParam = new List<SqlParameter>();

                            sqlParam.Add(new SqlParameter("@ITEMID", PartNotextBox.Text));
                            sqlParam.Add(new SqlParameter("@STATUS", Status.ACTIVE));

                            //aktifkan kembali status item
                            if (connection.callProcedure("DELETE_DATA_ITEM", sqlParam))
                            {
                                MessageBox.Show("Data Berhasil Di Aktifkan");
                                newMaster();
                                reloadAllData();
                                PartNotextBox.Text = "";
                                PartNotextBox.Focus();
                            }
                        }
                        else {
                            PartNotextBox.Text = "";
                            PartNotextBox.Focus();
                        }
                    }
                }
            }
        }

        private void AliascomboBox_Leave(object sender, EventArgs e)
        {
            bool isExistingDisplayMember = false;
            AliascomboBox.Text = AliascomboBox.Text.ToUpper();
            for (int i = 0; i < AliascomboBox.Items.Count; i++)
            {
                string name = ((DataRowView)AliascomboBox.Items[i])["ITEMID"].ToString();
                if (AliascomboBox.Text.ToUpper().Equals(name.ToUpper()))
                {
                    isExistingDisplayMember = true;
                    break;
                }
            }
            if (!isExistingDisplayMember)
            {
                AliascomboBox.Text = "";
            }
        }

        private void PartNametextBox_Leave(object sender, EventArgs e)
        {
            PartNametextBox.Text = PartNametextBox.Text.ToUpper();
        }

        private void PartpictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            if (isFind)
            {
                if (!PartNotextBox.Text.Equals(""))
                {
                    ContextMenuStrip menu = new ContextMenuStrip();
                    menu.BackColor = Color.White;
                    var download = menu.Items.Add("Download Image");
                    download.Name = "Download";
                    menu.Items["Download"].Click += new EventHandler(DownloadPictureBox_click);
                    Point point = PartpictureBox.PointToClient(Control.MousePosition);
                    menu.Show(PartpictureBox, point);
                }
            }
        }

        void DownloadPictureBox_click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Convert.ToString(Environment.SpecialFolder.MyDocuments);
            saveFileDialog.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp; *.png)|*.jpg; *.jpeg; *.gif; *.bmp; *.png";
            saveFileDialog.FilterIndex = 1;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                PartpictureBox.Image.Save(saveFileDialog.FileName);
                MessageBox.Show("Image Has Been Downloaded");
            } 
        }

        private void MasterItem_Activated(object sender, EventArgs e)
        {
            Main.ACTIVEFORM = this;
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            reloadAllData();
        }

    }
}
