﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Sufindo.Master
{
    public partial class MasterSupplier : Form
    {
        List<Control> controlTextBox;

        Connection connection;
        MenuStrip menustripAction;
        Supplierdatagridview supplierdatagridview;

        List<String> SupplierNo;

        DataTable datatable = null;

        String activity = "";
        Boolean isFind = false;

        public MasterSupplier(){
            InitializeComponent();
        }

        public MasterSupplier(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            
            supplierdatagridview = null;

            controlTextBox = new List<Control>();
            this.menustripAction = menustripAction; 
            //DatatableLayoutPanel.Location = new Point((this.Size.Width - DatatableLayoutPanel.Size.Width) / 2, (this.Size.Height - DatatableLayoutPanel.Size.Height) / 2);

            controlTextBox = new List<Control>();

            connection = new Connection();

            datatable = Utilities.convertDatagridViewtoDataTable(PersondataGridView);

            

            foreach (Control control in this.DatatableLayoutPanel.Controls) {
                if (control is TextBox) {
                    controlTextBox.Add(control);
                }
                else if (control is Panel) {
                    foreach (Control controlPanel in control.Controls) {
                        if (controlPanel is RichTextBox) {
                            controlTextBox.Add(controlPanel);
                        }
                    }
                }
                else if (control is ComboBox) {
                    controlTextBox.Add(control);
                }
            }
            newMasterSupplier();
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("MasterItem reloadAllData : " + ex.Message);
            }
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e){
            //MessageBox.Show("Completed");
        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e){
            if (SupplierNo == null) SupplierNo = new List<string>();
            SupplierNo.Clear();
            String query = String.Format("SELECT SUPPLIERID FROM M_SUPPLIER WHERE [STATUS] = '{0}'",Status.ACTIVE);
            SqlDataReader sqldataReader = connection.sqlDataReaders(query);

            while (sqldataReader.Read())
            {
                SupplierNo.Add(sqldataReader.GetString(0));
            }
        }

        public void save()
        {
            try
            {
                if (activity.Equals("UPDATE") || activity.Equals("INSERT"))
                {
                    if (isValidation())
                    {
                        List<SqlParameter> sqlParam = new List<SqlParameter>();

                        sqlParam.Add(new SqlParameter("@SUPPLIERID", SupplierNotextBox.Text.ToUpper()));
                        sqlParam.Add(new SqlParameter("@SUPPLIERNAME", SupplierNametextBox.Text.ToUpper()));
                        sqlParam.Add(new SqlParameter("@ADDRESS", AddressrichTextBox.Text));
                        sqlParam.Add(new SqlParameter("@ZONE", ZonetextBox.Text));
                        sqlParam.Add(new SqlParameter("@NOTELP", NoTelptextBox.Text));
                        sqlParam.Add(new SqlParameter("@FAX", NoFaxtextBox.Text));
                        sqlParam.Add(new SqlParameter("@STATUS", Status.ACTIVE));
                        sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));
                        
                        String proc = activity.Equals("UPDATE") ? "UPDATE_DATA_SUPPLIER" : "INSERT_DATA_SUPPLIER";
                        if (connection.callProcedure(proc, sqlParam))
                        {
                            MessageBox.Show("Master Supplier Berhasil diSimpan");
                            if (datatable.Rows.Count != 0)
                            {
                                List<string> values = new List<string>();
                                foreach (DataRow row in this.datatable.Rows)
                                {
                                    String value = String.Format("SELECT '{0}', '{1}', '{2}', '{3}', '{4}'",
                                                                  SupplierNotextBox.Text,
                                                                  row["CONTACTPERSON"].ToString(),
                                                                  row["NOPHONE"].ToString(),
                                                                  row["EMAIL"].ToString(),
                                                                  row["POSITION"].ToString());
                                    values.Add(value);
                                }
                                String[] columns = { "SUPPLIERID", "CONTACTPERSON", "NOPHONE", "EMAIL", "POSITION" };

                                String query = String.Format("DELETE FROM D_PERSON_SUPPLIER WHERE SUPPLIERID = '{0}'",SupplierNotextBox.Text);

                                if (connection.openReaderQuery(query))
                                {
                                    if (connection.openReaderQuery(Utilities.queryMultipleInsert("D_PERSON_SUPPLIER", columns, values)))
                                    {
                                        newMasterSupplier();
                                    }
                                }
                            }
                            else {
                                newMasterSupplier();
                            }
                            reloadAllData();
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("INSERT FAILED ON insertMasterSupplier() : " + ex);
            }
        }

        public void newMasterSupplier()
        {
            if (!(supplierdatagridview != null && !supplierdatagridview.IsDisposed))
            {
                Utilities.clearAllField(ref controlTextBox);
                searchSupplierNobutton.Visible = false;
                foreach (var item in controlTextBox)
                {
                    if (item is RichTextBox)
                    {
                        RichTextBox richtextBox = (RichTextBox)item;
                        richtextBox.ReadOnly = false;
                    }
                    else
                    {
                        item.Enabled = true;
                    }
                    item.BackColor = SystemColors.Window;

                }
                SupplierNotextBox.AutoCompleteCustomSource.Clear();
                SupplierNotextBox.AutoCompleteMode = AutoCompleteMode.None;
                SupplierNotextBox.AutoCompleteSource = AutoCompleteSource.None;
                isFind = false;
                activity = "INSERT";
                AddContactPersonbutton.Visible = true;
                datatable.Clear();
                AllowUserToEditRows(true);
                PersondataGridView.AllowUserToDeleteRows = true;
                foreach (DataGridViewColumn column in PersondataGridView.Columns)
                {
                    if (column.Name.Equals("Delete"))
                    {
                        column.Visible = true;
                        break;
                    }
                }
            }
        }

        public void findMasterSupplier()
        {
            if (!searchSupplierNobutton.Visible || isFind)
            {
                searchSupplierNobutton.Visible = true;
                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("SupplierNotextBox"))
                    {
                        if (item is RichTextBox)
                        {
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = true;
                            richtextBox.BackColor = SystemColors.Info;
                            richtextBox.Text = "";
                        }
                        else if (item is ComboBox)
                        {
                            ComboBox combobox = (ComboBox)item;
                            combobox.SelectedIndex = -1;
                            item.Enabled = false;
                        }
                        else
                        {
                            item.Enabled = false;
                            item.Text = "";
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        currentTextbox.AutoCompleteCustomSource.AddRange(SupplierNo.ToArray());
                        currentTextbox.Text = "";
                    }

                    item.BackColor = SystemColors.Info;
                }
                AddressrichTextBox.BackColor = SystemColors.Info;
                isFind = true;
                AddContactPersonbutton.Visible = false;
                datatable.Clear();
                PersondataGridView.AllowUserToDeleteRows = false;
                AllowUserToEditRows(false);
                foreach (DataGridViewColumn column in PersondataGridView.Columns) {
                    if (column.Name.Equals("Delete")){
                        column.Visible = false;
                        break;
                    }
                }
                activity = "";
            }
        }

        private void AllowUserToEditRows(Boolean allow) {
            foreach (DataGridViewColumn column in PersondataGridView.Columns)
            {
                column.ReadOnly = (column.Name.Equals("No")) ? true : !allow;
            }
        }

        public void editMasterSupplier()
        {
            if (!searchSupplierNobutton.Visible || isFind)
            {
                searchSupplierNobutton.Visible = true;
                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("SupplierNotextBox"))
                    {
                        if (item is RichTextBox)
                        {
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = false;
                            richtextBox.BackColor = Color.White;
                        }
                        else
                        {
                            item.Enabled = true;
                            item.BackColor = Color.White;
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        currentTextbox.AutoCompleteCustomSource.AddRange(SupplierNo.ToArray());
                        item.BackColor = SystemColors.Info;
                    }
                }
                AddContactPersonbutton.Visible = true;
                AllowUserToEditRows(true);
                PersondataGridView.AllowUserToDeleteRows = true;
                foreach (DataGridViewColumn column in PersondataGridView.Columns)
                {
                    if (column.Name.Equals("Delete"))
                    {
                        column.Visible = true;
                        break;
                    }
                }
                activity = "UPDATE";
                isFind = true;
            }
        }

        public void deleteMasterSupplier()
        {
            
            if (isFind && isAvailableSupplierNoForUpdate())
            {
            
                List<SqlParameter> sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@SUPPLIERID", SupplierNotextBox.Text));
                sqlParam.Add(new SqlParameter("@STATUS", Status.DEACTIVE));

                if (connection.callProcedure("DELETE_DATA_SUPPLIER", sqlParam))
                {
                    newMasterSupplier();
                    reloadAllData();
                }
            }
        }

        private Boolean isValidation()
        {
            if (Utilities.isEmptyString(SupplierNotextBox.Text))
            {
                MessageBox.Show("Please Input Supplier ID");
            }
            else if (isAvailableSuppliertNo()) {
                MessageBox.Show("Supplier ID has been Available");
            }
            else if (activity.Equals("UPDATE") && !isAvailableSupplierNoForUpdate()) {
                MessageBox.Show("Supplier ID Has not Availabel for Update");
            }
            else if (Utilities.isEmptyString(SupplierNametextBox.Text))
            {
                MessageBox.Show("Please Input Supplier Name");
            }
            else if (Utilities.isEmptyString(AddressrichTextBox.Text))
            {
                MessageBox.Show("Please Input Address");
            }
            else if (Utilities.isEmptyString(ZonetextBox.Text))
            {
                MessageBox.Show("Please Input Zone");
            }
            else if (Utilities.isEmptyString(NoTelptextBox.Text))
            {
                MessageBox.Show("Please Input Telp");
            }
            else if (Utilities.isEmptyString(NoFaxtextBox.Text))
            {
                MessageBox.Show("Please Input Fax");
            }
            else if (PersondataGridView.Rows.Count == 0) {
                MessageBox.Show("Please Input Contact Person Data Minimal 1 Contact Person");
            }
            else if (checkPersonDatagridView())
            {
                MessageBox.Show("Please Complete Contact Person Data");
            }
            else
            {
                return true;
            }
            return false;
        }

        private Boolean isAvailableSuppliertNo()
        {
            SupplierNotextBox.Text = Utilities.removeSpace(SupplierNotextBox.Text);
            Boolean result = activity.Equals("UPDATE") ? false : Utilities.isAvailableID(SupplierNo, SupplierNotextBox.Text);
            
            return result;
        }

        private Boolean isAvailableSupplierNoForUpdate() {
            SupplierNotextBox.Text = Utilities.removeSpace(SupplierNotextBox.Text);
            Boolean result = Utilities.isAvailableID(SupplierNo, SupplierNotextBox.Text);
            return result;
        }

        private Boolean checkPersonDatagridView() {
            if (PersondataGridView.Rows.Count == 0)
            {
                return true;
            }
            else {
                Boolean isValidation = true;
                foreach (DataGridViewRow rows in PersondataGridView.Rows) {
                    if (!checkPersonDatagridViewCell(rows.Cells)){
                        isValidation = false;
                        break;
                    }
                }
                if(!isValidation) return true;
            }
            return false;
        }

        private Boolean checkPersonDatagridViewCell(DataGridViewCellCollection cells) {
            foreach (DataGridViewCell cell in cells)
            {
                if(cell.GetType().ToString().Contains("DataGridViewTextBoxCell")){
                    if (cell.Value.ToString().Equals("")){
                        return false;
                    }
                }
            }
            return true;
        }

        private void NoTelptextBox_KeyPress(object sender, KeyPressEventArgs e){
            e.Handled = Utilities.onlyNumberTextBox(sender, e);
        }

        private void NoFaxtextBox_KeyPress(object sender, KeyPressEventArgs e){
            e.Handled = Utilities.onlyNumberTextBox(sender, e);
        }

        private void MasterSupplier_FormClosed(object sender, FormClosedEventArgs e){
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }

        private void searchSupplierNobutton_Click(object sender, EventArgs e)
        {
            if (supplierdatagridview == null)
            {
                supplierdatagridview = new Supplierdatagridview("MasterSupplier");
                supplierdatagridview.masterItemPassingData = new Supplierdatagridview.MasterItemPassingData(MasterSupplierPassingData);
                //supplierdatagridview.MdiParent = this.MdiParent;
                supplierdatagridview.ShowDialog();
            }
            else if (supplierdatagridview.IsDisposed)
            {
                supplierdatagridview = new Supplierdatagridview("MasterSupplier");
                supplierdatagridview.masterItemPassingData = new Supplierdatagridview.MasterItemPassingData(MasterSupplierPassingData);
                //supplierdatagridview.MdiParent = this.MdiParent;
                supplierdatagridview.ShowDialog();
            }
        }

        private void MasterSupplierPassingData(DataTable sender) {
            sender.Rows[0]["SUPPLIERID"].ToString();
            SupplierNotextBox.Text = sender.Rows[0]["SUPPLIERID"].ToString();
            SupplierNametextBox.Text = sender.Rows[0]["SUPPLIERNAME"].ToString();
            AddressrichTextBox.Text = sender.Rows[0]["ADDRESS"].ToString();
            ZonetextBox.Text = sender.Rows[0]["ZONE"].ToString();
            NoTelptextBox.Text = sender.Rows[0]["NOTELP"].ToString();
            NoFaxtextBox.Text = sender.Rows[0]["FAX"].ToString();

            String query = String.Format("SELECT ROW_NUMBER() OVER (ORDER BY SUPPLIERID) AS 'No', CONTACTPERSON, NOPHONE, EMAIL, POSITION " +
                                         "FROM D_PERSON_SUPPLIER " +
                                         "WHERE SUPPLIERID = '{0}'", SupplierNotextBox.Text);

            datatable = connection.openDataTableQuery(query);

            PersondataGridView.DataSource = datatable;
        }

        private void SupplierNotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (searchSupplierNobutton.Visible)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    Boolean isExistingPartNo = false;
                    foreach (String item in SupplierNo)
                    {
                        if (item.ToUpper().Equals(SupplierNotextBox.Text.ToUpper()))
                        {
                            isExistingPartNo = true;
                            SupplierNotextBox.Text = SupplierNotextBox.Text.ToUpper();
                            String query = String.Format("SELECT SUPPLIERID, SUPPLIERNAME, [ADDRESS], ZONE, NOTELP, FAX," +
                                                         "[STATUS] FROM M_SUPPLIER " +
                                                         "WHERE SUPPLIERID = '{0}'", SupplierNotextBox.Text);

                            DataTable datatable = connection.openDataTableQuery(query);
                            MasterSupplierPassingData(datatable);
                            break;
                        }
                    }

                    if (!isExistingPartNo)
                    {
                        datatable.Clear();
                        SupplierNotextBox.Text = "";
                        Utilities.clearAllField(ref controlTextBox);
                    }
                }
            }
            else {
                if (e.KeyCode == Keys.Enter)
                {
                    Boolean isExistingPartNo = false;
                    foreach (String item in SupplierNo)
                    {
                        if (item.ToUpper().Equals(SupplierNotextBox.Text.ToUpper()))
                        {
                            isExistingPartNo = true;
                            break;
                        }
                    }
                    if (isExistingPartNo)
                    {
                        MessageBox.Show("Supplier Already Exist");
                        SupplierNotextBox.Text = "";
                        //Utilities.clearAllField(ref controlTextBox);
                    }
                }
            }
        }

        private void SupplierNotextBox_Leave(object sender, EventArgs e)
        {
            SupplierNotextBox.Text = SupplierNotextBox.Text.ToUpper();
            if (isFind)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in SupplierNo)
                {
                    if (item.ToUpper().Equals(SupplierNotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        SupplierNotextBox.Text = SupplierNotextBox.Text.ToUpper();
                        String query = String.Format("SELECT SUPPLIERID, SUPPLIERNAME, [ADDRESS], ZONE, NOTELP, FAX," +
                                                     "[STATUS] FROM M_SUPPLIER " +
                                                     "WHERE SUPPLIERID = '{0}'", SupplierNotextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        MasterSupplierPassingData(datatable);
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    SupplierNotextBox.Text = "";
                    datatable.Clear();

                    Utilities.clearAllField(ref controlTextBox);
                }
            }
            else {
                Boolean isExistingPartNo = false;
                foreach (String item in SupplierNo)
                {
                    if (item.ToUpper().Equals(SupplierNotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        break;
                    }
                }
                if (isExistingPartNo)
                {
                    MessageBox.Show("Supplier Already Exist");
                    SupplierNotextBox.Text = "";
                    //Utilities.clearAllField(ref controlTextBox);
                }
            }
        }

        private void AddContactPersonbutton_Click(object sender, EventArgs e){
            int lastRow = (datatable.Rows.Count == 0) ? 1 : datatable.Rows.Count + 1;
            datatable.Rows.Add(lastRow.ToString(), "", "", "", "");
            PersondataGridView.DataSource = datatable;
        }

        private void PersondataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress +=new KeyPressEventHandler(Control_KeyPress);
        }

        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            int currColumn = PersondataGridView.CurrentCell.ColumnIndex;
            if (PersondataGridView.Columns[currColumn].Name.Equals("NoPhone")){
                e.Handled = Utilities.onlyNumberTextBox(sender, (KeyPressEventArgs)e);
            }
        }

        private void PersondataGridView_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            if (PersondataGridView.AllowUserToDeleteRows)
            {
                for (int i = 0; i < datatable.Rows.Count; i++)
                {
                    if (datatable.Rows[i].RowState == DataRowState.Deleted)
                    {
                        datatable.Rows.RemoveAt(i);
                    }
                }
                Utilities.generateNoDataTable(ref datatable);
            }
        }

        private void PersondataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (e.ColumnIndex == 0)
                {
                    datatable.Rows.RemoveAt(e.RowIndex);
                    Utilities.generateNoDataTable(ref datatable);

                    PersondataGridView.DataSource = datatable;
                }
            }
        }

        private void SupplierNametextBox_Leave(object sender, EventArgs e)
        {
            SupplierNametextBox.Text = SupplierNametextBox.Text.ToUpper();
        }

        private void MasterSupplier_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            Main.ACTIVEFORM = this;
            reloadAllData();
        }

    }
}
