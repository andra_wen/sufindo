﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Sufindo.Master
{
    public partial class MasterRack : Form
    {
        List<Control> controlTextBox;

        Connection connection;
        MenuStrip menustripAction;
        Rackdatagridview rackdatagridview;

        List<String> RackID;

        String activity = "";
        Boolean isFind = false;

        public MasterRack(){
            InitializeComponent();
        }

        public MasterRack(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();

            controlTextBox = new List<Control>();
            this.menustripAction = menustripAction; 
            //DatatableLayoutPanel.Location = new Point((this.Size.Width - DatatableLayoutPanel.Size.Width) / 2, (this.Size.Height - DatatableLayoutPanel.Size.Height) / 2);

            controlTextBox = new List<Control>();

            connection = new Connection();

            foreach (Control control in this.DatatableLayoutPanel.Controls) {
                if (control is TextBox) {
                    controlTextBox.Add(control);
                }
                else if (control is Panel) {
                    foreach (Control controlPanel in control.Controls) {
                        if (controlPanel is RichTextBox) {
                            controlTextBox.Add(controlPanel);
                        }
                    }
                }
                else if (control is ComboBox) {
                    controlTextBox.Add(control);
                }
            }
            newMaster();
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e){
            //MessageBox.Show("Completed");
        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e){
            try
            {
                if (RackID == null) RackID = new List<string>();

                RackID.Clear();

                String query = String.Format("SELECT RACKID FROM M_RACK WHERE [STATUS] = '{0}'", Status.ACTIVE);

                SqlDataReader sqldataReader = connection.sqlDataReaders(query);

                while (sqldataReader.Read())
                {
                    RackID.Add(sqldataReader.GetString(0));
                }
            }
            catch (Exception )
            {
                
            }
        }

        public void save()
        {
            try
            {
                if (activity.Equals("UPDATE") || activity.Equals("INSERT"))
                {
                    if (isValidation())
                    {
                        List<SqlParameter> sqlParam = new List<SqlParameter>();

                        sqlParam.Add(new SqlParameter("@RACKID", RackIDtextBox.Text.ToUpper()));
                        sqlParam.Add(new SqlParameter("@RACKNAME", RackNametextBox.Text.ToUpper()));
                        sqlParam.Add(new SqlParameter("@INFORMATION", InformationrichTextBox.Text));
                        sqlParam.Add(new SqlParameter("@STATUS", Status.ACTIVE));
                        sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));
                        
                        String proc = activity.Equals("UPDATE") ? "UPDATE_DATA_RACK" : "INSERT_DATA_RACK";
                        if (connection.callProcedure(proc, sqlParam))
                        {
                            MessageBox.Show("Master Rack Berhasil diSimpan");
                            newMaster();
                            reloadAllData();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("INSERT FAILED ON : " + ex);
            }
        }

        public void newMaster()
        {
            if (!(rackdatagridview != null && !rackdatagridview.IsDisposed))
            {
                Utilities.clearAllField(ref controlTextBox);
                searchRackIDbutton.Visible = false;
                foreach (var item in controlTextBox)
                {
                    if (item is RichTextBox)
                    {
                        RichTextBox richtextBox = (RichTextBox)item;
                        richtextBox.ReadOnly = false;
                    }
                    else
                    {
                        item.Enabled = true;
                    }
                    item.BackColor = SystemColors.Window;

                }
                RackIDtextBox.AutoCompleteCustomSource.Clear();
                RackIDtextBox.AutoCompleteMode = AutoCompleteMode.None;
                RackIDtextBox.AutoCompleteSource = AutoCompleteSource.None;
                isFind = false;
                activity = "INSERT";
            }
        }

        public void find()
        {
            if (!searchRackIDbutton.Visible || isFind)
            {
                searchRackIDbutton.Visible = true;
                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("RackIDtextBox"))
                    {
                        if (item is RichTextBox)
                        {
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = true;
                            richtextBox.BackColor = SystemColors.Info;
                            richtextBox.Text = "";
                        }
                        else if (item is ComboBox)
                        {
                            ComboBox combobox = (ComboBox)item;
                            combobox.SelectedIndex = -1;
                            item.Enabled = false;
                        }
                        else
                        {
                            item.Enabled = false;
                            item.Text = "";
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        currentTextbox.AutoCompleteCustomSource.AddRange(RackID.ToArray());
                        currentTextbox.Text = "";
                    }

                    item.BackColor = SystemColors.Info;
                }
                InformationrichTextBox.BackColor = SystemColors.Info;
                isFind = true;
                activity = "";
            }
        }

        public void edit()
        {
            if (!searchRackIDbutton.Visible || isFind)
            {
                searchRackIDbutton.Visible = true;
                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("RackIDtextBox"))
                    {
                        if (item is RichTextBox)
                        {
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = false;
                            richtextBox.BackColor = Color.White;
                            richtextBox.Text = "";
                        }
                        else
                        {
                            item.Enabled = true;
                            item.BackColor = Color.White;
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        currentTextbox.AutoCompleteCustomSource.AddRange(RackID.ToArray());
                        item.BackColor = SystemColors.Info;
                    }
                }
                activity = "UPDATE";
                isFind = true;
            }
        }

        public void delete()
        {
            
            if (isFind && isAvailableRackIDForUpdate())
            {
            
                List<SqlParameter> sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@RACKID", RackIDtextBox.Text));
                sqlParam.Add(new SqlParameter("@STATUS", Status.DEACTIVE));

                if (connection.callProcedure("DELETE_DATA_RACK", sqlParam))
                {
                    newMaster();
                    reloadAllData();
                }
            }
        }

        private Boolean isValidation()
        {
            if (Utilities.isEmptyString(RackIDtextBox.Text))
            {
                MessageBox.Show("Please Input Rack ID");
            }
            else if (isAvailableRackID())
            {
                MessageBox.Show("Rack ID has been Available");
            }
            else if (activity.Equals("UPDATE") && !isAvailableRackIDForUpdate())
            {
                MessageBox.Show("Rack ID Has not Availabel for Update");
            }
            else if (Utilities.isEmptyString(RackNametextBox.Text))
            {
                MessageBox.Show("Please Input Rack Name");
            }
            else
            {
                return true;
            }
            return false;
        }

        private Boolean isAvailableRackID()
        {
            //RackIDtextBox.Text = Utilities.removeSpace(RackIDtextBox.Text);
            Boolean result = activity.Equals("UPDATE") ? false : Utilities.isAvailableID(RackID, RackIDtextBox.Text);

            return result;
        }

        private Boolean isAvailableRackIDForUpdate()
        {
            //RackIDtextBox.Text = Utilities.removeSpace(RackIDtextBox.Text);
            Boolean result = Utilities.isAvailableID(RackID, RackIDtextBox.Text);
            return result;
        }

        private void searchRackIDbutton_Click(object sender, EventArgs e)
        {
            if (rackdatagridview == null)
            {
                rackdatagridview = new Rackdatagridview("MasterRack");
                rackdatagridview.masterRackPassingData = new Rackdatagridview.MasterRackPassingData(MasterRackPassingData);
                //rackdatagridview.MdiParent = this.MdiParent;
                rackdatagridview.ShowDialog();
            }
            else if (rackdatagridview.IsDisposed)
            {
                rackdatagridview = new Rackdatagridview("MasterRack");
                rackdatagridview.masterRackPassingData = new Rackdatagridview.MasterRackPassingData(MasterRackPassingData);
                //rackdatagridview.MdiParent = this.MdiParent;
                rackdatagridview.ShowDialog();
            }
        }

        private void MasterRackPassingData(DataTable sender){
            sender.Rows[0]["RACKID"].ToString();
            RackIDtextBox.Text = sender.Rows[0]["RACKID"].ToString();
            RackNametextBox.Text = sender.Rows[0]["RACKNAME"].ToString();
            InformationrichTextBox.Text = sender.Rows[0]["INFORMATION"].ToString();
        }

        private void RackIDtextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (searchRackIDbutton.Visible)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    Boolean isExistingPartNo = false;
                    foreach (String item in RackID)
                    {
                        if (item.ToUpper().Equals(RackIDtextBox.Text.ToUpper()))
                        {
                            isExistingPartNo = true;
                            RackIDtextBox.Text = RackIDtextBox.Text.ToUpper();
                            String query = String.Format("SELECT RACKID, RACKNAME, INFORMATION " +
                                                         "FROM M_RACK " +
                                                         "WHERE RACKID = '{0}'", RackIDtextBox.Text);

                            DataTable datatable = connection.openDataTableQuery(query);
                            MasterRackPassingData(datatable);
                            break;
                        }
                    }

                    if (!isExistingPartNo)
                    {
                        RackIDtextBox.Text = "";
                        Utilities.clearAllField(ref controlTextBox);
                    }
                }
            }
            else {
                if (e.KeyCode == Keys.Enter)
                {
                    Boolean isExistingPartNo = false;
                    foreach (String item in RackID)
                    {
                        if (item.ToUpper().Equals(RackIDtextBox.Text.ToUpper()))
                        {
                            isExistingPartNo = true;
                            break;
                        }
                    }
                    if (isExistingPartNo)
                    {
                        MessageBox.Show("Rack Already Exist");
                        RackIDtextBox.Text = "";
                        //Utilities.clearAllField(ref controlTextBox);
                    }
                }
            }
        }

        private void RackIDtextBox_Leave(object sender, EventArgs e)
        {
            RackIDtextBox.Text = RackIDtextBox.Text.ToUpper();
            if (isFind)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in RackID)
                {
                    if (item.ToUpper().Equals(RackIDtextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        RackIDtextBox.Text = RackIDtextBox.Text.ToUpper();
                        String query = String.Format("SELECT RACKID, RACKNAME, INFORMATION " +
                                                     "FROM M_RACK " +
                                                     "WHERE RACKID = '{0}'", RackIDtextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        MasterRackPassingData(datatable);
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    RackIDtextBox.Text = "";
                    Utilities.clearAllField(ref controlTextBox);
                }
            }
            else {
                Boolean isExistingPartNo = false;
                foreach (String item in RackID)
                {
                    if (item.ToUpper().Equals(RackIDtextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        break;
                    }
                }
                if (isExistingPartNo)
                {
                    MessageBox.Show("Rack Already Exist");
                    RackIDtextBox.Text = "";
                    //Utilities.clearAllField(ref controlTextBox);
                }
            }
        }

        private void MasterRack_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
        }

        private void RackNametextBox_Leave(object sender, EventArgs e)
        {
            RackNametextBox.Text = RackNametextBox.Text.ToUpper();
        }

        private void MasterRack_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            Main.ACTIVEFORM = this;
            reloadAllData();
        }

    }
}
