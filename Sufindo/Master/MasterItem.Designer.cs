﻿namespace Sufindo.Master
{
    partial class MasterItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DatatableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxUser = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Spesifikasipanel = new System.Windows.Forms.Panel();
            this.SpesifikasirichTextBox = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.PartNotextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.PartNametextBox = new System.Windows.Forms.TextBox();
            this.AliascomboBox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.BrandcomboBox = new System.Windows.Forms.ComboBox();
            this.RakcomboBox = new System.Windows.Forms.ComboBox();
            this.SatuancomboBox = new System.Windows.Forms.ComboBox();
            this.pictureboxpanel = new System.Windows.Forms.Panel();
            this.BrowseImagebutton = new System.Windows.Forms.Button();
            this.PartpictureBox = new System.Windows.Forms.PictureBox();
            this.searchPartNobutton = new System.Windows.Forms.Button();
            this.QuantitytextBox = new CustomControls.TextBoxSL();
            this.PricetextBox = new CustomControls.TextBoxSL();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxBrand = new System.Windows.Forms.TextBox();
            this.textBoxUOM = new System.Windows.Forms.TextBox();
            this.textBoxRAK = new System.Windows.Forms.TextBox();
            this.searchBrandbutton = new System.Windows.Forms.Button();
            this.searchSatuanbutton = new System.Windows.Forms.Button();
            this.searchRakbutton = new System.Windows.Forms.Button();
            this.upDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.DatatableLayoutPanel.SuspendLayout();
            this.Spesifikasipanel.SuspendLayout();
            this.pictureboxpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PartpictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // DatatableLayoutPanel
            // 
            this.DatatableLayoutPanel.ColumnCount = 8;
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.Controls.Add(this.label11, 0, 11);
            this.DatatableLayoutPanel.Controls.Add(this.label9, 0, 10);
            this.DatatableLayoutPanel.Controls.Add(this.label7, 0, 8);
            this.DatatableLayoutPanel.Controls.Add(this.Spesifikasipanel, 1, 5);
            this.DatatableLayoutPanel.Controls.Add(this.label5, 0, 5);
            this.DatatableLayoutPanel.Controls.Add(this.label4, 0, 3);
            this.DatatableLayoutPanel.Controls.Add(this.PartNotextBox, 1, 0);
            this.DatatableLayoutPanel.Controls.Add(this.label2, 0, 0);
            this.DatatableLayoutPanel.Controls.Add(this.label3, 0, 1);
            this.DatatableLayoutPanel.Controls.Add(this.label1, 0, 2);
            this.DatatableLayoutPanel.Controls.Add(this.PartNametextBox, 1, 1);
            this.DatatableLayoutPanel.Controls.Add(this.AliascomboBox, 1, 2);
            this.DatatableLayoutPanel.Controls.Add(this.label8, 0, 7);
            this.DatatableLayoutPanel.Controls.Add(this.label6, 0, 9);
            this.DatatableLayoutPanel.Controls.Add(this.BrandcomboBox, 1, 3);
            this.DatatableLayoutPanel.Controls.Add(this.RakcomboBox, 1, 10);
            this.DatatableLayoutPanel.Controls.Add(this.SatuancomboBox, 1, 7);
            this.DatatableLayoutPanel.Controls.Add(this.pictureboxpanel, 7, 0);
            this.DatatableLayoutPanel.Controls.Add(this.searchPartNobutton, 6, 0);
            this.DatatableLayoutPanel.Controls.Add(this.QuantitytextBox, 1, 8);
            this.DatatableLayoutPanel.Controls.Add(this.PricetextBox, 1, 9);
            this.DatatableLayoutPanel.Controls.Add(this.textBoxBrand, 3, 3);
            this.DatatableLayoutPanel.Controls.Add(this.textBoxUOM, 3, 7);
            this.DatatableLayoutPanel.Controls.Add(this.textBoxRAK, 3, 10);
            this.DatatableLayoutPanel.Controls.Add(this.searchBrandbutton, 6, 3);
            this.DatatableLayoutPanel.Controls.Add(this.searchSatuanbutton, 6, 7);
            this.DatatableLayoutPanel.Controls.Add(this.searchRakbutton, 6, 10);
            this.DatatableLayoutPanel.Controls.Add(this.label10, 4, 9);
            this.DatatableLayoutPanel.Controls.Add(this.upDateTimePicker, 5, 9);
            this.DatatableLayoutPanel.Controls.Add(this.textBoxUser, 1, 11);
            this.DatatableLayoutPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.DatatableLayoutPanel.Location = new System.Drawing.Point(2, 3);
            this.DatatableLayoutPanel.Name = "DatatableLayoutPanel";
            this.DatatableLayoutPanel.RowCount = 12;
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.DatatableLayoutPanel.Size = new System.Drawing.Size(611, 414);
            this.DatatableLayoutPanel.TabIndex = 0;
            // 
            // textBoxUser
            // 
            this.textBoxUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.textBoxUser, 3);
            this.textBoxUser.Enabled = false;
            this.textBoxUser.Location = new System.Drawing.Point(66, 386);
            this.textBoxUser.Name = "textBoxUser";
            this.textBoxUser.Size = new System.Drawing.Size(142, 20);
            this.textBoxUser.TabIndex = 16;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 383);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "User";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 355);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Rak";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 303);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Quantity";
            // 
            // Spesifikasipanel
            // 
            this.Spesifikasipanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.Spesifikasipanel, 7);
            this.Spesifikasipanel.Controls.Add(this.SpesifikasirichTextBox);
            this.Spesifikasipanel.Location = new System.Drawing.Point(66, 148);
            this.Spesifikasipanel.Name = "Spesifikasipanel";
            this.DatatableLayoutPanel.SetRowSpan(this.Spesifikasipanel, 2);
            this.Spesifikasipanel.Size = new System.Drawing.Size(402, 124);
            this.Spesifikasipanel.TabIndex = 6;
            // 
            // SpesifikasirichTextBox
            // 
            this.SpesifikasirichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SpesifikasirichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SpesifikasirichTextBox.Location = new System.Drawing.Point(0, 0);
            this.SpesifikasirichTextBox.Name = "SpesifikasirichTextBox";
            this.SpesifikasirichTextBox.Size = new System.Drawing.Size(400, 122);
            this.SpesifikasirichTextBox.TabIndex = 7;
            this.SpesifikasirichTextBox.Text = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 145);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Spesifikasi";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Brand";
            // 
            // PartNotextBox
            // 
            this.PartNotextBox.BackColor = System.Drawing.SystemColors.Window;
            this.PartNotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.PartNotextBox, 5);
            this.PartNotextBox.Location = new System.Drawing.Point(66, 3);
            this.PartNotextBox.Name = "PartNotextBox";
            this.PartNotextBox.Size = new System.Drawing.Size(255, 20);
            this.PartNotextBox.TabIndex = 1;
            this.PartNotextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PartNotextBox_KeyDown);
            this.PartNotextBox.Leave += new System.EventHandler(this.PartNotextBox_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Part No";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Part Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Alias";
            // 
            // PartNametextBox
            // 
            this.PartNametextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.PartNametextBox, 5);
            this.PartNametextBox.Location = new System.Drawing.Point(66, 31);
            this.PartNametextBox.Name = "PartNametextBox";
            this.PartNametextBox.Size = new System.Drawing.Size(255, 20);
            this.PartNametextBox.TabIndex = 2;
            this.PartNametextBox.Leave += new System.EventHandler(this.PartNametextBox_Leave);
            // 
            // AliascomboBox
            // 
            this.DatatableLayoutPanel.SetColumnSpan(this.AliascomboBox, 4);
            this.AliascomboBox.DropDownWidth = 1;
            this.AliascomboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AliascomboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.AliascomboBox.FormattingEnabled = true;
            this.AliascomboBox.Location = new System.Drawing.Point(66, 57);
            this.AliascomboBox.Name = "AliascomboBox";
            this.AliascomboBox.Size = new System.Drawing.Size(176, 21);
            this.AliascomboBox.TabIndex = 3;
            this.AliascomboBox.Leave += new System.EventHandler(this.AliascomboBox_Leave);
            this.AliascomboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AliascomboBox_KeyDown);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 275);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "UOM";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 329);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Sale Price";
            // 
            // BrandcomboBox
            // 
            this.DatatableLayoutPanel.SetColumnSpan(this.BrandcomboBox, 2);
            this.BrandcomboBox.DropDownWidth = 1;
            this.BrandcomboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BrandcomboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.BrandcomboBox.FormattingEnabled = true;
            this.BrandcomboBox.Location = new System.Drawing.Point(66, 84);
            this.BrandcomboBox.Name = "BrandcomboBox";
            this.BrandcomboBox.Size = new System.Drawing.Size(100, 21);
            this.BrandcomboBox.TabIndex = 4;
            this.BrandcomboBox.SelectedIndexChanged += new System.EventHandler(this.BrandcomboBox_SelectedIndexChanged);
            this.BrandcomboBox.Leave += new System.EventHandler(this.BrandcomboBox_Leave);
            this.BrandcomboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.BrandcomboBox_KeyDown);
            // 
            // RakcomboBox
            // 
            this.DatatableLayoutPanel.SetColumnSpan(this.RakcomboBox, 2);
            this.RakcomboBox.DropDownWidth = 1;
            this.RakcomboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RakcomboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.RakcomboBox.FormattingEnabled = true;
            this.RakcomboBox.Location = new System.Drawing.Point(66, 358);
            this.RakcomboBox.Name = "RakcomboBox";
            this.RakcomboBox.Size = new System.Drawing.Size(100, 21);
            this.RakcomboBox.TabIndex = 13;
            this.RakcomboBox.SelectedIndexChanged += new System.EventHandler(this.RakcomboBox_SelectedIndexChanged);
            this.RakcomboBox.Leave += new System.EventHandler(this.RakcomboBox_Leave);
            this.RakcomboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RakcomboBox_KeyDown);
            // 
            // SatuancomboBox
            // 
            this.DatatableLayoutPanel.SetColumnSpan(this.SatuancomboBox, 2);
            this.SatuancomboBox.DropDownWidth = 1;
            this.SatuancomboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SatuancomboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.SatuancomboBox.FormattingEnabled = true;
            this.SatuancomboBox.Location = new System.Drawing.Point(66, 278);
            this.SatuancomboBox.Name = "SatuancomboBox";
            this.SatuancomboBox.Size = new System.Drawing.Size(100, 21);
            this.SatuancomboBox.TabIndex = 8;
            this.SatuancomboBox.SelectedIndexChanged += new System.EventHandler(this.SatuancomboBox_SelectedIndexChanged);
            this.SatuancomboBox.Leave += new System.EventHandler(this.SatuancomboBox_Leave);
            this.SatuancomboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SatuancomboBox_KeyDown);
            // 
            // pictureboxpanel
            // 
            this.pictureboxpanel.Controls.Add(this.BrowseImagebutton);
            this.pictureboxpanel.Controls.Add(this.PartpictureBox);
            this.pictureboxpanel.Location = new System.Drawing.Point(405, 3);
            this.pictureboxpanel.Name = "pictureboxpanel";
            this.DatatableLayoutPanel.SetRowSpan(this.pictureboxpanel, 4);
            this.pictureboxpanel.Size = new System.Drawing.Size(198, 139);
            this.pictureboxpanel.TabIndex = 6;
            // 
            // BrowseImagebutton
            // 
            this.BrowseImagebutton.BackColor = System.Drawing.Color.White;
            this.BrowseImagebutton.FlatAppearance.BorderSize = 0;
            this.BrowseImagebutton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.BrowseImagebutton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.BrowseImagebutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BrowseImagebutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BrowseImagebutton.Location = new System.Drawing.Point(1, 115);
            this.BrowseImagebutton.Name = "BrowseImagebutton";
            this.BrowseImagebutton.Size = new System.Drawing.Size(196, 21);
            this.BrowseImagebutton.TabIndex = 6;
            this.BrowseImagebutton.Text = "Browse Image";
            this.BrowseImagebutton.UseVisualStyleBackColor = false;
            this.BrowseImagebutton.Click += new System.EventHandler(this.BrowseImagebutton_Click);
            // 
            // PartpictureBox
            // 
            this.PartpictureBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.PartpictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PartpictureBox.Image = global::Sufindo.Properties.Resources.noimage;
            this.PartpictureBox.Location = new System.Drawing.Point(0, 0);
            this.PartpictureBox.Name = "PartpictureBox";
            this.PartpictureBox.Size = new System.Drawing.Size(198, 139);
            this.PartpictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PartpictureBox.TabIndex = 15;
            this.PartpictureBox.TabStop = false;
            this.PartpictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PartpictureBox_MouseUp);
            // 
            // searchPartNobutton
            // 
            this.searchPartNobutton.BackColor = System.Drawing.Color.Transparent;
            this.searchPartNobutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchPartNobutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchPartNobutton.FlatAppearance.BorderSize = 0;
            this.searchPartNobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchPartNobutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchPartNobutton.Location = new System.Drawing.Point(373, 1);
            this.searchPartNobutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.searchPartNobutton.Name = "searchPartNobutton";
            this.searchPartNobutton.Size = new System.Drawing.Size(24, 24);
            this.searchPartNobutton.TabIndex = 15;
            this.searchPartNobutton.UseVisualStyleBackColor = false;
            this.searchPartNobutton.Visible = false;
            this.searchPartNobutton.Click += new System.EventHandler(this.searchPartNobutton_Click);
            // 
            // QuantitytextBox
            // 
            this.QuantitytextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.QuantitytextBox, 2);
            this.QuantitytextBox.Decimal = false;
            this.QuantitytextBox.Location = new System.Drawing.Point(66, 306);
            this.QuantitytextBox.Money = false;
            this.QuantitytextBox.Name = "QuantitytextBox";
            this.QuantitytextBox.Numeric = true;
            this.QuantitytextBox.Prefix = "";
            this.QuantitytextBox.Size = new System.Drawing.Size(100, 20);
            this.QuantitytextBox.TabIndex = 10;
            // 
            // PricetextBox
            // 
            this.PricetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.PricetextBox, 3);
            this.PricetextBox.Decimal = false;
            this.PricetextBox.Location = new System.Drawing.Point(66, 332);
            this.PricetextBox.Money = true;
            this.PricetextBox.Name = "PricetextBox";
            this.PricetextBox.Numeric = false;
            this.PricetextBox.Prefix = "";
            this.PricetextBox.Size = new System.Drawing.Size(142, 20);
            this.PricetextBox.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(214, 329);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Update";
            // 
            // textBoxBrand
            // 
            this.textBoxBrand.BackColor = System.Drawing.Color.White;
            this.textBoxBrand.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.textBoxBrand, 3);
            this.textBoxBrand.Enabled = false;
            this.textBoxBrand.Location = new System.Drawing.Point(172, 84);
            this.textBoxBrand.Name = "textBoxBrand";
            this.textBoxBrand.Size = new System.Drawing.Size(149, 20);
            this.textBoxBrand.TabIndex = 20;
            // 
            // textBoxUOM
            // 
            this.textBoxUOM.BackColor = System.Drawing.Color.White;
            this.textBoxUOM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.textBoxUOM, 3);
            this.textBoxUOM.Enabled = false;
            this.textBoxUOM.Location = new System.Drawing.Point(172, 278);
            this.textBoxUOM.Name = "textBoxUOM";
            this.textBoxUOM.Size = new System.Drawing.Size(149, 20);
            this.textBoxUOM.TabIndex = 21;
            // 
            // textBoxRAK
            // 
            this.textBoxRAK.BackColor = System.Drawing.Color.White;
            this.textBoxRAK.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.textBoxRAK, 3);
            this.textBoxRAK.Enabled = false;
            this.textBoxRAK.Location = new System.Drawing.Point(172, 358);
            this.textBoxRAK.Name = "textBoxRAK";
            this.textBoxRAK.Size = new System.Drawing.Size(195, 20);
            this.textBoxRAK.TabIndex = 14;
            // 
            // searchBrandbutton
            // 
            this.searchBrandbutton.BackColor = System.Drawing.Color.Transparent;
            this.searchBrandbutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchBrandbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchBrandbutton.FlatAppearance.BorderSize = 0;
            this.searchBrandbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchBrandbutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchBrandbutton.Location = new System.Drawing.Point(373, 82);
            this.searchBrandbutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.searchBrandbutton.Name = "searchBrandbutton";
            this.searchBrandbutton.Size = new System.Drawing.Size(24, 24);
            this.searchBrandbutton.TabIndex = 5;
            this.searchBrandbutton.UseVisualStyleBackColor = false;
            this.searchBrandbutton.Visible = false;
            // 
            // searchSatuanbutton
            // 
            this.searchSatuanbutton.BackColor = System.Drawing.Color.Transparent;
            this.searchSatuanbutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchSatuanbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchSatuanbutton.FlatAppearance.BorderSize = 0;
            this.searchSatuanbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchSatuanbutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchSatuanbutton.Location = new System.Drawing.Point(373, 276);
            this.searchSatuanbutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.searchSatuanbutton.Name = "searchSatuanbutton";
            this.searchSatuanbutton.Size = new System.Drawing.Size(24, 24);
            this.searchSatuanbutton.TabIndex = 9;
            this.searchSatuanbutton.UseVisualStyleBackColor = false;
            this.searchSatuanbutton.Visible = false;
            // 
            // searchRakbutton
            // 
            this.searchRakbutton.BackColor = System.Drawing.Color.Transparent;
            this.searchRakbutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchRakbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchRakbutton.FlatAppearance.BorderSize = 0;
            this.searchRakbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchRakbutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchRakbutton.Location = new System.Drawing.Point(373, 356);
            this.searchRakbutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.searchRakbutton.Name = "searchRakbutton";
            this.searchRakbutton.Size = new System.Drawing.Size(24, 24);
            this.searchRakbutton.TabIndex = 15;
            this.searchRakbutton.UseVisualStyleBackColor = false;
            this.searchRakbutton.Visible = false;
            // 
            // upDateTimePicker
            // 
            this.upDateTimePicker.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatatableLayoutPanel.SetColumnSpan(this.upDateTimePicker, 2);
            this.upDateTimePicker.CustomFormat = "dd/MM/yyyy";
            this.upDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.upDateTimePicker.Location = new System.Drawing.Point(262, 332);
            this.upDateTimePicker.Name = "upDateTimePicker";
            this.upDateTimePicker.Size = new System.Drawing.Size(137, 20);
            this.upDateTimePicker.TabIndex = 12;
            // 
            // MasterItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(616, 419);
            this.Controls.Add(this.DatatableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MasterItem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Item";
            this.Activated += new System.EventHandler(this.MasterItem_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MasterItem_FormClosed);
            this.DatatableLayoutPanel.ResumeLayout(false);
            this.DatatableLayoutPanel.PerformLayout();
            this.Spesifikasipanel.ResumeLayout(false);
            this.pictureboxpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PartpictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel DatatableLayoutPanel;
        private System.Windows.Forms.TextBox PartNotextBox;
        private System.Windows.Forms.TextBox PartNametextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel Spesifikasipanel;
        private System.Windows.Forms.RichTextBox SpesifikasirichTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox PartpictureBox;
        private System.Windows.Forms.Panel pictureboxpanel;
        private System.Windows.Forms.Button BrowseImagebutton;
        private System.Windows.Forms.ComboBox AliascomboBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox BrandcomboBox;
        private System.Windows.Forms.ComboBox SatuancomboBox;
        private System.Windows.Forms.ComboBox RakcomboBox;
        private System.Windows.Forms.Button searchSatuanbutton;
        private System.Windows.Forms.Button searchBrandbutton;
        private System.Windows.Forms.Button searchRakbutton;
        private System.Windows.Forms.Button searchPartNobutton;
        private CustomControls.TextBoxSL QuantitytextBox;
        private CustomControls.TextBoxSL PricetextBox;
        private System.Windows.Forms.TextBox textBoxUser;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxBrand;
        private System.Windows.Forms.TextBox textBoxUOM;
        private System.Windows.Forms.TextBox textBoxRAK;
        private System.Windows.Forms.DateTimePicker upDateTimePicker;
    }
}