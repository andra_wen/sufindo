﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Sufindo.Master
{
    public partial class MasterKurs : Form
    {
        List<Control> controlTextBox;

        Connection connection;
        MenuStrip menustripAction;
        Kursdatagridview kursdatagridview;

        List<String> KursID;

        String activity = "";
        Boolean isFind = false;

        public MasterKurs(){
            InitializeComponent();
        }

        public MasterKurs(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();

            kursdatagridview = null;

            controlTextBox = new List<Control>();
            this.menustripAction = menustripAction; 
            //DatatableLayoutPanel.Location = new Point((this.Size.Width - DatatableLayoutPanel.Size.Width) / 2, (this.Size.Height - DatatableLayoutPanel.Size.Height) / 2);

            controlTextBox = new List<Control>();

            connection = new Connection();

            foreach (Control control in this.DatatableLayoutPanel.Controls) {
                if (control is TextBox) {
                    controlTextBox.Add(control);
                }
                else if (control is Panel) {
                    foreach (Control controlPanel in control.Controls) {
                        if (controlPanel is RichTextBox) {
                            controlTextBox.Add(controlPanel);
                        }
                    }
                }
                else if (control is ComboBox) {
                    controlTextBox.Add(control);
                }
            }
            newMaster();
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("reloadAllData : " + ex.Message);
            }
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e){
            //MessageBox.Show("Completed");
        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e){
            if (KursID == null) KursID = new List<string>();
            KursID.Clear();
            String query = String.Format("SELECT KURSID FROM M_KURS WHERE [STATUS] = '{0}'",Status.ACTIVE);
            SqlDataReader sqldataReader = connection.sqlDataReaders(query);

            while (sqldataReader.Read()){
                KursID.Add(sqldataReader.GetString(0));
            }
        }

        public void save()
        {
            try
            {
                if (activity.Equals("UPDATE") || activity.Equals("INSERT"))
                {
                    if (isValidation())
                    {
                        List<SqlParameter> sqlParam = new List<SqlParameter>();

                        Double rate = RatetextBox.DecimalValue;

                        //RatetextBox.Text = RatetextBox.Text.Equals("") ? "0" :  RatetextBox.Text;

                        sqlParam.Add(new SqlParameter("@KURSID", KursIDtextBox.Text.ToUpper()));
                        sqlParam.Add(new SqlParameter("@KURSNAME", KursNametextBox.Text.ToUpper()));
                        sqlParam.Add(new SqlParameter("@RATE", rate.ToString()));
                        sqlParam.Add(new SqlParameter("@STATUS", Status.ACTIVE));
                        sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));
                        
                        String proc = activity.Equals("UPDATE") ? "UPDATE_DATA_KURS" : "INSERT_DATA_KURS";
                        if (connection.callProcedure(proc, sqlParam))
                        {
                            MessageBox.Show("Kurs Berhasil Disimpan");
                            newMaster();
                            reloadAllData();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("INSERT FAILED ON insertMasterItem() : " + ex);
            }
        }

        public void newMaster()
        {
            if (!(kursdatagridview != null && !kursdatagridview.IsDisposed))
            {
                Utilities.clearAllField(ref controlTextBox);
                searchKursIDbutton.Visible = false;
                foreach (var item in controlTextBox)
                {
                    if (item is RichTextBox)
                    {
                        RichTextBox richtextBox = (RichTextBox)item;
                        richtextBox.ReadOnly = false;
                    }
                    else
                    {
                        item.Enabled = true;
                    }
                    item.BackColor = SystemColors.Window;

                }
                KursIDtextBox.AutoCompleteCustomSource.Clear();
                KursIDtextBox.AutoCompleteMode = AutoCompleteMode.None;
                KursIDtextBox.AutoCompleteSource = AutoCompleteSource.None;
                isFind = false;
                activity = "INSERT";
            }
        }

        public void find()
        {
            if (!searchKursIDbutton.Visible || isFind)
            {
                searchKursIDbutton.Visible = true;
                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("KursIDtextBox"))
                    {
                        if (item is RichTextBox)
                        {
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = true;
                            richtextBox.BackColor = SystemColors.Info;
                            richtextBox.Text = "";
                        }
                        else if (item is ComboBox)
                        {
                            ComboBox combobox = (ComboBox)item;
                            combobox.SelectedIndex = -1;
                            item.Enabled = false;
                        }
                        else
                        {
                            item.Enabled = false;
                            item.Text = "";
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        currentTextbox.AutoCompleteCustomSource.AddRange(KursID.ToArray());
                        currentTextbox.Text = "";
                    }

                    item.BackColor = SystemColors.Info;
                }
                RatetextBox.BackColor = SystemColors.Info;
                isFind = true;
                activity = "";
                RatetextBox.Text = "";   
            }
        }

        public void edit()
        {
            if (!searchKursIDbutton.Visible || isFind)
            {
                searchKursIDbutton.Visible = true;
                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("KursIDtextBox"))
                    {
                        if (item is RichTextBox)
                        {
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = false;
                            richtextBox.BackColor = Color.White;
                        }
                        else
                        {
                            item.Enabled = true;
                            item.BackColor = Color.White;
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        currentTextbox.AutoCompleteCustomSource.AddRange(KursID.ToArray());
                        item.BackColor = SystemColors.Info;
                    }
                }
                activity = "UPDATE";
                isFind = true;
            }
        }

        public void delete()
        {
            
            if (isFind && isAvailableKursIDForUpdate())
            {
            
                List<SqlParameter> sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@KURSID", KursIDtextBox.Text));
                sqlParam.Add(new SqlParameter("@STATUS", Status.DEACTIVE));

                if (connection.callProcedure("DELETE_DATA_KURS", sqlParam)){
                    newMaster();
                    reloadAllData();
                }
            }
        }

        private Boolean isValidation()
        {
            if (Utilities.isEmptyString(KursIDtextBox.Text))
            {
                MessageBox.Show("Please Input Kurs ID");
            }
            else if (isAvailableKursID()){
                MessageBox.Show("Kurs ID has been Available");
            }
            else if (activity.Equals("UPDATE") && !isAvailableKursIDForUpdate()) {
                MessageBox.Show("KURS ID Has not Availabel for Update");
            }
            else if (Utilities.isEmptyString(KursNametextBox.Text))
            {
                MessageBox.Show("Please Input Kurs Name");
            }
            else
            {
                return true;
            }
            return false;
        }

        private Boolean isAvailableKursID()
        {
            KursIDtextBox.Text = Utilities.removeSpace(KursIDtextBox.Text);
            Boolean result = activity.Equals("UPDATE") ? false : Utilities.isAvailableID(KursID, KursIDtextBox.Text);

            return result;
        }

        private Boolean isAvailableKursIDForUpdate() {
            KursIDtextBox.Text = Utilities.removeSpace(KursIDtextBox.Text);
            Boolean result = Utilities.isAvailableID(KursID, KursIDtextBox.Text);
            return result;
        }

        private void MasterKurs_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }

        private void MasterKursPassingData(DataTable sender)
        {
            sender.Rows[0]["KURSID"].ToString();
            KursIDtextBox.Text = sender.Rows[0]["KURSID"].ToString();
            KursNametextBox.Text = sender.Rows[0]["KURSNAME"].ToString();
            RatetextBox.Text = sender.Rows[0]["RATE"].ToString();
        }

        private void KursIDtextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (searchKursIDbutton.Visible)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    Boolean isExistingPartNo = false;
                    foreach (String item in KursID)
                    {
                        if (item.ToUpper().Equals(KursIDtextBox.Text.ToUpper()))
                        {
                            isExistingPartNo = true;
                            KursIDtextBox.Text = KursIDtextBox.Text.ToUpper();
                            String query = String.Format("SELECT KURSID, KURSNAME, RATE " +
                                                         "FROM M_KURS " +
                                                         "WHERE KURSID = '{0}'", KursIDtextBox.Text);

                            DataTable datatable = connection.openDataTableQuery(query);
                            MasterKursPassingData(datatable);
                            break;
                        }
                    }

                    if (!isExistingPartNo)
                    {
                        KursIDtextBox.Text = "";
                        Utilities.clearAllField(ref controlTextBox);
                    }
                }
            }
            else {
                if (e.KeyCode == Keys.Enter)
                {
                    Boolean isExistingPartNo = false;
                    foreach (String item in KursID)
                    {
                        if (item.ToUpper().Equals(KursIDtextBox.Text.ToUpper()))
                        {
                            isExistingPartNo = true;
                            break;
                        }
                    }
                    if (isExistingPartNo)
                    {
                        MessageBox.Show("Currency Already Exist");
                        KursIDtextBox.Text = "";
                        //Utilities.clearAllField(ref controlTextBox);
                    }
                }
            }
        }

        private void KursIDtextBox_Leave(object sender, EventArgs e)
        {
            KursIDtextBox.Text = KursIDtextBox.Text.ToUpper();
            if (isFind)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in KursID)
                {
                    if (item.ToUpper().Equals(KursIDtextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        KursIDtextBox.Text = KursIDtextBox.Text.ToUpper();
                        String query = String.Format("SELECT KURSID, KURSNAME, RATE " +
                                                     "FROM M_KURS " +
                                                     "WHERE KURSID = '{0}'", KursIDtextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        MasterKursPassingData(datatable);
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    KursIDtextBox.Text = "";
                    Utilities.clearAllField(ref controlTextBox);
                }
            }
            else {
                Boolean isExistingPartNo = false;
                foreach (String item in KursID)
                {
                    if (item.ToUpper().Equals(KursIDtextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        break;
                    }
                }
                if (isExistingPartNo)
                {
                    MessageBox.Show("Currency Already Exist");
                    KursIDtextBox.Text = "";
                    //Utilities.clearAllField(ref controlTextBox);
                }
            }
            
        }

        private void searchKursIDbutton_Click(object sender, EventArgs e)
        {
            if (kursdatagridview == null)
            {
                kursdatagridview = new Kursdatagridview("MasterKurs");
                kursdatagridview.masterKursPassingData = new Kursdatagridview.MasterKursPassingData(MasterKursPassingData);
                //kursdatagridview.MdiParent = this.MdiParent;
                kursdatagridview.ShowDialog();
            }
            else if (kursdatagridview.IsDisposed)
            {
                kursdatagridview = new Kursdatagridview("MasterKurs");
                kursdatagridview.masterKursPassingData = new Kursdatagridview.MasterKursPassingData(MasterKursPassingData);
                //kursdatagridview.MdiParent = this.MdiParent;
                kursdatagridview.ShowDialog();
            }
        }

        private void KursNametextBox_Leave(object sender, EventArgs e)
        {
            KursNametextBox.Text = KursNametextBox.Text.ToUpper();
        }

        //private void RatetextBox_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    e.Handled = Utilities.onlyDecimalTextBox(sender, e);
        //}

        private void MasterKurs_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            Main.ACTIVEFORM = this;
            reloadAllData();
        }

    }
}
