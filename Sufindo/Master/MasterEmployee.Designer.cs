﻿namespace Sufindo.Master
{
    partial class MasterEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DatatableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.EmployeeNotextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.searchEmployeeNobutton = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.PasswordtextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.JoindateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.EmailtextBox = new System.Windows.Forms.TextBox();
            this.HandphonetextBox = new System.Windows.Forms.TextBox();
            this.BODdateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.Spesifikasipanel = new System.Windows.Forms.Panel();
            this.AddressrichTextBox = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.PositiontextBox = new System.Windows.Forms.TextBox();
            this.EmployeeNametextBox = new System.Windows.Forms.TextBox();
            this.ConfirmPasswordtextBox = new System.Windows.Forms.TextBox();
            this.DatatableLayoutPanel.SuspendLayout();
            this.Spesifikasipanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // DatatableLayoutPanel
            // 
            this.DatatableLayoutPanel.ColumnCount = 8;
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.Controls.Add(this.EmployeeNotextBox, 1, 0);
            this.DatatableLayoutPanel.Controls.Add(this.label2, 0, 0);
            this.DatatableLayoutPanel.Controls.Add(this.searchEmployeeNobutton, 4, 0);
            this.DatatableLayoutPanel.Controls.Add(this.label10, 0, 1);
            this.DatatableLayoutPanel.Controls.Add(this.PasswordtextBox, 1, 1);
            this.DatatableLayoutPanel.Controls.Add(this.label6, 0, 9);
            this.DatatableLayoutPanel.Controls.Add(this.label8, 0, 8);
            this.DatatableLayoutPanel.Controls.Add(this.label9, 0, 7);
            this.DatatableLayoutPanel.Controls.Add(this.label1, 0, 6);
            this.DatatableLayoutPanel.Controls.Add(this.label4, 0, 4);
            this.DatatableLayoutPanel.Controls.Add(this.label3, 0, 3);
            this.DatatableLayoutPanel.Controls.Add(this.JoindateTimePicker, 1, 9);
            this.DatatableLayoutPanel.Controls.Add(this.label7, 0, 2);
            this.DatatableLayoutPanel.Controls.Add(this.EmailtextBox, 1, 8);
            this.DatatableLayoutPanel.Controls.Add(this.HandphonetextBox, 1, 7);
            this.DatatableLayoutPanel.Controls.Add(this.BODdateTimePicker, 1, 6);
            this.DatatableLayoutPanel.Controls.Add(this.Spesifikasipanel, 1, 5);
            this.DatatableLayoutPanel.Controls.Add(this.label5, 0, 5);
            this.DatatableLayoutPanel.Controls.Add(this.PositiontextBox, 1, 4);
            this.DatatableLayoutPanel.Controls.Add(this.EmployeeNametextBox, 1, 3);
            this.DatatableLayoutPanel.Controls.Add(this.ConfirmPasswordtextBox, 1, 2);
            this.DatatableLayoutPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.DatatableLayoutPanel.Location = new System.Drawing.Point(3, 2);
            this.DatatableLayoutPanel.Name = "DatatableLayoutPanel";
            this.DatatableLayoutPanel.RowCount = 10;
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.Size = new System.Drawing.Size(520, 390);
            this.DatatableLayoutPanel.TabIndex = 1;
            // 
            // EmployeeNotextBox
            // 
            this.EmployeeNotextBox.BackColor = System.Drawing.SystemColors.Window;
            this.EmployeeNotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.EmployeeNotextBox, 3);
            this.EmployeeNotextBox.Location = new System.Drawing.Point(100, 3);
            this.EmployeeNotextBox.Name = "EmployeeNotextBox";
            this.EmployeeNotextBox.Size = new System.Drawing.Size(204, 20);
            this.EmployeeNotextBox.TabIndex = 1;
            this.EmployeeNotextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EmployeeNotextBox_KeyDown);
            this.EmployeeNotextBox.Leave += new System.EventHandler(this.EmployeeNotextBox_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Employee ID";
            // 
            // searchEmployeeNobutton
            // 
            this.searchEmployeeNobutton.BackColor = System.Drawing.Color.Transparent;
            this.searchEmployeeNobutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchEmployeeNobutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchEmployeeNobutton.FlatAppearance.BorderSize = 0;
            this.searchEmployeeNobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchEmployeeNobutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchEmployeeNobutton.Location = new System.Drawing.Point(310, 1);
            this.searchEmployeeNobutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.searchEmployeeNobutton.Name = "searchEmployeeNobutton";
            this.searchEmployeeNobutton.Size = new System.Drawing.Size(24, 24);
            this.searchEmployeeNobutton.TabIndex = 2;
            this.searchEmployeeNobutton.UseVisualStyleBackColor = false;
            this.searchEmployeeNobutton.Visible = false;
            this.searchEmployeeNobutton.Click += new System.EventHandler(this.searchEmployeeNobutton_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 28);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "Password";
            // 
            // PasswordtextBox
            // 
            this.PasswordtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.PasswordtextBox, 3);
            this.PasswordtextBox.Location = new System.Drawing.Point(100, 31);
            this.PasswordtextBox.Name = "PasswordtextBox";
            this.PasswordtextBox.PasswordChar = '*';
            this.PasswordtextBox.Size = new System.Drawing.Size(204, 20);
            this.PasswordtextBox.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 344);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 30;
            this.label6.Text = "Join Date";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 318);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Email";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 292);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Handphone";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 266);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "Date Of Birth";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 106);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 28;
            this.label4.Text = "Position";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Employee Name";
            // 
            // JoindateTimePicker
            // 
            this.DatatableLayoutPanel.SetColumnSpan(this.JoindateTimePicker, 4);
            this.JoindateTimePicker.CustomFormat = "dd/MM/yyyy";
            this.JoindateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.JoindateTimePicker.Location = new System.Drawing.Point(100, 347);
            this.JoindateTimePicker.Name = "JoindateTimePicker";
            this.JoindateTimePicker.Size = new System.Drawing.Size(143, 20);
            this.JoindateTimePicker.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 54);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 13);
            this.label7.TabIndex = 32;
            this.label7.Text = "Confirm Password";
            // 
            // EmailtextBox
            // 
            this.EmailtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.EmailtextBox, 4);
            this.EmailtextBox.Location = new System.Drawing.Point(100, 321);
            this.EmailtextBox.Name = "EmailtextBox";
            this.EmailtextBox.Size = new System.Drawing.Size(204, 20);
            this.EmailtextBox.TabIndex = 10;
            // 
            // HandphonetextBox
            // 
            this.HandphonetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.HandphonetextBox, 4);
            this.HandphonetextBox.Location = new System.Drawing.Point(100, 295);
            this.HandphonetextBox.Name = "HandphonetextBox";
            this.HandphonetextBox.Size = new System.Drawing.Size(204, 20);
            this.HandphonetextBox.TabIndex = 9;
            this.HandphonetextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HandphonetextBox_KeyPress);
            // 
            // BODdateTimePicker
            // 
            this.DatatableLayoutPanel.SetColumnSpan(this.BODdateTimePicker, 4);
            this.BODdateTimePicker.CustomFormat = "dd/MM/yyyy";
            this.BODdateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.BODdateTimePicker.Location = new System.Drawing.Point(100, 269);
            this.BODdateTimePicker.Name = "BODdateTimePicker";
            this.BODdateTimePicker.Size = new System.Drawing.Size(143, 20);
            this.BODdateTimePicker.TabIndex = 8;
            // 
            // Spesifikasipanel
            // 
            this.Spesifikasipanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.Spesifikasipanel, 7);
            this.Spesifikasipanel.Controls.Add(this.AddressrichTextBox);
            this.Spesifikasipanel.Location = new System.Drawing.Point(100, 135);
            this.Spesifikasipanel.Name = "Spesifikasipanel";
            this.Spesifikasipanel.Size = new System.Drawing.Size(406, 128);
            this.Spesifikasipanel.TabIndex = 7;
            // 
            // AddressrichTextBox
            // 
            this.AddressrichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.AddressrichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddressrichTextBox.Location = new System.Drawing.Point(0, 0);
            this.AddressrichTextBox.Name = "AddressrichTextBox";
            this.AddressrichTextBox.Size = new System.Drawing.Size(404, 126);
            this.AddressrichTextBox.TabIndex = 7;
            this.AddressrichTextBox.Text = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 132);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Address";
            // 
            // PositiontextBox
            // 
            this.PositiontextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.PositiontextBox, 4);
            this.PositiontextBox.Location = new System.Drawing.Point(100, 109);
            this.PositiontextBox.Name = "PositiontextBox";
            this.PositiontextBox.Size = new System.Drawing.Size(204, 20);
            this.PositiontextBox.TabIndex = 6;
            // 
            // EmployeeNametextBox
            // 
            this.EmployeeNametextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.EmployeeNametextBox, 4);
            this.EmployeeNametextBox.Location = new System.Drawing.Point(100, 83);
            this.EmployeeNametextBox.Name = "EmployeeNametextBox";
            this.EmployeeNametextBox.Size = new System.Drawing.Size(204, 20);
            this.EmployeeNametextBox.TabIndex = 5;
            this.EmployeeNametextBox.Leave += new System.EventHandler(this.EmployeeNametextBox_Leave);
            // 
            // ConfirmPasswordtextBox
            // 
            this.ConfirmPasswordtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.ConfirmPasswordtextBox, 4);
            this.ConfirmPasswordtextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.ConfirmPasswordtextBox.Location = new System.Drawing.Point(100, 57);
            this.ConfirmPasswordtextBox.Name = "ConfirmPasswordtextBox";
            this.ConfirmPasswordtextBox.PasswordChar = '*';
            this.ConfirmPasswordtextBox.Size = new System.Drawing.Size(204, 20);
            this.ConfirmPasswordtextBox.TabIndex = 4;
            // 
            // MasterEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(526, 398);
            this.Controls.Add(this.DatatableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MasterEmployee";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Employee";
            this.Activated += new System.EventHandler(this.MasterCustomer_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MasterCustomer_FormClosed);
            this.DatatableLayoutPanel.ResumeLayout(false);
            this.DatatableLayoutPanel.PerformLayout();
            this.Spesifikasipanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel DatatableLayoutPanel;
        private System.Windows.Forms.TextBox PasswordtextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button searchEmployeeNobutton;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox EmailtextBox;
        private System.Windows.Forms.TextBox HandphonetextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel Spesifikasipanel;
        private System.Windows.Forms.RichTextBox AddressrichTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox EmployeeNametextBox;
        private System.Windows.Forms.TextBox PositiontextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker BODdateTimePicker;
        private System.Windows.Forms.DateTimePicker JoindateTimePicker;
        private System.Windows.Forms.TextBox ConfirmPasswordtextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox EmployeeNotextBox;
    }
}