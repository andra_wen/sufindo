﻿namespace Sufindo.Master
{
    partial class MasterCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DatatableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.Spesifikasipanel = new System.Windows.Forms.Panel();
            this.AddressrichTextBox = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.CustomerNotextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CustomerNametextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.NoFaxtextBox = new System.Windows.Forms.TextBox();
            this.ZonetextBox = new System.Windows.Forms.TextBox();
            this.NoTelptextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.ContactPersontextBox = new System.Windows.Forms.TextBox();
            this.HandphonetextBox = new System.Windows.Forms.TextBox();
            this.EmailtextBox = new System.Windows.Forms.TextBox();
            this.SalestextBox = new System.Windows.Forms.TextBox();
            this.searchCustomerNobutton = new System.Windows.Forms.Button();
            this.DatatableLayoutPanel.SuspendLayout();
            this.Spesifikasipanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // DatatableLayoutPanel
            // 
            this.DatatableLayoutPanel.ColumnCount = 8;
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.Controls.Add(this.label10, 0, 12);
            this.DatatableLayoutPanel.Controls.Add(this.Spesifikasipanel, 1, 3);
            this.DatatableLayoutPanel.Controls.Add(this.label5, 0, 3);
            this.DatatableLayoutPanel.Controls.Add(this.CustomerNotextBox, 1, 0);
            this.DatatableLayoutPanel.Controls.Add(this.label3, 0, 1);
            this.DatatableLayoutPanel.Controls.Add(this.CustomerNametextBox, 1, 1);
            this.DatatableLayoutPanel.Controls.Add(this.label2, 0, 0);
            this.DatatableLayoutPanel.Controls.Add(this.label6, 0, 6);
            this.DatatableLayoutPanel.Controls.Add(this.label7, 0, 5);
            this.DatatableLayoutPanel.Controls.Add(this.label1, 0, 4);
            this.DatatableLayoutPanel.Controls.Add(this.NoFaxtextBox, 1, 6);
            this.DatatableLayoutPanel.Controls.Add(this.ZonetextBox, 1, 4);
            this.DatatableLayoutPanel.Controls.Add(this.NoTelptextBox, 1, 5);
            this.DatatableLayoutPanel.Controls.Add(this.label4, 0, 9);
            this.DatatableLayoutPanel.Controls.Add(this.label8, 0, 11);
            this.DatatableLayoutPanel.Controls.Add(this.label9, 0, 10);
            this.DatatableLayoutPanel.Controls.Add(this.ContactPersontextBox, 1, 9);
            this.DatatableLayoutPanel.Controls.Add(this.HandphonetextBox, 1, 10);
            this.DatatableLayoutPanel.Controls.Add(this.EmailtextBox, 1, 11);
            this.DatatableLayoutPanel.Controls.Add(this.SalestextBox, 1, 12);
            this.DatatableLayoutPanel.Controls.Add(this.searchCustomerNobutton, 4, 0);
            this.DatatableLayoutPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.75F);
            this.DatatableLayoutPanel.Location = new System.Drawing.Point(2, 2);
            this.DatatableLayoutPanel.Name = "DatatableLayoutPanel";
            this.DatatableLayoutPanel.RowCount = 13;
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.Size = new System.Drawing.Size(528, 396);
            this.DatatableLayoutPanel.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 349);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 15);
            this.label10.TabIndex = 24;
            this.label10.Text = "Sales";
            // 
            // Spesifikasipanel
            // 
            this.Spesifikasipanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.Spesifikasipanel, 7);
            this.Spesifikasipanel.Controls.Add(this.AddressrichTextBox);
            this.Spesifikasipanel.Location = new System.Drawing.Point(106, 58);
            this.Spesifikasipanel.Name = "Spesifikasipanel";
            this.Spesifikasipanel.Size = new System.Drawing.Size(404, 126);
            this.Spesifikasipanel.TabIndex = 4;
            // 
            // AddressrichTextBox
            // 
            this.AddressrichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.AddressrichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddressrichTextBox.Location = new System.Drawing.Point(0, 0);
            this.AddressrichTextBox.Name = "AddressrichTextBox";
            this.AddressrichTextBox.Size = new System.Drawing.Size(402, 124);
            this.AddressrichTextBox.TabIndex = 4;
            this.AddressrichTextBox.Text = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "Address";
            // 
            // CustomerNotextBox
            // 
            this.CustomerNotextBox.BackColor = System.Drawing.SystemColors.Window;
            this.CustomerNotextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.CustomerNotextBox, 3);
            this.CustomerNotextBox.Location = new System.Drawing.Point(106, 3);
            this.CustomerNotextBox.Name = "CustomerNotextBox";
            this.CustomerNotextBox.Size = new System.Drawing.Size(204, 21);
            this.CustomerNotextBox.TabIndex = 1;
            this.CustomerNotextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CustomerNotextBox_KeyDown);
            this.CustomerNotextBox.Leave += new System.EventHandler(this.CustomerNotextBox_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Customer Name";
            // 
            // CustomerNametextBox
            // 
            this.CustomerNametextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.CustomerNametextBox, 4);
            this.CustomerNametextBox.Location = new System.Drawing.Point(106, 31);
            this.CustomerNametextBox.Name = "CustomerNametextBox";
            this.CustomerNametextBox.Size = new System.Drawing.Size(255, 21);
            this.CustomerNametextBox.TabIndex = 3;
            this.CustomerNametextBox.Leave += new System.EventHandler(this.CustomerNametextBox_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Customer ID";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 241);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 15);
            this.label6.TabIndex = 10;
            this.label6.Text = "No Fax";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 214);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 15);
            this.label7.TabIndex = 11;
            this.label7.Text = "No Telp";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 187);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 15);
            this.label1.TabIndex = 17;
            this.label1.Text = "Zone";
            // 
            // NoFaxtextBox
            // 
            this.NoFaxtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.NoFaxtextBox, 4);
            this.NoFaxtextBox.Location = new System.Drawing.Point(106, 244);
            this.NoFaxtextBox.Name = "NoFaxtextBox";
            this.NoFaxtextBox.Size = new System.Drawing.Size(204, 21);
            this.NoFaxtextBox.TabIndex = 7;
            this.NoFaxtextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NoFaxtextBox_KeyPress);
            // 
            // ZonetextBox
            // 
            this.ZonetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.ZonetextBox, 4);
            this.ZonetextBox.Location = new System.Drawing.Point(106, 190);
            this.ZonetextBox.Name = "ZonetextBox";
            this.ZonetextBox.Size = new System.Drawing.Size(204, 21);
            this.ZonetextBox.TabIndex = 5;
            // 
            // NoTelptextBox
            // 
            this.NoTelptextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.NoTelptextBox, 4);
            this.NoTelptextBox.Location = new System.Drawing.Point(106, 217);
            this.NoTelptextBox.Name = "NoTelptextBox";
            this.NoTelptextBox.Size = new System.Drawing.Size(204, 21);
            this.NoTelptextBox.TabIndex = 6;
            this.NoTelptextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NoTelptextBox_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 268);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 15);
            this.label4.TabIndex = 18;
            this.label4.Text = "Contact Person";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 322);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 15);
            this.label8.TabIndex = 19;
            this.label8.Text = "Email";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 295);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 15);
            this.label9.TabIndex = 20;
            this.label9.Text = "Handphone";
            // 
            // ContactPersontextBox
            // 
            this.ContactPersontextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.ContactPersontextBox, 4);
            this.ContactPersontextBox.Location = new System.Drawing.Point(106, 271);
            this.ContactPersontextBox.Name = "ContactPersontextBox";
            this.ContactPersontextBox.Size = new System.Drawing.Size(204, 21);
            this.ContactPersontextBox.TabIndex = 21;
            // 
            // HandphonetextBox
            // 
            this.HandphonetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.HandphonetextBox, 4);
            this.HandphonetextBox.Location = new System.Drawing.Point(106, 298);
            this.HandphonetextBox.Name = "HandphonetextBox";
            this.HandphonetextBox.Size = new System.Drawing.Size(204, 21);
            this.HandphonetextBox.TabIndex = 22;
            this.HandphonetextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HandphonetextBox_KeyPress);
            // 
            // EmailtextBox
            // 
            this.EmailtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.EmailtextBox, 4);
            this.EmailtextBox.Location = new System.Drawing.Point(106, 325);
            this.EmailtextBox.Name = "EmailtextBox";
            this.EmailtextBox.Size = new System.Drawing.Size(204, 21);
            this.EmailtextBox.TabIndex = 23;
            // 
            // SalestextBox
            // 
            this.SalestextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.SalestextBox, 4);
            this.SalestextBox.Location = new System.Drawing.Point(106, 352);
            this.SalestextBox.Name = "SalestextBox";
            this.SalestextBox.Size = new System.Drawing.Size(204, 21);
            this.SalestextBox.TabIndex = 25;
            // 
            // searchCustomerNobutton
            // 
            this.searchCustomerNobutton.BackColor = System.Drawing.Color.Transparent;
            this.searchCustomerNobutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchCustomerNobutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchCustomerNobutton.FlatAppearance.BorderSize = 0;
            this.searchCustomerNobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchCustomerNobutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchCustomerNobutton.Location = new System.Drawing.Point(316, 1);
            this.searchCustomerNobutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.searchCustomerNobutton.Name = "searchCustomerNobutton";
            this.searchCustomerNobutton.Size = new System.Drawing.Size(24, 24);
            this.searchCustomerNobutton.TabIndex = 26;
            this.searchCustomerNobutton.UseVisualStyleBackColor = false;
            this.searchCustomerNobutton.Visible = false;
            this.searchCustomerNobutton.Click += new System.EventHandler(this.searchCustomerNobutton_Click);
            // 
            // MasterCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 404);
            this.Controls.Add(this.DatatableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MasterCustomer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Customer";
            this.Activated += new System.EventHandler(this.MasterCustomer_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MasterCustomer_FormClosed);
            this.DatatableLayoutPanel.ResumeLayout(false);
            this.DatatableLayoutPanel.PerformLayout();
            this.Spesifikasipanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel DatatableLayoutPanel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox CustomerNametextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox NoFaxtextBox;
        private System.Windows.Forms.TextBox NoTelptextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel Spesifikasipanel;
        private System.Windows.Forms.RichTextBox AddressrichTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ZonetextBox;
        private System.Windows.Forms.TextBox CustomerNotextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox ContactPersontextBox;
        private System.Windows.Forms.TextBox HandphonetextBox;
        private System.Windows.Forms.TextBox EmailtextBox;
        private System.Windows.Forms.TextBox SalestextBox;
        private System.Windows.Forms.Button searchCustomerNobutton;
    }
}