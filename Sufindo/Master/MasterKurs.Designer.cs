﻿namespace Sufindo.Master
{
    partial class MasterKurs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DatatableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.RatetextBox = new CustomControls.TextBoxSL();
            this.label5 = new System.Windows.Forms.Label();
            this.KursIDtextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.KursNametextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.searchKursIDbutton = new System.Windows.Forms.Button();
            this.DatatableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // DatatableLayoutPanel
            // 
            this.DatatableLayoutPanel.ColumnCount = 5;
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.Controls.Add(this.RatetextBox, 1, 2);
            this.DatatableLayoutPanel.Controls.Add(this.label5, 0, 2);
            this.DatatableLayoutPanel.Controls.Add(this.KursIDtextBox, 1, 0);
            this.DatatableLayoutPanel.Controls.Add(this.label3, 0, 1);
            this.DatatableLayoutPanel.Controls.Add(this.KursNametextBox, 1, 1);
            this.DatatableLayoutPanel.Controls.Add(this.label2, 0, 0);
            this.DatatableLayoutPanel.Controls.Add(this.searchKursIDbutton, 4, 0);
            this.DatatableLayoutPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.75F);
            this.DatatableLayoutPanel.Location = new System.Drawing.Point(3, 5);
            this.DatatableLayoutPanel.Margin = new System.Windows.Forms.Padding(1);
            this.DatatableLayoutPanel.Name = "DatatableLayoutPanel";
            this.DatatableLayoutPanel.RowCount = 3;
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.Size = new System.Drawing.Size(280, 87);
            this.DatatableLayoutPanel.TabIndex = 1;
            // 
            // RatetextBox
            // 
            this.RatetextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.RatetextBox, 3);
            this.RatetextBox.Decimal = false;
            this.RatetextBox.Location = new System.Drawing.Point(101, 58);
            this.RatetextBox.Money = true;
            this.RatetextBox.Name = "RatetextBox";
            this.RatetextBox.Numeric = false;
            this.RatetextBox.Prefix = "";
            this.RatetextBox.Size = new System.Drawing.Size(110, 21);
            this.RatetextBox.TabIndex = 2;
            this.RatetextBox.Text = "0.00";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "Rate";
            // 
            // KursIDtextBox
            // 
            this.KursIDtextBox.BackColor = System.Drawing.SystemColors.Window;
            this.KursIDtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.KursIDtextBox, 3);
            this.KursIDtextBox.Location = new System.Drawing.Point(101, 3);
            this.KursIDtextBox.Name = "KursIDtextBox";
            this.KursIDtextBox.Size = new System.Drawing.Size(110, 21);
            this.KursIDtextBox.TabIndex = 1;
            this.KursIDtextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KursIDtextBox_KeyDown);
            this.KursIDtextBox.Leave += new System.EventHandler(this.KursIDtextBox_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Currency Name";
            // 
            // KursNametextBox
            // 
            this.KursNametextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.KursNametextBox, 4);
            this.KursNametextBox.Location = new System.Drawing.Point(101, 31);
            this.KursNametextBox.Name = "KursNametextBox";
            this.KursNametextBox.Size = new System.Drawing.Size(174, 21);
            this.KursNametextBox.TabIndex = 3;
            this.KursNametextBox.Leave += new System.EventHandler(this.KursNametextBox_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Currency ID";
            // 
            // searchKursIDbutton
            // 
            this.searchKursIDbutton.BackColor = System.Drawing.Color.Transparent;
            this.searchKursIDbutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchKursIDbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchKursIDbutton.FlatAppearance.BorderSize = 0;
            this.searchKursIDbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchKursIDbutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchKursIDbutton.Location = new System.Drawing.Point(217, 1);
            this.searchKursIDbutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.searchKursIDbutton.Name = "searchKursIDbutton";
            this.searchKursIDbutton.Size = new System.Drawing.Size(24, 24);
            this.searchKursIDbutton.TabIndex = 2;
            this.searchKursIDbutton.UseVisualStyleBackColor = false;
            this.searchKursIDbutton.Visible = false;
            this.searchKursIDbutton.Click += new System.EventHandler(this.searchKursIDbutton_Click);
            // 
            // MasterKurs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(287, 94);
            this.Controls.Add(this.DatatableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MasterKurs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Currency";
            this.Activated += new System.EventHandler(this.MasterKurs_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MasterKurs_FormClosed);
            this.DatatableLayoutPanel.ResumeLayout(false);
            this.DatatableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel DatatableLayoutPanel;
        private System.Windows.Forms.TextBox KursIDtextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox KursNametextBox;
        private System.Windows.Forms.Button searchKursIDbutton;
        private System.Windows.Forms.Label label5;
        private CustomControls.TextBoxSL RatetextBox;
    }
}