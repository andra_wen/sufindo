﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Sufindo.Master
{
    public partial class MasterCustomer : Form
    {
        List<Control> controlTextBox;

        Connection connection;
        MenuStrip menustripAction;
        Customerdatagridview customerdatagridview;

        List<String> CustomerID;

        DataTable datatable = null;

        String activity = "";
        Boolean isFind = false;

        public MasterCustomer(){
            InitializeComponent();
        }

        public MasterCustomer(ref Main mainForm, ref MenuStrip menustripAction)
        {
            InitializeComponent();
            
            customerdatagridview = null;

            controlTextBox = new List<Control>();
            this.menustripAction = menustripAction; 
            //DatatableLayoutPanel.Location = new Point((this.Size.Width - DatatableLayoutPanel.Size.Width) / 2, (this.Size.Height - DatatableLayoutPanel.Size.Height) / 2);

            controlTextBox = new List<Control>();

            connection = new Connection();

            foreach (Control control in this.DatatableLayoutPanel.Controls) {
                if (control is TextBox) {
                    controlTextBox.Add(control);
                }
                else if (control is Panel) {
                    foreach (Control controlPanel in control.Controls) {
                        if (controlPanel is RichTextBox) {
                            controlTextBox.Add(controlPanel);
                        }
                    }
                }
                else if (control is ComboBox) {
                    controlTextBox.Add(control);
                }
            }
            newMasterCustomer();
        }

        private void reloadAllData()
        {
            try
            {
                BackgroundWorker bgWorker = new BackgroundWorker();
                bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("MasterItem reloadAllData : " + ex.Message);
            }
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e){
            //MessageBox.Show("Completed");
        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e){
            if (CustomerID == null) CustomerID = new List<string>();
            CustomerID.Clear();
            String query = String.Format("SELECT CUSTOMERID FROM M_CUSTOMER WHERE [STATUS] = '{0}'", Status.ACTIVE);
            SqlDataReader sqldataReader = connection.sqlDataReaders(query);

            while (sqldataReader.Read())
            {
                CustomerID.Add(sqldataReader.GetString(0));
            }
        }

        public void save()
        {
            try
            {
                
                if (activity.Equals("UPDATE") || activity.Equals("INSERT"))
                {
                    if (isValidation())
                    {
                        List<SqlParameter> sqlParam = new List<SqlParameter>();

                        sqlParam.Add(new SqlParameter("@CUSTOMERID", CustomerNotextBox.Text));
                        sqlParam.Add(new SqlParameter("@CUSTOMERNAME", CustomerNametextBox.Text));
                        sqlParam.Add(new SqlParameter("@ADDRESS", AddressrichTextBox.Text));
                        sqlParam.Add(new SqlParameter("@ZONE", ZonetextBox.Text));
                        sqlParam.Add(new SqlParameter("@NOTELP", NoTelptextBox.Text));
                        sqlParam.Add(new SqlParameter("@FAX", NoFaxtextBox.Text));
                        sqlParam.Add(new SqlParameter("@EMAIL", EmailtextBox.Text));
                        sqlParam.Add(new SqlParameter("@CONTACTPERSON", ContactPersontextBox.Text));
                        sqlParam.Add(new SqlParameter("@NOHP", HandphonetextBox.Text));
                        sqlParam.Add(new SqlParameter("@SALESID", SalestextBox.Text));
                        sqlParam.Add(new SqlParameter("@STATUS", Status.ACTIVE));
                        sqlParam.Add(new SqlParameter("@CREATEDBY", AuthorizeUser.sharedInstance.userdata["EMPLOYEEID"].ToString()));

                        String proc = activity.Equals("UPDATE") ? "UPDATE_DATA_CUSTOMER" : "INSERT_DATA_CUSTOMER";
                        if (connection.callProcedure(proc, sqlParam))
                        {
                            MessageBox.Show("Master Customer Berhasil diSimpan");
                            newMasterCustomer();
                            reloadAllData();
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("INSERT FAILED ON newMasterCustomer() : " + ex);
            }
        }

        public void newMasterCustomer()
        {
            if (!(customerdatagridview != null && !customerdatagridview.IsDisposed))
            {
                Utilities.clearAllField(ref controlTextBox);
                searchCustomerNobutton.Visible = false;
                foreach (var item in controlTextBox)
                {
                    if (item is RichTextBox)
                    {
                        RichTextBox richtextBox = (RichTextBox)item;
                        richtextBox.ReadOnly = false;
                    }
                    else
                    {
                        item.Enabled = true;
                    }
                    item.BackColor = SystemColors.Window;

                }
                CustomerNotextBox.AutoCompleteCustomSource.Clear();
                CustomerNotextBox.AutoCompleteMode = AutoCompleteMode.None;
                CustomerNotextBox.AutoCompleteSource = AutoCompleteSource.None;
                isFind = false;
                activity = "INSERT";
            }
        }

        public void findMasterCustomer()
        {
            if (!searchCustomerNobutton.Visible || isFind)
            {
                searchCustomerNobutton.Visible = true;
                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("CustomerNotextBox"))
                    {
                        if (item is RichTextBox)
                        {
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = true;
                            richtextBox.BackColor = SystemColors.Info;
                            richtextBox.Text = "";
                        }
                        else if (item is ComboBox)
                        {
                            ComboBox combobox = (ComboBox)item;
                            combobox.SelectedIndex = -1;
                            item.Enabled = false;
                        }
                        else
                        {
                            item.Enabled = false;
                            item.Text = "";
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        currentTextbox.AutoCompleteCustomSource.AddRange(CustomerID.ToArray());
                        currentTextbox.Text = "";
                    }

                    item.BackColor = SystemColors.Info;
                }
                AddressrichTextBox.BackColor = SystemColors.Info;
                isFind = true;
                
                activity = "";
            }
        }

        public void editMasterCustomer()
        {
            if (!searchCustomerNobutton.Visible || isFind)
            {
                searchCustomerNobutton.Visible = true;
                foreach (Control item in controlTextBox)
                {
                    if (!item.Name.Equals("CustomerNotextBox"))
                    {
                        if (item is RichTextBox)
                        {
                            RichTextBox richtextBox = (RichTextBox)item;
                            richtextBox.ReadOnly = false;
                            richtextBox.BackColor = Color.White;
                        }
                        else
                        {
                            item.Enabled = true;
                            item.BackColor = Color.White;
                        }
                    }
                    else
                    {
                        TextBox currentTextbox = (TextBox)item;
                        currentTextbox.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        currentTextbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        currentTextbox.AutoCompleteCustomSource.AddRange(CustomerID.ToArray());
                        item.BackColor = SystemColors.Info;
                    }
                }
                activity = "UPDATE";
                isFind = true;
            }
        }

        public void deleteMasterCustomer()
        {
            
            if (isFind && isAvailableCustomerIDForUpdate())
            {
            
                List<SqlParameter> sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@CUSTOMERID", CustomerNotextBox.Text));
                sqlParam.Add(new SqlParameter("@STATUS", Status.DEACTIVE));

                if (connection.callProcedure("DELETE_DATA_CUSTOMER", sqlParam))
                {
                    newMasterCustomer();
                    reloadAllData();
                }
            }
        }

        private Boolean isValidation()
        {
            if (Utilities.isEmptyString(CustomerNotextBox.Text))
            {
                MessageBox.Show("Please Input Customer No");
            }
            else if (isAvailableCustomerNo())
            {
                MessageBox.Show("Customer No has been Available");
            }
            else if (activity.Equals("UPDATE") && !isAvailableCustomerIDForUpdate()) {
                MessageBox.Show("Customer No Has not Availabel for Update");
            }
            else if (Utilities.isEmptyString(CustomerNametextBox.Text))
            {
                MessageBox.Show("Please Input Customer Name");
            }
            //else if (Utilities.isEmptyString(AddressrichTextBox.Text))
            //{
            //    MessageBox.Show("Please Input Address");
            //}
            else if (Utilities.isEmptyString(ZonetextBox.Text))
            {
                MessageBox.Show("Please Input Zone");
            }
            //else if (Utilities.isEmptyString(NoTelptextBox.Text))
            //{
            //    MessageBox.Show("Please Input Telp");
            //}
            //else if (Utilities.isEmptyString(NoFaxtextBox.Text))
            //{
            //    MessageBox.Show("Please Input Fax");
            //}
            //else if (Utilities.isEmptyString(ContactPersontextBox.Text))
            //{
            //    MessageBox.Show("Please Input Contact Person");
            //}
            //else if (Utilities.isEmptyString(HandphonetextBox.Text))
            //{
            //    MessageBox.Show("Please Input Handphone");
            //}
            //else if (Utilities.isEmptyString(EmailtextBox.Text))
            //{
            //    MessageBox.Show("Please Input Email");
            //}
            else
            {
                if (!isFind)
                {
                    String query = String.Format("SELECT * FROM M_CUSTOMER WHERE CUSTOMERID = '{0}'", CustomerNotextBox.Text);
                    DataTable dt = connection.openDataTableQuery(query);
                    if (dt.Rows.Count == 0)
                    {
                        return true;
                    }
                    MessageBox.Show("Customer ID Already exists");
                }
                else {
                    return true;
                }
            }
            return false;
        }

        private Boolean isAvailableCustomerNo()
        {
            CustomerNotextBox.Text = Utilities.removeSpace(CustomerNotextBox.Text);
            Boolean result = activity.Equals("UPDATE") ? false : Utilities.isAvailableID(CustomerID, CustomerNotextBox.Text);

            return result;
        }

        private Boolean isAvailableCustomerIDForUpdate() {
            CustomerNotextBox.Text = Utilities.removeSpace(CustomerNotextBox.Text);
            Boolean result = Utilities.isAvailableID(CustomerID, CustomerNotextBox.Text);
            return result;
        }

        private void NoTelptextBox_KeyPress(object sender, KeyPressEventArgs e){
            e.Handled = Utilities.onlyNumberTextBox(sender, e);
        }

        private void NoFaxtextBox_KeyPress(object sender, KeyPressEventArgs e){
            e.Handled = Utilities.onlyNumberTextBox(sender, e);
        }

        private void MasterCustomerPassingData(DataTable sender) {
            sender.Rows[0]["CUSTOMERID"].ToString();
            CustomerNotextBox.Text = sender.Rows[0]["CUSTOMERID"].ToString();
            CustomerNametextBox.Text = sender.Rows[0]["CUSTOMERNAME"].ToString();
            AddressrichTextBox.Text = sender.Rows[0]["ADDRESS"].ToString();
            ZonetextBox.Text = sender.Rows[0]["ZONE"].ToString();
            NoTelptextBox.Text = sender.Rows[0]["NOTELP"].ToString();
            NoFaxtextBox.Text = sender.Rows[0]["FAX"].ToString();
            EmailtextBox.Text = sender.Rows[0]["EMAIL"].ToString();
            ContactPersontextBox.Text = sender.Rows[0]["CONTACTPERSON"].ToString();
            HandphonetextBox.Text = sender.Rows[0]["NOHP"].ToString();
            SalestextBox.Text = sender.Rows[0]["SALESID"].ToString();
        }

        private void CustomerNotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (searchCustomerNobutton.Visible)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    Boolean isExistingPartNo = false;
                    foreach (String item in CustomerID)
                    {
                        if (item.ToUpper().Equals(CustomerNotextBox.Text.ToUpper()))
                        {
                            isExistingPartNo = true;
                            CustomerNotextBox.Text = CustomerNotextBox.Text.ToUpper();
                            String query = String.Format("SELECT CUSTOMERID, CUSTOMERNAME, " +
                                                         "[ADDRESS], ZONE, NOTELP, FAX, EMAIL, CONTACTPERSON, NOHP, SALESID, STATUS, CREATEDBY " +
                                                         "FROM M_CUSTOMER " +
                                                         "WHERE CUSTOMERID = '{0}'", CustomerNotextBox.Text);

                            DataTable datatable = connection.openDataTableQuery(query);
                            MasterCustomerPassingData(datatable);
                            break;
                        }
                    }

                    if (!isExistingPartNo)
                    {
                        CustomerNotextBox.Text = "";
                        Utilities.clearAllField(ref controlTextBox);
                    }
                }
            }
            else {
                if (e.KeyCode == Keys.Enter)
                {
                    Boolean isExistingPartNo = false;
                    foreach (String item in CustomerID)
                    {
                        if (item.ToUpper().Equals(CustomerNotextBox.Text.ToUpper()))
                        {
                            isExistingPartNo = true;
                            break;
                        }
                    }
                    if (isExistingPartNo)
                    {
                        MessageBox.Show("Customer ID Already Exist");
                        CustomerNotextBox.Text = "";
                        //Utilities.clearAllField(ref controlTextBox);
                    }
                }
            }
        }

        private void CustomerNotextBox_Leave(object sender, EventArgs e)
        {
            CustomerNotextBox.Text = CustomerNotextBox.Text.ToUpper();
            if (isFind)
            {
                Boolean isExistingPartNo = false;
                foreach (String item in CustomerID)
                {
                    if (item.ToUpper().Equals(CustomerNotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        CustomerNotextBox.Text = CustomerNotextBox.Text.ToUpper();
                        String query = String.Format("SELECT CUSTOMERID, CUSTOMERNAME, " +
                                                     "[ADDRESS], ZONE, NOTELP, FAX, EMAIL, CONTACTPERSON, NOHP, SALESID, STATUS, CREATEDBY " +
                                                     "FROM M_CUSTOMER " +
                                                     "WHERE CUSTOMERID = '{0}'", CustomerNotextBox.Text);

                        DataTable datatable = connection.openDataTableQuery(query);
                        MasterCustomerPassingData(datatable);
                        break;
                    }
                }

                if (!isExistingPartNo)
                {
                    CustomerNotextBox.Text = "";
                    Utilities.clearAllField(ref controlTextBox);
                }
            }
            else {
                Boolean isExistingPartNo = false;
                foreach (String item in CustomerID)
                {
                    if (item.ToUpper().Equals(CustomerNotextBox.Text.ToUpper()))
                    {
                        isExistingPartNo = true;
                        break;
                    }
                }
                if (isExistingPartNo)
                {
                    MessageBox.Show("Customer ID Already Exist");
                    CustomerNotextBox.Text = "";
                    //Utilities.clearAllField(ref controlTextBox);
                }
            }
        }

        private void CustomerNametextBox_Leave(object sender, EventArgs e)
        {
            CustomerNametextBox.Text = CustomerNametextBox.Text.ToUpper();
        }

        private void MasterCustomer_Activated(object sender, EventArgs e)
        {
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menustripAction, this.Text);
            Main.ACTIVEFORM = this;
            reloadAllData();
        }

        private void MasterCustomer_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection != null) connection.CloseConnection();
            Main.LISTFORM.Remove(this);
            if (Main.LISTFORM.Count == 0)
            {
                Utilities.showMenuStripAction(menustripAction.Items, false);
            }
            Main.ACTIVEFORM = null;
        }

        private void searchCustomerNobutton_Click(object sender, EventArgs e)
        {
            if (customerdatagridview == null)
            {
                customerdatagridview = new Customerdatagridview("MasterCustomer");
                customerdatagridview.masterCustomerPassingData = new Customerdatagridview.MasterCustomerPassingData(MasterCustomerPassingData);
                //supplierdatagridview.MdiParent = this.MdiParent;
                customerdatagridview.ShowDialog();
            }
            else if (customerdatagridview.IsDisposed)
            {
                customerdatagridview = new Customerdatagridview("MasterCustomer");
                customerdatagridview.masterCustomerPassingData = new Customerdatagridview.MasterCustomerPassingData(MasterCustomerPassingData);
                //supplierdatagridview.MdiParent = this.MdiParent;
                customerdatagridview.ShowDialog();
            }
        }

        private void HandphonetextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar));
        }

    }
}
