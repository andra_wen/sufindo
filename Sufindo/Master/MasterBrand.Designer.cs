﻿namespace Sufindo.Master
{
    partial class MasterBrand
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DatatableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.Spesifikasipanel = new System.Windows.Forms.Panel();
            this.InformationrichTextBox = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.BrandIDtextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.BrandNametextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.searchBrandIDbutton = new System.Windows.Forms.Button();
            this.DatatableLayoutPanel.SuspendLayout();
            this.Spesifikasipanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // DatatableLayoutPanel
            // 
            this.DatatableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.DatatableLayoutPanel.ColumnCount = 8;
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.DatatableLayoutPanel.Controls.Add(this.Spesifikasipanel, 1, 3);
            this.DatatableLayoutPanel.Controls.Add(this.label5, 0, 3);
            this.DatatableLayoutPanel.Controls.Add(this.BrandIDtextBox, 1, 0);
            this.DatatableLayoutPanel.Controls.Add(this.label3, 0, 1);
            this.DatatableLayoutPanel.Controls.Add(this.BrandNametextBox, 1, 1);
            this.DatatableLayoutPanel.Controls.Add(this.label2, 0, 0);
            this.DatatableLayoutPanel.Controls.Add(this.searchBrandIDbutton, 4, 0);
            this.DatatableLayoutPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.75F);
            this.DatatableLayoutPanel.Location = new System.Drawing.Point(3, 2);
            this.DatatableLayoutPanel.Name = "DatatableLayoutPanel";
            this.DatatableLayoutPanel.RowCount = 4;
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.DatatableLayoutPanel.Size = new System.Drawing.Size(537, 188);
            this.DatatableLayoutPanel.TabIndex = 1;
            // 
            // Spesifikasipanel
            // 
            this.Spesifikasipanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.Spesifikasipanel, 7);
            this.Spesifikasipanel.Controls.Add(this.InformationrichTextBox);
            this.Spesifikasipanel.Location = new System.Drawing.Point(86, 58);
            this.Spesifikasipanel.Name = "Spesifikasipanel";
            this.Spesifikasipanel.Size = new System.Drawing.Size(430, 128);
            this.Spesifikasipanel.TabIndex = 4;
            // 
            // InformationrichTextBox
            // 
            this.InformationrichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.InformationrichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InformationrichTextBox.Location = new System.Drawing.Point(0, 0);
            this.InformationrichTextBox.Name = "InformationrichTextBox";
            this.InformationrichTextBox.Size = new System.Drawing.Size(428, 126);
            this.InformationrichTextBox.TabIndex = 4;
            this.InformationrichTextBox.Text = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "Information";
            // 
            // BrandIDtextBox
            // 
            this.BrandIDtextBox.BackColor = System.Drawing.SystemColors.Window;
            this.BrandIDtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.BrandIDtextBox, 3);
            this.BrandIDtextBox.Location = new System.Drawing.Point(86, 3);
            this.BrandIDtextBox.Name = "BrandIDtextBox";
            this.BrandIDtextBox.Size = new System.Drawing.Size(204, 21);
            this.BrandIDtextBox.TabIndex = 1;
            this.BrandIDtextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.BrandIDtextBox_KeyDown);
            this.BrandIDtextBox.Leave += new System.EventHandler(this.BrandIDtextBox_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Brand Name";
            // 
            // BrandNametextBox
            // 
            this.BrandNametextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DatatableLayoutPanel.SetColumnSpan(this.BrandNametextBox, 4);
            this.BrandNametextBox.Location = new System.Drawing.Point(86, 31);
            this.BrandNametextBox.Name = "BrandNametextBox";
            this.BrandNametextBox.Size = new System.Drawing.Size(255, 21);
            this.BrandNametextBox.TabIndex = 3;
            this.BrandNametextBox.Leave += new System.EventHandler(this.BrandNametextBox_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Brand ID";
            // 
            // searchBrandIDbutton
            // 
            this.searchBrandIDbutton.BackColor = System.Drawing.Color.Transparent;
            this.searchBrandIDbutton.BackgroundImage = global::Sufindo.Properties.Resources.search;
            this.searchBrandIDbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchBrandIDbutton.FlatAppearance.BorderSize = 0;
            this.searchBrandIDbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchBrandIDbutton.ForeColor = System.Drawing.Color.Transparent;
            this.searchBrandIDbutton.Location = new System.Drawing.Point(296, 1);
            this.searchBrandIDbutton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.searchBrandIDbutton.Name = "searchBrandIDbutton";
            this.searchBrandIDbutton.Size = new System.Drawing.Size(24, 24);
            this.searchBrandIDbutton.TabIndex = 2;
            this.searchBrandIDbutton.UseVisualStyleBackColor = false;
            this.searchBrandIDbutton.Visible = false;
            this.searchBrandIDbutton.Click += new System.EventHandler(this.searchBrandIDbutton_Click);
            // 
            // MasterBrand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 192);
            this.Controls.Add(this.DatatableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MasterBrand";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Brand";
            this.Activated += new System.EventHandler(this.MasterBrand_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MasterBrand_FormClosed);
            this.DatatableLayoutPanel.ResumeLayout(false);
            this.DatatableLayoutPanel.PerformLayout();
            this.Spesifikasipanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel DatatableLayoutPanel;
        private System.Windows.Forms.TextBox BrandIDtextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox BrandNametextBox;
        private System.Windows.Forms.Button searchBrandIDbutton;
        private System.Windows.Forms.Panel Spesifikasipanel;
        private System.Windows.Forms.RichTextBox InformationrichTextBox;
        private System.Windows.Forms.Label label5;
    }
}