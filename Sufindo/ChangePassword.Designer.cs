﻿namespace Sufindo
{
    partial class ChangePassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.oldpasswordtextBox = new System.Windows.Forms.TextBox();
            this.newpasswordtextBox = new System.Windows.Forms.TextBox();
            this.confirmnewpasswordtextBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.Controls.Add(this.confirmnewpasswordtextBox, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.newpasswordtextBox, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.oldpasswordtextBox, 1, 0);
            this.tableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 3;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.Size = new System.Drawing.Size(328, 80);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Old Password";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "New Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Confirm New Password";
            // 
            // oldpasswordtextBox
            // 
            this.oldpasswordtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.oldpasswordtextBox.Location = new System.Drawing.Point(125, 3);
            this.oldpasswordtextBox.Name = "oldpasswordtextBox";
            this.oldpasswordtextBox.PasswordChar = '*';
            this.oldpasswordtextBox.Size = new System.Drawing.Size(198, 20);
            this.oldpasswordtextBox.TabIndex = 3;
            // 
            // newpasswordtextBox
            // 
            this.newpasswordtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.newpasswordtextBox.Location = new System.Drawing.Point(125, 29);
            this.newpasswordtextBox.Name = "newpasswordtextBox";
            this.newpasswordtextBox.PasswordChar = '*';
            this.newpasswordtextBox.Size = new System.Drawing.Size(198, 20);
            this.newpasswordtextBox.TabIndex = 4;
            // 
            // confirmnewpasswordtextBox
            // 
            this.confirmnewpasswordtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.confirmnewpasswordtextBox.Location = new System.Drawing.Point(125, 55);
            this.confirmnewpasswordtextBox.Name = "confirmnewpasswordtextBox";
            this.confirmnewpasswordtextBox.PasswordChar = '*';
            this.confirmnewpasswordtextBox.Size = new System.Drawing.Size(198, 20);
            this.confirmnewpasswordtextBox.TabIndex = 5;
            // 
            // ChangePassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(332, 84);
            this.Controls.Add(this.tableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChangePassword";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Change Password";
            this.Activated += new System.EventHandler(this.ChangePassword_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ChangePassword_FormClosed);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.TextBox confirmnewpasswordtextBox;
        private System.Windows.Forms.TextBox newpasswordtextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox oldpasswordtextBox;
    }
}