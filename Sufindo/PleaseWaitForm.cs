﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Sufindo
{
    public partial class PleaseWaitForm : Form
    {
        private static PleaseWaitForm instance = null;
        private static readonly object padlock = new object();

        public PleaseWaitForm()
        {
            InitializeComponent();
        }

        public static PleaseWaitForm sharedInstance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new PleaseWaitForm();
                    }
                    return instance;
                }
            }
        }
    }
}
