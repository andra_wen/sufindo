﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sufindo.Master;
using Sufindo.Purchase;
using Sufindo.Sales;
using Sufindo.Common;
using Sufindo.Setting;
using Sufindo.Report;
using Sufindo.Report.InventoryAudit;
using Sufindo.Report.OrderPicking;
using Sufindo.Report.ListInvoice;
using Sufindo.Report.ListAdjustment;
using Sufindo.Report.ListSpoil;
using Sufindo.Report.StockTag;
using Sufindo.Report.OrderPickingWithInvoice;
using Sufindo.Report.ListItemByRak;
using Sufindo.Report.ListPurchaseDirect;
using Sufindo.Report.ListPurchaseIndirect;
using Sufindo.Report.ListInvoicePerCustomer;
using Sufindo.Report.ListPurchaseReturn;
using Sufindo.Report.ListInvoiceTotal;
using Sufindo.Report.SalesReturn;
using Sufindo.Report.ListSalesOrder;
using Sufindo.Report.ListPurchaseRevision;
using Sufindo.Report.ListEOBI;

namespace Sufindo
{
    public partial class Main : Form
    {
        public delegate void AdjustAuthenticationMenuUser(ref MenuStrip menu);

        public AdjustAuthenticationMenuUser adjustAuthenticationMenuUser;

        public static Form ACTIVEFORM = null;
        public static List<Form> LISTFORM = new List<Form>();

        MasterItem masterItem;
        MasterSupplier masterSupplier;
        MasterBrand masterBrand;
        MasterUOM masterUOM;
        MasterRack masterRack;
        MasterKurs masterKurs;
        MasterEmployee masterEmployee;
        MasterCustomer masterCustomer;

        ItemSupplier itemsupplier;
        RecordCardStokItem recordCardStokItem;

        PurchaseOrder purchaseOrder;
        PurchaseOrderView purhcaseOrderView;
        PurchaseReceipt purchaseReceipt;
        PurchaseReturn purchaseReturn;
        PurchaseReceiptRevision receiptRevision;

        SalesOrder salesOrder;
        SalesReturn salesReturn;
        OrderPicking orderPicking;
        Quotation quotation;
        Cost cost;

        ItemBuild itemBuildIn;
        ItemBuild itemExtractOut;

        Adjustment_Spoil adjustmentspoil;
        ImportExportForm importexport;

        AuthenticationUser autheuser;
        AuthenticationPrice autheprice;

        ChangePassword changepassword;

        AllReportForm allreport;
        InventoryAuditReport inventoryauditreport;
        OrderPickingReport orderpickingreport;
        ListInvoiceReport listinvoicereport;
        ListAdjustmentReport listadjustmentreport;
        ListSpoilReport listspoilreport;
        StockTagReport stocktagreport;
        OrderPickingWithInvoiceReport orderpickingwithinvoicereport;
        ListItemByRakReport listitembyrakreport;
        ListPurchaseDirectReport listpurchasedirectreport;
        ListPurchaseIndirectReport listpurchaseindirectreport;
        ListInvoicePerCustomerReport listinvoicepercustomerreport;
        ListPurchaseReturnReport listpurchasereturnreport;
        ListInvoiceTotalReport listinvoicetotalreport;
        SalesReturnReport salesreturnreport;
        ListSalesOrderReport listsalesorderreport;
        ListPurchaseRevisionReport listpurchaserevisionreport;
        ListEOBIReport listeobireport;

        public Main()
        {
            InitializeComponent();
            Utilities.showMenuStripAction(menuStripAction.Items, false);
            foreach (ToolStripMenuItem parent in menuStrips.Items)
            {
                foreach (ToolStripMenuItem child in parent.DropDownItems)
                {
                    child.Click += new EventHandler(menuStrip_Click);
                }
            }

            masterItem = null;
            recordCardStokItem = null;
            itemsupplier = null;
            masterSupplier = null;
            masterBrand = null;
            masterUOM = null;
            masterRack = null;
            masterKurs = null;
            purchaseOrder = null;
            receiptRevision = null;
            itemBuildIn = null;
            itemExtractOut = null;
            masterEmployee = null;
            masterCustomer = null;
            salesOrder = null;
            quotation = null;
            cost = null;
            salesReturn = null;
            adjustmentspoil = null;
            importexport = null;

            autheuser = null;
            changepassword = null;

            inventoryauditreport = null;
            orderpickingreport = null;
            listinvoicereport = null;
            listadjustmentreport = null;
            listspoilreport = null;
            stocktagreport = null;
            orderpickingwithinvoicereport = null;
            listitembyrakreport = null;
            listpurchasedirectreport = null;
            listpurchaseindirectreport = null;
            listinvoicepercustomerreport = null;
            listpurchasereturnreport = null;
            salesreturnreport = null;
            listsalesorderreport = null;
            listpurchaserevisionreport = null;

            this.fileToolStripMenuItems.Text = AuthorizeUser.sharedInstance.userdata["EMPLOYEENAME"].ToString();
        }
        void menuStrip_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menu = (ToolStripMenuItem)sender;
            AuthorizeUser.sharedInstance.adjustAuthenticationMenuAction(ref menuStripAction, menu.Text);
        }
        public void setAdjustAuthenticationMenuUser(){
            if (adjustAuthenticationMenuUser != null)
            {
                adjustAuthenticationMenuUser(ref menuStrips);
            }    
        }
        private void printtoolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }
        private void closetoolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
        private void adjustmentSpoilToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }
       
        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Main.LISTFORM.RemoveRange(0, Main.LISTFORM.Count);
            AuthorizeUser.sharedInstance.userdata = null;
            AuthorizeUser.sharedInstance.authenticationmenu = null;
            Login.sharedInstance.Show();
            this.Dispose(true);
        }

        private void NewtoolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ACTIVEFORM != null)
            {

                /*MASTER ITEM*/
                if (masterItem != null && !masterItem.IsDisposed && ACTIVEFORM.GetType().Equals(masterItem.GetType()))
                {
                    masterItem.newMaster();
                }

                /*MASTER UOM*/
                else if (masterUOM != null && !masterUOM.IsDisposed && ACTIVEFORM.GetType().Equals(masterUOM.GetType()))
                {
                    masterUOM.newMaster();
                }

                /*MASTER BRAND*/
                else if (masterBrand != null && !masterBrand.IsDisposed && ACTIVEFORM.GetType().Equals(masterBrand.GetType()))
                {
                    masterBrand.newMaster();
                }

                /*MASTER SUPPLIER*/
                else if (masterSupplier != null && !masterSupplier.IsDisposed && ACTIVEFORM.GetType().Equals(masterSupplier.GetType()))
                {
                    masterSupplier.newMasterSupplier();
                }
                /*MASTER CUSTOMER*/
                else if (masterCustomer != null && !masterCustomer.IsDisposed && ACTIVEFORM.GetType().Equals(masterCustomer.GetType()))
                {
                    masterCustomer.newMasterCustomer();
                }
                /*MASTER EMPLOYEE*/
                else if (masterEmployee != null && !masterEmployee.IsDisposed && ACTIVEFORM.GetType().Equals(masterEmployee.GetType()))
                {
                    masterEmployee.newMasterEmployee();
                }

                /*MASTER ITEM SUPPLIER*/
                else if (itemsupplier != null && !itemsupplier.IsDisposed && ACTIVEFORM.GetType().Equals(itemsupplier.GetType()))
                {
                }

                /*MASTER RACK*/
                else if (masterRack != null && !masterRack.IsDisposed && ACTIVEFORM.GetType().Equals(masterRack.GetType()))
                {
                    masterRack.newMaster();
                }

                /*MASTER KURS*/
                else if (masterKurs != null && !masterKurs.IsDisposed && ACTIVEFORM.GetType().Equals(masterKurs.GetType()))
                {
                    masterKurs.newMaster();
                }

                /*PURCHASE ORDER*/
                else if (purchaseOrder != null && !purchaseOrder.IsDisposed && ACTIVEFORM.GetType().Equals(purchaseOrder.GetType()))
                {
                    purchaseOrder.newTransaction();
                }

                /*PURCHASE RECEIPT*/
                else if (purchaseReceipt != null && !purchaseReceipt.IsDisposed && ACTIVEFORM.GetType().Equals(purchaseReceipt.GetType()))
                {
                    purchaseReceipt.newTransaction();
                }

                /*PURCHASE RETURN*/
                else if (purchaseReturn != null && !purchaseReturn.IsDisposed && ACTIVEFORM.GetType().Equals(purchaseReturn.GetType()))
                {
                    purchaseReturn.newReturn();
                }

                /*RECEIPT REVISION*/
                else if (receiptRevision != null && !receiptRevision.IsDisposed && ACTIVEFORM.GetType().Equals(receiptRevision.GetType()))
                {
                    receiptRevision.newTransaction();
                }

                /*SALES ORDER*/
                else if (salesOrder != null && !salesOrder.IsDisposed && ACTIVEFORM.GetType().Equals(salesOrder.GetType()))
                {
                    salesOrder.newTransaction();
                }

                /*ORDER PICKING*/
                else if (orderPicking != null && !orderPicking.IsDisposed && ACTIVEFORM.GetType().Equals(orderPicking.GetType()))
                {
                    orderPicking.neworderpicking();
                }

                /*QUOTATION*/
                else if (quotation != null && !quotation.IsDisposed && ACTIVEFORM.GetType().Equals(quotation.GetType()))
                {
                    quotation.newTransaction();
                }

                 /*SALES RETURN*/
                else if (salesReturn != null && !salesReturn.IsDisposed && ACTIVEFORM.GetType().Equals(salesReturn.GetType()))
                {
                    salesReturn.newSalesReturn();
                }

                /*ADJUSTMENT SPOIL*/
                else if (adjustmentspoil != null && !adjustmentspoil.IsDisposed && ACTIVEFORM.GetType().Equals(adjustmentspoil.GetType()))
                {
                    adjustmentspoil.newas();
                }
            }
        }
        private void SavetoolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ACTIVEFORM != null)
            {

                /*MASTER ITEM*/
                if (masterItem != null && !masterItem.IsDisposed && ACTIVEFORM.GetType().Equals(masterItem.GetType()))
                {
                    masterItem.save();
                }

                /*MASTER UOM*/
                else if (masterUOM != null && !masterUOM.IsDisposed && ACTIVEFORM.GetType().Equals(masterUOM.GetType()))
                {
                    masterUOM.save();
                }

                /*MASTER BRAND*/
                else if (masterBrand != null && !masterBrand.IsDisposed && ACTIVEFORM.GetType().Equals(masterBrand.GetType()))
                {
                    masterBrand.save();
                }

                /*MASTER SUPPLIER*/
                else if (masterSupplier != null && !masterSupplier.IsDisposed && ACTIVEFORM.GetType().Equals(masterSupplier.GetType()))
                {
                    masterSupplier.save();
                }
                /*MASTER CUSTOMER*/
                else if (masterCustomer != null && !masterCustomer.IsDisposed && ACTIVEFORM.GetType().Equals(masterCustomer.GetType()))
                {
                    masterCustomer.save();
                }
                /*MASTER EMPLOYEE*/
                else if (masterEmployee != null && !masterEmployee.IsDisposed && ACTIVEFORM.GetType().Equals(masterEmployee.GetType()))
                {
                    masterEmployee.save();
                }

                /*MASTER ITEM SUPPLIER*/
                else if (itemsupplier != null && !itemsupplier.IsDisposed && ACTIVEFORM.GetType().Equals(itemsupplier.GetType()))
                {
                    itemsupplier.save();
                }

                /*MASTER RACK*/
                else if (masterRack != null && !masterRack.IsDisposed && ACTIVEFORM.GetType().Equals(masterRack.GetType()))
                {
                    masterRack.save();
                }

                /*MASTER KURS*/
                else if (masterKurs != null && !masterKurs.IsDisposed && ACTIVEFORM.GetType().Equals(masterKurs.GetType()))
                {
                    masterKurs.save();
                }

                /*PURCHASE ORDER*/
                else if (purchaseOrder != null && !purchaseOrder.IsDisposed && ACTIVEFORM.GetType().Equals(purchaseOrder.GetType()))
                {
                    purchaseOrder.save();
                }

                /*PURCHASE RECEIPT*/
                else if (purchaseReceipt != null && !purchaseReceipt.IsDisposed && ACTIVEFORM.GetType().Equals(purchaseReceipt.GetType()))
                {
                    purchaseReceipt.save();
                }

                /*PURCHASE RETURN*/
                else if (purchaseReturn != null && !purchaseReturn.IsDisposed && ACTIVEFORM.GetType().Equals(purchaseReturn.GetType()))
                {
                    purchaseReturn.save();
                }

                /*RECEIPT REVISION*/
                else if (receiptRevision != null && !receiptRevision.IsDisposed && ACTIVEFORM.GetType().Equals(receiptRevision.GetType()))
                {
                    receiptRevision.save();
                }

                /*SALES ORDER*/
                else if (salesOrder != null && !salesOrder.IsDisposed && ACTIVEFORM.GetType().Equals(salesOrder.GetType()))
                {
                    salesOrder.save();
                }

                 /*QUOTATION*/
                else if (quotation != null && !quotation.IsDisposed && ACTIVEFORM.GetType().Equals(quotation.GetType()))
                {
                    quotation.save();
                }

                 /*COST*/
                else if (cost != null && !cost.IsDisposed && ACTIVEFORM.GetType().Equals(cost.GetType()))
                {
                    cost.save();
                }

                /*SALES RETURN*/
                else if (salesReturn != null && !salesReturn.IsDisposed && ACTIVEFORM.GetType().Equals(salesReturn.GetType()))
                {
                    salesReturn.save();
                }

                /*ADJUSTMENT SPOIL*/
                else if (adjustmentspoil != null && !adjustmentspoil.IsDisposed && ACTIVEFORM.GetType().Equals(adjustmentspoil.GetType()))
                {
                    adjustmentspoil.save();
                }

                /*AUTHENTICATION USER*/
                else if (autheuser != null && !autheuser.IsDisposed && ACTIVEFORM.GetType().Equals(autheuser.GetType()))
                {
                    autheuser.save();
                }
                /*AUTHENTICATION PRICE*/
                else if (autheprice != null && !autheprice.IsDisposed && ACTIVEFORM.GetType().Equals(autheprice.GetType()))
                {
                    autheprice.save();
                }

                 /*Change Password*/
                else if (changepassword != null && !changepassword.IsDisposed && ACTIVEFORM.GetType().Equals(changepassword.GetType()))
                {
                    changepassword.save();
                }
            }
        }
        private void FindtoolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ACTIVEFORM != null)
            {
                /*MASTER ITEM*/
                if (masterItem != null && !masterItem.IsDisposed && ACTIVEFORM.GetType().Equals(masterItem.GetType()))
                {
                    masterItem.find();
                }

                /*MASTER UOM*/
                else if (masterUOM != null && !masterUOM.IsDisposed && ACTIVEFORM.GetType().Equals(masterUOM.GetType()))
                {
                    masterUOM.find();
                }

                /*MASTER BRAND*/
                else if (masterBrand != null && !masterBrand.IsDisposed && ACTIVEFORM.GetType().Equals(masterBrand.GetType()))
                {
                    masterBrand.find();
                }

                /*MASTER SUPPLIER*/
                else if (masterSupplier != null && !masterSupplier.IsDisposed && ACTIVEFORM.GetType().Equals(masterSupplier.GetType()))
                {
                    masterSupplier.findMasterSupplier();
                }
                /*MASTER CUSTOMER*/
                else if (masterCustomer != null && !masterCustomer.IsDisposed && ACTIVEFORM.GetType().Equals(masterCustomer.GetType()))
                {
                    masterCustomer.findMasterCustomer();
                }
                /*MASTER EMPLOYEE*/
                else if (masterEmployee != null && !masterEmployee.IsDisposed && ACTIVEFORM.GetType().Equals(masterEmployee.GetType()))
                {
                    masterEmployee.find();
                }

                /*MASTER ITEM SUPPLIER*/
                else if (itemsupplier != null && !itemsupplier.IsDisposed && ACTIVEFORM.GetType().Equals(itemsupplier.GetType()))
                {
                }

                /*MASTER RACK*/
                else if (masterRack != null && !masterRack.IsDisposed && ACTIVEFORM.GetType().Equals(masterRack.GetType()))
                {
                    masterRack.find();
                }

                /*MASTER KURS*/
                else if (masterKurs != null && !masterKurs.IsDisposed && ACTIVEFORM.GetType().Equals(masterKurs.GetType()))
                {
                    masterKurs.find();
                }

                /*PURCHASE ORDER*/
                else if (purchaseOrder != null && !purchaseOrder.IsDisposed && ACTIVEFORM.GetType().Equals(purchaseOrder.GetType()))
                {
                    purchaseOrder.find();
                }

                /*PURCHASE RECEIPT*/
                else if (purchaseReceipt != null && !purchaseReceipt.IsDisposed && ACTIVEFORM.GetType().Equals(purchaseReceipt.GetType()))
                {
                    purchaseReceipt.find();
                }

                /*PURCHASE RECEIPT*/
                else if (purchaseReturn != null && !purchaseReturn.IsDisposed && ACTIVEFORM.GetType().Equals(purchaseReturn.GetType()))
                {
                    purchaseReturn.find();
                }

                /*RECEIPT REVISION*/
                else if (receiptRevision != null && !receiptRevision.IsDisposed && ACTIVEFORM.GetType().Equals(receiptRevision.GetType()))
                {
                    receiptRevision.find();
                }

                /*SALES ORDER*/
                else if (salesOrder != null && !salesOrder.IsDisposed && ACTIVEFORM.GetType().Equals(salesOrder.GetType()))
                {
                    salesOrder.find();
                }

                 /*ORDER PICKING*/
                else if (orderPicking != null && !orderPicking.IsDisposed && ACTIVEFORM.GetType().Equals(orderPicking.GetType()))
                {
                    orderPicking.find();
                }

                 /*QUOTATION*/
                else if (quotation != null && !quotation.IsDisposed && ACTIVEFORM.GetType().Equals(quotation.GetType()))
                {
                    quotation.find();
                }

                /*SALES RETURN*/
                else if (salesReturn != null && !salesReturn.IsDisposed && ACTIVEFORM.GetType().Equals(salesReturn.GetType()))
                {
                    salesReturn.find();
                }
                /*ADJUSTMENT SPOIL*/
                else if (adjustmentspoil != null && !adjustmentspoil.IsDisposed && ACTIVEFORM.GetType().Equals(adjustmentspoil.GetType()))
                {
                    adjustmentspoil.find();
                }
            }
        }
        private void EdittoolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (ACTIVEFORM != null)
            {
                /*MASTER ITEM*/
                if (masterItem != null && !masterItem.IsDisposed && ACTIVEFORM.GetType().Equals(masterItem.GetType()))
                {
                    masterItem.edit();
                }

                /*MASTER UOM*/
                else if (masterUOM != null && !masterUOM.IsDisposed && ACTIVEFORM.GetType().Equals(masterUOM.GetType()))
                {
                    masterUOM.edit();
                }

                /*MASTER BRAND*/
                else if (masterBrand != null && !masterBrand.IsDisposed && ACTIVEFORM.GetType().Equals(masterBrand.GetType()))
                {
                    masterBrand.edit();
                }

                /*MASTER SUPPLIER*/
                else if (masterSupplier != null && !masterSupplier.IsDisposed && ACTIVEFORM.GetType().Equals(masterSupplier.GetType()))
                {
                    masterSupplier.editMasterSupplier();
                }
                /*MASTER CUSTOMER*/
                else if (masterCustomer != null && !masterCustomer.IsDisposed && ACTIVEFORM.GetType().Equals(masterCustomer.GetType()))
                {
                    masterCustomer.editMasterCustomer();
                }
                /*MASTER EMPLOYEE*/
                else if (masterEmployee != null && !masterEmployee.IsDisposed && ACTIVEFORM.GetType().Equals(masterEmployee.GetType()))
                {
                    masterEmployee.edit();
                }

                /*MASTER ITEM SUPPLIER*/
                else if (itemsupplier != null && !itemsupplier.IsDisposed && ACTIVEFORM.GetType().Equals(itemsupplier.GetType()))
                {
                }

                /*MASTER RACK*/
                else if (masterRack != null && !masterRack.IsDisposed && ACTIVEFORM.GetType().Equals(masterRack.GetType()))
                {
                    masterRack.edit();
                }

                /*MASTER KURS*/
                else if (masterKurs != null && !masterKurs.IsDisposed && ACTIVEFORM.GetType().Equals(masterKurs.GetType()))
                {
                    masterKurs.edit();
                }

                /*PURCHASE ORDER*/
                else if (purchaseOrder != null && !purchaseOrder.IsDisposed && ACTIVEFORM.GetType().Equals(purchaseOrder.GetType()))
                {
                    purchaseOrder.edit();
                }

                /*PURCHASE RECEIPT*/
                else if (purchaseReceipt != null && !purchaseReceipt.IsDisposed && ACTIVEFORM.GetType().Equals(purchaseReceipt.GetType()))
                {
                    purchaseReceipt.edit();
                }

                /*PURCHASE RETURN*/
                else if (purchaseReturn != null && !purchaseReturn.IsDisposed && ACTIVEFORM.GetType().Equals(purchaseReturn.GetType()))
                {
                    purchaseReturn.edit();
                }

                /*PURCHASE RECEIPT REVISI*/
                else if (receiptRevision != null && !receiptRevision.IsDisposed && ACTIVEFORM.GetType().Equals(receiptRevision.GetType()))
                {
                    receiptRevision.edit();
                }

                 /*SALES ORDER*/
                else if (salesOrder != null && !salesOrder.IsDisposed && ACTIVEFORM.GetType().Equals(salesOrder.GetType()))
                {
                    salesOrder.edit();
                }

                 /*QUOTATION*/
                else if (quotation != null && !quotation.IsDisposed && ACTIVEFORM.GetType().Equals(quotation.GetType()))
                {
                    quotation.edit();
                }

                /*SALES RETURN*/
                else if (salesReturn != null && !salesReturn.IsDisposed && ACTIVEFORM.GetType().Equals(salesReturn.GetType()))
                {
                    salesReturn.edit();
                }

                /*ADJUSTMENT SPOIL*/
                else if (adjustmentspoil != null && !adjustmentspoil.IsDisposed && ACTIVEFORM.GetType().Equals(adjustmentspoil.GetType()))
                {
                    adjustmentspoil.edit();
                }
            }
        }
        private void DeletetoolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ACTIVEFORM != null)
            {
                DialogResult result = MessageBox.Show("Apakah Anda Ingin Menghapus Data Ini ? ", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (result == DialogResult.Yes)
                {
                    /*MASTER ITEM*/
                    if (masterItem != null && !masterItem.IsDisposed && ACTIVEFORM.GetType().Equals(masterItem.GetType()))
                    {
                        masterItem.delete();
                    }

                    /*MASTER UOM*/
                    else if (masterUOM != null && !masterUOM.IsDisposed && ACTIVEFORM.GetType().Equals(masterUOM.GetType()))
                    {
                        masterUOM.delete();
                    }

                    /*MASTER BRAND*/
                    else if (masterBrand != null && !masterBrand.IsDisposed && ACTIVEFORM.GetType().Equals(masterBrand.GetType()))
                    {
                        masterBrand.delete();
                    }

                    /*MASTER SUPPLIER*/
                    else if (masterSupplier != null && !masterSupplier.IsDisposed && ACTIVEFORM.GetType().Equals(masterSupplier.GetType()))
                    {
                        masterSupplier.deleteMasterSupplier();
                    }
                    /*MASTER CUSTOMER*/
                    else if (masterCustomer != null && !masterCustomer.IsDisposed && ACTIVEFORM.GetType().Equals(masterCustomer.GetType()))
                    {
                        masterCustomer.deleteMasterCustomer();
                    }
                    /*MASTER EMPLOYEE*/
                    else if (masterEmployee != null && !masterEmployee.IsDisposed && ACTIVEFORM.GetType().Equals(masterEmployee.GetType()))
                    {
                        masterEmployee.delete();
                    }

                    /*MASTER ITEM SUPPLIER*/
                    else if (itemsupplier != null && !itemsupplier.IsDisposed && ACTIVEFORM.GetType().Equals(itemsupplier.GetType()))
                    {
                    }

                    /*MASTER RACK*/
                    else if (masterRack != null && !masterRack.IsDisposed && ACTIVEFORM.GetType().Equals(masterRack.GetType()))
                    {
                        masterRack.delete();
                    }

                    /*MASTER KURS*/
                    else if (masterKurs != null && !masterKurs.IsDisposed && ACTIVEFORM.GetType().Equals(masterKurs.GetType()))
                    {
                        masterKurs.delete();
                    }

                    /*PURCHASE ORDER*/
                    else if (purchaseOrder != null && !purchaseOrder.IsDisposed && ACTIVEFORM.GetType().Equals(purchaseOrder.GetType()))
                    {
                        purchaseOrder.delete();
                    }

                    /*PURCHASE RECEIPT*/
                    else if (purchaseReceipt != null && !purchaseReceipt.IsDisposed && ACTIVEFORM.GetType().Equals(purchaseReceipt.GetType()))
                    {
                        purchaseReceipt.delete();
                    }

                    /*PURCHASE RETURN*/
                    else if (purchaseReturn != null && !purchaseReturn.IsDisposed && ACTIVEFORM.GetType().Equals(purchaseReturn.GetType()))
                    {
                        purchaseReturn.delete();
                    }

                    /*SALES ORDER*/
                    else if (salesOrder != null && !salesOrder.IsDisposed && ACTIVEFORM.GetType().Equals(salesOrder.GetType()))
                    {
                        salesOrder.delete();
                    }

                     /*QUOTATION*/
                    else if (quotation != null && !quotation.IsDisposed && ACTIVEFORM.GetType().Equals(quotation.GetType()))
                    {
                        quotation.delete();
                    }

                    /*SALES RETURN*/
                    else if (salesReturn != null && !salesReturn.IsDisposed && ACTIVEFORM.GetType().Equals(salesReturn.GetType()))
                    {
                        salesReturn.delete();
                    }

                     /*ADJUSTMENT SPOIL*/
                    else if (adjustmentspoil != null && !adjustmentspoil.IsDisposed && ACTIVEFORM.GetType().Equals(adjustmentspoil.GetType()))
                    {
                        adjustmentspoil.delete();
                    }
                }
            }

        }
        private void PrinttoolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ACTIVEFORM != null)
            {
                /*PURCHASE ORDER*/
                if (purchaseOrder != null && !purchaseOrder.IsDisposed && ACTIVEFORM.GetType().Equals(purchaseOrder.GetType()))
                {
                    purchaseOrder.print();
                }
                /*ORDER PICKING*/
                else if (orderPicking != null && !orderPicking.IsDisposed && ACTIVEFORM.GetType().Equals(orderPicking.GetType()))
                {
                    orderPicking.print();
                }
                /*QUOTATION*/
                else if (quotation != null && !quotation.IsDisposed && ACTIVEFORM.GetType().Equals(quotation.GetType()))
                {
                    quotation.print();
                }
                /*SALES RETURN*/
                else if (salesReturn != null && !salesReturn.IsDisposed && ACTIVEFORM.GetType().Equals(salesReturn.GetType()))
                {
                    salesReturn.print();
                }
                /*PURCHASE RETURN*/
                else if (purchaseReturn != null && !purchaseReturn.IsDisposed && ACTIVEFORM.GetType().Equals(purchaseReturn.GetType())) {
                    purchaseReturn.print();
                }
                /*PURCHASE REVISION*/
                else if (listpurchaserevisionreport != null && !listpurchaserevisionreport.IsDisposed && ACTIVEFORM.GetType().Equals(listpurchaserevisionreport.GetType()))
                {
                    listpurchaserevisionreport.print();
                }
                /*ALL REPORT*/
                else if (allreport != null && !allreport.IsDisposed && ACTIVEFORM.GetType().Equals(allreport.GetType())) {
                    allreport.print();
                }
                  /*ALL REPORT*/
                else if (allreport != null && !allreport.IsDisposed && ACTIVEFORM.GetType().Equals(allreport.GetType())) {
                    allreport.print();
                }
                /*INVENTORY AUDIT*/
                else if (inventoryauditreport != null && !inventoryauditreport.IsDisposed && ACTIVEFORM.GetType().Equals(inventoryauditreport.GetType()))
                {
                    inventoryauditreport.print();
                }
                /*LIST INVOICE*/
                else if (listinvoicereport != null && !listinvoicereport.IsDisposed && ACTIVEFORM.GetType().Equals(listinvoicereport.GetType()))
                {
                    listinvoicereport.print();
                }
                /*LIST ADJUSTMENT*/
                else if (listadjustmentreport != null && !listadjustmentreport.IsDisposed && ACTIVEFORM.GetType().Equals(listadjustmentreport.GetType()))
                {
                    listadjustmentreport.print();
                }
                /*LIST SPOIL*/
                else if (listspoilreport != null && !listspoilreport.IsDisposed && ACTIVEFORM.GetType().Equals(listspoilreport.GetType()))
                {
                    listspoilreport.print();
                }
                /*STOCK TAG*/
                else if (stocktagreport != null && !stocktagreport.IsDisposed && ACTIVEFORM.GetType().Equals(stocktagreport.GetType()))
                {
                    stocktagreport.print();
                }
                /*ORDER PICKING REPORT*/
                else if (orderpickingreport != null && !orderpickingreport.IsDisposed && ACTIVEFORM.GetType().Equals(orderpickingreport.GetType()))
                {
                    orderpickingreport.print();
                }
                /*ORDER PICKING WITH INVOICE*/
                else if (orderpickingwithinvoicereport != null && !orderpickingwithinvoicereport.IsDisposed && ACTIVEFORM.GetType().Equals(orderpickingwithinvoicereport.GetType()))
                {
                    orderpickingwithinvoicereport.print();
                }
                /*LIST ITEM BY RAK*/
                else if (listitembyrakreport != null && !listitembyrakreport.IsDisposed && ACTIVEFORM.GetType().Equals(listitembyrakreport.GetType()))
                {
                    listitembyrakreport.print();
                }
                /*LIST PURCHASE DIRECT*/
                else if (listpurchasedirectreport != null && !listpurchasedirectreport.IsDisposed && ACTIVEFORM.GetType().Equals(listpurchasedirectreport.GetType()))
                {
                    listpurchasedirectreport.print();
                }
                /*LIST PURCHASE INDIRECT*/
                else if (listpurchaseindirectreport != null && !listpurchaseindirectreport.IsDisposed && ACTIVEFORM.GetType().Equals(listpurchaseindirectreport.GetType()))
                {
                    listpurchaseindirectreport.print();
                }
                /*LIST INVOICE PERCUSTOMER*/
                else if (listinvoicepercustomerreport != null && !listinvoicepercustomerreport.IsDisposed && ACTIVEFORM.GetType().Equals(listinvoicepercustomerreport.GetType()))
                {
                    listinvoicepercustomerreport.print();
                }
                /*LIST PURCHASE RETURN*/
                else if (listpurchasereturnreport != null && !listpurchasereturnreport.IsDisposed && ACTIVEFORM.GetType().Equals(listpurchasereturnreport.GetType()))
                {
                    listpurchasereturnreport.print();
                }
                /*LIST INVOICE*/
                else if (listinvoicetotalreport != null && !listinvoicetotalreport.IsDisposed && ACTIVEFORM.GetType().Equals(listinvoicetotalreport.GetType()))
                {
                    listinvoicetotalreport.print();
                }
                /*SALES RETURN*/
                else if (salesreturnreport != null && !salesreturnreport.IsDisposed && ACTIVEFORM.GetType().Equals(salesreturnreport.GetType()))
                {
                    salesreturnreport.print();
                }
                /*SALES ORDER*/
                else if (listsalesorderreport != null && !listsalesorderreport.IsDisposed && ACTIVEFORM.GetType().Equals(listsalesorderreport.GetType()))
                {
                    listsalesorderreport.print();
                }
                /*BUILD IN / EXTRA OUT*/
                else if (listeobireport != null && !listeobireport.IsDisposed && ACTIVEFORM.GetType().Equals(listeobireport.GetType()))
                {
                    listeobireport.print();
                }
            }
        }
        private void ClosetoolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ACTIVEFORM != null)
            {
                /*ORDER PICKING*/
                if (orderPicking != null && !orderPicking.IsDisposed && ACTIVEFORM.GetType().Equals(orderPicking.GetType()))
                {
                    orderPicking.close();
                }
            }
        }
        private void ImporttoolStripMenuItem_Click(object sender, EventArgs e)
        {
             if (ACTIVEFORM != null) {
                if (importexport != null && !importexport.IsDisposed && ACTIVEFORM.GetType().Equals(importexport.GetType()))
                {
                    importexport.import();
                }
             }
        }
        private void ExporttoolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ACTIVEFORM != null)
            {
                if (importexport != null && !importexport.IsDisposed && ACTIVEFORM.GetType().Equals(importexport.GetType()))
                {
                    importexport.export();
                }
            }
        }
        private void ConfirmtoolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ACTIVEFORM != null)
            {
                /*SALES ORDER*/
                if (salesOrder != null && !salesOrder.IsDisposed && ACTIVEFORM.GetType().Equals(salesOrder.GetType()))
                {
                    salesOrder.confirm();
                }
                /*SALES RETURN*/
                else if (salesReturn != null && !salesReturn.IsDisposed && ACTIVEFORM.GetType().Equals(salesReturn.GetType()))
                {
                    salesReturn.confirm();
                }
                /*Adjust-Spoil item*/
                else if (adjustmentspoil != null && !adjustmentspoil.IsDisposed && ACTIVEFORM.GetType().Equals(adjustmentspoil.GetType()))
                {
                    adjustmentspoil.confirm();
                }
                /*RECEIPT REVISION*/
                else if (receiptRevision != null && !receiptRevision.IsDisposed && ACTIVEFORM.GetType().Equals(receiptRevision.GetType()))
                {
                    receiptRevision.confirm();
                }
                /*PURCHASE RETURN*/
                else if (purchaseReturn != null && !purchaseReturn.IsDisposed && ACTIVEFORM.GetType().Equals(purchaseReturn.GetType()))
                {
                    purchaseReturn.confirm();
                }
            }
        }
        private void CanceltoolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ACTIVEFORM != null)
            {
                /*SALES ORDER*/
                if (salesOrder != null && !salesOrder.IsDisposed && ACTIVEFORM.GetType().Equals(salesOrder.GetType()))
                {
                    salesOrder.cancel();
                }
                /*SALES RETURN*/
                else if (salesReturn != null && !salesReturn.IsDisposed && ACTIVEFORM.GetType().Equals(salesReturn.GetType()))
                {
                    salesReturn.cancel();
                }
            }
        }
        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            Environment.Exit(0);
        }
        private void ItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (masterItem == null || masterItem.IsDisposed)
            {
                Main main = this;

                masterItem = new MasterItem(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(masterItem);
                masterItem.MdiParent = this;
                masterItem.Show();
            }
            else
            {
                masterItem.Focus();
            }
        }
        private void BuildInItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (itemBuildIn == null || itemBuildIn.IsDisposed)
            {
                Main main = this;

                itemBuildIn = new ItemBuild("Build In Item", main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(itemBuildIn);
                itemBuildIn.MdiParent = this;
                itemBuildIn.Show();
            }
            else
            {
                itemBuildIn.Focus();
            }
        }
        private void ExtractOutItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (itemExtractOut == null || itemExtractOut.IsDisposed)
            {
                Main main = this;

                itemExtractOut = new ItemBuild("Extract Out Item", main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(itemExtractOut);
                itemExtractOut.MdiParent = this;
                itemExtractOut.Show();
            }
            else
            {
                itemExtractOut.Focus();
            }
        }
        private void UOMToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (masterUOM == null || masterUOM.IsDisposed)
            {
                Main main = this;
                masterUOM = new MasterUOM(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(masterUOM);
                masterUOM.MdiParent = this;
                masterUOM.Show();
            }
            else
            {
                masterUOM.Focus();
            }
        }
        private void BrandToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (masterBrand == null || masterBrand.IsDisposed)
            {
                Main main = this;

                masterBrand = new MasterBrand(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(masterBrand);
                masterBrand.MdiParent = this;
                masterBrand.Show();
            }
            else
            {
                masterBrand.Focus();
            }
        }
        private void SupplierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (masterSupplier == null || masterSupplier.IsDisposed)
            {
                Main main = this;

                masterSupplier = new MasterSupplier(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(masterSupplier);
                masterSupplier.MdiParent = this;
                masterSupplier.Show();
            }
            else
            {
                masterSupplier.Focus();
            }
        }
        private void SupplierItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (itemsupplier == null || itemsupplier.IsDisposed)
            {
                Main main = this;
                itemsupplier = new ItemSupplier(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(itemsupplier);
                itemsupplier.MdiParent = this;
                itemsupplier.Show();
            }
            else
            {
                itemsupplier.Focus();
            }
        }
        private void RackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (masterRack == null || masterRack.IsDisposed)
            {
                Main main = this;
                masterRack = new MasterRack(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(masterRack);
                masterRack.MdiParent = this;
                masterRack.Show();
            }
            else
            {
                masterRack.Focus();
            }
        }
        private void CurrencyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (masterKurs == null || masterKurs.IsDisposed)
            {
                Main main = this;

                masterKurs = new MasterKurs(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(masterKurs);
                masterKurs.MdiParent = this;
                masterKurs.Show();
            }
            else
            {
                masterKurs.Focus();
            }
        }
        private void EmployeeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (masterEmployee == null || masterEmployee.IsDisposed)
            {
                Main main = this;

                masterEmployee = new MasterEmployee(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(masterEmployee);
                masterEmployee.MdiParent = this;
                masterEmployee.Show();
            }
            else
            {
                masterEmployee.Focus();
            }
        }
        private void CustomerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (masterCustomer == null || masterCustomer.IsDisposed)
            {
                Main main = this;

                masterCustomer = new MasterCustomer(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(masterCustomer);
                masterCustomer.MdiParent = this;
                masterCustomer.Show();
            }
            else
            {
                masterCustomer.Focus();
            }
        }

        private void PurchaseReturnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (purchaseReturn == null || purchaseReturn.IsDisposed)
            {
                Main main = this;

                purchaseReturn = new PurchaseReturn(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(purchaseReturn);
                purchaseReturn.MdiParent = this;
                purchaseReturn.Show();
            }
            else
            {
                purchaseReturn.Focus();
            }
        }
        private void PurchaseOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (purchaseOrder == null || purchaseOrder.IsDisposed)
            {
                Main main = this;

                purchaseOrder = new PurchaseOrder(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(purchaseOrder);
                purchaseOrder.MdiParent = this;
                purchaseOrder.Show();
            }
            else
            {
                purchaseOrder.Focus();
            }
        }
        private void POViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (purhcaseOrderView == null || purhcaseOrderView.IsDisposed)
            {
                Main main = this;

                purhcaseOrderView = new PurchaseOrderView(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(purhcaseOrderView);
                purhcaseOrderView.MdiParent = this;
                purhcaseOrderView.Show();
            }
            else
            {
                purhcaseOrderView.Focus();
            }
        }
        private void ReceiptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (purchaseReceipt == null || purchaseReceipt.IsDisposed)
            {
                Main main = this;

                purchaseReceipt = new PurchaseReceipt(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(purchaseReceipt);
                purchaseReceipt.MdiParent = this;
                purchaseReceipt.Show();
            }
            else
            {
                purchaseReceipt.Focus();
            }
        }

        private void ReceiptRevisionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (receiptRevision == null || receiptRevision.IsDisposed)
            {
                Main main = this;

                receiptRevision = new PurchaseReceiptRevision(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(receiptRevision);
                receiptRevision.MdiParent = this;
                receiptRevision.Show();
            }
            else
            {
                receiptRevision.Focus();
            }
        }

        private void SalesOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (salesOrder == null || salesOrder.IsDisposed)
            {
                Main main = this;

                salesOrder = new SalesOrder(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(salesOrder);
                salesOrder.MdiParent = this;
                salesOrder.Show();
            }
            else
            {
                salesOrder.Focus();
            }
        }
        private void OrderPickingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (orderPicking == null || orderPicking.IsDisposed)
            {
                Main main = this;

                orderPicking = new OrderPicking(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(orderPicking);
                orderPicking.MdiParent = this;
                orderPicking.Show();
            }
            else
            {
                orderPicking.Focus();
            }
        }
        private void QuotationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (quotation == null || quotation.IsDisposed)
            {
                Main main = this;

                quotation = new Quotation(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(quotation);
                quotation.MdiParent = this;
                quotation.Show();
            }
            else
            {
                quotation.Focus();
            }
        }
        private void CostToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (cost == null || cost.IsDisposed)
            {
                Main main = this;

                cost = new Cost(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(cost);
                cost.MdiParent = this;
                cost.Show();
            }
            else
            {
                cost.Focus();
            }
        }
        private void SalesReturnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (salesReturn == null || salesReturn.IsDisposed)
            {
                Main main = this;

                salesReturn = new SalesReturn(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(salesReturn);
                salesReturn.MdiParent = this;
                salesReturn.Show();
            }
            else
            {
                salesReturn.Focus();
            }
        }

        private void ImportToolStripMenuItems_Click(object sender, EventArgs e)
        {
            if (importexport == null || importexport.IsDisposed)
            {
                Main main = this;

                importexport = new ImportExportForm(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(importexport);
                importexport.MdiParent = this;
                importexport.Show();
            }
            else
            {
                importexport.Focus();
            }
        }

        private void InventoryAuditToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (recordCardStokItem == null || recordCardStokItem.IsDisposed)
            {
                Main main = this;

                recordCardStokItem = new RecordCardStokItem(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(recordCardStokItem);
                recordCardStokItem.MdiParent = this;
                recordCardStokItem.Show();
            }
            else
            {
                recordCardStokItem.Focus();
            }
        }

        private void AdjustmentSpoilToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (adjustmentspoil == null || adjustmentspoil.IsDisposed)
            {
                Main main = this;

                adjustmentspoil = new Adjustment_Spoil(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(adjustmentspoil);
                adjustmentspoil.MdiParent = this;
                adjustmentspoil.Show();
            }
            else
            {
                adjustmentspoil.Focus();
            }
        }

        private void ImportExportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (importexport == null || importexport.IsDisposed)
            {
                Main main = this;

                importexport = new ImportExportForm(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(importexport);
                importexport.MdiParent = this;
                importexport.Show();
            }
            else
            {
                importexport.Focus();
            }
        }

        private void ChangePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (changepassword == null || changepassword.IsDisposed)
            {
                Main main = this;

                changepassword = new ChangePassword(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(changepassword);
                changepassword.MdiParent = this;
                changepassword.Show();
            }
            else {
                changepassword.Focus();
            }
        }

        private void AuthenticationUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Main main = this;
            autheuser = new Sufindo.Setting.AuthenticationUser(ref main, ref menuStripAction);
            autheuser.MdiParent = this;
            autheuser.Show();
        }

        private void AuthenticationPriceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Main main = this;
            autheprice = new Sufindo.Setting.AuthenticationPrice(ref main, ref menuStripAction);
            autheprice.MdiParent = this;
            autheprice.Show();
        }

        private void AllReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (allreport == null || allreport.IsDisposed)
            {
                Main main = this;

                allreport = new AllReportForm(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(allreport);
                allreport.MdiParent = this;
                allreport.Show();
            }
            else
            {
                allreport.Focus();
            }
        }
        private void InventoryAuditReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (inventoryauditreport == null || inventoryauditreport.IsDisposed)
            {
                Main main = this;

                inventoryauditreport = new InventoryAuditReport(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(inventoryauditreport);
                inventoryauditreport.MdiParent = this;
                inventoryauditreport.Show();
            }
            else
            {
                inventoryauditreport.Focus();
            }
        }

        private void orderPickingReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (orderpickingreport == null || orderpickingreport.IsDisposed)
            {
                Main main = this;

                orderpickingreport = new OrderPickingReport(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(orderpickingreport);
                orderpickingreport.MdiParent = this;
                orderpickingreport.Show();
            }
            else
            {
                orderpickingreport.Focus();
            }
        }

        private void ListInvoiceReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            if (listinvoicereport == null || listinvoicereport.IsDisposed)
            {
                Main main = this;

                listinvoicereport = new ListInvoiceReport(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(listinvoicereport);
                listinvoicereport.MdiParent = this;
                listinvoicereport.Show();
            }
            else
            {
                listinvoicereport.Focus();
            }
        }

        private void listAdjustmentReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listadjustmentreport == null || listadjustmentreport.IsDisposed)
            {
                Main main = this;

                listadjustmentreport = new ListAdjustmentReport(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(listadjustmentreport);
                listadjustmentreport.MdiParent = this;
                listadjustmentreport.Show();
            }
            else
            {
                listadjustmentreport.Focus();
            }
        }

        private void listSpoilReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listspoilreport == null || listspoilreport.IsDisposed)
            {
                Main main = this;

                listspoilreport = new ListSpoilReport(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(listspoilreport);
                listspoilreport.MdiParent = this;
                listspoilreport.Show();
            }
            else
            {
                listspoilreport.Focus();
            }
        }

        private void stockTagReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (stocktagreport == null || stocktagreport.IsDisposed)
            {
                Main main = this;

                stocktagreport = new StockTagReport(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(stocktagreport);
                stocktagreport.MdiParent = this;
                stocktagreport.Show();
            }
            else
            {
                stocktagreport.Focus();
            }
        }

        private void orderPickingWithInvoiceReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (orderpickingwithinvoicereport == null || orderpickingwithinvoicereport.IsDisposed)
            {
                Main main = this;

                orderpickingwithinvoicereport = new OrderPickingWithInvoiceReport(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(orderpickingwithinvoicereport);
                orderpickingwithinvoicereport.MdiParent = this;
                orderpickingwithinvoicereport.Show();
            }
            else
            {
                orderpickingwithinvoicereport.Focus();
            }
        }

        private void listPartNoByRackReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listitembyrakreport == null || listitembyrakreport.IsDisposed)
            {
                Main main = this;

                listitembyrakreport = new ListItemByRakReport(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(listitembyrakreport);
                listitembyrakreport.MdiParent = this;
                listitembyrakreport.Show();
            }
            else
            {
                listitembyrakreport.Focus();
            }
        }

        private void listPurchaseDirectReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listpurchasedirectreport == null || listpurchasedirectreport.IsDisposed)
            {
                Main main = this;

                listpurchasedirectreport = new ListPurchaseDirectReport(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(listpurchasedirectreport);
                listpurchasedirectreport.MdiParent = this;
                listpurchasedirectreport.Show();
            }
            else
            {
                listpurchasedirectreport.Focus();
            }
        }

        private void listPurchaseIndirectReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listpurchaseindirectreport == null || listpurchaseindirectreport.IsDisposed)
            {
                Main main = this;

                listpurchaseindirectreport = new ListPurchaseIndirectReport(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(listpurchaseindirectreport);
                listpurchaseindirectreport.MdiParent = this;
                listpurchaseindirectreport.Show();
            }
            else
            {
                listpurchaseindirectreport.Focus();
            }
        }

        private void listInvoicePerCustomerReportToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (listinvoicepercustomerreport == null || listinvoicepercustomerreport.IsDisposed)
            {
                Main main = this;

                listinvoicepercustomerreport = new ListInvoicePerCustomerReport(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(listinvoicepercustomerreport);
                listinvoicepercustomerreport.MdiParent = this;
                listinvoicepercustomerreport.Show();
            }
            else
            {
                listinvoicepercustomerreport.Focus();
            }
        }

        private void listPurchaseReturnReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listpurchasereturnreport == null || listpurchasereturnreport.IsDisposed)
            {
                Main main = this;

                listpurchasereturnreport = new ListPurchaseReturnReport(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(listpurchasereturnreport);
                listpurchasereturnreport.MdiParent = this;
                listpurchasereturnreport.Show();
            }
            else
            {
                listpurchasereturnreport.Focus();
            }
        }

        private void listInvoiceTotalReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listinvoicetotalreport == null || listinvoicetotalreport.IsDisposed)
            {
                Main main = this;

                listinvoicetotalreport = new ListInvoiceTotalReport(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(listinvoicetotalreport);
                listinvoicetotalreport.MdiParent = this;
                listinvoicetotalreport.Show();
            }
            else
            {
                listinvoicetotalreport.Focus();
            }
        }

        private void listSalesReturnReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (salesreturnreport == null || salesreturnreport.IsDisposed)
            {
                Main main = this;

                salesreturnreport = new SalesReturnReport(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(salesreturnreport);
                salesreturnreport.MdiParent = this;
                salesreturnreport.Show();
            }
            else
            {
                salesreturnreport.Focus();
            }
            
        }

        private void listSalesOrderReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listsalesorderreport == null || listsalesorderreport.IsDisposed)
            {
                Main main = this;
                listsalesorderreport = new ListSalesOrderReport(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(listsalesorderreport);
                listsalesorderreport.MdiParent = this;
                listsalesorderreport.Show();
            }
            else
            {
                salesreturnreport.Focus();
            }
        }

        private void listReceiptRevisionReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listpurchaserevisionreport == null || listpurchaserevisionreport.IsDisposed)
            {
                Main main = this;
                listpurchaserevisionreport = new ListPurchaseRevisionReport(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(listpurchaserevisionreport);
                listpurchaserevisionreport.MdiParent = this;
                listpurchaserevisionreport.Show();
            }
            else
            {
                listpurchaserevisionreport.Focus();
            }
        }

        private void listEOBIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listeobireport == null || listeobireport.IsDisposed)
            {
                Main main = this;
                listeobireport = new ListEOBIReport(ref main, ref menuStripAction);
                if (LISTFORM == null) LISTFORM = new List<Form>();
                LISTFORM.Add(listeobireport);
                listeobireport.MdiParent = this;
                listeobireport.Show();
            }
            else
            {
                listeobireport.Focus();
            }
        }
    }
}
