﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;

namespace CustomControls
{
    public class TextBoxSL : TextBox
    {
        private Boolean _numeric;
        private Boolean _decimal;
        private Boolean _money;
        private String _prefix;
        private Double _decimalValue;
        private Boolean _onEnter;
        public TextBoxSL()
        {
            this._numeric = false;
            this._decimal = false;
            this._money = false;
            this._onEnter = false;
            this._prefix = String.Empty;
            this._decimalValue = 0.00;
            this.KeyPress += new KeyPressEventHandler(TextBoxCustom_KeyPress);
            this.Enter += new EventHandler(TextBoxCustom_Enter);
            this.Leave += new EventHandler(TextBoxCustom_Leave);
        }

        void TextBoxCustom_Leave(object sender, EventArgs e)
        {
            if (this._money) {
                this._onEnter = false;
                Double money = this.Text.Equals(String.Empty) ? 0.00 : Double.Parse(this.Text);
                this.Text = money.ToString("N2");
            }
        }

        void TextBoxCustom_Enter(object sender, EventArgs e)
        {
            if (this._money)
            {
                this._onEnter = true;
                if (!this._prefix.Equals(String.Empty))
                {
                    this.Text = this.Text.Replace(this._prefix, String.Empty);
                }
                this.Text = this.Text.Replace(",", String.Empty);
            }
        }

        void TextBoxCustom_KeyPress(object sender, KeyPressEventArgs e){
            if (this._numeric) {
                e.Handled = (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar));
                return;
            }

            if (this._decimal || this._money)
            {
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                    return;
                }

                // only allow one decimal point
                if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                    return;
                }

                //if (!char.IsControl(e.KeyChar))
                //{
                //    TextBoxCustom textBox = (TextBoxCustom)sender;
                //    if (textBox.Text.IndexOf('.') > -1 && textBox.Text.Substring(textBox.Text.IndexOf('.')).Length >= 3)
                //    {
                //        e.Handled = true;
                //        Console.WriteLine("TRUE3");
                //        return;
                //    }
                //}
                e.Handled = false;
                return;
            }
        }

        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                if (_money && !this._onEnter)
                {
                    if (!this._prefix.Equals(String.Empty))
                    {
                        base.Text = value.Equals(String.Empty) ? "" : String.Format("{0}{1}", this._prefix, Double.Parse(value).ToString("N2"));
                    }
                    else
                    {
                        base.Text = value.Equals(String.Empty) ? "" : Double.Parse(value).ToString("N2");
                    }
                }
                else
                {
                    base.Text = value;
                }
            }
        }

        [Browsable(true)]
        [Category("Extended Properties")]
        [Description("sets Prefix")]
        [DisplayName("Prefix")]
        public String Prefix
        {
            get
            {
                return _prefix;
            }
            set
            {
                this._prefix = value;
            }
        }

        [Browsable(true)]
        [Category("Extended Properties")]
        [Description("sets Numeric Type")]
        [DisplayName("Numeric")]
        public Boolean Numeric {
            get {
                return _numeric;
            }
            set {
                this._numeric = value;
            }
        }

        [Browsable(true)]
        [Category("Extended Properties")]
        [Description("sets Money Type")]
        [DisplayName("Money")]
        public Boolean Money
        {
            get
            {
                return _money;
            }
            set
            {
                this._money = value;
            }
        }

        [Browsable(true)]
        [Category("Extended Properties")]
        [Description("sets Decimal Type")]
        [DisplayName("Decimal")]
        public Boolean Decimal
        {
            get
            {
                return _decimal;
            }
            set
            {
                this._decimal = value;
            }
        }

        [Browsable(true)]
        [Category("Extended Properties")]
        [Description("get Decimal Value")]
        [DisplayName("DecimalValue")]
        public Double DecimalValue
        {
            get
            {
                if (this._decimal || this._money)
                {
                    String value = this.Text.Equals(String.Empty) ? "0" : this.Text;
                    if (!this._prefix.Equals(String.Empty))
                    {
                        value = value.Replace(this._prefix, "");
                    }
                    value = value.Replace(",", "");
                    this._decimalValue = Double.Parse(value);
                }
                else {
                    this._decimalValue = 0;
                }
                return _decimalValue;
            }
        }
    }
}
